rule Methodology_ScriptObf_ReplaceEmpty {
    meta:
        author = "Zander Work (@captainGeech42)"
        descr = "Detects the use of string.Replace() or similar, where the replacement string is an empty string. This is a common technique for basic script obfuscation."
    strings:
        // doesn't hit on the search string being passed in as a variable FYSA
        $re1 = /replace\(["'].*["'], ["']["']\)/ nocase // catches basic usage in at least python and powershell
        $re2 = /replace\(["'].*["'], ["']["'], \d+\)/ // python str.replace has an optional third argument, a number. this only catches a decimal number fysa
        $re3 = /replace\(\/.*\/\w*, ["']["']\)/ // javascript String.prototype.replace can take a regex pattern for the first argument
        $re4 = /replace\(\w+, ["'].*["'], ["']["']/ nocase // vbscript replace takes at least 3 arguments: var to replace in, search string, replacement string. there are three more optional args
    condition:
        any of them
}