






<!DOCTYPE html>
<html lang="en"  class="skip-scroll-target-into-view js-skip-scroll-target-into-view"data-color-mode="auto" data-light-theme="light" data-dark-theme="dark" data-a11y-animated-images="system">
  <head>
    <meta charset="utf-8">
  <link rel="dns-prefetch" href="https://github.githubassets.com">
  <link rel="dns-prefetch" href="https://avatars.githubusercontent.com">
  <link rel="dns-prefetch" href="https://github-cloud.s3.amazonaws.com">
  <link rel="dns-prefetch" href="https://user-images.githubusercontent.com/">
  <link rel="preconnect" href="https://github.githubassets.com" crossorigin>
  <link rel="preconnect" href="https://avatars.githubusercontent.com">



  <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/light-719f1193e0c0.css" /><link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/dark-0c343b529849.css" /><link data-color-theme="dark_dimmed" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_dimmed-f22da508b62a.css" /><link data-color-theme="dark_high_contrast" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_high_contrast-188ef1de59e6.css" /><link data-color-theme="dark_colorblind" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_colorblind-bc6bf4eea850.css" /><link data-color-theme="light_colorblind" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/light_colorblind-527658dec362.css" /><link data-color-theme="light_high_contrast" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/light_high_contrast-c7a7fe0cd8ec.css" /><link data-color-theme="light_tritanopia" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/light_tritanopia-6aa855bdae0f.css" /><link data-color-theme="dark_tritanopia" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_tritanopia-6aa5e25aacc0.css" />
  
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/primer-bb7dbc10bec9.css" />
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/global-f6f644aba0fa.css" />
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/github-c0e74c06514a.css" />
  <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/code-604d9fc8aedf.css" />
<link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/pull-requests-4285468b4eb7.css" />



  <script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/wp-runtime-f34e1f3b09d2.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_manuelpuyol_turbo_dist_turbo_es2017-esm_js-7ca92c8d513d.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_stacktrace-parser_dist_stack-trace-parser_esm_js-node_modules_github_bro-d351f6-c1d63d230b29.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/environment-77eab2b50cc9.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_selector-observer_dist_index_esm_js-650337916dbd.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_delegated-events_dist_index_js-node_modules_github_details-dialog-elemen-63debe-4a2f37f7419e.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_auto-complete-element_dist_index_js-node_modules_github_catalyst_-6afc16-fa4f01d20b81.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_fzy_js_index_js-node_modules_github_markdown-toolbar-element_dist_index_js-5936f45973f5.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_time-elements_dist_index_js-87d1befb43a5.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_file-attachment-element_dist_index_js-node_modules_github_text-ex-3415a8-daf4603c6e6b.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_filter-input-element_dist_index_js-node_modules_github_remote-inp-45d711-c6afdc0ca525.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/github-elements-02da082a040c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/element-registry-1bdd48b00a0c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_lit-html_lit-html_js-e954e8c01c93.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_morphdom_dist_morphdom-esm_js-node_modules_github_template-parts_lib_index_js-b4d65e83ab78.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_mini-throttle_dist_index_js-node_modules_github_remote-form_dist_-ece2b0-917c0dad4566.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_alive-client_dist_index_js-156187f13fbb.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_filter-input-element_dist_index_js-node_modules_github_paste-mark-25e505-67cf313e4f47.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_updatable-content_ts-62df1309213c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_sticky-scroll-into-view_ts-719d6a33bb20.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_behaviors_keyboard-shortcuts-helper_ts-app_assets_modules_github_be-ac2ea2-d1047ec820f8.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_blob-anchor_ts-app_assets_modules_github_hydro-analytics_ts-app_ass-7216b6-af4807f92230.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_behaviors_commenting_edit_ts-app_assets_modules_github_behaviors_ht-83c235-02b38260d85d.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_behaviors_batch-deferred-content_ts-app_assets_modules_github_failb-23a201-2fad9a90d714.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/behaviors-01aae67c31ed.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_delegated-events_dist_index_js-node_modules_github_catalyst_lib_index_js-06ff532-0eddb8f4d122.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/notifications-global-f030ce14eb3f.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_remote-form_dist_index_js-node_modules_delegated-events_dist_inde-e767ec-2f0e32b1ee84.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_ref-selector_ts-729bf915ace6.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/codespaces-8df7b9b05929.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_clipboard-copy-element_dist_index_esm_js-node_modules_github_remo-8e6bec-4a54aa10cc28.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_mini-throttle_dist_decorators_js-node_modules_scroll-anchoring_di-4942f3-1b4587daed8c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_diffs_blob-lines_ts-app_assets_modules_github_diffs_linkable-line-n-71fb2d-ebe2b66b277c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/diffs-011d7c937f44.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_mini-throttle_dist_index_js-node_modules_delegated-events_dist_in-3fcf56-55af9a9cce78.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/scanning-524b1d86dfbc.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/code-menu-8daa97ff737b.js"></script>
  

  <title>Add SM4 rules by sylvainpelissier · Pull Request #428 · Yara-Rules/rules · GitHub</title>



    

  <meta name="request-id" content="2026:8C41:37119D6:38DAE68:636FD1A3" data-pjax-transient="true"/><meta name="html-safe-nonce" content="15794ad160a6bb649bf583bad99025145dccf981a2db68f8ac9256cceb1316fa" data-pjax-transient="true"/><meta name="visitor-payload" content="eyJyZWZlcnJlciI6IiIsInJlcXVlc3RfaWQiOiIyMDI2OjhDNDE6MzcxMTlENjozOERBRTY4OjYzNkZEMUEzIiwidmlzaXRvcl9pZCI6IjQwOTIzODc0OTAwNDU4NzQ1OTUiLCJyZWdpb25fZWRnZSI6ImZyYSIsInJlZ2lvbl9yZW5kZXIiOiJmcmEifQ==" data-pjax-transient="true"/><meta name="visitor-hmac" content="1c914971fa46ec4c80f3dfaab65423ab0f19afb5ac734db8d124b853b464882a" data-pjax-transient="true"/>

    <meta name="hovercard-subject-tag" content="pull_request:934788780" data-turbo-transient>


  <meta name="github-keyboard-shortcuts" content="repository,pull-request-list,pull-request-conversation,pull-request-files-changed" data-turbo-transient="true" />
  

  <meta name="selected-link" value="repo_pulls" data-turbo-transient>

    <meta name="google-site-verification" content="c1kuD-K2HIVF635lypcsWPoD4kilo5-jA_wBFyT4uMY">
  <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
  <meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
  <meta name="google-site-verification" content="GXs5KoUUkNCoaAZn7wPN-t01Pywp9M3sEjnt_3_ZWPc">
  <meta name="google-site-verification" content="Apib7-x98H0j5cPqHWwSMm6dNU4GmODRoqxLiDzdx9I">

<meta name="octolytics-url" content="https://collector.github.com/github/collect" />

  <meta name="analytics-location" content="/&lt;user-name&gt;/&lt;repo-name&gt;/pull_requests/show/commits" data-turbo-transient="true" />

  




  

    <meta name="user-login" content="">

  

    <meta name="viewport" content="width=device-width">
    
      <meta name="description" content="SM4 constants according to https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10.">
      <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <meta property="fb:app_id" content="1401488693436528">
    <meta name="apple-itunes-app" content="app-id=1477376905" />
      <meta name="twitter:image:src" content="https://avatars.githubusercontent.com/u/7047419?s=400&amp;v=4" /><meta name="twitter:site" content="@github" /><meta name="twitter:card" content="summary_large_image" /><meta name="twitter:title" content="Add SM4 rules by sylvainpelissier · Pull Request #428 · Yara-Rules/rules" /><meta name="twitter:description" content="SM4 constants according to https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10." />
      <meta property="og:image" content="https://avatars.githubusercontent.com/u/7047419?s=400&amp;v=4" /><meta property="og:image:alt" content="SM4 constants according to https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10." /><meta property="og:site_name" content="GitHub" /><meta property="og:type" content="object" /><meta property="og:title" content="Add SM4 rules by sylvainpelissier · Pull Request #428 · Yara-Rules/rules" /><meta property="og:url" content="https://github.com/Yara-Rules/rules/pull/428" /><meta property="og:description" content="SM4 constants according to https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10." />
      
    <link rel="assets" href="https://github.githubassets.com/">


        <meta name="hostname" content="github.com">



        <meta name="expected-hostname" content="github.com">

    <meta name="enabled-features" content="IMAGE_METRIC_TRACKING,GEOJSON_AZURE_MAPS,STRICT_DEFERRED_DIFF_LINES_CHECKS">


  <meta http-equiv="x-pjax-version" content="3833854dd73a3a3b51c8d5173255d25587161579a8b56b3bd5347153d7ea1918" data-turbo-track="reload">
  <meta http-equiv="x-pjax-csp-version" content="40aca5a152a13213a876f7628c466cd600db12fb858cdddccc3f1cc387eb7dad" data-turbo-track="reload">
  <meta http-equiv="x-pjax-css-version" content="42601a5db6a8c11c3528914960ecfc0d469473967509ff4cf528c7e772604c9e" data-turbo-track="reload">
  <meta http-equiv="x-pjax-js-version" content="0a6b9b04c66d1ffa11c43d7c6bd11568360b521cadb379a63969c5fbede48458" data-turbo-track="reload">

  <meta name="turbo-cache-control" content="no-preview" data-turbo-transient="">

      <link data-turbo-transient rel="alternate" type="text/x-diff" href="/Yara-Rules/rules/pull/428.diff">
  <link data-turbo-transient rel="alternate" type="text/x-patch" href="/Yara-Rules/rules/pull/428.patch">
  <meta name="diff-view" content="unified" data-turbo-transient>

  <meta name="go-import" content="github.com/Yara-Rules/rules git https://github.com/Yara-Rules/rules.git">

  <meta name="octolytics-dimension-user_id" content="11882970" /><meta name="octolytics-dimension-user_login" content="Yara-Rules" /><meta name="octolytics-dimension-repository_id" content="33764615" /><meta name="octolytics-dimension-repository_nwo" content="Yara-Rules/rules" /><meta name="octolytics-dimension-repository_public" content="true" /><meta name="octolytics-dimension-repository_is_fork" content="false" /><meta name="octolytics-dimension-repository_network_root_id" content="33764615" /><meta name="octolytics-dimension-repository_network_root_nwo" content="Yara-Rules/rules" />



  <meta name="turbo-body-classes" content="logged-out env-production page-responsive">


  <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">

  <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">

  <meta name="browser-optimizely-client-errors-url" content="https://api.github.com/_private/browser/optimizely_client/errors">

  <link rel="mask-icon" href="https://github.githubassets.com/pinned-octocat.svg" color="#000000">
  <link rel="alternate icon" class="js-site-favicon" type="image/png" href="https://github.githubassets.com/favicons/favicon.png">
  <link rel="icon" class="js-site-favicon" type="image/svg+xml" href="https://github.githubassets.com/favicons/favicon.svg">

<meta name="theme-color" content="#1e2327">
<meta name="color-scheme" content="light dark" />


  <link rel="manifest" href="/manifest.json" crossOrigin="use-credentials">

  </head>

  <body class="logged-out env-production page-responsive" style="word-wrap: break-word;">
    


    <div class="position-relative js-header-wrapper ">
      <a href="#start-of-content" class="px-2 py-4 color-bg-accent-emphasis color-fg-on-emphasis show-on-focus js-skip-to-content">Skip to content</a>
      <span data-view-component="true" class="progress-pjax-loader Progress position-fixed width-full">
    <span style="width: 0%;" data-view-component="true" class="Progress-item progress-pjax-loader-bar left-0 top-0 color-bg-accent-emphasis"></span>
</span>      
      


        

            <script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_remote-form_dist_index_js-node_modules_delegated-events_dist_inde-94fd67-30d1a2e5d8ef.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/sessions-ce716ddb3189.js"></script>
<header class="Header-old header-logged-out js-details-container Details position-relative f4 py-3" role="banner">
  <button type="button" class="Header-backdrop d-lg-none border-0 position-fixed top-0 left-0 width-full height-full js-details-target" aria-label="Toggle navigation">
    <span class="d-none">Toggle navigation</span>
  </button>

  <div class="container-xl d-flex flex-column flex-lg-row flex-items-center p-responsive height-full position-relative z-1">
    <div class="d-flex flex-justify-between flex-items-center width-full width-lg-auto">
      <a class="mr-lg-3 color-fg-inherit flex-order-2" href="https://github.com/" aria-label="Homepage" data-ga-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
        <svg height="32" aria-hidden="true" viewBox="0 0 16 16" version="1.1" width="32" data-view-component="true" class="octicon octicon-mark-github">
    <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
</svg>
      </a>

        <div class="flex-1">
          <a href="/signup?ref_cta=Sign+up&amp;ref_loc=header+logged+out&amp;ref_page=%2F%3Cuser-name%3E%2F%3Crepo-name%3E%2Fpull_requests%2Fshow%2Fcommits&amp;source=header-repo"
            class="d-inline-block d-lg-none flex-order-1 f5 no-underline border color-border-default rounded-2 px-2 py-1 color-fg-inherit"
            data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;site header&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="57f44a6d67a2b0698b238b5cb34409bf2a055de32d83ea9e1f918529f4515923"
          >
            Sign&nbsp;up
          </a>
        </div>

      <div class="flex-1 flex-order-2 text-right">
          <button aria-label="Toggle navigation" aria-expanded="false" type="button" data-view-component="true" class="js-details-target Button--link Button--medium Button d-lg-none color-fg-inherit p-1">    <span class="Button-content">
      <span class="Button-label"><div class="HeaderMenu-toggle-bar rounded my-1"></div>
            <div class="HeaderMenu-toggle-bar rounded my-1"></div>
            <div class="HeaderMenu-toggle-bar rounded my-1"></div></span>
    </span>
</button>  
      </div>
    </div>


    <div class="HeaderMenu--logged-out p-responsive height-fit position-lg-relative d-lg-flex flex-column flex-auto pt-7 pb-4 top-0">
      <div class="header-menu-wrapper d-flex flex-column flex-self-end flex-lg-row flex-justify-between flex-auto p-3 p-lg-0 rounded rounded-lg-0 mt-3 mt-lg-0">
          <nav class="mt-0 px-3 px-lg-0 mb-3 mb-lg-0" aria-label="Global">
            <ul class="d-lg-flex list-style-none">
                <li class="HeaderMenu-item position-relative flex-wrap flex-justify-between flex-items-center d-block d-lg-flex flex-lg-nowrap flex-lg-items-center js-details-container js-header-menu-item">
    <button type="button" class="HeaderMenu-link border-0 width-full width-lg-auto px-0 px-lg-2 py-3 py-lg-2 no-wrap d-flex flex-items-center flex-justify-between js-details-target" aria-expanded="false">
      Product
      <svg opacity="0.5" aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-chevron-down HeaderMenu-icon ml-1">
    <path fill-rule="evenodd" d="M12.78 6.22a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06 0L3.22 7.28a.75.75 0 011.06-1.06L8 9.94l3.72-3.72a.75.75 0 011.06 0z"></path>
</svg>
    </button>
    <div class="HeaderMenu-dropdown dropdown-menu rounded m-0 p-0 py-2 py-lg-4 position-relative position-lg-absolute left-0 left-lg-n3 d-lg-flex dropdown-menu-wide">
        <ul class="list-style-none f5 px-lg-4 border-lg-right mb-4 mb-lg-0 pr-lg-7">

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Actions&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Actions;&quot;}" href="/features/actions">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-workflow color-fg-subtle mr-3">
    <path fill-rule="evenodd" d="M1 3a2 2 0 012-2h6.5a2 2 0 012 2v6.5a2 2 0 01-2 2H7v4.063C7 16.355 7.644 17 8.438 17H12.5v-2.5a2 2 0 012-2H21a2 2 0 012 2V21a2 2 0 01-2 2h-6.5a2 2 0 01-2-2v-2.5H8.437A2.938 2.938 0 015.5 15.562V11.5H3a2 2 0 01-2-2V3zm2-.5a.5.5 0 00-.5.5v6.5a.5.5 0 00.5.5h6.5a.5.5 0 00.5-.5V3a.5.5 0 00-.5-.5H3zM14.5 14a.5.5 0 00-.5.5V21a.5.5 0 00.5.5H21a.5.5 0 00.5-.5v-6.5a.5.5 0 00-.5-.5h-6.5z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Actions</div>
        Automate any workflow
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Packages&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Packages;&quot;}" href="/features/packages">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-package color-fg-subtle mr-3">
    <path fill-rule="evenodd" d="M12.876.64a1.75 1.75 0 00-1.75 0l-8.25 4.762a1.75 1.75 0 00-.875 1.515v9.525c0 .625.334 1.203.875 1.515l8.25 4.763a1.75 1.75 0 001.75 0l8.25-4.762a1.75 1.75 0 00.875-1.516V6.917a1.75 1.75 0 00-.875-1.515L12.876.639zm-1 1.298a.25.25 0 01.25 0l7.625 4.402-7.75 4.474-7.75-4.474 7.625-4.402zM3.501 7.64v8.803c0 .09.048.172.125.216l7.625 4.402v-8.947L3.501 7.64zm9.25 13.421l7.625-4.402a.25.25 0 00.125-.216V7.639l-7.75 4.474v8.947z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Packages</div>
        Host and manage packages
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Security&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Security;&quot;}" href="/features/security">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-shield-check color-fg-subtle mr-3">
    <path d="M16.53 9.78a.75.75 0 00-1.06-1.06L11 13.19l-1.97-1.97a.75.75 0 00-1.06 1.06l2.5 2.5a.75.75 0 001.06 0l5-5z"></path><path fill-rule="evenodd" d="M12.54.637a1.75 1.75 0 00-1.08 0L3.21 3.312A1.75 1.75 0 002 4.976V10c0 6.19 3.77 10.705 9.401 12.83.386.145.812.145 1.198 0C18.229 20.704 22 16.19 22 10V4.976c0-.759-.49-1.43-1.21-1.664L12.54.637zm-.617 1.426a.25.25 0 01.154 0l8.25 2.676a.25.25 0 01.173.237V10c0 5.461-3.28 9.483-8.43 11.426a.2.2 0 01-.14 0C6.78 19.483 3.5 15.46 3.5 10V4.976c0-.108.069-.203.173-.237l8.25-2.676z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Security</div>
        Find and fix vulnerabilities
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Codespaces&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Codespaces;&quot;}" href="/features/codespaces">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-codespaces color-fg-subtle mr-3">
    <path fill-rule="evenodd" d="M3.5 3.75C3.5 2.784 4.284 2 5.25 2h13.5c.966 0 1.75.784 1.75 1.75v7.5A1.75 1.75 0 0118.75 13H5.25a1.75 1.75 0 01-1.75-1.75v-7.5zm1.75-.25a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h13.5a.25.25 0 00.25-.25v-7.5a.25.25 0 00-.25-.25H5.25zM1.5 15.75c0-.966.784-1.75 1.75-1.75h17.5c.966 0 1.75.784 1.75 1.75v4a1.75 1.75 0 01-1.75 1.75H3.25a1.75 1.75 0 01-1.75-1.75v-4zm1.75-.25a.25.25 0 00-.25.25v4c0 .138.112.25.25.25h17.5a.25.25 0 00.25-.25v-4a.25.25 0 00-.25-.25H3.25z"></path><path fill-rule="evenodd" d="M10 17.75a.75.75 0 01.75-.75h6.5a.75.75 0 010 1.5h-6.5a.75.75 0 01-.75-.75zm-4 0a.75.75 0 01.75-.75h.5a.75.75 0 010 1.5h-.5a.75.75 0 01-.75-.75z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Codespaces</div>
        Instant dev environments
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Copilot&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Copilot;&quot;}" href="/features/copilot">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-copilot color-fg-subtle mr-3">
    <path d="M9.75 14a.75.75 0 01.75.75v2.5a.75.75 0 01-1.5 0v-2.5a.75.75 0 01.75-.75zm4.5 0a.75.75 0 01.75.75v2.5a.75.75 0 01-1.5 0v-2.5a.75.75 0 01.75-.75z"></path><path fill-rule="evenodd" d="M12 2c-2.214 0-4.248.657-5.747 1.756a7.43 7.43 0 00-.397.312c-.584.235-1.077.546-1.474.952-.85.87-1.132 2.037-1.132 3.368 0 .368.014.733.052 1.086l-.633 1.478-.043.022A4.75 4.75 0 000 15.222v1.028c0 .529.31.987.564 1.293.28.336.637.653.967.918a13.262 13.262 0 001.299.911l.024.015.006.004.04.025.144.087c.124.073.304.177.535.3.46.245 1.122.57 1.942.894C7.155 21.344 9.439 22 12 22s4.845-.656 6.48-1.303c.819-.324 1.481-.65 1.941-.895a13.797 13.797 0 00.68-.386l.039-.025.006-.004.024-.015a8.829 8.829 0 00.387-.248c.245-.164.577-.396.912-.663.33-.265.686-.582.966-.918.256-.306.565-.764.565-1.293v-1.028a4.75 4.75 0 00-2.626-4.248l-.043-.022-.633-1.478c.038-.353.052-.718.052-1.086 0-1.331-.282-2.499-1.132-3.368-.397-.406-.89-.717-1.474-.952a7.386 7.386 0 00-.397-.312C16.248 2.657 14.214 2 12 2zm-8 9.654l.038-.09c.046.06.094.12.145.177.793.9 2.057 1.259 3.782 1.259 1.59 0 2.739-.544 3.508-1.492.131-.161.249-.331.355-.508a32.948 32.948 0 00.344 0c.106.177.224.347.355.508.77.948 1.918 1.492 3.508 1.492 1.725 0 2.989-.359 3.782-1.259.05-.057.099-.116.145-.177l.038.09v6.669a17.618 17.618 0 01-2.073.98C16.405 19.906 14.314 20.5 12 20.5c-2.314 0-4.405-.594-5.927-1.197A17.62 17.62 0 014 18.323v-6.67zm6.309-1.092a2.35 2.35 0 01-.38.374c-.437.341-1.054.564-1.964.564-1.573 0-2.292-.337-2.657-.75-.192-.218-.331-.506-.423-.89-.091-.385-.135-.867-.135-1.472 0-1.14.243-1.847.705-2.32.477-.487 1.319-.861 2.824-1.024 1.487-.16 2.192.138 2.533.529l.008.01c.264.308.429.806.43 1.568v.031a7.203 7.203 0 01-.09 1.079c-.143.967-.406 1.754-.851 2.301zm2.504-2.497a7.174 7.174 0 01-.063-.894v-.02c.001-.77.17-1.27.438-1.578.341-.39 1.046-.69 2.533-.529 1.506.163 2.347.537 2.824 1.025.462.472.705 1.179.705 2.319 0 1.21-.174 1.926-.558 2.361-.365.414-1.084.751-2.657.751-1.21 0-1.902-.393-2.344-.938-.475-.584-.742-1.44-.878-2.497z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Copilot</div>
        Write better code with AI
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Code review&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Code review;&quot;}" href="/features/code-review">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-code-review color-fg-subtle mr-3">
    <path d="M10.3 6.74a.75.75 0 01-.04 1.06l-2.908 2.7 2.908 2.7a.75.75 0 11-1.02 1.1l-3.5-3.25a.75.75 0 010-1.1l3.5-3.25a.75.75 0 011.06.04zm3.44 1.06a.75.75 0 111.02-1.1l3.5 3.25a.75.75 0 010 1.1l-3.5 3.25a.75.75 0 11-1.02-1.1l2.908-2.7-2.908-2.7z"></path><path fill-rule="evenodd" d="M1.5 4.25c0-.966.784-1.75 1.75-1.75h17.5c.966 0 1.75.784 1.75 1.75v12.5a1.75 1.75 0 01-1.75 1.75h-9.69l-3.573 3.573A1.457 1.457 0 015 21.043V18.5H3.25a1.75 1.75 0 01-1.75-1.75V4.25zM3.25 4a.25.25 0 00-.25.25v12.5c0 .138.112.25.25.25h2.5a.75.75 0 01.75.75v3.19l3.72-3.72a.75.75 0 01.53-.22h10a.25.25 0 00.25-.25V4.25a.25.25 0 00-.25-.25H3.25z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Code review</div>
        Manage code changes
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center pb-lg-3" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Issues&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Issues;&quot;}" href="/features/issues">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-issue-opened color-fg-subtle mr-3">
    <path fill-rule="evenodd" d="M2.5 12a9.5 9.5 0 1119 0 9.5 9.5 0 01-19 0zM12 1C5.925 1 1 5.925 1 12s4.925 11 11 11 11-4.925 11-11S18.075 1 12 1zm0 13a2 2 0 100-4 2 2 0 000 4z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Issues</div>
        Plan and track work
      </div>

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Discussions&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Discussions;&quot;}" href="/features/discussions">
      <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-comment-discussion color-fg-subtle mr-3">
    <path fill-rule="evenodd" d="M1.75 1A1.75 1.75 0 000 2.75v9.5C0 13.216.784 14 1.75 14H3v1.543a1.457 1.457 0 002.487 1.03L8.061 14h6.189A1.75 1.75 0 0016 12.25v-9.5A1.75 1.75 0 0014.25 1H1.75zM1.5 2.75a.25.25 0 01.25-.25h12.5a.25.25 0 01.25.25v9.5a.25.25 0 01-.25.25h-6.5a.75.75 0 00-.53.22L4.5 15.44v-2.19a.75.75 0 00-.75-.75h-2a.25.25 0 01-.25-.25v-9.5z"></path><path d="M22.5 8.75a.25.25 0 00-.25-.25h-3.5a.75.75 0 010-1.5h3.5c.966 0 1.75.784 1.75 1.75v9.5A1.75 1.75 0 0122.25 20H21v1.543a1.457 1.457 0 01-2.487 1.03L15.939 20H10.75A1.75 1.75 0 019 18.25v-1.465a.75.75 0 011.5 0v1.465c0 .138.112.25.25.25h5.5a.75.75 0 01.53.22l2.72 2.72v-2.19a.75.75 0 01.75-.75h2a.25.25 0 00.25-.25v-9.5z"></path>
</svg>
      <div>
        <div class="color-fg-default h4">Discussions</div>
        Collaborate outside of code
      </div>

    
</a></li>

        </ul>
        <ul class="list-style-none f5 px-lg-4">
            <li class="h4 color-fg-default my-1">Explore</li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to All features&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:All features;&quot;}" href="/features">
      All features

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" target="_blank" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Documentation&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Documentation;&quot;}" href="https://docs.github.com">
      Documentation

    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-link-external HeaderMenu-external-icon color-fg-subtle">
    <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
</svg>
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" target="_blank" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to GitHub Skills&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:GitHub Skills;&quot;}" href="https://skills.github.com/">
      GitHub Skills

    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-link-external HeaderMenu-external-icon color-fg-subtle">
    <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
</svg>
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" target="_blank" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Product&quot;,&quot;action&quot;:&quot;click to go to Blog&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Blog;&quot;}" href="https://github.blog">
      Blog

    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-link-external HeaderMenu-external-icon color-fg-subtle">
    <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
</svg>
</a></li>

        </ul>
    </div>
</li>


                <li class="HeaderMenu-item position-relative flex-wrap flex-justify-between flex-items-center d-block d-lg-flex flex-lg-nowrap flex-lg-items-center js-details-container js-header-menu-item">
    <button type="button" class="HeaderMenu-link border-0 width-full width-lg-auto px-0 px-lg-2 py-3 py-lg-2 no-wrap d-flex flex-items-center flex-justify-between js-details-target" aria-expanded="false">
      Solutions
      <svg opacity="0.5" aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-chevron-down HeaderMenu-icon ml-1">
    <path fill-rule="evenodd" d="M12.78 6.22a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06 0L3.22 7.28a.75.75 0 011.06-1.06L8 9.94l3.72-3.72a.75.75 0 011.06 0z"></path>
</svg>
    </button>
    <div class="HeaderMenu-dropdown dropdown-menu rounded m-0 p-0 py-2 py-lg-4 position-relative position-lg-absolute left-0 left-lg-n3 px-lg-4">
        <ul class="list-style-none f5 border-bottom pb-3 mb-3">
            <li class="h4 color-fg-default my-1">By Plan</li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to Enterprise&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Enterprise;&quot;}" href="/enterprise">
      Enterprise

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to Teams&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Teams;&quot;}" href="/team">
      Teams

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to Compare all&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Compare all;&quot;}" href="/pricing#compare-features">
      Compare all

    
</a></li>

        </ul>
        <ul class="list-style-none f5 border-bottom pb-3 mb-3">
            <li class="h4 color-fg-default my-1">By Solution</li>

            
            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to CI/CD &amp;amp; Automation&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:CI/CD &amp;amp; Automation;&quot;}" href="/solutions/ci-cd/">
      CI/CD &amp; Automation

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" target="_blank" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to DevOps&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:DevOps;&quot;}" href="https://resources.github.com/devops/">
      DevOps

    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-link-external HeaderMenu-external-icon color-fg-subtle">
    <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
</svg>
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" target="_blank" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to DevSecOps&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:DevSecOps;&quot;}" href="https://resources.github.com/devops/fundamentals/devsecops/">
      DevSecOps

    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-link-external HeaderMenu-external-icon color-fg-subtle">
    <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
</svg>
</a></li>

        </ul>
        <ul class="list-style-none f5 ">
            <li class="h4 color-fg-default my-1">Case Studies</li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to Customer Stories&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Customer Stories;&quot;}" href="/customer-stories">
      Customer Stories

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" target="_blank" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Solutions&quot;,&quot;action&quot;:&quot;click to go to Resources&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Resources;&quot;}" href="https://resources.github.com/">
      Resources

    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-link-external HeaderMenu-external-icon color-fg-subtle">
    <path fill-rule="evenodd" d="M10.604 1h4.146a.25.25 0 01.25.25v4.146a.25.25 0 01-.427.177L13.03 4.03 9.28 7.78a.75.75 0 01-1.06-1.06l3.75-3.75-1.543-1.543A.25.25 0 0110.604 1zM3.75 2A1.75 1.75 0 002 3.75v8.5c0 .966.784 1.75 1.75 1.75h8.5A1.75 1.75 0 0014 12.25v-3.5a.75.75 0 00-1.5 0v3.5a.25.25 0 01-.25.25h-8.5a.25.25 0 01-.25-.25v-8.5a.25.25 0 01.25-.25h3.5a.75.75 0 000-1.5h-3.5z"></path>
</svg>
</a></li>

        </ul>
    </div>
</li>


                <li class="HeaderMenu-item position-relative flex-wrap flex-justify-between flex-items-center d-block d-lg-flex flex-lg-nowrap flex-lg-items-center js-details-container js-header-menu-item">
    <button type="button" class="HeaderMenu-link border-0 width-full width-lg-auto px-0 px-lg-2 py-3 py-lg-2 no-wrap d-flex flex-items-center flex-justify-between js-details-target" aria-expanded="false">
      Open Source
      <svg opacity="0.5" aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-chevron-down HeaderMenu-icon ml-1">
    <path fill-rule="evenodd" d="M12.78 6.22a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06 0L3.22 7.28a.75.75 0 011.06-1.06L8 9.94l3.72-3.72a.75.75 0 011.06 0z"></path>
</svg>
    </button>
    <div class="HeaderMenu-dropdown dropdown-menu rounded m-0 p-0 py-2 py-lg-4 position-relative position-lg-absolute left-0 left-lg-n3 px-lg-4">
        <ul class="list-style-none f5 border-bottom pb-3 mb-3">

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Open Source&quot;,&quot;action&quot;:&quot;click to go to GitHub Sponsors&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:GitHub Sponsors;&quot;}" href="/sponsors">
      
      <div>
        <div class="color-fg-default h4">GitHub Sponsors</div>
        Fund open source developers
      </div>

    
</a></li>

        </ul>
        <ul class="list-style-none f5 border-bottom pb-3 mb-3">

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary d-flex flex-items-center" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Open Source&quot;,&quot;action&quot;:&quot;click to go to The ReadME Project&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:The ReadME Project;&quot;}" href="/readme">
      
      <div>
        <div class="color-fg-default h4">The ReadME Project</div>
        GitHub community articles
      </div>

    
</a></li>

        </ul>
        <ul class="list-style-none f5 ">
            <li class="h4 color-fg-default my-1">Repositories</li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Open Source&quot;,&quot;action&quot;:&quot;click to go to Topics&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Topics;&quot;}" href="/topics">
      Topics

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Open Source&quot;,&quot;action&quot;:&quot;click to go to Trending&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Trending;&quot;}" href="/trending">
      Trending

    
</a></li>

            <li>
  <a class="HeaderMenu-dropdown-link lh-condensed d-block no-underline position-relative py-2 Link--secondary" data-analytics-event="{&quot;category&quot;:&quot;Header dropdown (logged out), Open Source&quot;,&quot;action&quot;:&quot;click to go to Collections&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Collections;&quot;}" href="/collections">
      Collections

    
</a></li>

        </ul>
    </div>
</li>


                <li class="HeaderMenu-item position-relative flex-wrap flex-justify-between flex-items-center d-block d-lg-flex flex-lg-nowrap flex-lg-items-center js-details-container js-header-menu-item">
    <a class="HeaderMenu-link no-underline px-0 px-lg-2 py-3 py-lg-2 d-block d-lg-inline-block" data-analytics-event="{&quot;category&quot;:&quot;Header menu top item (logged out)&quot;,&quot;action&quot;:&quot;click to go to Pricing&quot;,&quot;label&quot;:&quot;ref_page:/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47;ref_cta:Pricing;&quot;}" href="/pricing">Pricing</a>
</li>

            </ul>
          </nav>

        <div class="d-lg-flex flex-items-center px-3 px-lg-0 mb-3 mb-lg-0 text-center text-lg-left">
            <div class="d-lg-flex min-width-0 mb-2 mb-lg-0">
              



<div class="header-search flex-auto position-relative js-site-search flex-self-stretch flex-md-self-auto mb-3 mb-md-0 mr-0 mr-md-3 scoped-search site-scoped-search js-jump-to"
>
  <div class="position-relative">
    <!-- '"` --><!-- </textarea></xmp> --></option></form><form class="js-site-search-form" role="search" aria-label="Site" data-scope-type="Repository" data-scope-id="33764615" data-scoped-search-url="/Yara-Rules/rules/search" data-owner-scoped-search-url="/orgs/Yara-Rules/search" data-unscoped-search-url="/search" data-turbo="false" action="/Yara-Rules/rules/search" accept-charset="UTF-8" method="get">
      <label class="form-control header-search-wrapper input-sm p-0 js-chromeless-input-container header-search-wrapper-jump-to position-relative d-flex flex-justify-between flex-items-center">
        <input type="text"
          class="form-control js-site-search-focus header-search-input jump-to-field js-jump-to-field js-site-search-field is-clearable"
          data-hotkey=s,/
          name="q"
          data-test-selector="nav-search-input"
          placeholder="Search"
          data-unscoped-placeholder="Search GitHub"
          data-scoped-placeholder="Search"
          autocapitalize="off"
          role="combobox"
          aria-haspopup="listbox"
          aria-expanded="false"
          aria-autocomplete="list"
          aria-controls="jump-to-results"
          aria-label="Search"
          data-jump-to-suggestions-path="/_graphql/GetSuggestedNavigationDestinations"
          spellcheck="false"
          autocomplete="off"
        >
        <input type="hidden" data-csrf="true" class="js-data-jump-to-suggestions-path-csrf" value="V+ia7VTNlFJE2K8qsK39rsyUe5/BzoIWP6RDK3mMNtwoAcL4geD+Su8x2z224qVsmbIocdMOucLdCZKBlY1F+Q==" />
        <input type="hidden" class="js-site-search-type-field" name="type" >
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" aria-hidden="true" class="mr-1 header-search-key-slash"><path fill="none" stroke="#979A9C" opacity=".4" d="M3.5.5h12c1.7 0 3 1.3 3 3v13c0 1.7-1.3 3-3 3h-12c-1.7 0-3-1.3-3-3v-13c0-1.7 1.3-3 3-3z"></path><path fill="#979A9C" d="M11.8 6L8 15.1h-.9L10.8 6h1z"></path></svg>


          <div class="Box position-absolute overflow-hidden d-none jump-to-suggestions js-jump-to-suggestions-container">
            
<ul class="d-none js-jump-to-suggestions-template-container">
  

<li class="d-flex flex-justify-start flex-items-center p-0 f5 navigation-item js-navigation-item js-jump-to-suggestion" role="option">
  <a tabindex="-1" class="no-underline d-flex flex-auto flex-items-center jump-to-suggestions-path js-jump-to-suggestion-path js-navigation-open p-2" href="" data-item-type="suggestion">
    <div class="jump-to-octicon js-jump-to-octicon flex-shrink-0 mr-2 text-center d-none">
      <svg title="Repository" aria-label="Repository" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo js-jump-to-octicon-repo d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M2 2.5A2.5 2.5 0 014.5 0h8.75a.75.75 0 01.75.75v12.5a.75.75 0 01-.75.75h-2.5a.75.75 0 110-1.5h1.75v-2h-8a1 1 0 00-.714 1.7.75.75 0 01-1.072 1.05A2.495 2.495 0 012 11.5v-9zm10.5-1V9h-8c-.356 0-.694.074-1 .208V2.5a1 1 0 011-1h8zM5 12.25v3.25a.25.25 0 00.4.2l1.45-1.087a.25.25 0 01.3 0L8.6 15.7a.25.25 0 00.4-.2v-3.25a.25.25 0 00-.25-.25h-3.5a.25.25 0 00-.25.25z"></path>
</svg>
      <svg title="Project" aria-label="Project" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-project js-jump-to-octicon-project d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M1.75 0A1.75 1.75 0 000 1.75v12.5C0 15.216.784 16 1.75 16h12.5A1.75 1.75 0 0016 14.25V1.75A1.75 1.75 0 0014.25 0H1.75zM1.5 1.75a.25.25 0 01.25-.25h12.5a.25.25 0 01.25.25v12.5a.25.25 0 01-.25.25H1.75a.25.25 0 01-.25-.25V1.75zM11.75 3a.75.75 0 00-.75.75v7.5a.75.75 0 001.5 0v-7.5a.75.75 0 00-.75-.75zm-8.25.75a.75.75 0 011.5 0v5.5a.75.75 0 01-1.5 0v-5.5zM8 3a.75.75 0 00-.75.75v3.5a.75.75 0 001.5 0v-3.5A.75.75 0 008 3z"></path>
</svg>
      <svg title="Search" aria-label="Search" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-search js-jump-to-octicon-search d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M11.5 7a4.499 4.499 0 11-8.998 0A4.499 4.499 0 0111.5 7zm-.82 4.74a6 6 0 111.06-1.06l3.04 3.04a.75.75 0 11-1.06 1.06l-3.04-3.04z"></path>
</svg>
    </div>

    <img class="avatar mr-2 flex-shrink-0 js-jump-to-suggestion-avatar d-none" alt="" aria-label="Team" src="" width="28" height="28">

    <div class="jump-to-suggestion-name js-jump-to-suggestion-name flex-auto overflow-hidden text-left no-wrap css-truncate css-truncate-target">
    </div>

    <div class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none js-jump-to-badge-search">
      <span class="js-jump-to-badge-search-text-default d-none" aria-label="in this repository">
        In this repository
      </span>
      <span class="js-jump-to-badge-search-text-global d-none" aria-label="in all of GitHub">
        All GitHub
      </span>
      <span aria-hidden="true" class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>

    <div aria-hidden="true" class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none d-on-nav-focus js-jump-to-badge-jump">
      Jump to
      <span class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>
  </a>
</li>

</ul>

<ul class="d-none js-jump-to-no-results-template-container">
  <li class="d-flex flex-justify-center flex-items-center f5 d-none js-jump-to-suggestion p-2">
    <span class="color-fg-muted">No suggested jump to results</span>
  </li>
</ul>

<ul id="jump-to-results" role="listbox" class="p-0 m-0 js-navigation-container jump-to-suggestions-results-container js-jump-to-suggestions-results-container">
  

<li class="d-flex flex-justify-start flex-items-center p-0 f5 navigation-item js-navigation-item js-jump-to-scoped-search d-none" role="option">
  <a tabindex="-1" class="no-underline d-flex flex-auto flex-items-center jump-to-suggestions-path js-jump-to-suggestion-path js-navigation-open p-2" href="" data-item-type="scoped_search">
    <div class="jump-to-octicon js-jump-to-octicon flex-shrink-0 mr-2 text-center d-none">
      <svg title="Repository" aria-label="Repository" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo js-jump-to-octicon-repo d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M2 2.5A2.5 2.5 0 014.5 0h8.75a.75.75 0 01.75.75v12.5a.75.75 0 01-.75.75h-2.5a.75.75 0 110-1.5h1.75v-2h-8a1 1 0 00-.714 1.7.75.75 0 01-1.072 1.05A2.495 2.495 0 012 11.5v-9zm10.5-1V9h-8c-.356 0-.694.074-1 .208V2.5a1 1 0 011-1h8zM5 12.25v3.25a.25.25 0 00.4.2l1.45-1.087a.25.25 0 01.3 0L8.6 15.7a.25.25 0 00.4-.2v-3.25a.25.25 0 00-.25-.25h-3.5a.25.25 0 00-.25.25z"></path>
</svg>
      <svg title="Project" aria-label="Project" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-project js-jump-to-octicon-project d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M1.75 0A1.75 1.75 0 000 1.75v12.5C0 15.216.784 16 1.75 16h12.5A1.75 1.75 0 0016 14.25V1.75A1.75 1.75 0 0014.25 0H1.75zM1.5 1.75a.25.25 0 01.25-.25h12.5a.25.25 0 01.25.25v12.5a.25.25 0 01-.25.25H1.75a.25.25 0 01-.25-.25V1.75zM11.75 3a.75.75 0 00-.75.75v7.5a.75.75 0 001.5 0v-7.5a.75.75 0 00-.75-.75zm-8.25.75a.75.75 0 011.5 0v5.5a.75.75 0 01-1.5 0v-5.5zM8 3a.75.75 0 00-.75.75v3.5a.75.75 0 001.5 0v-3.5A.75.75 0 008 3z"></path>
</svg>
      <svg title="Search" aria-label="Search" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-search js-jump-to-octicon-search d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M11.5 7a4.499 4.499 0 11-8.998 0A4.499 4.499 0 0111.5 7zm-.82 4.74a6 6 0 111.06-1.06l3.04 3.04a.75.75 0 11-1.06 1.06l-3.04-3.04z"></path>
</svg>
    </div>

    <img class="avatar mr-2 flex-shrink-0 js-jump-to-suggestion-avatar d-none" alt="" aria-label="Team" src="" width="28" height="28">

    <div class="jump-to-suggestion-name js-jump-to-suggestion-name flex-auto overflow-hidden text-left no-wrap css-truncate css-truncate-target">
    </div>

    <div class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none js-jump-to-badge-search">
      <span class="js-jump-to-badge-search-text-default d-none" aria-label="in this repository">
        In this repository
      </span>
      <span class="js-jump-to-badge-search-text-global d-none" aria-label="in all of GitHub">
        All GitHub
      </span>
      <span aria-hidden="true" class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>

    <div aria-hidden="true" class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none d-on-nav-focus js-jump-to-badge-jump">
      Jump to
      <span class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>
  </a>
</li>

  

<li class="d-flex flex-justify-start flex-items-center p-0 f5 navigation-item js-navigation-item js-jump-to-owner-scoped-search d-none" role="option">
  <a tabindex="-1" class="no-underline d-flex flex-auto flex-items-center jump-to-suggestions-path js-jump-to-suggestion-path js-navigation-open p-2" href="" data-item-type="owner_scoped_search">
    <div class="jump-to-octicon js-jump-to-octicon flex-shrink-0 mr-2 text-center d-none">
      <svg title="Repository" aria-label="Repository" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo js-jump-to-octicon-repo d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M2 2.5A2.5 2.5 0 014.5 0h8.75a.75.75 0 01.75.75v12.5a.75.75 0 01-.75.75h-2.5a.75.75 0 110-1.5h1.75v-2h-8a1 1 0 00-.714 1.7.75.75 0 01-1.072 1.05A2.495 2.495 0 012 11.5v-9zm10.5-1V9h-8c-.356 0-.694.074-1 .208V2.5a1 1 0 011-1h8zM5 12.25v3.25a.25.25 0 00.4.2l1.45-1.087a.25.25 0 01.3 0L8.6 15.7a.25.25 0 00.4-.2v-3.25a.25.25 0 00-.25-.25h-3.5a.25.25 0 00-.25.25z"></path>
</svg>
      <svg title="Project" aria-label="Project" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-project js-jump-to-octicon-project d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M1.75 0A1.75 1.75 0 000 1.75v12.5C0 15.216.784 16 1.75 16h12.5A1.75 1.75 0 0016 14.25V1.75A1.75 1.75 0 0014.25 0H1.75zM1.5 1.75a.25.25 0 01.25-.25h12.5a.25.25 0 01.25.25v12.5a.25.25 0 01-.25.25H1.75a.25.25 0 01-.25-.25V1.75zM11.75 3a.75.75 0 00-.75.75v7.5a.75.75 0 001.5 0v-7.5a.75.75 0 00-.75-.75zm-8.25.75a.75.75 0 011.5 0v5.5a.75.75 0 01-1.5 0v-5.5zM8 3a.75.75 0 00-.75.75v3.5a.75.75 0 001.5 0v-3.5A.75.75 0 008 3z"></path>
</svg>
      <svg title="Search" aria-label="Search" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-search js-jump-to-octicon-search d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M11.5 7a4.499 4.499 0 11-8.998 0A4.499 4.499 0 0111.5 7zm-.82 4.74a6 6 0 111.06-1.06l3.04 3.04a.75.75 0 11-1.06 1.06l-3.04-3.04z"></path>
</svg>
    </div>

    <img class="avatar mr-2 flex-shrink-0 js-jump-to-suggestion-avatar d-none" alt="" aria-label="Team" src="" width="28" height="28">

    <div class="jump-to-suggestion-name js-jump-to-suggestion-name flex-auto overflow-hidden text-left no-wrap css-truncate css-truncate-target">
    </div>

    <div class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none js-jump-to-badge-search">
      <span class="js-jump-to-badge-search-text-default d-none" aria-label="in this organization">
        In this organization
      </span>
      <span class="js-jump-to-badge-search-text-global d-none" aria-label="in all of GitHub">
        All GitHub
      </span>
      <span aria-hidden="true" class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>

    <div aria-hidden="true" class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none d-on-nav-focus js-jump-to-badge-jump">
      Jump to
      <span class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>
  </a>
</li>

  

<li class="d-flex flex-justify-start flex-items-center p-0 f5 navigation-item js-navigation-item js-jump-to-global-search d-none" role="option">
  <a tabindex="-1" class="no-underline d-flex flex-auto flex-items-center jump-to-suggestions-path js-jump-to-suggestion-path js-navigation-open p-2" href="" data-item-type="global_search">
    <div class="jump-to-octicon js-jump-to-octicon flex-shrink-0 mr-2 text-center d-none">
      <svg title="Repository" aria-label="Repository" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo js-jump-to-octicon-repo d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M2 2.5A2.5 2.5 0 014.5 0h8.75a.75.75 0 01.75.75v12.5a.75.75 0 01-.75.75h-2.5a.75.75 0 110-1.5h1.75v-2h-8a1 1 0 00-.714 1.7.75.75 0 01-1.072 1.05A2.495 2.495 0 012 11.5v-9zm10.5-1V9h-8c-.356 0-.694.074-1 .208V2.5a1 1 0 011-1h8zM5 12.25v3.25a.25.25 0 00.4.2l1.45-1.087a.25.25 0 01.3 0L8.6 15.7a.25.25 0 00.4-.2v-3.25a.25.25 0 00-.25-.25h-3.5a.25.25 0 00-.25.25z"></path>
</svg>
      <svg title="Project" aria-label="Project" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-project js-jump-to-octicon-project d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M1.75 0A1.75 1.75 0 000 1.75v12.5C0 15.216.784 16 1.75 16h12.5A1.75 1.75 0 0016 14.25V1.75A1.75 1.75 0 0014.25 0H1.75zM1.5 1.75a.25.25 0 01.25-.25h12.5a.25.25 0 01.25.25v12.5a.25.25 0 01-.25.25H1.75a.25.25 0 01-.25-.25V1.75zM11.75 3a.75.75 0 00-.75.75v7.5a.75.75 0 001.5 0v-7.5a.75.75 0 00-.75-.75zm-8.25.75a.75.75 0 011.5 0v5.5a.75.75 0 01-1.5 0v-5.5zM8 3a.75.75 0 00-.75.75v3.5a.75.75 0 001.5 0v-3.5A.75.75 0 008 3z"></path>
</svg>
      <svg title="Search" aria-label="Search" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-search js-jump-to-octicon-search d-none flex-shrink-0">
    <path fill-rule="evenodd" d="M11.5 7a4.499 4.499 0 11-8.998 0A4.499 4.499 0 0111.5 7zm-.82 4.74a6 6 0 111.06-1.06l3.04 3.04a.75.75 0 11-1.06 1.06l-3.04-3.04z"></path>
</svg>
    </div>

    <img class="avatar mr-2 flex-shrink-0 js-jump-to-suggestion-avatar d-none" alt="" aria-label="Team" src="" width="28" height="28">

    <div class="jump-to-suggestion-name js-jump-to-suggestion-name flex-auto overflow-hidden text-left no-wrap css-truncate css-truncate-target">
    </div>

    <div class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none js-jump-to-badge-search">
      <span class="js-jump-to-badge-search-text-default d-none" aria-label="in this repository">
        In this repository
      </span>
      <span class="js-jump-to-badge-search-text-global d-none" aria-label="in all of GitHub">
        All GitHub
      </span>
      <span aria-hidden="true" class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>

    <div aria-hidden="true" class="border rounded-2 flex-shrink-0 color-bg-subtle px-1 color-fg-muted ml-1 f6 d-none d-on-nav-focus js-jump-to-badge-jump">
      Jump to
      <span class="d-inline-block ml-1 v-align-middle">↵</span>
    </div>
  </a>
</li>


</ul>

          </div>
      </label>
</form>  </div>
</div>

            </div>

          <div class="position-relative mr-lg-3 d-lg-inline-block">
            <a href="/login?return_to=https%3A%2F%2Fgithub.com%2FYara-Rules%2Frules%2Fpull%2F428%2Fcommits%2Fc0573b2ea53cf72470f4eed0bc2000e73b55ee47"
              class="HeaderMenu-link HeaderMenu-link--sign-in flex-shrink-0 no-underline d-block d-lg-inline-block border border-lg-0 rounded rounded-lg-0 p-2 p-lg-0"
              data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;site header menu&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="0fcc02c6c5ff439624eb908d832ae455bb6ff49d9b286a8e12a416131a396737"
              data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">
              Sign in
            </a>
          </div>

            <a href="/signup?ref_cta=Sign+up&amp;ref_loc=header+logged+out&amp;ref_page=%2F%3Cuser-name%3E%2F%3Crepo-name%3E%2Fpull_requests%2Fshow%2Fcommits&amp;source=header-repo&amp;source_repo=Yara-Rules%2Frules"
              class="HeaderMenu-link HeaderMenu-link--sign-up flex-shrink-0 d-none d-lg-inline-block no-underline border color-border-default rounded px-2 py-1"
              data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;site header menu&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="0fcc02c6c5ff439624eb908d832ae455bb6ff49d9b286a8e12a416131a396737"
              data-analytics-event="{&quot;category&quot;:&quot;Sign up&quot;,&quot;action&quot;:&quot;click to sign up for account&quot;,&quot;label&quot;:&quot;ref_page:/&lt;user-name&gt;/&lt;repo-name&gt;/pull_requests/show/commits;ref_cta:Sign up;ref_loc:header logged out&quot;}"
            >
              Sign up
            </a>
        </div>
      </div>
    </div>
  </div>
</header>

    </div>

  <div id="start-of-content" class="show-on-focus"></div>







    <div id="js-flash-container" data-turbo-replace>




  <template class="js-flash-template">
    
<div class="flash flash-full   {{ className }}">
  <div class="px-2" >
    <button autofocus class="flash-close js-flash-close" type="button" aria-label="Dismiss this message">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path>
</svg>
    </button>
    <div aria-atomic="true" role="alert" class="js-flash-alert">
      
      <div>{{ message }}</div>

    </div>
  </div>
</div>
  </template>
</div>


    
  <include-fragment class="js-notification-shelf-include-fragment" data-base-src="https://github.com/notifications/beta/shelf"></include-fragment>






  <div
    class="application-main "
    data-commit-hovercards-enabled
    data-discussion-hovercards-enabled
    data-issue-and-pr-hovercards-enabled
  >
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode" class="">
    <main id="js-repo-pjax-container" >
      
  




      
    







    <div id="repository-container-header"  class="pt-3 hide-full-screen" style="background-color: var(--color-page-header-bg);" data-turbo-replace>

      <div class="d-flex flex-wrap flex-justify-end mb-3  px-3 px-md-4 px-lg-5" style="gap: 1rem;">

        <div class="flex-auto min-width-0 width-fit mr-3">
            
  <div class=" d-flex flex-wrap flex-items-center wb-break-word f3 text-normal">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo color-fg-muted mr-2">
    <path fill-rule="evenodd" d="M2 2.5A2.5 2.5 0 014.5 0h8.75a.75.75 0 01.75.75v12.5a.75.75 0 01-.75.75h-2.5a.75.75 0 110-1.5h1.75v-2h-8a1 1 0 00-.714 1.7.75.75 0 01-1.072 1.05A2.495 2.495 0 012 11.5v-9zm10.5-1V9h-8c-.356 0-.694.074-1 .208V2.5a1 1 0 011-1h8zM5 12.25v3.25a.25.25 0 00.4.2l1.45-1.087a.25.25 0 01.3 0L8.6 15.7a.25.25 0 00.4-.2v-3.25a.25.25 0 00-.25-.25h-3.5a.25.25 0 00-.25.25z"></path>
</svg>
    
    <span class="author flex-self-stretch" itemprop="author">
      <a class="url fn" rel="author" data-hovercard-type="organization" data-hovercard-url="/orgs/Yara-Rules/hovercard" data-octo-click="hovercard-link-click" data-octo-dimensions="link_type:self" href="/Yara-Rules">Yara-Rules</a>
    </span>
    <span class="mx-1 flex-self-stretch color-fg-muted">/</span>
    <strong itemprop="name" class="mr-2 flex-self-stretch">
      <a data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" href="/Yara-Rules/rules">rules</a>
    </strong>

    <span></span><span class="Label Label--secondary v-align-middle mr-1">Public</span>
  </div>


        </div>

          <ul class="pagehead-actions flex-shrink-0 d-none d-md-inline" style="padding: 2px 0;">
      <li>
        <include-fragment src="/Yara-Rules/rules/sponsor_button"></include-fragment>
      </li>

      

  <li>
          <a href="/login?return_to=%2FYara-Rules%2Frules" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;notification subscription menu watch&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="e86d6568cc9f32257105636bcaa3ffc5133c9be98d4e102b7c1b74dd7f43e69f" aria-label="You must be signed in to change notification settings" data-view-component="true" class="tooltipped tooltipped-s btn-sm btn">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-bell mr-2">
    <path d="M8 16a2 2 0 001.985-1.75c.017-.137-.097-.25-.235-.25h-3.5c-.138 0-.252.113-.235.25A2 2 0 008 16z"></path><path fill-rule="evenodd" d="M8 1.5A3.5 3.5 0 004.5 5v2.947c0 .346-.102.683-.294.97l-1.703 2.556a.018.018 0 00-.003.01l.001.006c0 .002.002.004.004.006a.017.017 0 00.006.004l.007.001h10.964l.007-.001a.016.016 0 00.006-.004.016.016 0 00.004-.006l.001-.007a.017.017 0 00-.003-.01l-1.703-2.554a1.75 1.75 0 01-.294-.97V5A3.5 3.5 0 008 1.5zM3 5a5 5 0 0110 0v2.947c0 .05.015.098.042.139l1.703 2.555A1.518 1.518 0 0113.482 13H2.518a1.518 1.518 0 01-1.263-2.36l1.703-2.554A.25.25 0 003 7.947V5z"></path>
</svg>Notifications
</a>
  </li>

  <li>
          <a icon="repo-forked" href="/login?return_to=%2FYara-Rules%2Frules" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;repo details fork button&quot;,&quot;repository_id&quot;:33764615,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="76c7a352778250c95020afb38a8a7da569af91826e3faf1478087defa3944af7" aria-label="You must be signed in to fork a repository" data-view-component="true" class="tooltipped tooltipped-s btn-sm btn">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo-forked mr-2">
    <path fill-rule="evenodd" d="M5 3.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm0 2.122a2.25 2.25 0 10-1.5 0v.878A2.25 2.25 0 005.75 8.5h1.5v2.128a2.251 2.251 0 101.5 0V8.5h1.5a2.25 2.25 0 002.25-2.25v-.878a2.25 2.25 0 10-1.5 0v.878a.75.75 0 01-.75.75h-4.5A.75.75 0 015 6.25v-.878zm3.75 7.378a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm3-8.75a.75.75 0 100-1.5.75.75 0 000 1.5z"></path>
</svg>Fork
    <span id="repo-network-counter" data-pjax-replace="true" data-turbo-replace="true" title="880" data-view-component="true" class="Counter">880</span>
</a>
  </li>

  <li>
        <div data-view-component="true" class="BtnGroup d-flex">
        <a href="/login?return_to=%2FYara-Rules%2Frules" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;star button&quot;,&quot;repository_id&quot;:33764615,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="3c154b8f2a6ffdcf304901a5f818500981493c1e7228ac75928e8a6663dbea89" aria-label="You must be signed in to star a repository" data-view-component="true" class="tooltipped tooltipped-s btn-sm btn BtnGroup-item">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-star v-align-text-bottom d-inline-block mr-2">
    <path fill-rule="evenodd" d="M8 .25a.75.75 0 01.673.418l1.882 3.815 4.21.612a.75.75 0 01.416 1.279l-3.046 2.97.719 4.192a.75.75 0 01-1.088.791L8 12.347l-3.766 1.98a.75.75 0 01-1.088-.79l.72-4.194L.818 6.374a.75.75 0 01.416-1.28l4.21-.611L7.327.668A.75.75 0 018 .25zm0 2.445L6.615 5.5a.75.75 0 01-.564.41l-3.097.45 2.24 2.184a.75.75 0 01.216.664l-.528 3.084 2.769-1.456a.75.75 0 01.698 0l2.77 1.456-.53-3.084a.75.75 0 01.216-.664l2.24-2.183-3.096-.45a.75.75 0 01-.564-.41L8 2.694v.001z"></path>
</svg><span data-view-component="true" class="d-inline">
          Star
</span>          <span id="repo-stars-counter-star" aria-label="3330 users starred this repository" data-singular-suffix="user starred this repository" data-plural-suffix="users starred this repository" data-turbo-replace="true" title="3,330" data-view-component="true" class="Counter js-social-count">3.3k</span>
</a>        <button disabled="disabled" aria-label="You must be signed in to add this repository to a list" type="button" data-view-component="true" class="btn-sm btn BtnGroup-item px-2">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-triangle-down">
    <path d="M4.427 7.427l3.396 3.396a.25.25 0 00.354 0l3.396-3.396A.25.25 0 0011.396 7H4.604a.25.25 0 00-.177.427z"></path>
</svg>
</button></div>
  </li>

    

</ul>

      </div>

        <div id="responsive-meta-container" data-turbo-replace>
</div>


          <nav data-pjax="#js-repo-pjax-container" aria-label="Repository" data-view-component="true" class="js-repo-nav js-sidenav-container-pjax js-responsive-underlinenav overflow-hidden UnderlineNav px-3 px-md-4 px-lg-5">

  <ul data-view-component="true" class="UnderlineNav-body list-style-none">
      <li data-view-component="true" class="d-inline-flex">
  <a id="code-tab" href="/Yara-Rules/rules" data-tab-item="i0code-tab" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches repo_packages repo_deployments /Yara-Rules/rules" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g c" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Code&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-code UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M4.72 3.22a.75.75 0 011.06 1.06L2.06 8l3.72 3.72a.75.75 0 11-1.06 1.06L.47 8.53a.75.75 0 010-1.06l4.25-4.25zm6.56 0a.75.75 0 10-1.06 1.06L13.94 8l-3.72 3.72a.75.75 0 101.06 1.06l4.25-4.25a.75.75 0 000-1.06l-4.25-4.25z"></path>
</svg>
        <span data-content="Code">Code</span>
          <span id="code-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="Not available" data-view-component="true" class="Counter"></span>


    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="issues-tab" href="/Yara-Rules/rules/issues" data-tab-item="i1issues-tab" data-selected-links="repo_issues repo_labels repo_milestones /Yara-Rules/rules/issues" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g i" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Issues&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-issue-opened UnderlineNav-octicon d-none d-sm-inline">
    <path d="M8 9.5a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"></path><path fill-rule="evenodd" d="M8 0a8 8 0 100 16A8 8 0 008 0zM1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0z"></path>
</svg>
        <span data-content="Issues">Issues</span>
          <span id="issues-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="1" data-view-component="true" class="Counter">1</span>


    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="pull-requests-tab" href="/Yara-Rules/rules/pulls" data-tab-item="i2pull-requests-tab" data-selected-links="repo_pulls checks /Yara-Rules/rules/pulls" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g p" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Pull requests&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" aria-current="page" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item selected">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-git-pull-request UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M7.177 3.073L9.573.677A.25.25 0 0110 .854v4.792a.25.25 0 01-.427.177L7.177 3.427a.25.25 0 010-.354zM3.75 2.5a.75.75 0 100 1.5.75.75 0 000-1.5zm-2.25.75a2.25 2.25 0 113 2.122v5.256a2.251 2.251 0 11-1.5 0V5.372A2.25 2.25 0 011.5 3.25zM11 2.5h-1V4h1a1 1 0 011 1v5.628a2.251 2.251 0 101.5 0V5A2.5 2.5 0 0011 2.5zm1 10.25a.75.75 0 111.5 0 .75.75 0 01-1.5 0zM3.75 12a.75.75 0 100 1.5.75.75 0 000-1.5z"></path>
</svg>
        <span data-content="Pull requests">Pull requests</span>
          <span id="pull-requests-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="7" data-view-component="true" class="Counter">7</span>


    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="actions-tab" href="/Yara-Rules/rules/actions" data-tab-item="i3actions-tab" data-selected-links="repo_actions /Yara-Rules/rules/actions" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g a" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Actions&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-play UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M1.5 8a6.5 6.5 0 1113 0 6.5 6.5 0 01-13 0zM8 0a8 8 0 100 16A8 8 0 008 0zM6.379 5.227A.25.25 0 006 5.442v5.117a.25.25 0 00.379.214l4.264-2.559a.25.25 0 000-.428L6.379 5.227z"></path>
</svg>
        <span data-content="Actions">Actions</span>
          <span id="actions-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="Not available" data-view-component="true" class="Counter"></span>


    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="projects-tab" href="/Yara-Rules/rules/projects" data-tab-item="i4projects-tab" data-selected-links="repo_projects new_repo_project repo_project /Yara-Rules/rules/projects" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g b" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Projects&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-table UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M0 1.75C0 .784.784 0 1.75 0h12.5C15.216 0 16 .784 16 1.75v12.5A1.75 1.75 0 0114.25 16H1.75A1.75 1.75 0 010 14.25V1.75zM1.5 6.5v7.75c0 .138.112.25.25.25H5v-8H1.5zM5 5H1.5V1.75a.25.25 0 01.25-.25H5V5zm1.5 1.5v8h7.75a.25.25 0 00.25-.25V6.5h-8zm8-1.5h-8V1.5h7.75a.25.25 0 01.25.25V5z"></path>
</svg>
        <span data-content="Projects">Projects</span>
          <span id="projects-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="0" hidden="hidden" data-view-component="true" class="Counter">0</span>


    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="wiki-tab" href="/Yara-Rules/rules/wiki" data-tab-item="i5wiki-tab" data-selected-links="repo_wiki /Yara-Rules/rules/wiki" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g w" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Wiki&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-book UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M0 1.75A.75.75 0 01.75 1h4.253c1.227 0 2.317.59 3 1.501A3.744 3.744 0 0111.006 1h4.245a.75.75 0 01.75.75v10.5a.75.75 0 01-.75.75h-4.507a2.25 2.25 0 00-1.591.659l-.622.621a.75.75 0 01-1.06 0l-.622-.621A2.25 2.25 0 005.258 13H.75a.75.75 0 01-.75-.75V1.75zm8.755 3a2.25 2.25 0 012.25-2.25H14.5v9h-3.757c-.71 0-1.4.201-1.992.572l.004-7.322zm-1.504 7.324l.004-5.073-.002-2.253A2.25 2.25 0 005.003 2.5H1.5v9h3.757a3.75 3.75 0 011.994.574z"></path>
</svg>
        <span data-content="Wiki">Wiki</span>
          <span id="wiki-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="Not available" data-view-component="true" class="Counter"></span>


    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="security-tab" href="/Yara-Rules/rules/security" data-tab-item="i6security-tab" data-selected-links="security overview alerts policy token_scanning code_scanning /Yara-Rules/rules/security" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-hotkey="g s" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Security&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-shield UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M7.467.133a1.75 1.75 0 011.066 0l5.25 1.68A1.75 1.75 0 0115 3.48V7c0 1.566-.32 3.182-1.303 4.682-.983 1.498-2.585 2.813-5.032 3.855a1.7 1.7 0 01-1.33 0c-2.447-1.042-4.049-2.357-5.032-3.855C1.32 10.182 1 8.566 1 7V3.48a1.75 1.75 0 011.217-1.667l5.25-1.68zm.61 1.429a.25.25 0 00-.153 0l-5.25 1.68a.25.25 0 00-.174.238V7c0 1.358.275 2.666 1.057 3.86.784 1.194 2.121 2.34 4.366 3.297a.2.2 0 00.154 0c2.245-.956 3.582-2.104 4.366-3.298C13.225 9.666 13.5 8.36 13.5 7V3.48a.25.25 0 00-.174-.237l-5.25-1.68zM9 10.5a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.75a.75.75 0 10-1.5 0v3a.75.75 0 001.5 0v-3z"></path>
</svg>
        <span data-content="Security">Security</span>
          <include-fragment src="/Yara-Rules/rules/security/overall-count" accept="text/fragment+html"></include-fragment>

    
</a></li>
      <li data-view-component="true" class="d-inline-flex">
  <a id="insights-tab" href="/Yara-Rules/rules/pulse" data-tab-item="i7insights-tab" data-selected-links="repo_graphs repo_contributors dependency_graph dependabot_updates pulse people community /Yara-Rules/rules/pulse" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame" data-analytics-event="{&quot;category&quot;:&quot;Underline navbar&quot;,&quot;action&quot;:&quot;Click tab&quot;,&quot;label&quot;:&quot;Insights&quot;,&quot;target&quot;:&quot;UNDERLINE_NAV.TAB&quot;}" data-view-component="true" class="UnderlineNav-item no-wrap js-responsive-underlinenav-item js-selected-navigation-item">
    
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-graph UnderlineNav-octicon d-none d-sm-inline">
    <path fill-rule="evenodd" d="M1.5 1.75a.75.75 0 00-1.5 0v12.5c0 .414.336.75.75.75h14.5a.75.75 0 000-1.5H1.5V1.75zm14.28 2.53a.75.75 0 00-1.06-1.06L10 7.94 7.53 5.47a.75.75 0 00-1.06 0L3.22 8.72a.75.75 0 001.06 1.06L7 7.06l2.47 2.47a.75.75 0 001.06 0l5.25-5.25z"></path>
</svg>
        <span data-content="Insights">Insights</span>
          <span id="insights-repo-tab-count" data-pjax-replace="" data-turbo-replace="" title="Not available" data-view-component="true" class="Counter"></span>


    
</a></li>
</ul>
    <div style="visibility:hidden;" data-view-component="true" class="UnderlineNav-actions js-responsive-underlinenav-overflow position-absolute pr-3 pr-md-4 pr-lg-5 right-0">      <details data-view-component="true" class="details-overlay details-reset position-relative">
  <summary role="button" data-view-component="true">          <div class="UnderlineNav-item mr-0 border-0">
            <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-kebab-horizontal">
    <path d="M8 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zM1.5 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm13 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"></path>
</svg>
            <span class="sr-only">More</span>
          </div>
</summary>
  <details-menu role="menu" data-view-component="true" class="dropdown-menu dropdown-menu-sw">          <ul>
              <li data-menu-item="i0code-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches repo_packages repo_deployments /Yara-Rules/rules" href="/Yara-Rules/rules">
                  Code
</a>              </li>
              <li data-menu-item="i1issues-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="repo_issues repo_labels repo_milestones /Yara-Rules/rules/issues" href="/Yara-Rules/rules/issues">
                  Issues
</a>              </li>
              <li data-menu-item="i2pull-requests-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item selected dropdown-item" aria-current="page" data-selected-links="repo_pulls checks /Yara-Rules/rules/pulls" href="/Yara-Rules/rules/pulls">
                  Pull requests
</a>              </li>
              <li data-menu-item="i3actions-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="repo_actions /Yara-Rules/rules/actions" href="/Yara-Rules/rules/actions">
                  Actions
</a>              </li>
              <li data-menu-item="i4projects-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="repo_projects new_repo_project repo_project /Yara-Rules/rules/projects" href="/Yara-Rules/rules/projects">
                  Projects
</a>              </li>
              <li data-menu-item="i5wiki-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="repo_wiki /Yara-Rules/rules/wiki" href="/Yara-Rules/rules/wiki">
                  Wiki
</a>              </li>
              <li data-menu-item="i6security-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="security overview alerts policy token_scanning code_scanning /Yara-Rules/rules/security" href="/Yara-Rules/rules/security">
                  Security
</a>              </li>
              <li data-menu-item="i7insights-tab" hidden>
                <a role="menuitem" class="js-selected-navigation-item dropdown-item" data-selected-links="repo_graphs repo_contributors dependency_graph dependabot_updates pulse people community /Yara-Rules/rules/pulse" href="/Yara-Rules/rules/pulse">
                  Insights
</a>              </li>
          </ul>
</details-menu>
</details></div>
</nav>



  </div>



  <turbo-frame id="repo-content-turbo-frame" target="_top" data-turbo-action="advance" class="">
      <div id="repo-content-pjax-container" class="repository-content " >
    
    


    
      
    <div class="clearfix mt-4 px-3 px-md-4 px-lg-5">
        <div
      class="position-relative js-review-state-classes js-suggested-changes-subset-files"
      data-pjax
      data-discussion-hovercards-enabled
      data-issue-and-pr-hovercards-enabled
    >
      <div id="js-report-pull-request-refresh" data-hydro-view="{&quot;event_type&quot;:&quot;pull-request-refresh&quot;,&quot;payload&quot;:{&quot;pull_request_id&quot;:934788780,&quot;tab_context&quot;:&quot;files_changed&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-view-hmac="fc5ed7468b1cdab510623686cccdffd6bdabc894d7374d2222368bd42c9b7cf4"></div>


      <div id="files_bucket" class="files-bucket files-next-bucket clearfix pull-request-tab-content is-visible js-multi-line-comments-enabled">
        <diff-file-filter>
          


              

  <div
    id="partial-discussion-header"
    class="gh-header mb-3 js-details-container Details js-socket-channel js-updatable-content pull request js-pull-header-details"
    data-channel="eyJjIjoicHVsbF9yZXF1ZXN0OjkzNDc4ODc4MCIsInQiOjE2NjgyNzI1NDd9--a282774e2ca782031c49f622337cc70f90d86ce83c9918ba55f47be0e8411d22"
    data-url="/Yara-Rules/rules/pull/428/partials/title?sticky=false"
    data-pull-is-open="true"
    data-gid="PR_kwDOAgM1B843t76s">
    

  <div class="gh-header-show ">
    <div class="d-flex flex-column flex-md-row">
      <div class="gh-header-actions mt-0 mt-md-2 mb-3 mb-md-0 ml-0 flex-md-order-1 flex-shrink-0 d-flex flex-items-start">



          
<details class="details-reset details-overlay details-overlay-dark float-right" >
  <summary
    class="btn btn-sm btn-primary m-0 ml-0 ml-md-2"
    
    
    data-ga-click="Issues, create new issue, view:issue_show location:issue_header style:button logged_in:false"
    
  >
    
    New issue
  </summary>
  <details-dialog class="Box Box--overlay d-flex flex-column anim-fade-in fast overflow-auto" aria-label="Sign up for GitHub">
        <button aria-label="Close dialog" data-close-dialog="" type="button" data-view-component="true" class="Link--muted btn-link position-absolute p-4 right-0">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path>
</svg>
</button>    <div class="d-flex flex-column p-4">
            <div class="mt-3 mb-2 text-center">
  <svg height="60" aria-hidden="true" viewBox="0 0 24 24" version="1.1" width="60" data-view-component="true" class="octicon octicon-comment-discussion color-fg-accent">
    <path fill-rule="evenodd" d="M1.75 1A1.75 1.75 0 000 2.75v9.5C0 13.216.784 14 1.75 14H3v1.543a1.457 1.457 0 002.487 1.03L8.061 14h6.189A1.75 1.75 0 0016 12.25v-9.5A1.75 1.75 0 0014.25 1H1.75zM1.5 2.75a.25.25 0 01.25-.25h12.5a.25.25 0 01.25.25v9.5a.25.25 0 01-.25.25h-6.5a.75.75 0 00-.53.22L4.5 15.44v-2.19a.75.75 0 00-.75-.75h-2a.25.25 0 01-.25-.25v-9.5z"></path><path d="M22.5 8.75a.25.25 0 00-.25-.25h-3.5a.75.75 0 010-1.5h3.5c.966 0 1.75.784 1.75 1.75v9.5A1.75 1.75 0 0122.25 20H21v1.543a1.457 1.457 0 01-2.487 1.03L15.939 20H10.75A1.75 1.75 0 019 18.25v-1.465a.75.75 0 011.5 0v1.465c0 .138.112.25.25.25h5.5a.75.75 0 01.53.22l2.72 2.72v-2.19a.75.75 0 01.75-.75h2a.25.25 0 00.25-.25v-9.5z"></path>
</svg>
</div>

<div class="px-4">
  <p class="text-center mb-4">
  <strong>Have a question about this project?</strong> Sign up for a free GitHub account to open an issue and contact its maintainers and the community.
  </p>

  <!-- '"` --><!-- </textarea></xmp> --></option></form><form data-turbo="false" class="js-signup-form" autocomplete="off" action="/join?return_to=%2FYara-Rules%2Frules%2Fissues%2Fnew" accept-charset="UTF-8" method="post"><input type="hidden" data-csrf="true" name="authenticity_token" value="2az1bnujtEBzFsaJyRSmC6l9xM/9fivXyMcdQlefRNm2fyX3QzWL/IsfQz2wuvKmM/Cb9LvzJI9lwGlISdlyHQ==" />    <auto-check src="/signup_check/username">
      <dl class="form-group"><dt class="input-label"><label name="user[login]" autocapitalize="off" autofocus="autofocus" for="user_login_issues">Pick a username</label></dt><dd><input name="user[login]" autocapitalize="off" autofocus="autofocus" class="form-control" type="text" id="user_login_issues" /></dd></dl>
      <input type="hidden" data-csrf="true" value="gY8uqvSfSTdX4O9+LrkHgOmsqYaELpPRYFWoVdwEiu1pargQY3JfZ55Wl3640Cb2l5D8e698ZAbJaedZbBMCoA==" />
    </auto-check>

    <auto-check src="/signup_check/email">
      <dl class="form-group"><dt class="input-label"><label name="user[email]" autocapitalize="off" for="user_email_issues">Email Address</label></dt><dd><input name="user[email]" autocapitalize="off" class="form-control" type="text" id="user_email_issues" /></dd></dl>
      <input type="hidden" data-csrf="true" value="CJOUlUJguUfuCv9uLCFnK4gbOjSQzvsYV7jX1NUyiemmkkufTMm0QVgHoAew+cZyLg9glLsJAMwVqUudgG+6yw==" />
    </auto-check>

    <auto-check src="/users/password"><dl class="form-group"><dt class="input-label"><label name="user[password]" for="user_password_issues">Password</label></dt><dd><input name="user[password]" class="form-control" type="password" id="user_password_issues" /></dd></dl><input type="hidden" data-csrf="true" value="CdfZzz8qy3kEOpdsOjdeFYftEySpxlhsG777JXBmL+UGqKkBMldV6PCQSTxztC6CxcY5AvzHRzTFQ9/g2u/teA==" /></auto-check>

    <input type="hidden" name="source" class="js-signup-source" value="modal-issues">
    <input class="form-control" type="text" name="required_field_0218" hidden="hidden" />
<input class="form-control" type="hidden" name="timestamp" value="1668272547688" />
<input class="form-control" type="hidden" name="timestamp_secret" value="0ad2c05ed97678f047a96209ab755d8e2f29e4e1364b8bcbf3661582ff616bb3" />


      <button data-ga-click="(Logged out) New issue modal, clicked Sign up, text:sign-up" type="submit" data-view-component="true" class="btn-primary btn btn-block mt-2">    Sign up for GitHub
</button>
</form>
  <p class="mt-4 color-fg-muted text-center">By clicking &ldquo;Sign up for GitHub&rdquo;, you agree to our <a href="https://docs.github.com/terms" target="_blank">terms of service</a> and
  <a href="https://docs.github.com/privacy" target="_blank">privacy statement</a>. We’ll occasionally send you account related emails.</p>

  <p class="mt-4 color-fg-muted text-center">
    Already on GitHub?
    <a data-ga-click="(Logged out) New issue modal, clicked Sign in, text:sign-in" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;new issue modal&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="95ab6a16e277e47c52671620e1179533c05bee7d0cfd977e0b84fa28b47305a1" href="/login?return_to=%2FYara-Rules%2Frules%2Fissues%2Fnew">Sign in</a>
    to your account
  </p>
</div>

</div>
  </details-dialog>
</details>
        <div class="flex-auto text-right d-block d-md-none">
          <a href="#issue-comment-box" class="py-1">Jump to bottom</a>
        </div>
      </div>

    <h1 class="gh-header-title mb-2 lh-condensed f1 mr-0 flex-auto wb-break-word">
      <span class="js-issue-title markdown-title">Add SM4 rules</span>
      <span class="f1-light color-fg-muted">#428</span>
    </h1>
    </div>
  </div>

  <div class="d-flex flex-items-center flex-wrap mt-0 gh-header-meta">
    <div class="flex-shrink-0 mb-2 flex-self-start flex-md-self-center">
        <span reviewable_state="ready" title="Status: Open" data-view-component="true" class="State State--open">
  <svg height="16" class="octicon octicon-git-pull-request" viewBox="0 0 16 16" version="1.1" width="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.177 3.073L9.573.677A.25.25 0 0110 .854v4.792a.25.25 0 01-.427.177L7.177 3.427a.25.25 0 010-.354zM3.75 2.5a.75.75 0 100 1.5.75.75 0 000-1.5zm-2.25.75a2.25 2.25 0 113 2.122v5.256a2.251 2.251 0 11-1.5 0V5.372A2.25 2.25 0 011.5 3.25zM11 2.5h-1V4h1a1 1 0 011 1v5.628a2.251 2.251 0 101.5 0V5A2.5 2.5 0 0011 2.5zm1 10.25a.75.75 0 111.5 0 .75.75 0 01-1.5 0zM3.75 12a.75.75 0 100 1.5.75.75 0 000-1.5z"></path></svg> Open
</span>
    </div>




    <div class="flex-auto min-width-0 mb-2">
          <a class="author Link--secondary text-bold css-truncate css-truncate-target expandable" data-hovercard-type="user" data-hovercard-url="/users/sylvainpelissier/hovercard" data-octo-click="hovercard-link-click" data-octo-dimensions="link_type:self" href="/sylvainpelissier">sylvainpelissier</a>

  wants to merge
  <span class="js-updating-pull-request-commits-count">1</span>
  commit into



  <span title="Yara-Rules/rules:master" class="commit-ref css-truncate user-select-contain expandable base-ref"><a title="Yara-Rules/rules:master" class="no-underline " href="/Yara-Rules/rules"><span class="css-truncate-target">Yara-Rules</span>:<span class="css-truncate-target">master</span></a></span><span></span>

  <div class="commit-ref-dropdown">
    <details class="details-reset details-overlay select-menu commitish-suggester" id="branch-select-menu">
      <summary class="btn btn-sm select-menu-button branch" title="Choose a base branch">
        <i>base:</i>
        <span class="css-truncate css-truncate-target" title="master">master</span>
      </summary>
      <input-demux-context-wrapper data-context-type="baseChange">
        
<div class="SelectMenu">
  <div class="SelectMenu-modal">
    <header class="SelectMenu-header">
      <span class="SelectMenu-title">Choose a base branch</span>
      <button class="SelectMenu-closeButton" type="button" data-toggle-for="branch-select-menu"><svg aria-label="Close menu" aria-hidden="false" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path>
</svg></button>
    </header>

    <input-demux data-action="tab-container-change:input-demux#storeInput tab-container-changed:input-demux#updateInput">
      <tab-container class="d-flex flex-column js-branches-tags-tabs" style="min-height: 0;">
        <div class="SelectMenu-filter">
          <input data-target="input-demux.source"
                 id="context-commitish-filter-field"
                 class="SelectMenu-input form-control"
                 aria-owns="ref-list-branches"
                 data-controls-ref-menu-id="ref-list-branches"
                 autofocus
                 autocomplete="off"
                 aria-label="Find a branch"
                 placeholder="Find a branch"
                 type="text"
          >
        </div>

        <div class="SelectMenu-tabs" role="tablist" data-target="input-demux.control" hidden>
          <button class="SelectMenu-tab" type="button" role="tab" aria-selected="true">Branches</button>
          <button class="SelectMenu-tab" type="button" role="tab">Tags</button>
        </div>

        <div role="tabpanel" id="ref-list-branches" data-filter-placeholder="Find a branch" tabindex="" class="d-flex flex-column flex-auto">
          <ref-selector
            type="branch"
            data-targets="input-demux.sinks"
            data-action="
              input-entered:ref-selector#inputEntered
              tab-selected:ref-selector#tabSelected
              focus-list:ref-selector#focusFirstListMember
            "
            query-endpoint="/Yara-Rules/rules/refs"
            
            cache-key="v0:1538931540.0"
            current-committish="bWFzdGVy"
            default-branch="bWFzdGVy"
            name-with-owner="WWFyYS1SdWxlcy9ydWxlcw=="
            prefetch-on-mouseover
          >

            <template data-target="ref-selector.fetchFailedTemplate">
              <div class="SelectMenu-message" data-index="{{ index }}">Could not load branches</div>
            </template>

              <template data-target="ref-selector.noMatchTemplate">
  <div class="height-full width-full d-flex flex-items-center pl-3">
    <span>Branch not found: <strong>{{ refName }}</strong></span>
  </div>
</template>


            <div data-target="ref-selector.listContainer" role="menu" class="SelectMenu-list m-0" >
              <div class="SelectMenu-loading pt-3 pb-0 overflow-hidden" aria-label="Menu is loading">
                <svg style="box-sizing: content-box; color: var(--color-icon-primary);" width="32" height="32" viewBox="0 0 16 16" fill="none" data-view-component="true" class="anim-rotate">
  <circle cx="8" cy="8" r="7" stroke="currentColor" stroke-opacity="0.25" stroke-width="2" vector-effect="non-scaling-stroke" />
  <path d="M15 8a7.002 7.002 0 00-7-7" stroke="currentColor" stroke-width="2" stroke-linecap="round" vector-effect="non-scaling-stroke" />
</svg>
              </div>
            </div>

              <template data-target="ref-selector.itemTemplate">
  <button type="button" class="SelectMenu-item" role="menuitemradio" rel="nofollow" aria-checked="{{ isCurrent }}" data-index="{{ index }}" data-action="click:input-demux-context-wrapper#onItemSelected">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check SelectMenu-icon SelectMenu-icon--check">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
    <span class="flex-1 css-truncate css-truncate-overflow {{ isFilteringClass }} js-ref-name">{{ refName }}</span>
    <span hidden="{{ isNotDefault }}" class="Label Label--secondary flex-self-start">default</span>
  </button>
</template>


          </ref-selector>

        </div>

        <div role="tabpanel" id="tags-menu" data-filter-placeholder="Find a tag" tabindex="" hidden class="d-flex flex-column flex-auto">
          <ref-selector
            type="tag"
            data-action="
              input-entered:ref-selector#inputEntered
              tab-selected:ref-selector#tabSelected
              focus-list:ref-selector#focusFirstListMember
            "
            data-targets="input-demux.sinks"
            query-endpoint="/Yara-Rules/rules/refs"
            cache-key="v0:1538931540.0"
            current-committish="bWFzdGVy"
            default-branch="bWFzdGVy"
            name-with-owner="WWFyYS1SdWxlcy9ydWxlcw=="
          >

            <template data-target="ref-selector.fetchFailedTemplate">
              <div class="SelectMenu-message" data-index="{{ index }}">Could not load tags</div>
            </template>

            <template data-target="ref-selector.noMatchTemplate">
              <div class="SelectMenu-message" data-index="{{ index }}">Nothing to show</div>
            </template>

              <template data-target="ref-selector.itemTemplate">
  <button type="button" class="SelectMenu-item" role="menuitemradio" rel="nofollow" aria-checked="{{ isCurrent }}" data-index="{{ index }}" data-action="click:input-demux-context-wrapper#onItemSelected">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check SelectMenu-icon SelectMenu-icon--check">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
    <span class="flex-1 css-truncate css-truncate-overflow {{ isFilteringClass }} js-ref-name">{{ refName }}</span>
    <span hidden="{{ isNotDefault }}" class="Label Label--secondary flex-self-start">default</span>
  </button>
</template>


            <div data-target="ref-selector.listContainer" role="menu" class="SelectMenu-list" >
              <div class="SelectMenu-loading pt-3 pb-0 overflow-hidden" aria-label="Menu is loading">
                <svg style="box-sizing: content-box; color: var(--color-icon-primary);" width="32" height="32" viewBox="0 0 16 16" fill="none" data-view-component="true" class="anim-rotate">
  <circle cx="8" cy="8" r="7" stroke="currentColor" stroke-opacity="0.25" stroke-width="2" vector-effect="non-scaling-stroke" />
  <path d="M15 8a7.002 7.002 0 00-7-7" stroke="currentColor" stroke-width="2" stroke-linecap="round" vector-effect="non-scaling-stroke" />
</svg>
              </div>
            </div>
          </ref-selector>
        </div>
      </tab-container>
    </input-demux>
  </div>
</div>

        <template class="js-change-base-template">
          <div class="Box-header">
            <h3 class="Box-title">Are you sure you want to change the base?</h3>
          </div>
          <div class="Box-body">
            Some commits from the old base branch may be removed from the timeline,
            and old review comments may become outdated.
          </div>
          <div class="Box-footer">
            <button form="change-base-form" class="btn btn-primary btn-block" data-disable-with="Changing base…" type="submit">Change base</button>
          </div>
        </template>
      </input-demux-context-wrapper>
    </details>
    <!-- '"` --><!-- </textarea></xmp> --></option></form><form id="change-base-form" data-turbo="false" action="/Yara-Rules/rules/pull/428/change_base" accept-charset="UTF-8" method="post"><input type="hidden" data-csrf="true" name="authenticity_token" value="omXDEr79rBoSVRVC5AWhOx5KuPGPaO6HHQLYVl66CMENVdnEljVVV0rY1+5RLZ4enfay4a8Ea74LzuDa6+HI8w==" />
      <input type="hidden" id="pull-change-base-branch-field" name="new_base_binary" class="js-new-base-branch">
</form>  </div>

from

<span title="sylvainpelissier/rules:sm4" class="commit-ref css-truncate user-select-contain expandable head-ref"><a title="sylvainpelissier/rules:sm4" class="no-underline " href="/sylvainpelissier/rules/tree/sm4"><span class="css-truncate-target">sylvainpelissier</span>:<span class="css-truncate-target">sm4</span></a></span><span><clipboard-copy aria-label="Copy" data-copy-feedback="Copied!" value="sylvainpelissier:sm4" data-view-component="true" class="Link--onHover js-copy-branch color-fg-muted d-inline-block ml-1">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy">
    <path fill-rule="evenodd" d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 010 1.5h-1.5a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 00.25-.25v-1.5a.75.75 0 011.5 0v1.5A1.75 1.75 0 019.25 16h-7.5A1.75 1.75 0 010 14.25v-7.5z"></path><path fill-rule="evenodd" d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0114.25 11h-7.5A1.75 1.75 0 015 9.25v-7.5zm1.75-.25a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 00.25-.25v-7.5a.25.25 0 00-.25-.25h-7.5z"></path>
</svg>
    <svg style="display: none;" aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check color-fg-success">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
</clipboard-copy></span>



    </div>
  </div>



</div>


              
<div class="px-3 px-md-0 ml-n3 mr-n3 mx-md-0 tabnav">
    <div class="tabnav-extra float-right d-none d-md-block">
      <span class="diffstat" id="diffstat">
        <span class="color-fg-success">
          +36
        </span>
        <span class="color-fg-danger">
          −0
        </span>
        <span class="tooltipped tooltipped-s" aria-label="36 lines changed">
          <span class="diffstat-block-added"></span><span class="diffstat-block-added"></span><span class="diffstat-block-added"></span><span class="diffstat-block-added"></span><span class="diffstat-block-added"></span>
        </span>
      </span>
    </div>

  <nav class="tabnav-tabs d-flex overflow-auto" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame">
    <a href="/Yara-Rules/rules/pull/428" class="tabnav-tab flex-shrink-0  js-turbo-history-navigate">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-comment-discussion d-none d-md-inline-block">
    <path fill-rule="evenodd" d="M1.5 2.75a.25.25 0 01.25-.25h8.5a.25.25 0 01.25.25v5.5a.25.25 0 01-.25.25h-3.5a.75.75 0 00-.53.22L3.5 11.44V9.25a.75.75 0 00-.75-.75h-1a.25.25 0 01-.25-.25v-5.5zM1.75 1A1.75 1.75 0 000 2.75v5.5C0 9.216.784 10 1.75 10H2v1.543a1.457 1.457 0 002.487 1.03L7.061 10h3.189A1.75 1.75 0 0012 8.25v-5.5A1.75 1.75 0 0010.25 1h-8.5zM14.5 4.75a.25.25 0 00-.25-.25h-.5a.75.75 0 110-1.5h.5c.966 0 1.75.784 1.75 1.75v5.5A1.75 1.75 0 0114.25 12H14v1.543a1.457 1.457 0 01-2.487 1.03L9.22 12.28a.75.75 0 111.06-1.06l2.22 2.22v-2.19a.75.75 0 01.75-.75h1a.25.25 0 00.25-.25v-5.5z"></path>
</svg>
      Conversation

      <span id="conversation_tab_counter" title="0" data-view-component="true" class="Counter">0</span>
    </a>

    <a href="/Yara-Rules/rules/pull/428/commits" class="tabnav-tab flex-shrink-0 selected js-turbo-history-navigate" aria-current="page">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-git-commit d-none d-md-inline-block">
    <path fill-rule="evenodd" d="M10.5 7.75a2.5 2.5 0 11-5 0 2.5 2.5 0 015 0zm1.43.75a4.002 4.002 0 01-7.86 0H.75a.75.75 0 110-1.5h3.32a4.001 4.001 0 017.86 0h3.32a.75.75 0 110 1.5h-3.32z"></path>
</svg>
      Commits

      <span id="commits_tab_counter" title="1" data-view-component="true" class="Counter js-updateable-pull-request-commits-count">1</span>
    </a>

    <a href="/Yara-Rules/rules/pull/428/checks" class="tabnav-tab flex-shrink-0 ">
  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-checklist d-none d-md-inline-block">
    <path fill-rule="evenodd" d="M2.5 1.75a.25.25 0 01.25-.25h8.5a.25.25 0 01.25.25v7.736a.75.75 0 101.5 0V1.75A1.75 1.75 0 0011.25 0h-8.5A1.75 1.75 0 001 1.75v11.5c0 .966.784 1.75 1.75 1.75h3.17a.75.75 0 000-1.5H2.75a.25.25 0 01-.25-.25V1.75zM4.75 4a.75.75 0 000 1.5h4.5a.75.75 0 000-1.5h-4.5zM4 7.75A.75.75 0 014.75 7h2a.75.75 0 010 1.5h-2A.75.75 0 014 7.75zm11.774 3.537a.75.75 0 00-1.048-1.074L10.7 14.145 9.281 12.72a.75.75 0 00-1.062 1.058l1.943 1.95a.75.75 0 001.055.008l4.557-4.45z"></path>
</svg>
  Checks

  <span id="checks_tab_counter" title="1" data-view-component="true" class="Counter">1</span>
</a>


    <a href="/Yara-Rules/rules/pull/428/files" class="tabnav-tab flex-shrink-0  js-turbo-history-navigate">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-file-diff d-none d-md-inline-block">
    <path fill-rule="evenodd" d="M2.75 1.5a.25.25 0 00-.25.25v12.5c0 .138.112.25.25.25h10.5a.25.25 0 00.25-.25V4.664a.25.25 0 00-.073-.177l-2.914-2.914a.25.25 0 00-.177-.073H2.75zM1 1.75C1 .784 1.784 0 2.75 0h7.586c.464 0 .909.184 1.237.513l2.914 2.914c.329.328.513.773.513 1.237v9.586A1.75 1.75 0 0113.25 16H2.75A1.75 1.75 0 011 14.25V1.75zm7 1.5a.75.75 0 01.75.75v1.5h1.5a.75.75 0 010 1.5h-1.5v1.5a.75.75 0 01-1.5 0V7h-1.5a.75.75 0 010-1.5h1.5V4A.75.75 0 018 3.25zm-3 8a.75.75 0 01.75-.75h4.5a.75.75 0 010 1.5h-4.5a.75.75 0 01-.75-.75z"></path>
</svg>
      Files changed

        <span id="files_tab_counter" title="1" data-view-component="true" class="Counter">1</span>
    </a>
  </nav>
</div>


            <diff-layout sidebar-hidden>
                <button data-hotkey="t" data-action="click:diff-layout#handleOpenFilesListHotkeyEvent" hidden="hidden" type="button" data-view-component="true" class="btn">
</button>
                <div
                  class="pr-toolbar js-sticky js-position-sticky d-flex"
                  data-target="diff-layout.diffToolbar"
                >
                  <div class="diffbar details-collapse js-details-container Details flex-1 d-flex flex-items-center width-full">
                    <div class="show-if-stuck mr-2 hide-md hide-sm">
                      <div
  id="pull-state"
  class="js-socket-channel js-updatable-content"
  data-channel="eyJjIjoiaXNzdWU6MTIzMzkzMzEyNTpzdGF0ZSIsInQiOjE2NjgyNzI1NDd9--b270f38a8960d2801fb6320d4733fcb39a624e665b03d17b8cb92fbc463f9955"
  data-url="/Yara-Rules/rules/pull/428/partials/state"
  data-gid="PR_kwDOAgM1B843t76s"
>
  <span reviewable_state="ready" title="Status: Open" data-view-component="true" class="State State--open">
  <svg height="16" class="octicon octicon-git-pull-request" viewBox="0 0 16 16" version="1.1" width="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.177 3.073L9.573.677A.25.25 0 0110 .854v4.792a.25.25 0 01-.427.177L7.177 3.427a.25.25 0 010-.354zM3.75 2.5a.75.75 0 100 1.5.75.75 0 000-1.5zm-2.25.75a2.25 2.25 0 113 2.122v5.256a2.251 2.251 0 11-1.5 0V5.372A2.25 2.25 0 011.5 3.25zM11 2.5h-1V4h1a1 1 0 011 1v5.628a2.251 2.251 0 101.5 0V5A2.5 2.5 0 0011 2.5zm1 10.25a.75.75 0 111.5 0 .75.75 0 01-1.5 0zM3.75 12a.75.75 0 100 1.5.75.75 0 000-1.5z"></path></svg> Open
</span>
</div>

                    </div>

                    <div class="flex-auto min-width-0">
                      <div class="show-if-stuck hide-md hide-sm">
                        <h1 class="d-flex text-bold f5">
  <a class="js-issue-title css-truncate css-truncate-target Link--primary width-fit markdown-title js-smoothscroll-anchor" href="#top">
    Add SM4 rules
  </a>
  <span class="gh-header-number color-fg-muted pl-1">#428</span>
</h1>

                      </div>
                      <div class="d-flex flex-items-center flex-wrap" style="gap: 4px 16px;" data-pjax="#repo-content-pjax-container" data-turbo-frame="repo-content-turbo-frame">
                        <instrument-files
                          data-hydro-click-payload="{&quot;event_type&quot;:&quot;pull_request.user_action&quot;,&quot;payload&quot;:{&quot;user_id&quot;:null,&quot;pull_request_id&quot;:934788780,&quot;category&quot;:&quot;files&quot;,&quot;action&quot;:&quot;ctrl_f&quot;,&quot;data&quot;:{&quot;file_count&quot;:1},&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;}}"
                          data-hydro-click-hmac="fc989bf80ddfe5856707102b6e141c552ac4425c3a351e1a7c009477fbd5f182"
                        ></instrument-files>


                        

<details class="details-reset details-overlay diffbar-item diffbar-range-menu select-menu   ml-0">
    <summary data-hotkey="c" data-view-component="true" class="Link--muted select-menu-button btn-link">    Changes from <strong>all commits</strong>
</summary>
  <details-menu class="select-menu-modal position-absolute" style="z-index: 99;">
    <div class="select-menu-header">
      <span class="select-menu-title">Commits</span>
    </div>
    <div class="select-menu-list">
      <a href="/Yara-Rules/rules/pull/428/files" class="select-menu-item" role="menuitem">
        <div class="select-menu-item-text">
          <div class="text-emphasized css-truncate css-truncate-target">
            Show all changes
          </div>
          <span class="description">
            1 commit
          </span>
        </div>
      </a>


      <div class="select-menu-divider">
        Select commit
      </div>

      <div  data-range-url="/Yara-Rules/rules/pull/428/files/$range">
          <a
            href="/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47"
            class="select-menu-item "
            role="menuitem"
            data-commit="c0573b2" >

            <div class="select-menu-item-text">
              <code class="float-right">c0573b2</code>
              <div class="text-emphasized css-truncate css-truncate-target">
                Add SM4 rules
              </div>
              <span class="description">
                sylvainpelissier <relative-time datetime="2022-05-12T12:46:46Z" class="no-wrap">May 12, 2022</relative-time>
              </span>
            </div>
          </a>
      </div>
    </div>
  </details-menu>
</details>


                          <file-filter data-target="diff-file-filter.fileFilter" data-action="file-filter-change:diff-file-filter#applyFilter">
  <details class="diffbar-item details-reset details-overlay" >
    <summary class="Link--muted select-menu-button" aria-haspopup="true" data-target="file-filter.summary">
      <strong
        class="js-file-filter-text css-truncate css-truncate-target"
        data-target="file-filter.fileFilterActiveText"
        >
        File filter
      </strong>
    </summary>
    <details-menu class="SelectMenu js-file-filter">
      <div class="SelectMenu-modal">
        <header class="SelectMenu-header">
          <h3 class="SelectMenu-title">Filter by extension</h3>
        </header>
        <div class="SelectMenu-list SelectMenu-list--borderless">
          <form class="js-file-filter-form" data-turbo="false" action="/" accept-charset="UTF-8" method="post"><input type="hidden" name="authenticity_token" value="XpSG0lVWEOys8nH68M_YRkMR0OXsqyBVRMtTReQ_pI53xcPoV95Z39w-dBdAgP6FWVbEmbDwjo4cL6JcuZlwlg" autocomplete="off" />
            <fieldset>
              <legend class="sr-only">Filter by extension</legend>
                <label class="SelectMenu-item" role="menuitem">
                  <input
                    class="js-diff-file-type-option mr-2"
                    type="checkbox"
                    value=".yar"
                    name="file-filters[]"
                      checked
                    data-hydro-click-payload= "{&quot;event_type&quot;:&quot;pull_request.user_action&quot;,&quot;payload&quot;:{&quot;user_id&quot;:null,&quot;pull_request_id&quot;:934788780,&quot;category&quot;:&quot;file_filter&quot;,&quot;action&quot;:&quot;toggle_file_filter_option&quot;,&quot;data&quot;:{&quot;type&quot;:&quot;extension&quot;,&quot;file_type&quot;:&quot;.yar&quot;,&quot;file_count&quot;:1},&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;}}"
                    data-hydro-click-hmac= "0855a2787d0eb93080ab760a192364d52cfcc64b40a40122fb3ea02987fb8766"
                    data-non-deleted-files-count="1"
                    data-targets="file-filter.fileExtensions"
                    data-action="change:file-filter#updateFileInputs"
                    
                  >
                  .yar&nbsp
                  <span class="text-normal js-file-type-count"
                    data-non-deleted-file-count-markup="(1)"
                    data-all-file-count-markup="(1)"
                    data-targets="file-filter.fileTypeCount"
                  >
                    (1)
                  </span>
                </label>
              <label class="SelectMenu-item" role="menuitem">
                <input
                  type="checkbox"
                  class="sr-only hx_focus-input"
                  data-target="file-filter.selectAllInput"
                  data-action="change:file-filter#enableAllFileInputs"
                  
                >
                <span
                  class="color-fg-muted no-underline text-normal js-file-filter-select-all-container hx_focus-target"
                  data-select-all-markup="Select all 1 file type"
                  data-all-selected-markup="All 1 file type selected"
                  data-target="file-filter.selectAllContainer"
                  >
                  All 1 file type selected
                </span>
              </label>
            </fieldset>
            <hr class="SelectMenu-divider">

            <label class="SelectMenu-item" role="menuitem">
              <input
                type="checkbox"
                class="js-viewed-files-toggle mr-2"
                name="show-viewed-files"
                value="true"
                checked
                data-hydro-click-payload= "{&quot;event_type&quot;:&quot;pull_request.user_action&quot;,&quot;payload&quot;:{&quot;user_id&quot;:null,&quot;pull_request_id&quot;:934788780,&quot;category&quot;:&quot;file_filter&quot;,&quot;action&quot;:&quot;toggle_file_filter_option&quot;,&quot;data&quot;:{&quot;type&quot;:&quot;viewed&quot;},&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;}}"
                data-hydro-click-hmac= "42c5890f54600b8a42474cba8450b2bf8fa8e0dab6ce607ca8736d43845c27e4"
                data-target="file-filter.viewedFilesInput"
                data-action="change:file-filter#updateFileInputs"
                
                >
              Viewed files
            </label>
</form>        </div>
      </div>
    </details-menu>
  </details>
</file-filter>

                          <div
  class="js-reset-filters diffbar-item hide-sm"
  data-target="diff-file-filter.resetFilters"
  hidden
>
  <a class="text-bold color-fg-accent no-underline"
     href="/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47"
     data-action="click:diff-file-filter#clearFilters"
     data-hydro-click="{&quot;event_type&quot;:&quot;pull_request.user_action&quot;,&quot;payload&quot;:{&quot;user_id&quot;:null,&quot;pull_request_id&quot;:934788780,&quot;category&quot;:&quot;file_filter&quot;,&quot;action&quot;:&quot;clear_filters&quot;,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;}}"
     data-hydro-click-hmac="0dd4a0df02cbcd0990f5bf4d0e6a884e6d0476f373fb785075a4681fcb33c820"
   >
    <svg style="height: 12px; width: 12px;" aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x color-fg-muted">
    <path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path>
</svg> <span>Clear filters</span>
  </a>
</div>

                          
<div class="position-relative diffbar-item ml-0">
  <details class="details-reset details-overlay toc-select js-conversations-details select-menu ml-0">
      <summary data-ga-click="Pull Requests, open view comments menu, type:semantic" data-view-component="true" class="Link--muted select-menu-button btn-link">    <strong class="js-conversation-menu-button">Conversations</strong>
</summary>    <details-menu class="select-menu-modal position-absolute" style="z-index: 99;" id="conversations-menu" src="/Yara-Rules/rules/pull/428/conversations_menu" preload>
      <include-fragment>
        <div class="select-menu-blankslate select-menu-error">
          Failed to load comments.   <button type="button" data-view-component="true" class="js-toc-retry btn-link">    Retry
</button>
        </div>
        <div class="select-menu-loading-overlay d-flex flex-items-center">
          <svg style="box-sizing: content-box; color: var(--color-icon-primary);" width="32" height="32" viewBox="0 0 16 16" fill="none" data-view-component="true" class="flex-1 anim-rotate">
  <circle cx="8" cy="8" r="7" stroke="currentColor" stroke-opacity="0.25" stroke-width="2" vector-effect="non-scaling-stroke" />
  <path d="M15 8a7.002 7.002 0 00-7-7" stroke="currentColor" stroke-width="2" stroke-linecap="round" vector-effect="non-scaling-stroke" />
</svg>
        </div>
      </include-fragment>
    </details-menu>
  </details>
</div>

                          
<details
  class="details-reset details-overlay diffbar-item toc-select select-menu ml-0"
  data-target="diff-layout.tocMenu"
  
>
    <summary data-target="diff-layout.tocMenuButton" data-ga-click="Pull Requests, open table of contents, type:semantic" data-view-component="true" class="Link--muted select-menu-button btn-link">    <strong>Jump to</strong>
</summary>  <details-menu class="select-menu-modal position-absolute" style="z-index: 99;" src="/Yara-Rules/rules/pull/428/show_toc?base_sha=0f93570194a80d2f2032869055808b0ddcdfb360&amp;sha1=0f93570194a80d2f2032869055808b0ddcdfb360&amp;sha2=c0573b2ea53cf72470f4eed0bc2000e73b55ee47" preload>
      <div class="select-menu-header">
        <span class="select-menu-title">
          Jump to file
        </span>
      </div>
      <include-fragment>
        <div class="select-menu-blankslate select-menu-error">
          Failed to load files.   <button type="button" data-view-component="true" class="js-toc-retry btn-link">    Retry
</button>
        </div>
        <div class="select-menu-loading-overlay" aria-label="Loading">
          <svg style="box-sizing: content-box; color: var(--color-icon-primary);" width="32" height="32" viewBox="0 0 16 16" fill="none" data-view-component="true" class="anim-rotate">
  <circle cx="8" cy="8" r="7" stroke="currentColor" stroke-opacity="0.25" stroke-width="2" vector-effect="non-scaling-stroke" />
  <path d="M15 8a7.002 7.002 0 00-7-7" stroke="currentColor" stroke-width="2" stroke-linecap="round" vector-effect="non-scaling-stroke" />
</svg>
        </div>
      </include-fragment>
  </details-menu>
</details>

                          <div class="hide-sm hide-md">
  <details data-view-component="true" class="diffbar-item details-reset details-overlay position-relative text-center">
  <summary role="button" data-view-component="true">      <svg aria-label="Diff settings" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-gear color-fg-muted">
    <path fill-rule="evenodd" d="M7.429 1.525a6.593 6.593 0 011.142 0c.036.003.108.036.137.146l.289 1.105c.147.56.55.967.997 1.189.174.086.341.183.501.29.417.278.97.423 1.53.27l1.102-.303c.11-.03.175.016.195.046.219.31.41.641.573.989.014.031.022.11-.059.19l-.815.806c-.411.406-.562.957-.53 1.456a4.588 4.588 0 010 .582c-.032.499.119 1.05.53 1.456l.815.806c.08.08.073.159.059.19a6.494 6.494 0 01-.573.99c-.02.029-.086.074-.195.045l-1.103-.303c-.559-.153-1.112-.008-1.529.27-.16.107-.327.204-.5.29-.449.222-.851.628-.998 1.189l-.289 1.105c-.029.11-.101.143-.137.146a6.613 6.613 0 01-1.142 0c-.036-.003-.108-.037-.137-.146l-.289-1.105c-.147-.56-.55-.967-.997-1.189a4.502 4.502 0 01-.501-.29c-.417-.278-.97-.423-1.53-.27l-1.102.303c-.11.03-.175-.016-.195-.046a6.492 6.492 0 01-.573-.989c-.014-.031-.022-.11.059-.19l.815-.806c.411-.406.562-.957.53-1.456a4.587 4.587 0 010-.582c.032-.499-.119-1.05-.53-1.456l-.815-.806c-.08-.08-.073-.159-.059-.19a6.44 6.44 0 01.573-.99c.02-.029.086-.075.195-.045l1.103.303c.559.153 1.112.008 1.529-.27.16-.107.327-.204.5-.29.449-.222.851-.628.998-1.189l.289-1.105c.029-.11.101-.143.137-.146zM8 0c-.236 0-.47.01-.701.03-.743.065-1.29.615-1.458 1.261l-.29 1.106c-.017.066-.078.158-.211.224a5.994 5.994 0 00-.668.386c-.123.082-.233.09-.3.071L3.27 2.776c-.644-.177-1.392.02-1.82.63a7.977 7.977 0 00-.704 1.217c-.315.675-.111 1.422.363 1.891l.815.806c.05.048.098.147.088.294a6.084 6.084 0 000 .772c.01.147-.038.246-.088.294l-.815.806c-.474.469-.678 1.216-.363 1.891.2.428.436.835.704 1.218.428.609 1.176.806 1.82.63l1.103-.303c.066-.019.176-.011.299.071.213.143.436.272.668.386.133.066.194.158.212.224l.289 1.106c.169.646.715 1.196 1.458 1.26a8.094 8.094 0 001.402 0c.743-.064 1.29-.614 1.458-1.26l.29-1.106c.017-.066.078-.158.211-.224a5.98 5.98 0 00.668-.386c.123-.082.233-.09.3-.071l1.102.302c.644.177 1.392-.02 1.82-.63.268-.382.505-.789.704-1.217.315-.675.111-1.422-.364-1.891l-.814-.806c-.05-.048-.098-.147-.088-.294a6.1 6.1 0 000-.772c-.01-.147.039-.246.088-.294l.814-.806c.475-.469.679-1.216.364-1.891a7.992 7.992 0 00-.704-1.218c-.428-.609-1.176-.806-1.82-.63l-1.103.303c-.066.019-.176.011-.299-.071a5.991 5.991 0 00-.668-.386c-.133-.066-.194-.158-.212-.224L10.16 1.29C9.99.645 9.444.095 8.701.031A8.094 8.094 0 008 0zm1.5 8a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM11 8a3 3 0 11-6 0 3 3 0 016 0z"></path>
</svg>
      <div class="dropdown-caret color-fg-muted"></div>
</summary>
  <div data-view-component="true">      <div data-view-component="true" class="Popover position-absolute mt-2 pt-1 right-0 left-0">
  <div style="left: -107px; top: -7px" data-view-component="true" class="Popover-message Box position-relative mx-auto text-left color-shadow-large p-2 mt-2">
    <h5 data-view-component="true" class="mb-2">          Diff view
</h5>
              <!-- '"` --><!-- </textarea></xmp> --></option></form><form data-turbo="false" action="/Yara-Rules/rules/pull/428/diffview" accept-charset="UTF-8" method="post"><input type="hidden" data-csrf="true" name="authenticity_token" value="WNF/U2jM0j6K6lO0CP5cQFru1mFgytfmEtjqyEqvXVcXrqSZd0KNYwLtCewjOhR5ENxRKXr6oGf2R/O7TNyOIQ==" />
            <div class="d-flex flex-justify-center">
              <div class="js-unified-diff-view">
                <div data-view-component="true" class="js-unified-diff-view-box border rounded-1 pt-3 pb-1 color-border-accent-emphasis">
                  <label>
                    <div class="text-center px-4">
                      <img src="https://github.githubassets.com/images/modules/pulls/unified.svg" height="50" width="50" alt="Unified Diff View" data-view-component="true" />
                    </div>
                    <div class="d-flex flex-items-center ml-2">
                      <input type="radio" name="diff" id="diff_unified_lg" value="unified" class="js-unified-diff-view-option mr-1" checked="checked" />
                      Unified
                    </div>
                  </label>
</div>              </div>

              <div class="js-split-diff-view">
                <div data-view-component="true" class="js-split-diff-view-box border rounded-1 pt-3 pb-1 ml-2 color-border-default">
                  <label>
                    <div class="text-center px-4">
                      <img src="https://github.githubassets.com/images/modules/pulls/split.svg" height="50" width="50" alt="Split Diff View" data-view-component="true" />
                    </div>
                    <div class="d-flex flex-items-center ml-2">
                      <input type="radio" name="diff" id="diff_split_lg" value="split" class="js-split-diff-view-option mr-1" />
                      Split
                    </div>
                  </label>
</div>              </div>
            </div>

            <div class="mt-2 mb-2 ml-1 d-flex flex-items-center">
              <input type="hidden" name="w" id="show_whitespace_lg" value="0" autocomplete="off" class="form-control" />
              <input type="checkbox" name="w" id="whitespace-cb-lg" value="1" />
              <label for="whitespace-cb-lg" class="ml-1">Hide whitespace</label>
            </div>

            <div class="d-flex flex-column mt-2 px-1">
                <button type="submit" data-view-component="true" class="btn-sm btn">    Apply and reload
</button>
            </div>
</form>
</div></div></div>
</details></div>
<div class="ml-2 hide-sm hide-md">
  <!-- '"` --><!-- </textarea></xmp> --></option></form><form data-turbo="false" action="/Yara-Rules/rules/pull/428/diffview" accept-charset="UTF-8" method="post"><input type="hidden" data-csrf="true" name="authenticity_token" value="BKrnwsTedTDI8lho/sMxT8TDWw9aRGTYJgHYFEnPcr1L1TwI21AqbUD1AjDVB3l2jvHcR0B0E1nCnsFnT7yhyw==" />
      <button name="w" value="0" hidden="hidden" type="submit" data-view-component="true" class="btn-invisible btn-sm btn color-fg-accent py-0 px-1 rounded-1">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-eye color-fg-accent mr-2">
    <path fill-rule="evenodd" d="M1.679 7.932c.412-.621 1.242-1.75 2.366-2.717C5.175 4.242 6.527 3.5 8 3.5c1.473 0 2.824.742 3.955 1.715 1.124.967 1.954 2.096 2.366 2.717a.119.119 0 010 .136c-.412.621-1.242 1.75-2.366 2.717C10.825 11.758 9.473 12.5 8 12.5c-1.473 0-2.824-.742-3.955-1.715C2.92 9.818 2.09 8.69 1.679 8.068a.119.119 0 010-.136zM8 2c-1.981 0-3.67.992-4.933 2.078C1.797 5.169.88 6.423.43 7.1a1.619 1.619 0 000 1.798c.45.678 1.367 1.932 2.637 3.024C4.329 13.008 6.019 14 8 14c1.981 0 3.67-.992 4.933-2.078 1.27-1.091 2.187-2.345 2.637-3.023a1.619 1.619 0 000-1.798c-.45-.678-1.367-1.932-2.637-3.023C11.671 2.992 9.981 2 8 2zm0 8a2 2 0 100-4 2 2 0 000 4z"></path>
</svg>Show whitespace
</button></form></div>

<div class="hide-lg hide-xl">
  <details data-view-component="true" class="diffbar-item details-reset details-overlay position-relative text-center">
  <summary role="button" data-view-component="true">      <svg aria-label="Diff settings" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-gear color-fg-muted">
    <path fill-rule="evenodd" d="M7.429 1.525a6.593 6.593 0 011.142 0c.036.003.108.036.137.146l.289 1.105c.147.56.55.967.997 1.189.174.086.341.183.501.29.417.278.97.423 1.53.27l1.102-.303c.11-.03.175.016.195.046.219.31.41.641.573.989.014.031.022.11-.059.19l-.815.806c-.411.406-.562.957-.53 1.456a4.588 4.588 0 010 .582c-.032.499.119 1.05.53 1.456l.815.806c.08.08.073.159.059.19a6.494 6.494 0 01-.573.99c-.02.029-.086.074-.195.045l-1.103-.303c-.559-.153-1.112-.008-1.529.27-.16.107-.327.204-.5.29-.449.222-.851.628-.998 1.189l-.289 1.105c-.029.11-.101.143-.137.146a6.613 6.613 0 01-1.142 0c-.036-.003-.108-.037-.137-.146l-.289-1.105c-.147-.56-.55-.967-.997-1.189a4.502 4.502 0 01-.501-.29c-.417-.278-.97-.423-1.53-.27l-1.102.303c-.11.03-.175-.016-.195-.046a6.492 6.492 0 01-.573-.989c-.014-.031-.022-.11.059-.19l.815-.806c.411-.406.562-.957.53-1.456a4.587 4.587 0 010-.582c.032-.499-.119-1.05-.53-1.456l-.815-.806c-.08-.08-.073-.159-.059-.19a6.44 6.44 0 01.573-.99c.02-.029.086-.075.195-.045l1.103.303c.559.153 1.112.008 1.529-.27.16-.107.327-.204.5-.29.449-.222.851-.628.998-1.189l.289-1.105c.029-.11.101-.143.137-.146zM8 0c-.236 0-.47.01-.701.03-.743.065-1.29.615-1.458 1.261l-.29 1.106c-.017.066-.078.158-.211.224a5.994 5.994 0 00-.668.386c-.123.082-.233.09-.3.071L3.27 2.776c-.644-.177-1.392.02-1.82.63a7.977 7.977 0 00-.704 1.217c-.315.675-.111 1.422.363 1.891l.815.806c.05.048.098.147.088.294a6.084 6.084 0 000 .772c.01.147-.038.246-.088.294l-.815.806c-.474.469-.678 1.216-.363 1.891.2.428.436.835.704 1.218.428.609 1.176.806 1.82.63l1.103-.303c.066-.019.176-.011.299.071.213.143.436.272.668.386.133.066.194.158.212.224l.289 1.106c.169.646.715 1.196 1.458 1.26a8.094 8.094 0 001.402 0c.743-.064 1.29-.614 1.458-1.26l.29-1.106c.017-.066.078-.158.211-.224a5.98 5.98 0 00.668-.386c.123-.082.233-.09.3-.071l1.102.302c.644.177 1.392-.02 1.82-.63.268-.382.505-.789.704-1.217.315-.675.111-1.422-.364-1.891l-.814-.806c-.05-.048-.098-.147-.088-.294a6.1 6.1 0 000-.772c-.01-.147.039-.246.088-.294l.814-.806c.475-.469.679-1.216.364-1.891a7.992 7.992 0 00-.704-1.218c-.428-.609-1.176-.806-1.82-.63l-1.103.303c-.066.019-.176.011-.299-.071a5.991 5.991 0 00-.668-.386c-.133-.066-.194-.158-.212-.224L10.16 1.29C9.99.645 9.444.095 8.701.031A8.094 8.094 0 008 0zm1.5 8a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM11 8a3 3 0 11-6 0 3 3 0 016 0z"></path>
</svg>
      <div class="dropdown-caret color-fg-muted"></div>
</summary>
  <div data-view-component="true">      <div data-view-component="true" class="Popover position-fixed mt-2 pt-1 right-0 left-0">
  <div data-view-component="true" class="Popover-message Box position-relative mx-auto text-left color-shadow-large p-2 mt-2">
    <h5 data-view-component="true" class="mb-2">          Diff view
</h5>
              <!-- '"` --><!-- </textarea></xmp> --></option></form><form data-turbo="false" action="/Yara-Rules/rules/pull/428/diffview" accept-charset="UTF-8" method="post"><input type="hidden" data-csrf="true" name="authenticity_token" value="5Vh6yusMUZEThPls3UcYHNFADU4iCkCypOk//u+gpqeqJ6EA9IIOzJuDozT2g1Alm3KKBjg6NzNAdiaN6dN10Q==" />
            <div class="d-flex flex-items-center">
              <div class="js-unified-diff-view d-flex flex-1">
                <div data-view-component="true" class="js-unified-diff-view-box flex-1 border rounded-1 pt-3 px-2 pb-1 color-border-accent-emphasis">
                  <label>
                    <div class="text-center">
                      <img src="https://github.githubassets.com/images/modules/pulls/unified.svg" height="50" width="50" alt="Unified Diff View" data-view-component="true" />
                    </div>
                    <div class="d-flex flex-items-center">
                      <input type="radio" name="diff" id="diff_unified" value="unified" class="js-unified-diff-view-option mr-1" checked="checked" />
                      Unified
                    </div>
                  </label>
</div>              </div>

              <div class="js-split-diff-view d-flex flex-1">
                <div data-view-component="true" class="js-split-diff-view-box flex-1 border rounded-1 pt-3 px-2 pb-1 ml-2 color-border-default">
                  <label>
                    <div class="text-center">
                      <img src="https://github.githubassets.com/images/modules/pulls/split.svg" height="50" width="50" alt="Split Diff View" data-view-component="true" />
                    </div>
                    <div class="d-flex flex-items-center">
                      <input type="radio" name="diff" id="diff_split" value="split" class="js-split-diff-view-option mr-1" />
                      Split
                    </div>
                  </label>
</div>              </div>
            </div>

            <div class="mt-2 mb-2 d-flex flex-items-center">
              <input type="hidden" name="w" id="show_whitespace" value="0" autocomplete="off" class="form-control" />
              <input type="checkbox" name="w" id="whitespace-cb" value="1" />
              <label for="whitespace-cb" class="ml-1">Hide whitespace</label>
            </div>

            <div class="d-flex flex-column mt-2">
                <button type="submit" data-view-component="true" class="btn-sm btn">    Apply and reload
</button>
            </div>
</form>
</div></div></div>
</details></div>


                            <div class="js-socket-channel js-updatable-content js-pull-refresh-on-pjax" data-channel="eyJjIjoicHVsbF9yZXF1ZXN0OjkzNDc4ODc4MCIsInQiOjE2NjgyNzI1NDd9--a282774e2ca782031c49f622337cc70f90d86ce83c9918ba55f47be0e8411d22" data-url="/Yara-Rules/rules/pull/428/show_partial_comparison?base_commit_oid=0f93570194a80d2f2032869055808b0ddcdfb360&amp;end_commit_oid=c0573b2ea53cf72470f4eed0bc2000e73b55ee47&amp;partial=pull_requests%2Fstale_comparison&amp;start_commit_oid=0f93570194a80d2f2032869055808b0ddcdfb360" data-gid="PR_kwDOAgM1B843t76s"></div>

                      </div>
                    </div>

                    <div class="flex-grow-0 flex-shrink-0 pr-review-tools">

                    </div>
                  </div>
                </div>
                <div class="toolbar-shadow js-notification-shelf-offset-top" data-original-top="60px"></div>
                  
  
      <div class="commit full-commit prh-commit px-2 pt-2 ">

    <span class="mr-1 mt-1 float-left">
        
<batch-deferred-content class="d-inline-block" data-url="/Yara-Rules/rules/commits/checks-statuses-rollups">
    <input type="hidden" name="oid" value="c0573b2ea53cf72470f4eed0bc2000e73b55ee47" data-targets="batch-deferred-content.inputs" autocomplete="off" />
    <input type="hidden" name="dropdown_direction" value="se" data-targets="batch-deferred-content.inputs" autocomplete="off" />

  

  <div class="commit-build-statuses">
    <span class="Skeleton d-inline-block" style="width:14px; height:14px; margin-top:5px;"></span>
  </div>

</batch-deferred-content>
    </span>

    <div class="commit-title markdown-title">
      Add SM4 rules
    </div>


  <div class="commit-branches pb-2">
  <include-fragment src="/Yara-Rules/rules/branch_commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47" id="async-branches-list">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-git-branch">
    <path fill-rule="evenodd" d="M11.75 2.5a.75.75 0 100 1.5.75.75 0 000-1.5zm-2.25.75a2.25 2.25 0 113 2.122V6A2.5 2.5 0 0110 8.5H6a1 1 0 00-1 1v1.128a2.251 2.251 0 11-1.5 0V5.372a2.25 2.25 0 111.5 0v1.836A2.492 2.492 0 016 7h4a1 1 0 001-1v-.628A2.25 2.25 0 019.5 3.25zM4.25 12a.75.75 0 100 1.5.75.75 0 000-1.5zM3.5 3.25a.75.75 0 111.5 0 .75.75 0 01-1.5 0z"></path>
</svg>
    <ul class="branches-list">
      <li class="loading">Loading branch information<span class="AnimatedEllipsis"></span></li>
    </ul>
</include-fragment></div>


  <div class="commit-meta clearfix p-2 no-wrap d-flex flex-column flex-md-row flex-md-items-center">
    <div class="flex-auto pb-2 pb-md-0">
      <div class="float-left">
<div class="AvatarStack flex-self-start  " >
  <div class="AvatarStack-body" aria-label="sylvainpelissier" >
      <a class="avatar avatar-user" style="width:20px;height:20px;" data-test-selector="commits-avatar-stack-avatar-link" data-hovercard-type="user" data-hovercard-url="/users/sylvainpelissier/hovercard" data-octo-click="hovercard-link-click" data-octo-dimensions="link_type:self" href="/sylvainpelissier">
        <img data-test-selector="commits-avatar-stack-avatar-image" src="https://avatars.githubusercontent.com/u/7047419?s=40&amp;v=4" width="20" height="20" alt="@sylvainpelissier" class=" avatar-user" />
</a>  </div>
</div>
</div>
          <a class="commit-author user-mention" title="View all commits by sylvainpelissier" href="/Yara-Rules/rules/commits?author=sylvainpelissier">sylvainpelissier</a>
    
  committed
  <relative-time datetime="2022-05-12T12:46:46Z" class="no-wrap">May 12, 2022</relative-time>


        
<batch-deferred-content class="d-inline-block" data-url="/commits/badges">
    <input type="hidden" name="id" value="C_kwDOAgM1B9oAKGMwNTczYjJlYTUzY2Y3MjQ3MGY0ZWVkMGJjMjAwMGU3M2I1NWVlNDc" data-targets="batch-deferred-content.inputs" autocomplete="off" />
    <input type="hidden" name="badge_size" value="medium" data-targets="batch-deferred-content.inputs" autocomplete="off" />
    <input type="hidden" name="dropdown_direction" value="s" data-targets="batch-deferred-content.inputs" autocomplete="off" />

  
</batch-deferred-content>
    </div>
    <span>
      <span class="text-small color-fg-muted text-mono">commit <span class="sha user-select-contain">c0573b2ea53cf72470f4eed0bc2000e73b55ee47</span></span>
    </span>
  </div>
</div>



    
<template class="js-comment-button-template">
    <button data-path="{{ path }}" data-anchor="{{ anchor }}" data-position="{{ position }}" data-side="{{ side }}" data-line="{{ line }}" data-original-line="{{ originalLine }}" aria-label="Add line comment" type="button" data-view-component="true" class="add-line-comment js-add-line-comment js-add-single-line-comment btn-link">    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-plus">
    <path fill-rule="evenodd" d="M7.75 2a.75.75 0 01.75.75V7h4.25a.75.75 0 110 1.5H8.5v4.25a.75.75 0 11-1.5 0V8.5H2.75a.75.75 0 010-1.5H7V2.75A.75.75 0 017.75 2z"></path>
</svg>
</button></template>

<div id="files" class="diff-view  js-diff-container js-code-nav-container" data-hpc>


  <div
    class="container-md js-file-filter-blankslate"
    data-target="diff-file-filter.blankslate"
    hidden>
    
  <div data-view-component="true" class="blankslate">
    <svg aria-hidden="true" height="24" viewBox="0 0 24 24" version="1.1" width="24" data-view-component="true" class="octicon octicon-filter blankslate-icon">
    <path d="M2.75 6a.75.75 0 000 1.5h18.5a.75.75 0 000-1.5H2.75zM6 11.75a.75.75 0 01.75-.75h10.5a.75.75 0 010 1.5H6.75a.75.75 0 01-.75-.75zm4 4.938a.75.75 0 01.75-.75h2.5a.75.75 0 010 1.5h-2.5a.75.75 0 01-.75-.75z"></path>
</svg>

    <h2 data-view-component="true" class="blankslate-heading">        There are no files selected for viewing
</h2>
    

</div>  </div>

  <div class="js-diff-progressive-container">
    
<div id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671"
     data-details-container-group="file"
     class="file js-file js-details-container js-targetable-element show-inline-notes Details
              Details--on open
             
             
             
             
             
             js-tagsearch-file
           "
          data-file-type=".yar"
          data-file-deleted="false"
          data-tagsearch-path="crypto/crypto_signatures.yar"
          data-tagsearch-lang="YARA"
          data-targets="diff-file-filter.diffEntries"
      >
  <div class="file-header d-flex flex-md-row flex-column flex-md-items-center file-header--expandable js-file-header js-skip-tagsearch  sticky-file-header js-position-sticky js-position-sticky-stacked"
    
    data-path="crypto/crypto_signatures.yar"
    data-short-path="6f712e3"
    data-anchor="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671"
    data-file-type=".yar"
    data-file-deleted="false"
    >
    <div class="file-info flex-auto min-width-0 mb-md-0 mb-2">
      <button type="button" class="btn-octicon js-details-target" aria-label="Toggle diff contents" aria-expanded="true" style="width: 22px;">
  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-chevron-down Details-content--hidden">
    <path fill-rule="evenodd" d="M12.78 6.22a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06 0L3.22 7.28a.75.75 0 011.06-1.06L8 9.94l3.72-3.72a.75.75 0 011.06 0z"></path>
</svg>
  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-chevron-right Details-content--shown">
    <path fill-rule="evenodd" d="M6.22 3.22a.75.75 0 011.06 0l4.25 4.25a.75.75 0 010 1.06l-4.25 4.25a.75.75 0 01-1.06-1.06L9.94 8 6.22 4.28a.75.75 0 010-1.06z"></path>
</svg>
</button>


        <div class="js-expand-full-wrapper d-inline-block">
          <button
            type="button"
            class="btn-link color-fg-muted no-underline js-expand-full directional-expander tooltipped tooltipped-se"
            aria-label="Expand all"
            data-url="/Yara-Rules/rules/blob_expand/9d4ed0b6d2d3f510bae40cc0dc05f6a0f630b1b6?anchor=diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671&amp;context=pull_request&amp;diff=unified&amp;direction=full&amp;mode=100644&amp;path=crypto%2Fcrypto_signatures.yar&amp;pull_request_id=934788780"
          >
            <svg aria-label="Expand all" aria-hidden="false" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-unfold">
    <path d="M8.177.677l2.896 2.896a.25.25 0 01-.177.427H8.75v1.25a.75.75 0 01-1.5 0V4H5.104a.25.25 0 01-.177-.427L7.823.677a.25.25 0 01.354 0zM7.25 10.75a.75.75 0 011.5 0V12h2.146a.25.25 0 01.177.427l-2.896 2.896a.25.25 0 01-.354 0l-2.896-2.896A.25.25 0 015.104 12H7.25v-1.25zm-5-2a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5zM6 8a.75.75 0 01-.75.75h-.5a.75.75 0 010-1.5h.5A.75.75 0 016 8zm2.25.75a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5zM12 8a.75.75 0 01-.75.75h-.5a.75.75 0 010-1.5h.5A.75.75 0 0112 8zm2.25.75a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5z"></path>
</svg>
          </button>
          <button
            type="button"
            class="btn-link color-fg-muted no-underline js-collapse-diff tooltipped tooltipped-se"
            aria-label="Collapse expanded lines"
            hidden
          >
            <svg aria-label="Collapse added diff lines" aria-hidden="false" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-fold">
    <path d="M10.896 2H8.75V.75a.75.75 0 00-1.5 0V2H5.104a.25.25 0 00-.177.427l2.896 2.896a.25.25 0 00.354 0l2.896-2.896A.25.25 0 0010.896 2zM8.75 15.25a.75.75 0 01-1.5 0V14H5.104a.25.25 0 01-.177-.427l2.896-2.896a.25.25 0 01.354 0l2.896 2.896a.25.25 0 01-.177.427H8.75v1.25zm-6.5-6.5a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5zM6 8a.75.75 0 01-.75.75h-.5a.75.75 0 010-1.5h.5A.75.75 0 016 8zm2.25.75a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5zM12 8a.75.75 0 01-.75.75h-.5a.75.75 0 010-1.5h.5A.75.75 0 0112 8zm2.25.75a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5z"></path>
</svg>
          </button>
        </div>

        



        <span class="diffstat tooltipped tooltipped-e" aria-label="36 changes: 36 additions &amp; 0 deletions">36 <span class="diffstat-block-added"></span><span class="diffstat-block-added"></span><span class="diffstat-block-added"></span><span class="diffstat-block-added"></span><span class="diffstat-block-added"></span></span>

      
<span class="Truncate">
  <a title="crypto/crypto_signatures.yar" class="Link--primary Truncate-text" href="#diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671">crypto/crypto_signatures.yar</a>

  <clipboard-copy data-copy-feedback="Copied!" aria-label="Copy" value="crypto/crypto_signatures.yar" data-view-component="true" class="Link--onHover color-fg-muted ml-2 mr-2">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy">
    <path fill-rule="evenodd" d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 010 1.5h-1.5a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 00.25-.25v-1.5a.75.75 0 011.5 0v1.5A1.75 1.75 0 019.25 16h-7.5A1.75 1.75 0 010 14.25v-7.5z"></path><path fill-rule="evenodd" d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0114.25 11h-7.5A1.75 1.75 0 015 9.25v-7.5zm1.75-.25a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 00.25-.25v-7.5a.25.25 0 00-.25-.25h-7.5z"></path>
</svg>
    <svg style="display: none;" aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check color-fg-success">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
</clipboard-copy>
</span>


      
    </div>

    <div class="file-actions pt-0 mb-md-0 mb-2 ml-md-2 flex-shrink-0 flex-md-justify-end">
      <div class="d-flex flex-justify-end">









        <details class="js-file-header-dropdown dropdown details-overlay details-reset pr-2 pl-2">
          <summary class="height-full">
            <div class="height-full d-flex flex-items-center Link--secondary">
              <svg aria-label="Show options" role="img" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-kebab-horizontal">
    <path d="M8 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zM1.5 9a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm13 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z"></path>
</svg>
            </div>
          </summary>
          <details-menu class="dropdown-menu dropdown-menu-sw show-more-popover color-fg-default position-absolute f5" style="width:185px; z-index:99; right: -4px;">
                        <label role="menuitemradio" class="dropdown-item btn-link text-normal d-block pl-5" tabindex="0" aria-checked="true">
            <span class="position-absolute ml-n4"><svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg></span>
            <input type="checkbox" checked class="d-none js-toggle-file-notes">
            Show comments
          </label>

                        <div role="none" class="dropdown-divider"></div>

                          <a href="/Yara-Rules/rules/blob/c0573b2ea53cf72470f4eed0bc2000e73b55ee47/crypto/crypto_signatures.yar"
   class="pl-5 dropdown-item btn-link"
   rel="nofollow"
   role="menuitem"
   data-ga-click="View file, click, location:files_changed_dropdown"
   
   >
   View file
</a>


                                <button type="button" disabled role="menuitem" class="pl-5 dropdown-item btn-link" aria-label="You must be signed in and have push access to make changes.">
      Edit file
    </button>


                              <button type="button" disabled role="menuitem" class="pl-5 dropdown-item btn-link" aria-label="You must be signed in and have push access to delete this file.">
    Delete file
  </button>


                        <div role="none" class="dropdown-divider"></div>

                        
<a class="pl-5 dropdown-item btn-link js-remove-unless-platform"
    data-platforms="windows,mac" role="menuitem"
    href="https://desktop.github.com"
    aria-label="Open this file in GitHub Desktop"
    data-analytics-event="{&quot;category&quot;:&quot;Repository&quot;,&quot;action&quot;:&quot;open with desktop&quot;,&quot;label&quot;:&quot;pull_request_id:934788780&quot;}">
    Open in desktop
</a>


              
          </details-menu>
        </details>
      </div>
    </div>
  </div>


  <div class="js-file-content Details-content--hidden position-relative"
    data-hydro-view="{&quot;event_type&quot;:&quot;pull_request.select_diff_range&quot;,&quot;payload&quot;:{&quot;actor_id&quot;:null,&quot;pull_request_id&quot;:934788780,&quot;repository_id&quot;:33764615,&quot;diff_type&quot;:&quot;UNIFIED&quot;,&quot;whitespace_ignored&quot;:false,&quot;originating_url&quot;:&quot;https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47&quot;,&quot;user_id&quot;:null}}" data-hydro-view-hmac="04533ded2e61acea831c2e31c9255889dc9fc9aadacbfdddfa982d2ea0cc4b26">


        <div class="data highlight js-blob-wrapper js-check-bidi
          
          "
          style="overflow-x: auto; overflow-y: hidden;"
        >
          <template class="js-file-alert-template">
  <div data-view-component="true" class="flash flash-warn flash-full d-flex flex-items-center">
  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path fill-rule="evenodd" d="M8.22 1.754a.25.25 0 00-.44 0L1.698 13.132a.25.25 0 00.22.368h12.164a.25.25 0 00.22-.368L8.22 1.754zm-1.763-.707c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0114.082 15H1.918a1.75 1.75 0 01-1.543-2.575L6.457 1.047zM9 11a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.25a.75.75 0 00-1.5 0v2.5a.75.75 0 001.5 0v-2.5z"></path>
</svg>
  
    <span>
      This file contains bidirectional Unicode text that may be interpreted or compiled differently than what appears below. To review, open the file in an editor that reveals hidden Unicode characters.
      <a href="https://github.co/hiddenchars" target="_blank">Learn more about bidirectional Unicode characters</a>
    </span>


  <div data-view-component="true" class="flash-action">        <a href="{{ revealButtonHref }}" data-view-component="true" class="btn-sm btn">    Show hidden characters
</a>
</div>
</div></template>
<template class="js-line-alert-template">
  <span aria-label="This line has hidden Unicode characters" data-view-component="true" class="line-alert tooltipped tooltipped-e">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path fill-rule="evenodd" d="M8.22 1.754a.25.25 0 00-.44 0L1.698 13.132a.25.25 0 00.22.368h12.164a.25.25 0 00.22-.368L8.22 1.754zm-1.763-.707c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0114.082 15H1.918a1.75 1.75 0 01-1.543-2.575L6.457 1.047zM9 11a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.25a.75.75 0 00-1.5 0v2.5a.75.75 0 001.5 0v-2.5z"></path>
</svg>
</span></template>



            <deferred-diff-lines class="awaiting-highlight" data-url="/Yara-Rules/rules/diffs/0f93570194a80d2f2032869055808b0ddcdfb360..c0573b2ea53cf72470f4eed0bc2000e73b55ee47?base_sha=0f93570194a80d2f2032869055808b0ddcdfb360&amp;whitespace_ignored=false">
      <input type="hidden" name="path" value="crypto/crypto_signatures.yar" data-targets="deferred-diff-lines.inputs" autocomplete="off" />
    
            <table class=" diff-table js-diff-table tab-size  " data-tab-size="8" data-diff-anchor="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671" data-paste-markdown-skip>
              <thead hidden>
                <tr>
                    <th scope="col">Original file line number</th>
                    <th scope="col">Diff line number</th>
                    <th scope="col">Diff line change</th>
                </tr>
              </thead>
              <tbody>
                    
      <tr class="js-expandable-line js-skip-tagsearch" data-position="0">
    <td class="blob-num blob-num-expandable" colspan="2">
          <a href="#diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671"
              class="js-expand directional-expander single-expander" title="Expand Up" aria-label="Expand Up"
              data-url="/Yara-Rules/rules/blob_excerpt/9d4ed0b6d2d3f510bae40cc0dc05f6a0f630b1b6?context=pull_request&amp;diff=unified&amp;direction=up&amp;in_wiki_context=&amp;last_left=&amp;last_right=&amp;left=1171&amp;left_hunk_size=6&amp;mode=100644&amp;path=crypto%2Fcrypto_signatures.yar&amp;pull_request_id=934788780&amp;right=1171&amp;right_hunk_size=42"
              data-left-range="1-1170" data-right-range="1-1170">
            <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-fold-up">
    <path d="M7.823 1.677L4.927 4.573A.25.25 0 005.104 5H7.25v3.236a.75.75 0 101.5 0V5h2.146a.25.25 0 00.177-.427L8.177 1.677a.25.25 0 00-.354 0zM13.75 11a.75.75 0 000 1.5h.5a.75.75 0 000-1.5h-.5zm-3.75.75a.75.75 0 01.75-.75h.5a.75.75 0 010 1.5h-.5a.75.75 0 01-.75-.75zM7.75 11a.75.75 0 000 1.5h.5a.75.75 0 000-1.5h-.5zM4 11.75a.75.75 0 01.75-.75h.5a.75.75 0 010 1.5h-.5a.75.75 0 01-.75-.75zM1.75 11a.75.75 0 000 1.5h.5a.75.75 0 000-1.5h-.5z"></path>
</svg>
          </a>
    </td>
    <td class="blob-code blob-code-inner blob-code-hunk">@@ -1171,6 +1171,42 @@ rule RsaEuro_NN_modMult</td>
  </tr>

    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671L1171" data-line-number="1171"
        class="blob-num blob-num-context js-linkable-line-number"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1171" data-line-number="1171"
        class="blob-num blob-num-context js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-context  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker=" ">		$c0</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671L1172" data-line-number="1172"
        class="blob-num blob-num-context js-linkable-line-number"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1172" data-line-number="1172"
        class="blob-num blob-num-context js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-context  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker=" ">}</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671L1173" data-line-number="1173"
        class="blob-num blob-num-context js-linkable-line-number"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1173" data-line-number="1173"
        class="blob-num blob-num-context js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-context  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker=" "><br></span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1174" data-line-number="1174"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">rule SM4_SBox</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1175" data-line-number="1175"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">{	meta:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1176" data-line-number="1176"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		author = &quot;spelissier&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1177" data-line-number="1177"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		description = &quot;SM4 SBox&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1178" data-line-number="1178"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		date = &quot;2022-05&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1179" data-line-number="1179"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		reference=&quot;https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10#section-6.2.3&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1180" data-line-number="1180"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">	strings:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1181" data-line-number="1181"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		$c0 = { D6 90 E9 FE CC E1 3D B7 16 B6 14 C2 28 FB 2C 05 }</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1182" data-line-number="1182"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">	condition:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1183" data-line-number="1183"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		$c0</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1184" data-line-number="1184"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">}</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1185" data-line-number="1185"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+"><br></span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1186" data-line-number="1186"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">rule SM4_FK</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1187" data-line-number="1187"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">{	meta:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1188" data-line-number="1188"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		author = &quot;spelissier&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1189" data-line-number="1189"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		description = &quot;SM4 Familiy key FK&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1190" data-line-number="1190"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		date = &quot;2022-05&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1191" data-line-number="1191"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		reference=&quot;https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10#section-7.3.1&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1192" data-line-number="1192"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">	strings:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1193" data-line-number="1193"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		$c0 = { C6 BA B1 A3 50 33 AA 56 97 91 7D 67 DC 22 70 B2 }</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1194" data-line-number="1194"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">	condition:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1195" data-line-number="1195"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		$c0</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1196" data-line-number="1196"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">}</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1197" data-line-number="1197"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+"><br></span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1198" data-line-number="1198"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">rule SM4_CK</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1199" data-line-number="1199"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">{	meta:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1200" data-line-number="1200"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		author = &quot;spelissier&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1201" data-line-number="1201"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		description = &quot;SM4 Constant Key CK&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1202" data-line-number="1202"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		date = &quot;2022-05&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1203" data-line-number="1203"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		reference=&quot;https://datatracker.ietf.org/doc/html/draft-ribose-cfrg-sm4-10#section-7.3.2&quot;</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1204" data-line-number="1204"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">	strings:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1205" data-line-number="1205"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		$c0 = { 15 0E 07 00 31 2A 23 1C 4D 46 3F 38 69 62 5B 54 }</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1206" data-line-number="1206"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">	condition:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1207" data-line-number="1207"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">		$c0</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1208" data-line-number="1208"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+">}</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td class="blob-num blob-num-addition empty-cell"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1209" data-line-number="1209"
        class="blob-num blob-num-addition js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-addition  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker="+"><br></span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671L1174" data-line-number="1174"
        class="blob-num blob-num-context js-linkable-line-number"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1210" data-line-number="1210"
        class="blob-num blob-num-context js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-context  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker=" ">rule Miracl_Big_constructor</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671L1175" data-line-number="1175"
        class="blob-num blob-num-context js-linkable-line-number"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1211" data-line-number="1211"
        class="blob-num blob-num-context js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-context  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker=" ">{	meta:</span></td>
</tr>




    <tr data-hunk="1255c4538c8cd16ccfe6fee0f7e070823a68751e87861ea27191a3373db223f2" class="show-top-border">
    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671L1176" data-line-number="1176"
        class="blob-num blob-num-context js-linkable-line-number"></td>

    <td id="diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671R1212" data-line-number="1212"
        class="blob-num blob-num-context js-linkable-line-number js-code-nav-line-number js-blob-rnum"></td>

  <td class="blob-code blob-code-context  js-file-line">
    <span class='blob-code-inner blob-code-marker js-code-nav-pass ' data-code-marker=" ">		author = &quot;Maxx&quot;</span></td>
</tr>




  <tr class="js-expandable-line js-skip-tagsearch" data-position="">
    <td class="blob-num blob-num-expandable" colspan="2">
          <a href="#diff-6f712e3a5416eef980be54bd030df467bafbcf773d00fc462d758dba28894671"
                class="js-expand directional-expander single-expander" title="Expand Down" aria-label="Expand Down"
                data-url="/Yara-Rules/rules/blob_excerpt/9d4ed0b6d2d3f510bae40cc0dc05f6a0f630b1b6?context=pull_request&amp;diff=unified&amp;direction=down&amp;in_wiki_context=&amp;last_left=1176&amp;last_right=1212&amp;left=1617&amp;left_hunk_size=&amp;mode=100644&amp;path=crypto%2Fcrypto_signatures.yar&amp;pull_request_id=934788780&amp;right=1617&amp;right_hunk_size="
                data-left-range="1177-1616" data-right-range="1213-1616">
            <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-fold-down">
    <path d="M8.177 14.323l2.896-2.896a.25.25 0 00-.177-.427H8.75V7.764a.75.75 0 10-1.5 0V11H5.104a.25.25 0 00-.177.427l2.896 2.896a.25.25 0 00.354 0zM2.25 5a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5zM6 4.25a.75.75 0 01-.75.75h-.5a.75.75 0 010-1.5h.5a.75.75 0 01.75.75zM8.25 5a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5zM12 4.25a.75.75 0 01-.75.75h-.5a.75.75 0 010-1.5h.5a.75.75 0 01.75.75zm2.25.75a.75.75 0 000-1.5h-.5a.75.75 0 000 1.5h.5z"></path>
</svg>
          </a>
    </td>
    <td class="blob-code blob-code-inner blob-code-hunk"></td>
  </tr>


              </tbody>
            </table>

</deferred-diff-lines>        </div>

  </div>
</div>

  </div>


    <div class="js-diff-progressive-container">
  <include-fragment src="/Yara-Rules/rules/unchanged_files_with_annotations?blob_paths%5B%5D=.github&amp;pull_number=428&amp;sha2=c0573b2ea53cf72470f4eed0bc2000e73b55ee47" class="diff-progressive-loader js-diff-progressive-loader mb-4 d-flex flex-items-center flex-justify-center"
      data-targets="diff-file-filter.progressiveLoaders"
    
  >
    <svg data-hide-on-error="true" style="box-sizing: content-box; color: var(--color-icon-primary);" width="32" height="32" viewBox="0 0 16 16" fill="none" data-view-component="true" class="anim-rotate">
  <circle cx="8" cy="8" r="7" stroke="currentColor" stroke-opacity="0.25" stroke-width="2" vector-effect="non-scaling-stroke" />
  <path d="M15 8a7.002 7.002 0 00-7-7" stroke="currentColor" stroke-width="2" stroke-linecap="round" vector-effect="non-scaling-stroke" />
</svg>

    <div data-show-on-error hidden>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert color-fg-danger mr-1">
    <path fill-rule="evenodd" d="M8.22 1.754a.25.25 0 00-.44 0L1.698 13.132a.25.25 0 00.22.368h12.164a.25.25 0 00.22-.368L8.22 1.754zm-1.763-.707c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0114.082 15H1.918a1.75 1.75 0 01-1.543-2.575L6.457 1.047zM9 11a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.25a.75.75 0 00-1.5 0v2.5a.75.75 0 001.5 0v-2.5z"></path>
</svg>
      Oops, something went wrong.
        <button data-retry-button="" type="button" data-view-component="true" class="btn-link">    Retry
</button>
    </div>
  </include-fragment>
</div>

</div>

<button type="button" class="js-toggle-all-file-notes" data-hotkey="i" style="display:none">Toggle all file notes</button>

<button type="button" class="js-toggle-all-file-annotations" data-hotkey="a" style="display:none">Toggle all file annotations</button>

<svg aria-hidden="true" width="320px" height="84px" viewBox="0 0 340 84" version="1.1"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  class="diff-placeholder-svg position-absolute bottom-0">
  <defs>
    <clippath id="diff-placeholder">
      <rect x="0" y="0" width="67.0175439" height="11.9298746" rx="2"></rect>
      <rect x="18.9473684" y="47.7194983" width="100.701754" height="11.9298746" rx="2"></rect>
      <rect x="0" y="71.930126" width="37.8947368" height="11.9298746" rx="2"></rect>
      <rect x="127.017544" y="48.0703769" width="53.3333333" height="11.9298746" rx="2"></rect>
      <rect x="187.719298" y="48.0703769" width="72.9824561" height="11.9298746" rx="2"></rect>
      <rect x="76.8421053" y="0" width="140.350877" height="11.9298746" rx="2"></rect>
      <rect x="17.8947368" y="23.8597491" width="140.350877" height="11.9298746" rx="2"></rect>
      <rect x="166.315789" y="23.8597491" width="173.684211" height="11.9298746" rx="2"></rect>
    </clippath>

    <linearGradient id="animated-diff-gradient" x1="0" x2="0" y1="0" y2="1" spreadMethod="reflect">
      <stop offset="0" stop-color="#eee"></stop>
      <stop offset="0.2" stop-color="#eee"></stop>
      <stop offset="0.5" stop-color="#ddd"></stop>
      <stop offset="0.8" stop-color="#eee"></stop>
      <stop offset="1" stop-color="#eee"></stop>
      <animateTransform attributeName="y1" values="0%; 100%; 0" dur="1s" repeatCount="3"></animateTransform>
      <animateTransform attributeName="y2" values="100%; 200%; 0" dur="1s" repeatCount="3"></animateTransform>
    </linearGradient>
  </defs>
</svg>




            </diff-layout>
        </diff-file-filter>
      </div>
    </div>
    <div hidden>
  <span class="js-add-to-batch-enabled">Add this suggestion to a batch that can be applied as a single commit.</span>
  <span class="js-unchanged-suggestion">This suggestion is invalid because no changes were made to the code.</span>
  <span class="js-closed-pull">Suggestions cannot be applied while the pull request is closed.</span>
  <span class="js-viewing-subset-changes">Suggestions cannot be applied while viewing a subset of changes.</span>
  <span class="js-one-suggestion-per-line">Only one suggestion per line can be applied in a batch.</span>
  <span class="js-reenable-add-to-batch">Add this suggestion to a batch that can be applied as a single commit.</span>
  <span class="js-validation-on-left-blob">Applying suggestions on deleted lines is not supported.</span>
  <span class="js-validation-on-right-blob">You must change the existing code in this line in order to create a valid suggestion.</span>
  <span class="js-outdated-comment">Outdated suggestions cannot be applied.</span>
  <span class="js-resolved-thread">This suggestion has been applied or marked resolved.</span>
  <span class="js-pending-review">Suggestions cannot be applied from pending reviews.</span>
  <span class="js-is-multiline">Suggestions cannot be applied on multi-line comments.</span>
  <span class="js-in-merge-queue">Suggestions cannot be applied while the pull request is queued to merge.</span>
  <div class="form-group errored m-0 error js-suggested-changes-inline-validation-template d-flex" style="cursor: default;">
    <span class="js-suggested-changes-inline-error-message position-relative error m-0" style="max-width: inherit;"></span>
  </div>
</div>


    </div>


  </div>

  </turbo-frame>


    </main>
  </div>

  </div>

          <footer class="footer width-full container-xl p-responsive">
  <h2 class='sr-only'>Footer</h2>

  <div class="position-relative d-flex flex-items-center pb-2 f6 color-fg-muted border-top color-border-muted flex-column-reverse flex-lg-row flex-wrap flex-lg-nowrap mt-6 pt-6">
    <div class="list-style-none d-flex flex-wrap col-0 col-lg-2 flex-justify-start flex-lg-justify-between mb-2 mb-lg-0">
      <div class="mt-2 mt-lg-0 d-flex flex-items-center">
        <a aria-label="Homepage" title="GitHub" class="footer-octicon mr-2" href="https://github.com">
          <svg aria-hidden="true" height="24" viewBox="0 0 16 16" version="1.1" width="24" data-view-component="true" class="octicon octicon-mark-github">
    <path fill-rule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path>
</svg>
</a>        <span>
        &copy; 2022 GitHub, Inc.
        </span>
      </div>
    </div>

    <nav aria-label='footer' class="col-12 col-lg-8">
      <h3 class='sr-only' id='sr-footer-heading'>Footer navigation</h3>
      <ul class="list-style-none d-flex flex-wrap col-12 flex-justify-center flex-lg-justify-between mb-2 mb-lg-0" aria-labelledby='sr-footer-heading'>
          <li class="mr-3 mr-lg-0"><a href="https://docs.github.com/en/github/site-policy/github-terms-of-service" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to terms&quot;,&quot;label&quot;:&quot;text:terms&quot;}">Terms</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://docs.github.com/en/github/site-policy/github-privacy-statement" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to privacy&quot;,&quot;label&quot;:&quot;text:privacy&quot;}">Privacy</a></li>
          <li class="mr-3 mr-lg-0"><a data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to security&quot;,&quot;label&quot;:&quot;text:security&quot;}" href="https://github.com/security">Security</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://www.githubstatus.com/" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to status&quot;,&quot;label&quot;:&quot;text:status&quot;}">Status</a></li>
          <li class="mr-3 mr-lg-0"><a data-ga-click="Footer, go to help, text:Docs" href="https://docs.github.com">Docs</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://support.github.com?tags=dotcom-footer" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to contact&quot;,&quot;label&quot;:&quot;text:contact&quot;}">Contact GitHub</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://github.com/pricing" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to Pricing&quot;,&quot;label&quot;:&quot;text:Pricing&quot;}">Pricing</a></li>
        <li class="mr-3 mr-lg-0"><a href="https://docs.github.com" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to api&quot;,&quot;label&quot;:&quot;text:api&quot;}">API</a></li>
        <li class="mr-3 mr-lg-0"><a href="https://services.github.com" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to training&quot;,&quot;label&quot;:&quot;text:training&quot;}">Training</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://github.blog" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to blog&quot;,&quot;label&quot;:&quot;text:blog&quot;}">Blog</a></li>
          <li><a data-ga-click="Footer, go to about, text:about" href="https://github.com/about">About</a></li>
      </ul>
    </nav>
  </div>

  <div class="d-flex flex-justify-center pb-6">
    <span class="f6 color-fg-muted"></span>
  </div>
</footer>




  <div id="ajax-error-message" class="ajax-error-message flash flash-error" hidden>
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path fill-rule="evenodd" d="M8.22 1.754a.25.25 0 00-.44 0L1.698 13.132a.25.25 0 00.22.368h12.164a.25.25 0 00.22-.368L8.22 1.754zm-1.763-.707c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0114.082 15H1.918a1.75 1.75 0 01-1.543-2.575L6.457 1.047zM9 11a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.25a.75.75 0 00-1.5 0v2.5a.75.75 0 001.5 0v-2.5z"></path>
</svg>
    <button type="button" class="flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path>
</svg>
    </button>
    You can’t perform that action at this time.
  </div>

  <div class="js-stale-session-flash flash flash-warn flash-banner" hidden
    >
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path fill-rule="evenodd" d="M8.22 1.754a.25.25 0 00-.44 0L1.698 13.132a.25.25 0 00.22.368h12.164a.25.25 0 00.22-.368L8.22 1.754zm-1.763-.707c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0114.082 15H1.918a1.75 1.75 0 01-1.543-2.575L6.457 1.047zM9 11a1 1 0 11-2 0 1 1 0 012 0zm-.25-5.25a.75.75 0 00-1.5 0v2.5a.75.75 0 001.5 0v-2.5z"></path>
</svg>
    <span class="js-stale-session-flash-signed-in" hidden>You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
    <span class="js-stale-session-flash-signed-out" hidden>You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
  </div>
    <template id="site-details-dialog">
  <details class="details-reset details-overlay details-overlay-dark lh-default color-fg-default hx_rsm" open>
    <summary role="button" aria-label="Close dialog"></summary>
    <details-dialog class="Box Box--overlay d-flex flex-column anim-fade-in fast hx_rsm-dialog hx_rsm-modal">
      <button class="Box-btn-octicon m-0 btn-octicon position-absolute right-0 top-0" type="button" aria-label="Close dialog" data-close-dialog>
        <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path fill-rule="evenodd" d="M3.72 3.72a.75.75 0 011.06 0L8 6.94l3.22-3.22a.75.75 0 111.06 1.06L9.06 8l3.22 3.22a.75.75 0 11-1.06 1.06L8 9.06l-3.22 3.22a.75.75 0 01-1.06-1.06L6.94 8 3.72 4.78a.75.75 0 010-1.06z"></path>
</svg>
      </button>
      <div class="octocat-spinner my-6 js-details-dialog-spinner"></div>
    </details-dialog>
  </details>
</template>

    <div class="Popover js-hovercard-content position-absolute" style="display: none; outline: none;" tabindex="0">
  <div class="Popover-message Popover-message--bottom-left Popover-message--large Box color-shadow-large" style="width:360px;">
  </div>
</div>

    <template id="snippet-clipboard-copy-button">
  <div class="zeroclipboard-container position-absolute right-0 top-0">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn js-clipboard-copy m-2 p-0 tooltipped-no-delay" data-copy-feedback="Copied!" data-tooltip-direction="w">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon m-2">
    <path fill-rule="evenodd" d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 010 1.5h-1.5a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 00.25-.25v-1.5a.75.75 0 011.5 0v1.5A1.75 1.75 0 019.25 16h-7.5A1.75 1.75 0 010 14.25v-7.5z"></path><path fill-rule="evenodd" d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0114.25 11h-7.5A1.75 1.75 0 015 9.25v-7.5zm1.75-.25a.25.25 0 00-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 00.25-.25v-7.5a.25.25 0 00-.25-.25h-7.5z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none m-2">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>




  </body>
</html>

/div>
</template>




  </body>
</html>

 00.25-.25v-7.5a.25.25 0 00-.25-.25h-7.5z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none m-2">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>




  </body>
</html>

5z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none m-2">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>




  </body>
</html>

="js-global-screen-reader-notice" class="sr-only" aria-live="polite" ></div>
  </body>
</html>


6" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none m-2">
    <path fill-rule="evenodd" d="M13.78 4.22a.75.75 0 010 1.06l-7.25 7.25a.75.75 0 01-1.06 0L2.22 9.28a.75.75 0 011.06-1.06L6 10.94l6.72-6.72a.75.75 0 011.06 0z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>




      </div>

      <div id="js-global-screen-reader-notice" class="sr-only" aria-live="polite" ></div>
  </body>
</html>

tml>


tml>

ml>



>
</html>

html>

n-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>




    </div>

    <div id="js-global-screen-reader-notice" class="sr-only" aria-live="polite" ></div>
  </body>
</html>



