rule swivelload_bin
{
    meta:
        description = "Swivelload"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/34b3dc00-a855-49a0-a4be-0bc38b9007b9"
        date = "2021/10"
        maltype = "Loader"

    strings:
        $string1 = { 25 73 00 00 25 73 }
        $string2 = "curl"
        $string3 = "Sysnative"
        $string4 = "TEMP%u"
        $string5 = "-windowstyle hidden -executionpolicy bypass -file"

    condition:
        uint16(0) == 0x5A4D and all of ($string*) and filesize < 100KB
}

rule swivelload_mem
{
    meta:
        description = "Swivelload"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/34b3dc00-a855-49a0-a4be-0bc38b9007b9"
        date = "2021/10"
        maltype = "Loader"

    strings:
        $string1 = { 25 73 00 00 25 73 }
        $string2 = "curl"
        $string3 = "Sysnative"
        $string4 = "TEMP%u"
        $string5 = "-windowstyle hidden -executionpolicy bypass -file"

    condition:
        all of ($string*) and filesize > 100KB
}