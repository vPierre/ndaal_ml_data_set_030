import "pe"
import "math"
import "hash"

rule IterateResourcesDemo
{
    meta:
        description = "Example rule to iterate over PE resources and calculate entropy, MD5 and check for strings"

    strings:
        $test = "Test"

    condition:
        // Iterate over resources
        for any i in (0 .. pe.number_of_resources - 1):
        (
            // Check resource entropy
            math.entropy(pe.resources[i].offset, pe.resources[i].length) > 7.0 or
            // Check resource hash
            hash.md5(pe.resources[i].offset, pe.resources[i].length) == "44d88612fea8a8f36de82e1278abb02f" or
            // Check for string in resource
            $test in (pe.resources[i].offset .. pe.resources[i].offset + pe.resources[i].length)
        )
}

rule IterateSectionsDemo
{
    meta:
        description = "Example rule to iterate over PE sections and calculate entropy, MD5 and check for strings"

    strings:
        $test = "Test"

    condition:
        // Iterate over sections
        for any i in (0 .. pe.number_of_sections - 1):
        (
            // Check section entropy
            math.entropy(pe.sections[i].raw_data_offset, pe.sections[i].raw_data_size) > 7.0 or
            // Check section hash
            hash.md5(pe.sections[i].raw_data_offset, pe.sections[i].raw_data_size) == "44d88612fea8a8f36de82e1278abb02f" or
            // Check for string in section
            $test in (pe.sections[i].raw_data_offset .. pe.sections[i].raw_data_offset + pe.sections[i].raw_data_size)
        )
}
