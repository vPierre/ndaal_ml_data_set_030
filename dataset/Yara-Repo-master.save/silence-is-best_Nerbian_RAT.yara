rule nerbian_bin
{
    meta:
        description = "Nerbian RAT"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/778a4bd7-7ed2-4b61-aad5-822cd5195442/"
        date = "2022/05"
        maltype = "RAT"

    strings:
        $string1 = "main.downloadNerbian"
        $string2 = "main.g_nerbian_download_url"
        $string3 = "main.g_default_nerbian_store_path"

    condition:
        all of ($string*) and filesize > 100KB
}