rule Socelars_bin
{
    meta:
        description = "Socelars stealer"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/6cd9a083-44e6-48e2-9c21-355c35cb9a57"
        date = "2022/01"
        maltype = "Stealer"

    strings:
        $string1 = "Unknow OS"
        $string2 = "os_crypt"
        $string3 = "FrieldsCount"

    condition:
        uint16(0) == 0x5A4D and 2 of ($string*) and filesize < 2500KB
}

rule Socelars_mem
{
    meta:
        description = "Socelars stealer"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/6cd9a083-44e6-48e2-9c21-355c35cb9a57"
        date = "2022/01"
        maltype = "Stealer"

    strings:
        $string1 = "Unknow OS"
        $string2 = "os_crypt"
        $string3 = "FrieldsCount"

    condition:
        2 of ($string*) and filesize > 2100KB
}