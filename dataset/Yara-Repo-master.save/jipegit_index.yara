/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./jipegit/Banker/Citadel.yara"
include "./jipegit/Banker/Ice-IX.yara"
include "./jipegit/Banker/Qadars.yara"
include "./jipegit/Banker/Shylock.yara"
include "./jipegit/Banker/Spyeye.yara"
include "./jipegit/Document/office_document_vba.yara"
include "./jipegit/Packer/AutoIT.yara"
include "./jipegit/Packer/Dotfuscator.yara"
include "./jipegit/RAT/BlackShades.yara"
include "./jipegit/RAT/Cerberus.yara"
include "./jipegit/RAT/DarkComet.yara"
include "./jipegit/RAT/PlugX.yara"
include "./jipegit/RAT/Poisonivy.yara"
include "./jipegit/RAT/Swrort.yara"
include "./jipegit/RAT/Terminator.yara"
include "./jipegit/RAT/XTremRat.yara"
include "./jipegit/RAT/jRAT_conf.yara"
