/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./CiscoCXSecurity/yara/log4j.yara"
include "./CiscoCXSecurity/yara/personal/log4jJndiLookup.yara"
include "./CiscoCXSecurity/yara/personal/log4jball.yara"
include "./CiscoCXSecurity/yara/personal/log4jimport.yara"
include "./CiscoCXSecurity/yara/personal/log4jjavaclass.yara"
include "./CiscoCXSecurity/yara/personal/log4jjavasrc.yara"
