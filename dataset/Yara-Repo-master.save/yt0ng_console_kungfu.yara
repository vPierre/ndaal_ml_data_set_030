// Must have console module via yara-4.2.0-rc1+
// expects decoded beacons 

import "pe"
import "console"


rule CobaltStrike_Watermark_Profiler: Profiler 
{
meta:
    description = "Profiles Cobaltstrike Watermarks using Yara Console Module"
    author = "Markus Neis"
    date = "2022-05-02" 
        
strings:

    $watermark = {00 25 00 02 00 04 ?? ?? ?? ??} // TLV for watermark  

condition:
    filesize < 5MB and
    console.log("Watermark: ", uint32be(@watermark+6))

}
