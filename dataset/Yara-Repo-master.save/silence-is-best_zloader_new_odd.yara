rule zloader_new_bin
{
    meta:
        description = "zloader_new odd"
        author = "James_inthe_box"
        reference = "3e39f52e05238299ed622b996be05792b025d18bc56c878d772ee9002fef1015"
        date = "2021/08"
        maltype = "zloader_new naughty"

    strings:
        $string1 = "servername="
        $string2 = "account_login="
        $string3 = "curl_easy_"
        $string4 = "img0.jpg" wide
        $string5 = ".tmp"
        $string6 = "JavaXAdodb"

    condition:
        uint16(0) == 0x5A4D and all of ($string*) and filesize < 100KB
}

rule zloader_new_mem
{
    meta:
        description = "zloader_new odd"
        author = "James_inthe_box"
        reference = "3e39f52e05238299ed622b996be05792b025d18bc56c878d772ee9002fef1015"
        date = "2021/08"
        maltype = "zloader_new naughty"

    strings:
        $string1 = "servername="
        $string2 = "account_login="
        $string3 = "curl_easy_"
        $string4 = "img0.jpg" wide
        $string5 = ".tmp"
        $string6 = "JavaXAdodb"

    condition:
        all of ($string*) and filesize > 100KB
}