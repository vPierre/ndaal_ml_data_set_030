/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./abhinavbom/APT.yara"
include "./abhinavbom/Banbra-banker.yara"
include "./abhinavbom/Duqu2-0.yara"
include "./abhinavbom/Wannacry.yara"
include "./abhinavbom/XMLshell.yara"
include "./abhinavbom/ghostRAT.yara"
include "./abhinavbom/pos_malwares.yara"
include "./abhinavbom/virustotal-rules.yara"
include "./abhinavbom/vm-detect.yara"
