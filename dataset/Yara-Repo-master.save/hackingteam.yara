import "hash"

rule Sanesecurity_MalwareHash_HackingTeam_1
{
    condition:
        hash.md5(0, filesize) == "ceee22f4dc00e899d602ba6c93f9d40fe4bf22453031fab36b82029cbd8778c3"
}
rule Sanesecurity_MalwareHash_HackingTeam_2
{
    condition:
        hash.md5(0, filesize) == "ea1b1f409b29a5ccd157703599f875854bb679d06c6080984c6c1fe45ea9a9b4"
}
rule Sanesecurity_MalwareHash_HackingTeam_3
{
    condition:
        hash.md5(0, filesize) == "36ddc07510825fc7bbd6ac9fcda6f50504cb5a18381a71603af9bec2ea2b032d"
}
rule Sanesecurity_MalwareHash_HackingTeam_4
{
    condition:
        hash.md5(0, filesize) == "5b78a123bc37317ab187737377a34cb4d2ce2f85fc4313b4e76fb7bd1f7e8fab"
}
rule Sanesecurity_MalwareHash_HackingTeam_5
{
    condition:
        hash.md5(0, filesize) == "672318ef7c73936e562a95006e8b29f414f85741e92d937e7c62ef0017ed9e9a"
}
rule Sanesecurity_MalwareHash_HackingTeam_6
{
    condition:
        hash.md5(0, filesize) == "29028c75bea7874b881309287c94ac532de8df0549be9683cd72e421a6229573"
}
rule Sanesecurity_MalwareHash_HackingTeam_7
{
    condition:
        hash.md5(0, filesize) == "69f60a94b1014f1ef59c7d1cc0df925ef255734ac402e7d2525e73e1efd83905"
}
rule Sanesecurity_MalwareHash_HackingTeam_8
{
    condition:
        hash.md5(0, filesize) == "bdd1369c4ce15a17bd35323630d08f169e2672b80bbf0a885deefaa27a8f3171"
}
rule Sanesecurity_MalwareHash_HackingTeam_9
{
    condition:
        hash.md5(0, filesize) == "965c44e7f2598f606c6c965ecfcce722c4c77f6753feea62ac4d5674b6543dff"
}
rule Sanesecurity_MalwareHash_HackingTeam_10
{
    condition:
        hash.md5(0, filesize) == "9fa8ab680164bf0e2311d25e7a16870276a86ce719e3474f9027d6101a289329"
}
rule Sanesecurity_MalwareHash_HackingTeam_11
{
    condition:
        hash.md5(0, filesize) == "ccba6024b2d7c2bf191a47dcde16067f5f7c29297da7f1fbcd6535e555f4ca79"
}
rule Sanesecurity_MalwareHash_HackingTeam_12
{
    condition:
        hash.md5(0, filesize) == "10838fd5199ff78afd2bc7468e3f069b72a5230f181e790eddaa91a5b65fee2d"
}
rule Sanesecurity_MalwareHash_HackingTeam_13
{
    condition:
        hash.md5(0, filesize) == "e8d00b0f9194b4a0d8ea43bf994c4df6b41a87e78f5fa7531c60e4ab1ce2b74d"
}
rule Sanesecurity_MalwareHash_HackingTeam_14
{
    condition:
        hash.md5(0, filesize) == "b4936b372f772de7faf2e0e2d8861df63ce7e24002eab3d7953820127be48cfd"
}
rule Sanesecurity_MalwareHash_HackingTeam_15
{
    condition:
        hash.md5(0, filesize) == "595c3a71f7ca4a7e345823e23b5525c92bc58d1765d1acf7eb28253b677d4eb7"
}
rule Sanesecurity_MalwareHash_HackingTeam_16
{
    condition:
        hash.md5(0, filesize) == "72a699181c868f7dc4a6bb6c59f5cf96ee0295c3292d2dcacfd6d0f3cd0472dc"
}
rule Sanesecurity_MalwareHash_HackingTeam_17
{
    condition:
        hash.md5(0, filesize) == "f3e2fd54aa573051bcd36d4cc9e05ba11223d408f90b7fe00b31fa7103b04ae6"
}
rule Sanesecurity_MalwareHash_HackingTeam_18
{
    condition:
        hash.md5(0, filesize) == "c38b1a8b3d2ecdb75ef34987f7c35082df3d319538e56fb74b0a0702b81555aa"
}
rule Sanesecurity_MalwareHash_HackingTeam_19
{
    condition:
        hash.md5(0, filesize) == "9b50885ceb5624597d130a5f66fbcd6295aacb127509a91c8e9b62304fe60668"
}
rule Sanesecurity_MalwareHash_HackingTeam_20
{
    condition:
        hash.md5(0, filesize) == "61c508a04dce3286fdb86720d4c0b7a1a94e4a330279bb8c507080451715d0e9"
}
rule Sanesecurity_MalwareHash_HackingTeam_21
{
    condition:
        hash.md5(0, filesize) == "ac1df4b2e63101652c21b6a66728564600f97467b365b3bf1f594aee904948e7"
}
rule Sanesecurity_MalwareHash_HackingTeam_22
{
    condition:
        hash.md5(0, filesize) == "1bad86d89f5083a5fed48dcff10942e6e7603ebb0c3979083c6bc3e81d9018a0"
}
rule Sanesecurity_MalwareHash_HackingTeam_23
{
    condition:
        hash.md5(0, filesize) == "432390aa649448c6f91a0921a3fdce56d50e29a3fd95e068d3166b9533c26e83"
}
rule Sanesecurity_MalwareHash_HackingTeam_24
{
    condition:
        hash.md5(0, filesize) == "bd92bd94e2a8ada950da4435dff7f9a4abc7c010c2506350531738df9951287e"
}
rule Sanesecurity_MalwareHash_HackingTeam_25
{
    condition:
        hash.md5(0, filesize) == "7e53bdb5aeb993a61cf5361143d479cac76c4ccde2f30031fff3d7206bda77c2"
}
rule Sanesecurity_MalwareHash_HackingTeam_26
{
    condition:
        hash.md5(0, filesize) == "685b100b20c59e275fbbc559bc51495014278754fcdddfe02e3c3a2bd42bc633"
}
rule Sanesecurity_MalwareHash_HackingTeam_27
{
    condition:
        hash.md5(0, filesize) == "4a908d8ddf8ebe028a8214c08fd25dbac609c9a7ded8543f6b6bd03fe576f518"
}
rule Sanesecurity_MalwareHash_HackingTeam_28
{
    condition:
        hash.md5(0, filesize) == "9ad9519d7591b9a4ac1eac809c43becbc88db098d5df785db8a143430f9f5362"
}
rule Sanesecurity_MalwareHash_HackingTeam_29
{
    condition:
        hash.md5(0, filesize) == "2c3aa8359d1af7c2ff5d4c1ed5bffb6f707ee65006fcd2df354a7496b35bbc6a"
}
rule Sanesecurity_MalwareHash_HackingTeam_30
{
    condition:
        hash.md5(0, filesize) == "ccdf1fb0dcba91cfcea5ef907025674fe965580c8dd10495d6aaa9c9829cc1d1"
}
rule Sanesecurity_MalwareHash_HackingTeam_31
{
    condition:
        hash.md5(0, filesize) == "98cf937ac44843a3bc37130da20425ce7dcb5563d0af2a7d34a87f3cce478175"
}
rule Sanesecurity_MalwareHash_HackingTeam_32
{
    condition:
        hash.md5(0, filesize) == "d7c9dac1541a534e55a04256e0310fe0229dbdc0253d6afc4fc13589220edd62"
}
rule Sanesecurity_MalwareHash_HackingTeam_33
{
    condition:
        hash.md5(0, filesize) == "beff505e53144a261411d18ed574a9c226c2bdd25a0b07bcdba742402ef2f333"
}
rule Sanesecurity_MalwareHash_HackingTeam_34
{
    condition:
        hash.md5(0, filesize) == "26e9816116de8b26ba0b1aa12fe4177dd1196ccdb9c93cbcd1b0593ce3f81e79"
}
rule Sanesecurity_MalwareHash_HackingTeam_35
{
    condition:
        hash.md5(0, filesize) == "d6ab38fa7f41cda2bf10da8ba418ef49b0a3e534e8ca83a75aa23be0a81df167"
}
rule Sanesecurity_MalwareHash_HackingTeam_36
{
    condition:
        hash.md5(0, filesize) == "dccfe5c8e66f729783e39f6c2aa87e27a9c18d0aae94379a1cb97ae1933b66bb"
}
rule Sanesecurity_MalwareHash_HackingTeam_37
{
    condition:
        hash.md5(0, filesize) == "6b151f19d8b7add5a48b1fbe6142b89b52cd780ac5b0009746985c950f44920d"
}
rule Sanesecurity_MalwareHash_HackingTeam_38
{
    condition:
        hash.md5(0, filesize) == "391bc486ad12d73ff3dab94c617638539226c7f5f68f38bdd71110bca12b79ad"
}
rule Sanesecurity_MalwareHash_HackingTeam_39
{
    condition:
        hash.md5(0, filesize) == "03f98deb24b3602e660c617927a991b02cf3186eef45b122b7ce183300003e22"
}
rule Sanesecurity_MalwareHash_HackingTeam_40
{
    condition:
        hash.md5(0, filesize) == "064284a97ed6e09e7de161aedec9103a9f88443bc666bf710f642269521ac957"
}
rule Sanesecurity_MalwareHash_HackingTeam_41
{
    condition:
        hash.md5(0, filesize) == "f08bdf8a16434d254e8137134a98253af192224e29e6ec99ba66d0c88408a7d5"
}
rule Sanesecurity_MalwareHash_HackingTeam_42
{
    condition:
        hash.md5(0, filesize) == "7270f53ad27695f77ed685914d9bcb52e81fd159705f3d0c539570d4d6026159"
}
rule Sanesecurity_MalwareHash_HackingTeam_43
{
    condition:
        hash.md5(0, filesize) == "c509fc170cb182b37462179079e16c76ff4b87a70d2f17ff60ba5b7f98eb8cd8"
}
rule Sanesecurity_MalwareHash_HackingTeam_44
{
    condition:
        hash.md5(0, filesize) == "ba51d2d00cc0ef67d435b0c368dd3e0b3ed8c12839ba95d45b06c9df5e7668be"
}
rule Sanesecurity_MalwareHash_HackingTeam_45
{
    condition:
        hash.md5(0, filesize) == "dca5bb8ef6d240c8812ea154a21c26140ca7fe898d1e95fcbef5b9556bdb4be1"
}
rule Sanesecurity_MalwareHash_HackingTeam_46
{
    condition:
        hash.md5(0, filesize) == "77fc2dea8a36575376595a9093cb1ce122fab4c4a8e4cb16f5ef461a620d7eb1"
}
rule Sanesecurity_MalwareHash_HackingTeam_47
{
    condition:
        hash.md5(0, filesize) == "237f517ce1f43f68d280a8eae96e561b170c15e5f5c750dcfde344480cb2f99b"
}
rule Sanesecurity_MalwareHash_HackingTeam_48
{
    condition:
        hash.md5(0, filesize) == "a858f9a5c92f9d56a6fb60e62a3a98455ff45f5acf0eedf0079df845635964ee"
}
rule Sanesecurity_MalwareHash_HackingTeam_49
{
    condition:
        hash.md5(0, filesize) == "dae6ec4e3567cb805af50a5d4a8adee42f0f0c703eb02af948fd64a99df04e37"
}
rule Sanesecurity_MalwareHash_HackingTeam_50
{
    condition:
        hash.md5(0, filesize) == "66bd82f5ac2c7d976d5ede73b3019509ae3df5d8fc54488d0014f3f41ad0a95f"
}
rule Sanesecurity_MalwareHash_HackingTeam_51
{
    condition:
        hash.md5(0, filesize) == "2378230b47e6b27f392466b3ebbe520557afe6623eaeaa171195d86c5483939c"
}
rule Sanesecurity_MalwareHash_HackingTeam_52
{
    condition:
        hash.md5(0, filesize) == "6662b7842108e2bb2ac2f01653180ea4fd2f36c2c464524e371229cfe9f52714"
}
rule Sanesecurity_MalwareHash_HackingTeam_53
{
    condition:
        hash.md5(0, filesize) == "50046bd74109e71d93890ea68723d7ff1f7dc447198017d0265a79ce0ad27492"
}
rule Sanesecurity_MalwareHash_HackingTeam_54
{
    condition:
        hash.md5(0, filesize) == "358dacef4eee34bf393c71bbdc7322ce956e105187978edf492cdb553cc48e27"
}
rule Sanesecurity_MalwareHash_HackingTeam_55
{
    condition:
        hash.md5(0, filesize) == "8759a2de00933711fd36dabfffd6fc2fe4d4d29761fb467636b60c164cddb44f"
}
rule Sanesecurity_MalwareHash_HackingTeam_56
{
    condition:
        hash.md5(0, filesize) == "70a6491c4cad22a50bd0f94c58d758ce49d83a687268ecfad9fa3daf79fac50c"
}
rule Sanesecurity_MalwareHash_HackingTeam_57
{
    condition:
        hash.md5(0, filesize) == "b81a3cc3ae37c5dab0b40a58752f88d429bc5c92296ebffcefe555ea99c86c30"
}
rule Sanesecurity_MalwareHash_HackingTeam_58
{
    condition:
        hash.md5(0, filesize) == "eb67397eb7e825addb295e32d81f45c34c1991f8c993238771f8e868d1d4e2a9"
}
rule Sanesecurity_MalwareHash_HackingTeam_59
{
    condition:
        hash.md5(0, filesize) == "c73081ac1b62e2ad3eef196727123eec6ea353f023176effa1ef89127fce62d1"
}
rule Sanesecurity_MalwareHash_HackingTeam_60
{
    condition:
        hash.md5(0, filesize) == "1ad0024b950a45f483a6c253daea6657c1223c8a1a16479b50ff42bc73aa5856"
}
rule Sanesecurity_MalwareHash_HackingTeam_61
{
    condition:
        hash.md5(0, filesize) == "073db35a232ba83451864bd4ae9ea8cf40f2c297dc69c8f7ab0773971de6eac3"
}
rule Sanesecurity_MalwareHash_HackingTeam_62
{
    condition:
        hash.md5(0, filesize) == "850f6f8e87ad6cb1ddae38241d932d610169e827020e1efe33ddfaddbdbe7a25"
}
rule Sanesecurity_MalwareHash_HackingTeam_63
{
    condition:
        hash.md5(0, filesize) == "5e2401aa54a1b6ae4704a1e906491a9457f90b81cb6c9ecc47cab9f552ad083f"
}
rule Sanesecurity_MalwareHash_HackingTeam_64
{
    condition:
        hash.md5(0, filesize) == "cd3cfd11255a87c7ed4eb93a6b44330cea03f89620bbaf2a3218abdbdf1afcc0"
}
rule Sanesecurity_MalwareHash_HackingTeam_65
{
    condition:
        hash.md5(0, filesize) == "3f312838b6d5268edd15d44a8de3b72706d0dbbf6e0b44a1fe8865ac3a00f34d"
}
rule Sanesecurity_MalwareHash_HackingTeam_66
{
    condition:
        hash.md5(0, filesize) == "4575543e49b4df9f108e8f6b06a1e7b99e1ed6b369256111b3d40f9f2fbef490"
}
rule Sanesecurity_MalwareHash_HackingTeam_67
{
    condition:
        hash.md5(0, filesize) == "96fd36fdba57b8426d154ad429194ec669b6e0ca3d61ebf9b0d00cacc29b450a"
}
rule Sanesecurity_MalwareHash_HackingTeam_68
{
    condition:
        hash.md5(0, filesize) == "0252a86ad1f9df2940f17dd2e2297eb1e7fb16d51b86be91cd39a7e1571ead61"
}
rule Sanesecurity_MalwareHash_HackingTeam_69
{
    condition:
        hash.md5(0, filesize) == "40d7945f8c10195efb399876bd704a6021c8b89e8697fc43eb4d38b3f6bd3a05"
}
rule Sanesecurity_MalwareHash_HackingTeam_70
{
    condition:
        hash.md5(0, filesize) == "7a96db1f883c1f54e437cb6682364589f78e207c1aefb648c87698340fbdb48e"
}
rule Sanesecurity_MalwareHash_HackingTeam_71
{
    condition:
        hash.md5(0, filesize) == "07d3b91c92926c723e1796a0d1b9f823dbcb46ef5d62bd0f21235a42e20c3b6a"
}
rule Sanesecurity_MalwareHash_HackingTeam_72
{
    condition:
        hash.md5(0, filesize) == "3a6ff8835d66d5aa74b0b889b2f7066dfdbcb86d8a2f486177ae0d9a1b2d07ab"
}
rule Sanesecurity_MalwareHash_HackingTeam_73
{
    condition:
        hash.md5(0, filesize) == "aff97d9d8b6ac8942f63c006d258857c06aca19ab141cb0a264061898dfb73dd"
}
rule Sanesecurity_MalwareHash_HackingTeam_74
{
    condition:
        hash.md5(0, filesize) == "756aa91b2b4ed0e6303cd1c3553c547742063afb0e588498966bc2d5dcce5215"
}
rule Sanesecurity_MalwareHash_HackingTeam_75
{
    condition:
        hash.md5(0, filesize) == "518675e2c2d8f2ebc53483893e3319efcdb86d009740f9b1a79c1c919ace38a9"
}
rule Sanesecurity_MalwareHash_HackingTeam_76
{
    condition:
        hash.md5(0, filesize) == "f535f9b4888d549887ea06aec39c086eea0bbc9262773f386696c20e1e7a73b4"
}
rule Sanesecurity_MalwareHash_HackingTeam_77
{
    condition:
        hash.md5(0, filesize) == "d27cfafb556c61ae71a408cb60098bd4e785fc06ed43138beb0521c63db29112"
}
rule Sanesecurity_MalwareHash_HackingTeam_78
{
    condition:
        hash.md5(0, filesize) == "e1df8e8c2158cf18a48e5872f9daf0d3d11a883e6fa2571bc99a0ed1f398ed7f"
}
rule Sanesecurity_MalwareHash_HackingTeam_79
{
    condition:
        hash.md5(0, filesize) == "c6ba837c2857e6533a8150e216417c00bb6466772afb104ab80f720f6d7b864d"
}
rule Sanesecurity_MalwareHash_HackingTeam_80
{
    condition:
        hash.md5(0, filesize) == "9215fc637e018d19e93e8529669289d03b72b2c1662a6f2dcdcfbf24ce7ff9bf"
}
rule Sanesecurity_MalwareHash_HackingTeam_81
{
    condition:
        hash.md5(0, filesize) == "f408b207447dbde3a97de5d05fd3225580d6dbcf5cb3cbd03a45b07327e57ac0"
}
rule Sanesecurity_MalwareHash_HackingTeam_82
{
    condition:
        hash.md5(0, filesize) == "a4b74c6d42d863e290ff361b0382bf19e66eb9f9ec9861d899620102814f2c29"
}
rule Sanesecurity_MalwareHash_HackingTeam_83
{
    condition:
        hash.md5(0, filesize) == "38907f006018614e57bca99b3bd32d4f3cc6d4ee11adbd4549523d555aeaaddb"
}
rule Sanesecurity_MalwareHash_HackingTeam_84
{
    condition:
        hash.md5(0, filesize) == "a70e9b407740c30d87bcf4495ab74816bf732d25927de50a424a15db0fceab2c"
}
rule Sanesecurity_MalwareHash_HackingTeam_85
{
    condition:
        hash.md5(0, filesize) == "217854e1fdffe8b6917a7fa6e011fbe709837b5ccc6ab8c3323c29ffb7dfa00b"
}
rule Sanesecurity_MalwareHash_HackingTeam_86
{
    condition:
        hash.md5(0, filesize) == "4560b4e511ea8332861d58812a122f1463e84b40ce038048bcf40539a15851fb"
}
rule Sanesecurity_MalwareHash_HackingTeam_87
{
    condition:
        hash.md5(0, filesize) == "a49150dfe0f147e7bbbad2deec904ff1f453809f9f0d5f8809a988abd625ff15"
}
rule Sanesecurity_MalwareHash_HackingTeam_88
{
    condition:
        hash.md5(0, filesize) == "c5a6f33d0fd578d6b493126b190091278ac313712328e527876ca5c412e4b5d6"
}
rule Sanesecurity_MalwareHash_HackingTeam_89
{
    condition:
        hash.md5(0, filesize) == "d6545cda5027c87a44e2a616ef993cdad1904e2eb6d061d6ef8bd485516d0da3"
}
rule Sanesecurity_MalwareHash_HackingTeam_90
{
    condition:
        hash.md5(0, filesize) == "906e68614b03f886654c83d21fb9209113805f9a766c962535a85631ecb29945"
}
rule Sanesecurity_MalwareHash_HackingTeam_91
{
    condition:
        hash.md5(0, filesize) == "1a199cc53ac508ded68c573b9acd6b646ff75623264802281bcb9f0eb6fab1d9"
}
rule Sanesecurity_MalwareHash_HackingTeam_92
{
    condition:
        hash.md5(0, filesize) == "28f1ac0a1b2c6413947a742c92d3a9fe6e81d0bfcce6a686bef822943a19f253"
}
rule Sanesecurity_MalwareHash_HackingTeam_93
{
    condition:
        hash.md5(0, filesize) == "08348141224e6abe0a2a3433c9f266216bffb89e98a1f1bcaea05680c7be0225"
}
rule Sanesecurity_MalwareHash_HackingTeam_94
{
    condition:
        hash.md5(0, filesize) == "809e52604d171c6e37ff90c8d948818f5690da1fd64c98443f04c00b25e51616"
}
rule Sanesecurity_MalwareHash_HackingTeam_95
{
    condition:
        hash.md5(0, filesize) == "91d81e6580ea634b8382c6d3c58d1b14102d2cb88d2b7277cb383ac60922c911"
}
rule Sanesecurity_MalwareHash_HackingTeam_96
{
    condition:
        hash.md5(0, filesize) == "b142218fdad7498708b2369ffa19a02f02c04f08a554b9de5ea28205c776dd51"
}
rule Sanesecurity_MalwareHash_HackingTeam_97
{
    condition:
        hash.md5(0, filesize) == "379ee45ed055674122436d6eba89680bd7241f4b4d18ecf31256b683496475da"
}
rule Sanesecurity_MalwareHash_HackingTeam_98
{
    condition:
        hash.md5(0, filesize) == "42fd2064ef61aad7a41b170b46f711d336594c39b16b89d8d4c5a2caeefa2e6c"
}
rule Sanesecurity_MalwareHash_HackingTeam_99
{
    condition:
        hash.md5(0, filesize) == "d9f5ed16c9a7354dd6466a63688cfc22b964edacf861efc8d7b2dbf1e5604616"
}
rule Sanesecurity_MalwareHash_HackingTeam_100
{
    condition:
        hash.md5(0, filesize) == "2fc3cfa846a4f00ecb863f4e7187d66df1583b42f6a9ce53f4ec170621ff68b7"
}
rule Sanesecurity_MalwareHash_HackingTeam_101
{
    condition:
        hash.md5(0, filesize) == "efd46d292b33426fcee24d64acc93e89d6665c393e7ba27781b2c26093205e2a"
}
rule Sanesecurity_MalwareHash_HackingTeam_102
{
    condition:
        hash.md5(0, filesize) == "b1aa9efac39092ef6a2c8ad3a0b6f32e40d6d7845c980baafec84c2bea8bcc4b"
}
rule Sanesecurity_MalwareHash_HackingTeam_103
{
    condition:
        hash.md5(0, filesize) == "fcffc764e194b64af52d9774c57283fbfe2d1d9a1d685260eebf0d13cae41640"
}
rule Sanesecurity_MalwareHash_HackingTeam_104
{
    condition:
        hash.md5(0, filesize) == "ad2156fecde2f8019f345f0248182cd41c9127e86fd50db9e1c7ed5ea433e914"
}
rule Sanesecurity_MalwareHash_HackingTeam_105
{
    condition:
        hash.md5(0, filesize) == "06f174c53c80c0e1218a509e58df35de675f79780ec9baad22e9ad16ef54b978"
}
rule Sanesecurity_MalwareHash_HackingTeam_106
{
    condition:
        hash.md5(0, filesize) == "69f889fd22e35d905b3aef1d4d180f920384aa203cd0a70be7adb986ad3ca865"
}
rule Sanesecurity_MalwareHash_HackingTeam_107
{
    condition:
        hash.md5(0, filesize) == "171c154d0b97cd5d257e234a0498079032538aa9e43e561b851e8598f8c38208"
}
rule Sanesecurity_MalwareHash_HackingTeam_108
{
    condition:
        hash.md5(0, filesize) == "5df94400592a7f1b82c0068de4d4ea4428718f13624baf9a10254c14de5bec22"
}
rule Sanesecurity_MalwareHash_HackingTeam_109
{
    condition:
        hash.md5(0, filesize) == "1315928ddb7934b7849cd0925b7092de2329f158a3c027886c9e63065546cb3d"
}
rule Sanesecurity_MalwareHash_HackingTeam_110
{
    condition:
        hash.md5(0, filesize) == "6c430547f871f946a8829c36a8c24c3b45a272fc55b898de22e3405f9c9b9936"
}
rule Sanesecurity_MalwareHash_HackingTeam_111
{
    condition:
        hash.md5(0, filesize) == "a501c69d9440b8a6e244001eaed72d77ca57aad46c557d1d375e05118a3f32ad"
}
rule Sanesecurity_MalwareHash_HackingTeam_112
{
    condition:
        hash.md5(0, filesize) == "3264e84005a3fcb4a6f03b760d9cec3296b46bdb7a0966c5a4bb9b323511d544"
}
rule Sanesecurity_MalwareHash_HackingTeam_113
{
    condition:
        hash.md5(0, filesize) == "a19791f0d1a3c5926e10dfffb70352a30bb152ddcf1fb9b16dff7750e534a8e1"
}
rule Sanesecurity_MalwareHash_HackingTeam_114
{
    condition:
        hash.md5(0, filesize) == "cfa8f6a8b4e71227cf8e126671132fe2f830d66689d8ce429d0ac1230b73d90d"
}
rule Sanesecurity_MalwareHash_HackingTeam_115
{
    condition:
        hash.md5(0, filesize) == "141f9837abc99617c18c3020569625455b408f15adbdf3d596cb84a70e532b1e"
}
rule Sanesecurity_MalwareHash_HackingTeam_116
{
    condition:
        hash.md5(0, filesize) == "ba584105f3c655c06fd60945c67ec46bc6e19542c6aeedeaf18c6fed267c6637"
}
rule Sanesecurity_MalwareHash_HackingTeam_117
{
    condition:
        hash.md5(0, filesize) == "038cf884c9f6cd604f4cba084c279736b80aa8ef89e42836b7da3ab570a9884a"
}
rule Sanesecurity_MalwareHash_HackingTeam_118
{
    condition:
        hash.md5(0, filesize) == "73f175173a73c620dfe23101603746df844aedb5cf476b993892016310911be4"
}
rule Sanesecurity_MalwareHash_HackingTeam_119
{
    condition:
        hash.md5(0, filesize) == "9994883bdcbad8f564d8465797c4cbaccff4a1fc2a294ae8b1f79c0af042caef"
}
rule Sanesecurity_MalwareHash_HackingTeam_120
{
    condition:
        hash.md5(0, filesize) == "faf368a9fff809392098ac777d536f8ee49943cf7b3ef89a6bdec7409229d5d3"
}
rule Sanesecurity_MalwareHash_HackingTeam_121
{
    condition:
        hash.md5(0, filesize) == "04c2bbd114a56e7a0a4263584261e03f59de4a2d71a2057dbe580abf424a50bf"
}
rule Sanesecurity_MalwareHash_HackingTeam_122
{
    condition:
        hash.md5(0, filesize) == "2a1526deda30c20c6c6f8253c41b14f3086c6ae2ffc51a8bbed1d699fd94883e"
}
rule Sanesecurity_MalwareHash_HackingTeam_123
{
    condition:
        hash.md5(0, filesize) == "6ac505a01b767c2e8428212d76e23b88eaed0d87c430174b6d2aad5655467f3d"
}
rule Sanesecurity_MalwareHash_HackingTeam_124
{
    condition:
        hash.md5(0, filesize) == "6f2f72c5d229c54d91832427671be9c17e5189727f838960d8cb3fc66feab2c6"
}
rule Sanesecurity_MalwareHash_HackingTeam_125
{
    condition:
        hash.md5(0, filesize) == "71a3fb73a09b71c19b08a29142028b938f62b7e496b2a1f1dd972950be453c6b"
}
rule Sanesecurity_MalwareHash_HackingTeam_126
{
    condition:
        hash.md5(0, filesize) == "8e3dc9e7c5ad4e8071e5aa89f2044050979fcb78b9fecceef1097b4e989def0c"
}
rule Sanesecurity_MalwareHash_HackingTeam_127
{
    condition:
        hash.md5(0, filesize) == "1976b8fe6e0f785fe9461e8993ed9cdcb3e256d3246ed3eeb9081b37e4fcd788"
}
rule Sanesecurity_MalwareHash_HackingTeam_128
{
    condition:
        hash.md5(0, filesize) == "136f6ce570d065d8783cffe2826b250d753ab24088a263cf8f6048415322bd94"
}
rule Sanesecurity_MalwareHash_HackingTeam_129
{
    condition:
        hash.md5(0, filesize) == "ec630afdc073a0ec45b1f35f3bd38ce7a8a098ae462db9ec44483aec0eea150d"
}
rule Sanesecurity_MalwareHash_HackingTeam_130
{
    condition:
        hash.md5(0, filesize) == "c0aa9c780bd8bf004ba10dd826c884779ae06a51493e8f67b37c1ca591b9bd0d"
}
rule Sanesecurity_MalwareHash_HackingTeam_131
{
    condition:
        hash.md5(0, filesize) == "d9d82cb2fc41f82da23959c5e3c5b9fc6be09af37cddad569e858c3d5ff7748d"
}
rule Sanesecurity_MalwareHash_HackingTeam_132
{
    condition:
        hash.md5(0, filesize) == "ed490997faa41937f89a11a2b262654b0838bb1ff9aae64f15dd89fe64ed0857"
}
rule Sanesecurity_MalwareHash_HackingTeam_133
{
    condition:
        hash.md5(0, filesize) == "8be2d533e6520780289bff2d4e9cf6715ef9ef86727395838b7e656a033b164e"
}
rule Sanesecurity_MalwareHash_HackingTeam_134
{
    condition:
        hash.md5(0, filesize) == "9e75049ed6197d1990d139bf47b7727db9d7775c683c26461f7c9b737ad1d80e"
}
rule Sanesecurity_MalwareHash_HackingTeam_135
{
    condition:
        hash.md5(0, filesize) == "af84a933fa542dc7ab7457ec6028062ea2a188d720bd702f379cfb3ca42d773b"
}
rule Sanesecurity_MalwareHash_HackingTeam_136
{
    condition:
        hash.md5(0, filesize) == "727ae1bc55e47801f09ecc1a437a6fbff74a73e73226609d5d28a7d0777af714"
}
rule Sanesecurity_MalwareHash_HackingTeam_137
{
    condition:
        hash.md5(0, filesize) == "ab107d32d8426bb0d5631d0439d1daaf0d714ea928dd5c9403264c04b269803a"
}
rule Sanesecurity_MalwareHash_HackingTeam_138
{
    condition:
        hash.md5(0, filesize) == "7595828d47d8804463d53dc3b8319a70693c7ad8d5ed17ddc5b679b8d45bb540"
}
rule Sanesecurity_MalwareHash_HackingTeam_139
{
    condition:
        hash.md5(0, filesize) == "3e9eaf55a741f9d7f7b617b5ef03acba756014fc30df8d55b76a0ca819d6da7b"
}
rule Sanesecurity_MalwareHash_HackingTeam_140
{
    condition:
        hash.md5(0, filesize) == "2542b6d23a75ca3b93f9a9661b66fed3c9ae7170287b8fa7df33f84c32b8e559"
}
rule Sanesecurity_MalwareHash_HackingTeam_141
{
    condition:
        hash.md5(0, filesize) == "552e86bc584447ba4d0ee5e559c2fee71bde17dbc0a54c9848a7fbb1b9b0a8d2"
}
rule Sanesecurity_MalwareHash_HackingTeam_142
{
    condition:
        hash.md5(0, filesize) == "7b9107555d4c5f48883fab32f93696aceaaaae248ac348f4f733c5bcdb1d1b1e"
}
rule Sanesecurity_MalwareHash_HackingTeam_143
{
    condition:
        hash.md5(0, filesize) == "a27e5635bd93e3d13c3ec2b5244f9f0d937b94467177710624201656073fd1e3"
}
rule Sanesecurity_MalwareHash_HackingTeam_144
{
    condition:
        hash.md5(0, filesize) == "f7123aff38787916d435174ec96584925824ee8d3b749c92f72f6256b9b0d944"
}
rule Sanesecurity_MalwareHash_HackingTeam_145
{
    condition:
        hash.md5(0, filesize) == "e57eb6c4a7a6937bf15cc9d46f9739692a95e424d2ae2cb3bc7470fd16261ee8"
}
rule Sanesecurity_MalwareHash_HackingTeam_146
{
    condition:
        hash.md5(0, filesize) == "a6db53c510b555685ca98da76a3ac6cd14f401bc2b824b88822462fbb446b1f5"
}
rule Sanesecurity_MalwareHash_HackingTeam_147
{
    condition:
        hash.md5(0, filesize) == "add9262e969e3ced8e003df3516a46c543940bd7501d8eadac192d65ef1483db"
}
rule Sanesecurity_MalwareHash_HackingTeam_148
{
    condition:
        hash.md5(0, filesize) == "712141d42f31d0ff651d317ed08144ac04b13e432a0b482902364679e9ccff63"
}
rule Sanesecurity_MalwareHash_HackingTeam_149
{
    condition:
        hash.md5(0, filesize) == "45d3eb044f665e51b91814093f4f8ad97bc022de18c41bd159ae8feb9668e64d"
}
rule Sanesecurity_MalwareHash_HackingTeam_150
{
    condition:
        hash.md5(0, filesize) == "a09dc80da1017211b61c27326387880fa532fe6509a48c3198b27b3587e7a54b"
}
rule Sanesecurity_MalwareHash_HackingTeam_151
{
    condition:
        hash.md5(0, filesize) == "305adc551b40d2213cf820f4f0f9f5c2d93da9127b245b483dde68dc13179eab"
}
rule Sanesecurity_MalwareHash_HackingTeam_152
{
    condition:
        hash.md5(0, filesize) == "695d9cdba88682d96e3302119a96f75c1ffdf55b7d1b2d74192ab0a48fb8f292"
}
rule Sanesecurity_MalwareHash_HackingTeam_153
{
    condition:
        hash.md5(0, filesize) == "52c2e31d5f8cb5d2cf8e49998801182fa47e3b0dd2825229ff7aa2888910dc4e"
}
rule Sanesecurity_MalwareHash_HackingTeam_154
{
    condition:
        hash.md5(0, filesize) == "bc2a62748683514ea9f763afe73cf14934d70dc56eaebe28e3964c2a5d45eea7"
}
rule Sanesecurity_MalwareHash_HackingTeam_155
{
    condition:
        hash.md5(0, filesize) == "9f7c13bb9e406353e52ded871c8cc83038c5fb5ed21177544b8653a4a760ce58"
}
rule Sanesecurity_MalwareHash_HackingTeam_156
{
    condition:
        hash.md5(0, filesize) == "87106fac5bbb60b118327c130aeeae52c890795056cac5550139435d4223657f"
}
rule Sanesecurity_MalwareHash_HackingTeam_157
{
    condition:
        hash.md5(0, filesize) == "6cdcce832d9f7d404dbe0f53e9405ec0f82d65a00720a996be1f0adf3331bfd0"
}
rule Sanesecurity_MalwareHash_HackingTeam_158
{
    condition:
        hash.md5(0, filesize) == "2c2a1044acd7d47ade2e74b06fe366fdc1c363297b5292c8a362f34018ae100b"
}
rule Sanesecurity_MalwareHash_HackingTeam_159
{
    condition:
        hash.md5(0, filesize) == "6e6f6e40a2716d11425a88b560e80fefd1a16d81ddee9663ff42ab82ea3a35bd"
}
rule Sanesecurity_MalwareHash_HackingTeam_160
{
    condition:
        hash.md5(0, filesize) == "07ed3d9bd82a3b490f33f36117af3ad02152d51e9c2470eb0089dab1305368f1"
}
rule Sanesecurity_MalwareHash_HackingTeam_161
{
    condition:
        hash.md5(0, filesize) == "61fe96a5118b531e7f1659085bcd61084354961fb557588bae3619665a8dc681"
}
rule Sanesecurity_MalwareHash_HackingTeam_162
{
    condition:
        hash.md5(0, filesize) == "9261693b67b6e379ad0e57598602712b8508998c0cb012ca23139212ae0009a1"
}
rule Sanesecurity_MalwareHash_HackingTeam_163
{
    condition:
        hash.md5(0, filesize) == "7279dfe295bfb075bff6a856097491fbd4c932970bb654c969a995322f0d03db"
}
rule Sanesecurity_MalwareHash_HackingTeam_164
{
    condition:
        hash.md5(0, filesize) == "3ea8909c7e92d10a39ba08b002b489e718d77f12754e1bac8e69d62891ac8417"
}
rule Sanesecurity_MalwareHash_HackingTeam_165
{
    condition:
        hash.md5(0, filesize) == "9a1dc317baac5b31e8f9498c979e623db6e57f34aaea6dac923853cec1a30397"
}
rule Sanesecurity_MalwareHash_HackingTeam_166
{
    condition:
        hash.md5(0, filesize) == "92af7c751d9353ceb1b449bb6ea1a29c7a68a5bd2344759ad1c974ac5c63dee6"
}
rule Sanesecurity_MalwareHash_HackingTeam_167
{
    condition:
        hash.md5(0, filesize) == "da07eca4cd4cccc81d9418fcc796d28bc95756c8d6d4ad9503effd12b6c0aef7"
}
rule Sanesecurity_MalwareHash_HackingTeam_168
{
    condition:
        hash.md5(0, filesize) == "a4afe60c024a34ae16dfbde1224550224ab3195f3d5dfe35c50ebd6a12fd4170"
}
rule Sanesecurity_MalwareHash_HackingTeam_169
{
    condition:
        hash.md5(0, filesize) == "e2f8c5f8c3ab687b91dd28081fec71e0bb9f70066237768e7020fd992c80f2d5"
}
rule Sanesecurity_MalwareHash_HackingTeam_170
{
    condition:
        hash.md5(0, filesize) == "d31c5d91556d0dc52ddc77d70678441f6f7a647eaaf8e1438fdc5cf3160fb935"
}
rule Sanesecurity_MalwareHash_HackingTeam_171
{
    condition:
        hash.md5(0, filesize) == "40a10420b9d49f87527bc0396b19ec29e55e9109e80b52456891243791671c1c"
}
rule Sanesecurity_MalwareHash_HackingTeam_172
{
    condition:
        hash.md5(0, filesize) == "dac6abd5ba0865b7983cff40f7a13d9cde89fed3c5b81c2b785e884f9ccdf28c"
}
rule Sanesecurity_MalwareHash_HackingTeam_173
{
    condition:
        hash.md5(0, filesize) == "7561ace6f04ca6d023d7eba0c8cd49b2515baa71a40926f625538e41e21f641f"
}
rule Sanesecurity_MalwareHash_HackingTeam_174
{
    condition:
        hash.md5(0, filesize) == "32599e86cb3bc9e1f91ff630fa41cd140354a21ac47bdb48082fbb8fba900f53"
}
rule Sanesecurity_MalwareHash_HackingTeam_175
{
    condition:
        hash.md5(0, filesize) == "b7df931aa020195726002b235740bc844fc4b105920d4a139ca6b5a069e43575"
}
rule Sanesecurity_MalwareHash_HackingTeam_176
{
    condition:
        hash.md5(0, filesize) == "8cf6258d002326a03cf4cd70d97837b02a1ba5f3451e88fa354947180fb93eaa"
}
rule Sanesecurity_MalwareHash_HackingTeam_177
{
    condition:
        hash.md5(0, filesize) == "72ec760b698dc19693eaa846b2cc21ebceec4ee122feb30cb0802a9920af9898"
}
rule Sanesecurity_MalwareHash_HackingTeam_178
{
    condition:
        hash.md5(0, filesize) == "b6d736a68360253a94cc89bafbfa3141c382079d3e74346b12251da26149d1c3"
}
rule Sanesecurity_MalwareHash_HackingTeam_179
{
    condition:
        hash.md5(0, filesize) == "b924993e72cc8fd0b505e95cea5e8b09d17d2a15c9d9ebc2b0c32843edcd40ee"
}
rule Sanesecurity_MalwareHash_HackingTeam_180
{
    condition:
        hash.md5(0, filesize) == "4d9ced2ee7d979055d33564cfa5a67773e34f3e51d615f162003311c76f51bdb"
}
rule Sanesecurity_MalwareHash_HackingTeam_181
{
    condition:
        hash.md5(0, filesize) == "639152dcce89b669fa00213d853425bee35f8b79970a663492d24ce29421fb75"
}
rule Sanesecurity_MalwareHash_HackingTeam_182
{
    condition:
        hash.md5(0, filesize) == "bf2f9d19521cae12bf25a4108418f6c234af6cad2d7a6482323a12a2da13ebd6"
}
rule Sanesecurity_MalwareHash_HackingTeam_183
{
    condition:
        hash.md5(0, filesize) == "0dd0325e09c0ba103aedc9e899192204ab29f4a0d35a7e53e5c800d9284a37e8"
}
rule Sanesecurity_MalwareHash_HackingTeam_184
{
    condition:
        hash.md5(0, filesize) == "8bba59ce301d510bc3b24c941841ee4a8b0858d37e31c9d59193b78e7da81d9a"
}
rule Sanesecurity_MalwareHash_HackingTeam_185
{
    condition:
        hash.md5(0, filesize) == "91b0995ee522a6a01fe112dd6cdc21f2cd57b26ac84d8e3065f124ccb93c5eb4"
}
rule Sanesecurity_MalwareHash_HackingTeam_186
{
    condition:
        hash.md5(0, filesize) == "ce5d792faaca61d7bb63367f8772f492ee963f054bc03e61b4fae774c3a3c343"
}
rule Sanesecurity_MalwareHash_HackingTeam_187
{
    condition:
        hash.md5(0, filesize) == "a23b5fc7d309b982f9dafc712b6a95c1cfce6102f86a7dc3f3013819638081a9"
}
rule Sanesecurity_MalwareHash_HackingTeam_188
{
    condition:
        hash.md5(0, filesize) == "ab4de0951de38c475d846da1da8336b97e886b6dbd694332f3624ee5595186fe"
}
rule Sanesecurity_MalwareHash_HackingTeam_189
{
    condition:
        hash.md5(0, filesize) == "d5b3cc429c8a6fba074d9b1e2963273ac13cead47f63dbbb97e640b74e407134"
}
rule Sanesecurity_MalwareHash_HackingTeam_190
{
    condition:
        hash.md5(0, filesize) == "2ef643a29808aa6dedeb69165d8682d5a58a95aa68bce856783a2b8dc2d71087"
}
rule Sanesecurity_MalwareHash_HackingTeam_191
{
    condition:
        hash.md5(0, filesize) == "60f4e50985afa8c0b2437c78467fc11784416791cd8cacdb37542a3e14d79871"
}
rule Sanesecurity_MalwareHash_HackingTeam_192
{
    condition:
        hash.md5(0, filesize) == "1a178c22b5e9a7e99c0c733ff9d8452b22a3418b3c137687c8407c309e79a714"
}
rule Sanesecurity_MalwareHash_HackingTeam_193
{
    condition:
        hash.md5(0, filesize) == "d5d23fbad723009a6a6364ef28153ffc95190e269cf3749c3cf28128d4c89be1"
}
rule Sanesecurity_MalwareHash_HackingTeam_194
{
    condition:
        hash.md5(0, filesize) == "cfa438d2d1426c983134203329e30ac92a4c5f6170e1687dc287ecf67ef53404"
}
rule Sanesecurity_MalwareHash_HackingTeam_195
{
    condition:
        hash.md5(0, filesize) == "b785b107632a3b8e9070a5a9a610202b46d916709f6b969b30c5d3375a2f38e7"
}
rule Sanesecurity_MalwareHash_HackingTeam_196
{
    condition:
        hash.md5(0, filesize) == "5e5157e77089c4cfcfb2dfc82a574e465a943323e330dfe15316553d41f3d7eb"
}
rule Sanesecurity_MalwareHash_HackingTeam_197
{
    condition:
        hash.md5(0, filesize) == "8f6988e717e0334b33b7f4697c8ebbb5038c218994c8da7dc295986fe43b2b8b"
}
rule Sanesecurity_MalwareHash_HackingTeam_198
{
    condition:
        hash.md5(0, filesize) == "feee319cff39fe40dd0e0651bdbb24e9701d7f5adc9eefbfbd4e7e465ebee7f1"
}
rule Sanesecurity_MalwareHash_HackingTeam_199
{
    condition:
        hash.md5(0, filesize) == "c52f4d1cf3ff09b22cf2f4bef867456aa7426c00fcd19c38b66ee3adc7eba057"
}
rule Sanesecurity_MalwareHash_HackingTeam_200
{
    condition:
        hash.md5(0, filesize) == "7fcd2160127471fbd92e3dfd656d73eef31195f1fe5a1c77027bd2a961467883"
}
rule Sanesecurity_MalwareHash_HackingTeam_201
{
    condition:
        hash.md5(0, filesize) == "976a843ee5a35e5015b5b2394e520e82403e6f81f877a4206bfe705bcb5e13e4"
}
rule Sanesecurity_MalwareHash_HackingTeam_202
{
    condition:
        hash.md5(0, filesize) == "3bee8a4ee4efc157949587342ca73316eb9c95442cdb25dc349008c43dc64ba6"
}
rule Sanesecurity_MalwareHash_HackingTeam_203
{
    condition:
        hash.md5(0, filesize) == "4ae1e35dc83825dc81e886b7597f00781b184be4fa288a8aa7a3c0f62a526387"
}
rule Sanesecurity_MalwareHash_HackingTeam_204
{
    condition:
        hash.md5(0, filesize) == "13397ce53d5bcc5339a9e5b83144eed11e051666abcf26ad393505cfd82ee9ea"
}
rule Sanesecurity_MalwareHash_HackingTeam_205
{
    condition:
        hash.md5(0, filesize) == "8caa3a2f4c39992952cd2bb38bebadbbee5fb68114500e37832221d4e59aea30"
}
rule Sanesecurity_MalwareHash_HackingTeam_206
{
    condition:
        hash.md5(0, filesize) == "d70699e40511f4dd459420751e66a2564f050ab17b101ca9955423de2c579fa6"
}
rule Sanesecurity_MalwareHash_HackingTeam_207
{
    condition:
        hash.md5(0, filesize) == "73ab06fce6b9746c1010a3c588c62069213d94134823b7527559a0f41c88d20d"
}
rule Sanesecurity_MalwareHash_HackingTeam_208
{
    condition:
        hash.md5(0, filesize) == "c14327a7d2c7ab2d3edb5c0db2f87688c30f4f781c10b6017183f74403494c07"
}
rule Sanesecurity_MalwareHash_HackingTeam_209
{
    condition:
        hash.md5(0, filesize) == "a1eae49b5f732a7ceef30fa8aa1218c9c97e6436bfab5555ed79e4b29b0fda83"
}
rule Sanesecurity_MalwareHash_HackingTeam_210
{
    condition:
        hash.md5(0, filesize) == "c8b3fa82fdd97f731851fa19611499b2c7a493cd689ac4d1796b3687d7fb6c82"
}
rule Sanesecurity_MalwareHash_HackingTeam_211
{
    condition:
        hash.md5(0, filesize) == "637cf542512b0b6507b39686c7e87af30e7aa3a02eb9481a49efb4d0951adfe8"
}
rule Sanesecurity_MalwareHash_HackingTeam_212
{
    condition:
        hash.md5(0, filesize) == "71864e38545034655c934d46f6b50485cb3d605ad39a7c3889f7d3816440bf1c"
}
rule Sanesecurity_MalwareHash_HackingTeam_213
{
    condition:
        hash.md5(0, filesize) == "c65d9d6defebeacbf761ae61baee0386dd7aeb2bd8577611edfadfb765e6ca52"
}
rule Sanesecurity_MalwareHash_HackingTeam_214
{
    condition:
        hash.md5(0, filesize) == "84058a01bb257a5c0f9a27f893ded585d349c9d87036d1a386fb8368cea2f545"
}
rule Sanesecurity_MalwareHash_HackingTeam_215
{
    condition:
        hash.md5(0, filesize) == "edc3fba72f9a485c43c1aa3cbe0c5752d8af2ec7bfecb48a46f467e549daac05"
}
rule Sanesecurity_MalwareHash_HackingTeam_216
{
    condition:
        hash.md5(0, filesize) == "72dc79c35aac14f453674ac3b62c268843a9c614ae99da01879db04c1dd995f9"
}
rule Sanesecurity_MalwareHash_HackingTeam_217
{
    condition:
        hash.md5(0, filesize) == "010ce301d6ff509e111e9102ec7b883fd888f1510fe3bfba6d71986704dbcd28"
}
rule Sanesecurity_MalwareHash_HackingTeam_218
{
    condition:
        hash.md5(0, filesize) == "b0d3aad477487039fbe9a33a66bd3654fb17f8af731c965d78977ebeb20392a8"
}
rule Sanesecurity_MalwareHash_HackingTeam_219
{
    condition:
        hash.md5(0, filesize) == "fc609adef44b5c64de029b2b2cff22a6f36b6bdf9463c1bd320a522ed39de5d9"
}
rule Sanesecurity_MalwareHash_HackingTeam_220
{
    condition:
        hash.md5(0, filesize) == "602bb8e06f9ec55f1b4c78a77e4ec229548763076a69e6606a898c4dd9731ff4"
}
rule Sanesecurity_MalwareHash_HackingTeam_221
{
    condition:
        hash.md5(0, filesize) == "5691fefbba82244c63e2166e246b1ef16d66b46ff1434e13815c8796177dc522"
}
rule Sanesecurity_MalwareHash_HackingTeam_222
{
    condition:
        hash.md5(0, filesize) == "150924668c8d7cd9899360eba12f13246538c50fbe7ef1ebf234ed7128c9936e"
}
rule Sanesecurity_MalwareHash_HackingTeam_223
{
    condition:
        hash.md5(0, filesize) == "3d8a446c2da93d0c909caf9724ad452c66c944cf71f582a9b5002e9b2cc67793"
}
rule Sanesecurity_MalwareHash_HackingTeam_224
{
    condition:
        hash.md5(0, filesize) == "eda9ba61ad01810aa53eece81626e913c4058a3b3cbf65fded907528117db0ec"
}
rule Sanesecurity_MalwareHash_HackingTeam_225
{
    condition:
        hash.md5(0, filesize) == "314211107852b35dbf7d2abc54581aadfce1ddf79e1930bb44e37ea4af338541"
}
rule Sanesecurity_MalwareHash_HackingTeam_226
{
    condition:
        hash.md5(0, filesize) == "f8addfa091021a34f8b16fac0687b685b72ff1cac87ba1392d6195ab42954d42"
}
rule Sanesecurity_MalwareHash_HackingTeam_227
{
    condition:
        hash.md5(0, filesize) == "f82c4673a15ff6c5806f54811c4e782b595a0a445476c3ccdbdc4cd200bfe36e"
}
rule Sanesecurity_MalwareHash_HackingTeam_228
{
    condition:
        hash.md5(0, filesize) == "1c5f12e0c15adf930b31402e6586f3a05a0173237ea13adce2f01edde9748992"
}
rule Sanesecurity_MalwareHash_HackingTeam_229
{
    condition:
        hash.md5(0, filesize) == "3e9a6f168c4f9f6ce6c6db3fee35218408ee0f79189f53e174f19a439e4036fb"
}
rule Sanesecurity_MalwareHash_HackingTeam_230
{
    condition:
        hash.md5(0, filesize) == "559266876f060621f9b910ec75404946121460375b6f7812da717896e96dec26"
}
rule Sanesecurity_MalwareHash_HackingTeam_231
{
    condition:
        hash.md5(0, filesize) == "90324a869541e0e67f0a3d4dcbdcdeefcaa4839edcb55ee163b7f26f80725278"
}
rule Sanesecurity_MalwareHash_HackingTeam_232
{
    condition:
        hash.md5(0, filesize) == "b800ba5adfc26f20b4049dba2442be73347e999a224716c7ecb5271e482e0a4d"
}
rule Sanesecurity_MalwareHash_HackingTeam_233
{
    condition:
        hash.md5(0, filesize) == "0418ecb096bdb3360694780a76838cd333900ebb26a168e3a95225e6579ea20e"
}
rule Sanesecurity_MalwareHash_HackingTeam_234
{
    condition:
        hash.md5(0, filesize) == "656c897b39d7867bd4d38696100a09e379b06ab5e5f6842c1329f6bb83e70161"
}
rule Sanesecurity_MalwareHash_HackingTeam_235
{
    condition:
        hash.md5(0, filesize) == "e378812f4347b6ec7a517d9c06dc1cd608322033743ec075afe26857bb65c6b0"
}
rule Sanesecurity_MalwareHash_HackingTeam_236
{
    condition:
        hash.md5(0, filesize) == "7a136aff189f79dee342378d9d011ef35b639840148989670cd9ed3aaa404cdd"
}
rule Sanesecurity_MalwareHash_HackingTeam_237
{
    condition:
        hash.md5(0, filesize) == "a9e25fbb95253412de09bc1e4323602afbf5077aca71f861cbc7ad74581511a2"
}
rule Sanesecurity_MalwareHash_HackingTeam_238
{
    condition:
        hash.md5(0, filesize) == "a801ca60fe94c8182274cbea1f5d3666d0b9aada7feffe3d9a613eb1c3a6f949"
}
rule Sanesecurity_MalwareHash_HackingTeam_239
{
    condition:
        hash.md5(0, filesize) == "79deeb5af79f9a48cbbbb37400b940dc1e709230d0b176669bc1d095c4bedca7"
}
rule Sanesecurity_MalwareHash_HackingTeam_240
{
    condition:
        hash.md5(0, filesize) == "f2f6dfc7fc3ff1170a80d661c1dbc18dbdfa456c1327ac475a7b21a38ec014be"
}
rule Sanesecurity_MalwareHash_HackingTeam_241
{
    condition:
        hash.md5(0, filesize) == "1b72081c4422785d8c6c016b10bdd7545e5fc6f1ff73277b0366e9b40e624616"
}
rule Sanesecurity_MalwareHash_HackingTeam_242
{
    condition:
        hash.md5(0, filesize) == "fff8c7da09ace612e203a7d91b24e56a9e1715d5bfe6a7a4466adff284009a1e"
}
rule Sanesecurity_MalwareHash_HackingTeam_243
{
    condition:
        hash.md5(0, filesize) == "42dc1f9417fb067c3b96622ccf6e8c80c9d07202cc28f3c4d460d5bdc6ff249f"
}
rule Sanesecurity_MalwareHash_HackingTeam_244
{
    condition:
        hash.md5(0, filesize) == "7927f3a35d87250253d8abc021d44cc496d2185f376f0d33b0365a68ba81e636"
}
rule Sanesecurity_MalwareHash_HackingTeam_245
{
    condition:
        hash.md5(0, filesize) == "5ec8cd3180a2576b92d53085ff5e3dcf4e3dccaf2154b59879969ef8011fd1c2"
}
rule Sanesecurity_MalwareHash_HackingTeam_246
{
    condition:
        hash.md5(0, filesize) == "4f9f7f9b2a3ee884f4aa08c066a458a52f175a78b7748ef4a751543213b92d29"
}
rule Sanesecurity_MalwareHash_HackingTeam_247
{
    condition:
        hash.md5(0, filesize) == "598bab73e4e2e9a09da64a16c807fea62bac20ec206384194478fcaf9eac1b14"
}
rule Sanesecurity_MalwareHash_HackingTeam_248
{
    condition:
        hash.md5(0, filesize) == "a72dc5010dc21c3bc9075c74fc7b87f0f89cfbeb1b1c4cdab06db4262d84969d"
}
rule Sanesecurity_MalwareHash_HackingTeam_249
{
    condition:
        hash.md5(0, filesize) == "71fe815f897877e69e4a37844a6d2feb40fdecaed1dd55b07472234e87e22767"
}
rule Sanesecurity_MalwareHash_HackingTeam_250
{
    condition:
        hash.md5(0, filesize) == "654e7dd64ab4ef04ea22f63fb0497346fb8d484a488be428d78d32a17654604d"
}
rule Sanesecurity_MalwareHash_HackingTeam_251
{
    condition:
        hash.md5(0, filesize) == "6e678dc4d933b186557f671913fb2fada37f342d5007dac0b745ca718d2e7405"
}
rule Sanesecurity_MalwareHash_HackingTeam_252
{
    condition:
        hash.md5(0, filesize) == "cba8e646e951dbfde33daddc1ad6429814dad1ae1786c886948ce9ed7029f487"
}
rule Sanesecurity_MalwareHash_HackingTeam_253
{
    condition:
        hash.md5(0, filesize) == "b15b2acbe02d7b0649b41d1fe7e0cd008761cba28d20c5d9fa9c17e3a430d0eb"
}
rule Sanesecurity_MalwareHash_HackingTeam_254
{
    condition:
        hash.md5(0, filesize) == "cc87e067021f8b419cc73863d26bd54e25b6f4c8149d6d331ba50e54aea917ad"
}
rule Sanesecurity_MalwareHash_HackingTeam_255
{
    condition:
        hash.md5(0, filesize) == "0ca7fafd58f8ddca6dd182790b1a634205f45bac5c4a3ff4cecc3473d0c47726"
}
rule Sanesecurity_MalwareHash_HackingTeam_256
{
    condition:
        hash.md5(0, filesize) == "a9af1d410b796a7d89050bb8189048260564a1ff0b94db25d0f465ea18b1c02b"
}
rule Sanesecurity_MalwareHash_HackingTeam_257
{
    condition:
        hash.md5(0, filesize) == "9db48e1cb712104830461c062d0a93f8e3b4043c0ab8b2dc0e1f5599827f4e21"
}
rule Sanesecurity_MalwareHash_HackingTeam_258
{
    condition:
        hash.md5(0, filesize) == "5a45524e9ad739585c3851b32f660d777624c811d0b293b3474fa2568e8022d4"
}
rule Sanesecurity_MalwareHash_HackingTeam_259
{
    condition:
        hash.md5(0, filesize) == "b40d0ed8d1b7bbd0d52990ccbb7e927777d9854640c6c4b0adc683d55a965758"
}
rule Sanesecurity_MalwareHash_HackingTeam_260
{
    condition:
        hash.md5(0, filesize) == "8306c3a000636a21275774fcc17cd0bf75d1959bd9ea6bdb272666fda8494649"
}
rule Sanesecurity_MalwareHash_HackingTeam_261
{
    condition:
        hash.md5(0, filesize) == "988246ec5ee40470dd1c6661f7509d43dfa3debadd66ae4722a091935ecb56d9"
}
rule Sanesecurity_MalwareHash_HackingTeam_262
{
    condition:
        hash.md5(0, filesize) == "a5948e46db292b61d4c4032a7c7af15453477dd6ce4453daa4a6753c7763d873"
}
rule Sanesecurity_MalwareHash_HackingTeam_263
{
    condition:
        hash.md5(0, filesize) == "e32cfd415d5aee289a62a02b28b7815346cd150d70c0e1f95bb92ecf26a855de"
}
rule Sanesecurity_MalwareHash_HackingTeam_264
{
    condition:
        hash.md5(0, filesize) == "b524abb464b30366afff9b01da259432f76fef62a7b9d128284e289e76b3da16"
}
rule Sanesecurity_MalwareHash_HackingTeam_265
{
    condition:
        hash.md5(0, filesize) == "b606cad7024a165b899e3d2ae9625e6d0f207928eb2838a6c4c8b26ddd583bb8"
}
rule Sanesecurity_MalwareHash_HackingTeam_266
{
    condition:
        hash.md5(0, filesize) == "31e9433eccf1c150462b705af11eff50587d25526225d0c4ba07312af0c81969"
}
rule Sanesecurity_MalwareHash_HackingTeam_267
{
    condition:
        hash.md5(0, filesize) == "941ceeb2cbe1969dc41059e0766b5d6df687e8e8d96e31efea71699686ab6b9e"
}
rule Sanesecurity_MalwareHash_HackingTeam_268
{
    condition:
        hash.md5(0, filesize) == "adca333d2cee959c9323327ec8b3abd1193f34c520b80e4f699b49f70e14971c"
}
rule Sanesecurity_MalwareHash_HackingTeam_269
{
    condition:
        hash.md5(0, filesize) == "2c72175f96c651eea3d3411efacf73e0fb3e7543451b73f5e2521f47be67f006"
}
rule Sanesecurity_MalwareHash_HackingTeam_270
{
    condition:
        hash.md5(0, filesize) == "ec0e0c640f83d91fc50d657870f4b1d07bff0300ad6ba841bc7a211160ca79bf"
}
rule Sanesecurity_MalwareHash_HackingTeam_271
{
    condition:
        hash.md5(0, filesize) == "cedaf3f2bdbd936ca276b636bb119136d67e0e2fa74614442c95bdbae6c50585"
}
rule Sanesecurity_MalwareHash_HackingTeam_272
{
    condition:
        hash.md5(0, filesize) == "f3fc6d8ed53b5be3be601281848d26134fa85ba4737ab69b13a50a3a8dd523cb"
}
rule Sanesecurity_MalwareHash_HackingTeam_273
{
    condition:
        hash.md5(0, filesize) == "3b471511630e5ae364c28de07dae041a5b44a040f49e15735afa509e44801863"
}
rule Sanesecurity_MalwareHash_HackingTeam_274
{
    condition:
        hash.md5(0, filesize) == "d94b971cecd864fe6153ebe94a775157f3cdb69e8ad802eb78cfc0136737c0f2"
}
rule Sanesecurity_MalwareHash_HackingTeam_275
{
    condition:
        hash.md5(0, filesize) == "2b5560f11b24de4fac1b0998cfe80138c2a4f87bb15f6eba6f7f58a5cf1f8622"
}
rule Sanesecurity_MalwareHash_HackingTeam_276
{
    condition:
        hash.md5(0, filesize) == "60562a923d1fb595d6e144a0957bc5f9fda0d3f105c316ab5e7d7cd27ff0c27f"
}
rule Sanesecurity_MalwareHash_HackingTeam_277
{
    condition:
        hash.md5(0, filesize) == "0a5c0224092468a4669f04721e291e3e89653d1ecf436c5c4dd7f1f8df4d0ff7"
}
rule Sanesecurity_MalwareHash_HackingTeam_278
{
    condition:
        hash.md5(0, filesize) == "f1ab31f87585c824381ecd5411441bb1c755d81dd0f42bc08fbb061b9066fba0"
}
rule Sanesecurity_MalwareHash_HackingTeam_279
{
    condition:
        hash.md5(0, filesize) == "f08e6bc6c3a6771f697d4f724bb238f837f61d988c29a2d77dd73cd36a4a38b7"
}
rule Sanesecurity_MalwareHash_HackingTeam_280
{
    condition:
        hash.md5(0, filesize) == "3f85279eee498578935e7f51881f8411be5ac7ba45f2334699230cd0b9d60032"
}
rule Sanesecurity_MalwareHash_HackingTeam_281
{
    condition:
        hash.md5(0, filesize) == "200c0623f75433c1e2821d930e6f3e072c5e06f2bd1770551595acc3b170febf"
}
rule Sanesecurity_MalwareHash_HackingTeam_282
{
    condition:
        hash.md5(0, filesize) == "401e449c1648117af46395c5223a836f534c7333d5922826db803ff6e45c9f77"
}
rule Sanesecurity_MalwareHash_HackingTeam_283
{
    condition:
        hash.md5(0, filesize) == "511e3eb95e12a2b12b3ade129ed79f0f2399f56c29247b1d535acccabec3203f"
}
rule Sanesecurity_MalwareHash_HackingTeam_284
{
    condition:
        hash.md5(0, filesize) == "24a74d298d88b25bbaacf103a649896540a1870d89b76a17ad056252c065b393"
}
rule Sanesecurity_MalwareHash_HackingTeam_285
{
    condition:
        hash.md5(0, filesize) == "24a74d298d88b25bbaacf103a649896540a1870d89b76a17ad056252c065b393"
}
rule Sanesecurity_MalwareHash_HackingTeam_286
{
    condition:
        hash.md5(0, filesize) == "b254fe2bb3967a5f5093567a1204bca802e73a3f3a289c320b18f0f73bc7c5ec"
}
rule Sanesecurity_MalwareHash_HackingTeam_287
{
    condition:
        hash.md5(0, filesize) == "045d84ea644b90cf5ddcb5ec26c6683835ad7640c83488523a0ebc0d4063c92a"
}
rule Sanesecurity_MalwareHash_HackingTeam_288
{
    condition:
        hash.md5(0, filesize) == "3e75d9a4feeb21434ea3294a36d64dda5165d66b82b1d567e7ba14dc13d437c6"
}
rule Sanesecurity_MalwareHash_HackingTeam_289
{
    condition:
        hash.md5(0, filesize) == "5993a690c5131cc9269b72a5a6d8f0f6d5c4ed5a4f51418c0018a72105fe361e"
}
rule Sanesecurity_MalwareHash_HackingTeam_290
{
    condition:
        hash.md5(0, filesize) == "9415a9ae4a58ccc6a0e859aaf27e8a9b5e9aba1cb4fb2a711b9d209e388df6d8"
}
rule Sanesecurity_MalwareHash_HackingTeam_291
{
    condition:
        hash.md5(0, filesize) == "c2cfe41365a5ab251090b5f4583ae234f3445c626bcb85ef45ac5d63973c5964"
}
rule Sanesecurity_MalwareHash_HackingTeam_292
{
    condition:
        hash.md5(0, filesize) == "29720f3bd8b01e49a2266c89281a2cba78d5e417d11859ed113acfd2ce627f27"
}
rule Sanesecurity_MalwareHash_HackingTeam_293
{
    condition:
        hash.md5(0, filesize) == "c4a28888ba96e92c22d497d0690e9d484bdf093da12d419fde7c7f3674d845cd"
}
rule Sanesecurity_MalwareHash_HackingTeam_294
{
    condition:
        hash.md5(0, filesize) == "ec716cffd1650cea61d6536c2f4c52a7819990737b0ee619dbe3953d6d9debaf"
}
rule Sanesecurity_MalwareHash_HackingTeam_295
{
    condition:
        hash.md5(0, filesize) == "4c7d7c0f4340d7296c9c4f5a80b80793b85c33aabce31badc9dccfa37ef38949"
}
rule Sanesecurity_MalwareHash_HackingTeam_296
{
    condition:
        hash.md5(0, filesize) == "51816c81180eadfcdba84be0285d256ad650dd6eec131f94e1c1957623d7e9bc"
}
rule Sanesecurity_MalwareHash_HackingTeam_297
{
    condition:
        hash.md5(0, filesize) == "eb4a395d7a0bb8b755b79f76ffe4bbed04dd630e07132a4ff12d684e178c7a03"
}
rule Sanesecurity_MalwareHash_HackingTeam_298
{
    condition:
        hash.md5(0, filesize) == "6ee1da63a3d5a55c66df408c2c0b57fc5bae064778cdf1845bc00e44021c290a"
}
rule Sanesecurity_MalwareHash_HackingTeam_299
{
    condition:
        hash.md5(0, filesize) == "4d8951f93381442e7961f4aa69550f610160567ee44386f0637269af596b07b4"
}
rule Sanesecurity_MalwareHash_HackingTeam_300
{
    condition:
        hash.md5(0, filesize) == "65a37da4fd57747c8e73df8f88a7de27b1f3bfc6f0467e45f9241194c4f70954"
}
rule Sanesecurity_MalwareHash_HackingTeam_301
{
    condition:
        hash.md5(0, filesize) == "48097208ba892a8bb63e87b8d0dde52ac4d28591b9cf5aea1b93e1df0c24d500"
}
rule Sanesecurity_MalwareHash_HackingTeam_302
{
    condition:
        hash.md5(0, filesize) == "f6773236f089605d7604b90f5b6e3e223871f0bbfdad405fd89fde56cfc2ff27"
}
rule Sanesecurity_MalwareHash_HackingTeam_303
{
    condition:
        hash.md5(0, filesize) == "6f788920ac2df748947f767a1e9b5ee3a5c9f4d073fd07792c9ebfc4eaf45ca9"
}
rule Sanesecurity_MalwareHash_HackingTeam_304
{
    condition:
        hash.md5(0, filesize) == "f40712947be07627efbf9d4aa03f158c22afc37e3236beb935a495d3084db50b"
}
rule Sanesecurity_MalwareHash_HackingTeam_305
{
    condition:
        hash.md5(0, filesize) == "25e4376ebb9d3aa7f35aacf9298416e125d02bc6c180a78e749d46c1669ae886"
}
rule Sanesecurity_MalwareHash_HackingTeam_306
{
    condition:
        hash.md5(0, filesize) == "5d761821b44d6fc1de9020b17aed6b9dd58f9a00f83860ce9511783c98de5d46"
}
rule Sanesecurity_MalwareHash_HackingTeam_307
{
    condition:
        hash.md5(0, filesize) == "e56a2ebeaa01def702f6653bad90651854d1af461899cabac6db12d81566ba1d"
}
rule Sanesecurity_MalwareHash_HackingTeam_308
{
    condition:
        hash.md5(0, filesize) == "57797d9c4740cca49aabe0187bb38607e2c157eb800c6794bb64e2751f5fd967"
}
rule Sanesecurity_MalwareHash_HackingTeam_309
{
    condition:
        hash.md5(0, filesize) == "a9d52b28ca8e049386417ffed62dd8da5b21d99aeb13dea25e6cc081e7c12393"
}
rule Sanesecurity_MalwareHash_HackingTeam_310
{
    condition:
        hash.md5(0, filesize) == "c8d923daf0484747933acacec66edd2a7c1e1636e6551e45d0e4883195ae2458"
}
rule Sanesecurity_MalwareHash_HackingTeam_311
{
    condition:
        hash.md5(0, filesize) == "d38a068de0cdd67f0825c31e62977231bb1f287a8c39c148beaecf9c93f31605"
}
rule Sanesecurity_MalwareHash_HackingTeam_312
{
    condition:
        hash.md5(0, filesize) == "dcb06dce6163b736ae1623365c1023bebb9915d09da1097931952472ab1d02fa"
}
rule Sanesecurity_MalwareHash_HackingTeam_313
{
    condition:
        hash.md5(0, filesize) == "fdc4a5593fe32d96fa2afbc110d90d6ce202b6adbd71fbd424642cd409b212c4"
}
rule Sanesecurity_MalwareHash_HackingTeam_314
{
    condition:
        hash.md5(0, filesize) == "3833e00748827cc78010c7f8fbab07d3badd2513ff087d89ea8b323b5c97cfeb"
}
rule Sanesecurity_MalwareHash_HackingTeam_315
{
    condition:
        hash.md5(0, filesize) == "7b1d4378bae812656e4765e79bc388239381404d8a6080a16ff6776312442989"
}
rule Sanesecurity_MalwareHash_HackingTeam_316
{
    condition:
        hash.md5(0, filesize) == "442ee6ef5fc592627a3214336467559a86a57b9ecc899904e2f63320578a9fc8"
}
rule Sanesecurity_MalwareHash_HackingTeam_317
{
    condition:
        hash.md5(0, filesize) == "6cf4dd214241effa8ccee50d66dfc4df2c67762c0decd0da1fac9787d8231104"
}
rule Sanesecurity_MalwareHash_HackingTeam_318
{
    condition:
        hash.md5(0, filesize) == "5da4b794cda68bc7f0141d8b84a6b17ca22011fc27a7730ff9b5a4e3b33320ef"
}
rule Sanesecurity_MalwareHash_HackingTeam_319
{
    condition:
        hash.md5(0, filesize) == "ebab6eda6d74afc99021ded4d146bc4cc23f9a2a026d0fbfa217578e9acd22a8"
}
rule Sanesecurity_MalwareHash_HackingTeam_320
{
    condition:
        hash.md5(0, filesize) == "7dcf83c5af0991d927c8caa9bc1424e541c966de37aab8845e4e11dddfeabbeb"
}
rule Sanesecurity_MalwareHash_HackingTeam_321
{
    condition:
        hash.md5(0, filesize) == "1c3c6ade5e2e1a8f1476ceae58c143aae0d71a535fb3bc81981072ebd634f3ab"
}
rule Sanesecurity_MalwareHash_HackingTeam_322
{
    condition:
        hash.md5(0, filesize) == "bb3efa4fabc01da14ab5da04cde23902a866f0a308d58a0664e68555e71ecf69"
}
rule Sanesecurity_MalwareHash_HackingTeam_323
{
    condition:
        hash.md5(0, filesize) == "60947bddb5e549732ae3d2b4749f5dbf8d054bb955c768afc49224d297c0ea72"
}
rule Sanesecurity_MalwareHash_HackingTeam_324
{
    condition:
        hash.md5(0, filesize) == "ac149d3edd34428b5a9958b0f33cde23d6bfb5c8609062bcb03e9488fed10b47"
}
rule Sanesecurity_MalwareHash_HackingTeam_325
{
    condition:
        hash.md5(0, filesize) == "a3c5c15d9bbdd342ed22dc84bbfb66261648acf6bd3f8b56dea0c36a0e55a0f2"
}
rule Sanesecurity_MalwareHash_HackingTeam_326
{
    condition:
        hash.md5(0, filesize) == "2e456b7065573ad5e62db360f5a451f1e460df08479a8fffc89e5857d70516b9"
}
rule Sanesecurity_MalwareHash_HackingTeam_327
{
    condition:
        hash.md5(0, filesize) == "2cf44c90107891c22543fb0b71612fcf3e531352425b5bd9d6763a019e3b06a0"
}
rule Sanesecurity_MalwareHash_HackingTeam_328
{
    condition:
        hash.md5(0, filesize) == "ddc857de7180ae0d54d793509bfda9671bca5123f74791b1d8291d68d05e44eb"
}
rule Sanesecurity_MalwareHash_HackingTeam_329
{
    condition:
        hash.md5(0, filesize) == "436e43664724b946197849cc803f364e10b5924567d96eb6e7cb7ce56a323825"
}
rule Sanesecurity_MalwareHash_HackingTeam_330
{
    condition:
        hash.md5(0, filesize) == "7d805fc7c5f667a3825d0bee1f19d9e048a18b91c64af71e14b78ffddb5bb31e"
}
rule Sanesecurity_MalwareHash_HackingTeam_331
{
    condition:
        hash.md5(0, filesize) == "90690ebe2fece6a3432d3e169d26190162bfc3329ba5e4f0d157bf4ca853ea03"
}
rule Sanesecurity_MalwareHash_HackingTeam_332
{
    condition:
        hash.md5(0, filesize) == "6138fdaec7015624419c1e18157e938e8efebc54232e34b78f6cc41b4ecfea50"
}
rule Sanesecurity_MalwareHash_HackingTeam_333
{
    condition:
        hash.md5(0, filesize) == "25ac093f3ceaaef2061ba86e8887ab494b4113b6fcc611bc69fddaca912715ac"
}
rule Sanesecurity_MalwareHash_HackingTeam_334
{
    condition:
        hash.md5(0, filesize) == "f10aea369bc86b9350584355cc31bb8c909a744e747f8a9840bd0bc85fce176e"
}
rule Sanesecurity_MalwareHash_HackingTeam_335
{
    condition:
        hash.md5(0, filesize) == "514c0c34572895c41d83e674bbda9135da545c48c8e1dd9ff5a7ec5a8d52c72d"
}
rule Sanesecurity_MalwareHash_HackingTeam_336
{
    condition:
        hash.md5(0, filesize) == "e827ec9a1b0acaf346aa98dd77ea61479ff6cd89bb631e7dbef6ead3b5c59bc0"
}
rule Sanesecurity_MalwareHash_HackingTeam_337
{
    condition:
        hash.md5(0, filesize) == "683f59b3f4ea8b5b50446e08861a208eb154db039f5c769db52fb2df2ff39517"
}
rule Sanesecurity_MalwareHash_HackingTeam_338
{
    condition:
        hash.md5(0, filesize) == "7f27100409815208dcaa4c05dc5d1e5d7541a08371e579778a9b9cae298a498e"
}
rule Sanesecurity_MalwareHash_HackingTeam_339
{
    condition:
        hash.md5(0, filesize) == "afcab14b17a685f51809883bf501e8429152414b8f93a041eefa01e4d2d015ef"
}
rule Sanesecurity_MalwareHash_HackingTeam_340
{
    condition:
        hash.md5(0, filesize) == "5887451392b8d97f6e28b8e4256b135279d3cb532e6751e2edbf90525fc2abd7"
}
rule Sanesecurity_MalwareHash_HackingTeam_341
{
    condition:
        hash.md5(0, filesize) == "28bde3aa00980e9ca5893004c59301f32de55a1c37951e0fb437d0b0f86c38d8"
}
rule Sanesecurity_MalwareHash_HackingTeam_342
{
    condition:
        hash.md5(0, filesize) == "2a83621703aae61467158e0c85869e0f2277ee0427472c4c5ac268859fee7798"
}
rule Sanesecurity_MalwareHash_HackingTeam_343
{
    condition:
        hash.md5(0, filesize) == "296e9f04e0f291e9d6aff8443b4ab53ebc21fa524b18be0eb100b900cef1b5fa"
}
rule Sanesecurity_MalwareHash_HackingTeam_344
{
    condition:
        hash.md5(0, filesize) == "c1b7a8c1cd20032048681f5ace27be3d5b8d2e026e0f7698c69442ec9b518b4d"
}
rule Sanesecurity_MalwareHash_HackingTeam_345
{
    condition:
        hash.md5(0, filesize) == "ffd49343ef5d6a77d30c9e9678079d48285e82d9537abe4f12ccebdd5caea23c"
}
rule Sanesecurity_MalwareHash_HackingTeam_346
{
    condition:
        hash.md5(0, filesize) == "081d62be8d947b1ecbeb8f77c776bb79f06e86e39df4a182c3a3f25744313196"
}
rule Sanesecurity_MalwareHash_HackingTeam_347
{
    condition:
        hash.md5(0, filesize) == "2ae912e59afee8b92687549c81a49e5146985dae27502ad9e06637e481d5d627"
}
rule Sanesecurity_MalwareHash_HackingTeam_348
{
    condition:
        hash.md5(0, filesize) == "32fb339ad17e516c1ffe6e84f792cccc200ba059cbefb3854b2aff7c82e365ba"
}
rule Sanesecurity_MalwareHash_HackingTeam_349
{
    condition:
        hash.md5(0, filesize) == "7f974955714319a86b478fbe7f10fe0cd8f2cd15bb87f6b006f7a9deae010d6d"
}
rule Sanesecurity_MalwareHash_HackingTeam_350
{
    condition:
        hash.md5(0, filesize) == "e326ff4b3581b0c6cabdf973ad62a3feadad31f65bac975572f55cf1462bbab7"
}
rule Sanesecurity_MalwareHash_HackingTeam_351
{
    condition:
        hash.md5(0, filesize) == "23fc75c4cc154a74aeeff9c7729295b7436d26bb6a0292627120782fe698b18a"
}
rule Sanesecurity_MalwareHash_HackingTeam_352
{
    condition:
        hash.md5(0, filesize) == "bb2e56e22ec4fc86f70ca44b847cf03763ae4f7a5877993c3dc03dccbfa8c4d8"
}
rule Sanesecurity_MalwareHash_HackingTeam_353
{
    condition:
        hash.md5(0, filesize) == "65c3784519e5ea050b8b669d2f074183c663abf5ef5335f7c2dcc6c283d5c991"
}
rule Sanesecurity_MalwareHash_HackingTeam_354
{
    condition:
        hash.md5(0, filesize) == "a99a397d998961c176da6b16a3b68b8d6b209d0dbc90b9ab461657d9b50d5a7d"
}
rule Sanesecurity_MalwareHash_HackingTeam_355
{
    condition:
        hash.md5(0, filesize) == "d4414fffcc561578f53bdffc0a61ca081f45f8a7f203ec012ba80a3d2a45b7b0"
}
rule Sanesecurity_MalwareHash_HackingTeam_356
{
    condition:
        hash.md5(0, filesize) == "ad88c494669cf7224ca9511bfd4fcd130984f34d740e8d06661eb4b9967f019c"
}
rule Sanesecurity_MalwareHash_HackingTeam_357
{
    condition:
        hash.md5(0, filesize) == "8861391b767cb9fa74c442a11edf407acf614b404a5971c6292655fc448a5679"
}
rule Sanesecurity_MalwareHash_HackingTeam_358
{
    condition:
        hash.md5(0, filesize) == "3f44a887f3c06f2fbc77e8c1f4f3af0308abc85cad4268a194eea51395e9cd61"
}
rule Sanesecurity_MalwareHash_HackingTeam_359
{
    condition:
        hash.md5(0, filesize) == "c859aa2de123b3526dd7e5645d8b631d1a7ce54d62534288940f215243fda561"
}
rule Sanesecurity_MalwareHash_HackingTeam_360
{
    condition:
        hash.md5(0, filesize) == "622a646311cc4ceec5d9b904fd4e4b5cc27a77b76fbbe2daf00976477241b0e7"
}
rule Sanesecurity_MalwareHash_HackingTeam_361
{
    condition:
        hash.md5(0, filesize) == "568b1b2fd194688667619f071c470537b8d3f82ea8824e4b60cea3c4f7199328"
}
rule Sanesecurity_MalwareHash_HackingTeam_362
{
    condition:
        hash.md5(0, filesize) == "2424e1d1c1820a2cbb194f053f84f4a2c11ee2c75f301985c9236678944d3756"
}
rule Sanesecurity_MalwareHash_HackingTeam_363
{
    condition:
        hash.md5(0, filesize) == "91583788c81ef48567671b8173c31b826b8f3e20cb40b8bdd7a528093ec7c717"
}
rule Sanesecurity_MalwareHash_HackingTeam_364
{
    condition:
        hash.md5(0, filesize) == "3ffcd563ca6222ff5e88928a363e38d482302de57b04c36dd710a4bd1c5d430e"
}
rule Sanesecurity_MalwareHash_HackingTeam_365
{
    condition:
        hash.md5(0, filesize) == "9a68ae8f678636414d04cef6aaeea76b31f06870a039474e24ce0d299cfe7ff9"
}
rule Sanesecurity_MalwareHash_HackingTeam_366
{
    condition:
        hash.md5(0, filesize) == "4846f3d91b3ebb54b5a8739f6ac5baae2ae28d8e363b87ed6fd37e1a012f32ec"
}
rule Sanesecurity_MalwareHash_HackingTeam_367
{
    condition:
        hash.md5(0, filesize) == "b292df8f088af93ab479c47c8afef0144e952a742e9d136b078ea07fa691e328"
}
rule Sanesecurity_MalwareHash_HackingTeam_368
{
    condition:
        hash.md5(0, filesize) == "9a6837504d6c41409008c3b50bbe006d5cc167e92f6f63a62320a78417307fa5"
}
rule Sanesecurity_MalwareHash_HackingTeam_369
{
    condition:
        hash.md5(0, filesize) == "bc0ca8f7de902d9b387db9533d0bee3f94e3e915495821bc739adb6ccb70bed2"
}
rule Sanesecurity_MalwareHash_HackingTeam_370
{
    condition:
        hash.md5(0, filesize) == "78d54fd3dfdf0be1baaec7fd9320cd549582dcfb3e14257fa2169c0fa60af994"
}
rule Sanesecurity_MalwareHash_HackingTeam_371
{
    condition:
        hash.md5(0, filesize) == "c6aa5c8c72cb8dff50cefa4f3ec60fda50f64c54148658e3a1912728d83f5024"
}
rule Sanesecurity_MalwareHash_HackingTeam_372
{
    condition:
        hash.md5(0, filesize) == "342c4cac3b39eef3b1b455fc5e8cf493547f65aba415ba699c98bf675a746a49"
}
rule Sanesecurity_MalwareHash_HackingTeam_373
{
    condition:
        hash.md5(0, filesize) == "1381fb1455e99da4221589a7d0e51208e8038871586747209031d934fed5f2e6"
}
rule Sanesecurity_MalwareHash_HackingTeam_374
{
    condition:
        hash.md5(0, filesize) == "43b98d7043144d35b6657174669dcc1e695cb8e34c0df454c652bdede72ffb93"
}
rule Sanesecurity_MalwareHash_HackingTeam_375
{
    condition:
        hash.md5(0, filesize) == "37fe299ed3fd2584c9f6ff5473b2de4a7bd27e2561e0aa41df5afc5bf6ec3c41"
}
rule Sanesecurity_MalwareHash_HackingTeam_376
{
    condition:
        hash.md5(0, filesize) == "64abe3dea01a2064a62916e531a189ed93d730aed9876dc439c4eb78e3076345"
}
rule Sanesecurity_MalwareHash_HackingTeam_377
{
    condition:
        hash.md5(0, filesize) == "51965ab9ffcb63f6104d451fd15f78331cbba796e28ac7748e8eefbe0b0f006c"
}
rule Sanesecurity_MalwareHash_HackingTeam_378
{
    condition:
        hash.md5(0, filesize) == "5e54fc43c0013f38c12ad777dd209a463c7d73db3a0c01ca0903976d856dfae4"
}
rule Sanesecurity_MalwareHash_HackingTeam_379
{
    condition:
        hash.md5(0, filesize) == "1bff9f531ad4cbf5a829436b6e459867ec0a956cc1a40c0bd8eb3c0d4846293b"
}
rule Sanesecurity_MalwareHash_HackingTeam_380
{
    condition:
        hash.md5(0, filesize) == "e69f9773be05b1d0757ca826206aef1585e67282d2fbf544869034cff3a49194"
}
rule Sanesecurity_MalwareHash_HackingTeam_381
{
    condition:
        hash.md5(0, filesize) == "ff156bb8efa93bcd15fd0d49fc154dc43835e1b66f7226dd27b66bb67622a516"
}
rule Sanesecurity_MalwareHash_HackingTeam_382
{
    condition:
        hash.md5(0, filesize) == "fd0d4f1a711a8de644667f0013e27dabb826d01b4a7563266c9cc701e9d5d838"
}
rule Sanesecurity_MalwareHash_HackingTeam_383
{
    condition:
        hash.md5(0, filesize) == "62cac70e6ee1b46d4fd5402a567e8519a4ae16bc32b906c90f74ea471e97e4f5"
}
rule Sanesecurity_MalwareHash_HackingTeam_384
{
    condition:
        hash.md5(0, filesize) == "e1c2e858abf286ac76b072928c9581841618720d182394b8bcbbb7147bec0cf7"
}
rule Sanesecurity_MalwareHash_HackingTeam_385
{
    condition:
        hash.md5(0, filesize) == "03dddfa669c3aeabbdaf320ef725d4f9741096a13fe541946655b2acdab66003"
}
rule Sanesecurity_MalwareHash_HackingTeam_386
{
    condition:
        hash.md5(0, filesize) == "e69a5ff4fc4e3a1958d1d25f31ab85bcd477c2bc95365a01c513c22715f2db0d"
}
rule Sanesecurity_MalwareHash_HackingTeam_387
{
    condition:
        hash.md5(0, filesize) == "40977ce5c1e3a5296f4359d30a9a9518a83259410bdc6a568cab2e0c30311c8c"
}
rule Sanesecurity_MalwareHash_HackingTeam_388
{
    condition:
        hash.md5(0, filesize) == "ba6038236dfe8db7ccfe782a7ea55ce182c97767ca2fe195e0f67204e14983e1"
}
rule Sanesecurity_MalwareHash_HackingTeam_389
{
    condition:
        hash.md5(0, filesize) == "ecb4779c87ea2c0a95ccd1d0231ba063e4b53d86d28b29d0566a8ef0192f485d"
}
rule Sanesecurity_MalwareHash_HackingTeam_390
{
    condition:
        hash.md5(0, filesize) == "477f1f7dfd4ec43da8b3ebfd162812208ea085edf3259ba1e8b0b551825004b6"
}
rule Sanesecurity_MalwareHash_HackingTeam_391
{
    condition:
        hash.md5(0, filesize) == "c8a7064daa5c1e209a7085a829b4d393b8d80835deae7c1d1b35a0d437c0e0f8"
}
rule Sanesecurity_MalwareHash_HackingTeam_392
{
    condition:
        hash.md5(0, filesize) == "a58dd0b17c7989e3dcad1057c4f8473d5c18bc134b29f8aa52b30062c73ae672"
}
rule Sanesecurity_MalwareHash_HackingTeam_393
{
    condition:
        hash.md5(0, filesize) == "285d311762bd9beaf15447dcbc33bb5c08f3c89a269de0046b5f5bf16519cac6"
}
rule Sanesecurity_MalwareHash_HackingTeam_394
{
    condition:
        hash.md5(0, filesize) == "917a3d71a1cddeb983e3ce1823f60eca1942981265ebfbbe4cf243f1545bb575"
}
rule Sanesecurity_MalwareHash_HackingTeam_395
{
    condition:
        hash.md5(0, filesize) == "1a67b08eb7be0ec141d407b6273bf9358874f8431eb79394f7222387d2e3b198"
}
rule Sanesecurity_MalwareHash_HackingTeam_396
{
    condition:
        hash.md5(0, filesize) == "2e099df103d467e3d9a44af15c047e3deb2540ce4d54e01294c511e17e6479e4"
}
rule Sanesecurity_MalwareHash_HackingTeam_397
{
    condition:
        hash.md5(0, filesize) == "32abff1cee3791ea98f7205eb504465c60bd7e57c2b6048929eed56822e67efc"
}
rule Sanesecurity_MalwareHash_HackingTeam_398
{
    condition:
        hash.md5(0, filesize) == "58378701354e6eaa658d5f0977032522a80ce171e15b841b18dec8a56cb566ea"
}
rule Sanesecurity_MalwareHash_HackingTeam_399
{
    condition:
        hash.md5(0, filesize) == "ed4f21dd3c01e7636cbc30cf549d347cb385099c18bf282880b7df7a18140f6e"
}
rule Sanesecurity_MalwareHash_HackingTeam_400
{
    condition:
        hash.md5(0, filesize) == "199291791ccd7ca5bed11be7b50ca72750d9d78e30694730f159729a4835f5c2"
}
rule Sanesecurity_MalwareHash_HackingTeam_401
{
    condition:
        hash.md5(0, filesize) == "ba4d1658047690d3fc9dd96fb5d56c146dcaa14c1e72f2fd5bbab1920ed0741b"
}
rule Sanesecurity_MalwareHash_HackingTeam_402
{
    condition:
        hash.md5(0, filesize) == "635cdff6a80983828b251fef7147f2b92a6446cd55ed7127c8140cdc8100d60b"
}
rule Sanesecurity_MalwareHash_HackingTeam_403
{
    condition:
        hash.md5(0, filesize) == "4795d51d9cf26d9a3decb90695e4d903fd482548b356547c023e2899cd120d56"
}
rule Sanesecurity_MalwareHash_HackingTeam_404
{
    condition:
        hash.md5(0, filesize) == "99e4d4e0117a5b391b2c2030b62915d066900690b14145bf444fbe5f98f90df1"
}
rule Sanesecurity_MalwareHash_HackingTeam_405
{
    condition:
        hash.md5(0, filesize) == "23972fe89540cf6636288612ecb6090b9f0c23e12bf02d5cb69def679105157c"
}
rule Sanesecurity_MalwareHash_HackingTeam_406
{
    condition:
        hash.md5(0, filesize) == "0e3857bdba2e678a98da13317fd128341247964349c2483971e650b8d19ef914"
}
rule Sanesecurity_MalwareHash_HackingTeam_407
{
    condition:
        hash.md5(0, filesize) == "6739dd4361c559fd9099dfc967b06eb5bac95ee8693986ac29c7b368dc7cff08"
}
rule Sanesecurity_MalwareHash_HackingTeam_408
{
    condition:
        hash.md5(0, filesize) == "8d13453b459acd28dbc706677983b1397912124acf802dfa2b2f5c64f45c230b"
}
rule Sanesecurity_MalwareHash_HackingTeam_409
{
    condition:
        hash.md5(0, filesize) == "c585f02a22f2fe7dd7a1720bfc119613ea32b589ea72dbeb4d6c91d8e7bf399a"
}
rule Sanesecurity_MalwareHash_HackingTeam_410
{
    condition:
        hash.md5(0, filesize) == "b20b198d9e3af27ecac4a83b66234cae4eef6db0c1192b6f9ba9ca946033034b"
}
rule Sanesecurity_MalwareHash_HackingTeam_411
{
    condition:
        hash.md5(0, filesize) == "595e4dc95b391a0566bc8c9d32d352c205d0f8ae19d3842f6d914f0b696f98e2"
}
rule Sanesecurity_MalwareHash_HackingTeam_412
{
    condition:
        hash.md5(0, filesize) == "3e0d7773c6261d5f54924febec163163e9e434fe1b6cb59daff06dff190987c4"
}
rule Sanesecurity_MalwareHash_HackingTeam_413
{
    condition:
        hash.md5(0, filesize) == "3e0d7773c6261d5f54924febec163163e9e434fe1b6cb59daff06dff190987c4"
}
rule Sanesecurity_MalwareHash_HackingTeam_414
{
    condition:
        hash.md5(0, filesize) == "80ea72c70d8de5e111d4be5fa7515c0b35bbdecc2a4997c2930e918bc263c4c1"
}
rule Sanesecurity_MalwareHash_HackingTeam_415
{
    condition:
        hash.md5(0, filesize) == "1c83f67e338502f500d131e6f69c40a23ba4cf94e38e050de578733cd397126b"
}
rule Sanesecurity_MalwareHash_HackingTeam_416
{
    condition:
        hash.md5(0, filesize) == "fb3b9464e866b35b3d7a3b506f967b32e1c2015e0703780c89993ce6d50a0ea6"
}
rule Sanesecurity_MalwareHash_HackingTeam_417
{
    condition:
        hash.md5(0, filesize) == "957fcc2d137e9164635831dd0ab8bca8079ec8b1a4c2eb6e8ac254c5732b025b"
}
rule Sanesecurity_MalwareHash_HackingTeam_418
{
    condition:
        hash.md5(0, filesize) == "bdb971f13959add3ff1f61c2e1ac4368f7a706bec401b8bf81904b27fe6e59fe"
}
rule Sanesecurity_MalwareHash_HackingTeam_419
{
    condition:
        hash.md5(0, filesize) == "bfb2ac272617e4af5ddf176bb4bffcc090e47b1208f4285a7108d6a59ec51837"
}
rule Sanesecurity_MalwareHash_HackingTeam_420
{
    condition:
        hash.md5(0, filesize) == "ddf1371ec3cafedd2caacfa351e9be77e9dbf263f143d6f81a6202e4303ac8d8"
}
rule Sanesecurity_MalwareHash_HackingTeam_421
{
    condition:
        hash.md5(0, filesize) == "fc609adef44b5c64de029b2b2cff22a6f36b6bdf9463c1bd320a522ed39de5d9"
}
rule Sanesecurity_MalwareHash_HackingTeam_422
{
    condition:
        hash.md5(0, filesize) == "d9c55606c757e78940c3a22fc25ae12ed93a68c9f88983e58cd4795047504246"
}
rule Sanesecurity_MalwareHash_HackingTeam_423
{
    condition:
        hash.md5(0, filesize) == "a61c9ae6ac4149619f058a09b83e7ba16bf6bf2492201fa299c25495ef01ba30"
}
rule Sanesecurity_MalwareHash_HackingTeam_424
{
    condition:
        hash.md5(0, filesize) == "5f6bc6573d006609d1f0b5c3d051dc6eb5b30dbc60c4e2e7c7b6826434c6a59b"
}
rule Sanesecurity_MalwareHash_HackingTeam_425
{
    condition:
        hash.md5(0, filesize) == "9f3673b51a622dbe8ea5f92ad37ff12ed0a03ff5c30a9ca20575dca08c624fa3"
}
rule Sanesecurity_MalwareHash_HackingTeam_426
{
    condition:
        hash.md5(0, filesize) == "fc609adef44b5c64de029b2b2cff22a6f36b6bdf9463c1bd320a522ed39de5d9"
}
rule Sanesecurity_MalwareHash_HackingTeam_427
{
    condition:
        hash.md5(0, filesize) == "ad55c2dcf7e3373ea074061d119c891b34e4364cd7f5f679b475b5ec3371592e"
}
rule Sanesecurity_MalwareHash_HackingTeam_428
{
    condition:
        hash.md5(0, filesize) == "6d22dbb5285391be5dcce7a2aed9f14b7ef57de90fd5b02d4bd7ba07d4a5d455"
}
rule Sanesecurity_MalwareHash_HackingTeam_429
{
    condition:
        hash.md5(0, filesize) == "5e75e0babe92f1a7691a43641fadb7be84d4d273b8bcc6cce5dfeb5523a6b709"
}
rule Sanesecurity_MalwareHash_HackingTeam_430
{
    condition:
        hash.md5(0, filesize) == "abbac3dda22f825197dd65b8c1076c5ab8d7ecaa2ce2821b242f63154eafce3a"
}
rule Sanesecurity_MalwareHash_HackingTeam_431
{
    condition:
        hash.md5(0, filesize) == "f3caf8fac708bf3a4ea0511e06849a169e020bdf19c0f8f526abce0b3951fa42"
}
rule Sanesecurity_MalwareHash_HackingTeam_432
{
    condition:
        hash.md5(0, filesize) == "8ee48f42bcc3006d84ac7230f8a3aa811bbe1c0d5120573638c5ffe41af15fe8"
}
rule Sanesecurity_MalwareHash_HackingTeam_433
{
    condition:
        hash.md5(0, filesize) == "b26d39d79b068a7d13fe3526f29bb26165b518fca3b0baf28ad0daa8ed576e85"
}
rule Sanesecurity_MalwareHash_HackingTeam_434
{
    condition:
        hash.md5(0, filesize) == "e4c4086b2cd79453fd7a25db4c079f4ff1d183947499fdd4961f5df8d60a1c6d"
}
rule Sanesecurity_MalwareHash_HackingTeam_435
{
    condition:
        hash.md5(0, filesize) == "5af63dcc0c0330a2de8e4a3f3756e7951d2ce02655f94eee77682653eb6ae949"
}