/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./innominds_deadbits/rules/ACBackdoor_Linux.yara"
include "./innominds_deadbits/rules/APT32_KerrDown.yara"
include "./innominds_deadbits/rules/APT32_Ratsnif.yara"
include "./innominds_deadbits/rules/APT34_LONGWATCH.yara"
include "./innominds_deadbits/rules/APT34_PICKPOCKET.yara"
include "./innominds_deadbits/rules/APT34_VALUEVAULT.yara"
include "./innominds_deadbits/rules/DNSpionage.yara"
include "./innominds_deadbits/rules/Dacls_Linux.yara"
include "./innominds_deadbits/rules/Dacls_Windows.yara"
include "./innominds_deadbits/rules/EvilGnome_Linux.yara"
include "./innominds_deadbits/rules/Glupteba.yara"
include "./innominds_deadbits/rules/JSWorm.yara"
include "./innominds_deadbits/rules/KPOT_v2.yara"
include "./innominds_deadbits/rules/Linux_Golang_Ransomware.yara"
include "./innominds_deadbits/rules/REMCOS_RAT_2019.yara"
include "./innominds_deadbits/rules/RedGhost_Linux.yara"
include "./innominds_deadbits/rules/SilentTrinity_Delivery.yara"
include "./innominds_deadbits/rules/SilentTrinity_Payload.yara"
include "./innominds_deadbits/rules/TA505_FlowerPippi.yara"
include "./innominds_deadbits/rules/WatchBog_Linux.yara"
include "./innominds_deadbits/rules/avemaria_warzone.yara"
include "./innominds_deadbits/rules/crescentcore_dmg.yara"
include "./innominds_deadbits/rules/godlua_linux.yara"
include "./innominds_deadbits/rules/winnti_linux.yara"
