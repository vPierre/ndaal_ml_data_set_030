
rule SUSP_Netscaler_Forensic_Artefacts {
   meta:
      description = "Detects strings / forensic artefacts on exploited Netscaler systems"
      author = "Florian Roth"
      reference = "https://www.trustedsec.com/blog/netscaler-remote-code-execution-forensics/"
      date = "2020-01-14"
      score = 70
    strings:
      $ = "shell_command=\"whoami\"" ascii
      $ = "shell_command=\"hostname\"" ascii
      $ = "shell_command=\"uname\"" ascii
      $ = "/vpn/../vpns/portal/scripts/" ascii
      $ = "cat /var/nstmp/sess_" ascii          /* https://twitter.com/buffaloverflow/status/1216808338450677760 */
      $ = "cat /flash/nsconfig/ns.conf" ascii   /* https://twitter.com/buffaloverflow/status/1216807963974938624 */
      $ = "<?php @eval(base64_decode(" ascii    /* https://www.reddit.com/r/blueteamsec/comments/en4m7j/multiple_exploits_for_cve201919781_citrix/ */
      $ = ".pl HTTP/1.1\" 200 143" ascii        /* https://twitter.com/ItsReallyNick/status/1216821591281225729 */
      $ = ".pl HTTP/1.1\" 200 135" ascii        /* https://twitter.com/ItsReallyNick/status/1216821591281225729 */
   condition:
      1 of them
}