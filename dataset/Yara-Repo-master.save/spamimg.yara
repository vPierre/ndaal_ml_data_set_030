import "hash"

rule Sanesecurity_SpamImg_001
{
    condition:
        hash.md5(0, filesize) == "228de98135d15406f603d1b59a8f2a7c802601d357a3c3983a0001a2698bb1dc"
}
rule Sanesecurity_SpamImg_002
{
    condition:
        hash.md5(0, filesize) == "a8c1fb1d0577a55993a0b4792344452fa2bd398b993c7379c78bc2c0a03307ed"
}
rule Sanesecurity_SpamImg_003
{
    condition:
        hash.md5(0, filesize) == "aba4d23dd9565251da8b0f6b2e8d7ac86aa758a9c9e932862e7cdc709ce08d2f"
}
rule Sanesecurity_SpamImg_004
{
    condition:
        hash.md5(0, filesize) == "e24ae10b0dee4a80f91b19395d87e10dcffc816e0ed7b3308f4b27f68557d8f1"
}
rule Sanesecurity_SpamImg_005
{
    condition:
        hash.md5(0, filesize) == "5ef2cdf473f497a611f831aef349c13b3c623b7b18cea3a4df8a1b544644782e"
}
rule Sanesecurity_SpamImg_006
{
    condition:
        hash.md5(0, filesize) == "4b13884345ea812d775905d397ad12e78c05f7c263a80ba8c8743943fe468286"
}
rule Sanesecurity_SpamImg_007
{
    condition:
        hash.md5(0, filesize) == "6a4166824bd2e9958254692d4223a329fbdcbc950668acf105b0fca13b53768a"
}
rule Sanesecurity_SpamImg_008
{
    condition:
        hash.md5(0, filesize) == "4377854cb45a94fc0331f873f96efd567a6f02b6effdaa6438b1d4acb0aa9edd"
}
rule Sanesecurity_SpamImg_009
{
    condition:
        hash.md5(0, filesize) == "bed62ad68dd7903bc44db410ce962d46f7912729261e5684bc31ff884edfdcdd"
}
rule Sanesecurity_SpamImg_010
{
    condition:
        hash.md5(0, filesize) == "b68a2b9654c7bb7dae9ec1551cbfade3655129eaf2d3705403d9dff93db361d5"
}
rule Sanesecurity_SpamImg_011
{
    condition:
        hash.md5(0, filesize) == "bed62ad68dd7903bc44db410ce962d46f7912729261e5684bc31ff884edfdcdd"
}
rule Sanesecurity_SpamImg_012
{
    condition:
        hash.md5(0, filesize) == "2f5f336f48dbbe89fdd64f141c9ec55cbf68614259e3321cd4b48d72558bcfbd"
}
rule Sanesecurity_SpamImg_013
{
    condition:
        hash.md5(0, filesize) == "de552215cdb34929791514034270a56d7e33bcd29277b3b684b78e050684bb63"
}
rule Sanesecurity_SpamImg_014
{
    condition:
        hash.md5(0, filesize) == "a68e6f964049a627f90da7cb3c593356afdd20c7c8f03d22e0c64bacddbb9c65"
}
rule Sanesecurity_SpamImg_015
{
    condition:
        hash.md5(0, filesize) == "d8cd09112b865cd8944d88f8a47eaadb3203948129436e25b56f4758bd334034"
}
rule Sanesecurity_SpamImg_016
{
    condition:
        hash.md5(0, filesize) == "24337eba28f94154bda75be16451b7594b1ed9830e98f74c9f89a2481cb09f60"
}
rule Sanesecurity_SpamImg_017
{
    condition:
        hash.md5(0, filesize) == "391b838345d14c2e299cc57be6ef5bd4ba18902c78fec79038296b174e6463a9"
}
rule Sanesecurity_SpamImg_018
{
    condition:
        hash.md5(0, filesize) == "0fb14707b7f87efae3af46f00bcb8c4c7d41b8ddd225512fedc46b506470bcd3"
}
rule Sanesecurity_SpamImg_019
{
    condition:
        hash.md5(0, filesize) == "d95f2b4bf758c5034b8a4531bb2a7ae536af3b81333cc4f475d3101e4520c728"
}
rule Sanesecurity_SpamImg_020
{
    condition:
        hash.md5(0, filesize) == "eab99873a41ed29de7829d47f20163d9e12ffef916060ceb1347c9426177ca7e"
}
rule Sanesecurity_SpamImg_021
{
    condition:
        hash.md5(0, filesize) == "302e5aa2df5467f4095ae5142187a275f1814931885e3ff9e26d183c4ce6802d"
}
rule Sanesecurity_SpamImg_022
{
    condition:
        hash.md5(0, filesize) == "19849bba5fb349913580d17463e731a2f319f6e169234065f7a1a418456abf1c"
}
rule Sanesecurity_SpamImg_023
{
    condition:
        hash.md5(0, filesize) == "865ccf41600b8c7982c4fb6d268233e5f662afaea370251ed433a1bcf61e1215"
}
rule Sanesecurity_SpamImg_024
{
    condition:
        hash.md5(0, filesize) == "1269173db246f0fa1c8f849949e5125971eb1667cf7487977c3a1cfb5378b806"
}
rule Sanesecurity_SpamImg_025
{
    condition:
        hash.md5(0, filesize) == "d330d4b0a4b28a351c6e495e9a4fac43e2d57ee8dc601d0e58791a4c21a172e5"
}
rule Sanesecurity_SpamImg_026
{
    condition:
        hash.md5(0, filesize) == "302e5aa2df5467f4095ae5142187a275f1814931885e3ff9e26d183c4ce6802d"
}
rule Sanesecurity_SpamImg_027
{
    condition:
        hash.md5(0, filesize) == "89f75ccda9729a1cdf86ededf6a0608842bff3ee436fe1ac244385888c60002c"
}
rule Sanesecurity_SpamImg_028
{
    condition:
        hash.md5(0, filesize) == "6ca87dbf357822c439bd9d72319e207bdf296bc44b520745d617551a1fb00d89"
}
rule Sanesecurity_SpamImg_029
{
    condition:
        hash.md5(0, filesize) == "000680a67d464b17be95bca8b73de979a848709b27eb12e535e419b025bb5bbc"
}
rule Sanesecurity_SpamImg_030
{
    condition:
        hash.md5(0, filesize) == "b4b58ebb7f220c71de97767b5cee7dee251a5fe9d622485dbb89aa4628318978"
}
rule Sanesecurity_SpamImg_031
{
    condition:
        hash.md5(0, filesize) == "df36d78838155bccf3dd103f60be94c68a560d3a50212bd174af6317173a5a7b"
}
rule Sanesecurity_SpamImg_032
{
    condition:
        hash.md5(0, filesize) == "24a7d0eff8ce689ab63edb346675b143834899d1d054bf9aed289e497b941780"
}
rule Sanesecurity_SpamImg_032
{
    condition:
        hash.md5(0, filesize) == "33b3a68ab8bf751e6e8e7556b8d80afc478f710c0056bfa6c454cc6c3287dcbd"
}
rule Sanesecurity_SpamImg_034
{
    condition:
        hash.md5(0, filesize) == "078b8e455245b2906f1a2f1661aa045152465e1a44b2cab842968101e3e58884"
}
rule Sanesecurity_SpamImg_035
{
    condition:
        hash.md5(0, filesize) == "0de13750bef183342f72ca1155d01f1078ae7ee9e23633727b68ab1485a1abb2"
}
rule Sanesecurity_SpamImg_036
{
    condition:
        hash.md5(0, filesize) == "bb731a9742a0cea4e5607a1577942f9fd35e90a4ee0f1b8f0869d82e9e41a23f"
}
rule Sanesecurity_SpamImg_037
{
    condition:
        hash.md5(0, filesize) == "94a54daec9783e573d79f935e820f83b69264dad956204e6625f347bbe86afac"
}
rule Sanesecurity_SpamImg_038
{
    condition:
        hash.md5(0, filesize) == "910180035e2cd3955a7b711b9b5105aa03a19789d2fcd5c64c1dae0e551aba0d"
}
rule Sanesecurity_SpamImg_039
{
    condition:
        hash.md5(0, filesize) == "67a9660f8eed9e447e7a8fdf8471908ba25b34893c0b5dc5b58c537bfee1ad73"
}
rule Sanesecurity_SpamImg_040
{
    condition:
        hash.md5(0, filesize) == "65815799bb4bc0e72c6e5ada0a3a69ae459d680509e2668495524074e82a11b0"
}
rule Sanesecurity_SpamImg_041
{
    condition:
        hash.md5(0, filesize) == "36ce8ccfa7a58cee516a539ba5faf59d6157af216b54eba4a469903870e1dd76"
}
rule Sanesecurity_SpamImg_042
{
    condition:
        hash.md5(0, filesize) == "628a688c9173fcd5c0ca956621414b7fd741ed7795aad6fa1ae53a7cc7efcc5e"
}
rule Sanesecurity_SpamImg_043
{
    condition:
        hash.md5(0, filesize) == "d1fd807e6e34da2f915c64a02fd43db10ca1feeddece137eed5d1a0278f6add8"
}
rule Sanesecurity_SpamImg_044
{
    condition:
        hash.md5(0, filesize) == "4f5b3c979742b0d2e457b8bc98a2dabc91b4e291740e4389d1789bc4c5c31b1f"
}
rule Sanesecurity_SpamImg_045
{
    condition:
        hash.md5(0, filesize) == "bdaa975cf7611813f97b899ed1b3869eb25916ec0045bb85b9d85f22759156de"
}
rule Sanesecurity_SpamImg_046
{
    condition:
        hash.md5(0, filesize) == "aa3698870e3c515d55e1d5840c7991c4205a15be0f5e5f3e58130a4860385e02"
}
rule Sanesecurity_SpamImg_047
{
    condition:
        hash.md5(0, filesize) == "adbd7a0a886977fb2b633c76ecb712fd7d14636646b98db80298dc242743b502"
}
rule Sanesecurity_SpamImg_048
{
    condition:
        hash.md5(0, filesize) == "957be8e6ff3af8f8421de09cf1b40afd6dc396d54538d25bc3c4717f4de69a28"
}
rule Sanesecurity_SpamImg_049
{
    condition:
        hash.md5(0, filesize) == "d1791aec955ac9fbf6106520968afec7b9aa7f380900d59f1be90b6def39b1ab"
}
rule Sanesecurity_SpamImg_050
{
    condition:
        hash.md5(0, filesize) == "68fe78cc36a53f6d90e87e9f404d75f3552cc7d03077c93ae9fef2072808c5db"
}
rule Sanesecurity_SpamImg_051
{
    condition:
        hash.md5(0, filesize) == "0324dba809b0191edc7e68f63376b3021bcf171408a92f1243b05ce962d05cbd"
}
rule Sanesecurity_SpamImg_052
{
    condition:
        hash.md5(0, filesize) == "ba4d27329ba8335c9a11c6b426f8da542a79c3c6a34aa06f2f5df8b2d4cf9ccb"
}
rule Sanesecurity_SpamImg_053
{
    condition:
        hash.md5(0, filesize) == "323ab9ba0806843475e148fce5d76e9714b6a9dd9f958cbf779f0d6bd7352469"
}
rule Sanesecurity_SpamImg_054
{
    condition:
        hash.md5(0, filesize) == "417fb1768d681d50d208a4be2cc6d87b80fc3f6b893ce2fd522ec7d8b4a7a175"
}
rule Sanesecurity_SpamImg_055
{
    condition:
        hash.md5(0, filesize) == "0c0fc507d3777297fd3de2738958de6b4c20e0ded093e86aa88e595617a7756b"
}
rule Sanesecurity_SpamImg_056
{
    condition:
        hash.md5(0, filesize) == "3f2972a82403e812e9368ef98ea2311ea851407dc06bf12c5ed6ce8738e38677"
}
rule Sanesecurity_SpamImg_057
{
    condition:
        hash.md5(0, filesize) == "044f86f8a331ca877151c036b2149b328f49352ccd04b09eff2b7af62ca34e5d"
}
rule Sanesecurity_SpamImg_058
{
    condition:
        hash.md5(0, filesize) == "c739b8cab685ecfb74b7abd1faaf8f51233990a46211ff5e21ebece75081f7eb"
}
rule Sanesecurity_SpamImg_059
{
    condition:
        hash.md5(0, filesize) == "bfecac534d44f9c367d3d6378cbb9ae2db4dc4aefc8962d3cf4a1acc07904d1e"
}
rule Sanesecurity_SpamImg_060
{
    condition:
        hash.md5(0, filesize) == "8e56b4984fc397a65e47d4497c554754719c3874f8ffec82b2f4ef682f941f52"
}
rule Sanesecurity_SpamImg_061
{
    condition:
        hash.md5(0, filesize) == "fc27f63c1fee6abcbc4df91571be74c2b9db046a937e38aa06af4af52465ceb4"
}
rule Sanesecurity_SpamImg_062
{
    condition:
        hash.md5(0, filesize) == "13115d03d2549cebf63aa7e59ff6d43db87fb04860fa50e2fd77a30a36c74658"
}
rule Sanesecurity_SpamImg_063
{
    condition:
        hash.md5(0, filesize) == "250bd8c0004f1e47874e7f76fa2beebff40d7054a17e80ecea4aa15974873844"
}
rule Sanesecurity_SpamImg_064
{
    condition:
        hash.md5(0, filesize) == "3923b066842d6a55d067c1473ef382811eee47740378d0aaa87ada4b10527d6a"
}
rule Sanesecurity_SpamImg_065
{
    condition:
        hash.md5(0, filesize) == "1188e3e61eaf3487bb8ba1c871d436f37d294583a15af3a5cc3c42844648d592"
}
rule Sanesecurity_SpamImg_066
{
    condition:
        hash.md5(0, filesize) == "b5226c0d99f65568cfb6391c8cf8aaea710dc178fe00d2dbf94058f80df9abbe"
}
rule Sanesecurity_SpamImg_067
{
    condition:
        hash.md5(0, filesize) == "1bb0c288bfa52407aa52c3d5a6c7c6435e1bd234393f65e7cd083e25b6123754"
}
rule Sanesecurity_SpamImg_068
{
    condition:
        hash.md5(0, filesize) == "694aacd1324445a63a71c76fa69ecf171d7a38654158c03c0ee6b55a8657add6"
}
rule Sanesecurity_SpamImg_069
{
    condition:
        hash.md5(0, filesize) == "f57aea35d6e6d19e0cb60c590531cd0901dbf538e14aedfddc5330aeda678c37"
}
rule Sanesecurity_SpamImg_070
{
    condition:
        hash.md5(0, filesize) == "e8bab04119454b67cc4ae48e428e821859e471f34385a26e5723956c91561f7c"
}
rule Sanesecurity_SpamImg_071
{
    condition:
        hash.md5(0, filesize) == "9797f526a45b712de450cf4b80e1f48527af52110159948ed1793ef37c9b05fb"
}
rule Sanesecurity_SpamImg_072
{
    condition:
        hash.md5(0, filesize) == "6df6d97e26f5e2ae011272c0ef98581db3055c17c61e4700edaaed2b2dcbf516"
}
rule Sanesecurity_SpamImg_073
{
    condition:
        hash.md5(0, filesize) == "029fe9035421bd49ec1b265920d2a2384fd54244881aa62fa923fef43d2daf3e"
}
rule Sanesecurity_SpamImg_074
{
    condition:
        hash.md5(0, filesize) == "457e91c815aac95b0b459b734eb211a8507184c64eed9802467066487c5a2145"
}
rule Sanesecurity_SpamImg_075
{
    condition:
        hash.md5(0, filesize) == "02b99392b4ea339e912a6b557688bce7712e993a61f01b9c2ebfe93c426986f4"
}
rule Sanesecurity_SpamImg_076
{
    condition:
        hash.md5(0, filesize) == "037e7555b253dbd8a3a9b0287e4437a8201f51e93679cea0d10e41183bdab5c0"
}
rule Sanesecurity_SpamImg_077
{
    condition:
        hash.md5(0, filesize) == "0db6f9a454abc8abefcaebff00895928ca4be67e4e3121ba14512ad2b4f10015"
}
rule Sanesecurity_SpamImg_078
{
    condition:
        hash.md5(0, filesize) == "1635a006e0e346b5309976679905023705c0a22a6731d2647b8f6419ac9296b8"
}
rule Sanesecurity_SpamImg_079
{
    condition:
        hash.md5(0, filesize) == "3a3ca447127269c576704e21e65321ea86ea4f26f490370071a2f720494df500"
}
rule Sanesecurity_SpamImg_080
{
    condition:
        hash.md5(0, filesize) == "3bb7b4b7ee4d780fa184aa5d30071425f6a177acd00ac3c19b2e41f47a05d0ff"
}
rule Sanesecurity_SpamImg_081
{
    condition:
        hash.md5(0, filesize) == "470a3659b88556837c2e3149601c3a8ed02143bd3970208dfa6b0b35ca1f884b"
}
rule Sanesecurity_SpamImg_082
{
    condition:
        hash.md5(0, filesize) == "5ce3cf8c612489c144afe13f05b053dd16f72d9541f8aadcdcf5b734e8af0d18"
}
rule Sanesecurity_SpamImg_083
{
    condition:
        hash.md5(0, filesize) == "a126ef9b45dfb268b7262f24cb943cc628a8a0e3dbaa4720ce7ca97046264354"
}
rule Sanesecurity_SpamImg_084
{
    condition:
        hash.md5(0, filesize) == "b47257c8809c7e38a5fb39876414cea21f888c9580c3e37dc9554ab33dd1eb48"
}
rule Sanesecurity_SpamImg_085
{
    condition:
        hash.md5(0, filesize) == "bd4f3b6616ca840497c6a32156ab5e91ad48d8124fd7e2cc959af401e5e19e2b"
}
rule Sanesecurity_SpamImg_086
{
    condition:
        hash.md5(0, filesize) == "c4391844d6c7c38cf9175d6c6ba40f0feb8b8515705125353a1a0bb5aada7dda"
}
rule Sanesecurity_SpamImg_087
{
    condition:
        hash.md5(0, filesize) == "e853893f974ea7eda6e5809839149dd2db5b0b0e854f3feabba60d3d062624ce"
}
rule Sanesecurity_SpamImg_088
{
    condition:
        hash.md5(0, filesize) == "fa6a6c015f0d9815e1015bda4088f8dcf0933e65a3aed47689af5605df59df4f"
}
rule Sanesecurity_SpamImg_089
{
    condition:
        hash.md5(0, filesize) == "0697ad45bc05af35ab8b49d30b1f5f25d0049d0b149c693c77cfa3837661a452"
}
rule Sanesecurity_SpamImg_090
{
    condition:
        hash.md5(0, filesize) == "07ea2f2ce40636fe554cb9040d717ea41991f38830bfb4976a3282d949fd7e06"
}
rule Sanesecurity_SpamImg_091
{
    condition:
        hash.md5(0, filesize) == "099a0bafc09ac097116026f8b7731c905154cdf2848fe76ff693ad772bb128f1"
}
rule Sanesecurity_SpamImg_092
{
    condition:
        hash.md5(0, filesize) == "26b482ee9ba4b11f6e40a0c3a2fb0046c83e74147d2ff4eeeac62c8d1693cf4c"
}
rule Sanesecurity_SpamImg_093
{
    condition:
        hash.md5(0, filesize) == "27033adfa4ad31405591d0febefad086e5a05e7553e69ffa5e135539d9d66b72"
}
rule Sanesecurity_SpamImg_094
{
    condition:
        hash.md5(0, filesize) == "3aa4a32954d83ffe62248f9920c451add09f8406792cbcbc70cd1b291188c35d"
}
rule Sanesecurity_SpamImg_095
{
    condition:
        hash.md5(0, filesize) == "3ebe3dccb189f3bcae0f7f69c7c113a22fe34f41a8b88e300fdff92010753475"
}
rule Sanesecurity_SpamImg_096
{
    condition:
        hash.md5(0, filesize) == "411c1b17d31bdd16acd1f872eb7f4abe5bf7fc7946c3790447a71e072b100cbd"
}
rule Sanesecurity_SpamImg_097
{
    condition:
        hash.md5(0, filesize) == "4838b6d9df7b0b6c72d27e6a5cef43a3a5b9a13198d87a7f1ac448456fcf4d32"
}
rule Sanesecurity_SpamImg_098
{
    condition:
        hash.md5(0, filesize) == "530dc4d1815134bcc417ab06506cfa8c103a71439ff8a1dcd5263dd007e89aed"
}
rule Sanesecurity_SpamImg_099
{
    condition:
        hash.md5(0, filesize) == "5f9366eab6828d371c25033791680320a2aa7671f203ffa78fdc7df722394851"
}
rule Sanesecurity_SpamImg_100
{
    condition:
        hash.md5(0, filesize) == "acaaf84ac51e614b9567391e3117a5d60e244ed9429d71d557c9cdb481e94859"
}
rule Sanesecurity_SpamImg_101
{
    condition:
        hash.md5(0, filesize) == "acff855df4e91f30f765cd728817c51900c729df14b03f0ec56bb69b8d418cd9"
}
rule Sanesecurity_SpamImg_102
{
    condition:
        hash.md5(0, filesize) == "ae1b3b9a69d84cfb00ba39e07540471a7a288b07b7c81498ff3593ed4c7f7e72"
}
rule Sanesecurity_SpamImg_103
{
    condition:
        hash.md5(0, filesize) == "b37ff8831a410fdfba5e37b940681af475611aa4d7ebafdafa734b4d5e9fa811"
}
rule Sanesecurity_SpamImg_104
{
    condition:
        hash.md5(0, filesize) == "b380b70c2ef545bbb77ccf23e77163da191110af5d1f6a1d0d0409d4ab4251c7"
}
rule Sanesecurity_SpamImg_105
{
    condition:
        hash.md5(0, filesize) == "b432d6435ce38b55db95bbc1f21ecab0a741618748884925e3fb09ae67c460ae"
}
rule Sanesecurity_SpamImg_106
{
    condition:
        hash.md5(0, filesize) == "bc0c6fe3e15df2f0d56fbf7928a024bd1f0f540090ff9289b6b87553f11f2f2b"
}
rule Sanesecurity_SpamImg_107
{
    condition:
        hash.md5(0, filesize) == "bd147073e03881a55a5b2da0d1e03399871bc58a55ab836cce3ac96484e68e50"
}
rule Sanesecurity_SpamImg_108
{
    condition:
        hash.md5(0, filesize) == "cf8ab2b92ada64d33d43b55824de0c673cb99bbfd9093bca53ad0341eba0b55e"
}
rule Sanesecurity_SpamImg_109
{
    condition:
        hash.md5(0, filesize) == "d66d0be60bdfb5d70a560c0cacf05babae0723fe24f3f2dc8ced87f81e3b4bf9"
}
rule Sanesecurity_SpamImg_110
{
    condition:
        hash.md5(0, filesize) == "e042dfed5542ee0784a117d48afcc692522eeecf10ffa56d70531bc2898d7bfd"
}
rule Sanesecurity_SpamImg_111
{
    condition:
        hash.md5(0, filesize) == "e133e8364f0d910dfeffa55d78d2f00c06fab2719a504f98478115ca70665a00"
}
rule Sanesecurity_SpamImg_112
{
    condition:
        hash.md5(0, filesize) == "f9090fd32488d44eeee314b1f7f1bf3c664d7d352da3c866b07c676f4164a38a"
}
rule Sanesecurity_SpamImg_113
{
    condition:
        hash.md5(0, filesize) == "72099071f45c13cf4b583651c4596ee234467883a8bdd65c7e9972aa126c6812"
}
rule Sanesecurity_SpamImg_114
{
    condition:
        hash.md5(0, filesize) == "c2f59c1ccaed9f0bd378e5c2a8d01b14d48908e76427ae5ff95a7a3f030172a3"
}
rule Sanesecurity_SpamImg_115
{
    condition:
        hash.md5(0, filesize) == "ce8855b6126398ce6347da8a70ef3092f15d2250a01ae0fe142b475264493e13"
}
rule Sanesecurity_SpamImg_116
{
    condition:
        hash.md5(0, filesize) == "004d38dbdf0f18422739c64835acebaa72cedd5c2bd3ba092f08a20c40b6d1c0"
}
rule Sanesecurity_SpamImg_117
{
    condition:
        hash.md5(0, filesize) == "0051f26ac3571411161b1ec0bb8d8f84270099723ee9775a28fdab82093a88cc"
}
rule Sanesecurity_SpamImg_118
{
    condition:
        hash.md5(0, filesize) == "05fa784ce481bc34566c5969d0c51046e3c08df64e5fde778f1aae03c66459a3"
}
rule Sanesecurity_SpamImg_119
{
    condition:
        hash.md5(0, filesize) == "1935c00579853afd3011bbc0177a57b20cafd591ad227e79132b87354289b2d5"
}
rule Sanesecurity_SpamImg_120
{
    condition:
        hash.md5(0, filesize) == "38cf98d875cda859212dc41dcd26ae20053036f9ccb339c367066d963d57d482"
}
rule Sanesecurity_SpamImg_121
{
    condition:
        hash.md5(0, filesize) == "3a52560d330ece5b4d6ef8f57e2f150e56bb101c283ff79899590762518b877b"
}
rule Sanesecurity_SpamImg_122
{
    condition:
        hash.md5(0, filesize) == "72b1bd557bc76605049ac915c910c3944465d755293ce8b7987d50f44d108c9a"
}
rule Sanesecurity_SpamImg_123
{
    condition:
        hash.md5(0, filesize) == "eb970c9ddbbf820b3229191bab38241ca30213327315ccfe6765ea69ea1b21ae"
}
rule Sanesecurity_SpamImg_124
{
    condition:
        hash.md5(0, filesize) == "f7be3a531b3c1087661f92a31445d42cefa32f9b9eb5e551b90782fc3b908d1c"
}
rule Sanesecurity_SpamImg_125
{
    condition:
        hash.md5(0, filesize) == "f833265b5ef817305900c11690b870fcece6e9321755e7339b3fe0976b6ba1fd"
}
rule Sanesecurity_SpamImg_126
{
    condition:
        hash.md5(0, filesize) == "1a4565015eec63d89da1d2ad7e4a7033c5c3ebbad072f5be15fca592be36deb8"
}
rule Sanesecurity_SpamImg_127
{
    condition:
        hash.md5(0, filesize) == "28c2687778566bb7dabf1e73466c1f821cc0df39fc07b4abae90f707ea23fe28"
}
rule Sanesecurity_SpamImg_128
{
    condition:
        hash.md5(0, filesize) == "394866cdc6d6406226547185e874a893bd14dc6a8de6420c5668e706f01f886b"
}
rule Sanesecurity_SpamImg_129
{
    condition:
        hash.md5(0, filesize) == "624fc9924da2b094e1f1fac96f2273983db23e581241fe08c223de8860295996"
}
rule Sanesecurity_SpamImg_130
{
    condition:
        hash.md5(0, filesize) == "65643893d291bb9f4498faba2409a53cca5a066a38c0661fd3f07e018a4e3adb"
}
rule Sanesecurity_SpamImg_131
{
    condition:
        hash.md5(0, filesize) == "7ae28694d5781f80bc87c2f14bfb93db17c1791a0b89bf53683b3ec61b3850a5"
}
rule Sanesecurity_SpamImg_132
{
    condition:
        hash.md5(0, filesize) == "9e5cd7666897b1ce4cbd8db71e6f584a6e013c083d952c671c817aec9abed9db"
}
rule Sanesecurity_SpamImg_133
{
    condition:
        hash.md5(0, filesize) == "a0fe1c76805b073e995748439fa1801a5cec5972844bbe8dd6b06e42abe91c55"
}
rule Sanesecurity_SpamImg_134
{
    condition:
        hash.md5(0, filesize) == "a5bf1bbfc7eaf17d5cb27c146d3b95626624fa7fc44d032d80c55f8ac2574db5"
}
rule Sanesecurity_SpamImg_135
{
    condition:
        hash.md5(0, filesize) == "aaf8c5bdc8421bb0321242653a6da7e1a7c5aee79f30cc234cca18a510cfb40e"
}
rule Sanesecurity_SpamImg_136
{
    condition:
        hash.md5(0, filesize) == "b5444b70aac608e9fe417247a4cd79125cf412ab32dc1b4bdd09a0175a271029"
}
rule Sanesecurity_SpamImg_137
{
    condition:
        hash.md5(0, filesize) == "d19a1de6ad4a3459c617408a85fa3cbed64139881446dc9322de5674c8be000e"
}
rule Sanesecurity_SpamImg_138
{
    condition:
        hash.md5(0, filesize) == "f0c14fa275a68dc68da0585982f0706bfff0958f0ba1404aec016334010c30bb"
}
rule Sanesecurity_SpamImg_139
{
    condition:
        hash.md5(0, filesize) == "7a56f4df7a9386367eaa9357d262e58634b7bbf2e9092f08a9dc3921c8f14631"
}
rule Sanesecurity_SpamImg_140
{
    condition:
        hash.md5(0, filesize) == "c3e3ec81cf94f8d25ad0f366d6596944ee4051ce89f67f790a870fdbfae21654"
}
rule Sanesecurity_SpamImg_141
{
    condition:
        hash.md5(0, filesize) == "16e9cc5501b3dfb4cc9767475c120e451359241e2c456c7a3567c521a36eab56"
}
rule Sanesecurity_SpamImg_142
{
    condition:
        hash.md5(0, filesize) == "fd82214a63a35633120741d5839431e050182bc8b91c37e8e9c9746b924dd00a"
}
rule Sanesecurity_SpamImg_143
{
    condition:
        hash.md5(0, filesize) == "fa1a046a5543cf7377ba0c7a213f805c17f24105582e439b720d3b631075273c"
}
rule Sanesecurity_SpamImg_144
{
    condition:
        hash.md5(0, filesize) == "9cf9b1cf31a68d13df044eaf978db5b4d8a131a1a1f27d25a5bf8da7104b9d74"
}
rule Sanesecurity_SpamImg_145
{
    condition:
        hash.md5(0, filesize) == "5d878540a7ae75f13bbe62667fb9c3c0688d1393fb55a767e33ad856b524d164"
}
rule Sanesecurity_SpamImg_146
{
    condition:
        hash.md5(0, filesize) == "72099071f45c13cf4b583651c4596ee234467883a8bdd65c7e9972aa126c6812"
}
rule Sanesecurity_SpamImg_147
{
    condition:
        hash.md5(0, filesize) == "0d96f75abbdbddabc3ad6890c4eacbfe2ebb2ee5bcabc156a9d7e2ebfe070e7a"
}
rule Sanesecurity_SpamImg_148
{
    condition:
        hash.md5(0, filesize) == "9dcff58211004986896f435b2afb04f68083ebba5995424519b11768b8ba43f1"
}
rule Sanesecurity_SpamImg_149
{
    condition:
        hash.md5(0, filesize) == "a5e8c8435b990eefc00eb2018efea6bfae4557a2fc42a7c1829fdc3db5d157e4"
}
rule Sanesecurity_SpamImg_150
{
    condition:
        hash.md5(0, filesize) == "61e07b868c80baaffad4119c2b673188f2a9b41638d2df66ca860fa437b1c3ce"
}
rule Sanesecurity_SpamImg_151
{
    condition:
        hash.md5(0, filesize) == "630d500935e3758f3e2950a32453ae49261bac0351e572978cc1f788efaf3865"
}
rule Sanesecurity_SpamImg_152
{
    condition:
        hash.md5(0, filesize) == "b76b656348b4080abcc6aef94ddeb1f0c90081eb4f38d01873789918d949c7d5"
}
rule Sanesecurity_SpamImg_153
{
    condition:
        hash.md5(0, filesize) == "222659dca4b4f60891154919a6b0f9dc6b6e008dd77a0cdd8d7e6bc2289c6ecb"
}
rule Sanesecurity_SpamImg_154
{
    condition:
        hash.md5(0, filesize) == "b5fe1c5c723546b311b68608006f6063de7966e9be1a6f6a2c0be7d4efc7c390"
}
rule Sanesecurity_SpamImg_155
{
    condition:
        hash.md5(0, filesize) == "e6405f0871fb0f038569abe253c687b78edda5cb00ceed09f430e71ba4efbd21"
}
rule Sanesecurity_SpamImg_156
{
    condition:
        hash.md5(0, filesize) == "bb3bdcd954345164c73a5ed64e71ed04aa7078edeb0c03cec736d4698be9ba04"
}
rule Sanesecurity_SpamImg_157
{
    condition:
        hash.md5(0, filesize) == "172b6b416c31a3154404fb479010e733bb0e3e5059068f59be3566cf88ee2ac6"
}
rule Sanesecurity_SpamImg_158
{
    condition:
        hash.md5(0, filesize) == "47e579106402ba8461f7aa80e577d4f1183a9242a68829f30d701fb352f575f6"
}
rule Sanesecurity_SpamImg_159
{
    condition:
        hash.md5(0, filesize) == "5fbb8c8845cc898589298ac1484540a80a01e2f1ba4db323d8ab891a216a2255"
}
rule Sanesecurity_SpamImg_160
{
    condition:
        hash.md5(0, filesize) == "44440c504c57b965f1365ddfa8fb676dc40263b514f4f6eb27208f13b5abd7ba"
}
rule Sanesecurity_SpamImg_161
{
    condition:
        hash.md5(0, filesize) == "755581901312d06b518bed05546546f55e583ddf0e3e7f9380e62ac543c5afaa"
}
rule Sanesecurity_SpamImg_162
{
    condition:
        hash.md5(0, filesize) == "7e1fe82bb0b159f82aae2f064ecfc01b9e69627408db2ec593b2fc49f5746795"
}
rule Sanesecurity_SpamImg_163
{
    condition:
        hash.md5(0, filesize) == "a5e8c8435b990eefc00eb2018efea6bfae4557a2fc42a7c1829fdc3db5d157e4"
}
rule Sanesecurity_SpamImg_164
{
    condition:
        hash.md5(0, filesize) == "e11097bc5bb009e1c93dd70289b6610af372cb475dffa7c3e771e263f4622e21"
}
rule Sanesecurity_SpamImg_165
{
    condition:
        hash.md5(0, filesize) == "a5e8c8435b990eefc00eb2018efea6bfae4557a2fc42a7c1829fdc3db5d157e4"
}
rule Sanesecurity_SpamImg_166
{
    condition:
        hash.md5(0, filesize) == "c213ea7da8b192ceadef4c476aa9c4a7af3848f85a8c7fa3cd58072ec5a06483"
}
rule Sanesecurity_SpamImg_167
{
    condition:
        hash.md5(0, filesize) == "a6140139a7583d6b6ee621ee1b0f7b71649a88e03fcacf40e7cbba8f635b093e"
}
rule Sanesecurity_SpamImg_168
{
    condition:
        hash.md5(0, filesize) == "bb3bdcd954345164c73a5ed64e71ed04aa7078edeb0c03cec736d4698be9ba04"
}
rule Sanesecurity_SpamImg_169
{
    condition:
        hash.md5(0, filesize) == "d45d151888cad97f29135c62086b0b8af98090edb239252645f41c4fa89b38c1"
}
rule Sanesecurity_SpamImg_170
{
    condition:
        hash.md5(0, filesize) == "392830b1ccb141ac57d017f4257f6523dce3eb5777ef89228388ba95f0f48e40"
}
rule Sanesecurity_SpamImg_171
{
    condition:
        hash.md5(0, filesize) == "139b65560b91127e05ff0c14f260373b57b32f986ac7b4ab9eaa42c9a59cf9b2"
}
rule Sanesecurity_SpamImg_172
{
    condition:
        hash.md5(0, filesize) == "6dbc1016a8f8a6a3ff0cba2ac9c8d0e03b7446a164f6e12dafb4c2a53bac045d"
}
rule Sanesecurity_SpamImg_173
{
    condition:
        hash.md5(0, filesize) == "78ec6e2b1379e8bf7d2a88325ea84374e9c656bed01fa1ff39f96bb64727a67f"
}
rule Sanesecurity_SpamImg_174
{
    condition:
        hash.md5(0, filesize) == "83c0fd84426b333e892e57eb87cad1054b6ac856398d713280021c7e2ab063d5"
}
rule Sanesecurity_SpamImg_175
{
    condition:
        hash.md5(0, filesize) == "8a22768c23db210ef92f6ae8fb5ffa4b63c044dc51d18cecdc0a1c0cfe00f8e2"
}
rule Sanesecurity_SpamImg_176
{
    condition:
        hash.md5(0, filesize) == "9c9e32e8e48fdabd524bf5eef92ca789ddc39b9a040d47abf0f37e6497e1dc2b"
}
rule Sanesecurity_SpamImg_177
{
    condition:
        hash.md5(0, filesize) == "78241f0b39209750f35bd517490a54b93563d8f8ff7300bce19f2fd6b8b77c9f"
}
rule Sanesecurity_SpamImg_178
{
    condition:
        hash.md5(0, filesize) == "0c08b092325b007f74cf6f1b53c0e8b367391acb469fda534d6ac3f65d0eff34"
}
rule Sanesecurity_SpamImg_179
{
    condition:
        hash.md5(0, filesize) == "d0302aeb5216d634139f717a68e8c8f920290bd04f27cbd727cf6aeae1713fab"
}
rule Sanesecurity_SpamImg_180
{
    condition:
        hash.md5(0, filesize) == "67f9c85c6d09b2ae6b71044a8df69597a498fc985dcc94999c99b8ca4b657d25"
}
rule Sanesecurity_SpamImg_181
{
    condition:
        hash.md5(0, filesize) == "88814c66a05b6a570fce2128187c3ca3c1cd3ba3df7079edf81308d881636591"
}
rule Sanesecurity_SpamImg_182
{
    condition:
        hash.md5(0, filesize) == "28ded3ace0a78085d2ce0169889c5d2e9df6945825dfb68d5215f9d83804b013"
}
rule Sanesecurity_SpamImg_183
{
    condition:
        hash.md5(0, filesize) == "421ecd214f4443bb86bf20d8c45c34500a9dadb6278096fc23198f5a02307746"
}
rule Sanesecurity_SpamImg_184
{
    condition:
        hash.md5(0, filesize) == "bc8f9f10c9ada8613c5c0925ad6f028e4ab9748b87f56ef4445efbaced69a0b3"
}
rule Sanesecurity_SpamImg_185
{
    condition:
        hash.md5(0, filesize) == "9f96ff31b79104adff088ec9bba7a943a5673c495afee3846c38d07821eeb2b2"
}
rule Sanesecurity_SpamImg_186
{
    condition:
        hash.md5(0, filesize) == "750c4a6c6036903268b7d0b1dc7486f240581d7a2ef90240ad2bae0dfe98fe90"
}
rule Sanesecurity_SpamImg_187
{
    condition:
        hash.md5(0, filesize) == "db329fc92a7e7931819ed1dc7f7cf67b920076ed55e329d769f62af5a6b449a0"
}
rule Sanesecurity_SpamImg_188
{
    condition:
        hash.md5(0, filesize) == "1ca36fee595513a45ff10ae64230adb46811e5dd6aeb9240bde97a3158469e91"
}
rule Sanesecurity_SpamImg_189
{
    condition:
        hash.md5(0, filesize) == "aefeeb4482f7fd891f32fd55cbe7db5bc0c7781a7cb475b4b5bc19e6472e864a"
}
rule Sanesecurity_SpamImg_190
{
    condition:
        hash.md5(0, filesize) == "95985f3608075e0f49cd3d15c43cad156b58617890cb7fc5f3e314d6ae94b809"
}
rule Sanesecurity_SpamImg_191
{
    condition:
        hash.md5(0, filesize) == "6820401722498a3f175415957c43e2dd7c87d7fab76420658f377fac826feba6"
}
rule Sanesecurity_SpamImg_192
{
    condition:
        hash.md5(0, filesize) == "d813142c19671a0aa5e0b9e470a2e9e0074a9722ea9aee06384e48677004dd10"
}
rule Sanesecurity_SpamImg_193
{
    condition:
        hash.md5(0, filesize) == "982dabe805a88e12bee57fc20d861815eef2205bd534ba2bbd27b3cd7a004452"
}
rule Sanesecurity_SpamImg_194
{
    condition:
        hash.md5(0, filesize) == "800d21772051f428418f06304b5c209b35800e3e52ab8a47d1ab380306251b76"
}
rule Sanesecurity_SpamImg_195
{
    condition:
        hash.md5(0, filesize) == "8c982eb174d8b5303c4bdd2e767bbe29a33a3c458ccdc7ffd571ce171816c01b"
}
rule Sanesecurity_SpamImg_196
{
    condition:
        hash.md5(0, filesize) == "a09477942cfe12f687e05ca38a5e3101eaa44d325206ca2189ea2d2529f4b288"
}
rule Sanesecurity_SpamImg_197
{
    condition:
        hash.md5(0, filesize) == "eb805032ee4ab83b3a9fa45a29c53889f6ad5b401ac27e2a95e7e6d8cfe1787b"
}
rule Sanesecurity_SpamImg_198
{
    condition:
        hash.md5(0, filesize) == "981ca2102c4b286be6bc47b89137c1cd3aa4ab75c492792a1982038e0786bc6e"
}
rule Sanesecurity_SpamImg_199
{
    condition:
        hash.md5(0, filesize) == "9fc3caf88f6700f604ff8fc5093232685b9d592e73567c2b04a4d5603fdab508"
}
rule Sanesecurity_SpamImg_200
{
    condition:
        hash.md5(0, filesize) == "73b5474557affd655f20113ab6e295ef7535bb223468e5db7687301bb2e69563"
}
rule Sanesecurity_SpamImg_201
{
    condition:
        hash.md5(0, filesize) == "ddf45daee08fc2a0b36d8f4057bd8fd3d2775bc2e438f1c17b6a5cf241739c07"
}
rule Sanesecurity_SpamImg_202
{
    condition:
        hash.md5(0, filesize) == "52b02f27e4012b269707e299e80d1bbe4f81188cf134a3e21bb9f7c26f91358a"
}
rule Sanesecurity_SpamImg_203
{
    condition:
        hash.md5(0, filesize) == "d7515c2c7344f0944447ca88d77b260b0c0a411a20b6f2169df42108f76b3900"
}
rule Sanesecurity_SpamImg_204
{
    condition:
        hash.md5(0, filesize) == "7a163f7b8c56a72ada159c453888c8a0875c18022df32cb751741e071f485e07"
}
rule Sanesecurity_SpamImg_205
{
    condition:
        hash.md5(0, filesize) == "a1a7d40bef670b8884743a9fc7049fbfa05f7d8ed09b356a2867dfb61f85248c"
}
rule Sanesecurity_SpamImg_206
{
    condition:
        hash.md5(0, filesize) == "83a217a5a2f6fa6d20c443ce818057c656a0cb045d36222b84dccbf7197abc20"
}
rule Sanesecurity_SpamImg_207
{
    condition:
        hash.md5(0, filesize) == "25fa62871c90ed1a828eaeb0a272ce93de6739e3128d6c46f06bba0c28451667"
}
rule Sanesecurity_SpamImg_208
{
    condition:
        hash.md5(0, filesize) == "b99f9f3ea7e31ab1f1161214902acaaca2958df9bd7214364810bf90338e06b6"
}
rule Sanesecurity_SpamImg_209
{
    condition:
        hash.md5(0, filesize) == "1ce30cbcad84100eb60dd3cdd13b7d6327a92c41ebf24bba5ab9ef3322cb559c"
}
rule Sanesecurity_SpamImg_210
{
    condition:
        hash.md5(0, filesize) == "28d777c53485fc0d4ca742857f4f44fafde1c88754585ebefbb19d1e857d6ad7"
}
rule Sanesecurity_SpamImg_211
{
    condition:
        hash.md5(0, filesize) == "33c5268658561936d4083bcb8fee63cbf282128b91f63fc2febc2f67eb2076c8"
}
rule Sanesecurity_SpamImg_212
{
    condition:
        hash.md5(0, filesize) == "28018babe2fe2fa111c27c956ca43e69f01aa5faa091c94d30a9dccff49c092d"
}
rule Sanesecurity_SpamImg_213
{
    condition:
        hash.md5(0, filesize) == "513f014a6d14b20ed7d47869470b35eae761583d46ecdc5cf8d2151bc1f99de7"
}
rule Sanesecurity_SpamImg_214
{
    condition:
        hash.md5(0, filesize) == "67761585a591082a4689ec2533ba577c2be990694438cd7ead97dce48e624fec"
}
rule Sanesecurity_SpamImg_215
{
    condition:
        hash.md5(0, filesize) == "875f8aee606bd490551ca3edf592add261c855d428038fefc9afe2219a82af78"
}
rule Sanesecurity_SpamImg_216
{
    condition:
        hash.md5(0, filesize) == "947aea1e22d83af33c6b854b0330b7b47d9c67cf50136b1fa60a56de308e3823"
}
rule Sanesecurity_SpamImg_217
{
    condition:
        hash.md5(0, filesize) == "5dbff58c38561602550307d2d1e3ea1283bc62eefea108c5360cc5b2154df744"
}