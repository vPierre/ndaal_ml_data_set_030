import "dotnet"

rule DotnetStartupHook { 
  meta:
    description = "might be a .NET startup hook module"
    author = "William Ballenthin <william.ballenthin@mandiant.com>"
  strings:
     $a1 = "StartupHook"
     $a2 = "Initialize"
  condition: 
    uint16(0) == 0x5A4D 
    and dotnet.is_dotnet
    and all of them
}