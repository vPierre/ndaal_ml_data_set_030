/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./ProIntegritate/Archive/Emotet_2019-12.yara"
include "./ProIntegritate/Archive/Emotet_2020-01.yara"
include "./ProIntegritate/Archive/Emotet_2020-02.yara"
include "./ProIntegritate/Archive/Sodinikibi_2020.yara"
include "./ProIntegritate/Archive/Trickbot_2019-12.yara"
include "./ProIntegritate/Archive/Trickbot_2020-01.yara"
include "./ProIntegritate/Archive/Trickbot_2020-02.yara"
include "./ProIntegritate/Archive/Trickbot_2020-03.yara"
include "./ProIntegritate/Behaviour.yara"
include "./ProIntegritate/FileID.yara"
include "./ProIntegritate/Webshell.yara"
