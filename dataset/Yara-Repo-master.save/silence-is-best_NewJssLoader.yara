rule Jss_loader_bin
{
    meta:
        description = "NewJssLoader"
        author = "James_inthe_box"
        reference = "28f2fa4f9ac95c3fc906e201b758d56c6a888b657dcf57c351a4f34ffb3e0fe2"
        date = "2021/06"
        maltype = "Loader"

    strings:
        $string1 = "JSSLoader" nocase
        $string2 = "/?id=" ascii
        $string3 = "pc_model" ascii

    condition:
        all of ($string*) and filesize > 100KB
}

rule Jss_loader_mem
{
    meta:
        description = "NewJssLoader"
        author = "James_inthe_box"
        reference = "28f2fa4f9ac95c3fc906e201b758d56c6a888b657dcf57c351a4f34ffb3e0fe2"
        date = "2021/06"
        maltype = "Loader"
        
    strings:
        $string1 = "JSSLoader" nocase
        $string2 = "/?id=" ascii
        $string3 = "pc_model" ascii
        
    condition:
        all of ($string*) and filesize > 100KB
}
