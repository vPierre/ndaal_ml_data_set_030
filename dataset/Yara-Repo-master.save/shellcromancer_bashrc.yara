#!/bin/bash

matched_yara_fn() {
    match_w_rz_cmds=$(yara -s "$1" "$2" | awk -F  ':' '/1/ {printf "%s,aac; afi. @ $(?P %s)\n", $2, $1}');
        
    IFS=$'\n'; while IFS= read -r line; do
        oIFS=$IFS

        IFS=',' read -r match rzcmd <<< "${line}"
        echo "$(rizin -qc "$rzcmd" "$2") contains match for $match";
        
        IFS="$oIFS"
    done <<< "$match_w_rz_cmds"
}