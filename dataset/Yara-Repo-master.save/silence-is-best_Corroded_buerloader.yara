rule Rustyloader_mem_loose
{
    meta:
        description = "Corroded buerloader"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/83064edd-c7eb-4558-85e8-621db72b2a24"
        date = "2021/06"
        maltype = "Loader"

    strings:
        $rsstring1 = "defence.rs"
        $rsstring2 = "c2.rs"
        $rsstring3 = "identity.rs"
        $rsstring4 = "main.rs"
        $rsstring5 = "mod.rs"

    condition:
        2 of ($rsstring*) and filesize > 100KB
}