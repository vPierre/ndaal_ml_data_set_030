rule Recordbreaker_bin
{
    meta:
        description = "Recorderbreaker stealer"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/631b83d3-0f5d-4766-9b84-c35919fc4db0"
        date = "2022/06"
        maltype = "Stealer"

    strings:
        $string1 = "Local State" wide
        $string2 = "Login Data" wide
        $string3 = "edinayarossiya"
        $string4 = "Web Data" wide
        $string5 = "ffcookies.txt" wide
        $string6 = "wallet.dat" wide

    condition:
        uint16(0) == 0x5A4D and all of ($string*) and filesize < 12MB
}

rule Recordbreaker_mem
{
    meta:
        description = "Recorderbreaker stealer"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/631b83d3-0f5d-4766-9b84-c35919fc4db0"
        date = "2022/06"
        maltype = "Stealer"

    strings:
        $string1 = "Local State" wide
        $string2 = "Login Data" wide
        $string3 = "edinayarossiya"
        $string4 = "Web Data" wide
        $string5 = "ffcookies.txt" wide
        $string6 = "wallet.dat" wide

    condition:
        all of ($string*) and filesize > 12MB
}