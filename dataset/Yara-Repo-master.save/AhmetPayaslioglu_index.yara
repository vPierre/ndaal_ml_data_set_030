/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./AhmetPayaslioglu/Loader/DLL_SideLoading_For_Teams_And_OneDriveProcess.yara"
include "./AhmetPayaslioglu/MalwareFamily/CobaltStrike/CobaltStrikeDetection.yara"
include "./AhmetPayaslioglu/Persistence/Schedule_Runner.yara"
include "./AhmetPayaslioglu/Stealer/Chrome-Loader.yara"
include "./AhmetPayaslioglu/Stealer/Stealer_Mal.yara"
