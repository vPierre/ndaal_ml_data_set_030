#!/usr/bin/env bash
# https://github.com/Fadavvi/Yara-Repo

# Exit on error. Append "|| true" if you expect an error.
# set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

readonly USERSCRIPT="cloud"
echo "${USERSCRIPT}"

DESTINATION="/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master/"
echo "${DESTINATION}"

EXECUTIONPATH="${DESTINATION}"
echo "${EXECUTIONPATH}"

mkdir "${DESTINATION}"  || true

cd "${DESTINATION}" || exit

Function_Git_Pull_Repos () {
for n in "${array[@]}"
do
    (
    echo "${n}"
	 cd "${DESTINATION}${n}" || exit

    
	 git config pull.rebase false
    git pull --verbose
    
    )
done
}

# declare an array called array
declare -a array
array=(
"SigmaHQ"
"5l1v3r1"
"Adv-threat-research"
"BAMF_Detect"
"CiscoCXSecurity"
"CyberDefenses"
"DailyIOC"
"DanielRuf"
"FerdiGul"
"FirehaK"
"Hestat"
"InQuest"
"MainYaraRules"
"NVISOsecurity"
"OpenSourceYara"
"Penetrum-Security"
"PhishingKit-Yara-Rules"
"Phoul"
"SEKOIA-IO"
"SadFud"
"Sec-Soup"
"Signatures"
"SupportIntelligence"
"Te-k"
"Xumeiquer"
"Yara"
"ZephrFish"
"ail-project"
"apihash_to_yara"
"bartblaze"
"binaryalert-rules"
"citizenlab"
"codewatchorg"
"custom-yara-rules-master"
"data"
"deadbits"
"ditekshen"
"droberson"
"eset"
"f0wl-rules"
"fboldewin"
"fideliscyber"
"fpt-eagleeye"
"fr0gger"
"fsf-server"
"godaddy"
"h3x2b"
"hpthreatresearch"
"innominds_deadbits"
"innominds_lprat"
"innominds_stratosphereips"
"intezer"
"jeFF0Falltrades"
"jtrombley90"
"kevthehermit"
"malpedia"
"malware_analysis"
"mandiant"
"milad_kahsarialhadi"
"mokuso"
"nao-sec"
"naveenselvan"
"prolsen"
"red_team_tool_countermeasures"
"reversinglabs"
"rules"
"securitymagic"
"seyyid-bh"
"sophos"
"stairwell-inc"
"stvemillertime"
"t4d"
"telekom-security"
"tenable"
"thewhiteninja"
"tillmannw"
"tjnel"
"vendor"
"x64dbg_yarasigs"
"yara"
"yararules"
"ahhh"
"3vangel1st"
"timb-machine"
"JPCERTCC"
"naxonez"
"StrangerealIntel"
"sebdraven"
"instacyber"
"BushidoUK"
"imp0rtp3"
"pmelson"
"oldmonk007"
"avast"
"nembo81"
"S3cur3Th1sSh1t"
"1aN0rmus"
"0pc0deFR"
"malware-kitten"
"abhinavbom"
"jipegit"
"ProIntegritate"
"umair9747"
"CofenseLabs"
"VectraThreatLab"
"LD-Skystars"
"401trg"
"simonk9"
"executemalware"
)

RED='\e[31m'
NC='\033[0m'
clear

printf ${RED}' =============================\n'
printf '|      Yara repository        |\n'
printf '|-=-=-=-=-=-=-=-=-=-=-=-=-=-=-|\n'
printf '|       Version  0.0.2        |\n'
printf '|       Milad  Fadavvi        |\n'
printf '|     Git 2.28 or Higher      |\n'
printf '|   * Run script as Root *    |\n'
printf ' =============================\n\n'${NC}
sleep 10

Function_Clean_Up_YARA_Rules () {
for i in {1..10}
do
  echo "rm -r yara.${i}"
  rm -r *.yara.${i} || true
  echo "rm -r YARA.${i}"
  rm -r *.YARA.${i} || true
  echo "rm -r YAR.${i}"
  rm -r *.YAR.${i} || true
  echo "rm -r yar.${i}"
  rm -r *.yar.${i} || true
  echo "rm -r yar${i}"
  rm -r *.yar${i} || true
  echo "rm -r Rule.${i}"
  rm -r *.Rule.${i} || true
  echo "rm -r rule.${i}"
  rm -r *.rule.${i} || true
done
}

Function_Clean_Up_YARA_Rules

#cd ""/Users/${USERSCRIPT}/Yara-Repo-master/" || exit

rm -rf .git && git init && git remote add -f AlienVaultLabs https://github.com/AlienVault-Labs/AlienVaultLabs  > /dev/null && echo 'malware_analysis' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull AlienVaultLabs master > /dev/null
wget -q https://gist.githubusercontent.com/pedramamini/c586a151a978f971b70412ca4485c491/raw/68ba7792699177c033c673c7ffccfa7a0ed5ce47/XProtect.yara 
rm -rf .git && git init && git remote add -f bamfdetect https://github.com/bwall/bamfdetect  > /dev/null && echo 'BAMF_Detect/modules/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull bamfdetect master > /dev/null
git clone -q https://github.com/bartblaze/Yara-rules bartblaze
rm -rf .git && git init && git remote add -f binaryalert https://github.com/airbnb/binaryalert  > /dev/null && echo 'rules/public' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull binaryalert master > /dev/null
rm -rf .git && git init && git remote add -f CAPE  https://github.com/ctxis/CAPE  > /dev/null && echo 'data/yara/CAPE' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull CAPE master > /dev/null
git clone -q https://github.com/CyberDefenses/CDI_yara CyberDefenses
git clone -q https://github.com/citizenlab/malware-signatures citizenlab
git clone -q https://github.com/stvemillertime/ConventionEngine stvemillertime
git clone -q https://github.com/deadbits/yara-rules deadbits
git clone -q https://github.com/eset/malware-ioc/ eset
rm -rf .git && git init && git remote add -f indicators https://github.com/fideliscyber/indicators  > /dev/null && echo 'yararules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull indicators master > /dev/null
rm -rf .git && git init && git remote add -f signature-base https://github.com/Neo23x0/signature-base  > /dev/null && echo 'yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull signature-base master > /dev/null
wget -q https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44 && mv f1bb645a4f715cb499150c5a14d82b44 Neo23x0.yara
git clone -q https://github.com/fboldewin/YARA-rules fboldewin
git clone -q https://github.com/godaddy/yara-rules godaddy
git clone -q https://github.com/h3x2b/yara-rules h3x2b 
git clone -q https://github.com/SupportIntelligence/Icewater SupportIntelligence
git clone -q https://github.com/intezer/yara-rules intezer
git clone -q https://github.com/InQuest/yara-rules InQuest
git clone -q https://github.com/jeFF0Falltrades/YARA-Signatures jeFF0Falltrades
git clone -q https://github.com/kevthehermit/YaraRules kevthehermit
git clone -q https://github.com/Hestat/lw-yara Hestat
git clone -q https://github.com/tenable/yara-rules tenable
git clone -q https://github.com/tjnel/yara_repo tjnel
wget -q https://malpedia.caad.fkie.fraunhofer.de/api/get/yara/auto/zip && mv zip malpedia_auto_yar.zip && unzip malpedia_auto_yar.zip -d malpedia/  > /dev/null && rm -f malpedia_auto_yar.zip
git clone -q https://github.com/mikesxrs/Open-Source-YARA-rules OpenSourceYara
git clone -q https://github.com/prolsen/yara-rules prolsen
git clone -q https://github.com/advanced-threat-research/IOCs Adv-threat-research
git clone -q https://github.com/sophos-ai/yaraml_rules sophos
git clone -q https://github.com/reversinglabs/reversinglabs-yara-rules reversinglabs
git clone -q https://github.com/Yara-Rules/rules MainYaraRules
git clone -q https://github.com/fr0gger/Yara-Unprotect fr0gger
git clone -q https://github.com/Xumeiquer/yara-forensics Xumeiquer
fit clone -q https://github.com/malice-plugins/yara malice
wget -q --output-document VectraThreatLab_re.yar https://raw.githubusercontent.com/VectraThreatLab/reyara/master/re.yar
rm -rf .git && git init && git remote add -f fsf https://github.com/EmersonElectricCo/fsf  > /dev/null && echo 'fsf-server/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull fsf master > /dev/null
rm -rf .git && git init && git remote add -f Cyber-Defence https://github.com/nccgroup/Cyber-Defence/  > /dev/null && echo 'Signatures/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull Cyber-Defence master > /dev/null
rm -rf .git && git init && git remote add -f malware-analysis https://github.com/SpiderLabs/malware-analysis  > /dev/null && echo 'Yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull malware-analysis master > /dev/null
###
rm -rf .git && git init && git remote add -f ThreatHunting https://github.com/GossiTheDog/ThreatHunting > /dev/null && echo 'ThreatHunting' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull ThreatHunting master > /dev/null
### From Awesome Community  
rm -rf .git && git init && git remote add -f TheSweeper-Rules https://github.com/Jistrokz/TheSweeper-Rules > /dev/null && echo 'yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull TheSweeper-Rules main > /dev/null
git clone -q https://github.com/miladkahsarialhadi/RulesSet milad_kahsarialhadi
wget -q --output-document obfuscatedPHP_obfuscatdPHP.yar https://raw.githubusercontent.com/gil121983/obfuscatedPHP/master/obfuscatdPHP.yar
rm -rf .git && git init && git remote add -f binaryalert https://github.com/airbnb/binaryalert > /dev/null && echo 'rules/public' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull binaryalert master > /dev/null && mkdir binaryalert-rules && mv rules/public/* binaryalert-rules && rm -rf rules/public/
rm -rf .git && git init && git remote add -f indicators https://github.com/fideliscyber/indicators> /dev/null && echo 'yararules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull indicators master > /dev/null && mv yararules fideliscyber
rm -rf .git && git init && git remote add -f red_team_tool_countermeasures https://github.com/fireeye/red_team_tool_countermeasures > /dev/null && echo 'rules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull red_team_tool_countermeasures master > /dev/null && mv rules red_team_tool_countermeasures 
git clone -q https://github.com/f0wl/yara_rules f0wl-rules
rm -rf .git && git init && git remote add -f fsf https://github.com/EmersonElectricCo/fsf > /dev/null && echo 'fsf-server/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull fsf master > /dev/null
git clone -q https://github.com/jeFF0Falltrades/YARA-Signatures jeFF0Falltrades
wget -q --output-document tylabs_quicksand_exe.yara https://raw.githubusercontent.com/tylabs/qs_old/master/quicksand_exe.yara
wget -q --output-document tylabs_quicksand_exploits.yara https://raw.githubusercontent.com/tylabs/qs_old/master/quicksand_exploits.yara
wget -q --output-document tylabs_quicksand_general.yara https://raw.githubusercontent.com/tylabs/qs_old/master/quicksand_general.yara
git clone -q https://github.com/securitymagic/yara securitymagic
git clone -q https://github.com/StrangerealIntel/DailyIOC
git clone -q https://github.com/t4d/PhishingKit-Yara-Rules t4d
git clone -q https://github.com/x64dbg/yarasigs x64dbg_yarasigs
git clone -q https://github.com/thewhiteninja/yarasploit thewhiteninja
git clone -q https://github.com/mokuso/yara-rules mokuso
rm -rf .git && git init && git remote add -f YaraRules https://github.com/5l1v3r1/yaraRules > /dev/null && echo 'YaraRules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull yaraRules master > /dev/null
git clone -q https://github.com/telekom-security/malware_analysis telekom-security
git clone -q https://github.com/tbarabosch/apihash_to_yara apihash_to_yara
git clone -q https://github.com/tillmannw/yara-rules tillmannw

git clone -q https://github.com/jtrombley90/Yara_Rules jtrombley90
git clone -q https://github.com/ail-project/ail-yara-rules ail-project
git clone -q https://github.com/Yara-Rules/rules/blob/master/malware/DarkComet.yar
git clone -q https://github.com/mandiant/sunburst_countermeasures mandiant
git clone -q https://github.com/Te-k/cobaltstrike Te-k
git clone -q https://github.com/SEKOIA-IO/Community SEKOIA-IO
git clone -q https://github.com/NVISOsecurity/YARA NVISOsecurity
git clone -q https://github.com/t4d/PhishingKit-Yara-Search t4d

wget -q --output-document tlansec_pe_check.yar https://gist.githubusercontent.com/tlansec/4be4e92cbbd3354cf53386ef6edf0676/raw/f6cef23a2d3e6de6ead5b83c53801dbe1b653bf6/pe_check.yar
wget -q --output-document shellcromancer_mal_sysjoker_macOS.yara https://gist.githubusercontent.com/shellcromancer/e9e8c8ca95e0f31fc8b92ebc82b59303/raw/f706b420f6370d034781f605e55879e7d3322c1e/mal_sysjoker_macOS.yara
wget -q --output-document silascutler_WhisperGate.yar https://gist.githubusercontent.com/silascutler/f8e518564a8a1410ba58f0ab5ed493f6/raw/b465af32c4b546fb4ab3604fbe3c5d363aca7f2c/%2523WhisperGate%2520Yara%2520Rule
wget -q --output-document captainGeech42_scriptobf_replaceempty.yara https://gist.githubusercontent.com/captainGeech42/3e60e639ea62dd6e907e3e1e7cbac0fc/raw/43b3ef99249eb9b47c7062a98a3bdadad7863d65/scriptobf_replaceempty.yara
wget -q --output-document schrodyn_windows_drivers.yara https://gist.githubusercontent.com/schrodyn/30ca12d15e0e069224204adca41d5256/raw/7ff09541c30977173fb5dc192d5820a13f31a89d/windows_drivers.yara
wget -q --output-document Neo23x0_iddqd.yar https://gist.githubusercontent.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/raw/d621fcfd496d03dca78f9ff390cad88684139d64/iddqd.yar
wget -q --output-document kmdnet_EICAR_hash_rule.yar https://gist.githubusercontent.com/kmdnet/28f1dde6ab93d866a87357c1c25632a7/raw/c52c3d9c267d6ca8f9dc40043ba888d977203c54/EICAR_hash_rule.yar
wget -q --output-document MrThreat_findbadlinkers.yar https://gist.githubusercontent.com/MrThreat/d468caf9dc392431099f9b1958cf188f/raw/49aced5f32bebc20044ee75bcc9de20abeddd42b/findbadlinkers.yar
wget -q --output-document tombonner_pe_template.yara https://gist.githubusercontent.com/tombonner/70f9df512b649533c40cccc6f1a08c30/raw/34dd02a07827ca5ba3d22f47a3878ce4ac3de49e/pe_template.yara
wget -q --output-document tombonner_pdb_template.yara https://gist.githubusercontent.com/tombonner/0b0aea4722a1ea3e6e475568ab2f912d/raw/b3ef8ca4a475a0f711f3c887aba5cb48737694f9/pdb_template.yara
wget -q --output-document stvemillertime_CreatePEPolyObject.yar https://gist.githubusercontent.com/stvemillertime/b87dd7bf979dda6f1b6b5d70bf6620b8/raw/3fbaecc5d7bd1004e9d38bb4f6ac6c1a73710e5c/CreatePEPolyObject.yar
wget -q --output-document BoazDolev_ComLook.Yarhttps://gist.githubusercontent.com/BoazDolev/43b141957b94163fc1f2a0dca83c53b6/raw/3daf98403ff0142c5453bf4c2c13389125890847/ComLook.Yar
wget -q --output-document shellcromancer_tool_gscript.yara https://gist.githubusercontent.com/shellcromancer/b4fcdb7c118ef1aa0006ec5653c92c78/raw/c8bedbcbefa38e362be354badb3e23d6145564ee/tool_gscript.yara
wget -q --output-document shellcromancer_alg_crypto_des.yara https://gist.githubusercontent.com/shellcromancer/aefe541468bb9725dbc7d227faa13a08/raw/4839cc79e5470ea8ef7b05facb0f5f56c1f9e56a/alg_crypto_des.yara
wget -q --output-document shellcromancer_alg_crypto_sha1.yara https://gist.githubusercontent.com/shellcromancer/da08abd6e254c83c12dd9f69512ea872/raw/516ce63e0265b474ee5f7631e8802674d01e9a19/alg_crypto_sha1.yara
wget -q --output-document shellcromancer_mal_sysjoker_macOS.yara https://gist.github.com/shellcromancer/e9e8c8ca95e0f31fc8b92ebc82b59303/raw/f706b420f6370d034781f605e55879e7d3322c1e/mal_sysjoker_macOS.yara
wget -q --output-document shellcromancer_args_socket_tcpip.yara https://gist.github.com/shellcromancer/6baf9d799ff4291d55949ae9d796ea69/raw/171f56160b0a4b71f432c1746dd72f6c64c24959/args_socket_tcpip.yara
wget -q --output-document shellcromancer_alg_crypto_blake.yara https://gist.github.com/shellcromancer/f410a7cc75f6c069f8c5285990370002/raw/8cb46a37720c49167f9e7cdcbc6a7a4513a1c528/alg_crypto_blake.yara
wget -q --output-document shellcromancer_alg_crypto_md5.yara https://gist.github.com/shellcromancer/f7260f3b05787e37f811baa9c37e9c65/raw/65d2093260af2381deebe6e5128e2297afc59321/alg_crypto_md5.yara
wget -q --output-document shellcromancer_shellcode_metasploit_x86.yara https://gist.github.com/shellcromancer/9d683b37f7caac9c2b58999641ce140b/raw/6a01ff0abb69543774d4df1ecdebbf46d2b15c00/shellcode_metasploit_x86.yara
wget -q --output-document shellcromancer_lang_rust.yara https://gist.github.com/shellcromancer/3ebbb4b82b9c86038dbb829ab5f3abb1/raw/2cf5174cec1477a86d43180fc494e656e484215a/lang_rust.yara
wget -q --output-document shellcromancer_macho_space_in_seg_or_sect.yara https://gist.github.com/shellcromancer/59b0a9a044f7413b009962f64663f819/raw/9131ff0e7d9e82350724fb77aa89eedfd4b133c7/macho_space_in_seg_or_sect.yara
wget -q --output-document shellcromancer_imov_stackstrings.yara https://gist.github.com/shellcromancer/6eb7bd5849aff1d657ef3fc5bd31e71d/raw/4225a033780b20532c36c2a8388ecc3654339fe6/imov_stackstrings.yara
wget -q --output-document shellcromancer_alg_crypto_crc32.yara https://gist.github.com/shellcromancer/bbb926841ab94056379097d4d5c2219c/raw/a2973ce124db72eb8244ffef504aa7b25bea08ff/alg_crypto_crc32.yara
wget -q --output-document shellcromancer_alg_crypto_aes.yara https://gist.github.com/shellcromancer/570d3416dd4175752f883d173f10ac3d/raw/bc353cdf9922fe7c66d2edfe73a1f7d1baa85852/alg_crypto_aes.yara
wget -q --output-document shellcromancer_bashrc https://gist.github.com/shellcromancer/b6039d1c6da6328db699eedeae92e59f/raw/01077cb345799556af7a556f56bb410a3a87f06d/.bashrc
wget -q --output-document shellcromancer_alg_crypto_rc4.yara https://gist.github.com/shellcromancer/d1b0bd1832a1a3a3c3dfd5cc75dfb21e/raw/ba4c66fe0ff6f9e21631baac7eb98fc8ce729551/alg_crypto_rc4.yara
wget -q --output-document shellcromancer_alg_salsa20.yara https://gist.github.com/shellcromancer/c5dd110241040b12b90b021daf89a36c/raw/e2799df8c66fcb02899eb41a473701d1567a041a/alg_salsa20.yara


git clone -q https://github.com/FirehaK/YARA FirehaK
git clone -q https://github.com/stairwell-inc/threat-research stairwell-inc
git clone -q https://github.com/codewatchorg/Burp-Yara-Rules codewatchorg
git clone -q https://github.com/nao-sec/yara_rules nao-sec
git clone -q https://github.com/Penetrum-Security/Malware-Yara-Rules Penetrum-Security
git clone -q https://github.com/naveenselvan/malware naveenselvan
git clone -q https://github.com/droberson/yararules droberson
git clone -q https://github.innominds.com/lprat/static_file_analysis innominds_lprat
git clone -q https://github.innominds.com/deadbits/yara-rules innominds_deadbits
git clone -q https://github.innominds.com/stratosphereips/yara-rules innominds_stratosphereips
git clone -q https://github.com/SadFud/YARA.Rules SadFud
git clone -q https://github.com/FerdiGul/Yara-Rules FerdiGul
git clone -q https://github.com/Sec-Soup/Yara-Rules Sec-Soup
git clone -q https://github.com/fpt-eagleeye/rules fpt-eagleeye
git clone -q https://github.com/ditekshen/detection ditekshen
git clone -q https://github.com/hpthreatresearch/tools hpthreatresearch
git clone -q https://github.com/ZephrFish/Random-Yara-Rules ZephrFish
git clone -q https://github.com/DanielRuf/yara-rules-free DanielRuf
git clone -q https://github.com/CiscoCXSecurity/log4j CiscoCXSecurity
git clone -q https://github.com/Phoul/yara_rules Phoul
git clone -q https://github.com/seyyid-bh/FireEyeHackDetection seyyid-bh
git clone -q https://github.com/ahhh/YARA ahhh
git clone -q https://github.com/3vangel1st/Yara 3vangel1st
git clone -q https://github.com/timb-machine/linux-malware timb-machine
git clone -q https://github.com/JPCERTCC/jpcert-yara JPCERTCC
git clone -q https://github.com/naxonez/YaraRules naxonez
git clone -q https://github.com/StrangerealIntel/Orion StrangerealIntel
git clone -q https://github.com/cocaman/yararules cocaman
git clone -q https://github.com/sebdraven/yara-rules sebdraven
git clone -q https://github.com/instacyber/yara instacyber
git clone -q https://github.com/BushidoUK/IOCs-YARAs BushidoUK
git clone -q https://github.com/imp0rtp3/yara-rules imp0rtp3
git clone -q https://github.com/imp0rtp3/js-yara-rules imp0rtp3
git clone -q https://github.com/pmelson/yara_rules pmelson
git clone -q https://github.com/oldmonk007/myrep1 oldmonk007
git clone -q https://github.com/avast/ioc avast
git clone -q https://github.com/nembo81/YaraRules nembo81
git clone -q https://github.com/S3cur3Th1sSh1t/NimGetSyscallStub
git clone -q https://github.com/1aN0rmus/Yara 1aN0rmus
git clone -q https://github.com/0pc0deFR/YaraRules 0pc0deFR
git clone -q https://github.com/malware-kitten/public_yara_rules malware-kitten
git clone -q https://github.com/abhinavbom/Yara-Rules abhinavbom
git clone -q https://github.com/jipegit/yara-rules-public jipegit
git clone -q https://github.com/ProIntegritate/Yara-rules ProIntegritate
git clone -q https://github.com/umair9747/yara-rules
git clone -q https://github.com/CofenseLabs/Coronavirus-Phishing-Yara-Rules
git clone -q https://github.com/VectraThreatLab/reyara VectraThreatLab
git clone -q https://github.com/LD-Skystars/LightDefender-yara-rules LD-Skystars
git clone -q https://github.com/401trg/detections 401trg
git clone -q https://github.com/simonk9/yara_habibi simonk9
git clone -q https://github.com/executemalware/Malware-IOCs executemalware

###

Function_Git_Pull_Repos

###

Function_Clean_Up_YARA_Rules

raw_list_urls="https://raw.githubusercontent.com/Yara-Rules/rules/master/index.yar https://raw.githubusercontent.com/Yara-Rules/rules/master/index.yar https://raw.githubusercontent.com/Yara-Rules/rules/master/index.yar"
IFS=' ' read -r -a urls <<< "${raw_list_urls}"

for url in "${urls[@]}" ; do
        echo "Getting ${url}" ; sleep 2
        curl "${url}" | grep include | cut -d'"' -f2
done

for f in $(find . -mindepth 1 ! -path "./$(basename $PWD)_all.yar" -type f -name "*.yar*"); do
  if [[ "$f" == *"Mobile_Malware"* || "$f" == *"mobile"* ]]; then  # exclude Android rules
    continue
  fi
  echo "include \"$f\"" >> $(basename $PWD)_index.yar;
done;

clear
rm -rf .git

find . -name '*.yr' -exec sh -c 'mv "$0" "${0%.yr}.yara"' {} \;
find . -name '*.rules' -exec sh -c 'mv "$0" "${0%.rules}.yara"' {} \;
find . -name '*.Rules' -exec sh -c 'mv "$0" "${0%.Rules}.yara"' {} \;

cp -v -n -r ${DESTINATION}*.yar* /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/

echo -e "Rules path: /opt/yara-repo\n"
echo "Number of YARA Rules with extension .yar*  : " && find . -name "*.yar*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo "Number of YARA Rules with extension .yr*   : " && find . -name "*.yr*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo "Number of YARA Rules with extension .rules : " && find . -name "*.rules" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"

echo "cp -v /Users/${USERSCRIPT}/Yara-Repo-master/Yara-Repo.sh ${DESTINATION}"
cp -v /Users/${USERSCRIPT}/Yara-Repo-master/Yara-Repo.sh "${DESTINATION}" 

script_name1="basename ${0}"
script_path1="$(dirname "$(readlink -f "${0}")")"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
