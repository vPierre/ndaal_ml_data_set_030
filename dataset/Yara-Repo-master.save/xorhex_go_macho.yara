import "macho"

rule is_go_macho {
  strings:
    $go = { ff 20 47 6f 20 62 75 69 6c 64 20 49 44 3a 20 22  } // \xff Go build ID: \"

  condition:
      macho.cputype == macho.CPU_TYPE_X86_64
    and
      for any s in macho.segments : (
          for any sec in s.sections : (
              sec.sectname == "__text"
            and
              $go in (sec.addr..sec.addr+sec.size)
          )
      )
}