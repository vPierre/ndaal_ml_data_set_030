/*
Generated by Yara-Rules
On 18-05-2023
*/
include "./V1D1AN/rules/suricata/3coresec.yara"
include "./V1D1AN/rules/suricata/BSD-License.yara"
include "./V1D1AN/rules/suricata/botcc.portgrouped.yara"
include "./V1D1AN/rules/suricata/botcc.yara"
include "./V1D1AN/rules/suricata/ciarmy.yara"
include "./V1D1AN/rules/suricata/compromised-ips.yara"
include "./V1D1AN/rules/suricata/compromised.yara"
include "./V1D1AN/rules/suricata/drop.yara"
include "./V1D1AN/rules/suricata/dshield.yara"
include "./V1D1AN/rules/suricata/emerging-activex.yara"
include "./V1D1AN/rules/suricata/emerging-adware_pup.yara"
include "./V1D1AN/rules/suricata/emerging-attack_response.yara"
include "./V1D1AN/rules/suricata/emerging-chat.yara"
include "./V1D1AN/rules/suricata/emerging-coinminer.yara"
include "./V1D1AN/rules/suricata/emerging-current_events.yara"
include "./V1D1AN/rules/suricata/emerging-deleted.yara"
include "./V1D1AN/rules/suricata/emerging-dns.yara"
include "./V1D1AN/rules/suricata/emerging-dos.yara"
include "./V1D1AN/rules/suricata/emerging-exploit.yara"
include "./V1D1AN/rules/suricata/emerging-exploit_kit.yara"
include "./V1D1AN/rules/suricata/emerging-ftp.yara"
include "./V1D1AN/rules/suricata/emerging-games.yara"
include "./V1D1AN/rules/suricata/emerging-hunting.yara"
include "./V1D1AN/rules/suricata/emerging-icmp.yara"
include "./V1D1AN/rules/suricata/emerging-icmp_info.yara"
include "./V1D1AN/rules/suricata/emerging-imap.yara"
include "./V1D1AN/rules/suricata/emerging-inappropriate.yara"
include "./V1D1AN/rules/suricata/emerging-info.yara"
include "./V1D1AN/rules/suricata/emerging-ja3.yara"
include "./V1D1AN/rules/suricata/emerging-malware.yara"
include "./V1D1AN/rules/suricata/emerging-misc.yara"
include "./V1D1AN/rules/suricata/emerging-mobile_malware.yara"
include "./V1D1AN/rules/suricata/emerging-netbios.yara"
include "./V1D1AN/rules/suricata/emerging-p2p.yara"
include "./V1D1AN/rules/suricata/emerging-phishing.yara"
include "./V1D1AN/rules/suricata/emerging-policy.yara"
include "./V1D1AN/rules/suricata/emerging-pop3.yara"
include "./V1D1AN/rules/suricata/emerging-rpc.yara"
include "./V1D1AN/rules/suricata/emerging-scada.yara"
include "./V1D1AN/rules/suricata/emerging-scan.yara"
include "./V1D1AN/rules/suricata/emerging-shellcode.yara"
include "./V1D1AN/rules/suricata/emerging-smtp.yara"
include "./V1D1AN/rules/suricata/emerging-snmp.yara"
include "./V1D1AN/rules/suricata/emerging-sql.yara"
include "./V1D1AN/rules/suricata/emerging-telnet.yara"
include "./V1D1AN/rules/suricata/emerging-tftp.yara"
include "./V1D1AN/rules/suricata/emerging-user_agents.yara"
include "./V1D1AN/rules/suricata/emerging-voip.yara"
include "./V1D1AN/rules/suricata/emerging-web_client.yara"
include "./V1D1AN/rules/suricata/emerging-web_server.yara"
include "./V1D1AN/rules/suricata/emerging-web_specific_apps.yara"
include "./V1D1AN/rules/suricata/emerging-worm.yara"
include "./V1D1AN/rules/suricata/gpl-2.0.yara"
include "./V1D1AN/rules/suricata/local.yara"
include "./V1D1AN/rules/suricata/suricata-5.0-enhanced-open.yara"
include "./V1D1AN/rules/suricata/threatview_CS_c2.yara"
include "./V1D1AN/rules/suricata/tor.yara"
