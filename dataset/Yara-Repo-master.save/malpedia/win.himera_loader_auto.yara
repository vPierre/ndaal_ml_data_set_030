rule win_himera_loader_auto {

    meta:
        author = "Felix Bilstein - yara-signator at cocacoding dot com"
        date = "2020-12-22"
        version = "1"
        description = "autogenerated rule brought to you by yara-signator"
        tool = "yara-signator v0.6.0"
        signator_config = "callsandjumps;datarefs;binvalue"
        malpedia_reference = "https://malpedia.caad.fkie.fraunhofer.de/details/win.himera_loader"
        malpedia_rule_date = "20201222"
        malpedia_hash = "30354d830a29f0fbd3714d93d94dea941d77a130"
        malpedia_version = "20201023"
        malpedia_license = "CC BY-SA 4.0"
        malpedia_sharing = "TLP:WHITE"

    /* DISCLAIMER
     * The strings used in this rule have been automatically selected from the
     * disassembly of memory dumps and unpacked files, using YARA-Signator.
     * The code and documentation is published here:
     * https://github.com/fxb-cocacoding/yara-signator
     * As Malpedia is used as data source, please note that for a given
     * number of families, only single samples are documented.
     * This likely impacts the degree of generalization these rules will offer.
     * Take the described generation method also into consideration when you
     * apply the rules in your use cases and assign them confidence levels.
     */


    strings:
        $sequence_0 = { c745dc0c000000 c645e45e c645e55d c645e65a c645e741 c645e85c c645e94b }
            // n = 7, score = 200
            //   c745dc0c000000       | mov                 dword ptr [ebp - 0x24], 0xc
            //   c645e45e             | mov                 byte ptr [ebp - 0x1c], 0x5e
            //   c645e55d             | mov                 byte ptr [ebp - 0x1b], 0x5d
            //   c645e65a             | mov                 byte ptr [ebp - 0x1a], 0x5a
            //   c645e741             | mov                 byte ptr [ebp - 0x19], 0x41
            //   c645e85c             | mov                 byte ptr [ebp - 0x18], 0x5c
            //   c645e94b             | mov                 byte ptr [ebp - 0x17], 0x4b

        $sequence_1 = { 66894146 6a24 ba02000000 6bc224 8b4d08 }
            // n = 5, score = 200
            //   66894146             | mov                 word ptr [ecx + 0x46], ax
            //   6a24                 | push                0x24
            //   ba02000000           | mov                 edx, 2
            //   6bc224               | imul                eax, edx, 0x24
            //   8b4d08               | mov                 ecx, dword ptr [ebp + 8]

        $sequence_2 = { 6a14 ba02000000 6bc214 8b4d08 }
            // n = 4, score = 200
            //   6a14                 | push                0x14
            //   ba02000000           | mov                 edx, 2
            //   6bc214               | imul                eax, edx, 0x14
            //   8b4d08               | mov                 ecx, dword ptr [ebp + 8]

        $sequence_3 = { c645d24b c645d34d c645d445 c645d541 }
            // n = 4, score = 200
            //   c645d24b             | mov                 byte ptr [ebp - 0x2e], 0x4b
            //   c645d34d             | mov                 byte ptr [ebp - 0x2d], 0x4d
            //   c645d445             | mov                 byte ptr [ebp - 0x2c], 0x45
            //   c645d541             | mov                 byte ptr [ebp - 0x2b], 0x41

        $sequence_4 = { 837dfc04 7316 8b55f8 0355fc 0fbe02 83f02e }
            // n = 6, score = 200
            //   837dfc04             | cmp                 dword ptr [ebp - 4], 4
            //   7316                 | jae                 0x18
            //   8b55f8               | mov                 edx, dword ptr [ebp - 8]
            //   0355fc               | add                 edx, dword ptr [ebp - 4]
            //   0fbe02               | movsx               eax, byte ptr [edx]
            //   83f02e               | xor                 eax, 0x2e

        $sequence_5 = { c645e25a c645e347 c645e441 c645e540 c645e67e c645e75c c645e841 }
            // n = 7, score = 200
            //   c645e25a             | mov                 byte ptr [ebp - 0x1e], 0x5a
            //   c645e347             | mov                 byte ptr [ebp - 0x1d], 0x47
            //   c645e441             | mov                 byte ptr [ebp - 0x1c], 0x41
            //   c645e540             | mov                 byte ptr [ebp - 0x1b], 0x40
            //   c645e67e             | mov                 byte ptr [ebp - 0x1a], 0x7e
            //   c645e75c             | mov                 byte ptr [ebp - 0x19], 0x5c
            //   c645e841             | mov                 byte ptr [ebp - 0x18], 0x41

        $sequence_6 = { 837dfc49 7318 8b4d08 e8???????? 8b4df8 034dfc 8b55fc }
            // n = 7, score = 200
            //   837dfc49             | cmp                 dword ptr [ebp - 4], 0x49
            //   7318                 | jae                 0x1a
            //   8b4d08               | mov                 ecx, dword ptr [ebp + 8]
            //   e8????????           |                     
            //   8b4df8               | mov                 ecx, dword ptr [ebp - 8]
            //   034dfc               | add                 ecx, dword ptr [ebp - 4]
            //   8b55fc               | mov                 edx, dword ptr [ebp - 4]

        $sequence_7 = { 51 e8???????? 83c408 8b55fc 66894210 33c0 8b4dfc }
            // n = 7, score = 200
            //   51                   | push                ecx
            //   e8????????           |                     
            //   83c408               | add                 esp, 8
            //   8b55fc               | mov                 edx, dword ptr [ebp - 4]
            //   66894210             | mov                 word ptr [edx + 0x10], ax
            //   33c0                 | xor                 eax, eax
            //   8b4dfc               | mov                 ecx, dword ptr [ebp - 4]

        $sequence_8 = { c645e541 c645e645 c645e74f c645e800 }
            // n = 4, score = 200
            //   c645e541             | mov                 byte ptr [ebp - 0x1b], 0x41
            //   c645e645             | mov                 byte ptr [ebp - 0x1a], 0x45
            //   c645e74f             | mov                 byte ptr [ebp - 0x19], 0x4f
            //   c645e800             | mov                 byte ptr [ebp - 0x18], 0

        $sequence_9 = { 83c001 8945fc 837dfc09 7318 8b4d08 e8???????? }
            // n = 6, score = 200
            //   83c001               | add                 eax, 1
            //   8945fc               | mov                 dword ptr [ebp - 4], eax
            //   837dfc09             | cmp                 dword ptr [ebp - 4], 9
            //   7318                 | jae                 0x1a
            //   8b4d08               | mov                 ecx, dword ptr [ebp + 8]
            //   e8????????           |                     

    condition:
        7 of them and filesize < 385024
}