rule win_mikoponi_auto {

    meta:
        author = "Felix Bilstein - yara-signator at cocacoding dot com"
        date = "2020-12-22"
        version = "1"
        description = "autogenerated rule brought to you by yara-signator"
        tool = "yara-signator v0.6.0"
        signator_config = "callsandjumps;datarefs;binvalue"
        malpedia_reference = "https://malpedia.caad.fkie.fraunhofer.de/details/win.mikoponi"
        malpedia_rule_date = "20201222"
        malpedia_hash = "30354d830a29f0fbd3714d93d94dea941d77a130"
        malpedia_version = "20201023"
        malpedia_license = "CC BY-SA 4.0"
        malpedia_sharing = "TLP:WHITE"

    /* DISCLAIMER
     * The strings used in this rule have been automatically selected from the
     * disassembly of memory dumps and unpacked files, using YARA-Signator.
     * The code and documentation is published here:
     * https://github.com/fxb-cocacoding/yara-signator
     * As Malpedia is used as data source, please note that for a given
     * number of families, only single samples are documented.
     * This likely impacts the degree of generalization these rules will offer.
     * Take the described generation method also into consideration when you
     * apply the rules in your use cases and assign them confidence levels.
     */


    strings:
        $sequence_0 = { 33db 39bc2430060000 895c2410 8984249c010000 898424a0010000 898424a4010000 898424a8010000 }
            // n = 7, score = 100
            //   33db                 | xor                 ebx, ebx
            //   39bc2430060000       | cmp                 dword ptr [esp + 0x630], edi
            //   895c2410             | mov                 dword ptr [esp + 0x10], ebx
            //   8984249c010000       | mov                 dword ptr [esp + 0x19c], eax
            //   898424a0010000       | mov                 dword ptr [esp + 0x1a0], eax
            //   898424a4010000       | mov                 dword ptr [esp + 0x1a4], eax
            //   898424a8010000       | mov                 dword ptr [esp + 0x1a8], eax

        $sequence_1 = { 803d????????01 56 7527 8b742408 56 ff15???????? }
            // n = 6, score = 100
            //   803d????????01       |                     
            //   56                   | push                esi
            //   7527                 | jne                 0x29
            //   8b742408             | mov                 esi, dword ptr [esp + 8]
            //   56                   | push                esi
            //   ff15????????         |                     

        $sequence_2 = { 85c0 743a b9???????? 8bc3 8d642400 }
            // n = 5, score = 100
            //   85c0                 | test                eax, eax
            //   743a                 | je                  0x3c
            //   b9????????           |                     
            //   8bc3                 | mov                 eax, ebx
            //   8d642400             | lea                 esp, [esp]

        $sequence_3 = { 5e 8b8c2428060000 5f 5d 5b 33cc e8???????? }
            // n = 7, score = 100
            //   5e                   | pop                 esi
            //   8b8c2428060000       | mov                 ecx, dword ptr [esp + 0x628]
            //   5f                   | pop                 edi
            //   5d                   | pop                 ebp
            //   5b                   | pop                 ebx
            //   33cc                 | xor                 ecx, esp
            //   e8????????           |                     

        $sequence_4 = { 50 e8???????? 83c40c f706004a0000 740d }
            // n = 5, score = 100
            //   50                   | push                eax
            //   e8????????           |                     
            //   83c40c               | add                 esp, 0xc
            //   f706004a0000         | test                dword ptr [esi], 0x4a00
            //   740d                 | je                  0xf

        $sequence_5 = { 66890f 03fa 57 b9???????? e8???????? 8d0447 33d2 }
            // n = 7, score = 100
            //   66890f               | mov                 word ptr [edi], cx
            //   03fa                 | add                 edi, edx
            //   57                   | push                edi
            //   b9????????           |                     
            //   e8????????           |                     
            //   8d0447               | lea                 eax, [edi + eax*2]
            //   33d2                 | xor                 edx, edx

        $sequence_6 = { 0f840f010000 803d????????00 0f8502010000 68???????? e8???????? }
            // n = 5, score = 100
            //   0f840f010000         | je                  0x115
            //   803d????????00       |                     
            //   0f8502010000         | jne                 0x108
            //   68????????           |                     
            //   e8????????           |                     

        $sequence_7 = { 6800000040 8d842484000000 50 ff15???????? 8be8 83fdff }
            // n = 6, score = 100
            //   6800000040           | push                0x40000000
            //   8d842484000000       | lea                 eax, [esp + 0x84]
            //   50                   | push                eax
            //   ff15????????         |                     
            //   8be8                 | mov                 ebp, eax
            //   83fdff               | cmp                 ebp, -1

    condition:
        7 of them and filesize < 330752
}