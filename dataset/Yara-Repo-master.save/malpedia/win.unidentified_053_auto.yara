rule win_unidentified_053_auto {

    meta:
        author = "Felix Bilstein - yara-signator at cocacoding dot com"
        date = "2021-10-07"
        version = "1"
        description = "Detects win.unidentified_053."
        info = "autogenerated rule brought to you by yara-signator"
        tool = "yara-signator v0.6.0"
        signator_config = "callsandjumps;datarefs;binvalue"
        malpedia_reference = "https://malpedia.caad.fkie.fraunhofer.de/details/win.unidentified_053"
        malpedia_rule_date = "20211007"
        malpedia_hash = "e5b790e0f888f252d49063a1251ca60ec2832535"
        malpedia_version = "20211008"
        malpedia_license = "CC BY-SA 4.0"
        malpedia_sharing = "TLP:WHITE"

    /* DISCLAIMER
     * The strings used in this rule have been automatically selected from the
     * disassembly of memory dumps and unpacked files, using YARA-Signator.
     * The code and documentation is published here:
     * https://github.com/fxb-cocacoding/yara-signator
     * As Malpedia is used as data source, please note that for a given
     * number of families, only single samples are documented.
     * This likely impacts the degree of generalization these rules will offer.
     * Take the described generation method also into consideration when you
     * apply the rules in your use cases and assign them confidence levels.
     */


    strings:
        $sequence_0 = { 753c ff75e4 68???????? e8???????? 85c0 59 }
            // n = 6, score = 100
            //   753c                 | jne                 0x3e
            //   ff75e4               | push                dword ptr [ebp - 0x1c]
            //   68????????           |                     
            //   e8????????           |                     
            //   85c0                 | test                eax, eax
            //   59                   | pop                 ecx

        $sequence_1 = { 8a6e1d 8a4e1c 8d44082e 8d4804 3bf9 8945fc }
            // n = 6, score = 100
            //   8a6e1d               | mov                 ch, byte ptr [esi + 0x1d]
            //   8a4e1c               | mov                 cl, byte ptr [esi + 0x1c]
            //   8d44082e             | lea                 eax, dword ptr [eax + ecx + 0x2e]
            //   8d4804               | lea                 ecx, dword ptr [eax + 4]
            //   3bf9                 | cmp                 edi, ecx
            //   8945fc               | mov                 dword ptr [ebp - 4], eax

        $sequence_2 = { e8???????? 2bd5 03d1 2bd1 03d5 }
            // n = 5, score = 100
            //   e8????????           |                     
            //   2bd5                 | sub                 edx, ebp
            //   03d1                 | add                 edx, ecx
            //   2bd1                 | sub                 edx, ecx
            //   03d5                 | add                 edx, ebp

        $sequence_3 = { 3b1d???????? 0f838d000000 8bc3 c1f805 8d3c8540964100 }
            // n = 5, score = 100
            //   3b1d????????         |                     
            //   0f838d000000         | jae                 0x93
            //   8bc3                 | mov                 eax, ebx
            //   c1f805               | sar                 eax, 5
            //   8d3c8540964100       | lea                 edi, dword ptr [eax*4 + 0x419640]

        $sequence_4 = { f3a5 ff24950c884000 8bc7 ba03000000 }
            // n = 4, score = 100
            //   f3a5                 | rep movsd           dword ptr es:[edi], dword ptr [esi]
            //   ff24950c884000       | jmp                 dword ptr [edx*4 + 0x40880c]
            //   8bc7                 | mov                 eax, edi
            //   ba03000000           | mov                 edx, 3

        $sequence_5 = { c74618543c4100 83600400 e9???????? 8b461c }
            // n = 4, score = 100
            //   c74618543c4100       | mov                 dword ptr [esi + 0x18], 0x413c54
            //   83600400             | and                 dword ptr [eax + 4], 0
            //   e9????????           |                     
            //   8b461c               | mov                 eax, dword ptr [esi + 0x1c]

        $sequence_6 = { 83f8fb 7509 c7461804414100 eb28 }
            // n = 4, score = 100
            //   83f8fb               | cmp                 eax, -5
            //   7509                 | jne                 0xb
            //   c7461804414100       | mov                 dword ptr [esi + 0x18], 0x414104
            //   eb28                 | jmp                 0x2a

        $sequence_7 = { ff5508 8945fc 8b45fc 5e c9 c3 c3 }
            // n = 7, score = 100
            //   ff5508               | call                dword ptr [ebp + 8]
            //   8945fc               | mov                 dword ptr [ebp - 4], eax
            //   8b45fc               | mov                 eax, dword ptr [ebp - 4]
            //   5e                   | pop                 esi
            //   c9                   | leave               
            //   c3                   | ret                 
            //   c3                   | ret                 

        $sequence_8 = { 59 56 6804010000 8db5fcfeffff e8???????? }
            // n = 5, score = 100
            //   59                   | pop                 ecx
            //   56                   | push                esi
            //   6804010000           | push                0x104
            //   8db5fcfeffff         | lea                 esi, dword ptr [ebp - 0x104]
            //   e8????????           |                     

        $sequence_9 = { f7da 33f0 b9e6010000 f7d2 }
            // n = 4, score = 100
            //   f7da                 | neg                 edx
            //   33f0                 | xor                 esi, eax
            //   b9e6010000           | mov                 ecx, 0x1e6
            //   f7d2                 | not                 edx

    condition:
        7 of them and filesize < 294912
}