rule win_bit_rat_auto {

    meta:
        author = "Felix Bilstein - yara-signator at cocacoding dot com"
        date = "2020-12-22"
        version = "1"
        description = "autogenerated rule brought to you by yara-signator"
        tool = "yara-signator v0.6.0"
        signator_config = "callsandjumps;datarefs;binvalue"
        malpedia_reference = "https://malpedia.caad.fkie.fraunhofer.de/details/win.bit_rat"
        malpedia_rule_date = "20201222"
        malpedia_hash = "30354d830a29f0fbd3714d93d94dea941d77a130"
        malpedia_version = "20201023"
        malpedia_license = "CC BY-SA 4.0"
        malpedia_sharing = "TLP:WHITE"

    /* DISCLAIMER
     * The strings used in this rule have been automatically selected from the
     * disassembly of memory dumps and unpacked files, using YARA-Signator.
     * The code and documentation is published here:
     * https://github.com/fxb-cocacoding/yara-signator
     * As Malpedia is used as data source, please note that for a given
     * number of families, only single samples are documented.
     * This likely impacts the degree of generalization these rules will offer.
     * Take the described generation method also into consideration when you
     * apply the rules in your use cases and assign them confidence levels.
     */


    strings:
        $sequence_0 = { f7ea 0facd00d 2943fc c1fa0d 83e901 75e4 8b7510 }
            // n = 7, score = 100
            //   f7ea                 | imul                edx
            //   0facd00d             | shrd                eax, edx, 0xd
            //   2943fc               | sub                 dword ptr [ebx - 4], eax
            //   c1fa0d               | sar                 edx, 0xd
            //   83e901               | sub                 ecx, 1
            //   75e4                 | jne                 0xffffffe6
            //   8b7510               | mov                 esi, dword ptr [ebp + 0x10]

        $sequence_1 = { ffb658010000 e8???????? 6a00 68???????? ffb660010000 e8???????? 6a00 }
            // n = 7, score = 100
            //   ffb658010000         | push                dword ptr [esi + 0x158]
            //   e8????????           |                     
            //   6a00                 | push                0
            //   68????????           |                     
            //   ffb660010000         | push                dword ptr [esi + 0x160]
            //   e8????????           |                     
            //   6a00                 | push                0

        $sequence_2 = { e8???????? c645fc03 53 ff15???????? ff15???????? 8d45cc 50 }
            // n = 7, score = 100
            //   e8????????           |                     
            //   c645fc03             | mov                 byte ptr [ebp - 4], 3
            //   53                   | push                ebx
            //   ff15????????         |                     
            //   ff15????????         |                     
            //   8d45cc               | lea                 eax, [ebp - 0x34]
            //   50                   | push                eax

        $sequence_3 = { e8???????? 8bd8 6a00 6a00 85db 7504 6a41 }
            // n = 7, score = 100
            //   e8????????           |                     
            //   8bd8                 | mov                 ebx, eax
            //   6a00                 | push                0
            //   6a00                 | push                0
            //   85db                 | test                ebx, ebx
            //   7504                 | jne                 6
            //   6a41                 | push                0x41

        $sequence_4 = { f20f110424 50 e8???????? 83c428 8bb574ffffff 0f57c9 f20f104608 }
            // n = 7, score = 100
            //   f20f110424           | movsd               qword ptr [esp], xmm0
            //   50                   | push                eax
            //   e8????????           |                     
            //   83c428               | add                 esp, 0x28
            //   8bb574ffffff         | mov                 esi, dword ptr [ebp - 0x8c]
            //   0f57c9               | xorps               xmm1, xmm1
            //   f20f104608           | movsd               xmm0, qword ptr [esi + 8]

        $sequence_5 = { f7c200010000 7552 80be3c01000010 7549 0fb78e56010000 bf01010000 0fafcf }
            // n = 7, score = 100
            //   f7c200010000         | test                edx, 0x100
            //   7552                 | jne                 0x54
            //   80be3c01000010       | cmp                 byte ptr [esi + 0x13c], 0x10
            //   7549                 | jne                 0x4b
            //   0fb78e56010000       | movzx               ecx, word ptr [esi + 0x156]
            //   bf01010000           | mov                 edi, 0x101
            //   0fafcf               | imul                ecx, edi

        $sequence_6 = { f644242401 895c2410 7413 83faff 0f84f5000000 42 89542414 }
            // n = 7, score = 100
            //   f644242401           | test                byte ptr [esp + 0x24], 1
            //   895c2410             | mov                 dword ptr [esp + 0x10], ebx
            //   7413                 | je                  0x15
            //   83faff               | cmp                 edx, -1
            //   0f84f5000000         | je                  0xfb
            //   42                   | inc                 edx
            //   89542414             | mov                 dword ptr [esp + 0x14], edx

        $sequence_7 = { ff7514 50 ff750c 57 e8???????? 83c420 5f }
            // n = 7, score = 100
            //   ff7514               | push                dword ptr [ebp + 0x14]
            //   50                   | push                eax
            //   ff750c               | push                dword ptr [ebp + 0xc]
            //   57                   | push                edi
            //   e8????????           |                     
            //   83c420               | add                 esp, 0x20
            //   5f                   | pop                 edi

        $sequence_8 = { eb0e 8b07 8bcf ff5004 84c0 7430 8b7f04 }
            // n = 7, score = 100
            //   eb0e                 | jmp                 0x10
            //   8b07                 | mov                 eax, dword ptr [edi]
            //   8bcf                 | mov                 ecx, edi
            //   ff5004               | call                dword ptr [eax + 4]
            //   84c0                 | test                al, al
            //   7430                 | je                  0x32
            //   8b7f04               | mov                 edi, dword ptr [edi + 4]

        $sequence_9 = { ffb486a8010000 56 e8???????? 8b0d???????? 83c420 8bf0 ff742428 }
            // n = 7, score = 100
            //   ffb486a8010000       | push                dword ptr [esi + eax*4 + 0x1a8]
            //   56                   | push                esi
            //   e8????????           |                     
            //   8b0d????????         |                     
            //   83c420               | add                 esp, 0x20
            //   8bf0                 | mov                 esi, eax
            //   ff742428             | push                dword ptr [esp + 0x28]

    condition:
        7 of them and filesize < 19405824
}