rule project_relic_ransomware {

    meta:
        description = "Detects Project Relic ransomware in a running process"
        author = "Robel Campbell"
        reference = "https://cdn-production.blackpointcyber.com/wp-content/uploads/2022/11/17193215/Blackpoint-Cyber-Unearthing-Project-Relic-Whitepaper.pdf?utm_campaign=unearthing_project_relic_whitepaper&utm_source=linkedin&utm_medium=social&utm_content=apg_post"
        date = "2022-11-28"

    strings:
        // encoded strings prior to XOR decoding
        $1 = { CE 52 30 ED 08 9D 3F 03 E8 D4 B4 D5 90 5C 1E 2F 14 03 86 AA 1D 1B 03 82 43 8A 55 53 }
        $2 = { D9 C9 0D DA 69 F8 }
        $3 = { 06 3E 5B 90 34 8D 30 }
        $4 = { A8 D3 11 A8 33 8D A0 92 19 9C C9 50 74 30 }
        $5 = { 2A D7 50 8D 16 F9 A1 }


    condition:
        $1 or ($2 and $3 and $4 and $5)
}