import "pe"

private rule PEfile{
	condition:
		uint16(0) == 0x5A4D and uint32(uint32(0x3C)) == 0x00004550
}

rule syspicious_api{
	strings:
		$1 = "BlockInput" nocase wide ascii // interactive
		$2 = "SwitchDesktop" nocase wide ascii
		$3 = "GetProcessHeap" nocase wide ascii
	condition:
		PEfile and 
		any of them
}

rule api_kernel32 : Antidebug{
	strings:
		$s = "Kernel32.dll" nocase

		$s1 = "CheckRemoteDebuggerPresent" nocase wide ascii
		$s2 = "IsDebuggerPresent" nocase wide ascii

		// interactive_selfdebugging
		$s3 = "DebugActiveProcess" nocase wide ascii
	condition:
		PEfile and
		(pe.imports("kernel32.dll","CheckRemoteDebuggerPresent") or
		pe.imports("kernel32.dll","IsDebuggerPresent") or
		($s and 1 of ($s*)))
}

rule api_ntdll : Antidebug{
	strings:
		$s = "ntdll.dll" nocase
		$s1 = "NtQueryInformationProcess" nocase wide ascii

		$s2 = "RtlCreateQueryDebugBuffer" nocase wide ascii
		$s3 = "RtlQueryProcessHeapInformation" nocase wide ascii
		$s4 = "RtlQueryProcessDebugInformation" nocase wide ascii

		$s5 = "NtQuerySystemInformation" nocase wide ascii
		
		// object handles & exception
		$s6 = "NtOpenProcess" nocase wide ascii
		$s7 = "NtQueryObject" nocase wide ascii
		$s8 = "RtlRaiseException" nocase wide ascii

		$s9 = "DbgUiRemoteBreakin" nocase wide ascii
		
		// process nem
		$ss0 = "DbgBreakPoint" nocase wide ascii

		// interactive_selfdebugging
		$ss1 = "DbgUiDebugActiveProcess" nocase wide ascii
		$ss2 = "NtDebugActiveProcess" nocase wide ascii

		// interactive_hide thread
		$ss3 = "NtSetInformationThread" nocase wide ascii

	condition:
		PEfile and ($s and 1 of ($s*))
}

rule heap_const : antidebug_heapFlag{
	strings:
		$c = { 62000040 } // HEAP_TAIL_CHECKING_ENABLED | HEAP_FREE_CHECKING_ENABLED | HEAP_VALIDATE_PARAMETERS_ENABLE
		$c1 = { ABABABAB }
	condition:
		PEfile and any of them
}

rule load_peb {
	strings:
		$x32 = { 64 A1 ?? 00 00 00 } // mov eax, dword ptr ds:[??]
		$x64 = { 65 48 8B 04 25 ?? 00 00 00 } // mov rax, qword ptr qs:[??]
	condition:
		PEfile and any of them
}

rule openProcess_csrss : antidebug_objectHandle{
	strings:
		$s = "ntdll.dll" nocase
		$s1 = "CsrGetProcessId" nocase wide ascii
	condition:
		PEfile and 
		($s and 1 of ($s*)) or pe.imports("processthreadsapi.h", "OpenProcess")

}

rule closeHandle : antidebug_objectHandle{
	strings:
    	$s1 = "NtClose"  nocase wide ascii
    	$s2 = "CloseHandle"  nocase wide ascii
	condition:
		PEfile and any of them
}

rule loadLibrary: antidebug_objectHandle{
	strings:
		$s1 = "Loadlibrary" nocase wide ascii
		$s2 = "CreateFile" nocase wide ascii
	condition:
		PEfile and all of them
}

// maybe hight FP rate
rule setUnhandledExceptionFilter: antidebug_exception{
	strings:
		$ = "SetUnhandledExceptionFilter" nocase wide ascii
	condition:
		PEfile and any of them
}

rule raiseException : antidebug_exception{
	strings:
		$f = "RaiseException" nocase wide ascii
		$c1 = { 05 00 01 40 } // DBC_CONTROL_C (0x40010005)
		$c2 = { 07 00 01 40 } // DBG_RIPEVENT (0x40010007)
	condition:
		PEfile and 
		($c1 or $c2) and
		($f or pe.imports("kernel32.dll", "RaiseException"))
}

rule timing_check : Antidebug {
	strings:
		$rdtsc = { 0F 31 } 
    	$rdpmc = { 0F 33 }
		$s1 = "GetLocalTime" nocase wide ascii
		$s2 = "GetSystemTime" nocase wide ascii
		$s3 = "GetTickCount" nocase wide ascii
		$s4 = "timeGetTime" nocase wide ascii
		$s5 = "QueryPerformanceCounter" nocase wide ascii
		// KiGetTickCount
		$int_2a = { CD 2A }
	condition:
		PEfile and any of them
}

rule asm_instructions : Antidebug{
	strings:
		$int3 = { CC }
		$intCD = { CD }
		$int03 = { 03 }
		$int2D = { 2D }
		$ICE = { F1 }
		$Prefix = { F3 }
		$pushss_popss = { 16 17 }
		$pushf = { 9C }
	condition:
    	PEfile and any of them
}

rule antiStepOver : Antidebug_processMemory{
	strings:
		$s = "_ReturnAddress" nocase wide ascii
		$s1 = "VirtualProtect" nocase wide ascii

		$s2 = "WriteProcessMemory" nocase wide ascii
		$s3 = "Toolhelp32ReadProcessMemory" nocase wide ascii
	condition:
		PEfile and ($s and 1 of ($s*))
}

rule mem_bp: antidebug_processMem{
	strings:
		$c = { 40 10 } // PAGE_EXECUTE_READWRIRE| PAGE_GUARD = 140h
		$s = "VirtualProtect" nocase wide ascii
	condition:
		PEfile and
		$c and all of them 
	}

rule hardware_bp : antidebug_hardwarebp{
	strings:
		$s = "GetThreadContext"  nocase wide ascii
		$c = { 10 00 00 00 }// CONTEXT_DEBUG_REGISTERS 0x00000010
	condition:
		PEfile and all of them
}

rule enumWindow_suspendThread : interactive {
	strings:
        $1 = "SuspendThread"  nocase wide ascii
        $2 = "NtSuspendThread"  nocase wide ascii
        $3 = "OpenThread"  nocase wide ascii
        $4 ="SetThreadContext"  nocase wide ascii
        $5 ="SetInformationThread"  nocase wide ascii
        $x1 ="CreateToolHelp32Snapshot"  nocase wide ascii
        $x2 ="EnumWindows"  nocase wide ascii
    condition:
       PEfile and 3 of them
}

rule parent_proc_check {
	strings:
		$e = "explorer.exe" nocase
		$s1 = "CreateToolhelp32Snapshot"  nocase wide ascii
		$s2 = "GetParentProcessId" nocase wide ascii
		$s3 = "Process32Next" nocase wide ascii
		$s4 = "GetShellWindow" nocase wide ascii
		$s5 = "NtQueryInformationProcess" nocase wide ascii
	condition:
		PEfile and 2 of them
}
