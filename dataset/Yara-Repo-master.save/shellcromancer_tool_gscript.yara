rule tool_gscript {
    meta:
        description = "Identify the gscript dropper generator."
        author = "@shellcromancer <root@shellcromancer.io>"
        version = "0.1"
        date = "2022-01-18"
        reference = "https://github.com/gen0cide/gscript"
    strings:
        $ = "gen0cide/gscript" ascii

        // TODO:
        // match on built-in obfuscation i.e. Mordor
        // https://github.com/gen0cide/gscript/blob/master/compiler/obfuscator/mordorifier.go#L64
    condition:
        any of them
}