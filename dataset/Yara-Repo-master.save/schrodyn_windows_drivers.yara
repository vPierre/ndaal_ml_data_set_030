import "pe"

rule MAL_WIN_DRIVER_SIGNED_X_00
{
    meta:
        OneHundredDaysOfYARA    = "12"
        author                  = "Conor Quigley <schrodinger@konundrum.org>"
        description             = "Hunting for drivers signed by X."
        license                 = "BSD-2-Clause"
        created                 = "2022-01-12"
        version                 = "1.0"

    condition:
        uint16(0) == 0x5A4D
        and (uint32(uint32(0x3C)) == 0x00004550)
        and pe.number_of_signatures > 1
        and pe.imports("ntoskrnl.exe")
        and for any i in (0..pe.number_of_signatures-1):
        (
            pe.signatures[i].subject contains "SWDKTestCert hushangbin"
            //and pe.signatures[i].not_before >= 1580515200
        )
}

rule HUNT_WIN_DRIVER_UNSIGNED_00
{
    meta:
        OneHundredDaysOfYARA    = "12"
        author                  = "Conor Quigley <schrodinger@konundrum.org>"
        description             = "Hunting for unsigned drivers."
        license                 = "BSD-2-Clause"
        created                 = "2022-01-12"
        version                 = "1.0"

    condition:
        uint16(0) == 0x5A4D
        and (uint32(uint32(0x3C)) == 0x00004550)
        and pe.number_of_signatures == 0
        and pe.imports("ntoskrnl.exe")
}

rule HUNT_WIN_DRIVER_CONTAINS_RSRC_00
{
    meta:
        OneHundredDaysOfYARA    = "12"
        author                  = "Conor Quigley <schrodinger@konundrum.org>"
        description             = "Hunting for drivers with resources."
        license                 = "BSD-2-Clause"
        created                 = "2022-01-12"
        version                 = "1.0"

    condition:
        uint16(0) == 0x5A4D
        and (uint32(uint32(0x3C)) == 0x00004550)
        and pe.imports("ntoskrnl.exe")
        and pe.number_of_resources > 0
}