rule Msdeploy_exe0_Execute
{
    meta:
        id = "3S9xr0YazizOJeJQwRiScZ"
        fingerprint = "f9996f8ce8713766e4b984cf76062bfd5d33ada6d9bc65b9f3ed2537a496e0f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft tool used to deploy Web Applications."
        category = "TECHNIQUE"
        technique = "LAUNCH CALC.BAT VIA MSDEPLOY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msdeploy.exe -verb:sync -source:RunCommand -dest:runCommand=\"c:\\temp\\calc.bat\"" nocase

condition:
    $a0
}

rule Msdeploy_exe1_AWL_Bypass
{
    meta:
        id = "306Qri6um9HTZNALhUyumu"
        fingerprint = "f9996f8ce8713766e4b984cf76062bfd5d33ada6d9bc65b9f3ed2537a496e0f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft tool used to deploy Web Applications."
        category = "TECHNIQUE"
        technique = "LAUNCH CALC.BAT VIA MSDEPLOY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msdeploy.exe -verb:sync -source:RunCommand -dest:runCommand=\"c:\\temp\\calc.bat\"" nocase

condition:
    $a0
}

