rule Pester_bat0_Execute
{
    meta:
        id = "7OJAekQ5Bwj7ZdYRHr9uyu"
        fingerprint = "1546786b6891b4b9fe52419e9370df9ee966fa01783f866edb53f12f3915c3b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used as part of the Powershell pester"
        category = "TECHNIQUE"
        technique = "EXECUTE CODE USING PESTER. THE THIRD PARAMETER CAN BE ANYTHING. THE FOURTH IS THE PAYLOAD. EXAMPLE HERE EXECUTES NOTEPAD"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Pester.bat [/help|?|-?|/?] \"$null; notepad\"" nocase

condition:
    $a0
}

rule Pester_bat1_Execute
{
    meta:
        id = "3fFfIoLjfmEhiWBQvMPbGa"
        fingerprint = "ba250c6d54121b029c459d904fc27ddc2dd2b5da9a6f7ae47cd357653b36ee6a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used as part of the Powershell pester"
        category = "TECHNIQUE"
        technique = "EXECUTE CODE USING PESTER. EXAMPLE HERE EXECUTES CALC.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Pester.bat ;calc.exe" nocase

condition:
    $a0
}

rule Pester_bat2_Execute
{
    meta:
        id = "3C43a5s22l6LttnDbepuzP"
        fingerprint = "ba250c6d54121b029c459d904fc27ddc2dd2b5da9a6f7ae47cd357653b36ee6a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used as part of the Powershell pester"
        category = "TECHNIQUE"
        technique = "EXECUTE CODE USING PESTER. EXAMPLE HERE EXECUTES CALC.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Pester.bat ;calc.exe" nocase

condition:
    $a0
}

