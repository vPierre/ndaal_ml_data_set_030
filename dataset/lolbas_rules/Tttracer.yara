rule Tttracer_exe0_Execute
{
    meta:
        id = "5WrrCBidlLxbQc9leSXONj"
        fingerprint = "a8a67677179955a89998d803f97e3cb633246fbe4993129c4580625a50f97813"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC USING TTTRACER.EXE. REQUIRES ADMINISTRATOR PRIVILEGES"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="tttracer.exe C:\\windows\\system32\\calc.exe" nocase

condition:
    $a0
}

rule Tttracer_exe1_Dump
{
    meta:
        id = "4134qyEMIqjcUFyUhOyqZE"
        fingerprint = "32839d70550f6470151d73cda69708d714179e080c8e5b2da62304c5a7ea8779"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel"
        category = "TECHNIQUE"
        technique = "DUMPS PROCESS USING TTTRACER.EXE. REQUIRES ADMINISTRATOR PRIVILEGES"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="TTTracer.exe -dumpFull -attach pid" nocase

condition:
    $a0
}

