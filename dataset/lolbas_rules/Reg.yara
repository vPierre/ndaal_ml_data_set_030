rule Reg_exe0_ADS
{
    meta:
        id = "4WWD18oeHYHlGkw9P9QBSp"
        fingerprint = "2a82809283cb342b2813e0fe57897111f3b50902af3fdf038d2f99acd595276a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to manipulate the registry"
        category = "TECHNIQUE"
        technique = "EXPORT THE TARGET REGISTRY KEY AND SAVE IT TO THE SPECIFIED .REG FILE WITHIN AN ALTERNATE DATA STREAM."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="reg export HKLM\\SOFTWARE\\Microsoft\\Evilreg c:\\ads\\file.txt:evilreg.reg" nocase

condition:
    $a0
}

rule Reg_exe1_Credentials
{
    meta:
        id = "6ZOpZlHBaWmdpE4guDY4Sk"
        fingerprint = "74fe26603a46d18b09265bcc8e6056ecece5e8c11fa2ad93e430266b3137e1ef"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to manipulate the registry"
        category = "TECHNIQUE"
        technique = "DUMP REGISTRY HIVES (SAM, SYSTEM, SECURITY) TO RETRIEVE PASSWORD HASHES AND KEY MATERIAL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="reg save HKLM\\SECURITY c:\\test\\security.bak && reg save HKLM\\SYSTEM c:\\test\\system.bak && reg save HKLM\\SAM c:\\test\\sam.bak" nocase

condition:
    $a0
}

