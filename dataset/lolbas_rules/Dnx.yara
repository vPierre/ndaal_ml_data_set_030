rule dnx_exe0_Execute
{
    meta:
        id = "5BFFqWHQe4e8c7gXvwaxSW"
        fingerprint = "2b7ee3c2f3caff62cd501b9bc0f40fb6d7c6b9163f15939f9dbe1fa6e79a72dc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = ".Net Execution environment file included with .Net."
        category = "TECHNIQUE"
        technique = "EXECUTE C# CODE LOCATED IN THE CONSOLEAPP FOLDER VIA 'PROGRAM.CS' AND 'PROJECT.JSON' (NOTE - REQUIRES DEPENDENCIES)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dnx.exe consoleapp" nocase

condition:
    $a0
}

