rule Msedge_exe0_Download
{
    meta:
        id = "1rQ7hubzzLymhOKjmiTzjd"
        fingerprint = "6d41a1ec3ead99f6e49f7671bfefd74539a08aac77f08c8b5069548a40163ceb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Edge browser"
        category = "TECHNIQUE"
        technique = "EDGE WILL LAUNCH AND DOWNLOAD THE FILE. A HARMLESS FILE EXTENSION (E.G. .TXT, .ZIP) SHOULD BE APPENDED TO AVOID SMARTSCREEN."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedge.exe " nocase

condition:
    $a0
}

rule Msedge_exe1_Download
{
    meta:
        id = "6sxYx4yfz1Jb7YFF0BNu43"
        fingerprint = "866b05c48dfd4494ea68bdae40a7dcd311cc05a7cf4aa9a1b96bf23e553aae44"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Edge browser"
        category = "TECHNIQUE"
        technique = "EDGE WILL SILENTLY DOWNLOAD THE FILE. FILE EXTENSION SHOULD BE .HTML AND BINARIES SHOULD BE ENCODED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedge.exe --headless --enable-logging --disable-gpu --dump-dom \" > out.b64" nocase

condition:
    $a0
}

rule Msedge_exe2_Execute
{
    meta:
        id = "4GHk4ienYI9nEc4x9UFWK2"
        fingerprint = "91557cb8ecd573067e0bb620332729a72fa160748bb2baf0c85030d2b90a6728"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Edge browser"
        category = "TECHNIQUE"
        technique = "EDGE SPAWNS CMD.EXE AS A CHILD PROCESS OF MSEDGE.EXE AND EXECUTES THE PING COMMAND"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedge.exe --disable-gpu-sandbox --gpu-launcher=\"C:\\Windows\\system32\\cmd.exe /c ping google.com &&\"" nocase

condition:
    $a0
}

