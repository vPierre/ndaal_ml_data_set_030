rule fltMC_exe0_Tamper
{
    meta:
        id = "1YdsQiGDMcyECUMHZrWO4v"
        fingerprint = "7b1bba664064ca4ab72bbba54183a4d9088f78e6c3cb02165acd9c374036c492"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Filter Manager Control Program used by Windows"
        category = "TECHNIQUE"
        technique = "UNLOADS A DRIVER USED BY SECURITY AGENTS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fltMC.exe unload SysmonDrv" nocase

condition:
    $a0
}

