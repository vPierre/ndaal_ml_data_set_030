rule AgentExecutor_exe0_Execute
{
    meta:
        id = "LA3QryKINeZxw1yvBfMUZ"
        fingerprint = "ffa1a4806958695e474b10ebda27186676fa12e0cb713b4abb6bef1710d1802f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Intune Management Extension included on Intune Managed Devices"
        category = "TECHNIQUE"
        technique = "SPAWNS POWERSHELL.EXE AND EXECUTES A PROVIDED POWERSHELL SCRIPT WITH EXECUTIONPOLICY BYPASS ARGUMENT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AgentExecutor.exe -powershell \"c:\\temp\\malicious.ps1\" \"c:\\temp\\test.log\" \"c:\\temp\\test1.log\" \"c:\\temp\\test2.log\" 60000 \"C:\\Windows\\SysWOW64\\WindowsPowerShell\\v1.0\" 0 1" nocase

condition:
    $a0
}

rule AgentExecutor_exe1_Execute
{
    meta:
        id = "6rNVwi8qCJ5ONooXN8hy21"
        fingerprint = "862d491441177ae96925668624a04c01e0d9c7017738d1d0b1d66f64edd0e307"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Intune Management Extension included on Intune Managed Devices"
        category = "TECHNIQUE"
        technique = "IF WE PLACE A BINARY NAMED POWERSHELL.EXE IN THE PATH C:\\TEMP, AGENTEXECUTOR.EXE WILL EXECUTE IT SUCCESSFULLY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AgentExecutor.exe -powershell \"c:\\temp\\malicious.ps1\" \"c:\\temp\\test.log\" \"c:\\temp\\test1.log\" \"c:\\temp\\test2.log\" 60000 \"C:\\temp\\\" 0 1" nocase

condition:
    $a0
}

