rule ntdsutil_exe0_Dump
{
    meta:
        id = "4sr4nD1sTnUHWD90m75q0e"
        fingerprint = "8e12586df19b3f16db3018644f0332e584750926dbdfa5f9cd8ab80124dabcf8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to export Active Directory."
        category = "TECHNIQUE"
        technique = "DUMP NTDS.DIT INTO FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ntdsutil.exe \"ac i ntds\" \"ifm\" \"create full c:\\\" q q" nocase

condition:
    $a0
}

