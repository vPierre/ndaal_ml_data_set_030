rule FsiAnyCpu_exe0_AWL_Bypass
{
    meta:
        id = "12DtHCuDiyPRBHXjSZ8NtO"
        fingerprint = "aacfee93be8a204da915a4a20e8f75f365e6e728b69bb6f0416ebb4a00af43cb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "32/64-bit FSharp (F#) Interpreter included with Visual Studio."
        category = "TECHNIQUE"
        technique = "EXECUTE F# CODE VIA SCRIPT FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fsianycpu.exe c:\\path\\to\\test.fsscript" nocase

condition:
    $a0
}

rule FsiAnyCpu_exe1_AWL_Bypass
{
    meta:
        id = "1iCSupVoO73j4bqoCkIcxi"
        fingerprint = "5f43ad925ecd674e0379c6649436c0fa31efed6fcc0329d242475e8eba015862"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "32/64-bit FSharp (F#) Interpreter included with Visual Studio."
        category = "TECHNIQUE"
        technique = "EXECUTE F# CODE VIA INTERACTIVE COMMAND LINE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fsianycpu.exe" nocase

condition:
    $a0
}

