rule vsls_agent_exe0_Execute
{
    meta:
        id = "4LIKu8s3oCramY9Tb6ZT17"
        fingerprint = "7178deb50ed3acabc8ad82a5a9c6f88d4843dd6b582360560df73cb97feaad11"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Agent for Visual Studio Live Share (Code Collaboration)"
        category = "TECHNIQUE"
        technique = "LOAD A LIBRARY PAYLOAD USING THE --AGENTEXTENSIONPATH PARAMETER (32-BIT)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="vsls-agent.exe --agentExtensionPath c:\\path\\to\\payload.dll" nocase

condition:
    $a0
}

