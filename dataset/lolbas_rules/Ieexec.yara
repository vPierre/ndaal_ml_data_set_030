rule Ieexec_exe0_Download
{
    meta:
        id = "7N34A6TQE3dDj6QAbHiVrx"
        fingerprint = "2cf2d73f2d80cc4e9c691edb23ad0b76449692d0a44e7296cc2d3be95a884cd8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The IEExec.exe application is an undocumented Microsoft .NET Framework application that is included with the .NET Framework. You can use the IEExec.exe application as a host to run other managed applications that you start by using a URL."
        category = "TECHNIQUE"
        technique = "DOWNLOADS AND EXECUTES BYPASS.EXE FROM THE REMOTE SERVER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ieexec.exe " nocase

condition:
    $a0
}

rule Ieexec_exe1_Execute
{
    meta:
        id = "6SiHgpdywxkCP9Ldxp7iF6"
        fingerprint = "2cf2d73f2d80cc4e9c691edb23ad0b76449692d0a44e7296cc2d3be95a884cd8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The IEExec.exe application is an undocumented Microsoft .NET Framework application that is included with the .NET Framework. You can use the IEExec.exe application as a host to run other managed applications that you start by using a URL."
        category = "TECHNIQUE"
        technique = "DOWNLOADS AND EXECUTES BYPASS.EXE FROM THE REMOTE SERVER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ieexec.exe " nocase

condition:
    $a0
}

