rule Print_exe0_ADS
{
    meta:
        id = "6mvtvqCMVzH6ZcQ5X1v5Ea"
        fingerprint = "2486af6a0783fe4dff2287c5ab1ea0f10da728cd8cb5a5fd082b1138ed3cdbbc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to send files to the printer"
        category = "TECHNIQUE"
        technique = "COPY FILE.EXE INTO THE ALTERNATE DATA STREAM (ADS) OF FILE.TXT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="print /D:C:\\ADS\\File.txt:file.exe C:\\ADS\\File.exe" nocase

condition:
    $a0
}

rule Print_exe1_Copy
{
    meta:
        id = "6vWNHIYGVWjwm3y0T4mhHU"
        fingerprint = "43873bb748c3fca5046a2799795ff0ed0196c6d4bd4699861de962ab79f25311"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to send files to the printer"
        category = "TECHNIQUE"
        technique = "COPY FILETOCOPY.EXE TO THE TARGET C:\\ADS\\COPYOFFILE.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="print /D:C:\\ADS\\CopyOfFile.exe C:\\ADS\\FileToCopy.exe" nocase

condition:
    $a0
}

rule Print_exe2_Copy
{
    meta:
        id = "27SRxBWcmdthUwhIFcrFFP"
        fingerprint = "083230b8a0154fa2a621abc1000550992e3fb3ba5ea20f225a10119480423703"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to send files to the printer"
        category = "TECHNIQUE"
        technique = "COPY FILE.EXE FROM A NETWORK SHARE TO THE TARGET C:\\OUTFOLDER\\OUTFILE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="print /D:C:\\OutFolder\\outfile.exe \\\\WebDavServer\\Folder\\File.exe" nocase

condition:
    $a0
}

