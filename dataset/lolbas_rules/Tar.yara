rule Tar_exe0_Copy
{
    meta:
        id = "agn4njtTCTdOjk78HzO4n"
        fingerprint = "c45228911895c661b967c633cf12f3953d0d2657e98343b90bf3abdac15622dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to extract and create archives."
        category = "TECHNIQUE"
        technique = "EXTRACTS ARCHIVE.TAR FROM THE REMOTE (INTERNAL) HOST (HOST1) TO THE CURRENT HOST."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="tar -xf \\\\host1\\archive.tar" nocase

condition:
    $a0
}

