rule Devinit_exe0_Execute
{
    meta:
        id = "1rMieoceREjjP6HVB61I7e"
        fingerprint = "591c262701179d586b2a6d895d868d2928a7e6d544f9ab72cac2e48967aa8e74"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Visual Studio 2019 tool"
        category = "TECHNIQUE"
        technique = "DOWNLOADS AN MSI FILE TO C:\\WINDOWS\\INSTALLER AND THEN INSTALLS IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="devinit.exe run -t msi-install -i " nocase

condition:
    $a0
}

