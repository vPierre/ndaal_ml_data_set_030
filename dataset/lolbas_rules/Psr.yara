rule Psr_exe0_Reconnaissance
{
    meta:
        id = "1QWjnX2eImlBrxrfFdHnQw"
        fingerprint = "51c9c67759af3b61ebb3aca2fa1b058deca7fb240770c4c06054abb6c537ae19"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Problem Steps Recorder, used to record screen and clicks."
        category = "TECHNIQUE"
        technique = "RECORD A USER SCREEN WITHOUT CREATING A GUI. YOU SHOULD USE \"PSR.EXE /STOP\" TO STOP RECORDING AND CREATE OUTPUT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="psr.exe /start /output D:\\test.zip /sc 1 /gui 0" nocase

condition:
    $a0
}

