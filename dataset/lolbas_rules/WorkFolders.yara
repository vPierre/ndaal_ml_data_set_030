rule WorkFolders_exe0_Execute
{
    meta:
        id = "2cpV64brFnA8D0VqkK0bgK"
        fingerprint = "3fa63017e93e0ff955e51af9940090cc380c230ea14a2dd4e2d8aa8cd3c6971e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Work Folders"
        category = "TECHNIQUE"
        technique = "EXECUTE CONTROL.EXE IN THE CURRENT WORKING DIRECTORY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="WorkFolders" nocase

condition:
    $a0
}

