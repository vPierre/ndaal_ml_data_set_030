rule GfxDownloadWrapper_exe0_Download
{
    meta:
        id = "3KTzmDVHJubcFAo92GJ8jC"
        fingerprint = "714df61340036bc0e841525d077f01ac331a19d8d4db9001ca7b7fc5046ce0e8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Remote file download used by the Intel Graphics Control Panel, receives as first parameter a URL and a destination file path."
        category = "TECHNIQUE"
        technique = "GFXDOWNLOADWRAPPER.EXE DOWNLOADS THE CONTENT THAT RETURNS URL AND WRITES IT TO THE FILE DESTINATION FILE PATH. THE BINARY IS SIGNED BY \"MICROSOFT WINDOWS HARDWARE\", \"COMPATIBILITY PUBLISHER\", \"MICROSOFT WINDOWS THIRD PARTY COMPONENT CA 2012\", \"MICROSOFT TIME-STAMP PCA 2010\", \"MICROSOFT TIME-STAMP SERVICE\"."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="C:\\Windows\\System32\\DriverStore\\FileRepository\\igdlh64.inf_amd64_[0-9]+\\GfxDownloadWrapper.exe \"URL\" \"DESTINATION FILE\"" nocase

condition:
    $a0
}

