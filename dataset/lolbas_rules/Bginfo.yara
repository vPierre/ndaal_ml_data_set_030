rule Bginfo_exe0_Execute
{
    meta:
        id = "1AByOo5zgjMVdKuamoQTqF"
        fingerprint = "712d8fc3689614c86d07312cf754427f6002f974232a207e2dc36998c24b6f91"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Background Information Utility included with SysInternals Suite"
        category = "TECHNIQUE"
        technique = "EXECUTE VBSCRIPT CODE THAT IS REFERENCED WITHIN THE BGINFO.BGI FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bginfo.exe bginfo.bgi /popup /nolicprompt" nocase

condition:
    $a0
}

rule Bginfo_exe1_AWL_Bypass
{
    meta:
        id = "6ezhrMg4Sdk3LcLct64Nxm"
        fingerprint = "712d8fc3689614c86d07312cf754427f6002f974232a207e2dc36998c24b6f91"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Background Information Utility included with SysInternals Suite"
        category = "TECHNIQUE"
        technique = "EXECUTE VBSCRIPT CODE THAT IS REFERENCED WITHIN THE BGINFO.BGI FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bginfo.exe bginfo.bgi /popup /nolicprompt" nocase

condition:
    $a0
}

rule Bginfo_exe2_Execute
{
    meta:
        id = "cyncEDNK7M3tWRoPCm7SZ"
        fingerprint = "ee28771b568523c06a442d27de952618bac16c13c5ac4a98a9787ec4b58a47d6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Background Information Utility included with SysInternals Suite"
        category = "TECHNIQUE"
        technique = "EXECUTE BGINFO.EXE FROM A WEBDAV SERVER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="\\\\10.10.10.10\\webdav\\bginfo.exe bginfo.bgi /popup /nolicprompt" nocase

condition:
    $a0
}

rule Bginfo_exe3_AWL_Bypass
{
    meta:
        id = "7b0wWg4VQfxxjBLNgQ3M50"
        fingerprint = "ee28771b568523c06a442d27de952618bac16c13c5ac4a98a9787ec4b58a47d6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Background Information Utility included with SysInternals Suite"
        category = "TECHNIQUE"
        technique = "EXECUTE BGINFO.EXE FROM A WEBDAV SERVER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="\\\\10.10.10.10\\webdav\\bginfo.exe bginfo.bgi /popup /nolicprompt" nocase

condition:
    $a0
}

rule Bginfo_exe4_Execute
{
    meta:
        id = "5wb8optMBtITCMlsIcBRLL"
        fingerprint = "5f4359e8c4e692c6246ebabed4af20ecf310f5ecdbbf401df418d763d42fd086"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Background Information Utility included with SysInternals Suite"
        category = "TECHNIQUE"
        technique = "THIS STYLE OF EXECUTION MAY NOT LONGER WORK DUE TO PATCH."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="\\\\live.sysinternals.com\\Tools\\bginfo.exe \\\\10.10.10.10\\webdav\\bginfo.bgi /popup /nolicprompt" nocase

condition:
    $a0
}

rule Bginfo_exe5_AWL_Bypass
{
    meta:
        id = "7LVWifJOnfFM4IupF72IDk"
        fingerprint = "5f4359e8c4e692c6246ebabed4af20ecf310f5ecdbbf401df418d763d42fd086"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Background Information Utility included with SysInternals Suite"
        category = "TECHNIQUE"
        technique = "THIS STYLE OF EXECUTION MAY NOT LONGER WORK DUE TO PATCH."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="\\\\live.sysinternals.com\\Tools\\bginfo.exe \\\\10.10.10.10\\webdav\\bginfo.bgi /popup /nolicprompt" nocase

condition:
    $a0
}

