rule Shdocvw_dll0_Execute
{
    meta:
        id = "5XDkikbeL57B3XfU4jrH79"
        fingerprint = "eaf7ac7de13eeda07efd6ccf4edf1d55e8aeb9056b12ddec3a480264ceb1b39f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Shell Doc Object and Control Library."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD VIA PROXY THROUGH A URL (INFORMATION) FILE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe shdocvw.dll,OpenURL \"C:\\test\\calc.url\"" nocase

condition:
    $a0
}

