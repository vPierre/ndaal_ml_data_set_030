rule Dump64_exe0_Dump
{
    meta:
        id = "5nmtGzmUUA8an9GqQHsBs"
        fingerprint = "45124f3a91bd3ba47e16cbd2338903c81a938cde13594189b6470bc0be14786f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Memory dump tool that comes with Microsoft Visual Studio"
        category = "TECHNIQUE"
        technique = "CREATES A MEMORY DUMP OF THE LSASS PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dump64.exe <pid> out.dmp" nocase

condition:
    $a0
}

