rule Scriptrunner_exe0_Execute
{
    meta:
        id = "5m1CL92ScMBFtEPgqSt0GV"
        fingerprint = "930f6371eff6d6fdd94283f5202ab607a10fb5dc26efb58fa8155a3cfba71344"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute binary through proxy binary to evade defensive counter measures"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Scriptrunner.exe -appvscript calc.exe" nocase

condition:
    $a0
}

rule Scriptrunner_exe1_Execute
{
    meta:
        id = "RowV2Lp957GDi2rd1L88m"
        fingerprint = "aac27fac31a0fc9f23b2d8388065e5757faf2d516a6e79ac29af42f0e80871f9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute binary through proxy binary to evade defensive counter measures"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.CMD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ScriptRunner.exe -appvscript \"\\\\fileserver\\calc.cmd\"" nocase

condition:
    $a0
}

