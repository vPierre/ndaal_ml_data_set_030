rule Netsh_exe0_Execute
{
    meta:
        id = "3aQbz3KB8mis8xSPrbMNaf"
        fingerprint = "f514ebd21c27cae3bd41b148a5e178db0ff06725ee02df3daeb903c661a37783"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Netsh is a Windows tool used to manipulate network interface settings."
        category = "TECHNIQUE"
        technique = "USE NETSH IN ORDER TO EXECUTE A .DLL FILE AND ALSO GAIN PERSISTENCE, EVERY TIME THE NETSH COMMAND IS CALLED"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="netsh.exe add helper C:\\Users\\User\\file.dll" nocase

condition:
    $a0
}

