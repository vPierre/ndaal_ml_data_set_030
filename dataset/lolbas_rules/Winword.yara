rule Winword_exe0_Download
{
    meta:
        id = "2oza3gAqDwi7PkNPDcnU6L"
        fingerprint = "c73e4d6e94400c650fa5d1dfba8863e5fe83a3f7e13f2d6dfeb36df2a157cdf5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="winword.exe \"" nocase

condition:
    $a0
}

