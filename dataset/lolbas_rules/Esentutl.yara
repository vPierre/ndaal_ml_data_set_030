rule Esentutl_exe0_Copy
{
    meta:
        id = "1tkAtBORc7csZ9cUhQPRBM"
        fingerprint = "3a1a207e817e30de6fa5f523bbce8b96ae9cd57b854bbbccba2a5b51cf5c30f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE VBS FILE TO THE DESTINATION VBS FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="esentutl.exe /y C:\\folder\\sourcefile.vbs /d C:\\folder\\destfile.vbs /o" nocase

condition:
    $a0
}

rule Esentutl_exe1_ADS
{
    meta:
        id = "6yFUViS6Wo3AOZRb5z9tup"
        fingerprint = "a2d7d4ec70ca5286c4e6f1c2273d9cab75ad253f4fa0a3e45cc953cf29cb8dd1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE EXE TO AN ALTERNATE DATA STREAM (ADS) OF THE DESTINATION FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="esentutl.exe /y C:\\ADS\\file.exe /d c:\\ADS\\file.txt:file.exe /o" nocase

condition:
    $a0
}

rule Esentutl_exe2_ADS
{
    meta:
        id = "7GMdSnbxSjysk07kWI7opm"
        fingerprint = "bafce268d13cac4ba72d737a2bd2d3b2c6ba3805044a9a408b54bfe05cf60a4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE ALTERNATE DATA STREAM (ADS) TO THE DESTINATION EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="esentutl.exe /y C:\\ADS\\file.txt:file.exe /d c:\\ADS\\file.exe /o" nocase

condition:
    $a0
}

rule Esentutl_exe3_ADS
{
    meta:
        id = "4qhHEqBcnkw9tweC3phe99"
        fingerprint = "f1ecf5a1c5e561255572d6c4022516feb9e341650ac56047ff631c9daae8ab5f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE REMOTE SOURCE EXE TO THE DESTINATION ALTERNATE DATA STREAM (ADS) OF THE DESTINATION FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="esentutl.exe /y \\\\192.168.100.100\\webdav\\file.exe /d c:\\ADS\\file.txt:file.exe /o" nocase

condition:
    $a0
}

rule Esentutl_exe4_Download
{
    meta:
        id = "6oenwZqvnM4Eod6GA7MFaS"
        fingerprint = "2e2ea365a08adc20f4f684f07bacf39ead371aea9268e0a3a1b9ec8052c33dba"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE EXE TO THE DESTINATION EXE FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="esentutl.exe /y \\\\live.sysinternals.com\\tools\\adrestore.exe /d \\\\otherwebdavserver\\webdav\\adrestore.exe /o" nocase

condition:
    $a0
}

rule Esentutl_exe5_Copy
{
    meta:
        id = "5yCAzTQVXe42U5dmIt5qqt"
        fingerprint = "c70daf1ca0d24ebbeb156c80ef1c113d68efe354a7145eb1e9bf3f471817d0bb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES A (LOCKED) FILE USING VOLUME SHADOW COPY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="esentutl.exe /y /vss c:\\windows\\ntds\\ntds.dit /d c:\\folder\\ntds.dit" nocase

condition:
    $a0
}

