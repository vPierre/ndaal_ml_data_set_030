rule Comsvcs_dll0_Dump
{
    meta:
        id = "H85WvbqCWhcNLCb0SqRqY"
        fingerprint = "50bdd116dc09db8e3375051ee2ded1d863a29e4166848b8035071ad4b68f6a6e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "COM+ Services"
        category = "TECHNIQUE"
        technique = "CALLS THE MINIDUMP EXPORTED FUNCTION OF COMSVCS.DLL, WHICH IN TURNS CALLS MINIDUMPWRITEDUMP."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32 C:\\windows\\system32\\comsvcs.dll MiniDump [LSASS_PID] dump.bin full" nocase

condition:
    $a0
}

