rule Createdump_exe0_Dump
{
    meta:
        id = "5AOE5GiUWTohpDlbqQP5Ju"
        fingerprint = "149c70544f235f0b424f2e69cbaca01dcc3b005d265900d56ad7099df669c3c9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft .NET Runtime Crash Dump Generator (included in .NET Core)"
        category = "TECHNIQUE"
        technique = "DUMP PROCESS BY PID AND CREATE A MINIDUMP FILE. IF \"-F DUMP.DMP\" IS NOT SPECIFIED, THE FILE IS CREATED AS '%TEMP%\\DUMP.%P.DMP' WHERE %P IS THE PID OF THE TARGET PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="createdump.exe -n -f dump.dmp [PID]" nocase

condition:
    $a0
}

