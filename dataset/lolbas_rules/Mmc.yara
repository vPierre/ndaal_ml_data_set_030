rule Mmc_exe0_Execute
{
    meta:
        id = "2bNjLbCZoorCxzNIY1PVxU"
        fingerprint = "61ff93fc34544f223253805914603381cd5b6ba3706529d05330b9aa6c95fd72"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Load snap-ins to locally and remotely manage Windows systems"
        category = "TECHNIQUE"
        technique = "LAUNCH A 'BACKGROUNDED' MMC PROCESS AND INVOKE A COM PAYLOAD"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mmc.exe -Embedding c:\\path\\to\\test.msc" nocase

condition:
    $a0
}

rule Mmc_exe1_UAC_Bypass
{
    meta:
        id = "2zejnYM48zi4NTUvpkweRG"
        fingerprint = "e99b9bee59d05d798b49049828b8a1b002b618dc7366f02233cf431e3a0e45d6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Load snap-ins to locally and remotely manage Windows systems"
        category = "TECHNIQUE"
        technique = "LOAD AN ARBITRARY PAYLOAD DLL BY CONFIGURING COR PROFILER REGISTRY SETTINGS AND LAUNCHING MMC TO BYPASS UAC."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mmc.exe gpedit.msc" nocase

condition:
    $a0
}

