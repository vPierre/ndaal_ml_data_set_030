rule Mspub_exe0_Download
{
    meta:
        id = "3OrAHBQytiDyuKTQLAvJgM"
        fingerprint = "b007927ed2e383929e9d192e67e84f3e3b3b5e74a8ac0b88a9647586c186cc06"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Publisher"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mspub.exe " nocase

condition:
    $a0
}

