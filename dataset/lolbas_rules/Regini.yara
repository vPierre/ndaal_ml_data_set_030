rule Regini_exe0_ADS
{
    meta:
        id = "19GgAGMJMDWgsk0eWWx6b8"
        fingerprint = "2fb5c37350fe88f88695dfdf1022ad5b9dd12d80d15d462092f585498727b397"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to manipulate the registry"
        category = "TECHNIQUE"
        technique = "WRITE REGISTRY KEYS FROM DATA INSIDE THE ALTERNATE DATA STREAM."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regini.exe newfile.txt:hidden.ini" nocase

condition:
    $a0
}

