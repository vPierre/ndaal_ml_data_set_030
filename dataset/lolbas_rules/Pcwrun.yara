rule Pcwrun_exe0_Execute
{
    meta:
        id = "1yun83ws7rzgrUFr03sDZM"
        fingerprint = "dbe7ae378abdd67646a3131a925c06797fd3073d436c042ae3e5745219492898"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Wizard"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .EXE FILE WITH THE PROGRAM COMPATIBILITY WIZARD."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Pcwrun.exe c:\\temp\\beacon.exe" nocase

condition:
    $a0
}

rule Pcwrun_exe1_Execute
{
    meta:
        id = "O85FGgChfcorvHMNDzxGK"
        fingerprint = "4c2fdcd96384bb945e6f02f128d2131f21b537fbff6a779f2c4f175ac108b577"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Wizard"
        category = "TECHNIQUE"
        technique = "LEVERAGE THE MSDT FOLLINA VULNERABILITY THROUGH PCWRUN TO EXECUTE ARBITRARY COMMANDS AND BINARIES. NOTE THAT THIS SPECIFIC TECHNIQUE WILL NOT WORK ON A PATCHED SYSTEM WITH THE JUNE 2022 WINDOWS SECURITY UPDATE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Pcwrun.exe /../../$(calc).exe" nocase

condition:
    $a0
}

