rule Wab_exe0_Execute
{
    meta:
        id = "5Ck7CpbOLGPRc9depuUpeu"
        fingerprint = "0d1167c739ed33b2c9d6ac5197bc3909784d441646f69ddd5e35a8cc3013ba53"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows address book manager"
        category = "TECHNIQUE"
        technique = "CHANGE HKLM\\SOFTWARE\\MICROSOFT\\WAB\\DLLPATH AND EXECUTE DLL OF CHOICE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wab.exe" nocase

condition:
    $a0
}

