rule Extexport_exe0_Execute
{
    meta:
        id = "1pgTwPbTXX3gi9PdbeBnVh"
        fingerprint = "961d985a8805734e501b70224a1a7ca7fe2e3463b0dc11700a72f4200d8e2ab0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Load a DLL located in the c:\\test folder with a specific name."
        category = "TECHNIQUE"
        technique = "LOAD A DLL LOCATED IN THE C:\\TEST FOLDER WITH ONE OF THE FOLLOWING NAMES MOZCRT19.DLL, MOZSQLITE3.DLL, OR SQLITE.DLL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Extexport.exe c:\\test foo bar" nocase

condition:
    $a0
}

