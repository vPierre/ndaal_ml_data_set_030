rule MpCmdRun_exe0_Download
{
    meta:
        id = "5NADHnjqjywlrCtHuJSBs3"
        fingerprint = "e978468e162aaecd7b44aa9077fe422ef13273b2182db3f64f11c87c7c6cee79"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE TO SPECIFIED PATH - SLASHES WORK AS WELL AS DASHES (/DOWNLOADFILE, /URL, /PATH)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="MpCmdRun.exe -DownloadFile -url  -path c:\\\\temp\\\\beacon.exe" nocase

condition:
    $a0
}

rule MpCmdRun_exe1_Download
{
    meta:
        id = "lHdObnpcLA4RKxb7xzz9x"
        fingerprint = "1f4ac3f24dd8e1056d78acf51b84c81fd4dbfcf673eae446f879c78ffda228bd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE TO SPECIFIED PATH - SLASHES WORK AS WELL AS DASHES (/DOWNLOADFILE, /URL, /PATH) [UPDATED VERSION TO BYPASS WINDOWS 10 MITIGATION]"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="copy \"C:\\ProgramData\\Microsoft\\Windows Defender\\Platform\\4.18.2008.9-0\\MpCmdRun.exe\" C:\\Users\\Public\\Downloads\\MP.exe && chdir \"C:\\ProgramData\\Microsoft\\Windows Defender\\Platform\\4.18.2008.9-0\\\" && \"C:\\Users\\Public\\Downloads\\MP.exe\" -DownloadFile -url  -path C:\\Users\\Public\\Downloads\\evil.exe" nocase

condition:
    $a0
}

rule MpCmdRun_exe2_ADS
{
    meta:
        id = "7VVvO1A4baQ6eBCHswaEpC"
        fingerprint = "05a0cfa87a40cec1428a979ff36ba8d8bc0dcc860c36c7bcb8279abd95fa67a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE TO MACHINE AND STORE IT IN ALTERNATE DATA STREAM"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="MpCmdRun.exe -DownloadFile -url  -path c:\\temp\\nicefile.txt:evil.exe" nocase

condition:
    $a0
}

