rule Regsvcs_exe0_Execute
{
    meta:
        id = "540nH0EsvftUM0bHwTEJ0g"
        fingerprint = "793e8e609c34c2f49d87529f9a2c5a4b0ea31f47f764d588cd6cceae07eb380e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Regsvcs and Regasm are Windows command-line utilities that are used to register .NET Component Object Model (COM) assemblies"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE REGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regsvcs.exe AllTheThingsx64.dll" nocase

condition:
    $a0
}

rule Regsvcs_exe1_AWL_Bypass
{
    meta:
        id = "50ntP8DxVVdFNetOyXezwS"
        fingerprint = "793e8e609c34c2f49d87529f9a2c5a4b0ea31f47f764d588cd6cceae07eb380e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Regsvcs and Regasm are Windows command-line utilities that are used to register .NET Component Object Model (COM) assemblies"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE REGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regsvcs.exe AllTheThingsx64.dll" nocase

condition:
    $a0
}

