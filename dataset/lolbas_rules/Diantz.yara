rule Diantz_exe0_ADS
{
    meta:
        id = "5jScJ65xBgnNWLcaI1lf4Z"
        fingerprint = "8ac44b5328425d6eec719edbf3db9869c21bbc9880f6bf51e744427477ad6f9e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "COMPRESS TAGET FILE INTO A CAB FILE STORED IN THE ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="diantz.exe c:\\pathToFile\\file.exe c:\\destinationFolder\\targetFile.txt:targetFile.cab" nocase

condition:
    $a0
}

rule Diantz_exe1_Download
{
    meta:
        id = "NOQ2dOVdYjwbB0MDOQfL0"
        fingerprint = "1f209d8417ca91b386af84ccb44cf7a7eb1d43239a78b42b0d0757fe2f316858"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND COMPRESS A REMOTE FILE AND STORE IT IN A CAB FILE ON LOCAL MACHINE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="diantz.exe \\\\remotemachine\\pathToFile\\file.exe c:\\destinationFolder\\file.cab" nocase

condition:
    $a0
}

