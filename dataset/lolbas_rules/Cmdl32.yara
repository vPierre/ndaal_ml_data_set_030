rule cmdl32_exe0_Download
{
    meta:
        id = "HVbG5s1wLJJwJA2nRcNrc"
        fingerprint = "8c4f73b004718fa523c625909fcbc43b70478f19cdbbace86f3e0074397651e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Connection Manager Auto-Download"
        category = "TECHNIQUE"
        technique = "DOWNLOAD A FILE FROM THE WEB ADDRESS SPECIFIED IN THE CONFIGURATION FILE. THE DOWNLOADED FILE WILL BE IN %TMP% UNDER THE NAME VPNXXXX.TMP WHERE \"X\" DENOTES A RANDOM NUMBER OR LETTER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmdl32 /vpn /lan %cd%\\config" nocase

condition:
    $a0
}

