rule Appvlp_exe0_Execute
{
    meta:
        id = "62x6ssrD1MWDWfW1At3gBm"
        fingerprint = "66207022293f75f8ee508613a0c1c4dc271d5759ba6f6d5e34c46df9be3a2ba5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Application Virtualization Utility Included with Microsoft Office 2016"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.BAT THROUGH APPVLP.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AppVLP.exe \\\\webdav\\calc.bat" nocase

condition:
    $a0
}

rule Appvlp_exe1_Execute
{
    meta:
        id = "4r3mjdBY65pfJLZegK3DnW"
        fingerprint = "cf267513e9504818396dc8cd59757fef259a51fab3eb1da38e5ebd8e012a7ade"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Application Virtualization Utility Included with Microsoft Office 2016"
        category = "TECHNIQUE"
        technique = "EXECUTES POWERSHELL.EXE AS A SUBPROCESS OF APPVLP.EXE AND RUN THE RESPECTIVE PS COMMAND."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AppVLP.exe powershell.exe -c \"$e=New-Object -ComObject shell.application;$e.ShellExecute('calc.exe','', '', 'open', 1)\"" nocase

condition:
    $a0
}

rule Appvlp_exe2_Execute
{
    meta:
        id = "2QN8P5u0CigK7z3XX9VGx0"
        fingerprint = "7a6f774d51510490a3082dd15156d84764cb9793c67b9236ee1f7c0508e402ff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Application Virtualization Utility Included with Microsoft Office 2016"
        category = "TECHNIQUE"
        technique = "EXECUTES POWERSHELL.EXE AS A SUBPROCESS OF APPVLP.EXE AND RUN THE RESPECTIVE PS COMMAND."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AppVLP.exe powershell.exe -c \"$e=New-Object -ComObject excel.application;$e.RegisterXLL('\\\\webdav\\xll_poc.xll')\"" nocase

condition:
    $a0
}

