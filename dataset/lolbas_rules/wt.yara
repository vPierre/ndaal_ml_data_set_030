rule wt_exe0_Execute
{
    meta:
        id = "12Y8nbkDF4fhhf6Wb1PIVN"
        fingerprint = "4ab28a219b5955d34c1e4e6794a6d7e5388371ade8767bc330eb688d7c9ed4dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Terminal"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE VIA WINDOWS TERMINAL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wt.exe calc.exe" nocase

condition:
    $a0
}

