rule Control_exe0_ADS
{
    meta:
        id = "5Dsn2IizV2E2Jslg0cNjnG"
        fingerprint = "0858fd8cb0964ffb045b110c6962bee787ecb9d8e157de294df58737ce3d1728"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used to launch controlpanel items in Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE EVIL.DLL WHICH IS STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="control.exe c:\\windows\\tasks\\file.txt:evil.dll" nocase

condition:
    $a0
}

