rule Regsvr32_exe0_AWL_Bypass
{
    meta:
        id = "1riz65kIknDcR4cLHzpBy"
        fingerprint = "8f5fab77d48968582faa50af74c6b87e771e470e0e087e1faa06df4a79acf010"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED REMOTE .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regsvr32 /s /n /u /i: scrobj.dll" nocase

condition:
    $a0
}

rule Regsvr32_exe1_AWL_Bypass
{
    meta:
        id = "S6EjHcumXIukPYGbFnzhQ"
        fingerprint = "6c5a78d181ab0ed55853bbd130af0f2482fa47780076c165f415f31fe72b0f69"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED LOCAL .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regsvr32.exe /s /u /i:file.sct scrobj.dll" nocase

condition:
    $a0
}

rule Regsvr32_exe2_Execute
{
    meta:
        id = "59Ga1rIAiCB4mn2JZ87kAp"
        fingerprint = "8f5fab77d48968582faa50af74c6b87e771e470e0e087e1faa06df4a79acf010"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED REMOTE .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regsvr32 /s /n /u /i: scrobj.dll" nocase

condition:
    $a0
}

rule Regsvr32_exe3_Execute
{
    meta:
        id = "2Gbd3E7Qq6jZzVHMoVMvxu"
        fingerprint = "6c5a78d181ab0ed55853bbd130af0f2482fa47780076c165f415f31fe72b0f69"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED LOCAL .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regsvr32.exe /s /u /i:file.sct scrobj.dll" nocase

condition:
    $a0
}

