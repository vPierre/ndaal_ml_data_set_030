rule Manage_bde_wsf0_Execute
{
    meta:
        id = "5qy4Y7KNzRfPmAEPhcLat4"
        fingerprint = "8091fc0c1c2509d1ee8cb4bbb847a3f212990d906cd33105a78d6a38a1e3f8ab"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script for managing BitLocker"
        category = "TECHNIQUE"
        technique = "SET THE COMSPEC VARIABLE TO ANOTHER EXECUTABLE PRIOR TO CALLING MANAGE-BDE.WSF FOR EXECUTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="set comspec=c:\\windows\\system32\\calc.exe & cscript c:\\windows\\system32\\manage-bde.wsf" nocase

condition:
    $a0
}

rule Manage_bde_wsf1_Execute
{
    meta:
        id = "417eaXezkmJJU5FOeovq0D"
        fingerprint = "d042c4773251020fb53a4d508624b2333bd67ac4c46308ba180bcf1b7ed1ed7c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script for managing BitLocker"
        category = "TECHNIQUE"
        technique = "RUN THE MANAGE-BDE.WSF SCRIPT WITH A PAYLOAD NAMED MANAGE-BDE.EXE IN THE SAME DIRECTORY TO RUN THE PAYLOAD FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="copy c:\\users\\person\\evil.exe c:\\users\\public\\manage-bde.exe & cd c:\\users\\public\\ & cscript.exe c:\\windows\\system32\\manage-bde.wsf" nocase

condition:
    $a0
}

