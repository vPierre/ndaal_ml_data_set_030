rule SQLToolsPS_exe0_Execute
{
    meta:
        id = "15UJ7fzjNETkIkyysEVwPe"
        fingerprint = "13457a8c9dd99a7708522f6440f99cbafa2e2320d4fb5372be64b11b9e050a32"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Tool included with Microsoft SQL that loads SQL Server cmdlts. A replacement for sqlps.exe. Successor to sqlps.exe in SQL Server 2016+."
        category = "TECHNIQUE"
        technique = "RUN A SQL SERVER POWERSHELL MINI-CONSOLE WITHOUT MODULE AND SCRIPTBLOCK LOGGING."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="SQLToolsPS.exe -noprofile -command Start-Process calc.exe" nocase

condition:
    $a0
}

