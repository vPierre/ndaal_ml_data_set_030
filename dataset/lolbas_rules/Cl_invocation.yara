rule CL_Invocation_ps10_Execute
{
    meta:
        id = "5drUybHb4BlwKx34K21Ikw"
        fingerprint = "90d8b506083d8f0098a17c67287aef5894adb167792b587ee93974ef85e9d1a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Aero diagnostics script"
        category = "TECHNIQUE"
        technique = "IMPORT THE POWERSHELL DIAGNOSTIC CL_INVOCATION SCRIPT AND CALL SYNCINVOKE TO LAUNCH AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0=". C:\\Windows\\diagnostics\\system\\AERO\\CL_Invocation.ps1   \\nSyncInvoke <executable> [args]" nocase

condition:
    $a0
}

