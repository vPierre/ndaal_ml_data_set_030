rule Dotnet_exe0_AWL_Bypass
{
    meta:
        id = "4ZfyWwjybvjtSWlhYM7Uag"
        fingerprint = "b9bd055caeecbe20a1ba7469c36bb02ddb5562a1ff15073221b4d669e77f74ad"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WILL EXECUTE ANY DLL EVEN IF APPLOCKER IS ENABLED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dotnet.exe [PATH_TO_DLL]" nocase

condition:
    $a0
}

rule Dotnet_exe1_Execute
{
    meta:
        id = "6FPQUU7ZP2CnBk4GVDjfWx"
        fingerprint = "b9bd055caeecbe20a1ba7469c36bb02ddb5562a1ff15073221b4d669e77f74ad"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WILL EXECUTE ANY DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dotnet.exe [PATH_TO_DLL]" nocase

condition:
    $a0
}

rule Dotnet_exe2_Execute
{
    meta:
        id = "hwnT1Loo4MkifuvlWisG8"
        fingerprint = "caa9251079a0c9c736807399032edc4a7bd1a8193d2f96ad17ad7ab24173d0d7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WILL OPEN A CONSOLE WHICH ALLOWS FOR THE EXECUTION OF ARBITRARY F# COMMANDS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dotnet.exe fsi" nocase

condition:
    $a0
}

rule Dotnet_exe3_AWL_Bypass
{
    meta:
        id = "5vGbiZYJUldZX3H1bcWY4V"
        fingerprint = "f94a1fdefbf66deb411f42d94c14c1b30fd2ce4b71197762651974870c2294d1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WITH MSBUILD (SDK VERSION) WILL EXECUTE UNSIGNED CODE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dotnet.exe msbuild [Path_TO_XML_CSPROJ]" nocase

condition:
    $a0
}

