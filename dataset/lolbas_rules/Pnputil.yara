rule Pnputil_exe0_Execute
{
    meta:
        id = "1Qjd8nZaiC3VsxwZ38V44L"
        fingerprint = "37ab9cf646d375128cecc3fc19d8dc2d37050163c32014636e36baf5ccee23a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for installing drivers"
        category = "TECHNIQUE"
        technique = "USED FOR INSTALLING DRIVERS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pnputil.exe -i -a C:\\Users\\hai\\Desktop\\mo.inf" nocase

condition:
    $a0
}

