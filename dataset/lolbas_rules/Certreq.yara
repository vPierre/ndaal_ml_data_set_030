rule CertReq_exe0_Download
{
    meta:
        id = "5yqKc1NNWfat1hkYWaD5ph"
        fingerprint = "77297956e0026bb5db6d0656a5f08bf4d0de7894758a46c79a3766c7a88e28e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for requesting and managing certificates"
        category = "TECHNIQUE"
        technique = "SAVE THE RESPONSE FROM A HTTP POST TO THE ENDPOINT HTTPS://EXAMPLE.ORG/ AS OUTPUT.TXT IN THE CURRENT DIRECTORY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="CertReq -Post -config  c:\\windows\\win.ini output.txt" nocase

condition:
    $a0
}

rule CertReq_exe1_Upload
{
    meta:
        id = "6KynZ45NG5r28kfiUqn8ww"
        fingerprint = "0273909b8216ee224a4216461230a519c70b749faa13792782425eba641969fc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for requesting and managing certificates"
        category = "TECHNIQUE"
        technique = "SEND THE FILE C:\\WINDOWS\\WIN.INI TO THE ENDPOINT HTTPS://EXAMPLE.ORG/ VIA HTTP POST"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="CertReq -Post -config  c:\\windows\\win.ini and show response in terminal" nocase

condition:
    $a0
}

