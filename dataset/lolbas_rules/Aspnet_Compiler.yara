rule Aspnet_Compiler_exe0_AWL_Bypass
{
    meta:
        id = "1AMzA03heOG9e4Wpi7r43Z"
        fingerprint = "844ab3b717ec7a9b9b908ae19b984a6e736d61a6fbbc4596a8a5e09e3b674f1e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ASP.NET Compilation Tool"
        category = "TECHNIQUE"
        technique = "EXECUTE C# CODE WITH THE BUILD PROVIDER AND PROPER FOLDER STRUCTURE IN PLACE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\aspnet_compiler.exe -v none -p C:\\users\\cpl.internal\\desktop\\asptest\\ -f C:\\users\\cpl.internal\\desktop\\asptest\\none -u" nocase

condition:
    $a0
}

