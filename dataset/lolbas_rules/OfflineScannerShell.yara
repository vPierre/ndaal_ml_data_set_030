rule OfflineScannerShell_exe0_Execute
{
    meta:
        id = "2vikROzUrlko0vmFngpeM5"
        fingerprint = "f4446b1642cb054307d42155e73e1c820f958bb89a924acd6237112f631b5c08"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Defender Offline Shell"
        category = "TECHNIQUE"
        technique = "EXECUTE MPCLIENT.DLL LIBRARY IN THE CURRENT WORKING DIRECTORY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="OfflineScannerShell" nocase

condition:
    $a0
}

