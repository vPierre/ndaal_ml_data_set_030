rule Ieadvpack_dll0_AWL_Bypass
{
    meta:
        id = "7b2dm7TUH36yDoE7pbFsXo"
        fingerprint = "8eec1053a97056411db4b2af84b7f7d4ade27be24f957ef39838c1b04054f8e0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe ieadvpack.dll,LaunchINFSection c:\\test.inf,DefaultInstall_SingleUser,1," nocase

condition:
    $a0
}

rule Ieadvpack_dll1_AWL_Bypass
{
    meta:
        id = "4U0mEuQBPuriioY3n4iCiJ"
        fingerprint = "f3267b604b52fb4d4bad86e7496e901e10b5d87aca51076aa06bedd1cb12bcb2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (DEFAULTINSTALL SECTION IMPLIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe ieadvpack.dll,LaunchINFSection c:\\test.inf,,1," nocase

condition:
    $a0
}

rule Ieadvpack_dll2_Execute
{
    meta:
        id = "4xlRz1bmsegfGmHUEBXfRo"
        fingerprint = "618043224a402a2286165def3f8f30b5fe50dedbd8f7ea17bf86ab763ea0dcc4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "LAUNCH A DLL PAYLOAD BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe ieadvpack.dll,RegisterOCX test.dll" nocase

condition:
    $a0
}

rule Ieadvpack_dll3_Execute
{
    meta:
        id = "4X5YM5jfVErF4A62VEZPE6"
        fingerprint = "cd4f3c09ea8867ed64ccd461e4f02a8a03ea17c1a02044ebf3d4b1d1e03c9c11"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe ieadvpack.dll,RegisterOCX calc.exe" nocase

condition:
    $a0
}

rule Ieadvpack_dll4_Execute
{
    meta:
        id = "4M290JUBLIffsjLspZymQj"
        fingerprint = "836136ab0e89181a1e9b53e77ed35f8af4b8cd31d079e42339f99d033878927e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "LAUNCH COMMAND LINE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32 ieadvpack.dll, RegisterOCX \"cmd.exe /c calc.exe\"" nocase

condition:
    $a0
}

