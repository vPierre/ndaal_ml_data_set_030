rule Runscripthelper_exe0_Execute
{
    meta:
        id = "2m3PULTMLjQY0LHy0FpKCn"
        fingerprint = "c6c24159359a5265c412ba870ed8146389702771f2661338b30adb2c57ee83e7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute target PowerShell script"
        category = "TECHNIQUE"
        technique = "EXECUTE THE POWERSHELL SCRIPT NAMED TEST.TXT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="runscripthelper.exe surfacecheck \\\\?\\C:\\Test\\Microsoft\\Diagnosis\\scripts\\test.txt C:\\Test" nocase

condition:
    $a0
}

