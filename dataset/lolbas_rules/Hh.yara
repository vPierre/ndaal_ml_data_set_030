rule Hh_exe0_Download
{
    meta:
        id = "2efMF6a9TgCKhUDMDs2Plk"
        fingerprint = "d6ad131a3e944fb3404695e4dc93f1e63fd626995b9a845a404b3c671a798736"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for processing chm files in Windows"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET POWERSHELL SCRIPT WITH HTML HELP."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="HH.exe " nocase

condition:
    $a0
}

rule Hh_exe1_Execute
{
    meta:
        id = "136nnQsSu3ssVpK2SoRFYG"
        fingerprint = "2862f2f6ae0584ddedb5e677d5596631da9fcebd96fa8bfb14a475a2cfc9897f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for processing chm files in Windows"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE WITH HTML HELP."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="HH.exe c:\\windows\\system32\\calc.exe" nocase

condition:
    $a0
}

