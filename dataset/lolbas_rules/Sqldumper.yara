rule Sqldumper_exe0_Dump
{
    meta:
        id = "59FWGldcnydHITsHkLorSQ"
        fingerprint = "848dc766d2b5f0c0667dc4eefc1b5a37493dc51ca5d4300284327e34e93b2040"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging utility included with Microsoft SQL."
        category = "TECHNIQUE"
        technique = "DUMP PROCESS BY PID AND CREATE A DUMP FILE (APPEARS TO CREATE A DUMP FILE CALLED SQLDMPRXXXX.MDMP)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="sqldumper.exe 464 0 0x0110" nocase

condition:
    $a0
}

rule Sqldumper_exe1_Dump
{
    meta:
        id = "4Fb1n6XDKSzpkuYvUmR6lb"
        fingerprint = "8d4411d5fd906d6fd634823a3f95778e7c928fff4e7818d40864bd0cc20b6b11"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging utility included with Microsoft SQL."
        category = "TECHNIQUE"
        technique = "0X01100:40 FLAG WILL CREATE A MIMIKATZ COMPATIBLE DUMP FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="sqldumper.exe 540 0 0x01100:40" nocase

condition:
    $a0
}

