rule msxsl_exe0_Execute
{
    meta:
        id = "35mEjcjlHrwTCMLU14LCEz"
        fingerprint = "0e10e236af049f40d49dad7576b35de5b8e11f5806e1759ec72c5ea9ffbcd538"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SCRIPT.XSL FILE (LOCAL)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msxsl.exe customers.xml script.xsl" nocase

condition:
    $a0
}

rule msxsl_exe1_AWL_Bypass
{
    meta:
        id = "3hLADSRB1lk1jLcyySXUrS"
        fingerprint = "0e10e236af049f40d49dad7576b35de5b8e11f5806e1759ec72c5ea9ffbcd538"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SCRIPT.XSL FILE (LOCAL)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msxsl.exe customers.xml script.xsl" nocase

condition:
    $a0
}

rule msxsl_exe2_Execute
{
    meta:
        id = "1rIBUQGLUM8DCa80Jibh2e"
        fingerprint = "c9f3f50a6453f5a93c647a94109d2558afdfdfa8985ecbd48da1c947cb6cfe0c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SHELLCODE.XML(XSL) FILE (REMOTE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msxsl.exe  " nocase

condition:
    $a0
}

rule msxsl_exe3_AWL_Bypass
{
    meta:
        id = "5bzSvKsRpIDG9qxdJZUQFT"
        fingerprint = "c9f3f50a6453f5a93c647a94109d2558afdfdfa8985ecbd48da1c947cb6cfe0c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SHELLCODE.XML(XSL) FILE (REMOTE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msxsl.exe  " nocase

condition:
    $a0
}

