rule Schtasks_exe0_Execute
{
    meta:
        id = "3ui8XrILrlHEX5hmWvL3lY"
        fingerprint = "7800209fc3820a22a914eee68e744af9536615c7cbd8b4f970b247c21a9fdac3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Schedule periodic tasks"
        category = "TECHNIQUE"
        technique = "CREATE A RECURRING TASK TO EXECUTE EVERY MINUTE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="schtasks /create /sc minute /mo 1 /tn \"Reverse shell\" /tr c:\\some\\directory\\revshell.exe" nocase

condition:
    $a0
}

rule Schtasks_exe1_Execute
{
    meta:
        id = "5xRGIHXQZrAcPMJ6SI044A"
        fingerprint = "4b5035e795324b8f352d105a6f74ce0702865f551cda868a88affed2cf2f572b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Schedule periodic tasks"
        category = "TECHNIQUE"
        technique = "CREATE A SCHEDULED TASK ON A REMOTE COMPUTER FOR PERSISTENCE/LATERAL MOVEMENT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="schtasks /create /s targetmachine /tn \"MyTask\" /tr c:\\some\\directory\\notevil.exe /sc daily" nocase

condition:
    $a0
}

