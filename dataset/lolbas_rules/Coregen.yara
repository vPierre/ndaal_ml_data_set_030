rule coregen_exe0_Execute
{
    meta:
        id = "7fy3JvwU42ulXJZmIxPGXI"
        fingerprint = "a59adf06030b0a373aeca6cc04e1e5be3b6ab082d158aad546a2424182f10b17"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary coregen.exe (Microsoft CoreCLR Native Image Generator) loads exported function GetCLRRuntimeHost from coreclr.dll or from .DLL in arbitrary path. Coregen is located within \"C:\\Program Files (x86)\\Microsoft Silverlight\\5.1.50918.0\\\" or another version of Silverlight. Coregen is signed by Microsoft and bundled with Microsoft Silverlight."
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL IN ARBITRARY PATH SPECIFIED WITH /L."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="coregen.exe /L C:\\folder\\evil.dll dummy_assembly_name" nocase

condition:
    $a0
}

rule coregen_exe1_Execute
{
    meta:
        id = "5agmUpHLgtFt1Up7xK9gNZ"
        fingerprint = "55ad45338e50a61d6e886c77f896ef053aa31a676e8879d68202dd52a51423b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary coregen.exe (Microsoft CoreCLR Native Image Generator) loads exported function GetCLRRuntimeHost from coreclr.dll or from .DLL in arbitrary path. Coregen is located within \"C:\\Program Files (x86)\\Microsoft Silverlight\\5.1.50918.0\\\" or another version of Silverlight. Coregen is signed by Microsoft and bundled with Microsoft Silverlight."
        category = "TECHNIQUE"
        technique = "LOADS THE CORECLR.DLL IN THE CORGEN.EXE DIRECTORY (E.G. C:\\PROGRAM FILES\\MICROSOFT SILVERLIGHT\\5.1.50918.0)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="coregen.exe dummy_assembly_name" nocase

condition:
    $a0
}

rule coregen_exe2_AWL_Bypass
{
    meta:
        id = "2aLiGZ0b7KDn0zXL2Poofn"
        fingerprint = "a59adf06030b0a373aeca6cc04e1e5be3b6ab082d158aad546a2424182f10b17"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary coregen.exe (Microsoft CoreCLR Native Image Generator) loads exported function GetCLRRuntimeHost from coreclr.dll or from .DLL in arbitrary path. Coregen is located within \"C:\\Program Files (x86)\\Microsoft Silverlight\\5.1.50918.0\\\" or another version of Silverlight. Coregen is signed by Microsoft and bundled with Microsoft Silverlight."
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL IN ARBITRARY PATH SPECIFIED WITH /L. SINCE BINARY IS SIGNED IT CAN ALSO BE USED TO BYPASS APPLICATION WHITELISTING SOLUTIONS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="coregen.exe /L C:\\folder\\evil.dll dummy_assembly_name" nocase

condition:
    $a0
}

