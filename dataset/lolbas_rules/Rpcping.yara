rule Rpcping_exe0_Credentials
{
    meta:
        id = "6ahFOs0ihsqgqw1DIAL91V"
        fingerprint = "34218a11b4be26de187d40d1793551fde2ec199aace136e601ae16a808f329ff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to verify rpc connection"
        category = "TECHNIQUE"
        technique = "SEND A RPC TEST CONNECTION TO THE TARGET SERVER (-S) AND FORCE THE NTLM HASH TO BE SENT IN THE PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rpcping -s 127.0.0.1 -e 1234 -a privacy -u NTLM" nocase

condition:
    $a0
}

rule Rpcping_exe1_Credentials
{
    meta:
        id = "4DEnkCvYnF6GYxe6yn6SnO"
        fingerprint = "f8f20d72092c7a310474ba09806325b0528a3141aa6df8ccaaff669bdbc4d6dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to verify rpc connection"
        category = "TECHNIQUE"
        technique = "TRIGGER AN AUTHENTICATED RPC CALL TO THE TARGET SERVER (/S) THAT COULD BE RELAYED TO A PRIVILEGED RESOURCE (SIGN NOT SET)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rpcping /s 10.0.0.35 /e 9997 /a connect /u NTLM" nocase

condition:
    $a0
}

