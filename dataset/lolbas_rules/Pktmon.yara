rule Pktmon_exe0_Reconnaissance
{
    meta:
        id = "2tx3jXiaE5W5FFeAGhl4ms"
        fingerprint = "725a354860dff76f1ed7dc6da0168b7338725e01af83c15191f22b877613d3c2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Capture Network Packets on the windows 10 with October 2018 Update or later."
        category = "TECHNIQUE"
        technique = "WILL START A PACKET CAPTURE AND STORE LOG FILE AS PKTMON.ETL. USE PKTMON.EXE STOP"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pktmon.exe start --etw" nocase

condition:
    $a0
}

rule Pktmon_exe1_Reconnaissance
{
    meta:
        id = "6somZQDtKuOuUE02fVs3ub"
        fingerprint = "5c5b92c7e933d3d2c78ec2c3d73d64f87cf7f5ea7a45e1f074d49c04d8fae21d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Capture Network Packets on the windows 10 with October 2018 Update or later."
        category = "TECHNIQUE"
        technique = "SELECT DESIRED PORTS FOR PACKET CAPTURE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pktmon.exe filter add -p 445" nocase

condition:
    $a0
}

