rule Infdefaultinstall_exe0_Execute
{
    meta:
        id = "2csW9nXG1sOWD5JerU1UO2"
        fingerprint = "c6415429625023ab5d0cf3de502278f7ec3fe43d89dbcdb7ffd565b0dcaa3daf"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used to perform installation based on content inside inf files"
        category = "TECHNIQUE"
        technique = "EXECUTES SCT SCRIPT USING SCROBJ.DLL FROM A COMMAND IN ENTERED INTO A SPECIALLY PREPARED INF FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="InfDefaultInstall.exe Infdefaultinstall.inf" nocase

condition:
    $a0
}

