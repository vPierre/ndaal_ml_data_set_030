rule Certutil_exe0_Download
{
    meta:
        id = "2hPhBGfsqEH1z6Uw0YAlzT"
        fingerprint = "2770b0818185aa2c6dc98ed44032ef209fd9e53891871b9311e326a763df623d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND SAVE 7ZIP TO DISK IN THE CURRENT FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certutil.exe -urlcache -split -f  7zip.exe" nocase

condition:
    $a0
}

rule Certutil_exe1_Download
{
    meta:
        id = "C89lsr2qxJBY79SqF7rVZ"
        fingerprint = "f6cb06a96b6679f72b273c7b6fd6ab986ba998a464c3306cf5d026581643d559"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND SAVE 7ZIP TO DISK IN THE CURRENT FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certutil.exe -verifyctl -f -split  7zip.exe" nocase

condition:
    $a0
}

rule Certutil_exe2_ADS
{
    meta:
        id = "6kZNQUSkaIJHtAFx5UPp19"
        fingerprint = "2d8066dfe0e723c7b4a4e2d69de3f586872a0478b7a2d9876a95046311c1afab"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND SAVE A PS1 FILE TO AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certutil.exe -urlcache -split -f  c:\\temp:ttt" nocase

condition:
    $a0
}

rule Certutil_exe3_Encode
{
    meta:
        id = "5qpbCGGXmADVNzzPplx5f3"
        fingerprint = "7aa4d93254ada9ce11adba1e754954511aa092660f275d807bafc099601af321"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "COMMAND TO ENCODE A FILE USING BASE64"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certutil -encode inputFileName encodedOutputFileName" nocase

condition:
    $a0
}

rule Certutil_exe4_Decode
{
    meta:
        id = "5LyCgc5cOqyt0gP2hP49Yg"
        fingerprint = "5fdbebebbfff17decc4ec4ce54ae99b1f1daa3b51d2f03121c4720ffbd842c27"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "COMMAND TO DECODE A BASE64 ENCODED FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certutil -decode encodedInputFileName decodedOutputFileName" nocase

condition:
    $a0
}

rule Certutil_exe5_Decode
{
    meta:
        id = "5tIkHRwTse2StRWzCvKamZ"
        fingerprint = "42ad22b35310fdf307f5402f9f64dedfb66dc47d2cb33fe1e9126309328a0cf0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "COMMAND TO DECODE A HEXADECIMAL-ENCODED FILE DECODEDOUTPUTFILENAME"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certutil -decodehex encoded_hexadecimal_InputFileName decodedOutputFileName" nocase

condition:
    $a0
}

