rule Verclsid_exe0_Execute
{
    meta:
        id = "77NBwP7NxnagYPAsOwXWi4"
        fingerprint = "f7dd88c454ee812f56c59ba8a93fef113ff5ad27e25177b181582ca4eba65de9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to verify a COM object before it is instantiated by Windows Explorer"
        category = "TECHNIQUE"
        technique = "USED TO VERIFY A COM OBJECT BEFORE IT IS INSTANTIATED BY WINDOWS EXPLORER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="verclsid.exe /S /C {CLSID}" nocase

condition:
    $a0
}

