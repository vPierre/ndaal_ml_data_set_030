rule Microsoft_Workflow_Compiler_exe0_Execute
{
    meta:
        id = "7ViykLdGsw52mCRpO4px5U"
        fingerprint = "b58f8859f1888e3f05cff06ed7b0a05760fc914389fb76647b4e732074801e7a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A utility included with .NET that is capable of compiling and executing C# or VB.net code."
        category = "TECHNIQUE"
        technique = "COMPILE AND EXECUTE C# OR VB.NET CODE IN A XOML FILE REFERENCED IN THE TEST.XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Microsoft.Workflow.Compiler.exe tests.xml results.xml" nocase

condition:
    $a0
}

rule Microsoft_Workflow_Compiler_exe1_Execute
{
    meta:
        id = "mbR0N2m2FvVTMGNs1cLLA"
        fingerprint = "8a9f8952c1d5bfaed5e14695b6bcc50a4275ceed4a31efeeb9a8eb22385c212f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A utility included with .NET that is capable of compiling and executing C# or VB.net code."
        category = "TECHNIQUE"
        technique = "COMPILE AND EXECUTE C# OR VB.NET CODE IN A XOML FILE REFERENCED IN THE TEST.TXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Microsoft.Workflow.Compiler.exe tests.txt results.txt" nocase

condition:
    $a0
}

rule Microsoft_Workflow_Compiler_exe2_AWL_Bypass
{
    meta:
        id = "7X27D27S5pbk6bgXEDpIpG"
        fingerprint = "8a9f8952c1d5bfaed5e14695b6bcc50a4275ceed4a31efeeb9a8eb22385c212f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A utility included with .NET that is capable of compiling and executing C# or VB.net code."
        category = "TECHNIQUE"
        technique = "COMPILE AND EXECUTE C# OR VB.NET CODE IN A XOML FILE REFERENCED IN THE TEST.TXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Microsoft.Workflow.Compiler.exe tests.txt results.txt" nocase

condition:
    $a0
}

