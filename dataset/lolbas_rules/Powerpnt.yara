rule Powerpnt_exe0_Download
{
    meta:
        id = "1ZOYIFOfR3GcpScfA5OhB2"
        fingerprint = "471fdecfe0824f62b49606e87db0d80fd8807d85b491714c035f945d28337d0f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary."
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Powerpnt.exe \"" nocase

condition:
    $a0
}

