rule Advpack_dll0_AWL_Bypass
{
    meta:
        id = "4Am9G7YtWkl6SFctREJLiI"
        fingerprint = "7ee49baefc106d0e46875d972a1e14568ca692c75acdf7a383aae169fc22f775"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe advpack.dll,LaunchINFSection c:\\test.inf,DefaultInstall_SingleUser,1," nocase

condition:
    $a0
}

rule Advpack_dll1_AWL_Bypass
{
    meta:
        id = "3K4MdeZshPiZlzp7kp0zGy"
        fingerprint = "f3b8f3c7e13a269bf8c137787a0c47fe4e820c5fba962c911d07e1c34a17827c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (DEFAULTINSTALL SECTION IMPLIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe advpack.dll,LaunchINFSection c:\\test.inf,,1," nocase

condition:
    $a0
}

rule Advpack_dll2_Execute
{
    meta:
        id = "16JwLJygY5cb3mhpvhsqi5"
        fingerprint = "04a95b2df873957fe82e7a10311758528adb05fb4109c9d77a4baa76ffbe373f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "LAUNCH A DLL PAYLOAD BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe advpack.dll,RegisterOCX test.dll" nocase

condition:
    $a0
}

rule Advpack_dll3_Execute
{
    meta:
        id = "7EIApSDldYpvFFFXJKJk33"
        fingerprint = "45f20e5aaed4957788b28ae233caf0d7fb516ec9f07df62195d2f2fe74ad50b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe advpack.dll,RegisterOCX calc.exe" nocase

condition:
    $a0
}

rule Advpack_dll4_Execute
{
    meta:
        id = "671CsallnHW1vmuMWu9Ado"
        fingerprint = "9e2745e380284a254d6b42ba78e18c48a53dc11fac795a0358fe475748dc9ada"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "LAUNCH COMMAND LINE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32 advpack.dll, RegisterOCX \"cmd.exe /c calc.exe\"" nocase

condition:
    $a0
}

