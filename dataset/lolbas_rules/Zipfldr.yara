rule Zipfldr_dll0_Execute
{
    meta:
        id = "3ClOHN2AbbS7kMuB182KWt"
        fingerprint = "9c5c8ff519c951aedcbb04e399d0aef4d9f4aa395b9641d8c7d3d57325fc37fa"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Compressed Folder library"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD BY CALLING ROUTETHECALL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe zipfldr.dll,RouteTheCall calc.exe" nocase

condition:
    $a0
}

rule Zipfldr_dll1_Execute
{
    meta:
        id = "6quOKra91Ig0CotvpbxXfX"
        fingerprint = "ac31219ed581b03f7ea1b72406166a1d29a3ec85eb0e11953721098f2202e3b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Compressed Folder library"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD BY CALLING ROUTETHECALL (OBFUSCATED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe zipfldr.dll,RouteTheCall file://^C^:^/^W^i^n^d^o^w^s^/^s^y^s^t^e^m^3^2^/^c^a^l^c^.^e^x^e" nocase

condition:
    $a0
}

