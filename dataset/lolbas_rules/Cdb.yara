rule Cdb_exe0_Execute
{
    meta:
        id = "76abt1DSmW9mlklv9GNA9d"
        fingerprint = "cb6c212d37a5b184c13b04a28130b7e9ff18f12e0e389b73001611cffc228c80"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools."
        category = "TECHNIQUE"
        technique = "LAUNCH 64-BIT SHELLCODE FROM THE X64_CALC.WDS FILE USING CDB.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cdb.exe -cf x64_calc.wds -o notepad.exe" nocase

condition:
    $a0
}

rule Cdb_exe1_Execute
{
    meta:
        id = "57YRgIajVSCghCaX8AqwHg"
        fingerprint = "a921767a8fa905eab01b8480ed3509925fb0d9f1d069b98bb33e65d0cec1cac7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools."
        category = "TECHNIQUE"
        technique = "ATTACHING TO ANY PROCESS AND EXECUTING SHELL COMMANDS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cdb.exe -pd -pn " nocase

condition:
    $a0
}

rule Cdb_exe2_Execute
{
    meta:
        id = "1pTHNJIgB3IE4IkbS9Dff1"
        fingerprint = "43ea26662b4e32bbeae8fd50638fc9dab845169fd27aabe375db8c67834e2dbd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools."
        category = "TECHNIQUE"
        technique = "EXECUTE ARBITRARY COMMANDS AND BINARIES USING A DEBUGGING SCRIPT (SEE RESOURCES SECTION FOR A SAMPLE FILE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cdb.exe -c C:\\debug-script.txt calc" nocase

condition:
    $a0
}

