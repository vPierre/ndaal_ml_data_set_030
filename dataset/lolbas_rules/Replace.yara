rule Replace_exe0_Copy
{
    meta:
        id = "2MWoWXzEL7kdLhuNTfCNdE"
        fingerprint = "e09ce9f15fb2b3a1332f5ad0d9aab59eccb1b66527514d609c9292b19ed9b315"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to replace file with another file"
        category = "TECHNIQUE"
        technique = "COPY FILE.CAB TO DESTINATION"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="replace.exe C:\\Source\\File.cab C:\\Destination /A" nocase

condition:
    $a0
}

rule Replace_exe1_Download
{
    meta:
        id = "54FETYjfHFG01h7SxVo9cY"
        fingerprint = "a54395e3be661dec6aa931e662e603f4ba9b13bd3e3b42016b2c7dd66431ad0a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to replace file with another file"
        category = "TECHNIQUE"
        technique = "DOWNLOAD/COPY BAR.EXE TO OUTDIR"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="replace.exe \\\\webdav.host.com\\foo\\bar.exe c:\\outdir /A" nocase

condition:
    $a0
}

