rule Dfsvc_exe0_AWL_Bypass
{
    meta:
        id = "7ShMWO3naTQtid5ylRVVbJ"
        fingerprint = "a8be15b7e6853abef7fde7b03d4cfab948437e4e08e0cfb67ad3c9df9a540fee"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ClickOnce engine in Windows used by .NET"
        category = "TECHNIQUE"
        technique = "EXECUTES CLICK-ONCE-APPLICATION FROM URL (TRAMPOLINE FOR DFSVC.EXE, DOTNET CLICKONCE HOST)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe dfshim.dll,ShOpenVerbApplication " nocase

condition:
    $a0
}

