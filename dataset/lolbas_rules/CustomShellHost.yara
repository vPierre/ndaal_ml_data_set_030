rule CustomShellHost_exe0_Execute
{
    meta:
        id = "19nYX8QHRHc9Oy5a0P2Rcg"
        fingerprint = "ff9b7defbd7361f51995c88961c280f77aa14128cce13397159b100fbc0db9cf"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A host process that is used by custom shells when using Windows in Kiosk mode."
        category = "TECHNIQUE"
        technique = "EXECUTES EXPLORER.EXE (WITH COMMAND-LINE ARGUMENT /NOSHELLREGISTRATIONCHECK) IF PRESENT IN THE CURRENT WORKING FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="CustomShellHost.exe" nocase

condition:
    $a0
}

