rule Desk_cpl0_Execute
{
    meta:
        id = "2kkVcLHyjkSa7EJvgZCp61"
        fingerprint = "8d8a3dcc3e59ee87e49559daba11e2cf37d6b6a46b0c8a744ee1357773870745"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Desktop Settings Control Panel"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE WITH A .SCR EXTENSION BY CALLING THE INSTALLSCREENSAVER FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe desk.cpl,InstallScreenSaver C:\\temp\\file.scr" nocase

condition:
    $a0
}

rule Desk_cpl1_Execute
{
    meta:
        id = "5qdgOUeSM3D9a9lqEyUYOq"
        fingerprint = "ef16f9bd04a93993768863474bb5c52c9819a84e5a29243ee6de88b2ff121130"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Desktop Settings Control Panel"
        category = "TECHNIQUE"
        technique = "LAUNCH A REMOTE EXECUTABLE WITH A .SCR EXTENSION, LOCATED ON AN SMB SHARE, BY CALLING THE INSTALLSCREENSAVER FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe desk.cpl,InstallScreenSaver \\\\127.0.0.1\\c$\\temp\\file.scr" nocase

condition:
    $a0
}

