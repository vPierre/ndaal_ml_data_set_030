rule Cmstp_exe0_Execute
{
    meta:
        id = "4uYzXYRi4NY8rufjnQpLrZ"
        fingerprint = "9ac1fc1602b52f9b2c8fb7e997a716654b7fec74fbb48091367680bd6442d246"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Installs or removes a Connection Manager service profile."
        category = "TECHNIQUE"
        technique = "SILENTLY INSTALLS A SPECIALLY FORMATTED LOCAL .INF WITHOUT CREATING A DESKTOP ICON. THE .INF FILE CONTAINS A UNREGISTEROCXSECTION SECTION WHICH EXECUTES A .SCT FILE USING SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmstp.exe /ni /s c:\\cmstp\\CorpVPN.inf" nocase

condition:
    $a0
}

rule Cmstp_exe1_AWL_Bypass
{
    meta:
        id = "7KEqwtW1OtvhFjJJ2NIEwe"
        fingerprint = "36a6c0803530c45caa4752a25896441da491b19cc9b99214eb5e8eba08b6ab90"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Installs or removes a Connection Manager service profile."
        category = "TECHNIQUE"
        technique = "SILENTLY INSTALLS A SPECIALLY FORMATTED REMOTE .INF WITHOUT CREATING A DESKTOP ICON. THE .INF FILE CONTAINS A UNREGISTEROCXSECTION SECTION WHICH EXECUTES A .SCT FILE USING SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmstp.exe /ni /s " nocase

condition:
    $a0
}

