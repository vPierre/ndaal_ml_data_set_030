rule Update_exe0_Download
{
    meta:
        id = "1cY8vuzRfhE3csz4z4lqQn"
        fingerprint = "1e659e25892fe7f5489ff71d9313dd7be5cd188113d3ced50bf3280cfa7df88a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE AND DOWNLOAD THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --download [url to package]" nocase

condition:
    $a0
}

rule Update_exe1_AWL_Bypass
{
    meta:
        id = "6DXFEibA1aHZ3rLxnYQnhE"
        fingerprint = "ff12383898343cb0c4201cecde3fbf16d25e3d4d5b457e961ab5e2032f3f5d5e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --update=[url to package]" nocase

condition:
    $a0
}

rule Update_exe2_Execute
{
    meta:
        id = "6XZw6NgSvcG1fzL0COq8HF"
        fingerprint = "ff12383898343cb0c4201cecde3fbf16d25e3d4d5b457e961ab5e2032f3f5d5e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --update=[url to package]" nocase

condition:
    $a0
}

rule Update_exe3_AWL_Bypass
{
    meta:
        id = "3ghlbmW979Gl7VpsICU7JQ"
        fingerprint = "be3178c75c3b280d1ebd6f28b017ec275f3dd3ac85a252ad273c422c2ea070c4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --update=\\\\remoteserver\\payloadFolder" nocase

condition:
    $a0
}

rule Update_exe4_Execute
{
    meta:
        id = "d03g1hj73l2FjEJoz9i3Z"
        fingerprint = "be3178c75c3b280d1ebd6f28b017ec275f3dd3ac85a252ad273c422c2ea070c4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --update=\\\\remoteserver\\payloadFolder" nocase

condition:
    $a0
}

rule Update_exe5_AWL_Bypass
{
    meta:
        id = "1v2mzQnBeONX5Qw0hDXeRC"
        fingerprint = "a1f2ace1be2c485ce0372af780af344f6bb0020a65436b7db17f269985482547"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --updateRollback=[url to package]" nocase

condition:
    $a0
}

rule Update_exe6_Execute
{
    meta:
        id = "2ObByOtCoYuUUhgvmXjbqA"
        fingerprint = "a1f2ace1be2c485ce0372af780af344f6bb0020a65436b7db17f269985482547"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --updateRollback=[url to package]" nocase

condition:
    $a0
}

rule Update_exe7_AWL_Bypass
{
    meta:
        id = "5I9Bk2My1HNUo75WARqczZ"
        fingerprint = "067debe6f99dd7ce0e3548db7acd0a7538ad6327118b44dc3470ddc23c20c912"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "COPY YOUR PAYLOAD INTO %USERPROFILE%\\APPDATA\\LOCAL\\MICROSOFT\\TEAMS\\CURRENT\\. THEN RUN THE COMMAND. UPDATE.EXE WILL EXECUTE THE FILE YOU COPIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --processStart payload.exe --process-start-args \"whatever args\"" nocase

condition:
    $a0
}

rule Update_exe8_AWL_Bypass
{
    meta:
        id = "23bH6t1krSYYvebToiVf8I"
        fingerprint = "de7d77c4be9dfa6bbf836d26baf36c24d5a794749a0000c73673f680a267ed4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --updateRollback=\\\\remoteserver\\payloadFolder" nocase

condition:
    $a0
}

rule Update_exe9_Execute
{
    meta:
        id = "32BljrfhvVK87UL3Ypt8ZS"
        fingerprint = "de7d77c4be9dfa6bbf836d26baf36c24d5a794749a0000c73673f680a267ed4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --updateRollback=\\\\remoteserver\\payloadFolder" nocase

condition:
    $a0
}

rule Update_exe10_Execute
{
    meta:
        id = "2hQQCmHOIim3ffoMVk0lMz"
        fingerprint = "067debe6f99dd7ce0e3548db7acd0a7538ad6327118b44dc3470ddc23c20c912"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "COPY YOUR PAYLOAD INTO %USERPROFILE%\\APPDATA\\LOCAL\\MICROSOFT\\TEAMS\\CURRENT\\. THEN RUN THE COMMAND. UPDATE.EXE WILL EXECUTE THE FILE YOU COPIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --processStart payload.exe --process-start-args \"whatever args\"" nocase

condition:
    $a0
}

rule Update_exe11_Execute
{
    meta:
        id = "4CAf3iYQOrJmDwGl1GnyPx"
        fingerprint = "2022ac375d970b78235197cc285c5a4acc3a1f3c29f26cec0b4c9efbbcabd20c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "COPY YOUR PAYLOAD INTO \"%LOCALAPPDATA%\\MICROSOFT\\TEAMS\\CURRENT\\\". THEN RUN THE COMMAND. UPDATE.EXE WILL CREATE A PAYLOAD.EXE SHORTCUT IN \"%APPDATA%\\MICROSOFT\\WINDOWS\\START MENU\\PROGRAMS\\STARTUP\". THEN PAYLOAD WILL RUN ON EVERY LOGIN OF THE USER WHO RUNS IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --createShortcut=payload.exe -l=Startup" nocase

condition:
    $a0
}

rule Update_exe12_Execute
{
    meta:
        id = "7SEnbBkFrF1ocrPy02nFuq"
        fingerprint = "908d95e61244c399077237749b470966bd6ba0344f3ff37b26392859af4d2cd0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "RUN THE COMMAND TO REMOVE THE SHORTCUT CREATED IN THE \"%APPDATA%\\MICROSOFT\\WINDOWS\\START MENU\\PROGRAMS\\STARTUP\" DIRECTORY YOU CREATED WITH THE LOLBINEXECUTION \"--CREATESHORTCUT\" DESCRIBED ON THIS PAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Update.exe --removeShortcut=payload.exe -l=Startup" nocase

condition:
    $a0
}

