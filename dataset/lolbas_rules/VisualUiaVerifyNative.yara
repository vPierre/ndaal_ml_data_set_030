rule VisualUiaVerifyNative_exe0_AWL_Bypass
{
    meta:
        id = "1rjjBPbPeQN22UyfwWei3t"
        fingerprint = "209632dc6b5ea963c7e79089cfb2b5b71237f5f101359d58a07e557abaf2f1a9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A Windows SDK binary for manual and automated testing of Microsoft UI Automation implementation and controls."
        category = "TECHNIQUE"
        technique = "GENERATE SERIALIZED GADGET AND SAVE TO - C:\\USERS\\[CURRENT USER]\\APPDATA\\ROAMINGUIVERIFY.CONFIG BEFORE EXECUTING."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="VisualUiaVerifyNative.exe" nocase

condition:
    $a0
}

