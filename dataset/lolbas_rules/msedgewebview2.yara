rule msedgewebview2_exe0_Execute
{
    meta:
        id = "3SM3IVlemvkeOac5a3GtJb"
        fingerprint = "2d63e7e3d090311b72be27076ef476b65a9eab98a95cd66854631f76f034ccda"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedgewebview2.exe --no-sandbox --browser-subprocess-path=\"C:\\Windows\\System32\\calc.exe\"" nocase

condition:
    $a0
}

rule msedgewebview2_exe1_Execute
{
    meta:
        id = "LAgjtx6LLdzMztMcKu79m"
        fingerprint = "aa673c85313a1386b9c719a4c48497aa514bf0ecfb4d0253d7bb84931ede0d71"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedgewebview2.exe --utility-cmd-prefix=\"calc.exe\"" nocase

condition:
    $a0
}

rule msedgewebview2_exe2_Execute
{
    meta:
        id = "2b26I1GVZf9so7eUrLLG8z"
        fingerprint = "f4fd7c87011b2c4414f8eedbe4fb778c733bd3b0debf71c64aec470e644bf047"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedgewebview2.exe --disable-gpu-sandbox --gpu-launcher=\"calc.exe\"" nocase

condition:
    $a0
}

rule msedgewebview2_exe3_Execute
{
    meta:
        id = "3kBzma4VdNYu1q5dw1sU8I"
        fingerprint = "d8621a49395fa1a5103a4da6394178d35ea47363c6bbf8547bee2be27a7f0c28"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msedgewebview2.exe --no-sandbox --renderer-cmd-prefix=\"calc.exe\"" nocase

condition:
    $a0
}

