rule Explorer_exe0_Execute
{
    meta:
        id = "10xXTEDPJecnAJGdGjToAA"
        fingerprint = "871e8ff6d5cbb800ddb012f1717ab9f686b2b627642fd9f0428f8e8fca69a925"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for managing files and system components within Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH THE PARENT PROCESS SPAWNING FROM A NEW INSTANCE OF EXPLORER.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="explorer.exe /root,\"C:\\Windows\\System32\\calc.exe\"" nocase

condition:
    $a0
}

rule Explorer_exe1_Execute
{
    meta:
        id = "1cCwOwu0QctxAGMJ6hzUh8"
        fingerprint = "9f7ed5922f2b524835a4be16ddd984b2ae1f72738ab900d1aa56838ea6a659de"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for managing files and system components within Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE NOTEPAD.EXE WITH THE PARENT PROCESS SPAWNING FROM A NEW INSTANCE OF EXPLORER.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="explorer.exe C:\\Windows\\System32\\notepad.exe" nocase

condition:
    $a0
}

