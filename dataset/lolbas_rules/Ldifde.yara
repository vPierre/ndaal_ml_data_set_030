rule Ldifde_exe0_Download
{
    meta:
        id = "4hBlGyycNkBdCkZnI2Ed07"
        fingerprint = "0c39489a9162a30dc330877d2cbcde1a9edb2635c440d74e97a4104a6d717163"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Creates, modifies, and deletes LDAP directory objects."
        category = "TECHNIQUE"
        technique = "IMPORT INPUTFILE.LDF INTO LDAP. IF THE FILE CONTAINS HTTP-BASED ATTRVAL-SPEC SUCH AS THUMBNAILPHOTO:< HTTP://EXAMPLE.ORG/SOMEFILE.TXT, THE FILE WILL BE DOWNLOADED INTO IE TEMP FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Ldifde -i -f inputfile.ldf" nocase

condition:
    $a0
}

