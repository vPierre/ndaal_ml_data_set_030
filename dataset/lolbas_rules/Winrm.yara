rule winrm_vbs0_Execute
{
    meta:
        id = "7GTdB4Wx9uYUTFlnq8Dxfs"
        fingerprint = "bcc49dd4b37a96aaa227f9f700807bb3478de4ac4c16d2cc80c36ac2bc9b1b3c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used for manage Windows RM settings"
        category = "TECHNIQUE"
        technique = "LATERAL MOVEMENT/REMOTE COMMAND EXECUTION VIA WMI WIN32_PROCESS CLASS OVER THE WINRM PROTOCOL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="winrm invoke Create wmicimv2/Win32_Process @{CommandLine=\"notepad.exe\"} -r:" nocase

condition:
    $a0
}

rule winrm_vbs1_Execute
{
    meta:
        id = "3gc1HkI3sA3rwMBqXI9ykz"
        fingerprint = "4cd4d80287a3fdae705a3838fbe14fc02842fecfeb928856d085d1f9eb803793"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used for manage Windows RM settings"
        category = "TECHNIQUE"
        technique = "LATERAL MOVEMENT/REMOTE COMMAND EXECUTION VIA WMI WIN32_SERVICE CLASS OVER THE WINRM PROTOCOL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="winrm invoke Create wmicimv2/Win32_Service @{Name=\"Evil\";DisplayName=\"Evil\";PathName=\"cmd.exe /k c:\\windows\\system32\\notepad.exe\"} -r: && winrm invoke StartService wmicimv2/Win32_Service?Name=Evil -r:" nocase

condition:
    $a0
}

rule winrm_vbs2_AWL_Bypass
{
    meta:
        id = "7Ircc6B7lTbOSkm6WNbXJP"
        fingerprint = "84b89b92f0f79e12f15b362658b6e2ebd82c4fa0bc199497549918eb41a6e5cd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used for manage Windows RM settings"
        category = "TECHNIQUE"
        technique = "BYPASS AWL SOLUTIONS BY COPYING CSCRIPT.EXE TO AN ATTACKER-CONTROLLED LOCATION; CREATING A MALICIOUS WSMPTY.XSL IN THE SAME LOCATION, AND EXECUTING WINRM.VBS VIA THE RELOCATED CSCRIPT.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="%SystemDrive%\\BypassDir\\cscript //nologo %windir%\\System32\\winrm.vbs get wmicimv2/Win32_Process?Handle=4 -format:pretty" nocase

condition:
    $a0
}

