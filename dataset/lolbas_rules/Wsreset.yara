rule Wsreset_exe0_UAC_Bypass
{
    meta:
        id = "4XGbGpgW3gF6QGXhZCD1M7"
        fingerprint = "f06ba92844e5b384dfa943f1a21ee5b7b28e629bff81058760da248a333d6e49"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to reset Windows Store settings according to its manifest file"
        category = "TECHNIQUE"
        technique = "DURING STARTUP, WSRESET.EXE CHECKS THE REGISTRY VALUE HKCU\\SOFTWARE\\CLASSES\\APPX82A6GWRE4FDG3BT635TN5CTQJF8MSDD2\\SHELL\\OPEN\\COMMAND FOR THE COMMAND TO RUN. BINARY WILL BE EXECUTED AS A HIGH-INTEGRITY PROCESS WITHOUT A UAC PROMPT BEING DISPLAYED TO THE USER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wsreset.exe" nocase

condition:
    $a0
}

