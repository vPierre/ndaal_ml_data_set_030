rule AccCheckConsole_exe0_Execute
{
    meta:
        id = "67lQv2JCUJquLbB0zrdO2s"
        fingerprint = "03ee8fa056c5fcde1d7c20012a3ff42674195cca42505c63b145a677b53ce26f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Verifies UI accessibility requirements"
        category = "TECHNIQUE"
        technique = "LOAD A MANAGED DLL IN THE CONTEXT OF ACCCHECKCONSOLE.EXE. THE -WINDOW SWITCH VALUE CAN BE SET TO AN ARBITRARY ACTIVE WINDOW NAME."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AccCheckConsole.exe -window \"Untitled - Notepad\" C:\\path\\to\\your\\lolbas.dll" nocase

condition:
    $a0
}

rule AccCheckConsole_exe1_AWL_Bypass
{
    meta:
        id = "6StZ9P7bYrBwWT7x4uNahT"
        fingerprint = "03ee8fa056c5fcde1d7c20012a3ff42674195cca42505c63b145a677b53ce26f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Verifies UI accessibility requirements"
        category = "TECHNIQUE"
        technique = "LOAD A MANAGED DLL IN THE CONTEXT OF ACCCHECKCONSOLE.EXE. THE -WINDOW SWITCH VALUE CAN BE SET TO AN ARBITRARY ACTIVE WINDOW NAME."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="AccCheckConsole.exe -window \"Untitled - Notepad\" C:\\path\\to\\your\\lolbas.dll" nocase

condition:
    $a0
}

