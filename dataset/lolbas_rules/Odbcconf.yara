rule Odbcconf_exe0_Execute
{
    meta:
        id = "49Qe648Q9Q10g29KPensQP"
        fingerprint = "47ec0f04ddc509755394d5923095afa1d19e1e9ceca603006e79a1b9f9e419fd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used in Windows for managing ODBC connections"
        category = "TECHNIQUE"
        technique = "EXECUTE DLLREGISTERSERVER FROM DLL SPECIFIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="odbcconf /a {REGSVR c:\\test\\test.dll}" nocase

condition:
    $a0
}

rule Odbcconf_exe1_Execute
{
    meta:
        id = "71NyM216VO7trd3OS70qqJ"
        fingerprint = "eeb45e4bddf084a223f3d04186afeba6a5c052eaeee9858c69def5bae0ef8124"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used in Windows for managing ODBC connections"
        category = "TECHNIQUE"
        technique = "INSTALL A DRIVER AND LOAD THE DLL. REQUIRES ADMINISTRATOR PRIVILEGES."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0=/odbcconf INSTALLDRIVER|odbcconf configsysdsn/

condition:
    $a0
}

rule Odbcconf_exe2_Execute
{
    meta:
        id = "5nPnjaLhTDNirL9ASYex9p"
        fingerprint = "be8ac6a178cdde21e96c2680524ca7044eaa7d6b0e3b2453ee9f94369aa8f314"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used in Windows for managing ODBC connections"
        category = "TECHNIQUE"
        technique = "LOAD DLL SPECIFIED IN TARGET .RSP FILE. SEE THE CODE SAMPLE SECTION FOR AN EXAMPLE .RSP FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="odbcconf -f file.rsp" nocase

condition:
    $a0
}

