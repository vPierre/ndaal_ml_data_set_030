rule DataSvcUtil_exe0_Upload
{
    meta:
        id = "1zzO9P4QyAYiRauFsGcS1L"
        fingerprint = "cb9d74c46bd93259cbf8d08585fea0746c1a73745a6e55fc14f1a8ad87af1285"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "DataSvcUtil.exe is a command-line tool provided by WCF Data Services that consumes an Open Data Protocol (OData) feed and generates the client data service classes that are needed to access a data service from a .NET Framework client application."
        category = "TECHNIQUE"
        technique = "UPLOAD FILE, CREDENTIALS OR DATA EXFILTRATION IN GENERAL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="DataSvcUtil /out:C:\\Windows\\System32\\calc.exe /uri:" nocase

condition:
    $a0
}

