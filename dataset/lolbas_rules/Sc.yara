rule Sc_exe0_ADS
{
    meta:
        id = "3zjhN5kFhxLZwE9eVe0oS7"
        fingerprint = "643399d81483659dc78e2e8c6e74c9c111040097de155bb9d5afbd0fcc9711e4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manage services"
        category = "TECHNIQUE"
        technique = "CREATES A NEW SERVICE AND EXECUTES THE FILE STORED IN THE ADS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="sc create evilservice binPath=\"\\\"c:\\\\ADS\\\\file.txt:cmd.exe\\\" /c echo works > \\\"c:\\ADS\\works.txt\\\"\" DisplayName= \"evilservice\" start= auto\\ & sc start evilservice" nocase

condition:
    $a0
}

rule Sc_exe1_ADS
{
    meta:
        id = "1TBSAIVMTdCAOG2eLO9OUo"
        fingerprint = "96c018141a7b1319ddbf0bac9c2d78cd2bfa4f0214bb0f84a97c6902cb747c0b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manage services"
        category = "TECHNIQUE"
        technique = "MODIFIES AN EXISTING SERVICE AND EXECUTES THE FILE STORED IN THE ADS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="sc config <existing> binPath=\"\\\"c:\\\\ADS\\\\file.txt:cmd.exe\\\" /c echo works > \\\"c:\\ADS\\works.txt\\\"\" & sc start <existing>" nocase

condition:
    $a0
}

