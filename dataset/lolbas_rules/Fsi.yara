rule Fsi_exe0_AWL_Bypass
{
    meta:
        id = "7O9QYO097wcm75h03GileM"
        fingerprint = "90ab9033170b27d93bdbad7d7902dfc9ba635251adcde67daaf3c190b0405eff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "64-bit FSharp (F#) Interpreter included with Visual Studio and DotNet Core SDK."
        category = "TECHNIQUE"
        technique = "EXECUTE F# CODE VIA SCRIPT FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fsi.exe c:\\path\\to\\test.fsscript" nocase

condition:
    $a0
}

rule Fsi_exe1_AWL_Bypass
{
    meta:
        id = "7gs6dlotemW7qNY1yC0DC5"
        fingerprint = "f8a807baf3a41d5a859d7fb9f37c1a6114ea00b6bb73ee57c904399945159c2e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "64-bit FSharp (F#) Interpreter included with Visual Studio and DotNet Core SDK."
        category = "TECHNIQUE"
        technique = "EXECUTE F# CODE VIA INTERACTIVE COMMAND LINE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fsi.exe" nocase

condition:
    $a0
}

