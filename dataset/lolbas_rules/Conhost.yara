rule Conhost_exe0_Execute
{
    meta:
        id = "5e2xgFcNCfFMeuhxZxA9D7"
        fingerprint = "195979634a58c2e5da5f0a91cd8dd35087565bb91e9ba60605b59b5f03ab59de"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Console Window host"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH CONHOST.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="conhost.exe calc.exe" nocase

condition:
    $a0
}

rule Conhost_exe1_Execute
{
    meta:
        id = "7Bf9rhAZXhOonQstkTvVu4"
        fingerprint = "d156d2affca3c78eccf9f78eb8fdd27c3a861ff710b43bd5d775dd1e11d9a687"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Console Window host"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH CONHOST.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="conhost.exe --headless calc.exe" nocase

condition:
    $a0
}

