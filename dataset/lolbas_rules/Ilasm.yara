rule Ilasm_exe0_Compile
{
    meta:
        id = "5pSnDCJ0iDlTYEg7t9cSjP"
        fingerprint = "5cb6ba2a0f552dd4b581bfc7909b32a94cdcfbb748a9a781121421456eddcc6d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "used for compile c# code into dll or exe."
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE C#/INTERMEDIATE (IL) CODE TO .EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ilasm.exe C:\\public\\test.txt /exe" nocase

condition:
    $a0
}

rule Ilasm_exe1_Compile
{
    meta:
        id = "46w0AFlBNSgU7z7GG83kol"
        fingerprint = "402dd16bac19fa05f3ec223c683ae755bd24b3ee9c28a0ddedb70665a2a60ef3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "used for compile c# code into dll or exe."
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE C#/INTERMEDIATE (IL) CODE TO DLL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ilasm.exe C:\\public\\test.txt /dll" nocase

condition:
    $a0
}

