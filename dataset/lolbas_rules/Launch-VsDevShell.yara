rule Launch_VsDevShell_ps10_Execute
{
    meta:
        id = "6bjTlmZwyjyc45ySAsWhdL"
        fingerprint = "3b8bb68af75b29a6fbf2ab0552cfd24589b7e147c6bb99c35e868385362df4cb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Locates and imports a Developer PowerShell module and calls the Enter-VsDevShell cmdlet"
        category = "TECHNIQUE"
        technique = "EXECUTE BINARIES FROM THE CONTEXT OF THE SIGNED SCRIPT USING THE \"VSWHEREPATH\" FLAG."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="powershell -ep RemoteSigned -f .\\Launch-VsDevShell.ps1 -VsWherePath \"C:\\windows\\system32\\calc.exe\"" nocase

condition:
    $a0
}

rule Launch_VsDevShell_ps11_Execute
{
    meta:
        id = "3W6RaoCuhCf7GZgx4V2U8G"
        fingerprint = "ebff7a5bbb104177aabf67856c3d5f710a97373e68e1637542286aaf57688a46"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Locates and imports a Developer PowerShell module and calls the Enter-VsDevShell cmdlet"
        category = "TECHNIQUE"
        technique = "EXECUTE BINARIES AND COMMANDS FROM THE CONTEXT OF THE SIGNED SCRIPT USING THE \"VSINSTALLATIONPATH\" FLAG."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="powershell -ep RemoteSigned -f .\\Launch-VsDevShell.ps1 -VsInstallationPath \"/../../../../../; calc.exe ;\"" nocase

condition:
    $a0
}

