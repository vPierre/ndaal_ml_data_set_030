rule Runonce_exe0_Execute
{
    meta:
        id = "6vmtSujPnxdKwthkSC8e7v"
        fingerprint = "dab5c83dde504db893c62d0f02ba3d655b65eabb44593657b4c9a559fc5bd92f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Executes a Run Once Task that has been configured in the registry"
        category = "TECHNIQUE"
        technique = "EXECUTES A RUN ONCE TASK THAT HAS BEEN CONFIGURED IN THE REGISTRY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Runonce.exe /AlternateShellStartup" nocase

condition:
    $a0
}

