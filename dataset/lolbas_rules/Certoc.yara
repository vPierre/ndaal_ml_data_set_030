rule CertOC_exe0_Execute
{
    meta:
        id = "6W2Negt9NUrMRkKEfSTAlr"
        fingerprint = "38ebae73ce72b67fa3753f05373bd7c1dc8876bc83b14a8b6dcadd70939bb1e0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for installing certificates"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET DLL FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certoc.exe -LoadDLL \"C:\\test\\calc.dll\"" nocase

condition:
    $a0
}

rule CertOC_exe1_Download
{
    meta:
        id = "4eJ3PlunxrY1pSuTAOmfQT"
        fingerprint = "1ecbdd86dee6b169426a9f7e02ab644820d2a5f221ddceecf8f4c1c21f04b3bb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for installing certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOADS TEXT FORMATTED FILES"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="certoc.exe -GetCACAPS " nocase

condition:
    $a0
}

