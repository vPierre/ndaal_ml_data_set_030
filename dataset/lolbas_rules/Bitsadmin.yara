rule Bitsadmin_exe0_ADS
{
    meta:
        id = "7TKmJRerreiEHgpdOOwW2v"
        fingerprint = "c3b7a309458861f5942a8aac21333a6cbf2a01c1d798726d606acb3142bb9b7a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "CREATE A BITSADMIN JOB NAMED 1, ADD CMD.EXE TO THE JOB, CONFIGURE THE JOB TO RUN THE TARGET COMMAND FROM AN ALTERNATE DATA STREAM, THEN RESUME AND COMPLETE THE JOB."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bitsadmin /create 1 bitsadmin /addfile 1 c:\\windows\\system32\\cmd.exe c:\\data\\playfolder\\cmd.exe bitsadmin /SetNotifyCmdLine 1 c:\\data\\playfolder\\1.txt:cmd.exe NULL bitsadmin /RESUME 1 bitsadmin /complete 1" nocase

condition:
    $a0
}

rule Bitsadmin_exe1_Download
{
    meta:
        id = "7UFRSTZdcbkkXZg82rbYeG"
        fingerprint = "85423e398a433e3e74cdd4afda26fa99739778100bec9524e97899349d4f0eed"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "CREATE A BITSADMIN JOB NAMED 1, ADD CMD.EXE TO THE JOB, CONFIGURE THE JOB TO RUN THE TARGET COMMAND, THEN RESUME AND COMPLETE THE JOB."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bitsadmin /create 1 bitsadmin /addfile 1  c:\\data\\playfolder\\autoruns.exe bitsadmin /RESUME 1 bitsadmin /complete 1" nocase

condition:
    $a0
}

rule Bitsadmin_exe2_Copy
{
    meta:
        id = "2jkuQAbLS3N5s6O9jJLpAU"
        fingerprint = "811e469677f69c4f9b3154a74645d1925b3d77150baf1b02bfe733903eacb56a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "COMMAND FOR COPYING CMD.EXE TO ANOTHER FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bitsadmin /create 1 & bitsadmin /addfile 1 c:\\windows\\system32\\cmd.exe c:\\data\\playfolder\\cmd.exe & bitsadmin /RESUME 1 & bitsadmin /Complete 1 & bitsadmin /reset" nocase

condition:
    $a0
}

rule Bitsadmin_exe3_Execute
{
    meta:
        id = "6Bhd83pnX8noZFpg0uGcJV"
        fingerprint = "ee73a3d6fa89aa7714e2eb27c328d11680ca3ecc2b43d5ef368b8779d1a62372"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "ONE-LINER THAT CREATES A BITSADMIN JOB NAMED 1, ADD CMD.EXE TO THE JOB, CONFIGURE THE JOB TO RUN THE TARGET COMMAND, THEN RESUME AND COMPLETE THE JOB."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="bitsadmin /create 1 & bitsadmin /addfile 1 c:\\windows\\system32\\cmd.exe c:\\data\\playfolder\\cmd.exe & bitsadmin /SetNotifyCmdLine 1 c:\\data\\playfolder\\cmd.exe NULL & bitsadmin /RESUME 1 & bitsadmin /Reset" nocase

condition:
    $a0
}

