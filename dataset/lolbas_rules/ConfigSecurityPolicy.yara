rule ConfigSecurityPolicy_exe0_Upload
{
    meta:
        id = "3WomNOqXCaVJsd1jahdKSa"
        fingerprint = "f723da24530aa104899ed38813d69550cd0038718176766cdb6e527a713e91a7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender. you can configure different pilot collections for each of the co-management workloads. Being able to use different pilot collections allows you to take a more granular approach when shifting workloads."
        category = "TECHNIQUE"
        technique = "UPLOAD FILE, CREDENTIALS OR DATA EXFILTRATION IN GENERAL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ConfigSecurityPolicy.exe C:\\Windows\\System32\\calc.exe " nocase

condition:
    $a0
}

rule ConfigSecurityPolicy_exe1_Download
{
    meta:
        id = "9NgaJwnCqV6HGf4PrpbLX"
        fingerprint = "00ef8334b60d4aa86dff0300d745a2e6abd1257ced7116168dafcf44455c2ad4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender. you can configure different pilot collections for each of the co-management workloads. Being able to use different pilot collections allows you to take a more granular approach when shifting workloads."
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ConfigSecurityPolicy.exe " nocase

condition:
    $a0
}

