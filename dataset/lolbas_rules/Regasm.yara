rule Regasm_exe0_AWL_Bypass
{
    meta:
        id = "1bY4aEi2lZGAqYeM2qcHXq"
        fingerprint = "7ffc233b39cbf72108dcac0a0949dcd861dec796761b4d57708c86887734097c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Part of .NET"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE REGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regasm.exe AllTheThingsx64.dll" nocase

condition:
    $a0
}

rule Regasm_exe1_Execute
{
    meta:
        id = "344tvORSqhn6RzkkVi8eRF"
        fingerprint = "8f17faa9966b3aae09567c454cf2a515d0d05e4a78d9a605eaabe1875f0a8236"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Part of .NET"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE UNREGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regasm.exe /U AllTheThingsx64.dll" nocase

condition:
    $a0
}

