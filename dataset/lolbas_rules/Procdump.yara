rule Procdump_exe0_Execute
{
    meta:
        id = "oAo8QnQDhOCJtGQ9X1qAc"
        fingerprint = "1ca5ab88c72f4a19989c1268f5f488fe9921eab1520c78e56d89c5ebb6bef28d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "SysInternals Memory Dump Tool"
        category = "TECHNIQUE"
        technique = "LOADS CALC.DLL WHERE DLL IS CONFIGURED WITH A 'MINIDUMPCALLBACKROUTINE' EXPORTED FUNCTION. VALID PROCESS MUST BE PROVIDED AS DUMP STILL CREATED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="procdump.exe -md calc.dll explorer.exe" nocase

condition:
    $a0
}

rule Procdump_exe1_Execute
{
    meta:
        id = "28HzGfbnr7huoQCVgLmXny"
        fingerprint = "daee23c81b801d509db42d089a1f8d45a6a5a1c204f6871341bc1cd54413858d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "SysInternals Memory Dump Tool"
        category = "TECHNIQUE"
        technique = "LOADS CALC.DLL WHERE CONFIGURED WITH DLL_PROCESS_ATTACH EXECUTION, PROCESS ARGUMENT CAN BE ARBITRARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="procdump.exe -md calc.dll foobar" nocase

condition:
    $a0
}

