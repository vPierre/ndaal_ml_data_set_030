rule DefaultPack_EXE0_Execute
{
    meta:
        id = "5Ta4AtgDeX6s7olTfpSADL"
        fingerprint = "1e9ee848bcfde9ce03be0b089c1b6ef8f1dcc8d0ef6a11b0044af9eb06b5f668"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "This binary can be downloaded along side multiple software downloads on the microsoft website. It gets downloaded when the user forgets to uncheck the option to set Bing as the default search provider."
        category = "TECHNIQUE"
        technique = "USE DEFAULTPACK.EXE TO EXECUTE ARBITRARY BINARIES, WITH ADDED ARGUMENT SUPPORT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="DefaultPack.EXE /C:\"process.exe args\"" nocase

condition:
    $a0
}

