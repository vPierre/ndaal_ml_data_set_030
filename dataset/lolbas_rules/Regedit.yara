rule Regedit_exe0_ADS
{
    meta:
        id = "5y1oEkgKECOvNZQD8CxBN8"
        fingerprint = "4fe6073de31302ebd247ab79e262b337b93c15600264f832f10ebc59d00ba0e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manipulate registry"
        category = "TECHNIQUE"
        technique = "EXPORT THE TARGET REGISTRY KEY TO THE SPECIFIED .REG FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regedit /E c:\\ads\\file.txt:regfile.reg HKEY_CURRENT_USER\\MyCustomRegKey" nocase

condition:
    $a0
}

rule Regedit_exe1_ADS
{
    meta:
        id = "7SQI80R5FEUTlf6LJq0pM4"
        fingerprint = "8f1071c4bcea2ca97dcdd7b08d3d0b8af1527aed75ddf34f67206d9b5beac7d5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manipulate registry"
        category = "TECHNIQUE"
        technique = "IMPORT THE TARGET .REG FILE INTO THE REGISTRY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="regedit C:\\ads\\file.txt:regfile.reg" nocase

condition:
    $a0
}

