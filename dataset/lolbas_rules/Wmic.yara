rule Wmic_exe0_ADS
{
    meta:
        id = "XCHV4y3CTEJDNkrAnY1T3"
        fingerprint = "2e7197594cae08bbab0c902f17eef768076f3daaf03a5aa61cdbc342b536f29e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTE A .EXE FILE STORED AS AN ALTERNATE DATA STREAM (ADS)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wmic.exe process call create \"c:\\ads\\file.txt:program.exe\"" nocase

condition:
    $a0
}

rule Wmic_exe1_Execute
{
    meta:
        id = "1lLZG5Y4GiTiBIn2Tvo8Xi"
        fingerprint = "922c8c8ef31b655a1d58baa977467ef71fb99bf5fb74a0a082f511cb467640dc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC FROM WMIC"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wmic.exe process call create calc" nocase

condition:
    $a0
}

rule Wmic_exe2_Execute
{
    meta:
        id = "5iPVDCN55NleAwPGm8zkGb"
        fingerprint = "c9e7cd232101a7162546b24616f36c9e7388491031159bb3d5643b32328a36a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTE EVIL.EXE ON THE REMOTE SYSTEM."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wmic.exe /node:\"192.168.0.1\" process call create \"evil.exe\"" nocase

condition:
    $a0
}

rule Wmic_exe3_Execute
{
    meta:
        id = "4jQ8wcfgWIAClywnolsRKE"
        fingerprint = "766bbafde00a303df173bdc12897ceb05307ca18cea4689e70e3f3cf4e868e39"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "CREATE A VOLUME SHADOW COPY OF NTDS.DIT THAT CAN BE COPIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wmic.exe process get brief /format:\"" nocase

condition:
    $a0
}

rule Wmic_exe4_Execute
{
    meta:
        id = "9gR6Tmwm9nd3kVQKT1tKS"
        fingerprint = "736bd5ef57401f999b22ae91355f5dbe3c3c43db68596fe850e377b8db3facaf"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTES JSCRIPT OR VBSCRIPT EMBEDDED IN THE TARGET REMOTE XSL STYLSHEET."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wmic.exe process get brief /format:\"\\\\127.0.0.1\\c$\\Tools\\pocremote.xsl\"" nocase

condition:
    $a0
}

