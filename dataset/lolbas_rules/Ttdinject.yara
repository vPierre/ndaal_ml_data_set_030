rule Ttdinject_exe0_Execute
{
    meta:
        id = "2RFFI8rcRgULkX2E7nAH96"
        fingerprint = "fe203deee89b1768007a7f849cd2a8c775c3086828a99ddfdf7bd740d5220d8c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel (Underlying call of tttracer.exe)"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC USING TTDINJECT.EXE. REQUIRES ADMINISTRATOR PRIVILEGES. A LOG FILE WILL BE CREATED IN TMP.RUN. THE LOG FILE CAN BE CHANGED, BUT THE LENGTH (7) HAS TO BE UPDATED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="TTDInject.exe /ClientParams \"7 tmp.run 0 0 0 0 0 0 0 0 0 0\" /Launch \"C:/Windows/System32/calc.exe\"" nocase

condition:
    $a0
}

rule Ttdinject_exe1_Execute
{
    meta:
        id = "7MVbI0EE98pCySH7E38Jrc"
        fingerprint = "5103c1ee2ee62bbcf91bc97764fc404fff08839711dd73016f079d0411c037af"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel (Underlying call of tttracer.exe)"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC USING TTDINJECT.EXE. REQUIRES ADMINISTRATOR PRIVILEGES. A LOG FILE WILL BE CREATED IN TMP.RUN. THE LOG FILE CAN BE CHANGED, BUT THE LENGTH (7) HAS TO BE UPDATED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ttdinject.exe /ClientScenario TTDRecorder /ddload 0 /ClientParams \"7 tmp.run 0 0 0 0 0 0 0 0 0 0\" /launch \"C:/Windows/System32/calc.exe\"" nocase

condition:
    $a0
}

