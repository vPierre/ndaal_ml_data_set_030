rule Atbroker_exe0_Execute
{
    meta:
        id = "5b5221MbBnhEGobce3nmfA"
        fingerprint = "1258a28a26b12cda0db7f60c7869bc254ba5df06ea3987d63989dfe33af11e2a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Helper binary for Assistive Technology (AT)"
        category = "TECHNIQUE"
        technique = "START A REGISTERED ASSISTIVE TECHNOLOGY (AT)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ATBroker.exe /start malware" nocase

condition:
    $a0
}

