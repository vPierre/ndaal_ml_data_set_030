rule Eventvwr_exe0_UAC_Bypass
{
    meta:
        id = "6jkGdHHW33gHzLUn2Hxae4"
        fingerprint = "f1890ec46fb7dc385d363e6da06748246e7cf452cad36f6664c295919bf8ebed"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Displays Windows Event Logs in a GUI window."
        category = "TECHNIQUE"
        technique = "DURING STARTUP, EVENTVWR.EXE CHECKS THE REGISTRY VALUE HKCU\\SOFTWARE\\CLASSES\\MSCFILE\\SHELL\\OPEN\\COMMAND FOR THE LOCATION OF MMC.EXE, WHICH IS USED TO OPEN THE EVENTVWR.MSC SAVED CONSOLE FILE. IF THE LOCATION OF ANOTHER BINARY OR SCRIPT IS ADDED TO THIS REGISTRY VALUE, IT WILL BE EXECUTED AS A HIGH-INTEGRITY PROCESS WITHOUT A UAC PROMPT BEING DISPLAYED TO THE USER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="eventvwr.exe" nocase

condition:
    $a0
}

rule Eventvwr_exe1_UAC_Bypass
{
    meta:
        id = "1cOJwdIFNweshlUhzN9l5B"
        fingerprint = "a6fb0a89ff6c67e24e6cfd7637abf70445988cade450699ba45f0f2b4eae7037"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Displays Windows Event Logs in a GUI window."
        category = "TECHNIQUE"
        technique = "DURING STARTUP, EVENTVWR.EXE USES .NET DESERIALIZATION WITH %LOCALAPPDATA%\\MICROSOFT\\EVENTV~1\\RECENTVIEWS FILE. THIS FILE CAN BE CREATED USING HTTPS://GITHUB.COM/PWNTESTER/YSOSERIAL.NET"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ysoserial.exe -o raw -f BinaryFormatter - g DataSet -c calc > RecentViews & copy RecentViews %LOCALAPPDATA%\\Microsoft\\EventV~1\\RecentViews & eventvwr.exe" nocase

condition:
    $a0
}

