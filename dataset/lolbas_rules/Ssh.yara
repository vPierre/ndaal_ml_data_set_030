rule ssh_exe0_Execute
{
    meta:
        id = "3PcgBo0eVtoLDhXW2GvYuq"
        fingerprint = "25d2f77061c2d0b2bd48ab689fd3767505af2720d2d9777893f6a10a7b6197ce"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Ssh.exe is the OpenSSH compatible client can be used to connect to Windows 10 (build 1809 and later) and Windows Server 2019 devices."
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE ON HOST MACHINE. THE PROMPT FOR PASSWORD CAN BE ELIMINATED BY ADDING THE HOST'S PUBLIC KEY IN THE USER'S AUTHORIZED_KEYS FILE. ADVERSARIES CAN DO THE SAME FOR EXECUTION ON REMOTE MACHINES."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ssh localhost calc.exe" nocase

condition:
    $a0
}

rule ssh_exe1_Execute
{
    meta:
        id = "7DMFD7GIHdvs9ZYixxDNgo"
        fingerprint = "1380050ef4e0000bf610a9afba42b6a3b2062889c3457f229b28979439e09352"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Ssh.exe is the OpenSSH compatible client can be used to connect to Windows 10 (build 1809 and later) and Windows Server 2019 devices."
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM SSH.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ssh -o ProxyCommand=calc.exe ." nocase

condition:
    $a0
}

