rule Mftrace_exe0_Execute
{
    meta:
        id = "2ItQKv3buS1OnYpLTVdj78"
        fingerprint = "e42522d3d9196a820abc4852cf0da5f8d946ce83542ac0f6023c35c0f0955229"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Trace log generation tool for Media Foundation Tools."
        category = "TECHNIQUE"
        technique = "LAUNCH CMD.EXE AS A SUBPROCESS OF MFTRACE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Mftrace.exe cmd.exe" nocase

condition:
    $a0
}

rule Mftrace_exe1_Execute
{
    meta:
        id = "6hRXfSUlvyoExm1YZdGFWH"
        fingerprint = "b2c7d9ef0e8f683876cfd8a88c3547748b57802ebd2d3237e23081331c8631e6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Trace log generation tool for Media Foundation Tools."
        category = "TECHNIQUE"
        technique = "LAUNCH CMD.EXE AS A SUBPROCESS OF MFTRACE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Mftrace.exe powershell.exe" nocase

condition:
    $a0
}

