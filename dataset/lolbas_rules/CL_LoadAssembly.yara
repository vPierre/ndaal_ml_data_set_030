rule CL_LoadAssembly_ps10_Execute
{
    meta:
        id = "4pXB9Yhjs4FsGqgqXW0TTr"
        fingerprint = "f9713399897265037607bb2028f7beff0be7a401f46091d7ddf6f333d3c697af"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "PowerShell Diagnostic Script"
        category = "TECHNIQUE"
        technique = "PROXY EXECUTE MANAGED DLL WITH POWERSHELL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="powershell.exe -ep bypass -command \"set-location -path C:\\Windows\\diagnostics\\system\\Audio; import-module .\\CL_LoadAssembly.ps1; LoadAssemblyFromPath ..\\..\\..\\..\\testing\\fun.dll;[Program]::Fun()\"" nocase

condition:
    $a0
}

