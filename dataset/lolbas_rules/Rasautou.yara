rule Rasautou_exe0_Execute
{
    meta:
        id = "7AE4Q0EpFIsAZNcdqkY7gj"
        fingerprint = "f3fd650066aabe579e2e4e66cd7c01c2854eb102c877fb5822def6c3804ab3e1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Remote Access Dialer"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL SPECIFIED IN -D AND EXECUTES THE EXPORT SPECIFIED IN -P. OPTIONS REMOVED IN WINDOWS 10."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rasautou -d powershell.dll -p powershell -a a -e e" nocase

condition:
    $a0
}

