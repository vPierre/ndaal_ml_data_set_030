rule SettingSyncHost_exe0_Execute
{
    meta:
        id = "1tRRuqhj0L56zepUtehpG9"
        fingerprint = "f9954cae6920bfc194a2d26c1ff698c088e7f2f1d5c2c359d7bb221f49957846"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Host Process for Setting Synchronization"
        category = "TECHNIQUE"
        technique = "EXECUTE FILE SPECIFIED IN %COMSPEC%"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="SettingSyncHost -LoadAndRunDiagScript anything" nocase

condition:
    $a0
}

rule SettingSyncHost_exe1_Execute
{
    meta:
        id = "1OIetaeuk8SubBHNDtLSsA"
        fingerprint = "4477fca54eefdebe24c77b984ec51e45265480045938d50a4eec0d512f0d076f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Host Process for Setting Synchronization"
        category = "TECHNIQUE"
        technique = "EXECUTE A BATCH SCRIPT IN THE BACKGROUND (NO WINDOW EVER POPS UP) WHICH CAN BE SUBVERTED TO RUNNING ARBITRARY PROGRAMS BY SETTING THE CURRENT WORKING DIRECTORY TO %TMP% AND CREATING FILES SUCH AS REG.BAT/REG.EXE IN THAT DIRECTORY THEREBY CAUSING THEM TO EXECUTE INSTEAD OF THE ONES IN C:\\WINDOWS\\SYSTEM32."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="SettingSyncHost -LoadAndRunDiagScriptNoCab anything" nocase

condition:
    $a0
}

