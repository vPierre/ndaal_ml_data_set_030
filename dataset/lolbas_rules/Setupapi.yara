rule Setupapi_dll0_AWL_Bypass
{
    meta:
        id = "lXSJln086ySX88leKiskX"
        fingerprint = "24431f022d44a4b9415496f4f24317a28d59a4fd61d86a0d2cbd1e63738281e5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Setup Application Programming Interface"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe setupapi.dll,InstallHinfSection DefaultInstall 128 C:\\Tools\\shady.inf" nocase

condition:
    $a0
}

rule Setupapi_dll1_Execute
{
    meta:
        id = "3XWZDYcSn9AEUsEIggeP8X"
        fingerprint = "599309ac7b109166ecfa1eecdf29249e60ec44c5c0d007ffb3e0c1b90954d8ee"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Setup Application Programming Interface"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE FILE VIA THE INSTALLHINFSECTION FUNCTION AND .INF FILE SECTION DIRECTIVE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe setupapi.dll,InstallHinfSection DefaultInstall 128 C:\\Tools\\calc_exe.inf" nocase

condition:
    $a0
}

