rule Wfc_exe0_AWL_Bypass
{
    meta:
        id = "6fL5MX6VlSxXVBGAQriCCj"
        fingerprint = "bd2eaf93b4a091040178583bd18ee8dfd658fd2610f1c9f95668f0b38615442f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Workflow Command-line Compiler tool is included with the Windows Software Development Kit (SDK)."
        category = "TECHNIQUE"
        technique = "EXECUTE ARBITRARY C# CODE EMBEDDED IN A XOML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wfc.exe c:\\path\\to\\test.xoml" nocase

condition:
    $a0
}

