rule Gpscript_exe0_Execute
{
    meta:
        id = "5Np4kObx3DJcKSNi3SuzDT"
        fingerprint = "69a5a3372ac43b88fc2d8022e34d2e9c74de0cb6c890a1fe92a70cfa3ac14899"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by group policy to process scripts"
        category = "TECHNIQUE"
        technique = "EXECUTES LOGON SCRIPTS CONFIGURED IN GROUP POLICY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Gpscript /logon" nocase

condition:
    $a0
}

rule Gpscript_exe1_Execute
{
    meta:
        id = "QWI5dAbciKRocLqGFk8Cz"
        fingerprint = "3ede1f6657e93e0b9703d8a385b28fd7cd09092dc123d8279eafc937bacd21cc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by group policy to process scripts"
        category = "TECHNIQUE"
        technique = "EXECUTES STARTUP SCRIPTS CONFIGURED IN GROUP POLICY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Gpscript /startup" nocase

condition:
    $a0
}

