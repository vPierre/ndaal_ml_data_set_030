rule Desktopimgdownldr_exe0_Download
{
    meta:
        id = "3sAivvoZRGyIj1Ki6mJaAo"
        fingerprint = "75239fcfd99b147adc3ccc6519a29a6fab6bb03a5489232c9b26e88e1ec3a8c1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used to configure lockscreen/desktop image"
        category = "TECHNIQUE"
        technique = "DOWNLOADS THE FILE AND SETS IT AS THE COMPUTER'S LOCKSCREEN"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="set \"SYSTEMROOT=C:\\Windows\\Temp\" && cmd /c desktopimgdownldr.exe /lockscreenurl: /eventName:desktopimgdownldr" nocase

condition:
    $a0
}

