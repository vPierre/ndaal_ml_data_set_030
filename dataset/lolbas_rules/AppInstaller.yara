rule AppInstaller_exe0_Download
{
    meta:
        id = "2JuQj49SyFEjQgHSKIt1kC"
        fingerprint = "bdd5689e0c41a103a1462fc63950077b2bf11a1263e2e26ddda4acad2dd2e53e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Tool used for installation of AppX/MSIX applications on Windows 10"
        category = "TECHNIQUE"
        technique = "APPINSTALLER.EXE IS SPAWNED BY THE DEFAULT HANDLER FOR THE URI, IT ATTEMPTS TO LOAD/INSTALL A PACKAGE FROM THE URL AND IS SAVED IN C:\\USERS\\%USERNAME%\\APPDATA\\LOCAL\\PACKAGES\\MICROSOFT.DESKTOPAPPINSTALLER_8WEKYB3D8BBWE\\AC\\INETCACHE\\<RANDOM-8-CHAR-DIRECTORY>"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="start ms-appinstaller://?source=" nocase

condition:
    $a0
}

