rule IMEWDBLD_exe0_Download
{
    meta:
        id = "1ykiX3PFb3TSdalGR43gWQ"
        fingerprint = "d5367e17d6cac654be034ffce12b78e1b7054184b5c6418f170dd0a77c5db92b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft IME Open Extended Dictionary Module"
        category = "TECHNIQUE"
        technique = "IMEWDBLD.EXE ATTEMPTS TO LOAD A DICTIONARY FILE, IF PROVIDED A URL AS AN ARGUMENT, IT WILL DOWNLOAD THE FILE SERVED AT BY THAT URL AND SAVE IT TO %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION> OR %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION>"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="C:\\Windows\\System32\\IME\\SHARED\\IMEWDBLD.exe " nocase

condition:
    $a0
}

