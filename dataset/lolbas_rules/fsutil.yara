rule fsutil_exe0_Tamper
{
    meta:
        id = "72U4xb8kG3oBr7KN7s4n0p"
        fingerprint = "2eab278f5e29142ad00165b20a2f8b59a179dd6e40f870bcf97e515c8c3183b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File System Utility"
        category = "TECHNIQUE"
        technique = "ZERO OUT A FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fsutil.exe file setZeroData offset=0 length=9999999999 C:\\Windows\\Temp\\payload.dll" nocase

condition:
    $a0
}

rule fsutil_exe1_Tamper
{
    meta:
        id = "8oYOFULiiOR1YXcf8fPJ7"
        fingerprint = "c81b5a05d8803d3e0def36a9dd1473a90e4b06329d410f69fcff0f50f0140a91"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File System Utility"
        category = "TECHNIQUE"
        technique = "DELETE THE USN JOURNAL VOLUME TO HIDE FILE CREATION ACTIVITY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="fsutil.exe usn deletejournal /d c:" nocase

condition:
    $a0
}

