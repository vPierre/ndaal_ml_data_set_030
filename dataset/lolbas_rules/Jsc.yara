rule Jsc_exe0_Compile
{
    meta:
        id = "60XgcMAK5PcLrueT9RqsXW"
        fingerprint = "c4f01b81bdd9cbcce6f00abafa77532b27819a095a757acf04b7fc6ccf675967"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile JavaScript code to .exe or .dll format"
        category = "TECHNIQUE"
        technique = "USE JSC.EXE TO COMPILE JAVASCRIPT CODE STORED IN SCRIPTFILE.JS AND OUTPUT SCRIPTFILE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="jsc.exe scriptfile.js" nocase

condition:
    $a0
}

rule Jsc_exe1_Compile
{
    meta:
        id = "57qRRfjWY9rLarb1u4hxwM"
        fingerprint = "6bacc9dc2fef1c35f1faec3e57c80400af1b127cd677fdc2cba25b090cb5270f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile JavaScript code to .exe or .dll format"
        category = "TECHNIQUE"
        technique = "USE JSC.EXE TO COMPILE JAVASCRIPT CODE STORED IN LIBRARY.JS AND OUTPUT LIBRARY.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="jsc.exe /t:library Library.js" nocase

condition:
    $a0
}

