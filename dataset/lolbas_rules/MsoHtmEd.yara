rule MsoHtmEd_exe0_Download
{
    meta:
        id = "VVtJ3J52HV8iNZmsOub1g"
        fingerprint = "004edbcf6be689d7f2b8502132b31d0a8153cd8e5b2da895034d45cfdedb4af5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office component"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="MsoHtmEd.exe " nocase

condition:
    $a0
}

