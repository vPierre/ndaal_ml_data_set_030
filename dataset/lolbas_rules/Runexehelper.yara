rule Runexehelper_exe0_Execute
{
    meta:
        id = "3a1xMULNPI8418vdgA5rDz"
        fingerprint = "1250f5a7a7eb5ae286d9692fa74de14adb4988e6c4685b26987702a48101dbb3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Launcher process"
        category = "TECHNIQUE"
        technique = "LAUNCHES THE SPECIFIED EXE. PREREQUISITES: (1) DIAGTRACK_ACTION_OUTPUT ENVIRONMENT VARIABLE MUST BE SET TO AN EXISTING, WRITABLE FOLDER; (2) RUNEXEWITHARGS_OUTPUT.TXT FILE CANNOT EXIST IN THE FOLDER INDICATED BY THE VARIABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="runexehelper.exe c:\\windows\\system32\\calc.exe" nocase

condition:
    $a0
}

