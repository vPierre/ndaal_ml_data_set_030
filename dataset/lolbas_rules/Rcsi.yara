rule rcsi_exe0_Execute
{
    meta:
        id = "7KGD9L5Fx0LQ1PgLZ5A1VW"
        fingerprint = "979008b1556297cb6cd91891cd1832e924d1043535678cb9ff3203d186ecad45"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Non-Interactive command line inerface included with Visual Studio."
        category = "TECHNIQUE"
        technique = "USE EMBEDDED C# WITHIN THE CSX SCRIPT TO EXECUTE THE CODE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rcsi.exe bypass.csx" nocase

condition:
    $a0
}

rule rcsi_exe1_AWL_Bypass
{
    meta:
        id = "1NlwODOm3EuWPCxbhTnMC4"
        fingerprint = "979008b1556297cb6cd91891cd1832e924d1043535678cb9ff3203d186ecad45"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Non-Interactive command line inerface included with Visual Studio."
        category = "TECHNIQUE"
        technique = "USE EMBEDDED C# WITHIN THE CSX SCRIPT TO EXECUTE THE CODE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rcsi.exe bypass.csx" nocase

condition:
    $a0
}

