rule Stordiag_exe0_Execute
{
    meta:
        id = "5yY4Db3WuDoAIxbYZXEsOt"
        fingerprint = "b1dd6e4f6e891bd3d40d40399bd55d83c44bddab534d32f633af40f2b3b9b5b0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Storage diagnostic tool"
        category = "TECHNIQUE"
        technique = "ONCE EXECUTED, STORDIAG.EXE WILL EXECUTE SCHTASKS.EXE SYSTEMINFO.EXE AND FLTMC.EXE - IF STORDIAG.EXE IS COPIED TO A FOLDER AND AN ARBITRARY EXECUTABLE IS RENAMED TO ONE OF THESE NAMES, STORDIAG.EXE WILL EXECUTE IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="stordiag.exe" nocase

condition:
    $a0
}

