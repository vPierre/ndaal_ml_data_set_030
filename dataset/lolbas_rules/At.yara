rule At_exe0_Execute
{
    meta:
        id = "7cPN3NmsoIybo6L4yaAJFd"
        fingerprint = "899ef81ef0dbe33c594c17f21e50048289208994e276d9dfab1fc38c08347875"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Schedule periodic tasks"
        category = "TECHNIQUE"
        technique = "CREATE A RECURRING TASK TO EXECUTE EVERY DAY AT A SPECIFIC TIME."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="C:\\Windows\\System32\\at.exe 09:00 /interactive /every:m,t,w,th,f,s,su C:\\Windows\\System32\\revshell.exe" nocase

condition:
    $a0
}

