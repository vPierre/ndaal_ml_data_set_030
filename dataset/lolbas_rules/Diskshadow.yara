rule Diskshadow_exe0_Dump
{
    meta:
        id = "3SazHZarOjbyjRb3K1LUF4"
        fingerprint = "5986fcc94f001e2b542f86d6250fea893b08a2fbc30875849daa8a23c294490f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Diskshadow.exe is a tool that exposes the functionality offered by the volume shadow copy Service (VSS)."
        category = "TECHNIQUE"
        technique = "EXECUTE COMMANDS USING DISKSHADOW.EXE FROM A PREPARED DISKSHADOW SCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="diskshadow.exe /s c:\\test\\diskshadow.txt" nocase

condition:
    $a0
}

rule Diskshadow_exe1_Execute
{
    meta:
        id = "4d8AvJYyE7fGTQxSTyceDQ"
        fingerprint = "a525dbfa4d9149424b2ed158adea706aa5e34078013d303d45d684d6dbd0221c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Diskshadow.exe is a tool that exposes the functionality offered by the volume shadow copy Service (VSS)."
        category = "TECHNIQUE"
        technique = "EXECUTE COMMANDS USING DISKSHADOW.EXE TO SPAWN CHILD PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="diskshadow> exec calc.exe" nocase

condition:
    $a0
}

