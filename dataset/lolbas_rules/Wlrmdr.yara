rule Wlrmdr_exe0_Execute
{
    meta:
        id = "HQWqyCH2Z0jVfIuWpL14u"
        fingerprint = "124454a3d4c7d69758f03d6197ae0a6c23fe823bc38248d312e93284719fbf30"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Logon Reminder executable"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH WLRMDR.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wlrmdr.exe -s 3600 -f 0 -t _ -m _ -a 11 -u calc.exe" nocase

condition:
    $a0
}

