rule te_exe0_Execute
{
    meta:
        id = "4mkGxGQm2W0JTcGEQDWvKR"
        fingerprint = "19c7f106cfa106f6d297cee648644de9c1f016a52f9fff2524e5428cc9ec9002"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Testing tool included with Microsoft Test Authoring and Execution Framework (TAEF)."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLETS (E.G. VBSCRIPT) BY CALLING A WINDOWS SCRIPT COMPONENT (WSC) FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="te.exe bypass.wsc" nocase

condition:
    $a0
}

