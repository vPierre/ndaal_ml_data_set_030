rule VSIISExeLauncher_exe0_Execute
{
    meta:
        id = "daOzd7PcwI4oTB5lVl3dS"
        fingerprint = "840d629db417e220683d424dc7de0207f1b67f0df44c3973b7f8f866b181453d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary will execute specified binary. Part of VS/VScode installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL EXECUTE OTHER BINARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="VSIISExeLauncher.exe -p [PATH_TO_BIN] -a \"argument here\"" nocase

condition:
    $a0
}

