rule Register_cimprovider_exe0_Execute
{
    meta:
        id = "7Ntr1qVhrfgrtRnt7MsvhL"
        fingerprint = "b28d7adbf19bae830ab0f0a9822a83656d29bd6ba355384361eb4994e004dfe5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to register new wmi providers"
        category = "TECHNIQUE"
        technique = "LOAD THE TARGET .DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Register-cimprovider -path \"C:\\folder\\evil.dll\"" nocase

condition:
    $a0
}

