rule DumpMinitool_exe0_Dump
{
    meta:
        id = "iJeSrBozpIla7mBkkYcaq"
        fingerprint = "1a5e608be4e207fd948ff88001b1f46a3cfd1bec55f11965e74fd0f052b4aa43"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Dump tool part Visual Studio 2022"
        category = "TECHNIQUE"
        technique = "CREATES A MEMORY DUMP OF THE LSASS PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="DumpMinitool.exe --file c:\\users\\mr.d0x\\dump.txt --processId 1132 --dumpType Full" nocase

condition:
    $a0
}

