rule Ieframe_dll0_Execute
{
    meta:
        id = "53qRjb8scADBwlt5ZR3TNX"
        fingerprint = "7f645686bf2263b3e6d02bf7ce7f15906f8e461b5af20281874038dbe713573e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Browser DLL for translating HTML code."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD VIA PROXY THROUGH A(N) URL (INFORMATION) FILE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe ieframe.dll,OpenURL \"C:\\test\\calc.url\"" nocase

condition:
    $a0
}

