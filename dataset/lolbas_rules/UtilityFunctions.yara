rule UtilityFunctions_ps10_Execute
{
    meta:
        id = "1s4GoK1AcNdylrnH1FNpM2"
        fingerprint = "ccf227ff46eba8d3847afc4fa237a68fd409fb099f6aff7a4d1a6bddc1ce8455"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "PowerShell Diagnostic Script"
        category = "TECHNIQUE"
        technique = "PROXY EXECUTE MANAGED DLL WITH POWERSHELL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="powershell.exe -ep bypass -command \"set-location -path c:\\windows\\diagnostics\\system\\networking; import-module .\\UtilityFunctions.ps1; RegSnapin ..\\..\\..\\..\\temp\\unsigned.dll;[Program.Class]::Main()\"" nocase

condition:
    $a0
}

