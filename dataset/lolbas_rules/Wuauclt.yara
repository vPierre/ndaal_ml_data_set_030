rule wuauclt_exe0_Execute
{
    meta:
        id = "8sBZmWpNRJVHoQPkYeeVU"
        fingerprint = "47aeb879cfe043c3afa763ba78b8fd79da0c906a967a4aa81420a3722c5c7e68"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Update Client"
        category = "TECHNIQUE"
        technique = "FULL_PATH_TO_DLL WOULD BE THE ABOSOLUTE PATH TO .DLL FILE AND WOULD EXECUTE CODE ON ATTACH."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wuauclt.exe /UpdateDeploymentProvider Full_Path_To_DLL /RunHandlerComServer" nocase

condition:
    $a0
}

