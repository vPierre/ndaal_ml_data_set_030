rule Dxcap_exe0_Execute
{
    meta:
        id = "7HCJycqIPV1GzjbAU6iHjy"
        fingerprint = "1a794f4c70b5f9cfe960a6a56b12569b37ba6476e389a2a9e3c811d67134ef88"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "DirectX diagnostics/debugger included with Visual Studio."
        category = "TECHNIQUE"
        technique = "LAUNCH NOTEPAD.EXE AS A SUBPROCESS OF DXCAP.EXE. NOTE THAT YOU SHOULD HAVE WRITE PERMISSIONS IN THE CURRENT WORKING DIRECTORY FOR THE COMMAND TO SUCCEED; ALTERNATIVELY, ADD '-FILE C:\\PATH\\TO\\WRITABLE\\LOCATION.EXT' AS FIRST ARGUMENT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Dxcap.exe -c C:\\Windows\\System32\\notepad.exe" nocase

condition:
    $a0
}

