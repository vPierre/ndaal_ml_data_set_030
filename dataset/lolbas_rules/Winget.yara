rule winget_exe0_Execute
{
    meta:
        id = "31qJoUza7tjj5NJfmwJumT"
        fingerprint = "c96813df30877b71dc6291861fc777cd9e4d2654bb474b33ba3167d60fa3d393"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Package Manager tool"
        category = "TECHNIQUE"
        technique = "DOWNLOADS A FILE FROM THE WEB ADDRESS SPECIFIED IN MANIFEST.YML AND EXECUTES IT ON THE SYSTEM. LOCAL MANIFEST SETTING MUST BE ENABLED IN WINGET FOR IT TO WORK: \"WINGET SETTINGS --ENABLE LOCALMANIFESTFILES\""
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="winget.exe install --manifest manifest.yml" nocase

condition:
    $a0
}

