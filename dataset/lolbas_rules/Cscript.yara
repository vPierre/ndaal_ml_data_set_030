rule Cscript_exe0_ADS
{
    meta:
        id = "OcyesQO0ThRJlfpWuFnfA"
        fingerprint = "181dbdf97174f6e1bc81b350a2800ffe29b16c8dc5586e32b8384d99c5d7a0e0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used to execute scripts in Windows"
        category = "TECHNIQUE"
        technique = "USE CSCRIPT.EXE TO EXECTUTE A VISUAL BASIC SCRIPT STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cscript //e:vbscript c:\\ads\\file.txt:script.vbs" nocase

condition:
    $a0
}

