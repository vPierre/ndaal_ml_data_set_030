rule Mshta_exe0_Execute
{
    meta:
        id = "7Y8YHbGCY2AsSsCl7g04yI"
        fingerprint = "381c78b2b385bdbea32c013372f4ddabf6356ccbc1b2fb836c9bcdad1ce2da18"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "OPENS THE TARGET .HTA AND EXECUTES EMBEDDED JAVASCRIPT, JSCRIPT, OR VBSCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mshta.exe evilfile.hta" nocase

condition:
    $a0
}

rule Mshta_exe1_Execute
{
    meta:
        id = "7Dit548zWurw21bb2vPIuN"
        fingerprint = "da61d8cf82da6f7973ba2479abf3fa8090a5ee6144f590cd5e8c754d485e578d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "EXECUTES VBSCRIPT SUPPLIED AS A COMMAND LINE ARGUMENT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mshta.exe vbscript:Close(Execute(\"GetObject(\"\"script:" nocase

condition:
    $a0
}

rule Mshta_exe2_Execute
{
    meta:
        id = "7EONMYVLWorfnHWAizONJg"
        fingerprint = "bfe77ce792a0ac174f0459cdb65061acdc860a07e0f3e36a036742b37dc4fdc1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "EXECUTES JAVASCRIPT SUPPLIED AS A COMMAND LINE ARGUMENT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mshta.exe javascript:a=GetObject(\"script:" nocase

condition:
    $a0
}

rule Mshta_exe3_ADS
{
    meta:
        id = "15lvXfkraufkGAZrzjBR2K"
        fingerprint = "8526c6a288a2c9da49d5764333ddfc1e42a95508fa86dc33a56632450410ec05"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "OPENS THE TARGET .HTA AND EXECUTES EMBEDDED JAVASCRIPT, JSCRIPT, OR VBSCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mshta.exe \"C:\\ads\\file.txt:file.hta\"" nocase

condition:
    $a0
}

rule Mshta_exe4_Download
{
    meta:
        id = "7C33mhZCnAZ8iys5d1Q1s5"
        fingerprint = "4f2d9884cd9d1f0a84803fcb744cc9638ac490c2f1acf5841f66939c8b5648d3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="mshta.exe " nocase

condition:
    $a0
}

