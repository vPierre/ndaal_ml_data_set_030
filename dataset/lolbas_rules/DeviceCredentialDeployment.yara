rule DeviceCredentialDeployment_exe0_Conceal
{
    meta:
        id = "49QWo2Qax9IPFhuXtemrGJ"
        fingerprint = "429ffabbd48c953eaa2447779ccb82fadd279b5ab98490b1a6578f13eab16dde"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Device Credential Deployment"
        category = "TECHNIQUE"
        technique = "GRAB THE CONSOLE WINDOW HANDLE AND SET IT TO HIDDEN"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="DeviceCredentialDeployment" nocase

condition:
    $a0
}

