rule Pcwutl_dll0_Execute
{
    meta:
        id = "Z4Rv9ffWE1Gx2PYHmFoqc"
        fingerprint = "8b9e10844d453baddcb485a4c948b2a6183922fca9d0a5b7c0e14710ce631e7b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft HTML Viewer"
        category = "TECHNIQUE"
        technique = "LAUNCH EXECUTABLE BY CALLING THE LAUNCHAPPLICATION FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe pcwutl.dll,LaunchApplication calc.exe" nocase

condition:
    $a0
}

