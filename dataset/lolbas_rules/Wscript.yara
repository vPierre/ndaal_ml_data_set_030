rule Wscript_exe0_ADS
{
    meta:
        id = "1F5DtazMr8bcaZCeHouwG3"
        fingerprint = "438345c8f86a1d0186dbb9008f9a2d263a77f487d5e8ee5ac338841bd42a5eb1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute scripts"
        category = "TECHNIQUE"
        technique = "EXECUTE SCRIPT STORED IN AN ALTERNATE DATA STREAM"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="wscript //e:vbscript c:\\ads\\file.txt:script.vbs" nocase

condition:
    $a0
}

rule Wscript_exe1_ADS
{
    meta:
        id = "73a6l9eQisSKHqYlRzfP3J"
        fingerprint = "83868d8561bd8b00c064ee6692daff01dc56cf4d388b355c2972c33cee128067"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute scripts"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND EXECUTE SCRIPT STORED IN AN ALTERNATE DATA STREAM"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="echo GetObject(\"script: > %temp%\\test.txt:hi.js && wscript.exe %temp%\\test.txt:hi.js" nocase

condition:
    $a0
}

