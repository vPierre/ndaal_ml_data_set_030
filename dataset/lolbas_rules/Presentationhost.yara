rule Presentationhost_exe0_Execute
{
    meta:
        id = "xkjxPtNU8Zy9dc1a4E5GA"
        fingerprint = "b6a51f1151a00899dc9601f4f41b51fb1887f306c09d7b356cf819857322e440"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File is used for executing Browser applications"
        category = "TECHNIQUE"
        technique = "EXECUTES THE TARGET XAML BROWSER APPLICATION (XBAP) FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Presentationhost.exe C:\\temp\\Evil.xbap" nocase

condition:
    $a0
}

rule Presentationhost_exe1_Download
{
    meta:
        id = "4NVJwAGpqsoZ1svXd0J80j"
        fingerprint = "ebe30cd031bc11635d34a4f8694a8e28661a77a1d211b24c09c98cf760e30537"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File is used for executing Browser applications"
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Presentationhost.exe " nocase

condition:
    $a0
}

