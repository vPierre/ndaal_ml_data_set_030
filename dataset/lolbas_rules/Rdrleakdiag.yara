rule rdrleakdiag_exe0_Dump
{
    meta:
        id = "3K7phZujtpYGcX3mIY9Akd"
        fingerprint = "3c7986be980d5dfdad1a78c1d1a47827392f82031a1159b0a03c8c88d82e92ae"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows resource leak diagnostic tool"
        category = "TECHNIQUE"
        technique = "DUMP PROCESS BY PID AND CREATE A DUMP FILE (CREATES FILES CALLED MINIDUMP_<PID>.DMP AND RESULTS_<PID>.HLK)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rdrleakdiag.exe /p 940 /o c:\\evil /fullmemdmp /wait 1" nocase

condition:
    $a0
}

rule rdrleakdiag_exe1_Dump
{
    meta:
        id = "45lbat0dig4auempzyJleX"
        fingerprint = "406db65ebb2dcbbc386a14ce7a7b11a92d19f3c6fc39e63c430653578788bcc8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows resource leak diagnostic tool"
        category = "TECHNIQUE"
        technique = "DUMP LSASS PROCESS BY PID AND CREATE A DUMP FILE (CREATES FILES CALLED MINIDUMP_<PID>.DMP AND RESULTS_<PID>.HLK)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rdrleakdiag.exe /p 832 /o c:\\evil /fullmemdmp /wait 1" nocase

condition:
    $a0
}

rule rdrleakdiag_exe2_Dump
{
    meta:
        id = "4FbGHzAknMfyQaCMMU90ET"
        fingerprint = "3f856eb5d2c11a4df089472f1e406894abc9fc81c3c3d4b62b43e07252554230"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows resource leak diagnostic tool"
        category = "TECHNIQUE"
        technique = "AFTER DUMPING A PROCESS USING /WAIT 1, SUBSEQUENT DUMPS MUST USE /SNAP (CREATES FILES CALLED MINIDUMP_<PID>.DMP AND RESULTS_<PID>.HLK)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rdrleakdiag.exe /p 832 /o c:\\evil /fullmemdmp /snap" nocase

condition:
    $a0
}

