rule OpenConsole_exe0_Execute
{
    meta:
        id = "5NHsSOioxwpKnAT31zilS6"
        fingerprint = "8ec61058df8564e45096e9cd4558fe5c8918af3f156006829eb6d7e1371119f0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Console Window host for Windows Terminal"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC WITH OPENCONSOLE.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="OpenConsole.exe calc" nocase

condition:
    $a0
}

