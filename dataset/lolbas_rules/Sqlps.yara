rule Sqlps_exe0_Execute
{
    meta:
        id = "2PklGgjVavX2Nd3j75GRjx"
        fingerprint = "d01f7ee98c224c50b5f20dec483f5ada7aed5f32a851e13f73382e83f1f355be"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Tool included with Microsoft SQL Server that loads SQL Server cmdlets. Microsoft SQL Server\\100 and 110 are Powershell v2. Microsoft SQL Server\\120 and 130 are Powershell version 4. Replaced by SQLToolsPS.exe in SQL Server 2016, but will be included with installation for compatability reasons."
        category = "TECHNIQUE"
        technique = "RUN A SQL SERVER POWERSHELL MINI-CONSOLE WITHOUT MODULE AND SCRIPTBLOCK LOGGING."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Sqlps.exe -noprofile" nocase

condition:
    $a0
}

