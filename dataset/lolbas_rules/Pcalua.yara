rule Pcalua_exe0_Execute
{
    meta:
        id = "37CdHskRvPJREran0wnFTn"
        fingerprint = "542a08f742f29947d446cb755cad8e01d2c15f2bdd3ca1e0674c29ae20d59639"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Assistant"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .EXE USING THE PROGRAM COMPATIBILITY ASSISTANT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pcalua.exe -a calc.exe" nocase

condition:
    $a0
}

rule Pcalua_exe1_Execute
{
    meta:
        id = "4LEX1cpeTXMDjRNoBmYLgx"
        fingerprint = "ca450b0c87e9fa05b55b637c74c50753a50687eb9ff7f3a5cb639aff2f0f1cc3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Assistant"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .DLL FILE WITH THE PROGRAM COMPATIBILTY ASSISTANT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pcalua.exe -a \\\\server\\payload.dll" nocase

condition:
    $a0
}

rule Pcalua_exe2_Execute
{
    meta:
        id = "5iNCN39oyBwTkyAHrC0xY0"
        fingerprint = "dd86579bd4f3cc6da232e6c42c16820c82f6b5f2683a1b79f95a7090154bf740"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Assistant"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .CPL FILE WITH THE PROGRAM COMPATIBILITY ASSISTANT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pcalua.exe -a C:\\Windows\\system32\\javacpl.cpl -c Java" nocase

condition:
    $a0
}

