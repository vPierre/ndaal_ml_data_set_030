rule PrintBrm_exe0_Download
{
    meta:
        id = "2K16mVLIPwnE3GlovtRqO3"
        fingerprint = "5368dd02564ba31a3a81f8750d06a0ad488ef6c91b832c5d84d78c2d6baebeb4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Printer Migration Command-Line Tool"
        category = "TECHNIQUE"
        technique = "CREATE A ZIP FILE FROM A FOLDER IN A REMOTE DRIVE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="PrintBrm -b -d \\\\1.2.3.4\\share\\example_folder -f C:\\Users\\user\\Desktop\\new.zip" nocase

condition:
    $a0
}

rule PrintBrm_exe1_ADS
{
    meta:
        id = "4NrS3ZE1lgZmngLRo9Rzjh"
        fingerprint = "f1d7a406fb9b1127dc1c7984ee7f00ec4c77fea2b72f6563222c8330a7170cd1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Printer Migration Command-Line Tool"
        category = "TECHNIQUE"
        technique = "EXTRACT THE CONTENTS OF A ZIP FILE STORED IN AN ALTERNATE DATA STREAM (ADS) AND STORE IT IN A FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="PrintBrm -r -f C:\\Users\\user\\Desktop\\data.txt:hidden.zip -d C:\\Users\\user\\Desktop\\new_folder" nocase

condition:
    $a0
}

