rule Csc_exe0_Compile
{
    meta:
        id = "26IyvCBbWL24qzaT1iHGL0"
        fingerprint = "7d75ce8ab3ac99848bdad30cbb0376e1ed3bc7c20a7a981734d33ed50fa33c01"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile C# code"
        category = "TECHNIQUE"
        technique = "USE CSC.EXE TO COMPILE C# CODE STORED IN FILE.CS AND OUTPUT THE COMPILED VERSION TO MY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="csc.exe -out:My.exe File.cs" nocase

condition:
    $a0
}

rule Csc_exe1_Compile
{
    meta:
        id = "4sinqNoNSw5KhKM4qdzCyT"
        fingerprint = "6dfbab6f752dfa1b24a5c3b0a2acc5862ac6823e0cd90169d050a7c8c5fe7b8a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile C# code"
        category = "TECHNIQUE"
        technique = "USE CSC.EXE TO COMPILE C# CODE STORED IN FILE.CS AND OUTPUT THE COMPILED VERSION TO A DLL FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="csc -target:library File.cs" nocase

condition:
    $a0
}

