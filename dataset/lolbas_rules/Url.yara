rule Url_dll0_Execute
{
    meta:
        id = "zi5BqPkJMJ5IYTVRutGKF"
        fingerprint = "36926c18155ef517618df32a9c3abb3783cf4019d370f7a7a8354a47764c9a49"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH A HTML APPLICATION PAYLOAD BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe url.dll,OpenURL \"C:\\test\\calc.hta\"" nocase

condition:
    $a0
}

rule Url_dll1_Execute
{
    meta:
        id = "4eBg2ZhJlEAYdmDhoo3v52"
        fingerprint = "484d7cf29516ba51e8e89dab481d5e9b6f75b8fb830e0aa28d2b2704f5643567"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD VIA PROXY THROUGH A(N) URL (INFORMATION) FILE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe url.dll,OpenURL \"C:\\test\\calc.url\"" nocase

condition:
    $a0
}

rule Url_dll2_Execute
{
    meta:
        id = "1wRCe0Ss7e179QLC4Dy6RV"
        fingerprint = "8d5f0c04c4efd1540c893b087083f3003a26610a8a822659d76a5f8b5a91e415"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe url.dll,OpenURL file://^C^:^/^W^i^n^d^o^w^s^/^s^y^s^t^e^m^3^2^/^c^a^l^c^.^e^x^e" nocase

condition:
    $a0
}

rule Url_dll3_Execute
{
    meta:
        id = "5tFyZmbfBpqFJ8s7ydFCnb"
        fingerprint = "8264a19bb055de3b50229ec3962ba489555ab57bff59494021a2740881267ba2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING FILEPROTOCOLHANDLER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe url.dll,FileProtocolHandler calc.exe" nocase

condition:
    $a0
}

rule Url_dll4_Execute
{
    meta:
        id = "3qV0iy0TkIK5vRCTZMVeHp"
        fingerprint = "17e0c53ef409ed73969a068a2f0a0aaa56907bf79e3f1fbeb69ac41bfd74fa4f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING FILEPROTOCOLHANDLER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe url.dll,FileProtocolHandler file://^C^:^/^W^i^n^d^o^w^s^/^s^y^s^t^e^m^3^2^/^c^a^l^c^.^e^x^e" nocase

condition:
    $a0
}

rule Url_dll5_Execute
{
    meta:
        id = "6YsYDoO9CVGB1WmWdq12Bk"
        fingerprint = "292ff1f8bf3a5a69c8a77d9636c396d34a3b940f6310965aee5ed1e01932bed4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH A HTML APPLICATION PAYLOAD BY CALLING FILEPROTOCOLHANDLER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe url.dll,FileProtocolHandler file:///C:/test/test.hta" nocase

condition:
    $a0
}

