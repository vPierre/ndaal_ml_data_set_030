rule Microsoft_NodejsTools_PressAnyKey_exe0_Execute
{
    meta:
        id = "4YCWbGQSV7k6WO6i2jzWNS"
        fingerprint = "b6a24e88d0907990c97a85b26cd4a5d3f7b06dd0178fd6c38accae344887ebff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Part of the NodeJS Visual Studio tools."
        category = "TECHNIQUE"
        technique = "LAUNCH CMD.EXE AS A SUBPROCESS OF MICROSOFT.NODEJSTOOLS.PRESSANYKEY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Microsoft.NodejsTools.PressAnyKey.exe normal 1 cmd.exe" nocase

condition:
    $a0
}

