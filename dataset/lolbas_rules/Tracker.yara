rule Tracker_exe0_Execute
{
    meta:
        id = "5gCTujvsun30cV17mvJHYb"
        fingerprint = "432b72452f4823ee94b5b9d75b1d85aa7272813696663872dde239dd2517fe9a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Tool included with Microsoft .Net Framework."
        category = "TECHNIQUE"
        technique = "USE TRACKER.EXE TO PROXY EXECUTION OF AN ARBITRARY DLL INTO ANOTHER PROCESS. SINCE TRACKER.EXE IS ALSO SIGNED IT CAN BE USED TO BYPASS APPLICATION WHITELISTING SOLUTIONS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Tracker.exe /d .\\calc.dll /c C:\\Windows\\write.exe" nocase

condition:
    $a0
}

rule Tracker_exe1_AWL_Bypass
{
    meta:
        id = "15hEY8ONdCb7DLaEo9KXMM"
        fingerprint = "432b72452f4823ee94b5b9d75b1d85aa7272813696663872dde239dd2517fe9a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Tool included with Microsoft .Net Framework."
        category = "TECHNIQUE"
        technique = "USE TRACKER.EXE TO PROXY EXECUTION OF AN ARBITRARY DLL INTO ANOTHER PROCESS. SINCE TRACKER.EXE IS ALSO SIGNED IT CAN BE USED TO BYPASS APPLICATION WHITELISTING SOLUTIONS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Tracker.exe /d .\\calc.dll /c C:\\Windows\\write.exe" nocase

condition:
    $a0
}

