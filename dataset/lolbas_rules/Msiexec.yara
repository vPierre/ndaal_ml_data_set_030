rule Msiexec_exe0_Execute
{
    meta:
        id = "2ipvECZvXMC9wrkyCIg3T2"
        fingerprint = "232b66352f88bf3d39a78ffaec2bf5afb4db5eb9c9666071cd63fb7df11a96bd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "INSTALLS THE TARGET .MSI FILE SILENTLY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msiexec /quiet /i cmd.msi" nocase

condition:
    $a0
}

rule Msiexec_exe1_Execute
{
    meta:
        id = "5UA42bqlZoYFTjqpScEebp"
        fingerprint = "2a2d9bb3dd79083ff6418a9e336e0a992fd7a18cad6fd0c55c6c4407e3cb362c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "INSTALLS THE TARGET REMOTE & RENAMED .MSI FILE SILENTLY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msiexec /q /i " nocase

condition:
    $a0
}

rule Msiexec_exe2_Execute
{
    meta:
        id = "rI2BcCyCyB7zClOIj38F2"
        fingerprint = "28c4cf9ab8a7682bf0f1adb4198239eb3f32a03a0a8928435e016f9741dea71f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "CALLS DLLREGISTERSERVER TO REGISTER THE TARGET DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msiexec /y \"C:\\folder\\evil.dll\"" nocase

condition:
    $a0
}

rule Msiexec_exe3_Execute
{
    meta:
        id = "5AjbCITKgdLHxCn9zux5LY"
        fingerprint = "4fa74a4b3548a9d17b154993685815c3356f31399bf08f25121dc1dcf937c865"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "CALLS DLLREGISTERSERVER TO UN-REGISTER THE TARGET DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msiexec /z \"C:\\folder\\evil.dll\"" nocase

condition:
    $a0
}

