rule Mshtml_dll0_Execute
{
    meta:
        id = "7ev6c08lWjnDwApzdTmqMJ"
        fingerprint = "9000c3785caa6d5a89bcc8b5846af9d5049ac73be25d9a2383c0ceffd5a96dd7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft HTML Viewer"
        category = "TECHNIQUE"
        technique = "INVOKE AN HTML APPLICATION VIA MSHTA.EXE (NOTE: POPS A SECURITY WARNING AND A PRINT DIALOGUE BOX)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe Mshtml.dll,PrintHTML \"C:\\temp\\calc.hta\"" nocase

condition:
    $a0
}

