rule Squirrel_exe0_Download
{
    meta:
        id = "6P1kt1a6T0RakdIJyOpsc1"
        fingerprint = "88dab312b4444b5c02ded5f45f0376091cd4045447a0b6249947bef6f161b4ac"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE AND DOWNLOAD THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="squirrel.exe --download [url to package]" nocase

condition:
    $a0
}

rule Squirrel_exe1_AWL_Bypass
{
    meta:
        id = "1d7wDFgGla3YO8BIrOTD2E"
        fingerprint = "3bd9d03b96451fd5369f4d27409171e6fd54e68e8c16ba9bf8102da0c3b28b89"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="squirrel.exe --update [url to package]" nocase

condition:
    $a0
}

rule Squirrel_exe2_Execute
{
    meta:
        id = "3mEfFI2VboWk0spvjFlaqk"
        fingerprint = "3bd9d03b96451fd5369f4d27409171e6fd54e68e8c16ba9bf8102da0c3b28b89"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="squirrel.exe --update [url to package]" nocase

condition:
    $a0
}

rule Squirrel_exe3_AWL_Bypass
{
    meta:
        id = "1dEeg6jYU8tqQ2eGeaaBzV"
        fingerprint = "6690a9708dbe9a95610a8142de12c207abcf83a6601520d2727b1ccc0d23aa23"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="squirrel.exe --updateRoolback=[url to package]" nocase

condition:
    $a0
}

rule Squirrel_exe4_Execute
{
    meta:
        id = "5sYwvjytINEFNvDftHiXHX"
        fingerprint = "bf832477f4abbcadb3a66b75a3e69a895b3a68e9c6d9c4652cf6c337a2bed7b9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="squirrel.exe --updateRollback=[url to package]" nocase

condition:
    $a0
}

