rule Teams_exe0_Execute
{
    meta:
        id = "34ai3BgBaAUZdONHcmKXDp"
        fingerprint = "b6c8c443047a1f24471dd8017d8c9157ecf1059c50d50ace7120d413190ca8d0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Teams"
        category = "TECHNIQUE"
        technique = "TEAMS SPAWNS CMD.EXE AS A CHILD PROCESS OF TEAMS.EXE AND EXECUTES THE PING COMMAND"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="teams.exe --disable-gpu-sandbox --gpu-launcher=\"C:\\Windows\\system32\\cmd.exe /c ping google.com &&\"" nocase

condition:
    $a0
}

