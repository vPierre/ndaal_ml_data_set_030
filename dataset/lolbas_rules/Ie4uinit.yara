rule Ie4uinit_exe0_Execute
{
    meta:
        id = "3pIGXDoFZwz2XawlAcko1e"
        fingerprint = "8756f435293bed14f4ce999027a20f3ff953ae145f79cef02de4bb684c877907"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Executes commands from a specially prepared ie4uinit.inf file."
        category = "TECHNIQUE"
        technique = "EXECUTES COMMANDS FROM A SPECIALLY PREPARED IE4UINIT.INF FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ie4uinit.exe -BaseSettings" nocase

condition:
    $a0
}

