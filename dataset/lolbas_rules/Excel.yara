rule Excel_exe0_Download
{
    meta:
        id = "7JGuYm3DL4Iafl1CnIHYVr"
        fingerprint = "8e0e20774c7c946cf3caac4ced5e9333b16d2451c242400e649543d5dea5f9a8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Excel.exe " nocase

condition:
    $a0
}

