rule OneDriveStandaloneUpdater_exe0_Download
{
    meta:
        id = "im6PYG2CaXrDuj9p7yjbz"
        fingerprint = "7530d32672ab58658120e24d20a3eee4033e29add711937bd01b5eabc6d9e4d2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "OneDrive Standalone Updater"
        category = "TECHNIQUE"
        technique = "DOWNLOAD A FILE FROM THE WEB ADDRESS SPECIFIED IN HKCU\\SOFTWARE\\MICROSOFT\\ONEDRIVE\\UPDATEOFFICECONFIG\\UPDATERINGSETTINGURLFROMOC. ODSUUPDATEXMLURLFROMOC AND UPDATEXMLURLFROMOC MUST BE EQUAL TO NON-EMPTY STRING VALUES IN THAT SAME REGISTRY KEY. UPDATEOFFICECONFIGTIMESTAMP IS A UNIX EPOCH TIME WHICH MUST BE SET TO A LARGE QWORD SUCH AS 99999999999 (IN DECIMAL) TO INDICATE THE URL CACHE IS GOOD. THE DOWNLOADED FILE WILL BE IN %LOCALAPPDATA%\\ONEDRIVE\\STANDALONEUPDATER\\PRESIGNINSETTINGSCONFIG.JSON"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="OneDriveStandaloneUpdater" nocase

condition:
    $a0
}

