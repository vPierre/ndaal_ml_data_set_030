rule Msdt_exe0_Execute
{
    meta:
        id = "7RBwDEkLevnQ2FzokfO7oG"
        fingerprint = "67ae55844d67079f657f60c144c760bda3da6f7b0bc11cbe5a291d099f020ce5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft diagnostics tool"
        category = "TECHNIQUE"
        technique = "EXECUTES THE MICROSOFT DIAGNOSTICS TOOL AND EXECUTES THE MALICIOUS .MSI REFERENCED IN THE PCW8E57.XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msdt.exe -path C:\\WINDOWS\\diagnostics\\index\\PCWDiagnostic.xml -af C:\\PCW8E57.xml /skip TRUE" nocase

condition:
    $a0
}

rule Msdt_exe1_AWL_Bypass
{
    meta:
        id = "ugy7D7eSdBrIn2uLddJpx"
        fingerprint = "67ae55844d67079f657f60c144c760bda3da6f7b0bc11cbe5a291d099f020ce5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft diagnostics tool"
        category = "TECHNIQUE"
        technique = "EXECUTES THE MICROSOFT DIAGNOSTICS TOOL AND EXECUTES THE MALICIOUS .MSI REFERENCED IN THE PCW8E57.XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msdt.exe -path C:\\WINDOWS\\diagnostics\\index\\PCWDiagnostic.xml -af C:\\PCW8E57.xml /skip TRUE" nocase

condition:
    $a0
}

rule Msdt_exe2_AWL_Bypass
{
    meta:
        id = "2aQfDGTXIdYtkaek6WRODC"
        fingerprint = "5606290b5e75b433aab406e1060a37f161fd3fcd1b65e9e7e3929d517134bd68"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft diagnostics tool"
        category = "TECHNIQUE"
        technique = "EXECUTES ARBITRARY COMMANDS USING THE MICROSOFT DIAGNOSTICS TOOL AND LEVERAGING THE \"PCWDIAGNOSTIC\" MODULE (CVE-2022-30190). NOTE THAT THIS SPECIFIC TECHNIQUE WILL NOT WORK ON A PATCHED SYSTEM WITH THE JUNE 2022 WINDOWS SECURITY UPDATE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msdt.exe /id PCWDiagnostic /skip force /param \"IT_LaunchMethod=ContextMenu IT_BrowseForFile=/../../$(calc).exe\"" nocase

condition:
    $a0
}

