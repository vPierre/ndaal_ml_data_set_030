rule Devtoolslauncher_exe0_Execute
{
    meta:
        id = "wsGbiU4ZU9KagwLVzlZRz"
        fingerprint = "10754c7c9497b0ae34ef8b6cb4d4919e051f3ba01e1114c7957d4b9fe82d53bd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary will execute specified binary. Part of VS/VScode installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL EXECUTE OTHER BINARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="devtoolslauncher.exe LaunchForDeploy [PATH_TO_BIN] \"argument here\" test" nocase

condition:
    $a0
}

rule Devtoolslauncher_exe1_Execute
{
    meta:
        id = "2iMWjEuRVfSHcaOiWGYRy3"
        fingerprint = "815c147fb368844c4e5b2b6e8d850c75ad336126ab1acee262243b1e70b4e0ef"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary will execute specified binary. Part of VS/VScode installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL EXECUTE OTHER BINARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="devtoolslauncher.exe LaunchForDebug [PATH_TO_BIN] \"argument here\" test" nocase

condition:
    $a0
}

