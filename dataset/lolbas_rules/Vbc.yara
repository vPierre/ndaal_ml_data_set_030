rule vbc_exe0_Compile
{
    meta:
        id = "6G973ZityXW2CZg7RJhTeu"
        fingerprint = "f5fcc632c840a0b779aee860c03aa3e40ff8d4a63383acc2c02130cdf5a14b44"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used for compile vbs code"
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE VISUAL BASIC CODE TO AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="vbc.exe /target:exe c:\\temp\\vbs\\run.vb" nocase

condition:
    $a0
}

rule vbc_exe1_Compile
{
    meta:
        id = "5Q48PGzMgICO94dN6EIwuv"
        fingerprint = "ae2fc51d7a2e4b258f2a20eb453f569f08e0a0743395608d580a9981095746e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used for compile vbs code"
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE VISUAL BASIC CODE TO AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="vbc -reference:Microsoft.VisualBasic.dll c:\\temp\\vbs\\run.vb" nocase

condition:
    $a0
}

