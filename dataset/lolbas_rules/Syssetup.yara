rule Syssetup_dll0_AWL_Bypass
{
    meta:
        id = "56VhtTR9hAAT03sdRBHRoD"
        fingerprint = "e0db248d7afbd1106c846edd53c78babfe86a4ce37e07d4172d5969b976ace87"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows NT System Setup"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe syssetup.dll,SetupInfObjectInstallAction DefaultInstall 128 c:\\test\\shady.inf" nocase

condition:
    $a0
}

rule Syssetup_dll1_Execute
{
    meta:
        id = "4lPRTRmCQ5tb2iMDoqz5rj"
        fingerprint = "1d4c21efe9a6107e3f79b55b8f448d16e8d4d8ab29c8a23064d2d1c204fc6f06"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows NT System Setup"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE FILE VIA THE SETUPINFOBJECTINSTALLACTION FUNCTION AND .INF FILE SECTION DIRECTIVE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32 syssetup.dll,SetupInfObjectInstallAction DefaultInstall 128 c:\\temp\\something.inf" nocase

condition:
    $a0
}

