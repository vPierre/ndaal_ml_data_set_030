rule SyncAppvPublishingServer_exe0_Execute
{
    meta:
        id = "2fqHtW2eSjj3PgMgD1Uk6X"
        fingerprint = "73877443572b118cf8847aaad2a76b6e7fe00b8424df5d713976b59531d63a6f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by App-v to get App-v server lists"
        category = "TECHNIQUE"
        technique = "EXAMPLE COMMAND ON HOW INJECT POWERSHELL CODE INTO THE PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="SyncAppvPublishingServer.exe \"n;(New-Object Net.WebClient).DownloadString(' | IEX\"" nocase

condition:
    $a0
}

rule Syncappvpublishingserver_vbs0_Execute
{
    meta:
        id = "68CQea25msAeOiU3JaMte4"
        fingerprint = "e117ddd3641329c74d71c1ff0ab33beda7d5c6392d2ae62cb74a6134471986aa"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used related to app-v and publishing server"
        category = "TECHNIQUE"
        technique = "INJECT POWERSHELL SCRIPT CODE WITH THE PROVIDED ARGUMENTS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="SyncAppvPublishingServer.vbs \"n;((New-Object Net.WebClient).DownloadString(' | IEX\"" nocase

condition:
    $a0
}

