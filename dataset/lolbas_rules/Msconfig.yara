rule Msconfig_exe0_Execute
{
    meta:
        id = "1YcjJxNfj0pvoiFc1SJlxA"
        fingerprint = "b1d57e94727258fab1047960286ac7c8414e4422fe96210c63abcb677824d0d7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "MSConfig is a troubleshooting tool which is used to temporarily disable or re-enable software, device drivers or Windows services that run during startup process to help the user determine the cause of a problem with Windows"
        category = "TECHNIQUE"
        technique = "EXECUTES COMMAND EMBEDED IN CRAFTED C:\\WINDOWS\\SYSTEM32\\MSCFGTLC.XML."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Msconfig.exe -5" nocase

condition:
    $a0
}

