rule Pubprn_vbs0_Execute
{
    meta:
        id = "1cLfyWxCeOC7nM1DpUuUg0"
        fingerprint = "baadb15805b19f55384f7c21eff10c270ff1848a1702ddc0833e7e5bc06cc3be"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Proxy execution with Pubprn.vbs"
        category = "TECHNIQUE"
        technique = "SET THE 2ND VARIABLE WITH A SCRIPT COM MONIKER TO PERFORM WINDOWS SCRIPT HOST (WSH) INJECTION"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="pubprn.vbs 127.0.0.1 script:" nocase

condition:
    $a0
}

