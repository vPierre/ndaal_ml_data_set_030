rule Msbuild_exe0_AWL_Bypass
{
    meta:
        id = "f7B7iaYwdr7uDaIScXGUI"
        fingerprint = "dbe2c96fdb243cf8620fd15270972ad5f23ed00cf4831c2c640a0cee1ce12e53"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "BUILD AND EXECUTE A C# PROJECT STORED IN THE TARGET XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msbuild.exe pshell.xml" nocase

condition:
    $a0
}

rule Msbuild_exe1_Execute
{
    meta:
        id = "64OiJnZzSqMkZYnaAPP7oG"
        fingerprint = "31b7117141b6650ab16ec1ef012acb8cc28c0146b8875115b15b29a991aa7c8b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "BUILD AND EXECUTE A C# PROJECT STORED IN THE TARGET CSPROJ FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msbuild.exe project.csproj" nocase

condition:
    $a0
}

rule Msbuild_exe2_Execute
{
    meta:
        id = "1tQb0o0SqEcnurLB0JzsOe"
        fingerprint = "2abe63df9ee4c118ab4f776ef50286ab7bd35eef16a28026b452443dbad67570"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "EXECUTES GENERATED LOGGER DLL FILE WITH TARGETLOGGER EXPORT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msbuild.exe /logger:TargetLogger,C:\\Loggers\\TargetLogger.dll;MyParameters,Foo" nocase

condition:
    $a0
}

rule Msbuild_exe3_Execute
{
    meta:
        id = "4vEZmbqpBtPlRiyx0Dasrm"
        fingerprint = "1fa5906b4400869619603bd3462ebb8d1dc05fd483ee614c53c682ebf5edece1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "EXECUTE JSCRIPT/VBSCRIPT CODE THROUGH XML/XSL TRANSFORMATION. REQUIRES VISUAL STUDIO MSBUILD V14.0+."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msbuild.exe project.proj" nocase

condition:
    $a0
}

rule Msbuild_exe4_Execute
{
    meta:
        id = "5QRfTAfOEuXHuGIjzYvHQ6"
        fingerprint = "f1f98789c238d69f2005aaa3bcc62f16ec9ba1c008ce6c7c4b9788775f1a013b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "BY PUTTING ANY VALID MSBUILD.EXE COMMAND-LINE OPTIONS IN AN RSP FILE AND CALLING IT AS ABOVE WILL INTERPRET THE OPTIONS AS IF THEY WERE PASSED ON THE COMMAND LINE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="msbuild.exe @sample.rsp" nocase

condition:
    $a0
}

