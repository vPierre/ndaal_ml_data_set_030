rule CL_Mutexverifiers_ps10_Execute
{
    meta:
        id = "4rm7drnYwVzl5UYUP6yQDZ"
        fingerprint = "7323386fce4327fa9e0c1ba85d7bf9d1068dcc33a764bfb22031c203b80a963a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Proxy execution with CL_Mutexverifiers.ps1"
        category = "TECHNIQUE"
        technique = "IMPORT THE POWERSHELL DIAGNOSTIC CL_MUTEXVERIFIERS SCRIPT AND CALL RUNAFTERCANCELPROCESS TO LAUNCH AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0=". C:\\Windows\\diagnostics\\system\\AERO\\CL_Mutexverifiers.ps1   \\nrunAfterCancelProcess calc.ps1" nocase

condition:
    $a0
}

