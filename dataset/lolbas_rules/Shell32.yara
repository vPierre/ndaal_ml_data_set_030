rule Shell32_dll0_Execute
{
    meta:
        id = "7eyIVCfVbfpRGZSgvqYtqQ"
        fingerprint = "c92497cfddd816146a46e4979251261ae4dad899115c821e2740b57931819b9a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Shell Common Dll"
        category = "TECHNIQUE"
        technique = "LAUNCH A DLL PAYLOAD BY CALLING THE CONTROL_RUNDLL FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe shell32.dll,Control_RunDLL c:\\path\\to\\payload.dll" nocase

condition:
    $a0
}

rule Shell32_dll1_Execute
{
    meta:
        id = "3hi74hjghSJRbBZkkFaLaW"
        fingerprint = "4347290997dbca7828ff78a97593f633b439c90b66cf5e0b91d6d6b0d3eec24c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Shell Common Dll"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING THE SHELLEXEC_RUNDLL FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe shell32.dll,ShellExec_RunDLL beacon.exe" nocase

condition:
    $a0
}

rule Shell32_dll2_Execute
{
    meta:
        id = "4rRwLYbF8p2pTCk37iHufy"
        fingerprint = "28ec373661f297549257d132e2ffdae1d1f45a72e9b5671f951f5a7edba7ff9e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Shell Common Dll"
        category = "TECHNIQUE"
        technique = "LAUNCH COMMAND LINE BY CALLING THE SHELLEXEC_RUNDLL FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32 SHELL32.DLL,ShellExec_RunDLL \"cmd.exe\" \"/c echo hi\"" nocase

condition:
    $a0
}

