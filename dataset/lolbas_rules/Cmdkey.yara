rule Cmdkey_exe0_Credentials
{
    meta:
        id = "2l6KREH42wSVq2KHhgjpJd"
        fingerprint = "353cd8e7744396dc6df4e9f4d62c5cb3159a36666373b7b2ba4f6719faa8e5f4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "creates, lists, and deletes stored user names and passwords or credentials."
        category = "TECHNIQUE"
        technique = "LIST CACHED CREDENTIALS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="cmdkey /list" nocase

condition:
    $a0
}

