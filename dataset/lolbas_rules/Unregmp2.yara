rule Unregmp2_exe0_Execute
{
    meta:
        id = "2BtKLOSohsPMnMmnZ3dUxz"
        fingerprint = "5626c58eb288ac26de3234d344512590e63c24b00f534fb546821f917941b72e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Windows Media Player Setup Utility"
        category = "TECHNIQUE"
        technique = "ALLOWS AN ATTACKER TO COPY A TARGET BINARY TO A CONTROLLED DIRECTORY AND MODIFY THE 'PROGRAMW6432' ENVIRONMENT VARIABLE TO POINT TO THAT CONTROLLED DIRECTORY, THEN EXECUTE 'UNREGMP2.EXE' WITH ARGUMENT '/HIDEWMP' WHICH WILL SPAWN A PROCESS AT THE HIJACKED PATH '%PROGRAMW6432%\\WMPNSCFG.EXE'."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rmdir %temp%\\lolbin /s /q 2>nul & mkdir \"%temp%\\lolbin\\Windows Media Player\" & copy C:\\Windows\\System32\\calc.exe \"%temp%\\lolbin\\Windows Media Player\\wmpnscfg.exe\" >nul && cmd /V /C \"set \"ProgramW6432=%temp%\\lolbin\" && unregmp2.exe /HideWMP\"" nocase

condition:
    $a0
}

