rule Forfiles_exe0_Execute
{
    meta:
        id = "6DgNBZUvQpw4jfbo3dj78n"
        fingerprint = "1c0126e7303d16883c36295a6cdd03e55dcf9fc7dcea9d664973a13af035b3a2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Selects and executes a command on a file or set of files. This command is useful for batch processing."
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE SINCE THERE IS A MATCH FOR NOTEPAD.EXE IN THE C:\\WINDOWS\\SYSTEM32 FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="forfiles /p c:\\windows\\system32 /m notepad.exe /c calc.exe" nocase

condition:
    $a0
}

rule Forfiles_exe1_ADS
{
    meta:
        id = "4tEqlcmKXAv5J1yq2ZDzJC"
        fingerprint = "280152c40aa99039f955589e4f4066c06429f369b47d1c6349bc916815bc75d8"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Selects and executes a command on a file or set of files. This command is useful for batch processing."
        category = "TECHNIQUE"
        technique = "EXECUTES THE EVIL.EXE ALTERNATE DATA STREAM (AD) SINCE THERE IS A MATCH FOR NOTEPAD.EXE IN THE C:\\WINDOWS\\SYSTEM32 FOLDER."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="forfiles /p c:\\windows\\system32 /m notepad.exe /c \"c:\\folder\\normal.dll:evil.exe\"" nocase

condition:
    $a0
}

