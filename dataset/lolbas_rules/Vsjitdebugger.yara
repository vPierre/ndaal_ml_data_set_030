rule vsjitdebugger_exe0_Execute
{
    meta:
        id = "1PC2FBf4HK4UC1irznzgEU"
        fingerprint = "6100851652e191c045c830caaee4a15bfbfe7b3662272d91f61636b9de4a4e4a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Just-In-Time (JIT) debugger included with Visual Studio"
        category = "TECHNIQUE"
        technique = "EXECUTES CALC.EXE AS A SUBPROCESS OF VSJITDEBUGGER.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Vsjitdebugger.exe calc.exe" nocase

condition:
    $a0
}

