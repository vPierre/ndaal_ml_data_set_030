rule Expand_exe0_Download
{
    meta:
        id = "yHH3qtyyHg94oap88DlDo"
        fingerprint = "4b3202a82b23a1f2e786dab2a752e2001b92877962e53de1b73449daabd4f2d5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that expands one or more compressed files"
        category = "TECHNIQUE"
        technique = "COPIES SOURCE FILE TO DESTINATION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="expand \\\\webdav\\folder\\file.bat c:\\ADS\\file.bat" nocase

condition:
    $a0
}

rule Expand_exe1_Copy
{
    meta:
        id = "5bYnaRHLebWTSDTFneLjXP"
        fingerprint = "9ed77cc4944beca8fcdb601ce1f2818c383cbb7dff0d4bf7dfe5468f9436bade"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that expands one or more compressed files"
        category = "TECHNIQUE"
        technique = "COPIES SOURCE FILE TO DESTINATION."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="expand c:\\ADS\\file1.bat c:\\ADS\\file2.bat" nocase

condition:
    $a0
}

rule Expand_exe2_ADS
{
    meta:
        id = "2yuC619qIUzVxUZV7n1jhd"
        fingerprint = "b53589fb49ca24bf2e752d9ebe0f0e0b9333d6a9d5115767d57998b4b8fcb418"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary that expands one or more compressed files"
        category = "TECHNIQUE"
        technique = "COPIES SOURCE FILE TO DESTINATION ALTERNATE DATA STREAM (ADS)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="expand \\\\webdav\\folder\\file.bat c:\\ADS\\file.txt:file.bat" nocase

condition:
    $a0
}

