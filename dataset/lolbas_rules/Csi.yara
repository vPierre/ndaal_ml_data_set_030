rule csi_exe0_Execute
{
    meta:
        id = "2SVhHJsKtiWgSCfx8bGEoT"
        fingerprint = "082df78a63db41dc468e827e9c6e9e9027bdbe554483b72edcbb7c9905b0c214"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Command line interface included with Visual Studio."
        category = "TECHNIQUE"
        technique = "USE CSI.EXE TO RUN UNSIGNED C# CODE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="csi.exe file" nocase

condition:
    $a0
}

