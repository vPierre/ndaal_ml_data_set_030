rule adplus_exe0_Dump
{
    meta:
        id = "7V2fcefXbmgIsdXGzoYsbM"
        fingerprint = "680bd2ae342632315a13208df12dc344562190a59a9b8285009ef249251a1a03"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "CREATES A MEMORY DUMP OF THE LSASS PROCESS"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="adplus.exe -hang -pn lsass.exe -o c:\\users\\mr.d0x\\output\\folder -quiet" nocase

condition:
    $a0
}

rule adplus_exe1_Execute
{
    meta:
        id = "5zihCuE3AcTcmrn4F11Qmq"
        fingerprint = "1f0b7a983ec986b7a7f344d0908641668a8665784f7f3c663af5a43532fd81b3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "EXECUTE ARBITRARY COMMANDS USING ADPLUS CONFIG FILE (SEE RESOURCES SECTION FOR A SAMPLE FILE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="adplus.exe -c config-adplus.xml" nocase

condition:
    $a0
}

rule adplus_exe2_Dump
{
    meta:
        id = "7aELKzaAMhfeGCBzZ2t8gg"
        fingerprint = "1f0b7a983ec986b7a7f344d0908641668a8665784f7f3c663af5a43532fd81b3"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "DUMP PROCESS MEMORY USING ADPLUS CONFIG FILE (SEE RESOURCES SECTION FOR A SAMPLE FILE)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="adplus.exe -c config-adplus.xml" nocase

condition:
    $a0
}

rule adplus_exe3_Execute
{
    meta:
        id = "nhTDXj0C3qGUlAchM40yQ"
        fingerprint = "e1d2b8b2f21db63f2ded79235b349c6c4b7e3fcb79dfd73c0f48868d56ad88f2"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Debugging tool included with Windows Debugging Tools"
        category = "TECHNIQUE"
        technique = "EXECUTE ARBITRARY COMMANDS AND BINARIES FROM THE CONTEXT OF ADPLUS. NOTE THAT PROVIDING AN OUTPUT DIRECTORY VIA '-O' IS REQUIRED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="adplus.exe -crash -o \"C:\\temp\\\" -sc calc.exe" nocase

condition:
    $a0
}

