rule Setres_exe0_Execute
{
    meta:
        id = "4quj9RkeOs7ifNcQ86PICc"
        fingerprint = "a703c94d4faa4acbd30f6e6f618a142d8c537e3c87ae707e7f319b83983953a0"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Configures display settings"
        category = "TECHNIQUE"
        technique = "SETS THE RESOLUTION AND THEN LAUNCHES 'CHOICE' COMMAND FROM THE WORKING DIRECTORY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="setres.exe -w 800 -h 600" nocase

condition:
    $a0
}

