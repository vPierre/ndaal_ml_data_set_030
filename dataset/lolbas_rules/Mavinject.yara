rule Mavinject_exe0_Execute
{
    meta:
        id = "4AIAW3GVWFm2mC5AvKTaTN"
        fingerprint = "ef11fe83b5c6ea1dbf37ffb1a61cb5be97538948d2011eff006928193a731c81"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by App-v in Windows"
        category = "TECHNIQUE"
        technique = "INJECT EVIL.DLL INTO A PROCESS WITH PID 3110."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="MavInject.exe 3110 /INJECTRUNNING c:\\folder\\evil.dll" nocase

condition:
    $a0
}

rule Mavinject_exe1_ADS
{
    meta:
        id = "2f34M8jXO9z1WoJ7EE2PCS"
        fingerprint = "4186d7b9c4bcdeaaa6b954c0c537c8404115967df5681c512ddda56911b913d5"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used by App-v in Windows"
        category = "TECHNIQUE"
        technique = "INJECT FILE.DLL STORED AS AN ALTERNATE DATA STREAM (ADS) INTO A PROCESS WITH PID 4172"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="Mavinject.exe 4172 /INJECTRUNNING \"c:\\ads\\file.txt:file.dll\"" nocase

condition:
    $a0
}

