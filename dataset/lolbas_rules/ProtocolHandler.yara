rule ProtocolHandler_exe0_Download
{
    meta:
        id = "1lsKjXgsZFMRP5Jj55O5Dj"
        fingerprint = "0b8e1a328a058db414ec90e97c8164ee0e57f0dc4ed60d064bf6b8f16e33ad42"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Microsoft Office binary"
        category = "TECHNIQUE"
        technique = "DOWNLOADS PAYLOAD FROM REMOTE SERVER"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="ProtocolHandler.exe " nocase

condition:
    $a0
}

