rule Xwizard_exe0_Execute
{
    meta:
        id = "5v3UkUOEs1zdxp7PvGQL0G"
        fingerprint = "089e0aea0d1f8e49e05b3aa79fc3002f65b309a1a710b6e895b6e9037911c72c"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute custom class that has been added to the registry or download a file with Xwizard.exe"
        category = "TECHNIQUE"
        technique = "XWIZARD.EXE RUNNING A CUSTOM CLASS THAT HAS BEEN ADDED TO THE REGISTRY."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="xwizard RunWizard {00000001-0000-0000-0000-0000FEEDACDC}" nocase

condition:
    $a0
}

rule Xwizard_exe1_Execute
{
    meta:
        id = "6qTwFtTOM9ESs6g67kRfgi"
        fingerprint = "794bb4dc904c2ad6365c2a824369709abe1c1e9cf003405a7ad94be907bab1e4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute custom class that has been added to the registry or download a file with Xwizard.exe"
        category = "TECHNIQUE"
        technique = "XWIZARD.EXE RUNNING A CUSTOM CLASS THAT HAS BEEN ADDED TO THE REGISTRY. THE /T AND /U SWITCH PREVENT AN ERROR MESSAGE IN LATER WINDOWS 10 BUILDS."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="xwizard RunWizard /taero /u {00000001-0000-0000-0000-0000FEEDACDC}" nocase

condition:
    $a0
}

rule Xwizard_exe2_Download
{
    meta:
        id = "6NyPpgf0cNTtELw8feOy9x"
        fingerprint = "852debb2e043a343848f0e93ceca485b024266a66ad2dd6ec64dec4403a96c51"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Execute custom class that has been added to the registry or download a file with Xwizard.exe"
        category = "TECHNIQUE"
        technique = "XWIZARD.EXE USES REMOTEAPP AND DESKTOP CONNECTIONS WIZARD TO DOWNLOAD A FILE, AND SAVE IT TO %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION> OR %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE\\<8_RANDOM_ALNUM_CHARS>/<FILENAME>[1].<EXTENSION>"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="xwizard RunWizard {7940acf8-60ba-4213-a7c3-f3b400ee266d} /z" nocase

condition:
    $a0
}

