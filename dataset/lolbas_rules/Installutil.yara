rule Installutil_exe0_AWL_Bypass
{
    meta:
        id = "5OulyIadVTPwRhLK6oWaTG"
        fingerprint = "66f6753e3fa73d588faad529d3a49caa3b054e1c2b2b4d62d0fcc65c020677f4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Installer tool is a command-line utility that allows you to install and uninstall server resources by executing the installer components in specified assemblies"
        category = "TECHNIQUE"
        technique = "EXECUTE THE TARGET .NET DLL OR EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="InstallUtil.exe /logfile= /LogToConsole=false /U AllTheThings.dll" nocase

condition:
    $a0
}

rule Installutil_exe1_Execute
{
    meta:
        id = "2cyDS1icQqNkXfkZ9dkZWJ"
        fingerprint = "66f6753e3fa73d588faad529d3a49caa3b054e1c2b2b4d62d0fcc65c020677f4"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Installer tool is a command-line utility that allows you to install and uninstall server resources by executing the installer components in specified assemblies"
        category = "TECHNIQUE"
        technique = "EXECUTE THE TARGET .NET DLL OR EXE."
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="InstallUtil.exe /logfile= /LogToConsole=false /U AllTheThings.dll" nocase

condition:
    $a0
}

rule Installutil_exe2_Download
{
    meta:
        id = "7VNMPjaDYaC3a1g35OugQh"
        fingerprint = "683588ff9f8039fadbedc3034f53df4b5acb88f1b7b127069d07d35f8c982da9"
        version = "1.0"
        date = "2023-07-05"
        modified = "2023-07-05"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The Installer tool is a command-line utility that allows you to install and uninstall server resources by executing the installer components in specified assemblies"
        category = "TECHNIQUE"
        technique = "IT WILL DOWNLOAD A REMOTE PAYLOAD AND PLACE IT IN THE CACHE FOLDER (FOR EXAMPLE - %LOCALAPPDATA%\\MICROSOFT\\WINDOWS\\INETCACHE\\IE)"
        reference = "https://github.com/LOLBAS-Project/LOLBAS"

strings:
    $a0="InstallUtil.exe " nocase

condition:
    $a0
}

