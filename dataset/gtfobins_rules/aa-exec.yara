rule aa_exec_shell
{
    meta:
        id = "4zdUwXNvhyK5Kfk3e74BH1"
        fingerprint = "69b2d4a278e261a6178fda03dbee6cd62d81f3da0aa4eb4eaf10945361e138e4"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aa-exec to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="aa-exec /bin/sh" nocase

condition:
    $a0
}

rule aa_exec_sudo
{
    meta:
        id = "4BpI6oZUu0K1MrloCDrj5u"
        fingerprint = "3e1c39013ac6c95caf3512b6ca4503e43b3b9eb7daa6e845d209cd50d64a658b"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aa-exec to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo aa-exec /bin/sh" nocase

condition:
    $a0
}

rule aa_exec_suid
{
    meta:
        id = "6IZi3PwmaAnwQnt2BH4I8D"
        fingerprint = "b9534b27a620ef927099199def4492816d2bbfe64437989aabef18abe540876c"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aa-exec to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./aa-exec /bin/sh -p" nocase

condition:
    $a0
}

