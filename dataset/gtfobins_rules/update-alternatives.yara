rule update_alternatives_sudo
{
    meta:
        id = "5STG6xzijtElFum3k1i7m2"
        fingerprint = "6c63ced97077bde52555835d19e73d1211e86c37f818d2d83530d55ed18f6ab7"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using update-alternatives to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=/path/to/file_to_write TF=$(mktemp) echo DATA >$TF sudo update-alternatives --force --install \"$LFILE\" x \"$TF\" 0" nocase

condition:
    $a0
}

rule update_alternatives_suid
{
    meta:
        id = "7lwJ6FFq9IJePh8kA5Ixa8"
        fingerprint = "ec2267246dbdff2306b8720fdecc7efe40a5dde46623fb9326c0ad2e8610cbd8"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using update-alternatives to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=/path/to/file_to_write TF=$(mktemp) echo DATA >$TF ./update-alternatives --force --install \"$LFILE\" x \"$TF\" 0" nocase

condition:
    $a0
}

