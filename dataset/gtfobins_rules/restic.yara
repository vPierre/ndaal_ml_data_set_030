rule restic_file_upload
{
    meta:
        id = "mRilcb199aJxqKULkceSk"
        fingerprint = "ba635edea8f40b49bd2ae7058285bb9f09e1c02c23110fee45cae5a485860cb1"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using restic to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=file_or_dir_to_get NAME=backup_name restic backup -r \"rest: \"$LFILE\"" nocase

condition:
    $a0
}

rule restic_sudo
{
    meta:
        id = "4RE9mbURa6YVekZhg66Vh7"
        fingerprint = "cd8b0a460c4e10bbbc6c68ad08d3540848dea376f16e068267ba706c5d6e54ce"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using restic to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=file_or_dir_to_get NAME=backup_name sudo restic backup -r \"rest: \"$LFILE\"" nocase

condition:
    $a0
}

rule restic_suid
{
    meta:
        id = "61iJvTRaqJ0sqj6PeaOXjU"
        fingerprint = "362de44e10be9f0f17fbb730a4d774cb34081049e2be51eb5c599e59fdace566"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using restic to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=file_or_dir_to_get NAME=backup_name ./restic backup -r \"rest: \"$LFILE\"" nocase

condition:
    $a0
}

