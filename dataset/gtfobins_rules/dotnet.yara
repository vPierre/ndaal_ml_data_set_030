rule dotnet_file_read
{
    meta:
        id = "62HITtrlWDDKrJP8eT8FdI"
        fingerprint = "35b80d05d5a9d2d56876c71006dcb7003fb8bef643b2085da22ef278908d419b"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dotnet to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="dotnet fsi System.IO.File.ReadAllText(System.Environment.GetEnvironmentVariable(\"LFILE\"));;" nocase

condition:
    $a0
}

rule dotnet_shell
{
    meta:
        id = "5INQP5YdzfkbMf67FNQ5jn"
        fingerprint = "8fdfd453f03f1fccc84ae116188deafc958e727ad5a7e833aada5530003a023f"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dotnet to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="dotnet fsi System.Diagnostics.Process.Start(\"/bin/sh\").WaitForExit();;" nocase

condition:
    $a0
}

rule dotnet_sudo
{
    meta:
        id = "6A0c4FKLt8yHXNgYhZOqvj"
        fingerprint = "1bb2b158a43ed85f319048a85ca4c0edbe8c7df687b1b8fcc3bb2a3d21cc2044"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dotnet to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo dotnet fsi System.Diagnostics.Process.Start(\"/bin/sh\").WaitForExit();;" nocase

condition:
    $a0
}

