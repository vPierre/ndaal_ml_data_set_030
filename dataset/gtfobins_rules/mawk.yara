rule mawk_file_read
{
    meta:
        id = "4td0BlxVMBWlGa61FagQfM"
        fingerprint = "7a0528ed33c50040331fc0e19bf57e400825505bfcfefa2eef0d264ece7e5c17"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mawk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="mawk '//' \"$LFILE\"" nocase

condition:
    $a0
}

rule mawk_file_write
{
    meta:
        id = "3DV644Jz3bOtHFQ78pBvDS"
        fingerprint = "5cb6ed176cc5427db2d7ffc75b07340e0397d5fc1d211eea0c526ead9a5f72ab"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mawk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="mawk -v LFILE=$LFILE 'BEGIN { print \"DATA\" > LFILE }'" nocase

condition:
    $a0
}

rule mawk_limited_suid
{
    meta:
        id = "2W84Xse4SVGzPO6UMxs0r2"
        fingerprint = "4e77362b58a44f1c0033b70d9104ca3a19a8ad9a00bd4eb608ed331919791e1e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mawk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./mawk 'BEGIN {system(\"/bin/sh\")}'" nocase

condition:
    $a0
}

rule mawk_shell
{
    meta:
        id = "4DlTpzlFCOiVsPFPFjXHnf"
        fingerprint = "20b0231121b9b23bf86cb8f49a0026d1cf9cee637bb7816a2dd95bd348fac760"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mawk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="mawk 'BEGIN {system(\"/bin/sh\")}'" nocase

condition:
    $a0
}

rule mawk_sudo
{
    meta:
        id = "SNGvjS7LilIVhqVtgcRg1"
        fingerprint = "3cd9207d44b860d0feae09ae64a7a127afc659576b385df3600d8aa824ad3d69"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mawk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo mawk 'BEGIN {system(\"/bin/sh\")}'" nocase

condition:
    $a0
}

rule mawk_suid
{
    meta:
        id = "1Gv3Vn5K7p5kq3oCRbtuaG"
        fingerprint = "8ff20faa60b6f2cc2d189191ad0d2ed1b2035bd3ce0f933fb7458eadddeb4fcc"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mawk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./mawk '//' \"$LFILE\"" nocase

condition:
    $a0
}

