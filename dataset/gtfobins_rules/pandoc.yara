rule pandoc_file_read
{
    meta:
        id = "Q0tZ98yme44I0cCDZS6yj"
        fingerprint = "f72a3f567b6a6ea3068b121fba1f2fa549900b324efa01c836c8f1fdd592c1dd"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pandoc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="pandoc -t plain \"$LFILE\"" nocase

condition:
    $a0
}

rule pandoc_file_write
{
    meta:
        id = "5KY6h57uv7ypUj4VruvFYT"
        fingerprint = "f447320b4dc4a370bf26e6121a97ddbdd46899aea55b1eb3759f00199c783701"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pandoc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo DATA | pandoc -t plain -o \"$LFILE\"" nocase

condition:
    $a0
}

rule pandoc_sudo
{
    meta:
        id = "2Gfdp2GbHxj7ZJJc2U1zOv"
        fingerprint = "a9d843717c6f88f1ce4460bd393f996653c362e5aa7d7833759dc760f5fae763"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pandoc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo DATA | sudo pandoc -t plain -o \"$LFILE\"" nocase

condition:
    $a0
}

rule pandoc_suid
{
    meta:
        id = "fve7br92UZu1cxBIGtxdq"
        fingerprint = "a0f6509aef3981f65f52bbfb13e04c0849df5e3eac244e57fda0d3b6182fd410"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using pandoc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo DATA | ./pandoc -t plain -o \"$LFILE\"" nocase

condition:
    $a0
}

