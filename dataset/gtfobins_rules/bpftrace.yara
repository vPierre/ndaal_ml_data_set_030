rule bpftrace_sudo
{
    meta:
        id = "50xdxwWjesqu3wrNjNVhAV"
        fingerprint = "97f50a4576418c5dd2071b4fd399a249d60fb6d8eb6f356e27dbd7fe7bea0910"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using bpftrace to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo bpftrace -e 'BEGIN {system(\"/bin/sh\");exit()}'" nocase
    $a1="TF=$(mktemp) echo 'BEGIN {system(\"/bin/sh\");exit()}' >$TF sudo bpftrace $TF" nocase

condition:
    ($a0 or $a1)
}

