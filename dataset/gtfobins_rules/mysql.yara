rule mysql_library_load
{
    meta:
        id = "6roJgDARqBNscGJC9zf1YQ"
        fingerprint = "369b2334dc64992e496c6d4b91275b3c0eedea12c67938bb0432a38bd63ed69c"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mysql to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT LOADS SHARED LIBRARIES THAT MAY BE USED TO RUN CODE IN THE BINARY EXECUTION CONTEXT."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="mysql --default-auth ../../../../../path/to/lib" nocase

condition:
    $a0
}

rule mysql_limited_suid
{
    meta:
        id = "3cdKsBIEk2kkRi1PTMabVH"
        fingerprint = "de680dccfd9587bc00f1099e0c75f05dfea7c216f9f2e82a6107bc6db12d832f"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mysql to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./mysql -e '\\! /bin/sh'" nocase

condition:
    $a0
}

rule mysql_shell
{
    meta:
        id = "1RplUruV7iRMCBXu6sy5JK"
        fingerprint = "4d3e04e09b1ac7863e3ba61096de252bff5fcc9344c466c5b0bbec97cc2aa2ae"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mysql to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="mysql -e '\\! /bin/sh'" nocase

condition:
    $a0
}

rule mysql_sudo
{
    meta:
        id = "NTPHWeCNsXz8yxYKs90gF"
        fingerprint = "eda2889a6fd6e1f35714ec4d0c1591339596f949ae39aac170d78c608228fdc3"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mysql to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo mysql -e '\\! /bin/sh'" nocase

condition:
    $a0
}

