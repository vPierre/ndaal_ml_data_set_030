rule latexmk_file_read
{
    meta:
        id = "1s4iyXFZXFpUUf40QuS21k"
        fingerprint = "4418aecdd9e7cbbda227e3eecb768029218ce521c3ca5a9f1d49181ec09097d1"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using latexmk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="latexmk -e 'open(X,\"/etc/passwd\");while(<X>){print $_;}exit'" nocase
    $a1="TF=$(mktemp) echo '\\documentclass{article}\\usepackage{verbatim}\\begin{document}\\verbatiminput{file_to_read}\\end{document}' >$TF strings tmp.dvi" nocase

condition:
    ($a0 or $a1)
}

rule latexmk_shell
{
    meta:
        id = "5oEKiesTF95rMg2jrqyqYp"
        fingerprint = "52405f4bbe4daa54098361df3c89414cebe6ed51c6636d206b0f9c252828ad38"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using latexmk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="latexmk -e 'exec \"/bin/sh\";'" nocase
    $a1="latexmk -latex='/bin/sh #' /dev/null" nocase

condition:
    ($a0 or $a1)
}

rule latexmk_sudo
{
    meta:
        id = "6j9m0JKY4d7tm4OrFDPXJB"
        fingerprint = "7388752afeb57b8236afc7c33ff6eebfcc98a5b0ea291af3abcbcb6d96263104"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using latexmk to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo latexmk -e 'exec \"/bin/sh\";'" nocase

condition:
    $a0
}

