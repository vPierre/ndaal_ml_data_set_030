rule gtester_file_write
{
    meta:
        id = "4eJExS4xglR2rMeQEHQJ3P"
        fingerprint = "2584ca0fab040a653fab1e677e7a1e3f487e833553621e986d7fe0cdab8ca0da"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using gtester to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="gtester \"DATA\" -o $LFILE" nocase

condition:
    $a0
}

rule gtester_shell
{
    meta:
        id = "5LofCeZY5OHCWURTevdHgf"
        fingerprint = "f250263a970c0b926f014aea0d30623d29b970d2c86bdaca260ced1b71d414ef"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using gtester to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo '#!/bin/sh' > $TF echo 'exec /bin/sh -p 0<&1' >> $TF chmod +x $TF gtester -q $TF" nocase

condition:
    $a0
}

rule gtester_sudo
{
    meta:
        id = "18o18nmzN2yf3axIAb0Jrk"
        fingerprint = "a1a3254f9d20d3b9705811953be1439cf7b84b1447ed32aaee6b70157dd6162c"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using gtester to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo '#!/bin/sh' > $TF echo 'exec /bin/sh 0<&1' >> $TF chmod +x $TF sudo gtester -q $TF" nocase

condition:
    $a0
}

rule gtester_suid
{
    meta:
        id = "2dhvNc5iOT488rNfgDum7e"
        fingerprint = "308ae71b0e9e9a4ae4dd76b5a328faeb227f9269d1f4700ea8b27b976d3fafe3"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using gtester to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo '#!/bin/sh -p' > $TF echo 'exec /bin/sh -p 0<&1' >> $TF chmod +x $TF sudo gtester -q $TF" nocase

condition:
    $a0
}

