rule tmate_limited_suid
{
    meta:
        id = "6t31G3jb5ErzEJ51HhJdDW"
        fingerprint = "e9451960d05c47f2683b3f281c054a1a585de60dd38fe37e597faa8ed23e0b5a"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tmate to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./tmate -c /bin/sh" nocase

condition:
    $a0
}

rule tmate_shell
{
    meta:
        id = "PnDWI6p22HjNBd3cxVJKM"
        fingerprint = "b992388b030dce1f52ba5188302ee0fb9c6de134696aaf775d903839e6571c78"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tmate to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="tmate -c /bin/sh" nocase

condition:
    $a0
}

rule tmate_sudo
{
    meta:
        id = "2mVrPxDFKL1RSuKq9uVFv7"
        fingerprint = "9b60c51a1f4b91756e58180c75767859dd8a21b3274680c87bfa849a4f490204"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using tmate to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo tmate -c /bin/sh" nocase

condition:
    $a0
}

