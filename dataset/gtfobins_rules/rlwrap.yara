rule rlwrap_file_write
{
    meta:
        id = "1Jcjagttb8YqKSpLnfjQQd"
        fingerprint = "d16a06aee7aeb38830d9269d26a2ad357a3b4a59c4f07ba804e0eeb3337bec7d"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rlwrap to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rlwrap -l \"$LFILE\" echo DATA" nocase

condition:
    $a0
}

rule rlwrap_shell
{
    meta:
        id = "z1bSvS5G0K2QbPMwwQZgL"
        fingerprint = "6aecc89f02c3684e2082690b23fd33fa16f38ab1ca6ed45bdb19d5504bd41c8c"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rlwrap to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="rlwrap /bin/sh" nocase

condition:
    $a0
}

rule rlwrap_sudo
{
    meta:
        id = "3J7GlhoJcw8SE2ttL1EL6G"
        fingerprint = "fdd5d28eca482816ef36d9ee13aa80a3d754edcb8f308a206fbaf988207c8435"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rlwrap to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo rlwrap /bin/sh" nocase

condition:
    $a0
}

rule rlwrap_suid
{
    meta:
        id = "3MTHweciDrxCMyySWbLXrY"
        fingerprint = "d1b0055b7648b3c7ae8ae66d53ea86ed0802cb6ece46e33217849b99ac0d63e1"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using rlwrap to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./rlwrap -H /dev/null /bin/sh -p" nocase

condition:
    $a0
}

