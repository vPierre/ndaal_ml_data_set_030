rule npm_shell
{
    meta:
        id = "7Gcao1zsKMApS1hB4Qy28b"
        fingerprint = "db6c92808b14936b377b08b49d5061b663b8b1e0bd2dc4348b73b6467e78c284"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using npm to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="npm exec /bin/sh" nocase
    $a1="TF=$(mktemp -d) echo '{\"scripts\": {\"preinstall\": \"/bin/sh\"}}' > $TF/package.json npm -C $TF i" nocase

condition:
    ($a0 or $a1)
}

rule npm_sudo
{
    meta:
        id = "4kkKI1T7qbnPSJcwzYLptl"
        fingerprint = "1d58256dc177053bd1b04dd855adb213954a544dc741a5f5a245c2529144771f"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using npm to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo '{\"scripts\": {\"preinstall\": \"/bin/sh\"}}' > $TF/package.json sudo npm -C $TF --unsafe-perm i" nocase

condition:
    $a0
}

