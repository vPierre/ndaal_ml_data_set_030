rule dvips_limited_suid
{
    meta:
        id = "2QgVOLCGDZVB0vyqdwjGna"
        fingerprint = "213d48e37662a31888423177ac7f200b3290ea0bb108e770b83e5b7fe8ff4eef"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dvips to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="tex '\\special{psfile=\"`/bin/sh 1>&0\"}\\end' ./dvips -R0 texput.dvi" nocase

condition:
    $a0
}

rule dvips_shell
{
    meta:
        id = "36EfSU5BTV5viM9zK1aNg3"
        fingerprint = "43b4e22d8429dee97357246f3d2a6d7dad12934a689fcc0aca11c6005d13729f"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dvips to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="tex '\\special{psfile=\"`/bin/sh 1>&0\"}\\end' dvips -R0 texput.dvi" nocase

condition:
    $a0
}

rule dvips_sudo
{
    meta:
        id = "75tFy73DSm01gSPJQKZnne"
        fingerprint = "92740a34e4d576604c5fb319275335070f0f46b1bc40fafaf6847fb0b67e3ef7"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dvips to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="tex '\\special{psfile=\"`/bin/sh 1>&0\"}\\end' sudo dvips -R0 texput.dvi" nocase

condition:
    $a0
}

