rule cpio_file_read
{
    meta:
        id = "2vwANbGPLQtaRUrjNGZ456"
        fingerprint = "ffc6916f8683fd35c34ee5ae38b1f7ee58b0c7a42c1ba4fcf3a932ec3796b2dd"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cpio to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"$LFILE\" | cpio -o" nocase
    $a1="TF=$(mktemp -d) echo \"$LFILE\" | cpio -dp $TF cat \"$TF/$LFILE\"" nocase

condition:
    ($a0 or $a1)
}

rule cpio_file_write
{
    meta:
        id = "6k92EQYhfDicZK1bwuClZl"
        fingerprint = "7c6884d8392930ef3a7fcdef17d787b1b6894c89e83652d4e1865accac8678f2"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cpio to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LDIR=where_to_write echo DATA >$LFILE echo $LFILE | cpio -up $LDIR" nocase

condition:
    $a0
}

rule cpio_shell
{
    meta:
        id = "5G7b54dmOlUIk2pk21MEKw"
        fingerprint = "f4f158881e3a7d3e7ac78d2fbded262d8d94194d461aaf3c4991d32543c430a5"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cpio to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo '/bin/sh </dev/tty >/dev/tty' >localhost cpio -o --rsh-command /bin/sh -F localhost:" nocase

condition:
    $a0
}

rule cpio_sudo
{
    meta:
        id = "4EpI0XP9gIje8FxQx7RNUc"
        fingerprint = "9d5cfa719443657cab196d46240ca763205915bae63e0333db8c96aff5ef1344"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cpio to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo '/bin/sh </dev/tty >/dev/tty' >localhost sudo cpio -o --rsh-command /bin/sh -F localhost:" nocase
    $a1="TF=$(mktemp -d) echo \"$LFILE\" | sudo cpio -R $UID -dp $TF cat \"$TF/$LFILE\"" nocase

condition:
    ($a0 or $a1)
}

rule cpio_suid
{
    meta:
        id = "4qyEsjRjKydLUhAsupDyuv"
        fingerprint = "28205d741c98ef3ff455b21fb081655d77178b03ab8a57e6c084c6b7ddecaef7"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cpio to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo \"$LFILE\" | ./cpio -R $UID -dp $TF cat \"$TF/$LFILE\"" nocase
    $a1="LDIR=where_to_write echo DATA >$LFILE echo $LFILE | ./cpio -R 0:0 -p $LDIR" nocase

condition:
    ($a0 or $a1)
}

