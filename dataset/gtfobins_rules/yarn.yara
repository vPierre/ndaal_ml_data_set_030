rule yarn_shell
{
    meta:
        id = "22hEJwHPf9OR6uTripD3Lc"
        fingerprint = "123e56d5dfa23ae66d11f24c13b17806b22f725f70aca1fa1b1643d157960b1e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using yarn to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="yarn exec /bin/sh" nocase
    $a1="TF=$(mktemp -d) echo '{\"scripts\": {\"preinstall\": \"/bin/sh\"}}' > $TF/package.json yarn --cwd $TF install" nocase

condition:
    ($a0 or $a1)
}

rule yarn_sudo
{
    meta:
        id = "56LZOYRSUq2WGgj6MatGA4"
        fingerprint = "54c184000552a91603aa226ec3d114cbf7288661161ec3f7e9473385acbf2f41"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using yarn to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo yarn exec /bin/sh" nocase

condition:
    $a0
}

