rule systemctl_sudo
{
    meta:
        id = "3UzrdPjbfDr8xqFTCJJtFS"
        fingerprint = "66a5b61987eae358c5bd6b5658c6c3cc3c16392a65128919d2dd0df85be8f3da"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using systemctl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo /bin/sh >$TF chmod +x $TF sudo SYSTEMD_EDITOR=$TF systemctl edit system.slice" nocase
    $a1="TF=$(mktemp).service echo '[Service] Type=oneshot ExecStart=/bin/sh -c \"id > /tmp/output\" [Install] WantedBy=multi-user.target' > $TF sudo systemctl link $TF sudo systemctl enable --now $TF" nocase

condition:
    ($a0 or $a1)
}

rule systemctl_suid
{
    meta:
        id = "uDBTkDCliMsJWTRshluSZ"
        fingerprint = "3f4821f149c93b5ed98959a6da0c031a8a225199759f66d81ba905bf49d47734"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using systemctl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp).service echo '[Service] Type=oneshot ExecStart=/bin/sh -c \"id > /tmp/output\" [Install] WantedBy=multi-user.target' > $TF ./systemctl link $TF ./systemctl enable --now $TF" nocase

condition:
    $a0
}

