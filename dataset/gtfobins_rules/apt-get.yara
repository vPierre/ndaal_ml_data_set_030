rule apt_get_shell
{
    meta:
        id = "2niYRwEMeA2EfBpDFsWVF0"
        fingerprint = "ea3acb052c79ca784e72b61947155bbb34aba8fa9d578e6c352b6fe6b03e0464"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using apt-get to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="apt-get changelog apt !/bin/sh" nocase

condition:
    $a0
}

rule apt_get_sudo
{
    meta:
        id = "3GtNZ0JOgJHDap8SpaSZhw"
        fingerprint = "6088c63555119c10eb37f50ff2a4665aba0d8c08f40383628cf553b024aed408"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using apt-get to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo apt-get changelog apt !/bin/sh" nocase
    $a1="TF=$(mktemp) echo 'Dpkg::Pre-Invoke {\"/bin/sh;false\"}' > $TF sudo apt-get install -c $TF sl" nocase

condition:
    ($a0 or $a1)
}

