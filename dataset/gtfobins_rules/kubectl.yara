rule kubectl_file_upload
{
    meta:
        id = "5KHUyQwF6NxWHqI9LnES9D"
        fingerprint = "d4c2860964a3c2be2480dea3e41cdd55e1cbcdff40097d99df72f8f905482763"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using kubectl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=dir_to_serve kubectl proxy --address=0.0.0.0 --port=4444 -- --" nocase

condition:
    $a0
}

rule kubectl_sudo
{
    meta:
        id = "7ApCs4exmHiFLyAsm34jz9"
        fingerprint = "082490695711256c63911d498a936522b33848a50a8cea5fc5f00ad3c8587f96"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using kubectl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=dir_to_serve sudo kubectl proxy --address=0.0.0.0 --port=4444 -- --" nocase

condition:
    $a0
}

rule kubectl_suid
{
    meta:
        id = "5WQaSJyo82jpQTYiyWTfoG"
        fingerprint = "9208bd4e99ac8ffc29b7619ab3da325cc4e99b7422c83b00fd0d82d64e535193"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using kubectl to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="LFILE=dir_to_serve ./kubectl proxy --address=0.0.0.0 --port=4444 -- --" nocase

condition:
    $a0
}

