rule crash_command
{
    meta:
        id = "3wSxa6t4ejTmdmsk4FAIWJ"
        fingerprint = "9b9b1a8c1326c3f70d9bc978195037fbe7bffc707b11c051a0f353c130464822"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using crash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='/usr/bin/id' CRASHPAGER=\"$COMMAND\" crash -h" nocase

condition:
    $a0
}

rule crash_shell
{
    meta:
        id = "6IVljbTGbLv4pGcocvKCAK"
        fingerprint = "56202f5a8d3795573c84c94e9e95eda4c2c4de1cc9c53d6087f154001a51d111"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using crash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="crash -h !sh" nocase

condition:
    $a0
}

rule crash_sudo
{
    meta:
        id = "xareRdw2igh9u7keymQti"
        fingerprint = "92213bb88b833f6f75e7851968addccc4c5d6e33e9dc419d6ccec076ad3c2e4e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using crash to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo crash -h !sh" nocase

condition:
    $a0
}

