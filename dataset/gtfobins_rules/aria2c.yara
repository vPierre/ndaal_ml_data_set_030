rule aria2c_command
{
    meta:
        id = "3aGSS0cA8SYzNRtE5jGryG"
        fingerprint = "28ea4817950077749cc649b576aabda69b2fe44b43736485647690404262a75f"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aria2c to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY RUNNING NON-INTERACTIVE SYSTEM COMMANDS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='id' TF=$(mktemp) echo \"$COMMAND\" > $TF chmod +x $TF aria2c --on-download-error=$TF" nocase
    $a1="aria2c --allow-overwrite --gid=aaaaaaaaaaaaaaaa --on-download-complete=bash" nocase

condition:
    ($a0 or $a1)
}

rule aria2c_file_download
{
    meta:
        id = "2D0vVw6pF5yV0psySeACdx"
        fingerprint = "c8911aac9eede7db24d7afacb3b284dcb6e326afbf3d430a5820460976ad82c6"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aria2c to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="aria2c -o \"$LFILE\" \"$URL\"" nocase

condition:
    $a0
}

rule aria2c_limited_suid
{
    meta:
        id = "5hyGnCpuS1FPKgLcUEZP5w"
        fingerprint = "580cf121d0c4a44632d7f32243047063521199aacd64f9dc96785865f9cb9305"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aria2c to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN ACCESS WITH ELEVATED PRIVILEGES WORKING AS A SUID BACKDOOR."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='id' TF=$(mktemp) echo \"$COMMAND\" > $TF chmod +x $TF ./aria2c --on-download-error=$TF" nocase

condition:
    $a0
}

rule aria2c_sudo
{
    meta:
        id = "6m4p5zQkmHxNSvBCJuaOfm"
        fingerprint = "5b13c815d660e769fa0ef5b54c39227705d9af8ea514af5fd744c61f21beb682"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using aria2c to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="COMMAND='id' TF=$(mktemp) echo \"$COMMAND\" > $TF chmod +x $TF sudo aria2c --on-download-error=$TF" nocase

condition:
    $a0
}

