rule zypper_shell
{
    meta:
        id = "hl9cU6jveBZEL5KfOxb6F"
        fingerprint = "7a4faed2f43610dd60e03334d8fc3133f9d4a5276756b52695cbfe6d6d8a81bd"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using zypper to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="zypper x" nocase
    $a1="TF=$(mktemp -d) cp /bin/sh $TF/zypper-x export PATH=$TF:$PATH zypper x" nocase

condition:
    ($a0 or $a1)
}

rule zypper_sudo
{
    meta:
        id = "7LMlP6PPM1dbNKvWYl2uk"
        fingerprint = "5ab3a46c12d47332f70a47d1e54f8dca1b8bc5ad1b5bbb220d039bf114ef93cf"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using zypper to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo zypper x" nocase
    $a1="TF=$(mktemp -d) cp /bin/sh $TF/zypper-x sudo PATH=$TF:$PATH zypper x" nocase

condition:
    ($a0 or $a1)
}

