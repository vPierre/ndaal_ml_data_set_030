rule wireshark_sudo
{
    meta:
        id = "7PlvOAtGxmVr5eA8LTBInf"
        fingerprint = "5279244e4c76023f43e22d2d8b709c2f535f77da7b8cb4bde6ffaeee5d775c00"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using wireshark to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="PORT=4444 sudo wireshark -c 1 -i lo -k -f \"udp port $PORT\" & echo 'DATA' | nc -u 127.127.127.127 \"$PORT\"" nocase

condition:
    $a0
}

