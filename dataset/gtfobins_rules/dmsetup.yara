rule dmsetup_sudo
{
    meta:
        id = "3SHFJUVdqqcnUC7F5jb2q3"
        fingerprint = "fb239eee45781c65137ff3e519194a7a204fc09c5eab1bca246367baf3076410"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dmsetup to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo dmsetup create base <<EOF 0 3534848 linear /dev/loop0 94208 EOF sudo dmsetup ls --exec '/bin/sh -s'" nocase

condition:
    $a0
}

rule dmsetup_suid
{
    meta:
        id = "7ONX6m5HUrHc1HhgnbatTU"
        fingerprint = "f0054ace5c24a5cd008af8f2ece84190decac9857359adb53d49ff182c5f03d8"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using dmsetup to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./dmsetup create base <<EOF 0 3534848 linear /dev/loop0 94208 EOF ./dmsetup ls --exec '/bin/sh -p -s'" nocase

condition:
    $a0
}

