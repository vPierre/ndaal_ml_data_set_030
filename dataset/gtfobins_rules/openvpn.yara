rule openvpn_file_read
{
    meta:
        id = "3Rs1ZdrKRkj5x00gvHk4Om"
        fingerprint = "4322f00e39e08fcb073e1ffcc838971b9f787097c02083a3e6d8d34677d665d3"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using openvpn to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="openvpn --config \"$LFILE\"" nocase

condition:
    $a0
}

rule openvpn_shell
{
    meta:
        id = "5BCsaTDNyggrc2ICvLtOVk"
        fingerprint = "efb9ce34ed9e4592597566ad228af7f7416d3caa5d76d07946996feb282b4870"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using openvpn to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="openvpn --dev null --script-security 2 --up '/bin/sh -c sh'" nocase

condition:
    $a0
}

rule openvpn_sudo
{
    meta:
        id = "4sZ9a9YyPhjUkTUrolqrua"
        fingerprint = "99f5b8fea29dd4813d9ec4357260f1d781d23902d93bc460103e2ae2cdacbacf"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using openvpn to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo openvpn --dev null --script-security 2 --up '/bin/sh -c sh'" nocase
    $a1="sudo openvpn --config \"$LFILE\"" nocase

condition:
    ($a0 or $a1)
}

rule openvpn_suid
{
    meta:
        id = "7VsscTg1ADeaQxgOjLSVsk"
        fingerprint = "65e8cee5d4dfb45adf47a6a3555e0d0629557100fad8a49974f170168f227328"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using openvpn to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="./openvpn --dev null --script-security 2 --up '/bin/sh -p -c \"sh -p\"'" nocase
    $a1="./openvpn --config \"$LFILE\"" nocase

condition:
    ($a0 or $a1)
}

