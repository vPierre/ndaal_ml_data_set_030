rule cobc_shell
{
    meta:
        id = "2EJ2gLia4y7utWixeMBI7I"
        fingerprint = "a934f795f7d916491a674e90f2f8027b46651346fee3c542a0a2807a1176b0db"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cobc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'CALL \"SYSTEM\" USING \"/bin/sh\".' > $TF/x cobc -xFj --frelax-syntax-checks $TF/x" nocase

condition:
    $a0
}

rule cobc_sudo
{
    meta:
        id = "30zXVm84meza9PB3irxoJk"
        fingerprint = "258ad497b85cfe7a032026355f56e7ab63dd931ac322a37f2607ab07c0fa4e06"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cobc to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp -d) echo 'CALL \"SYSTEM\" USING \"/bin/sh\".' > $TF/x sudo cobc -xFj --frelax-syntax-checks $TF/x" nocase

condition:
    $a0
}

