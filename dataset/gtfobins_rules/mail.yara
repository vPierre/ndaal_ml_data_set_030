rule mail_shell
{
    meta:
        id = "2YzFR4nD7l0n3a56iQkqlT"
        fingerprint = "aa8c1da0f2063d78f21932b9acca5eb899fa530d555c6d01cf158ee4567dbf12"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mail to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="mail --exec='!/bin/sh'" nocase
    $a1="TF=$(mktemp) echo \"From nobody@localhost $(date)\" > $TF mail -f $TF !/bin/sh" nocase

condition:
    ($a0 or $a1)
}

rule mail_sudo
{
    meta:
        id = "1J1QPHzOJrn49kxLYml3Dn"
        fingerprint = "343ed5077b32fd5a78d3850b2244838955f315bf9fde76f4ebb566592e0042e7"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using mail to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo mail --exec='!/bin/sh'" nocase

condition:
    $a0
}

