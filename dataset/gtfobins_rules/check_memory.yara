rule check_memory_file_read
{
    meta:
        id = "1FRTWX7hWSy13BWsCkAWlb"
        fingerprint = "df1c534e3e2467e31190eb5d091283c72c10f403f834d412c4134f31630e6235"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using check_memory to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="check_memory --extra-opts=@$LFILE" nocase

condition:
    $a0
}

rule check_memory_sudo
{
    meta:
        id = "lCrNi3W9zSenKnHkvyNxg"
        fingerprint = "891c757b1f019b42b885699cc8595e5efabbdcd1ac4b22352255956fe2ec3850"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using check_memory to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo check_memory --extra-opts=@$LFILE" nocase

condition:
    $a0
}

