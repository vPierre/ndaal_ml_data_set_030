rule cowthink_shell
{
    meta:
        id = "654MOz8dk1Pgd5cv0WM13L"
        fingerprint = "f04e734d49a7c6d3d7c1b4c563da9e8928b4eb5168915b9c73ed26db34c12bf7"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cowthink to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'exec \"/bin/sh\";' >$TF cowthink -f $TF x" nocase

condition:
    $a0
}

rule cowthink_sudo
{
    meta:
        id = "5mbwMhJkqqZ47jvXCbsJ7p"
        fingerprint = "6a9d7cde421b97071a4758c06d9ef2d3010f8ab885d664b5b4da169c768f0142"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using cowthink to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="TF=$(mktemp) echo 'exec \"/bin/sh\";' >$TF sudo cowthink -f $TF x" nocase

condition:
    $a0
}

