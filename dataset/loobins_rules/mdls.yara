rule mdls0
{
    meta:
        id = "3LoOlbGoKfLmVg94FaKaBU"
        fingerprint = "df3352b5eca8672f3cc811459ff11b1716b17284794438de121ec0e232f00efb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdls list file metadata across standard metadata (creation date, size), extended attribute (quarantine), and Spotlight APIs (Finder flags)."
        category = "TECHNIQUE"
        technique = "USE MDLS TO VALIDATE PAYLOAD DOWNLOAD SOURCES AND TIMESTAMPS TO GUARD AGAINST SANDBOX EXECUTIONS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="mdls -name \"kMDItemWhereFroms\" -name \"kMDItemDownloadedDate\"" nocase

condition:
    $a0
}

rule mdls1
{
    meta:
        id = "3ZHDU1Jk4BW4KsHJB6JuLO"
        fingerprint = "5de2a7396610a9aa58a307618463102777aa488f3f1bcfe3f5be464d903a27b3"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdls list file metadata across standard metadata (creation date, size), extended attribute (quarantine), and Spotlight APIs (Finder flags)."
        category = "TECHNIQUE"
        technique = "USE MDLS TO PRINT FILE PATHS AND SIZES WHEN ENUMERATING HOST RESOURCES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="xargs -0 mdls -n kMDItemPath -n kMDItemFSSize" nocase

condition:
    $a0
}

