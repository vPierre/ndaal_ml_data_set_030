rule sqlite30
{
    meta:
        id = "6OmBJqvkyMvbM8eCxMliE5"
        fingerprint = "eeaeb2c02c041c2b0cd03b304599dfe5bfebfe3fe7f30d9356bec0af3abbc040"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "sqlite is a command-line utility that allows users to query and manage sqlite databases. Many components of macOS and apps used sqlite to store data. Attackers can leverage this tool to discover sensitive data."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND INTERACTS WITH THE TCC (TRANSPARENCY, CONSENT, AND CONTROL) DATABASE TO SHOW THE APPS THAT HAVE FULL DISK ACCESS PERMISSION"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sqlite3 /Library/Application\\ Support/com.apple.TCC/TCC.db \\" nocase
    $a1="'select client from access where auth_value and service = \"kTCCServiceSystemPolicyAllFiles\"'" nocase

condition:
    ($a0 or $a1)
}

rule sqlite31
{
    meta:
        id = "3Z6J2SlphgoHwQHfr8mOAJ"
        fingerprint = "29cf89684a4b39d63f573feaca9c9ac22f3e2f036721126ad73f2c733fa61acc"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "sqlite is a command-line utility that allows users to query and manage sqlite databases. Many components of macOS and apps used sqlite to store data. Attackers can leverage this tool to discover sensitive data."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING ONE-LINER CAN BE USED TO KILL FIREFOX AND DUMP COOKIE DATA FROM THE USER'S FIREFOX PROFILE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="killall firefox; find ~/Library/Application\\ Support/Firefox/Profiles/. | grep cookies.sqlite | xargs -I {} sqlite3 {} \"select * from moz_cookies\"" nocase

condition:
    $a0
}

rule sqlite32
{
    meta:
        id = "2h13npGK8Bfps3xkqLIL8N"
        fingerprint = "2f4610a15d3d4b3c03d203884937a5073b257ab031f284a24068c0c00d01a552"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "sqlite is a command-line utility that allows users to query and manage sqlite databases. Many components of macOS and apps used sqlite to store data. Attackers can leverage this tool to discover sensitive data."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING SQLITE COMMAND IS COMMONLY USED BY MACOS MALWARE TO VIEW THE URL IN WHICH THE PAYLOAD WAS DOWNLOADED FROM."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'select LSQuarantineDataURLString from LSQuarantineEvent'" nocase

condition:
    $a0
}

