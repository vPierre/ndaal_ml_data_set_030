rule nvram0
{
    meta:
        id = "2ewrX7ahGO6mkX7WDDPJJP"
        fingerprint = "ca39873e81db04510d339335ebac3061cf2d8ab5e58d2c5921b808606c923e0c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "THE -P OPTION PRINTS ALL THE NVRAM VARIABLES THAT CONTAIN SOME POTENTIALLY SENSITIVE INFORMATION LIKE WIFI SSIDS AND BLUETOOTH DEVICES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="nvram -p" nocase

condition:
    $a0
}

