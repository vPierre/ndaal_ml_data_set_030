rule csrutil0
{
    meta:
        id = "6FTPaYw08cpuCbxnFQ9Jz7"
        fingerprint = "e6269079ea70bbfaae74f063ea4264807ca9aec83a365f40bb257f01da8b844e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "DISABLE SIP (SYSTEM INTEGRITY PROTECTION) - REQUIRES BOOTING INTO RECOVERY MODE"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="csrutil disable" nocase

condition:
    $a0
}

rule csrutil1
{
    meta:
        id = "4C3kvlHxs6qwOhGa4Xgrkc"
        fingerprint = "5796fb6123817528ebd3984538ea52754cf930a4d786c957764939cddc06eb4f"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "WHEN AUTHENTICATED-ROOT IS DISABLED, BOOTING IS ALLOWED FROM NON-SEALED SYSTEM SNAPSHOTS - REQUIRES BOOTING INTO RECOVERY MODE"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="csrutil authenticated-root disable" nocase

condition:
    $a0
}

rule csrutil2
{
    meta:
        id = "3nvoggg6HfEkYQvj6LN1dv"
        fingerprint = "9387f4d6a2e12ceaf90892f819c69842b62c9508afa4009ea34083ac39543448"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "INSERT A NEW IPV4 ADDRESS IN THE LIST OF ALLOWED NETBOOT SOURCES"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="csrutil netboot add <address>" nocase

condition:
    $a0
}

rule csrutil3
{
    meta:
        id = "m1dO29wSoXQOI4bFXtbGK"
        fingerprint = "a100f49c20c030030269a46d9875b8526714dd1c3a331f08b309a73bc7da9661"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "LIST ALLOWED NETBOOT SOURCES"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="csrutil netboot list" nocase

condition:
    $a0
}

