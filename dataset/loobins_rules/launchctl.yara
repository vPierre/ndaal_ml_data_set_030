rule launchctl0
{
    meta:
        id = "reazeJLMc45iiqQWzuU2u"
        fingerprint = "bf32b6682fa8d4d05d78d4b5f10be43ff7d9205928968de972ad453b62d30289"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "launchctl can be used to load, start, stop, and unload macOS services. It is a command-line frontend to launchd."
        category = "TECHNIQUE"
        technique = "A ONELINER THAT WILL LOAD A PLIST AS A LAUNCHAGENT OR LAUNCHDAEMON, ACHIEVING PERSISTENCE ON A TARGET MACHINE. THIS COMMAND REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo launchctl load /Library/LaunchAgent/com.apple.installer" nocase

condition:
    $a0
}

