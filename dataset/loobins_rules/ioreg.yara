rule ioreg0
{
    meta:
        id = "7BZjO19otDFEyBZ2bQZy1I"
        fingerprint = "e553d6676616953e91fbc35ae4719320f03134d52caaa20b00a43698e7233608"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND WILL DISPLAY OF LIST OF KEYS THAT CONTAIN \"CGSSESSION\". IF THE KEY \"CGSSESSIONSCREENISLOCKED\" US PRESENT, THE SCREEN IS ACTIVELY LOCKED."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ioreg -n Root -d1 -a | grep CGSSession" nocase

condition:
    $a0
}

rule ioreg1
{
    meta:
        id = "26ZQAML8efLUewnLlU4Ikw"
        fingerprint = "7da5525499c491157991651028f1d009e881c16f65a3427afcafdb603e6418a7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "CHECK THE OUTPUT OF THIS COMMAND (SPECIFICALLY THE IOPLATFORMSERIALNUMBER, BOARD-ID, AND MANUFACTURER FIELDS) TO CHECK WHETHER OR NOT THIS HOST IS IN A VIRTUAL MACHINE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ioreg -rd1 -c IOPlatformExpertDevice" nocase

condition:
    $a0
}

rule ioreg2
{
    meta:
        id = "78Atp4JgAGOh15tLcE5Who"
        fingerprint = "7eb41c99732a37ac53ed6fe60d5bdfc8c76ae4b91eab97133fb17a5daa7c27ce"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "GREP FOR \"USB VENDOR NAME\" VALUES TO VIEW USB VENDOR NAMES. ON VIRTUALIZED HARDWARE THESE VALUES MAY CONTAIN THE HYPERVISOR NAME SUCH AS \"VIRTUALBOX\". THIS IS AN ADDITIONAL WAY TO CHECK FOR VIRTUALIZATION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ioreg -rd1 -c IOUSBHostDevice" nocase

condition:
    $a0
}

rule ioreg3
{
    meta:
        id = "6289lGTKyfzpV5NlMvr1o9"
        fingerprint = "4487f382144de5c218a0060732531ef4fe51083b296a711e166c6700a1aa068c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "GREP FOR \"VIRTUAL BOX\", \"ORALCE\", AND \"VMWARE\" FROM THE OUTPUT OF THE IOREG -L COMMAND. THIS IS AN ADDITIONAL WAY TO CHECK FOR VIRTUALIZATION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ioreg -l" nocase

condition:
    $a0
}

