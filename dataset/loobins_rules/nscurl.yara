rule nscurl0
{
    meta:
        id = "Mr96iLIyEaxoCJCWko6p6"
        fingerprint = "fc1343bdeb3b7882408f152047d4287d68dbe24715a8e06479c2542d8095e7a1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "macOS version of curl that is used to download files to a target without applying the quarantine extended attribute"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE AND IGNORE CERT CHECKING"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="nscurl -k  -o /private/tmp/google" nocase

condition:
    $a0
}

