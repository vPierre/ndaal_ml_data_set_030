rule dns_sd0
{
    meta:
        id = "1CwZdqnPQfPAkYYT9hcuc0"
        fingerprint = "db4c4c856386e72849687e40d3e46487b4cc388bf51ade7c5f042d027b56b986"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING SSH CAN BE DISCOVERED USING THE _SSH._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dns-sd -B _ssh._tcp" nocase

condition:
    $a0
}

rule dns_sd1
{
    meta:
        id = "14rw74KEiVdiRoywayLZ38"
        fingerprint = "db44ca58f008352144ef13be535b18d2a3c5ac82d4c557ce7c83e2b46c015630"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING WEB SERVICES CAN BE DISCOVERED USING THE _HTTP._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dns-sd -B _" nocase

condition:
    $a0
}

rule dns_sd2
{
    meta:
        id = "6jEJ5rDsSVEZ86w9x8E8zj"
        fingerprint = "f4f864dbddec0554729e32303a0eae0ab7ac0f9fd1c22188e3a5bf4fef1f8e59"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING REMOTE SCREEN SHARING CAN BE DISCOVERED USING THE _RFB._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dns-sd -B _rfb._tcp" nocase

condition:
    $a0
}

rule dns_sd3
{
    meta:
        id = "50oxFRy2wuHf9VI2tBfcH"
        fingerprint = "d883938b39855033bf49a08ee720cf4ed52049997199a4dad460b3c4c3d2851b"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING SMB CAN BE DISCOVERED USING THE _SMB._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dns-sd -B _smb._tcp" nocase

condition:
    $a0
}

