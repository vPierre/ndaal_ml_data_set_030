rule pbpaste0
{
    meta:
        id = "3IhiqsHMTC71DNKb3D1DeD"
        fingerprint = "37f40c1d39e068ccc89610a3cedf06aa215f142eeb0df4ed00af8cb5a06d6140"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Retrieves the contents of the clipboard (a.k.a. pasteboard) and writes them to the standard output (stdout). The utility is often used for creating new files with the clipboard content or for piping clipboard contents to other commands. It can also be used in shell scripts that may require clipboard content as input."
        category = "TECHNIQUE"
        technique = "A PBPASTE BASH LOOP CAN CONTINOUSLY COLLECT CLIPBOARD CONTENTS EVERY X MINUTES AND WRITE CONTENTS TO A FILE (OR ANOTHER LOCATION). THIS MAY ALLOW AN ATTACKER TO GATHER USER CREDENTIALS OR COLLECT OTHER SENSITIVE INFORMATION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="while true; do echo $(pbpaste) >> loot.txt; sleep 10; done" nocase

condition:
    $a0
}

