rule networksetup0
{
    meta:
        id = "2QqnSyxiSoCLqd4TpB7Q4z"
        fingerprint = "abc0a1d8d01e6dad3fcd1e21a52cf3ae79b1b4ca6cb86c727c31756e797d2f90"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO DISPLAY SERVICES WITH CORRESPONDING PORT AND DEVICE IN ORDER THEY ARE TRIED FOR CONNECTING TO A NETWORK."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -listnetworkserviceorder" nocase

condition:
    $a0
}

rule networksetup1
{
    meta:
        id = "55WseTeg8aITGizqoCEyp3"
        fingerprint = "9f265ac73f24214da0cc607d35631ef8a03f5aaa09c843ec8b28b6120315ca48"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO DETECT NEW NETWORK HARDWARE AND CREATE A DEFAULT NETWORK SERVICE ON THE HARDWARE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -detectnewhardware" nocase

condition:
    $a0
}

rule networksetup2
{
    meta:
        id = "60ZTeevQPU7EeSkJtQY0Fr"
        fingerprint = "937232122526a67304a5d504c0038ea455f3e02e49772f6b37f92375a6aa29ac"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO LIST ALL NETWORK INTERFACES, PROVIDING NAME, DEVICE NAME, MAC ADDRESS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -listallhardwareports" nocase

condition:
    $a0
}

rule networksetup3
{
    meta:
        id = "6kWpuM3Y3m1jZi3IDahsbJ"
        fingerprint = "dbb0017882ab200372c03cece464e8ed249de34ff094253000bf05f5e8416b32"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO LIST ALL NETWORK INTERFACE NAMES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -listallnetworkservices" nocase

condition:
    $a0
}

rule networksetup4
{
    meta:
        id = "6Z642EyHvAKXygoaXutxDC"
        fingerprint = "32f11911999069e07fb256dc5001ce9972c4ed172427ab3b4953f5d4b0886114"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO GET CONFIGURED DNS SERVERS FOR A SPECIFIC INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -getdnsservers Wi-Fi" nocase

condition:
    $a0
}

rule networksetup5
{
    meta:
        id = "51c1yyo9p2SzXaxg8YZrwW"
        fingerprint = "d5095e963fce675c24f4077ae7f4a4c98ccad3b9e2276cfa27226013364a1e45"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "DISPLAYS WEB PROXY AUTO-CONFIGURATION INFORMATION FOR THE SPECIFIED INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -getautoproxyurl \"Thunderbolt Ethernet\"" nocase

condition:
    $a0
}

rule networksetup6
{
    meta:
        id = "2n6p52lLrdljGzxjbXLIN7"
        fingerprint = "7c62f18a79b916b2eefc8b618a5d43a35f7fdb1641695296f292cea88d623c66"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "DISPLAYS STANDARD WEB PROXY INFORMATION FOR THE SPECIFIED INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -getwebproxy \"Wi-Fi\"" nocase

condition:
    $a0
}

rule networksetup7
{
    meta:
        id = "621kC9suKGWAMLo7Acr1tT"
        fingerprint = "5df8d7acb08f47de363bd3b3da6033a1fcdd375913d62887ead792d52aa704d7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO SET THE HTTPS WEB PROXY FOR AN INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -setsecurewebproxy \"Wi-Fi\" 46.226.108.171" nocase

condition:
    $a0
}

rule networksetup8
{
    meta:
        id = "6tuSL5wRFYKUEfkv4rh8At"
        fingerprint = "171c35612d2bfc57411d90365190dbb4a01b52fb4ebe262918b30d292187606c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO SET THE HTTP WEB PROXY FOR AN INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -setwebproxy \"Wi-Fi\" 46.226.108.171" nocase

condition:
    $a0
}

rule networksetup9
{
    meta:
        id = "LbAgdWqNusRNoWSxP2Qb3"
        fingerprint = "fa154887ec52b739accd3191281f37507dde03430a83b730ad4366d23c019372"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO SET THE PROXY URL FOR AN INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -setautoproxyurl \"Wi-Fi\" $autoProxyURL" nocase

condition:
    $a0
}

rule networksetup10
{
    meta:
        id = "3TDaAA8052STH9aJIThpLI"
        fingerprint = "8f914855a9b8ef71942452cd16c4b4cbff7c95c6499df9f191088007103e9a02"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO ENABLE THE PROXY AUTO-CONFIG"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="networksetup -setautoproxystate \"Wi-Fi\" on" nocase

condition:
    $a0
}

