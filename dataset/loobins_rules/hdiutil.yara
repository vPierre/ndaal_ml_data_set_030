rule hdiutil0
{
    meta:
        id = "5gaEnHdFHGZV1AvRoM7vaw"
        fingerprint = "3c575be95250f54d286642cee641573513dcc8201d8758c8f9a254ad29f76b7a"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS DMG FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="hdiutil mount malicious.dmg" nocase

condition:
    $a0
}

rule hdiutil1
{
    meta:
        id = "1sGfTa2zkKMie34ZtOJ6nA"
        fingerprint = "acd1cb9df942db9c4ea37e579b26e7d049aabb769c1dfa641d7ca3d1546e9487"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS DMG FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="hdiutil attach malicious.dmg" nocase

condition:
    $a0
}

rule hdiutil2
{
    meta:
        id = "3qDoUC9iYScwYiyEBvTxms"
        fingerprint = "af5ffbe46eaec29649ecf777b94acb31325e1557aa5837f268ecad17a3ae267e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS ISO FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="hdiutil mount malicious.iso" nocase

condition:
    $a0
}

rule hdiutil3
{
    meta:
        id = "4xtSfIB5rICaiSHbKbX3Ea"
        fingerprint = "15e13632698c0ef5f74af8334dcca606556e8eb36be4fd817b116cc68ca749e3"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS ISO FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="hdiutil attach malicious.iso" nocase

condition:
    $a0
}

rule hdiutil4
{
    meta:
        id = "5IEYLR6A0mHHW6sSvZ0XDK"
        fingerprint = "a60d894c4b6bcff3509393401974eec71677ed7f16806425f04d5c2fe467d4b4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO CREATE A DMG FILE TO STORE EXFILTRATE DATA"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="hdiutil create -volname \"Volume Name\" -srcfolder /path/to/folder -ov diskimage.dmg" nocase

condition:
    $a0
}

rule hdiutil5
{
    meta:
        id = "5lgS4habz4tRzavVjrfXw0"
        fingerprint = "4bc8b625b89a49c22eb80452f27df928d6f45581af01f89d5b31982a02c3d980"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO CREATE A DMG FILE TO STORE EXFILTRATE DATA"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="hdiutil create -encryption -stdinpass -volname \"Volume Name\" -srcfolder /path/to/folder -ov encrypteddiskimage.dmg" nocase

condition:
    $a0
}

