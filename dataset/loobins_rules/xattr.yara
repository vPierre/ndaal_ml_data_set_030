rule xattr0
{
    meta:
        id = "7jWEMnIgMdAqTcYeALY7vS"
        fingerprint = "32c239b6b3cfb6199a135465b653d7a1a39264d5fde860d135e1a43b008bcdf2"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The xattr command can be used to display, modify or remove the extended attributes of one or more files, including directories and symbolic links.  Extended attributes are arbitrary metadata stored with a file, but separate from the filesystem attributes (such as modification time or file size).  The metadata is often a null-terminated UTF-8 string, but can also be arbitrary binary data.  xattr can be used to bypass Gatekeeper."
        category = "TECHNIQUE"
        technique = "USE XATTR TO REMOVE QUARATINE EXTENDED ATTRIBUTE FROM A FILE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="xattr -d com.apple.quarantine FILE" nocase

condition:
    $a0
}

rule xattr1
{
    meta:
        id = "HRK73lFixm2GV1ANGwjac"
        fingerprint = "a3429d98fc1d4e6e18a1bda05e61b064445510d239fd5f464d3ba2e78d99a2e4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The xattr command can be used to display, modify or remove the extended attributes of one or more files, including directories and symbolic links.  Extended attributes are arbitrary metadata stored with a file, but separate from the filesystem attributes (such as modification time or file size).  The metadata is often a null-terminated UTF-8 string, but can also be arbitrary binary data.  xattr can be used to bypass Gatekeeper."
        category = "TECHNIQUE"
        technique = "USE XATTR TO REMOVE QUARATINE EXTENDED ATTRIBUTE FROM MULTIPLE FILES OR DIRECTORIES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="xattr -d -r com.apple.quarantine *" nocase

condition:
    $a0
}

