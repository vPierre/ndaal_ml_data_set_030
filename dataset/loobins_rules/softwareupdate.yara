rule softwareupdate0
{
    meta:
        id = "5qwLKCyxrEe4IHfsEn7UyB"
        fingerprint = "698446f6e9a808e154b10d64f5bd8a41ef27063b078a5f27335c55c01e0f6655"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A command-line utility for running software updates."
        category = "TECHNIQUE"
        technique = "DETERMINE OS AND SAFARI VERSION BY ENUMERATING THE AVAILABLE SOFTWARE UPDATES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="softwareupdate --list" nocase

condition:
    $a0
}

rule softwareupdate1
{
    meta:
        id = "5qarImT6M7RrIWsUZQrG40"
        fingerprint = "eef1b263fe774201a5aa73c9eb7f9238a5fa0ced60383d1e274fd21e5de601e7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A command-line utility for running software updates."
        category = "TECHNIQUE"
        technique = "USE THE --SCHEDULE FLAG TO RETURN THE OS UPDATE POLICY."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="softwareupdate --schedule" nocase

condition:
    $a0
}

