rule osascript0
{
    meta:
        id = "6ImUgZWb3Vy7FV7nzEv1ou"
        fingerprint = "1497df44ff74b73eb1fb99da1c57cefbf41e844c3cd00d0102e641290c0b29af"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The osascript binary is a command-line utility included in macOS that allows users to run AppleScript and Open Scripting Architecture (OSA) scripts or commands. AppleScript is a scripting language that is designed for power users to automate various tasks, application actions, and to interact with the operating system."
        category = "TECHNIQUE"
        technique = "A BASH LOOP CAN GATHER CLIPBOARD CONTENTS OVER A DEFINED TIME PERIOD. THE FOLLOWING COMMAND CALLS /USR/BIN/OSASCRIPT -E 'RETURN (THE CLIPBOARD)' INDEFINITELY EVERY 10 SECONDS AND WRITES CLIPBOARD CONTENT TO A TEXT FILE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="while true; do echo $(osascript -e 'return (the clipboard)') >> clipdata.txt; sleep 10; done" nocase

condition:
    $a0
}

rule osascript1
{
    meta:
        id = "7gIa8syJfuc6lLkXBEysVH"
        fingerprint = "9be020d3131bbbb1c8f770f7e837c2ab994f24b29295d4d7218dc666a8ef5159"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The osascript binary is a command-line utility included in macOS that allows users to run AppleScript and Open Scripting Architecture (OSA) scripts or commands. AppleScript is a scripting language that is designed for power users to automate various tasks, application actions, and to interact with the operating system."
        category = "TECHNIQUE"
        technique = "OSASCRIPT CAN BE USED TO GATHER THE OPERATING SYSTEM VERSION, CURRENT USERNAME, USER ID, COMPUTER NAME, IP ADDRESS, AND OTHER INFORMATION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="osascript -e 'return (system info)'" nocase

condition:
    $a0
}

rule osascript2
{
    meta:
        id = "1Q47t3Xk6JU3W78GvRgl30"
        fingerprint = "b5124413664bc46d220dc0188d0c01949f2d4513e86297f4958ea3c06f1cfc22"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The osascript binary is a command-line utility included in macOS that allows users to run AppleScript and Open Scripting Architecture (OSA) scripts or commands. AppleScript is a scripting language that is designed for power users to automate various tasks, application actions, and to interact with the operating system."
        category = "TECHNIQUE"
        technique = "OSASCRIPT CAN BE USED TO GENERATE A DIALOGUE BOX AND REQUEST THE USER TO ENTER THE KEYCHAIN PASSWORD."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="osascript -e 'set popup to display dialog \"Keychain Access wants to use the login keychain\" & return & return & \"Please enter the keychain password\" & return default answer \"\" with icon file \"System:Library:CoreServices:CoreTypes.bundle:Contents:Resources:FileVaultIcon.icns\" with title \"Authentication Needed\" with hidden answer'" nocase

condition:
    $a0
}

rule osascript3
{
    meta:
        id = "7j4AAYvrhtrWOrN9Ji6hps"
        fingerprint = "3965e8e5cd993ba78c573b90bf337f316b594cc30c43f2af43ee8104f00b5127"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The osascript binary is a command-line utility included in macOS that allows users to run AppleScript and Open Scripting Architecture (OSA) scripts or commands. AppleScript is a scripting language that is designed for power users to automate various tasks, application actions, and to interact with the operating system."
        category = "TECHNIQUE"
        technique = "JXA IS OFTEN USED BY RED TEAMS (AND POTENTIALLY ATTACKERS) AS A MACOS PAYLOAD, AS JXA IS NATIVE TO MACOS AND CAN ACCESS VARIOUS INTERNAL MACOS APIS (SUCH AS COCOA, FOUNDATION, OSAKIT, ETC.). THE OSASCRIPT BINARY CAN BE USED TO EXECUTE JXA PAYLOADS BY SIMPLY RUNNING \"OSASCRIPT [FILE.JS]\" BUT SOME MALWARE OR OFFENSIVE TOOLS MAY ALSO USE \"OSASCRIPT -L JAVASCRIPT [FILE.JS]\"."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="echo \"ObjC.import('Cocoa');\\nObjC.import('stdlib');\\nvar currentApp = Application.currentApplication();\\ncurrentApp.includeStandardAdditions = true;\\ncurrentApp.doShellScript('open -a Calculator.app');\" > calc.js && osascript -l JavaScript calc.js" nocase

condition:
    $a0
}

