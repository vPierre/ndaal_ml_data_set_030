rule sysctl0
{
    meta:
        id = "6t6XCGV0OmRwGq1KZGuMNQ"
        fingerprint = "c7a7e3d7485cf37dd51df20b0ab08322a7b2e335b22e57060b4aff885304b65e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Gets the macOS hardware information, which can be used to determine whether the target macOS host is running on a physical or virtual machine."
        category = "TECHNIQUE"
        technique = "SYSCTL CAN BE USED TO GATHER INTERESTING MACOS HOST DATA, INCLUDING HARDWARE INFORMATION, MEMORY SIZE, LOGICAL CPU INFORMATION, ETC."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sysctl -n hw.model" nocase

condition:
    $a0
}

