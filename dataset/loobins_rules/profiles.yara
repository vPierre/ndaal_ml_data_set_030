rule profiles0
{
    meta:
        id = "1BIU1Ylm4WTBkgYlu9fikZ"
        fingerprint = "3aa57b5b30c2bbed29dd23cc0c7360eacf39fcafc9d08df7df1062d76aa2a48c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Profiles on macOS are responsible for managing different types of profiles including configuration, provisioning, bootstraptoken, or enrollment. However, starting from macOS 11.0, this tool cannot be used for installing configuration profiles."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DETERMINES WHETHER DEVICE IS DEP(DEVICE ENROLMENT PROGRAM) ENABLED AND OUTPUT THE DEP INFORMATION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo profiles show -type enrollment" nocase

condition:
    $a0
}

rule profiles1
{
    meta:
        id = "2tXAmy3MCq5qwZugNGFb2k"
        fingerprint = "fe58e4ea3a86e09ab5848b808868238ca80bccddee77c99ad7b2952816959afd"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Profiles on macOS are responsible for managing different types of profiles including configuration, provisioning, bootstraptoken, or enrollment. However, starting from macOS 11.0, this tool cannot be used for installing configuration profiles."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DELETES THE SPECIFIED PROFILES. AN OPTIONAL PASSWORD USED WHEN REMOVING A CONFIGURATION PROFILE WHICH REQUIRES THE PASSWORD REMOVAL OPTION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="profiles remove -identifier com.profile.identifier -password <password>" nocase

condition:
    $a0
}

