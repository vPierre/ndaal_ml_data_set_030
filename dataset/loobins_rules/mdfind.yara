rule mdfind0
{
    meta:
        id = "4quXgfshWjb4zHOm2lotVo"
        fingerprint = "183b53bb00c802bc6075656fe73171c5a5adf7b240dfec3e094f6ba551230b2d"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdfind to locate files on MacOS by searching a pre-built database. It is a command-line alternative to Spotlight in MacOS"
        category = "TECHNIQUE"
        technique = "A BASH OR ZSH ONELINER CAN CAUSE MDFIND TO PROVIDE AN ATTACKER WITH LIVE UPDATES TO THE NUMBER OF FILES ON A SYSTEM."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="mdfind -live passw" nocase

condition:
    $a0
}

rule mdfind1
{
    meta:
        id = "3LQRPEXhEVGtJLOIo1QOis"
        fingerprint = "861d63866b1fecc57cdc773711a6dacc9271a15ff3e18af2dff3b7476abd5301"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdfind to locate files on MacOS by searching a pre-built database. It is a command-line alternative to Spotlight in MacOS"
        category = "TECHNIQUE"
        technique = "ALLOWS AN ATTACKER TO QUERY THE FILESYSTEM VIA THE COMMANDLINE/TERMINAL TO SEARCH FOR AWS KEYS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="mdfind 'kMDItemTextContext == AKIA || kMDItemDisplayName = *AKIA* -onlyin ~'" nocase

condition:
    $a0
}

rule mdfind2
{
    meta:
        id = "30htHqerjVORbG9NJoV2VX"
        fingerprint = "1b1d37408a227d619b1e1b6b690a6176f7a9bedbd24c5b53ba02e56f55d1f0e8"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "mdfind to locate files on MacOS by searching a pre-built database. It is a command-line alternative to Spotlight in MacOS"
        category = "TECHNIQUE"
        technique = "ALLOWS AN ATTACKER TO DETERMINE IF SPECIFIC APPLICATIONS ARE INSTALLED AND CAN BE LEVERAGED"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="set appId to do shell script \"mdfind kMDItemCFBundleIdentifier = '\" & bundleId & \"'\"" nocase

condition:
    $a0
}

