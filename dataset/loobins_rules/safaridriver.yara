rule safaridriver0
{
    meta:
        id = "lRLjWRewGHEHevClTCgtb"
        fingerprint = "fec116c20e6af584a0da9987879e0b74d28d23c2602f1a00c2cb0a4eb77c6deb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "safaridriver is a tool that is used to enable Selenium testing via the macOS WebDriver protocol. Once enabled, the WebDriver API could be abused by attackers to communicate with external servers for command and control or exfiltration purposes."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO ENABLE THE WEBDRIVER SAFARI BROWSER API. THE COMMAND MUST BE RUN AS ROOT OR WITH SUDO PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo safaridriver --enable" nocase

condition:
    $a0
}

