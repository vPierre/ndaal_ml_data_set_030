rule GetFileInfo0
{
    meta:
        id = "24mH8At3gFo9nJo2Paxk2s"
        fingerprint = "46cbc43c2d0c20932711b20fa00877dcdfeced7db2259ae5070b0502774aadda"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Uses the CommandLine/Terminal to return type, creator, attributes, created, and modified file information of a file or directory."
        category = "TECHNIQUE"
        technique = "A BASH OR ZSH ONELINER CAN PROVIDE AN ATTACKER WITH INFORMATION ABOUT SPECIFIC FILES OF INTEREST."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="for FILE in ~/Downloads/*; do echo $(GetFileInfo $FILE) >> fileinfo.txt; sleep 2; done" nocase

condition:
    $a0
}

