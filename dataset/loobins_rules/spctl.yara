rule spctl0
{
    meta:
        id = "6XpHnRRyY2qyHDYASlf4ca"
        fingerprint = "e1e21e0f49334dee4f67e59d57f228ee8ea2d5c83d3df36d33b0f6fbaf6258c5"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "THE --MASTER-DISABLE SWITCH DISABLES GATEKEEPER. THE COMMAND MUST BE RUN WITH ROOT/SUDO PERMISSION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo spctl --master-disable" nocase

condition:
    $a0
}

