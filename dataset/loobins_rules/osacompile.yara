rule osacompile0
{
    meta:
        id = "6hpgtRUOKhKBthpcRB7Wpu"
        fingerprint = "73957bbe9dbcbae37457d4d9a56dceeca9dbd24e56513cbbe2abbe9d313db7c6"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "osacompile is a utility used to compile scripts into executables. It's a component of Open Scripting Architecture (OSA) that Apple uses for its scripting languages, like AppleScript and JavaScript for Automation (JXA). osacompile accepts AppleScript code as input and produces a compiled script file, which can be either a script file (.scpt), an app (.app), a droplet, or a script bundle."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DOWNLOADS AN APPLESCRIPT PAYLOAD FROM GETPAYLOAD.COM AND COMPILES IT INTO AN APP."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="curl  && osacompile -x -e payload_code.apple_script -o payload.app" nocase

condition:
    $a0
}

