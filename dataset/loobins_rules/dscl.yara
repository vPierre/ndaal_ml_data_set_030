rule dscl0
{
    meta:
        id = "6Hzi4VL0JZ1N0NwS852JU2"
        fingerprint = "2fa32ea4abf0e9d62d666df936d28869df9e2860d7ee55d6589dc11a9748108c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL LOCAL USERS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . -list /Users" nocase

condition:
    $a0
}

rule dscl1
{
    meta:
        id = "4utpk15rutyk9TOxAkg7Ni"
        fingerprint = "61fb37c45972e00f9265cf7a9bf538e5f7d6a374d893f3054b6fa09c2bff713b"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL ACTIVE DIRECTORY USERS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl \"/Active Directory/TEST/All Domains\" -list /Users" nocase

condition:
    $a0
}

rule dscl2
{
    meta:
        id = "7KX3cUt7a0KKDCp9CK0iA3"
        fingerprint = "b8116045f8bff142aa6191c7e8e50c44073a89f4ad6142449ab06a4b0214d266"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL LOCAL USER INFORMATION SUCH AS WHEN THEIR PASSWORD WAS LAST SET, THEIR KEYBOARD LAYOUT, THEIR AVATAR, THEIR HOME DIRECTORY, UID AND DEFAULT SHELL."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . -read /Users/$USERNAME" nocase

condition:
    $a0
}

rule dscl3
{
    meta:
        id = "438ARCgXSWCazwLMQP1NGE"
        fingerprint = "93be7aa4b6ffeb4179e05a321bf8af57fe451ca5f5f9fb4d5ff8aa96f54d0ed7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL ACTIVE DIRECTORY USER INFORMATION SUCH AS WHEN THEIR PASSWORD WAS LAST SET, THEIR KEYBOARD LAYOUT, THEIR AVATAR, THEIR HOME DIRECTORY, UID AND DEFAULT SHELL."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl \"/Active Directory/TEST/All Domains\" -read /Users/$USERNAME" nocase

condition:
    $a0
}

rule dscl4
{
    meta:
        id = "6wfc3omy0IQ3JQbEekzmn9"
        fingerprint = "f6756d2969ed2c2309c13f98247bc1f2ea275162966bc8a21ff40a4cd6d9376e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL LOCAL GROUPS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . -list /Groups" nocase

condition:
    $a0
}

rule dscl5
{
    meta:
        id = "7IxyFlkAUoEVSt73ElTvc7"
        fingerprint = "0240cb8169db3902728059f4de569e1f59654836d7dca515fd66793e60387c83"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL ACTIVE DIRECTORY GROUPS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl \"/Active Directory/TEST/All Domains\" -list /Groups" nocase

condition:
    $a0
}

rule dscl6
{
    meta:
        id = "6eLhYWHB5IGemqxHyuFH98"
        fingerprint = "35b0dac1e6d10043e30a65564f3c6f66db9cceca86d88e1a205ede250dd1bd93"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL LOCAL GROUP INFORMATION SUCH AS WHICH USERS BELONG TO THAT GROUP, SMB SIDS AND GROUP ID. ESPECIALLY USEFUL FOR THE \"ADMIN\" GROUP."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . -read /Groups/$GROUPNAME" nocase

condition:
    $a0
}

rule dscl7
{
    meta:
        id = "59Wqgr8AGDmJqh9mNoxeBz"
        fingerprint = "9919e44b9d4c074d5bf47188204c009c0a7f323a53e0d09da2fe66cbb6c843da"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN USEFUL ACTIVE DIRECTORY GROUP INFORMATION SUCH AS WHICH USERS BELONG TO THAT GROUP, SMB SIDS AND GROUP ID. ESPECIALLY USEFUL FOR THE \"ADMIN\" GROUP."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl \"/Active Directory/TEST/All Domains\" -read /Groups/$GROUPNAME" nocase

condition:
    $a0
}

rule dscl8
{
    meta:
        id = "6gctCc7sd60BgYmlP9KjjR"
        fingerprint = "42a7bd4511d5191e18a73da091a8733f7f7c3c317af9b66a76f9939a44ef52da"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL COMPUTERS IN AN ACTIVE DIRECTORY."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl  \"/Active Directory/TEST/All Domains\" -list /Computers" nocase

condition:
    $a0
}

rule dscl9
{
    meta:
        id = "5CBvGnZdDTia6e8hshCX4l"
        fingerprint = "0a7e767e2d70542ac57449685df1ce96c94a2f430af908a49654b693ac054743"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "ENUMERATE ALL SHARES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . -list /SharePoints" nocase

condition:
    $a0
}

rule dscl10
{
    meta:
        id = "2mionUtZLQXA1o90MbrvJZ"
        fingerprint = "2930c74f86c0349de6fcb8351e085af9bcfa05f81a30f7c01e3fc6e0722b451e"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "GAIN PASSWORD POLICY INFORMATION"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . -read /Config/shadowhash" nocase

condition:
    $a0
}

rule dscl11
{
    meta:
        id = "4KwQVa78t0QXz1Lnlrz0hO"
        fingerprint = "1e4d016e965776fdfb7b13760274d55de11352d4af8fdae48031fb66bb411165"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "An extensive tool for communicating with the Directory Services, useful for Discovery."
        category = "TECHNIQUE"
        technique = "CHANGE AN EXISTING USER'S PASSWORD."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dscl . passwd /Users/$USERNAME oldPassword newPassword" nocase

condition:
    $a0
}

