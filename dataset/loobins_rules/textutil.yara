rule textutil0
{
    meta:
        id = "4S3oCMfEl4oVSMzMzymCzW"
        fingerprint = "33a40925964ddf8b4c566cb857831cb94f97a8e5c4421ef55ef766c3982999b8"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The textutil binary is a command-line utility included in macOS that allows users to manipulate text files of various formats, using the mechanisms provided by the Cocoa text system. Formats include rtf, html, docx and others"
        category = "TECHNIQUE"
        technique = "A ONE-LINER CAN LOAD THE CONTENT OF MULTIPLE RTF FILES IN A DIRECTORY, CONCATENATE THEIR CONTENTS, AND WRITE THE RESULTS OUT AS A NEW FILE. THIS PROVIDES TWO SUB-USE-CASES; ONE IS BUILDING A MALICIOUS FILE FROM A COLLECTION OF SMALLER FILES WHICH COULD EVADE BOTH NETWORK AND HOST-BASED SECURITY CONTROLS AS THE TRADITIONAL MEANS OF SIGNATURE-BASED DETECTION WOULD BE REDUNDANT; TWO IS CONCATENATING THE CONTENT OF SEVERAL, POTENTIALLY SENSITIVE FILES BEFORE EXFILTRATION. THIS COMMAND CAN ALSO BE LOOPED TO ITERATE A DIRECTORY OF FILES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="textutil -convert html Quote.doc secondQuote.doc" nocase

condition:
    $a0
}

rule textutil1
{
    meta:
        id = "3da5yYxf01X4nyUHmY2ng"
        fingerprint = "6ab79a8afae84edd5e03e86be4028c06eae34640c44a904db716c5d7bdd3f991"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The textutil binary is a command-line utility included in macOS that allows users to manipulate text files of various formats, using the mechanisms provided by the Cocoa text system. Formats include rtf, html, docx and others"
        category = "TECHNIQUE"
        technique = "BY LEVERAGING ANOTHER COMMAND LINE TOOL, PBPASTE, IT IS POSSIBLE TO WRITE A ONE-LINER WHICH CAPTURES THE CONTENT OF THE CLIPBOARD. IF AN ATTACKER ALREADY HAS ACCESS TO THE SYSTEM, THE ATTACKER COULD RUN THIS COMMAND TO OBTAIN SENSITIVE INFORMATION SUCH AS A PASSWORD AND THEN ELEVATE THEIR PRIVILEGES OR EXFILTRATE THE INFORMATION."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="pbpaste | textutil -stdin -info > Clipboard.txt" nocase

condition:
    $a0
}

