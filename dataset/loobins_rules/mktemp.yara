rule mktemp0
{
    meta:
        id = "5vmyzqvQle8KN0fODp5bde"
        fingerprint = "fd7dfa3648472526ea939c2a6a5b85a58fe186af9cd1872b97d498fc46b486d1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The mktemp binary located in \"usr/bin/mktemp\" can generate unique directory or file names and has historically been used to generate unique payloads."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO GENERATE A RANDOM DIRECTORY NAME FOR STAGING PAYLOADS"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="export tmpDir=\"$(mktemp -d /tmp/XXXXXXXXXXXX)\"" nocase

condition:
    $a0
}

rule mktemp1
{
    meta:
        id = "2PrGuouXWJjXhR6rYn3PQO"
        fingerprint = "2860ccdd81a60c9a2576d9a46f5282eaa6f4b72ce5e764bda3a3bbf295f70a76"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The mktemp binary located in \"usr/bin/mktemp\" can generate unique directory or file names and has historically been used to generate unique payloads."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO GENERATE A UNIQUE DIRECTORY BASED ON A TEMPLATE"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="TMP_DIR=\"mktemp -d -t x\"" nocase

condition:
    $a0
}

