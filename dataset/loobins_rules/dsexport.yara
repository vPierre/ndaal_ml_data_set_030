rule dsexport0
{
    meta:
        id = "1hCKBIXyKlAbS6HGZaIGcY"
        fingerprint = "53c738edf82ebb5af8a353f4c9cb4ff69234942fccf1991f09dc473a52495996"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dsexport is a command-line utility designed to export records from the directory services database on a local host or from a connected LDAP service. The tool can be used to gather information about users, groups, and computers. The tool can also be used to export the directory services database to a file for offline analysis."
        category = "TECHNIQUE"
        technique = "EXPORT THE LOCAL HOST USER INFORMATION TO A FILE"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dsexport local_users.txt /Local/Default dsRecTypeStandard:Users" nocase

condition:
    $a0
}

rule dsexport1
{
    meta:
        id = "4tpux9jPlld9SsGTuF333P"
        fingerprint = "5c37b447d4377d2893b67aa88fd341d579967367238b8e41c22bfc3af60433eb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "dsexport is a command-line utility designed to export records from the directory services database on a local host or from a connected LDAP service. The tool can be used to gather information about users, groups, and computers. The tool can also be used to export the directory services database to a file for offline analysis."
        category = "TECHNIQUE"
        technique = "EXPORT THE LOCAL HOST GROUP INFORMATION TO A FILE"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="dsexport local_groups.txt /Local/Default dsRecTypeStandard:Groups" nocase

condition:
    $a0
}

