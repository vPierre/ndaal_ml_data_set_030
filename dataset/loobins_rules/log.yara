rule log0
{
    meta:
        id = "3zluBBUOWYkcBPin9k1TQu"
        fingerprint = "1544aeea38669af6c178118a253a55d40f03fd6cb53e0b256ed8b58fac9c880d"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The log command can be used to access system log messages from Apple Unified Logging (AUL). The tool can be used to inspect exiting logs, stream logs in realtime, and delete logs. This tool is normally used by system admins and application developers for troubleshooting purposes but can be used by an adversary to gain an understanding of the user's behavior or to cover up their tracks by deleting log messages."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN COVER UP THEIR TRACKS BY REMOVING ALL LOG MESSAGES USING THE FOLLOWING COMMAND. REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="log erase --all" nocase

condition:
    $a0
}

