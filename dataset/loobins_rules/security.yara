rule security0
{
    meta:
        id = "QCeobejajfEd4KLKow7DN"
        fingerprint = "4ac7832ea3dc15c818a14d84d015f8d42bb934b24f37ad6ca2508a52373eea69"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "security is a command-line utility included in macOS that allows users to interact with the Keychain app. Keychains allow users to manager passwords and credentials for many services and features, including Wi-Fi and website passwords, secure notes, certificates, and Kerberos."
        category = "TECHNIQUE"
        technique = "THIS COMMAND WILL DUMP KEYCHAIN PASSWORDS FROM LOGIN.KEYCHAIN"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo security dump-keychain -d login.keychain" nocase

condition:
    $a0
}

rule security1
{
    meta:
        id = "4OQhn5qK02zSWir2EyOgTi"
        fingerprint = "6a3de51976cea1f8f660ee5ec0052bd9e2dc3920a7a1ccca6edb55cc51e9cfa4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "security is a command-line utility included in macOS that allows users to interact with the Keychain app. Keychains allow users to manager passwords and credentials for many services and features, including Wi-Fi and website passwords, secure notes, certificates, and Kerberos."
        category = "TECHNIQUE"
        technique = "THIS COMMAND WILL RETRIEVE THE CHROME SAFE STORAGE PASSWORD MANAGER SECRET FROM THE KEYCHAIN."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="security find-generic-password -w -s \"Chrome Safe Storage\"" nocase

condition:
    $a0
}

rule security2
{
    meta:
        id = "6E4BuDdGWPHqJrRtnq4Bz7"
        fingerprint = "1128f8a3608866e9667fd96e3489b2c39eb9affdb965e402318f9be99cf353d6"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "security is a command-line utility included in macOS that allows users to interact with the Keychain app. Keychains allow users to manager passwords and credentials for many services and features, including Wi-Fi and website passwords, secure notes, certificates, and Kerberos."
        category = "TECHNIQUE"
        technique = "THIS COMMAND WILL ADD A CERTIFICATE TO THE KEYCHAIN."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain bad_cert.crt" nocase

condition:
    $a0
}

