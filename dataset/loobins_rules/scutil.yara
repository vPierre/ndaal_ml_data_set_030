rule scutil0
{
    meta:
        id = "34P1sN5UBLg1U5LinvviUj"
        fingerprint = "5b674f650c65bfa534d148daf85a8de92e37067e2b1c20bbefd831ef2849f041"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "GET THE CURRENT DNS CONFIGURATION OF THE SYSTEMS"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="scutil --dns" nocase

condition:
    $a0
}

rule scutil1
{
    meta:
        id = "6IBAStUs6LTGJZ6O7Qn3dF"
        fingerprint = "919d3025238b8d78d52c485cb7fb0d958d4404082e23f8eb5cc2a7fda7cde065"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "GET THE CURRENT PROXY CONFIGURATION OF THE SYSTEMS"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="scutil --proxy" nocase

condition:
    $a0
}

rule scutil2
{
    meta:
        id = "4jAIOl7jnCyMWHsGsFaiKT"
        fingerprint = "44b1c80903f9e722ef4555a6d643b32987b24ccf979546b4010bf4de8c66a849"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "CHECK IF THE DESTINATION HOST IS REACHABLE FROM YOUR MAC"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="scutil -r { nodename | address | local-address remote-address }" nocase

condition:
    $a0
}

rule scutil3
{
    meta:
        id = "5UpU4JnoqTCpu7RKK4w9Tk"
        fingerprint = "75c02919e587029010bfda406cbc799d51a8ac7824f2a6e6c672bd7d402a6218"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "scutil provides a command line interface to the dynamic store data maintained by configd. Interaction with this data (using the SystemConfiguration.framework SCDynamicStore APIs) is handled with a set of commands read from standard input."
        category = "TECHNIQUE"
        technique = "DISPLAY THE CURRENT HOSTNAME, LOCALHOST NAME AND COMPUTERNAME"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="scutil --get { HostName | LocalHostName | ComputerName }" nocase

condition:
    $a0
}

