rule SetFile0
{
    meta:
        id = "6t3VdpRXwOzeaQ5p12rI1i"
        fingerprint = "e37ca8a8e1a187d1cced2bef6c08fdc3d147054e7dd6b6c2f6248c140a80752f"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Uses the CommandLine/Terminal to set file and or directory attributes. It can set attributes, creator, creation date, modification date, and file type for multiple files at a time."
        category = "TECHNIQUE"
        technique = "A BASH OR ZSH ONELINER CAN ALLOW AN ATTACKER TO SET THE FILE ATTRIBUTE TO INVISIBLE. THIS ACTION CAN ESTABLISH PERSISTENCE AND EVADE DETECTION FOR MALICIOUS FILES ON THE SYSTEM."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="for FILE in ~/*; do echo $(SetFile -a V $FILE && echo $(GetFileInfo $FILE)) >> /tmp/fileinfo.txt; sleep 2; done" nocase

condition:
    $a0
}

rule SetFile1
{
    meta:
        id = "2GrctnELL4jTKMgH4eFjsa"
        fingerprint = "7952b11a1b52b67816ae5708b8fa78f7e9758b1d0d25ca450cfd63bfe1ea10f7"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "Uses the CommandLine/Terminal to set file and or directory attributes. It can set attributes, creator, creation date, modification date, and file type for multiple files at a time."
        category = "TECHNIQUE"
        technique = "SETFILE CAN BE USED WITH THE -D AND -M ARGUMENTS TO ALTER A FILE'S CREATION AND MODIFICATION DATE, RESPECTIVELY."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="SetFile -d \"04/25/2023 11:11:00\" -m \"04/25/2023 11:12:00\" targetfile.txt" nocase

condition:
    $a0
}

