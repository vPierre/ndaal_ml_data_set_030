rule open0
{
    meta:
        id = "2jb2Yy6HKvTEwiN75LBvCq"
        fingerprint = "2e16b0c5dc07feeee14d6b94e89e8315e61498308d534a878d45ab07d1c5a515"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The open command-line utility can be used to open files, folders, app, URLs or header files in their associate macOS app."
        category = "TECHNIQUE"
        technique = "THE OPEN COMMAND CAN BE USED TO OPEN A MALICIOUS MACOS APP FROM THE TERMINAL."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="open Malicious.app" nocase

condition:
    $a0
}

rule open1
{
    meta:
        id = "dBRm1qLQiNakYyc0w1vLW"
        fingerprint = "adfdb0794d18d9f3c0cf3c74f9c025605a6e33bf4318d2b78b005666de9d7af8"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The open command-line utility can be used to open files, folders, app, URLs or header files in their associate macOS app."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DOWNLOADS THE PAYLOAD.ZIP FILE IN THE DEFAULT BROWSER (SAFARI) AND THEN KILLS IT."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="open -g  sleep 3; killall Safari" nocase

condition:
    $a0
}

