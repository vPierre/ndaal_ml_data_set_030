rule lsregister0
{
    meta:
        id = "4ZyyxuwtyKztVbya13f4AU"
        fingerprint = "bfb464628d99c80bc9dba5217f037cba6f2c2ae558967970335c9f7e286ce74a"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "lsregister is used to build, dump, and check the validity of the Launch Services database. This database is often abused to create custom URL scheme handlers that point to malicious apps."
        category = "TECHNIQUE"
        technique = "THE -F FLAG CAN BE USED TO FORCE AN UPDATE OF THE LAUNCH SERVICES DATABASE. THIS CAN BE USED TO QUICKLY REGISTER A CUSTOM URL SCHEME THAT POINTS TO A MALICIOUS APP."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -f" nocase

condition:
    $a0
}

rule lsregister1
{
    meta:
        id = "7fZfupywzUyB8dBOtvkJmn"
        fingerprint = "b77aef1bb3af2f6ee8c804aa6b942016bd5d3e24422657020fade1a10927a831"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "lsregister is used to build, dump, and check the validity of the Launch Services database. This database is often abused to create custom URL scheme handlers that point to malicious apps."
        category = "TECHNIQUE"
        technique = "THE -DUMP FLAG CAN BE USED TO GET A LIST OF APPS AND THEIR BINDINGS"
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="/System/Library/Frameworks/CoreServices.framework/Versions/A/Frameworks/LaunchServices.framework/Versions/A/Support/lsregister -dump | grep -E \"path:|bindings:|name: | more\"" nocase

condition:
    $a0
}

rule lsregister2
{
    meta:
        id = "3A0cgJrMPHIfe9YXFQwUva"
        fingerprint = "e68c931511d101893b7449ffdfbe974dee9dfc3e73b1f6511ea90c758e93bcf1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "lsregister is used to build, dump, and check the validity of the Launch Services database. This database is often abused to create custom URL scheme handlers that point to malicious apps."
        category = "TECHNIQUE"
        technique = "THE -DELETE FLAG CAN BE USED TO DELETE THE LAUNCH SERVICES DATABASE TO IMPACT NORMAL OPERATION OF THE SYSTEM."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="lsregister -delete" nocase

condition:
    $a0
}

