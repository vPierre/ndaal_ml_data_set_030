rule tmutil0
{
    meta:
        id = "4sxkQK8mq2NHy4qj4ftSzM"
        fingerprint = "bf6677d9c7eda1c33905a706de9d3c5dcf0c378a3bbc65679a659c49f0b0874a"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DISABLES TIME MACHINE. AN ATTACKER CAN USE THIS TO PREVENT BACKUPS FROM OCCURRING."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="tmutil disable" nocase

condition:
    $a0
}

rule tmutil1
{
    meta:
        id = "63xybJufJ6jtPQRIj3BYLH"
        fingerprint = "7c2949b2e86c0d24754f7f0f3a43518c2a01e69326c14498672d413ff5c66e80"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DELETES THE SPECIFIED BACKUP. AN ADVERSARY MAY PERFORM THIS ACTION BEFORE LAUNCHING A RANSONWARE ATTACK TO PREVENT THE VICTIM FROM RESTORING THEIR FILES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="tmutil delete /path/to/backup" nocase

condition:
    $a0
}

rule tmutil2
{
    meta:
        id = "zXpA2UQwTzzpMKNwPWvl8"
        fingerprint = "8036dd097d7ed0e06ad3bbcf6ee7eb8ac70f7faa7f55da9bf033de779408b135"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND RESTORE THE SPECIFIED BACKUP. AN ATTACKER CAN USE THIS TO RESTORE A BACKUP OF A SENSITIVE FILE THAT WAS DELETED."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="tmutil restore /path/to/backup" nocase

condition:
    $a0
}

rule tmutil3
{
    meta:
        id = "1qGkjs7qiYeHR1SKE9VOJ5"
        fingerprint = "4702ddce4f008fb60e450569cc9d2543ae9c7be3ec25908e6fa7edd0884cfd43"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "AN ADVERSARY CAN USE THE SNAPSHOT AND RESTORE COMMANDS TOGETHER TO TAMPER WITH SYSTEM LOGS. THIS IS FIXED IN MACOS 10.15.4+."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="mkdir /tmp/snapshot" nocase
    $a1="tmutil localsnapshot" nocase
    $a2="tmutil listlocalsnapshots /" nocase
    $a3="mount_apfs -o noowners -s com.apple.TimeMachine.2023-05-01-090000.local /System/Volumes/Data /tmp/snapshot" nocase
    $a4="open /tmp/snapshot" nocase
    $a5="sudo vim /var/log/system.log" nocase
    $a6="tmutil restore com.apple.TimeMachine.2023-05-01-090000.local" nocase

condition:
    ($a0 or $a1 or $a2 or $a3 or $a4 or $a5 or $a6)
}

rule tmutil4
{
    meta:
        id = "7i682ngQ8N9PoK9OiFLjmC"
        fingerprint = "264850da9b01a83cfaebd97d43a7ad3c9ffc062361d1f161d518b944b475e77d"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tool for managing Time Machine, the native macOS backup utility."
        category = "TECHNIQUE"
        technique = "AN ADVERSARY COULD EXCLUDE A PATH FROM TIME MACHINE BACKUPS TO PREVENT CERTAIN FILES FROM BEING BACKED UP."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="tmutil addexclusion /path/to/exclude" nocase

condition:
    $a0
}

