rule defaults0
{
    meta:
        id = "54WT2g0IDEKxsnTGzvD0Vh"
        fingerprint = "80ddac73fbe473f5c8a3ad4e1014f1dfcc0bf2a3575d6a1cae0fc8d855f2a103"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND CAN BE USED TO DISABLE GATEKEEPERS REARM FUNCTIONALITY. THIS COMMAND REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo defaults write /Library/Preferences/com.apple.security GKAutoRearm -bool NO" nocase

condition:
    $a0
}

rule defaults1
{
    meta:
        id = "7AwAnCVPUkVjYytewHY8B6"
        fingerprint = "692058280e73d6bd961e498ce18eaa755e1ccd2e28acd3a7e740992c8684fabb"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "SHOW ALL MOUNTED SERVERS ON THE DESKTOP."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="defaults read com.apple.finder \"ShowMountedServersOnDesktop\"" nocase

condition:
    $a0
}

rule defaults2
{
    meta:
        id = "6HgmTbjfY1XPkhTf62vtZj"
        fingerprint = "3ace6b85f183e32fb4f0b0e4e86ffba9b22648a2a77057d6c7ee777f18e29f93"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A full length description of the binary goes here."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN USE DEFAULTS TO ADD A LOGIN HOOK IN ATTEMPT TO GAIN PERSISTENCE. THIS COMMAND REQUIRES ROOT PRIVILEGES."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="sudo defaults write /Library/Preferences/com.apple.loginwindow LoginHook gain_persistence.sh" nocase

condition:
    $a0
}

