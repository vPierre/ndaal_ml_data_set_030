rule plutil0
{
    meta:
        id = "3iNfZRSC3ip8GzhicOoQJL"
        fingerprint = "468c0444d6cbef0ea8f45623fe921bf8ddfbe7f7a56407b22aee030150ad2d18"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "plutil is a command-line utility used for managing property list (.plist) files. These files are commonly used by macOS to store a app settings and other configuration info. The utility allows users to check the validity of plist files `plutil -lint`, convert plist files between XML and binary formats (plutil -convert), and add, modify or remove plist key value pairs."
        category = "TECHNIQUE"
        technique = "PLUTIL CAN BE USED TO SET THE \"LSUIELEMENT\" ATTRIBUTE TO TUE WHICH WILL FORCE THE TARGETED APP TO RUN WITHOUT THE UI AND DOCK ICON."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="plutil -insert LSUIElement -string \"1\" /Applications/TargetApp.app/Contents/Info.plist" nocase

condition:
    $a0
}

