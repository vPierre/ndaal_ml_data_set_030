rule tclsh0
{
    meta:
        id = "9GY8aw2QzCXdfqmSqz8SV"
        fingerprint = "05ff0d9489a30cdcc315cc5439e1d768a75767921c836e39f24cca4878d6fc63"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "tclsh is a shell-like utility that runs Tcl from standard input or a file. tclsh holds the \"com.apple.security.cs.disable-library-validation\" entitlement and is capable of loading arbitary plug-ins, framework, and libraries without requiring signed code."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN EXECUTE A MALICIOUS .DYLIB FROM STDIN BY ECHOING A LOAD COMMAND AND PIPING TO TCLSH. THIS WILL BYPASS CODE SIGNING REQUIREMENTS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="echo \"load bad.dylib\" | tclsh" nocase

condition:
    $a0
}

