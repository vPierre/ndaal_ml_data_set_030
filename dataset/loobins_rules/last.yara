rule last0
{
    meta:
        id = "5n84mpw87rHMRYbd2lBuaK"
        fingerprint = "861d0139c9d1ea761dcb2350bc219599db9981946ec310004ecf4831b62f9cbf"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command shows a list of user sessions including the user name, terminal used, host name, start and stop times, and duration. It also indicates if a session is still active or was terminated unexpectedly."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND WILL DISPLAY SESSIONS THAT ARE CURRENTLY ACTIVE."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="last | grep \"still logged in\"" nocase

condition:
    $a0
}

rule last1
{
    meta:
        id = "lZ65h0XvyVeFtjRLuEJdb"
        fingerprint = "028d99470392d859446c9fb7221ebd146f342b0cace71a49d4cfdaf9987c3fe4"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command shows a list of user sessions including the user name, terminal used, host name, start and stop times, and duration. It also indicates if a session is still active or was terminated unexpectedly."
        category = "TECHNIQUE"
        technique = "THE LAST COMMAND CAN BE USED TO OUTPUT USERS WHO HAVE PREVIOUSLY LOGGED IN, BY SPECIFYING THE TTY INTERFACE 'CONSOLE'."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="last -t console" nocase

condition:
    $a0
}

rule last2
{
    meta:
        id = "1iW3yjybPHLIi1NO8noKzD"
        fingerprint = "0854577badf4dd19aa9e182354ad5aef2935ea0aed027c140cf8de15b280c08c"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "The command shows a list of user sessions including the user name, terminal used, host name, start and stop times, and duration. It also indicates if a session is still active or was terminated unexpectedly."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN USE 'LAST' WITH A FILTER TO RETRIEVE THE CONNECTION DATE AND REMOTE HOST INFORMATION FOR REMOTE LOGINS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="last | grep -E '[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+'" nocase

condition:
    $a0
}

