rule ssh_keygen0
{
    meta:
        id = "1YXO9DOniVIzSHiBiX38wJ"
        fingerprint = "01190c7fdfe95b2e174958c39bb61b643cc4593776aa57cd8ff97aa12bd2d4e1"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "ssh-keygen is a tool for creating new authentication key pairs for SSH (Secure Shell). ssh-keygen holds the \"com.apple.security.cs.disable-library-validation\" entitlement and is capable of loading arbitary libraries without requiring signed code."
        category = "TECHNIQUE"
        technique = "AN ATTACKER CAN EXECUTE A MALICIOUS .DYLIB FROM STDIN BY ECHOING A LOAD COMMAND AND PIPING TO TCLSH. THIS WILL BYPASS CODE SIGNING REQUIREMENTS."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="ssh-keygen -D /private/tmp/evil.dylib" nocase

condition:
    $a0
}

