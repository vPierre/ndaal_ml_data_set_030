rule screencapture0
{
    meta:
        id = "3SmbiunOLj4TcLkYpqQX9V"
        fingerprint = "0fe10167ae47b63a8ae5fec37634118ba5fc690d556fc84ce70f754813fe5872"
        version = "1.0"
        date = "2023-07-03"
        modified = "2023-07-03"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT IN DER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG"
        description = "A tools that allows users to take screenshots of their desktop or specific app windows. The tool can be used by malicious actors to collect sensitve information from the targeted system."
        category = "TECHNIQUE"
        technique = "THE FOLLOWING COMMAND DEMONSTRATES HOW AN ATTACKER CAN USE THE TOOL TO CAPTURE SCREENSHOTS EVERY 10 SECONDS. THE -X FLAG PREVENTS SNAPSHOT SOUNDS FROM BEING PLAYED."
        reference = "https://github.com/infosecB/LOOBins"

strings:
    $a0="while true; do ts=$(date +\"%Y%m%d-%H%M%S\"); o=\"/tmp/screenshots\"; screencapture -x \"$o/ss-$ts.png\"; sleep 10; done" nocase

condition:
    $a0
}

