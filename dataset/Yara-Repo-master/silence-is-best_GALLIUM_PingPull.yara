alert icmp any any -> any any (msg:"GALLIUM PINGPULL Detected"; content:"PROJECT_PINGPULL"; fast_pattern; within:20; dsize:<100;  classtype:trojan-activity; sid:20166342; rev:1; metadata:created_at 2022_06_13;)

rule PingPull_bin
{
    meta:
        description = "GALLIUM PingPull"
        author = "James_inthe_box"
        reference = "https://unit42.paloaltonetworks.com/pingpull-gallium/"
        date = "2022/06"
        maltype = "APT horsecrap"

    strings:
        $string1 = "Iph1psvc"
        $string2 = "PROJECT_%s_%s_%08X"

    condition:
        uint16(0) == 0x5A4D and all of ($string*) and filesize < 500KB
}

rule PingPull_mem
{
    meta:
        description = "GALLIUM PingPull"
        author = "James_inthe_box"
        reference = "https://unit42.paloaltonetworks.com/pingpull-gallium/"
        date = "2022/06"
        maltype = "APT horsecrap"

    strings:
        $string1 = "Iph1psvc"
        $string2 = "PROJECT_%s_%s_%08X"

    condition:
        all of ($string*) and filesize > 100KB
}