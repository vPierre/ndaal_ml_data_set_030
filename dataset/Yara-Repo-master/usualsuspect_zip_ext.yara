rule zip_with_ext
{
    meta:
        author = "@jaydinbas"
        description = "Only match zip files containing desired file extensions"

    strings:
        $file_sig = "PK\x03\x04"    //zip header sig
        $entry_sig = "PK\x01\x02"   //ZIPDIRENTRY sig

        //add in any file extensions/file name suffices you want
        $ext1 = ".exe" nocase
        $ext2 = ".dll" nocase
        $ext3 = ".scr" nocase
        
    condition:
        $file_sig at 0
        and for any i in (1..#entry_sig) :
        (
            for any of ($ext*) :
            (
                $ at (@entry_sig[i] + 46 + uint16(@entry_sig[i]+28) - !)
            )
        )
}
