import "math"

rule general_vba_high_entropy_function_names : General
{
    meta:
        author = "threatintel@volexity.com"
        description = "Looks for VBA files containing function names that have been randomized based on their entropy."
        date = "2022-03-14"
        hash1 = "c2badcdfa9b7ece00f245990bb85fb6645c05b155b77deaf2bb7a2a0aacbe49"
        memory_suitable = 0

    strings:
        $vba = "end function" nocase ascii wide
        $js = "function(){"

        $ascii_re = /Function [a-zA-Z0-9]{10,100}\(/i ascii
        $wide_re = /Function [a-zA-Z0-9]{10,100}\(/i wide
        
    condition:
        filesize < 2MB and 
        $vba and not 
        any of ($js*) and 
        (
            for 4 i in (1..(math.min(#ascii_re, 16))):
            (
                    math.entropy(@ascii_re[i],!ascii_re[i]) > 4.3
            ) or 
            for 4 i in (1..(math.min(#wide_re, 16))):
            (
                    math.entropy(@wide_re[i],!wide_re[i]) > 3.25
            )
        )
}