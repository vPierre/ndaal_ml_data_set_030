rule mal_sysjoker_macOS {
    meta:
        description = "Identify string artifacts from the SysJoker macOS backdoor."
        author = "@shellcromancer <root@shellcromancer.io>"
        version = "0.1"
        date = "2022-01-13"
        reference = "https://www.intezer.com/blog/malware-analysis/new-backdoor-sysjoker/"
        reference = "https://objective-see.com/blog/blog_0x6C.html"
        sha256 = "1a9a5c797777f37463b44de2b49a7f95abca786db3977dcdac0f79da739c08ac"
    strings:
        $s1 = "1W64PQQxrwY3XjBnv_QAeBQu-ePr537eu" // Google Sheets ID
        $s2 = "welcome to extenal app"
        $s3 = "updateMacOs"
        $s4 = "/Users/mac/Desktop/test/test/json.hpp"
    condition:
        (uint32(0) == 0xfeedface or uint32(0) == 0xcefaedfe or uint32(0) == 0xfeedfacf or uint32(0) == 0xcffaedfe or uint32(0) == 0xcafebabe or uint32(0) == 0xbebafeca)
        and any of them
}

rule mal_sysjoker_macOS_cmds {
    meta:
        description = "Identify shell commands in the SysJoker macOS backdoor."
        author = "@shellcromancer <root@shellcromancer.io>"
        version = "0.1"
        date = "2022-01-13"
        reference = "https://www.intezer.com/blog/malware-analysis/new-backdoor-sysjoker/"
        reference = "https://objective-see.com/blog/blog_0x6C.html"
        sha256 = "1a9a5c797777f37463b44de2b49a7f95abca786db3977dcdac0f79da739c08ac"
    strings:
        $s1 = ">/dev/null 2>&1 &"
        $s2 = "chmod 0777"
        $s3 = "unzip -o"
        $s4 = "whoami"
    condition:
        all of them
}