rule lnkfileoverRFC
{
    strings:
        $header = {4c00 0000 0114 0200 0000} //lnk file header
        $command = "C:\\Windows\\System32\\cmd.exe" fullword ascii //cmd is precursor to findstr
        $command2 =  {2F 00 63 00 20 00 66 00 69 00 6E 00 64 00 73 00 74 00 72} //findstr in hex
        $base64 = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD" ascii //some base64 filler, needed to work with routine
        $cert = "l -decode" ascii //base64 decoder
    condition:
        filesize > 15KB and ($header at 0) and $command and $command2 and $cert and $base64
}