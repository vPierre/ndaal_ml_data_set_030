#!/usr/bin/env bash
# https://github.com/Fadavvi/Yara-Repo
# https://github.com/InQuest/awesome-yara
#
# Exit on error. Append "|| true" if you expect an error.
# set -o errexit
# Exit on error inside any functions or subshells.
set -o errtrace
# Do not allow use of undefined vars. Use ${VAR:-} to use an undefined VAR
set -o nounset
# Catch the error in case mysqldump fails (but gzip succeeds) in `mysqldump |gzip`
# https://vaneyckt.io/posts/safer_bash
set -o pipefail
# Turn on traces, useful while debugging but commented out by default
# set -o xtrace

trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    # script cleanup here
}

readonly USERSCRIPT="cloud"
echo "${USERSCRIPT}"

DESTINATION="/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master/"
echo "${DESTINATION}"

EXECUTIONPATH="${DESTINATION}"
echo "${EXECUTIONPATH}"

DIRECTORY="${DESTINATION}"
echo "${DIRECTORY}"

if [ ! -d "${DIRECTORY}" ]; then
    # Control will enter here if $DIRECTORY doesn't exist.
    mkdir -p -v "${DIRECTORY}"
fi

echo "Ensure that specific Python packages are installed on the system"

pip3 install requests
pip3 install argparse
pip3 install response

echo "Ensure that I got hard disk space back on my macOS"

brew cleanup

cd "${DESTINATION}" || exit

Function_Git_Pull_Repos () {
for n in "${array[@]}"
do
    (
    echo "${n}"
	cd "${DESTINATION}${n}" || exit
    echo "${DESTINATION}${n}"
    
	git config pull.rebase false
    git pull --verbose --force
    #git add -f --verbose -A
	#git commit -m "update ../repos/ndaal_ml_data_set_030/dataset"
    #git config --global http.postBuffer 15728640
    git config --global http.postBuffer 524288000
    #git branch --unset-upstream
    #git push -v -u origin master
    
    )
done
}

Function_Rename_Rules () {
    cd "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/" || exit
    echo "rename *.YAR to *.yara"
    find . -name '*.YAR' -exec sh -c 'mv "$0" "${0%.YAR}.yara"' {} \;
    echo "rename *.YARA to *.yara"
    find . -name '*.YARA' -exec sh -c 'mv "$0" "${0%.YARA}.yara"' {} \;
    echo "rename *.Yar to *.yara"
    find . -name '*.Yar' -exec sh -c 'mv "$0" "${0%.Yar}.yara"' {} \;
    echo "rename *.yar to *.yara"
    find . -name '*.yar' -exec sh -c 'mv "$0" "${0%.yar}.yara"' {} \;
    echo "rename *.YARA to *.yara"
    find . -name '*.YARA' -exec sh -c 'mv "$0" "${0%.Yara}.yara"' {} \;
    echo "rename *.yr to *.yara"
    find . -name '*.yr' -exec sh -c 'mv "$0" "${0%.yr}.yara"' {} \;
    echo "rename *.YR to *.yara"
    find . -name '*.YR' -exec sh -c 'mv "$0" "${0%.YR}.yara"' {} \;
    echo "rename *.Yr to *.yara"
    find . -name '*.Yr' -exec sh -c 'mv "$0" "${0%.Yr}.yara"' {} \;
    echo "rename *.yR to *.yara"
    find . -name '*.yR' -exec sh -c 'mv "$0" "${0%.yR}.yara"' {} \;
    echo "rename *.rule.yara to *.yara"
    find . -name '*.rule.yara' -exec sh -c 'mv "$0" "${0%.rule.yara}.yara"' {} \;
    echo "rename *.rules.yara to *.yara"
    find . -name '*.rules.yara' -exec sh -c 'mv "$0" "${0%.rule.yara}.yara"' {} \;
    echo "rename *.rule to *.yara"
    find . -name '*.rule' -exec sh -c 'mv "$0" "${0%.rule}.yara"' {} \;
    echo "rename *.rules to *.yara"
    find . -name '*.rules' -exec sh -c 'mv "$0" "${0%.rules}.yara"' {} \;
    echo "rename *.Rule to *.yara"
    find . -name '*.Rule' -exec sh -c 'mv "$0" "${0%.Rule}.yara"' {} \;
    echo "rename *.Rules to *.yara"
    find . -name '*.Rules' -exec sh -c 'mv "$0" "${0%.Rules}.yara"' {} \;
    echo "rename *.txt to *.yara"
    find . -name '*.txt' -exec sh -c 'mv "$0" "${0%.txt}.yara"' {} \;
    echo "rename *.ptn to *.yara"
    find . -name '*.ptn' -exec sh -c 'mv "$0" "${0%.ptn}.yara"' {} \;
    echo "rename *.jar to *.yara"
    find . -name '*.jar' -exec sh -c 'mv "$0" "${0%.jar}.yara"' {} \;
    echo "rename *.jara to *.yara"
    find . -name '*.jara' -exec sh -c 'mv "$0" "${0%.jara}.yara"' {} \;
    echo "rename *.* to *.yara finished"
    cd "${DESTINATION}" || exit
}

# declare an array called array
declare -a array
array=(
"SigmaHQ"
"5l1v3r1"
"Adv-threat-research"
"BAMF_Detect"
"CiscoCXSecurity"
"CyberDefenses"
"DailyIOC"
#"DanielRuf"
"FerdiGul"
"FirehaK"
"Hestat"
"InQuest"
"MainYaraRules"
"NVISOsecurity"
"OpenSourceYara"
"Penetrum-Security"
"PhishingKit-Yara-Rules"
"Phoul"
"SEKOIA-IO"
"SadFud"
"Sec-Soup"
"Signatures"
"SupportIntelligence"
"Te-k"
"Xumeiquer"
"malice"
"Yara"
"ZephrFish_Random-Yara-Rules"
"ail-project"
"apihash_to_yara"
"bartblaze"
"binaryalert-rules"
"citizenlab"
"codewatchorg"
"custom-yara-rules-master"
"data"
"deadbits"
"ditekshen"
"droberson"
"eset"
"f0wl-rules"
"fboldewin"
"fideliscyber"
"fpt-eagleeye"
"fr0gger"
"fsf-server"
"godaddy"
"h3x2b"
"hpthreatresearch"
"innominds_deadbits"
"innominds_lprat"
"innominds_stratosphereips"
"intezer"
"jeFF0Falltrades"
"jtrombley90"
"kevthehermit"
"malpedia"
"malware_analysis"
"mandiant"
"milad_kahsarialhadi"
"mokuso"
"nao-sec"
"naveenselvan"
#"prolsen"
"red_team_tool_countermeasures"
"reversinglabs"
"rules"
"securitymagic"
"seyyid-bh"
"sophos"
"stairwell-inc"
"stvemillertime"
"t4d"
"x64dbg_yarasigs"
"telekom-security_malware_analysis"
"tenable"
"thewhiteninja"
"tillmannw"
"tjnel"
"vendor"
"x64dbg_yarasigs"
"yara"
"yararules"
"ahhh"
"3vangel1st"
"timb-machine"
"JPCERTCC"
"naxonez"
"StrangerealIntel"
"StrangerealIntel_Orion"
"sebdraven"
"instacyber"
"BushidoUK"
"imp0rtp3"
"pmelson"
"oldmonk007"
"avast"
"nembo81"
"S3cur3Th1sSh1t"
"1aN0rmus"
"0pc0deFR"
"malware-kitten"
"abhinavbom"
"jipegit"
"ProIntegritate"
"umair9747"
"CofenseLabs"
"VectraThreatLab"
#"LD-Skystars"
"401trg"
"simonk9"
"executemalware"
"target"
"pollonegro"
"ulisesrc"
"SIFalcon"
"sirpedrotavares"
"zourick"
"SinkingYachts"
"loginsoft.com"
"mikesxrs"
"bi-zone"
"Cluster25"
"Neo23x0"
"Neo23x0_signature-base"
"Neo23x0_yara-type-selectors"
"0xthr3atint31"
"resteex0"
"imp0rtp3"
"sbousseaden"
"k-vitali"
"intezer"
"cbecks2"
"g-les"
"IntelCorgi"
"albertzsigovits"
"secman-pl"
"EmergingThreats"
"rakovskij-stanislav"
"AhmetPayaslioglu"
"righel"
"rivitna"
"tsumarios"
"nasbench"
"MalConfScan"
"n0leptr"
"ulisesrc"
"alex-cart"
"ChaitanyaHaritash"
"farhanfaisal"
"BassGonzilla"
"Sprite-Pop"
"alvosec"
"th3b3ginn3r"
"pollonegro"
"gavz"
"CashlessConsumer"
"SecSamDev"
"volexity"
"Onils"
"3vangel1st_100DaysOfYARA"
"delyee"
"jamesbower"
"aleprada"
"jemik"
"sathishshan"
"wesinator"
"alex-gehrig"
"jaegeral"
"paulzroe"
"forensenellanebbia"
"2096779623"
#"ALittlePatate"
"malpedia_signator-rules"
"CD-R0M_HundredDaysofYARA"
"CD-R0M_YARA"
"V1D1AN"
"jakubd"
"NimGetSyscallStub"
"nobodyatall648"
"anil-yelken"
"Mike-Resheph"
"elastic"
"optiv"
"intezer"
"cert-ee"
"Gokulgoky1"
"advanced-threat-research_Yara-Rules"
"advanced-threat-research_Russian_CyberThreats_Yara"
"JPMinty"
"daredumidu"
"CybercentreCanada"
"evild3ad"
"embee-research_APIHashReplace"
"embee-research_Yara"
"taogoldi"
"MalGamy"
"wxsBSD"
"stvemillertime_100daysofYARA-2022"
"stvemillertime_100daysofYARA-2023"
"GCTI"
"galkofahi"
"TibetanBrownBear"
"knightsc_XProtect"
#"viper-framework"
"hackOtack_YaraForPentest"
"straysheep-dev_malware-analysis"
"Randsec"
"laruence"
"100DaysofYARA_2022"
"100DaysofYARA_2022"
"google_vxsig"
"dr4k0nia"
"MayerDaniel"
"elceef_yara-rulz"
"jstrosch_malware-signatures"
"Idov31_Sandman"
"malquarium"
"MayerDaniel_100DY_2023"
"0xThiebaut_Signatures"
"hvs-consulting_ioc_signatures"
"phbiohazard_Yara"
"delivr-to_detections"
"mandiant_red_team_tool_countermeasures"
"daal_public_yara_gtfobins_unix_binaries_that_can_be_used_to_bypass_local_security_restrictions_in_misconfigured_systems"
"ndaal_public_yara_loldrivers_living_off_the_land_drivers"
"ndaal_public_yara_lolbas_living_off_the_land_binaries_and_scripts"
"ndaal_public_yara_living_off_the_orchard"
"ndaal_public_yara_living_off_the_orchard_results"
"ndaal_public_YARA_passwords_default"
"ndaal_public_YARA_passwords_default_results"
"ndaal_public_YARA_passwords_weak"
"ndaal_public_YARA_passwords_weak_results"
"albertzsigovits_malware-yara"
"candk-cyber_Custom-Rules-ClamAV"
"fox-it_mkYARA"
"fox-it_operation-wocao"
"Qu1cksc0pe"
"yara_qakbot"
"phish-report_IOK"
"threatlabz_iocs"
"uptycslabs_yara-rules"
"eset_malware-ioc"
"trendmicro_research"
"magicsword-io_LOLDrivers"
"AhmetPayaslioglu_YaraRules"
"zahidadeel_YARA-Rules"
"nr1z_yara_rules"
"Cyb3rtus_keepass_CVE-2023-24055_yara_rule"
"securiteinfo_expl_outlook_cve_2023_23397"
"airbus-cert_yara-ttd"
"3pun0x_RepoTele"
"3pun0x_YaraRules"
"regeciovad_YaraRex-demo"
"michelcrypt4d4mus_pdfalyzer"
"c3rb3ru5d3d53c_signatures"
"mohabye_Emotet_yara_rule"
#"ladar_clamav-data"
"ruppde_yara_rules"
"horsicq_XYara"
"signalblur_detection-artifacts"
)

RED='\e[31m'
NC='\033[0m'
clear

printf ${RED}' =============================\n'
printf '|      Yara repository        |\n'
printf '|-=-=-=-=-=-=-=-=-=-=-=-=-=-=-|\n'
printf '|       Version  0.1.8        |\n'
printf '|       Milad  Fadavvi        |\n'
printf '|       Pierre Gronau         |\n'
printf '|     Git 2.28 or Higher      |\n'
printf '|   * Run script as Root *    |\n'
printf ' =============================\n\n'${NC}
sleep 10

git config pull.rebase false
git pull --verbose

cp -n -r -v /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/*.yara ${DESTINATION}
#cp -n -r -v -p /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/*.rule* ${DESTINATION}
#cp -n -r -v -p /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/*.Rule* ${DESTINATION}
#cp -n -r -v -p /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/* ${DESTINATION}

sleep 20

Function_Clean_Up_YARA_Rules () {
for i in {0..50}
do
    echo "rm -v -r yara.${i}"
    rm -v -r *.yara.${i} || true
    echo "rm -v -r YARA.${i}"
    rm -v -r *.YARA.${i} || true
    echo "rm -v -r YAR.${i}"
    rm -v -r *.YAR.${i} || true
    echo "rm -v -r yar.${i}"
    rm -v -r *.yar.${i} || true
    echo "rm -v -r yar${i}"
    rm -v -r *.yar${i} || true
    echo "rm -v -r Rule.${i}"
    rm -v -r *.Rule.${i} || true
    echo "rm -v -r rule.${i}"
    rm -v -r *.rule.${i} || true
    echo "rm -v -r rule.zip.${i}"
    rm -v -r *.rule.zip.${i} || true
    echo "rm -v -r .zip.${i}"
    rm -v -r *.zip.${i} || true
done
}

Function_Clean_Up_Files () {
    echo "cleanup some files like .github"
    find . -name ".github" -exec rm -rf {} \; || true
    find . -name ".gitignore" -exec rm -rf {} \; || true
    find . -name ".gitmodules" -exec rm -rf {} \; || true
    find . -name ".gitattributes" -exec rm -rf {} \; || true
    find . -name ".git" -exec rm -rf {} \; || true
    find . -name ".travis.yml" -exec rm -rf {} \; || true
    echo "cleanup finished"
}

Function_Remove_Directories () {
for n in "${array[@]}"
do
    (
    echo "${n}"
    echo "${DESTINATION}"
    cd "${DESTINATION}" || exit
    echo "remove directory ${n}"
	rm -rf "${n}"
    echo "removed directory ${n}"   
    )
done
}

Function_Convert_ClamAV_Signatures_to_YARA_Rules () {

cd "${DESTINATION}ladar_clamav-data/" || exit
echo "${DESTINATION}ladar_clamav-data/"
echo "##########################"
echo "#                        #"
for i in {1..9}
do
    echo "unpack daily.cvd.* with sigtool"
    sigtool --unpack daily.cvd.0${i} || true
    #python3 "${DESTINATION}clamav2yara.py" -i "daily.cvd.0${i}" -o "daily.cvd.0${i}.yara" || exit
    echo "unpack main.cvd.* with sigtool"
    sigtool --unpack main.cvd.0${i} || true
    #python3 "${DESTINATION}clamav2yara.py" -i "main.cvd.0${i}" -o "haupt.cvd.0${i}.yara" || exit
done
echo "#                        #"
echo "##########################"
}

Function_Clean_Up_YARA_Rules

cd "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/" || exit

rm -rf .git

Function_Clean_Up_YARA_Rules

Function_Remove_Directories

cd "${DESTINATION}" || exit

rm -rf .git && git init && git remote add -f AlienVaultLabs https://github.com/AlienVault-Labs/AlienVaultLabs  > /dev/null && echo 'malware_analysis' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull AlienVaultLabs master > /dev/null
wget -q https://gist.githubusercontent.com/pedramamini/c586a151a978f971b70412ca4485c491/raw/68ba7792699177c033c673c7ffccfa7a0ed5ce47/XProtect.yara 
rm -rf .git && git init && git remote add -f bamfdetect https://github.com/bwall/bamfdetect  > /dev/null && echo 'BAMF_Detect/modules/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull bamfdetect master > /dev/null
git clone -v https://github.com/bartblaze/Yara-rules bartblaze
rm -rf .git && git init && git remote add -f binaryalert https://github.com/airbnb/binaryalert  > /dev/null && echo 'rules/public' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull binaryalert master > /dev/null
rm -rf .git && git init && git remote add -f CAPE  https://github.com/ctxis/CAPE  > /dev/null && echo 'data/yara/CAPE' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull CAPE master > /dev/null
git clone -v https://github.com/CyberDefenses/CDI_yara CyberDefenses
git clone -v https://github.com/citizenlab/malware-signatures citizenlab
git clone -v https://github.com/stvemillertime/ConventionEngine stvemillertime
git clone -v https://github.com/deadbits/yara-rules deadbits
git clone -v https://github.com/eset/malware-ioc/ eset
rm -rf .git && git init && git remote add -f indicators https://github.com/fideliscyber/indicators  > /dev/null && echo 'yararules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull indicators master > /dev/null
wget -q https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44 && mv f1bb645a4f715cb499150c5a14d82b44 Neo23x0.yara
git clone -v https://github.com/fboldewin/YARA-rules fboldewin
git clone -v https://github.com/godaddy/yara-rules godaddy
git clone -v https://github.com/h3x2b/yara-rules h3x2b 
git clone -v https://github.com/SupportIntelligence/Icewater SupportIntelligence
git clone -v https://github.com/intezer/yara-rules intezer
git clone -v https://github.com/InQuest/yara-rules InQuest
git clone -v https://github.com/jeFF0Falltrades/YARA-Signatures jeFF0Falltrades
# rm -rf kevthehermit
git clone -v https://github.com/kevthehermit/YaraRules kevthehermit
git clone -v https://github.com/Hestat/lw-yara Hestat
git clone -v https://github.com/tenable/yara-rules tenable
git clone -v https://github.com/tjnel/yara_repo tjnel
wget -q https://malpedia.caad.fkie.fraunhofer.de/api/get/yara/auto/zip && mv zip malpedia_auto_yar.zip && unzip malpedia_auto_yar.zip -d malpedia/  > /dev/null && rm -f malpedia_auto_yar.zip
wget -q http://didierstevens.com/files/software/yara-rules-V0.0.8.zip && mv zip yara-rules-V0.0.8.zip && unzip yara-rules-V0.0.8.zip -d didierstevens/  > /dev/null && rm -f yara-rules-V0.0.8.zip

git clone -v https://github.com/mikesxrs/Open-Source-YARA-rules mikesxrs
#git clone -v https://github.com/prolsen/yara-rules prolsen
git clone -v https://github.com/advanced-threat-research/IOCs Adv-threat-research
git clone -v https://github.com/sophos-ai/yaraml_rules sophos
git clone -v https://github.com/reversinglabs/reversinglabs-yara-rules reversinglabs
git clone -v https://github.com/Yara-Rules/rules MainYaraRules
git clone -v https://github.com/fr0gger/Yara-Unprotect fr0gger
git clone -v https://github.com/Xumeiquer/yara-forensics Xumeiquer
git clone -v https://github.com/malice-plugins/yara malice
wget --continue --verbose --tries=10 --output-document VectraThreatLab_re.yara https://raw.githubusercontent.com/VectraThreatLab/reyara/master/re.yar
#rm -rf .git && git init && git remote add -f fsf https://github.com/EmersonElectricCo/fsf  > /dev/null && echo 'fsf-server/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull fsf master > /dev/null
rm -rf .git && git init && git remote add -f Cyber-Defence https://github.com/nccgroup/Cyber-Defence/  > /dev/null && echo 'Signatures/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull Cyber-Defence master > /dev/null
rm -rf .git && git init && git remote add -f malware-analysis https://github.com/SpiderLabs/malware-analysis  > /dev/null && echo 'Yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull malware-analysis master > /dev/null
###
rm -rf .git && git init && git remote add -f ThreatHunting https://github.com/GossiTheDog/ThreatHunting > /dev/null && echo 'ThreatHunting' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull ThreatHunting master > /dev/null
### From Awesome Community  
rm -rf .git && git init && git remote add -f TheSweeper-Rules https://github.com/Jistrokz/TheSweeper-Rules > /dev/null && echo 'yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull TheSweeper-Rules main > /dev/null
git clone -v https://github.com/miladkahsarialhadi/RulesSet milad_kahsarialhadi
wget --continue --verbose --tries=10 --output-document obfuscatedPHP_obfuscatdPHP.yara https://raw.githubusercontent.com/gil121983/obfuscatedPHP/master/obfuscatdPHP.yar
rm -rf .git && git init && git remote add -f binaryalert https://github.com/airbnb/binaryalert > /dev/null && echo 'rules/public' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull binaryalert master > /dev/null && mkdir binaryalert-rules && mv rules/public/* binaryalert-rules && rm -rf rules/public/
rm -rf .git && git init && git remote add -f indicators https://github.com/fideliscyber/indicators> /dev/null && echo 'yararules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull indicators master > /dev/null && mv yararules fideliscyber
rm -rf .git && git init && git remote add -f red_team_tool_countermeasures https://github.com/fireeye/red_team_tool_countermeasures > /dev/null && echo 'rules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull red_team_tool_countermeasures master > /dev/null && mv rules red_team_tool_countermeasures 
git clone -v https://github.com/f0wl/yara_rules f0wl-rules

git clone -v https://github.com/jeFF0Falltrades/YARA-Signatures jeFF0Falltrades
git clone -v https://github.com/securitymagic/yara securitymagic
git clone -v https://github.com/StrangerealIntel/DailyIOC
# rm -rf t4d
git clone -v https://github.com/t4d/PhishingKit-Yara-Rules t4d
git clone -v https://github.com/x64dbg/yarasigs x64dbg_yarasigs
git clone -v https://github.com/thewhiteninja/yarasploit thewhiteninja
git clone -v https://github.com/mokuso/yara-rules mokuso
rm -rf .git && git init && git remote add -f YaraRules https://github.com/5l1v3r1/yaraRules > /dev/null && echo 'YaraRules' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull yaraRules master > /dev/null
git clone -v https://github.com/telekom-security/malware_analysis telekom-security_malware_analysis
git clone -v https://github.com/tbarabosch/apihash_to_yara apihash_to_yara
git clone -v https://github.com/tillmannw/yara-rules tillmannw

git clone -v https://github.com/jtrombley90/Yara_Rules jtrombley90
git clone -v https://github.com/ail-project/ail-yara-rules ail-project
git clone -v https://github.com/mandiant/sunburst_countermeasures mandiant
git clone -v https://github.com/Te-k/cobaltstrike Te-k
git clone -v https://github.com/SEKOIA-IO/Community SEKOIA-IO
git clone -v https://github.com/NVISOsecurity/YARA NVISOsecurity

wget --continue --verbose --tries=10 --output-document tylabs_quicksand_exe.yara https://raw.githubusercontent.com/tylabs/qs_old/master/quicksand_exe.yara
wget --continue --verbose --tries=10 --output-document tylabs_quicksand_exploits.yara https://raw.githubusercontent.com/tylabs/qs_old/master/quicksand_exploits.yara
wget --continue --verbose --tries=10 --output-document tylabs_quicksand_general.yara https://raw.githubusercontent.com/tylabs/qs_old/master/quicksand_general.yara
wget --continue --verbose --tries=10 --output-document tlansec_pe_check.yara https://gist.githubusercontent.com/tlansec/4be4e92cbbd3354cf53386ef6edf0676/raw/f6cef23a2d3e6de6ead5b83c53801dbe1b653bf6/pe_check.yar
wget --continue --verbose --tries=10 --output-document shellcromancer_mal_sysjoker_macOS.yara https://gist.githubusercontent.com/shellcromancer/e9e8c8ca95e0f31fc8b92ebc82b59303/raw/f706b420f6370d034781f605e55879e7d3322c1e/mal_sysjoker_macOS.yara
wget --continue --verbose --tries=10 --output-document silascutler_WhisperGate.yara https://gist.githubusercontent.com/silascutler/f8e518564a8a1410ba58f0ab5ed493f6/raw/b465af32c4b546fb4ab3604fbe3c5d363aca7f2c/%2523WhisperGate%2520Yara%2520Rule
wget --continue --verbose --tries=10 --output-document silascutler_Cyclops.yara https://gist.githubusercontent.com/silascutler/00b9308b429808bbb5e72c07f963134d/raw/d0e6b4681184e911ccd8adb605684b14e8bd5b4e/NCSC%2520Cyclops%2520Blink%2520yara%2520Rules
wget --continue --verbose --tries=10 --output-document captainGeech42_scriptobf_replaceempty.yara https://gist.githubusercontent.com/captainGeech42/3e60e639ea62dd6e907e3e1e7cbac0fc/raw/43b3ef99249eb9b47c7062a98a3bdadad7863d65/scriptobf_replaceempty.yara
wget --continue --verbose --tries=10 --output-document schrodyn_windows_drivers.yara https://gist.githubusercontent.com/schrodyn/30ca12d15e0e069224204adca41d5256/raw/7ff09541c30977173fb5dc192d5820a13f31a89d/windows_drivers.yara
wget --continue --verbose --tries=10 --output-document Neo23x0_iddqd.yara https://gist.githubusercontent.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/raw/d621fcfd496d03dca78f9ff390cad88684139d64/iddqd.yar
wget --continue --verbose --tries=10 --output-document kmdnet_EICAR_hash_rule.yara https://gist.githubusercontent.com/kmdnet/28f1dde6ab93d866a87357c1c25632a7/raw/c52c3d9c267d6ca8f9dc40043ba888d977203c54/EICAR_hash_rule.yar
wget --continue --verbose --tries=10 --output-document MrThreat_findbadlinkers.yara https://gist.githubusercontent.com/MrThreat/d468caf9dc392431099f9b1958cf188f/raw/49aced5f32bebc20044ee75bcc9de20abeddd42b/findbadlinkers.yar
wget --continue --verbose --tries=10 --output-document tombonner_pe_template.yara https://gist.githubusercontent.com/tombonner/70f9df512b649533c40cccc6f1a08c30/raw/34dd02a07827ca5ba3d22f47a3878ce4ac3de49e/pe_template.yara
wget --continue --verbose --tries=10 --output-document tombonner_pdb_template.yara https://gist.githubusercontent.com/tombonner/0b0aea4722a1ea3e6e475568ab2f912d/raw/b3ef8ca4a475a0f711f3c887aba5cb48737694f9/pdb_template.yara
wget --continue --verbose --tries=10 --output-document stvemillertime_CreatePEPolyObject.yara https://gist.githubusercontent.com/stvemillertime/b87dd7bf979dda6f1b6b5d70bf6620b8/raw/3fbaecc5d7bd1004e9d38bb4f6ac6c1a73710e5c/CreatePEPolyObject.yar
wget --continue --verbose --tries=10 --output-document BoazDolev_ComLook.yara https://gist.githubusercontent.com/BoazDolev/43b141957b94163fc1f2a0dca83c53b6/raw/3daf98403ff0142c5453bf4c2c13389125890847/ComLook.Yar
wget --continue --verbose --tries=10 --output-document shellcromancer_tool_gscript.yara https://gist.githubusercontent.com/shellcromancer/b4fcdb7c118ef1aa0006ec5653c92c78/raw/c8bedbcbefa38e362be354badb3e23d6145564ee/tool_gscript.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_des.yara https://gist.githubusercontent.com/shellcromancer/aefe541468bb9725dbc7d227faa13a08/raw/4839cc79e5470ea8ef7b05facb0f5f56c1f9e56a/alg_crypto_des.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_sha1.yara https://gist.githubusercontent.com/shellcromancer/da08abd6e254c83c12dd9f69512ea872/raw/516ce63e0265b474ee5f7631e8802674d01e9a19/alg_crypto_sha1.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_mal_sysjoker_macOS.yara https://gist.github.com/shellcromancer/e9e8c8ca95e0f31fc8b92ebc82b59303/raw/f706b420f6370d034781f605e55879e7d3322c1e/mal_sysjoker_macOS.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_args_socket_tcpip.yara https://gist.github.com/shellcromancer/6baf9d799ff4291d55949ae9d796ea69/raw/171f56160b0a4b71f432c1746dd72f6c64c24959/args_socket_tcpip.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_blake.yara https://gist.github.com/shellcromancer/f410a7cc75f6c069f8c5285990370002/raw/8cb46a37720c49167f9e7cdcbc6a7a4513a1c528/alg_crypto_blake.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_md5.yara https://gist.github.com/shellcromancer/f7260f3b05787e37f811baa9c37e9c65/raw/65d2093260af2381deebe6e5128e2297afc59321/alg_crypto_md5.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_shellcode_metasploit_x86.yara https://gist.github.com/shellcromancer/9d683b37f7caac9c2b58999641ce140b/raw/6a01ff0abb69543774d4df1ecdebbf46d2b15c00/shellcode_metasploit_x86.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_lang_rust.yara https://gist.github.com/shellcromancer/3ebbb4b82b9c86038dbb829ab5f3abb1/raw/2cf5174cec1477a86d43180fc494e656e484215a/lang_rust.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_macho_space_in_seg_or_sect.yara https://gist.github.com/shellcromancer/59b0a9a044f7413b009962f64663f819/raw/9131ff0e7d9e82350724fb77aa89eedfd4b133c7/macho_space_in_seg_or_sect.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_imov_stackstrings.yara https://gist.github.com/shellcromancer/6eb7bd5849aff1d657ef3fc5bd31e71d/raw/4225a033780b20532c36c2a8388ecc3654339fe6/imov_stackstrings.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_crc32.yara https://gist.github.com/shellcromancer/bbb926841ab94056379097d4d5c2219c/raw/a2973ce124db72eb8244ffef504aa7b25bea08ff/alg_crypto_crc32.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_aes.yara https://gist.github.com/shellcromancer/570d3416dd4175752f883d173f10ac3d/raw/bc353cdf9922fe7c66d2edfe73a1f7d1baa85852/alg_crypto_aes.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_bashrc.yara https://gist.github.com/shellcromancer/b6039d1c6da6328db699eedeae92e59f/raw/01077cb345799556af7a556f56bb410a3a87f06d/.bashrc
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_crypto_rc4.yara https://gist.github.com/shellcromancer/d1b0bd1832a1a3a3c3dfd5cc75dfb21e/raw/ba4c66fe0ff6f9e21631baac7eb98fc8ce729551/alg_crypto_rc4.yara
wget --continue --verbose --tries=10 --output-document shellcromancer_alg_salsa20.yara https://gist.github.com/shellcromancer/c5dd110241040b12b90b021daf89a36c/raw/e2799df8c66fcb02899eb41a473701d1567a041a/alg_salsa20.yara
wget --continue --verbose --tries=10 --output-document yt0ng_console_kungfu.yara https://gist.githubusercontent.com/yt0ng/2fb4832f97a5d90687d07d2886b595c3/raw/a08ef8935f1ad2fbe24b4fce9f3abbdc306b95d8/console_kungfu.yar
wget --continue --verbose --tries=10 --output-document Neo23x0_iddqd.yara https://gist.githubusercontent.com/gs3cl/1ad3f524f5e7744ca2334a6b144ccc01/raw/d621fcfd496d03dca78f9ff390cad88684139d64/iddqd.yar
wget --continue --verbose --tries=10 --output-document captainGeech42_scriptobf_replaceempty.yara https://gist.githubusercontent.com/captainGeech42/3e60e639ea62dd6e907e3e1e7cbac0fc/raw/43b3ef99249eb9b47c7062a98a3bdadad7863d65/scriptobf_replaceempty.yara
wget --continue --verbose --tries=10 --output-document tlansec_entropy_functions.yara https://gist.githubusercontent.com/tlansec/ec99e033135040bc29a78265d2857750/raw/86702eba38e9a05fee6d8c06a44d1323863b2d4a/entropy_functions.yar
wget --continue --verbose --tries=10 --output-document tlansec_casing_anomaly.yara https://gist.githubusercontent.com/tlansec/8328c8c60f748ca562cf9a7045d1d0e0/raw/106371c462543ca02d9d9724d6c683187533d1d0/casing_anomaly.yar
wget --continue --verbose --tries=10 --output-document greylupi_eMAP_rule_SampleA1.yara https://raw.githubusercontent.com/greylupi/Yara_Rules/main/eMAP_rule_SampleA1.txt
wget --continue --verbose --tries=10 --output-document captainGeech42_pe_load_getproc.yara https://gist.githubusercontent.com/captainGeech42/30a709143ad8881c1682d4c769678eba/raw/2efa7427c89323d0a224469a68752bcdb68e8933/pe_load_getproc.yara
wget --continue --verbose --tries=10 --output-document itsreallynick_gen_URLpersistence.yara https://gist.githubusercontent.com/itsreallynick/a5c10f5c4c19f153117c423ea57dc8d0/raw/ceece1c51abb866f190a01a833e3cd3507d70f86/gen_URLpersistence.yar

wget --continue --verbose --tries=10 --output-document silence-is-best_NewJssLoader.yara https://gist.githubusercontent.com/silence-is-best/e545dc09fc53dc48f1ee4c57bfb60a64/raw/e072bd20975e285a6e3150d7fddec2c2096f8ec0/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_SVCReady.yara https://gist.githubusercontent.com/silence-is-best/ae10c1b627faeb7ff9e4f05dbfb8c7db/raw/a59fd1f37c29e2a9502c816acbc6b09a6248bc31/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Nerbian_RAT.yara https://gist.githubusercontent.com/silence-is-best/bd60c296a8adb6a8e8ff01bede974ec2/raw/71893e83b5862824356cf56059f23e8062f2cd5c/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Bumblebee_loader.yara https://gist.githubusercontent.com/silence-is-best/dce8183ac6e7d41435e1cfa137e9a0a4/raw/266316744500a18074cc2ffaf530159cd775cd1e/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_fbrobot_stealer.yara https://gist.githubusercontent.com/silence-is-best/7ec6ce480371ad2e8bf40fcc77481825/raw/ff3293bd4f5185765b04e8955ac9f55293ef0b52/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Socelars_stealer.yara https://gist.githubusercontent.com/silence-is-best/e5c728b933eaf5659f572f55f7ca03c1/raw/0bf490b2dd160015590ad5735cf64a9f84a9662f/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Una_Stealer.yara https://gist.githubusercontent.com/silence-is-best/e1ac68d2039d61df1331ea65b620bddb/raw/4ede084bb57ea2e9d2487060576412dcc3076d15/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Swivelload.yara https://gist.githubusercontent.com/silence-is-best/dc4f4ee214955ad8e41b2ef366b5f06f/raw/04b8fbd6a0b106631ebf423f503dc2e9568ee0ff/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_zloader_new_odd.yara https://gist.githubusercontent.com/silence-is-best/31a15d9366709d454284c30819b8b425/raw/f04b7a7b53d96fe56d797b8639c867af70ec30a6/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Corroded_buerloader.yara https://gist.githubusercontent.com/silence-is-best/94e0701e54c0fa2f6d7ae6dbc1874f03/raw/50c4165f8f300652c5ac1d7ae4d6d9727eb11844/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_GALLIUM_PingPull.yara https://gist.githubusercontent.com/silence-is-best/aed87420d90f50abf6e33cd8bff94ec5/raw/fe5fd909ee808067aee43220dd0563ccf7efe8ae/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Recorderbreaker_stealer.yara https://gist.githubusercontent.com/silence-is-best/ee1a7774fdb176cd32299dc3286623e9/raw/1ac848a6c2bf9989999b0054dc8f2058fe198231/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_fbrobot_stealer.yara https://gist.githubusercontent.com/silence-is-best/7ec6ce480371ad2e8bf40fcc77481825/raw/ff3293bd4f5185765b04e8955ac9f55293ef0b52/gistfile1.txt
wget --continue --verbose --tries=10 --output-document silence-is-best_Matanbuchus.yara https://gist.githubusercontent.com/silence-is-best/1bc62a53c1a0ddb3a8bcdff19bc80c3e/raw/dd15ca837258298831d094767954358a406d5ebb/gistfile1.txt

wget --continue --verbose --tries=10 --output-document notareverser_ASPack_PE.yara https://gist.githubusercontent.com/notareverser/d5f4f0d09285edca3ec027534c233271/raw/db140e7498b250f94ff6feca365704ab21b5ff37/YARA%2520rules%2520for%2520%2523100DaysOfYARA
wget --continue --verbose --tries=10 --output-document notareverser_sevenzip_sfx.yara https://gist.githubusercontent.com/notareverser/f1b0c4b871459af2adfe1d1e44c8fa1e/raw/eb54cf4b7f12bb42ded0519ca8fe4065310b3476/sevenzip_sfx.yara
wget --continue --verbose --tries=10 --output-document notareverser_zlib.yara https://gist.githubusercontent.com/notareverser/b14667284046f7fdcf8dd84e48add1bd/raw/ac5bda427bf3cb25b1d262824f28da4f74e58ff3/zlib.yara
wget --continue --verbose --tries=10 --output-document notareverser_innosetup.yara https://gist.githubusercontent.com/notareverser/d892bc31423ad8ff8b68b9c78ec0ae6f/raw/5e31cfedec937e6ab85ac52b3ef228f376d6a298/innosetup.yara
wget --continue --verbose --tries=10 --output-document notareverser_kernel32.yara https://gist.githubusercontent.com/notareverser/eb2a7cf37e6cd64b817212aba9b556eb/raw/81e9e1a40e069ed9ea1de11418c05420d1f348e6/kernel32.yara

wget --continue --verbose --tries=10 --output-document crypto_signatures_with_SM4.yara https://github.com/Yara-Rules/rules/pull/428/commits/c0573b2ea53cf72470f4eed0bc2000e73b55ee47
wget --continue --verbose --tries=10 --output-document DiabloHorn_juicy_files.yara https://github.com/DiabloHorn/yara4pentesters/blob/master/juicy_files.txt
wget --continue --verbose --tries=10 --output-document andretavare5_win_privateloader.yara https://gist.githubusercontent.com/andretavare5/9d8eb659946ff509d9987c9be4031bb6/raw/deff8a0f0dc0c0918ed8e658a8c88fbec998d2cd/win_privateloader.yara
wget --continue --verbose --tries=10 --output-document PMATFinal_wannaHusky.yara https://raw.githubusercontent.com/SynackSyndicate/PMATFinal/main/YaraRule
wget --continue --verbose --tries=10 --output-document xorhex_go_macho.yara https://gist.githubusercontent.com/xorhex/0d75316a00bd7c640483935e3dabc131/raw/63fb7815f5ee194ddd5f88f7e0916d743318fc0d/go_macho.yar 
wget --continue --verbose --tries=10 --output-document Sanesecurity_BlackEnergy.yara https://ftp.swin.edu.au/sanesecurity/Sanesecurity_BlackEnergy.yara
wget --continue --verbose --tries=10 --output-document Sanesecurity_sigtest.yara https://ftp.swin.edu.au/sanesecurity/Sanesecurity_sigtest.yara
wget --continue --verbose --tries=10 --output-document Sanesecurity_spam.yara https://ftp.swin.edu.au/sanesecurity/Sanesecurity_spam.yara
wget --continue --verbose --tries=10 --output-document Droogy_100DaysOfYARA.yara https://gist.githubusercontent.com/Droogy/b5e0480815e6a960c2858ec4b1f9ba84/raw/bb157e4366c6f1324af23b05fb32c661e9522682/100DaysOfYARA.yar
wget --continue --verbose --tries=10 --output-document x0rb3l_project_relic_ransomware.yara https://gist.githubusercontent.com/x0rb3l/0bb7fd6383312f2d1b39ae4232465f5b/raw/d569873d9ee2fa355712aade135c57bdd28a3e73/project_relic_ransomware.yar
wget --continue --verbose --tries=10 --output-document yara4pentesters_juicy_files.yara https://raw.githubusercontent.com/DiabloHorn/yara4pentesters/master/juicy_files.txt
wget --continue --verbose --tries=10 --output-document usualsuspect_zip_ext.yara https://gist.githubusercontent.com/usualsuspect/ce70e5db438178611a75b4f051f8d570/raw/51eab4fece41710720f575956ac5cb7f3c8826e3/zip_ext.yara
wget --continue --verbose --tries=10 --output-document Neo23x0_zIDDQD_Godmode_Rule.yara https://gist.githubusercontent.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/raw/d621fcfd496d03dca78f9ff390cad88684139d64/iddqd.yar 
wget --continue --verbose --tries=10 --output-document Neo23x0_shitrix_artefacts.yara https://gist.githubusercontent.com/Neo23x0/babc97ba67923662d5f49ec6f090d877/raw/7f10ea2e7be108131b96249ac970c291666c5f6b/shitrix_artefacts.yar
wget --continue --verbose --tries=10 --output-document williballenthin_DotnetStartupHook.yara https://gist.githubusercontent.com/williballenthin/adf1fe24e1c07f0e5a0f3a0938cd2712/raw/02cc77308202fd36919bd9afb809356ec3111e97/DotnetStartupHook.yar
wget --continue --verbose --tries=10 --output-document williballenthin_Nuitka.yara https://gist.githubusercontent.com/williballenthin/adf1fe24e1c07f0e5a0f3a0938cd2712/raw/02cc77308202fd36919bd9afb809356ec3111e97/Nuitka.yar
wget --continue --verbose --tries=10 --output-document williballenthin_get_eip.yara https://gist.githubusercontent.com/williballenthin/6057247ac0485ee4e974c93eb1530919/raw/d4888aabe4773506a54878b09b38f2402b551dac/get_eip.yara
wget --continue --verbose --tries=10 --output-document elceef_Outlook_CVE_2023_23397.yara https://raw.githubusercontent.com/elceef/yara-rulz/main/rules/Outlook_CVE_2023_23397.yara
wget --continue --verbose --tries=10 --output-document infobyte_CVE-2023-21036_acropalypse_detection.yara https://raw.githubusercontent.com/infobyte/CVE-2023-21036/master/acropalypse_detection.yar
wget --continue --verbose --tries=10 --output-document mohabye_Emotet.yara https://raw.githubusercontent.com/mohabye/Emotet_yara_rule/main/yara_rule.yar
wget --continue --verbose --tries=10 --output-document vietd0x_detect-debug.yara https://gist.githubusercontent.com/vietd0x/837dc93edae05a2a426ffb5f45b488e2/raw/f65793695e7fab520daed0457c8b40b9dcf1658c/detect-debug.yara
wget --continue --verbose --tries=10 --output-document phbiohazard_spectre.yara https://raw.githubusercontent.com/phbiohazard/Yara/master/spectre.ptn
wget --continue --verbose --tries=10 --output-document eicar_malware.yara https://raw.githubusercontent.com/IBM/qradar-yara-app-samples/main/eicar_malware.yar
wget --continue --verbose --tries=10 --output-document Follina_CVE_2022_30190_Generic.yara https://gist.githubusercontent.com/ammasajan/69722ae3bd8e1bdd30e32904c8cab8f9/raw/0e194c5a745f0d2113025c7f9f0a23c466fc544d/gistfile1.txt
wget --continue --verbose --tries=10 --output-document Zerologon_CVE-2020-1472.yara https://gist.githubusercontent.com/socbizone/6a5c69e9f2783438785631c5ea691f79/raw/77d7c90452530e09452f8807033afa7ce76ea15c/CVE-2020-1472.yara

git clone -v https://github.com/FirehaK/YARA FirehaK
git clone -v https://github.com/stairwell-inc/threat-research stairwell-inc
git clone -v https://github.com/codewatchorg/Burp-Yara-Rules codewatchorg
git clone -v https://github.com/nao-sec/yara_rules nao-sec
git clone -v https://github.com/Penetrum-Security/Malware-Yara-Rules Penetrum-Security
git clone -v https://github.com/naveenselvan/malware naveenselvan
git clone -v https://github.com/droberson/yararules droberson
git clone -v https://github.innominds.com/lprat/static_file_analysis innominds_lprat
git clone -v https://github.innominds.com/deadbits/yara-rules innominds_deadbits
git clone -v https://github.innominds.com/stratosphereips/yara-rules innominds_stratosphereips
git clone -v https://github.com/SadFud/YARA.Rules SadFud
git clone -v https://github.com/FerdiGul/Yara-Rules FerdiGul
git clone -v https://github.com/Sec-Soup/Yara-Rules Sec-Soup
git clone -v https://github.com/fpt-eagleeye/rules fpt-eagleeye
git clone -v https://github.com/ditekshen/detection ditekshen
git clone -v https://github.com/hpthreatresearch/tools hpthreatresearch
git clone -v https://github.com/ZephrFish/Random-Yara-Rules ZephrFish_Random-Yara-Rules
#git clone -v https://github.com/DanielRuf/yara-rules-free DanielRuf
git clone -v https://github.com/CiscoCXSecurity/log4j CiscoCXSecurity
git clone -v https://github.com/Phoul/yara_rules Phoul
git clone -v https://github.com/seyyid-bh/FireEyeHackDetection seyyid-bh
git clone -v https://github.com/ahhh/YARA ahhh
git clone -v https://github.com/3vangel1st/Yara 3vangel1st
git clone -v https://github.com/timb-machine/linux-malware timb-machine
git clone -v https://github.com/JPCERTCC/jpcert-yara JPCERTCC
git clone -v https://github.com/naxonez/YaraRules naxonez
git clone -v https://github.com/StrangerealIntel/Orion StrangerealIntel_Orion
git clone -v https://github.com/cocaman/yararules cocaman
git clone -v https://github.com/sebdraven/yara-rules sebdraven
git clone -v https://github.com/instacyber/yara instacyber
git clone -v https://github.com/BushidoUK/IOCs-YARAs BushidoUK
git clone -v https://github.com/imp0rtp3/yara-rules imp0rtp3_yara-rules
git clone -v https://github.com/imp0rtp3/js-yara-rules imp0rtp3_js-yara-rules
git clone -v https://github.com/pmelson/yara_rules pmelson
git clone -v https://github.com/oldmonk007/myrep oldmonk007_myrep
git clone -v https://github.com/oldmonk007/myrep1 oldmonk007_myrep1
git clone -v https://github.com/avast/ioc avast
git clone -v https://github.com/nembo81/YaraRules nembo81
git clone -v https://github.com/S3cur3Th1sSh1t/NimGetSyscallStub
git clone -v https://github.com/1aN0rmus/Yara 1aN0rmus
git clone -v https://github.com/0pc0deFR/YaraRules 0pc0deFR
git clone -v https://github.com/malware-kitten/public_yara_rules malware-kitten
git clone -v https://github.com/abhinavbom/Yara-Rules abhinavbom
git clone -v https://github.com/jipegit/yara-rules-public jipegit
git clone -v https://github.com/ProIntegritate/Yara-rules ProIntegritate
git clone -v https://github.com/umair9747/yara-rules
git clone -v https://github.com/CofenseLabs/Coronavirus-Phishing-Yara-Rules
git clone -v https://github.com/VectraThreatLab/reyara VectraThreatLab
#git clone -v https://github.com/LD-Skystars/LightDefender-yara-rules LD-Skystars
git clone -v https://github.com/401trg/detections 401trg
git clone -v https://github.com/simonk9/yara_habibi simonk9
git clone -v https://github.com/executemalware/Malware-IOCs executemalware
git clone -v https://github.com/target/mmk-ui-api target
git clone -v https://gitlab.com/pollonegro/yara-rules/ pollonegro
git clone -v https://github.com/ulisesrc/yararules ulisesrc
git clone -v https://github.com/SIFalcon/Detection SIFalcon
git clone -v https://github.com/sirpedrotavares/SI-LAB-Yara_rules sirpedrotavares
git clone -v https://github.com/zourick/yrepo zourick
git clone -v https://github.com/SinkingYachts/yara-rules SinkingYachts
git clone -v https://research.loginsoft.com/yara-rules/ loginsoft.com
git clone -v https://github.com/bi-zone/Log4j_Detector bi-zone
git clone -v https://github.com/Cluster25/detection Cluster25
git clone -v https://github.com/Neo23x0/signature-base Neo23x0_signature-base
git clone -v https://github.com/Neo23x0/yara-type-selectors Neo23x0_yara-type-selectors
git clone -v https://github.com/0xthr3atint31/yara_rules 0xthr3atint31
git clone -v https://github.com/resteex0/yarex resteex0
git clone -v https://github.com/imp0rtp3/yara-rules imp0rtp3
git clone -v https://github.com/sbousseaden/YaraHunts sbousseaden
git clone -v https://github.com/k-vitali/Malware-Misc-RE k-vitali
git clone -v https://github.com/cbecks2/yara cbecks2
git clone -v https://github.com/g-les/100DaysofYARA g-les
git clone -v https://github.com/IntelCorgi/Yara_Rules_and_IOCs IntelCorgi
git clone -v https://github.com/albertzsigovits/malware-notes albertzsigovits
git clone -v https://github.com/secman-pl/yaras secman-pl
git clone -v https://github.com/EmergingThreats/threatresearch EmergingThreats
git clone -v https://github.com/rakovskij-stanislav/pyc_rules rakovskij-stanislav
git clone -v https://github.com/AhmetPayaslioglu/MyYaraRules AhmetPayaslioglu
git clone -v https://github.com/righel/yara-rules righel
git clone -v https://github.com/manhalab/Yara_Rulez manhalab
git clone -v https://github.com/rivitna/Malware rivitna
git clone -v https://github.com/tsumarios/AMAYARA-Lab tsumarios
git clone -v https://github.com/nasbench/Yara-Rules nasbench
git clone -v https://github.com/JPCERTCC/MalConfScan MalConfScan
git clone -v https://github.com/n0leptr/yara-rules n0leptr
git clone -v https://github.com/ulisesrc/Yara-Rule ulisesrc
git clone -v https://github.com/alex-cart/LEAF alex-cart
git clone -v https://github.com/ChaitanyaHaritash/rules ChaitanyaHaritash
git clone -v https://github.com/farhanfaisal/yararule_web farhanfaisal
git clone -v https://github.com/BassGonzilla/YARA BassGonzilla
git clone -v https://github.com/Sprite-Pop/YaraRules Sprite-Pop
git clone -v https://github.com/alvosec/yara-rules alvosec
git clone -v https://github.com/th3b3ginn3r/CVE-2022-26134-Exploit-Detection th3b3ginn3r
git clone -v https://gitlab.com/gavz/yara-rules gavz
git clone -v https://gitlab.com/CashlessConsumer/yararules CashlessConsumer
git clone -v https://github.com/SecSamDev/personal-yaras SecSamDev
git clone -v https://github.com/volexity/threat-intel volexity
git clone -v https://github.com/Onils/yara-linux-malware Onils
git clone -v https://github.com/3vangel1st/100DaysOfYARA 3vangel1st_100DaysOfYARA
git clone -v https://github.com/delyee/yara_rules delyee
git clone -v https://github.com/jamesbower/Yara jamesbower
git clone -v https://github.com/aleprada/my_yara_rules aleprada
git clone -v https://github.com/jemik/OSX-ATT-CK-feature-mapping jemik
git clone -v https://github.com/sathishshan/S_Yara-ruleset sathishshan
git clone -v https://github.com/wesinator/OperationSMN-YARA-sigs wesinator
git clone -v https://github.com/alex-gehrig/yara-rules alex-gehrig
git clone -v https://github.com/jaegeral/yara-forensics-rules jaegeral
git clone -v https://github.com/paulzroe/yara_rules paulzroe
git clone -v https://github.com/forensenellanebbia/My-Yara-Rules forensenellanebbia
git clone -v https://github.com/2096779623/awa-yara-rules 2096779623
#git clone -v https://github.com/ALittlePatate/Yara-rules ALittlePatate
git clone -v https://github.com/malpedia/signator-rules malpedia_signator-rules
git clone -v https://github.com/CD-R0M/HundredDaysofYARA CD-R0M_HundredDaysofYARA
git clone -v https://github.com/CD-R0M/YARA CD-R0M_YARA
git clone -v https://github.com/V1D1AN/S1EM V1D1AN
git clone -v https://github.com/jakubd/malware-signatures jakubd
git clone -v https://github.com/S3cur3Th1sSh1t/NimGetSyscallStub NimGetSyscallStub
git clone -v https://github.com/h4wkst3r/SCMKit h4wkst3r
git clone -v https://github.com/nobodyatall648/yara-rules nobodyatall648
git clone -v https://github.com/anil-yelken/yara-rules anil-yelken
git clone -v https://github.com/Mike-Resheph/Yara-rules Mike-Resheph
git clone -v https://github.com/elastic/protections-artifacts elastic
git clone -v https://github.com/optiv/Yara-Rules optiv
git clone -v https://github.com/cert-ee/cuckoo3_signatures cert-ee
git clone -v https://github.com/Gokulgoky1/yara-rules Gokulgoky1
git clone -v https://github.com/advanced-threat-research/Yara-Rules advanced-threat-research_Yara-Rules 
git clone -v https://github.com/advanced-threat-research/Russian_CyberThreats_Yara advanced-threat-research_Russian_CyberThreats_Yara
git clone -v https://github.com/JPMinty/Detection_Engineering_Signatures JPMinty
git clone -v https://github.com/daredumidu/yara_rules daredumidu
# rm -rf CybercentreCanada
git clone -v https://github.com/CybercentreCanada/assemblyline-service-yara CybercentreCanada
git clone -v https://github.com/evild3ad/yara-rules evild3ad
git clone -v https://github.com/embee-research/APIHashReplace embee-research_APIHashReplace
# rm -rf embee-research_Yara
git clone -v https://github.com/embee-research/Yara embee-research_Yara
git clone -v https://github.com/taogoldi/YARA taogoldi 
git clone -v https://github.com/MalGamy/YARA_Rules MalGamy_YARA_Rules
git clone -v https://github.com/mohabye/Emotet_yara_rule mohabye_Emotet_yara_rule
git clone -v https://github.com/wxsBSD/100daysofyara wxsBSD
git clone -v https://github.com/stvemillertime/100daysofYARA-2022 stvemillertime_100daysofYARA-2022
git clone -v https://github.com/stvemillertime/100daysofYARA-2023 stvemillertime_100daysofYARA-2023
git clone -v https://github.com/chronicle/GCTI GCTI 
git clone -v https://github.com/galkofahi/Yara-Rules galkofahi
git clone -v https://github.com/yeti-platform/TibetanBrownBear TibetanBrownBear
git clone -v https://github.com/knightsc/XProtect knightsc_XProtect
#git clone -v https://github.com/viper-framework/yara-rules viper-framework
git clone -v https://github.com/hackOtack/YaraForPentest hackOtack_YaraForPentest
git clone -v https://github.com/InQuest/awesome-yara InQuest_awesome-yara
# rm -rf ndaal_yara_passwords_default
git clone -v https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default ndaal_yara_passwords_default
# rm -rf ndaal_public_yara_passwords_default
git clone -v https://gitlab.com/ndaal_open_source/ndaal_public_yara_passwords_default ndaal_public_yara_passwords_default
# rm -rf ndaal_yara_passwords_weak
git clone -v https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_weak ndaal_yara_passwords_weak
# rm -rf ndaal_public_yara_passwords_weak
git clone -v https://gitlab.com/ndaal_open_source/ndaal_public_yara_passwords_weak ndaal_public_yara_passwords_weak
# rm -rf ndaal_public_yara_gtfobins_unix_binaries_that_can_be_used_to_bypass_local_security_restrictions_in_misconfigured_systems
git clone -v https://gitlab.com/ndaal_open_source/ndaal_public_yara_gtfobins_unix_binaries_that_can_be_used_to_bypass_local_security_restrictions_in_misconfigured_systems
# rm -rf ndaal_public_yara_loldrivers_living_off_the_land_drivers
git clone -v https://gitlab.com/ndaal_open_source/ndaal_public_yara_living_off_the_orchard

git clone -v https://github.com/straysheep-dev/malware-analysis straysheep-dev_malware-analysis
git clone -v https://gitlab.com/Randsec/triage-yara-rules Randsec
git clone -v https://github.com/laruence/yar laruence
git clone -v https://github.com/100DaysofYARA/2022 100DaysofYARA_2022
# rm -rf 100DaysofYARA_2023
git clone -v https://github.com/100DaysofYARA/2023 100DaysofYARA_2023

git clone -v https://github.com/google/vxsig google_vxsig
git clone -v https://github.com/dr4k0nia/yara-rules dr4k0nia
git clone -v https://github.com/MayerDaniel/100DY_2023 MayerDaniel
git clone -v https://github.com/elceef/yara-rulz elceef_yara-rulz
git clone -v https://github.com/jstrosch/malware-signatures jstrosch_malware-signatures
git clone -v https://github.com/Idov31/Sandman Idov31_Sandman
git clone -v https://github.com/drbeni/malquarium 
git clone -v https://github.com/MayerDaniel/100DY_2023 MayerDaniel_100DY_2023
git clone -v https://github.com/0xThiebaut/Signatures 0xThiebaut_Signatures
git clone -v https://github.com/hvs-consulting/ioc_signatures hvs-consulting_ioc_signatures
git clone -v https://github.com/phbiohazard/Yara phbiohazard_Yara
git clone -v https://github.com/delivr-to/detections delivr-to_detections
git clone -v https://github.com/mandiant/red_team_tool_countermeasures mandiant_red_team_tool_countermeasures
git clone -v https://github.com/albertzsigovits/malware-yara albertzsigovits_malware-yara 
git clone -v https://github.com/candk-cyber/Custom-Rules-ClamAV candk-cyber_Custom-Rules-ClamAV
git clone -v https://github.com/fox-it/mkYARA fox-it_mkYARA
git clone -v https://github.com/fox-it/operation-wocao fox-it_operation-wocao
git clone -v https://github.com/CYB3RMX/Qu1cksc0pe Qu1cksc0pe
git clone -v https://github.com/securiteinfo/yara_qakbot
git clone -v https://github.com/phish-report/IOK phish-report_IOK 
git clone -v https://github.com/threatlabz/iocs threatlabz_iocs
git clone -v https://github.com/uptycslabs/yara-rules uptycslabs_yara-rules
git clone -v https://github.com/eset/malware-ioc eset_malware-ioc 
git clone -v https://github.com/trendmicro/research trendmicro_research
git clone -v https://github.com/magicsword-io/LOLDrivers magicsword-io_LOLDrivers
git clone -v https://github.com/AhmetPayaslioglu/YaraRules AhmetPayaslioglu_YaraRules
git clone -v https://github.com/zahidadeel/YARA-Rules zahidadeel_YARA-Rules
git clone -v https://github.com/nr1z/yara_rules nr1z_yara_rules
git clone -v https://github.com/Cyb3rtus/keepass_CVE-2023-24055_yara_rule Cyb3rtus_keepass_CVE-2023-24055_yara_rule
git clone -v https://github.com/securiteinfo/expl_outlook_cve_2023_23397_securiteinfo.yar securiteinfo_expl_outlook_cve_2023_23397
git clone -v https://github.com/airbus-cert/yara-ttd airbus-cert_yara-ttd
git clone -v https://github.com/3pun0x/RepoTele 3pun0x_RepoTele
git clone -v https://github.com/3pun0x/YaraRules 3pun0x_YaraRules 
git clone -v https://github.com/regeciovad/YaraRex-demo regeciovad_YaraRex-demo
git clone -v https://github.com/michelcrypt4d4mus/pdfalyzer michelcrypt4d4mus_pdfalyzer
git clone -v https://github.com/c3rb3ru5d3d53c/signatures c3rb3ru5d3d53c_signatures
git clone -v https://github.com/ladar/clamav-data ladar_clamav-data
git clone -v https://github.com/ruppde/yara_rules ruppde_yara_rules
git clone -v https://github.com/horsicq/XYara horsicq_XYara 
git clone -v https://github.com/signalblur/detection-artifacts signalblur_detection-artifacts

###
rm -rf .git && git init && git remote add -f fsf https://github.com/EmersonElectricCo/fsf  > /dev/null && echo 'fsf-server/yara' >> .git/info/sparse-checkout && git config core.sparseCheckout true && git config pull.rebase false && git pull fsf master > /dev/null


###

mkdir -p "extremeshok" || true

cd "${DESTINATION}extremeshok/" || exit
echo "${DESTINATION}extremeshok/"

echo "/usr/local/var/clamav/db/* ${DESTINATION}extremeshok/"
cp -v /usr/local/var/clamav/db/* "${DESTINATION}extremeshok/"

cd "${DESTINATION}" || exit
echo "${DESTINATION}"

# ClamAV
echo "Download ClamAV Signature Tool to convert to YARA"
echo "just for the record - the source was removed"
# wget -c -v https://raw.githubusercontent.com/js-on/clamav2yara/master/clamav2yara.py 
echo "Download clamav-unofficial-sigs.sh to fetch ClamAV signatures"
wget -c -v https://raw.githubusercontent.com/extremeshok/clamav-unofficial-sigs/master/clamav-unofficial-sigs.sh
echo "chmod +x clamav-unofficial-sigs.sh"
chmod +x "clamav-unofficial-sigs.sh"
./"clamav-unofficial-sigs.sh"
echo "chmod +x clamav-unofficial-sigs_ndaal.sh"
chmod +x "clamav-unofficial-sigs_ndaal.sh"
./"clamav-unofficial-sigs_ndaal.sh"

echo "Download atomicorp-sigs.sh to fetch ClamAV signatures"
wget -c -v https://raw.githubusercontent.com/stimpy23/clamav-unofficial-atomicorp-docker/master/update-atomicorp-sigs.sh
echo "chmod +x update-atomicorp-sigs.sh"
chmod +x "update-atomicorp-sigs.sh"
./"update-atomicorp-sigs.sh"

echo "/usr/local/var/clamav/db/*.yar ${DESTINATION}"
cp -v /usr/local/var/clamav/db/*.yar "${DESTINATION}" || true
echo "/usr/local/var/clamav/db/*.yara ${DESTINATION}"
cp -v /usr/local/var/clamav/db/*.yara "${DESTINATION}" || true


echo "unpack main.cvd with sigtool"
# https://docs.clamav.net/manual/Signatures.html#inspecting-signatures-inside-a-cvd-file

rm -f main.cvd daily.cld daily.cvd bytecode.cvd main.cvd.sha256 daily.cvd.sha256 bytecode.cvd.sha256 main.cvd.* daily.cvd.* && curl -LSOs https://github.com/ladar/clamav-data/raw/main/main.cvd.[01-10] -LSOs https://github.com/ladar/clamav-data/raw/main/main.cvd.sha256 -LSOs https://github.com/ladar/clamav-data/raw/main/daily.cvd.[01-10] -LSOs https://github.com/ladar/clamav-data/raw/main/daily.cvd.sha256 -LSOs https://github.com/ladar/clamav-data/raw/main/bytecode.cvd -LSOs https://github.com/ladar/clamav-data/raw/main/bytecode.cvd.sha256 && cat main.cvd.01 main.cvd.02 main.cvd.03 main.cvd.04 main.cvd.05 main.cvd.06 main.cvd.07 main.cvd.08 main.cvd.09 main.cvd.10 > main.cvd && cat daily.cvd.01 daily.cvd.02 daily.cvd.03 daily.cvd.04 daily.cvd.05 daily.cvd.06 daily.cvd.07 daily.cvd.08 daily.cvd.09 daily.cvd.10 > daily.cvd && sha256sum -c main.cvd.sha256 daily.cvd.sha256 bytecode.cvd.sha256 || { printf "ClamAV database download failed.\n" ; rm -f main.cvd daily.cvd bytecode.cvd ; } ; rm -f main.cvd.sha256 daily.cvd.sha256 bytecode.cvd.sha256 main.cvd.* daily.cvd.* 
rm -f main.cvd daily.cld daily.cvd bytecode.cvd main.cvd.sha256 daily.cvd.sha256 bytecode.cvd.sha256 main.cvd.* daily.cvd.* && freshclam --user $(id -n -u) --datadir=$(pwd) --config-file=$(pwd)/freshclam.conf && split --numeric-suffixes=01 --number=10 main.cvd main.cvd. && split --numeric-suffixes=01 --number=10 daily.cvd daily.cvd. && sha256sum main.cvd &> main.cvd.sha256 && sha256sum daily.cvd &> daily.cvd.sha256 && sha256sum bytecode.cvd &> bytecode.cvd.sha256 && sha256sum -c main.cvd.sha256 daily.cvd.sha256 bytecode.cvd.sha256 &> /dev/null && [ "$(git status -s --untracked-files=no -- main.cvd.[0-1][0-9] main.cvd.sha256 daily.cvd.[0-1][0-9] daily.cvd.sha256 bytecode.cvd bytecode.cvd.sha256 | wc -l)" -ne "0" ] && { git add main.cvd.[0-1][0-9] main.cvd.sha256 daily.cvd.[0-1][0-9] daily.cvd.sha256 bytecode.cvd bytecode.cvd.sha256 && git commit -m "Database Files Updated / $(date --utc +%Y%m%d-%H%M) UTC" && git push ; } ; git checkout -- main.cvd.[0-1][0-9] main.cvd.sha256 daily.cvd.[0-1][0-9] daily.cvd.sha256 bytecode.cvd bytecode.cvd.sha256

sigtool --unpack main.cvd
echo "unpack daily.cvd with sigtool"
sigtool --unpack daily.cvd
python3 clamav2yara.py -d
python3 clamav2yara.py -i main.ndb -o haupt.yara
python3 clamav2yara.py -i daily.ndb -o daily.yara
python3 clamav2yara.py -i interserver256.hdb -o interserver256.yara
python3 clamav2yara.py -i malwareexpert.ndb -o malwareexpert.yara

python3 clamav2yara.py -i "${DESTINATION}extremeshok/badmacro.ndb" -o "${DESTINATION}badmacro.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/blurl.ndb" -o "${DESTINATION}blurl.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/bofhland_cracked_URL.ndb" -o "${DESTINATION}bofhland_cracked_URL.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/bofhland_malware_attach.hdb" -o "${DESTINATION}bofhland_malware_attach.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/bofhland_malware_URL.ndb" -o "${DESTINATION}bofhland_malware_URL.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/bofhland_phishing_URL.ndb" -o "${DESTINATION}bofhland_phishing_URL.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/hackingteam.hsb" -o "${DESTINATION}hackingteam.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/foxhole_filename.cdb" -o "${DESTINATION}foxhole_filename.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/foxhole_generic.cdb" -o "${DESTINATION}foxhole_generic.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/foxhole_js.ndb" -o "${DESTINATION}foxhole_js.yara"

python3 clamav2yara.py -i "${DESTINATION}extremeshok/junk.ndb" -o "${DESTINATION}junk.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/jurlbl.ndb" -o "${DESTINATION}jurlbl.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/jurlbla.ndb" -o "${DESTINATION}jurlbla.yara"

python3 clamav2yara.py -i "${DESTINATION}extremeshok/lott.ndb" -o "${DESTINATION}lott.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/malwarehash.hsb" -o "${DESTINATION}malwarehash.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/MiscreantPunch099-Low.ldb" -o "${DESTINATION}MiscreantPunch099-Low.yara"

python3 clamav2yara.py -i "${DESTINATION}extremeshok/phish.ndb" -o "${DESTINATION}phish.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/phishtank.ndb" -o "${DESTINATION}phishtank.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/porcupine.ndb" -o "${DESTINATION}porcupine.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/scam.ndb" -o "${DESTINATION}scam.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/sigs.ndb" -o "${DESTINATION}sigs.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/shelter.ldb" -o "${DESTINATION}shelter.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/spam.ldb" -o "${DESTINATION}spam.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/spamattach.hdb" -o "${DESTINATION}spamattach.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/spamimg.hdb" -o "${DESTINATION}spamimg.yara"

python3 clamav2yara.py -i "${DESTINATION}extremeshok/spear.ndb" -o "${DESTINATION}spear.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/spearl.ndb" -o "${DESTINATION}spearl.yara"

python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow.attachments.hdb" -o "${DESTINATION}winnow_attachments.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow.complex.patterns.ldb" -o "${DESTINATION}winnow_complex_patterns.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_bad_cw.hdb" -o "${DESTINATION}winnow_bad_cw.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_extended_malware.hdb" -o "${DESTINATION}winnow_extended_malware.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_extended_malware_links.ndb" -o "${DESTINATION}winnow_extended_malware_links.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_malware.hdb" -o "${DESTINATION}winnow_extended_malware.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_phish_complete.ndb" -o "${DESTINATION}winnow_phish_complete.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_phish_complete_url.ndb" -o "${DESTINATION}winnow_phish_complete_url.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/winnow_spam_complete.ndb" -o "${DESTINATION}winnow_spam_complete.yara"


python3 clamav2yara.py -i "${DESTINATION}extremeshok/interserver256.hdb" -o "${DESTINATION}interserver256.yara"
python3 clamav2yara.py -i "${DESTINATION}extremeshok/spamimg.hdb" -o "${DESTINATION}spamimg.yara"

Function_Convert_ClamAV_Signatures_to_YARA_Rules
Function_Rename_Rules

###

Function_Git_Pull_Repos

###

Function_Clean_Up_YARA_Rules
Function_Rename_Rules
echo "${DESTINATION}"
rm -v "${DESTINATION}"*.yar
rm -v "${DESTINATION}"*_index.yar
rm -v "${DESTINATION}malpedia"*.yar

cp -r -v -p ${DESTINATION}*.yara "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/"

cd "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/" || exit
echo "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/Yara-Repo-master.save/"

rm -r -v *.yar
rm -r -v *_index.yar

cp -n -r -v -p *.yara "${DESTINATION}"

Function_Clean_Up_Files

cd "${DESTINATION}" || exit
echo "${DESTINATION}"

Function_Clean_Up_Files

raw_list_urls="https://raw.githubusercontent.com/Yara-Rules/rules/master/index.yar https://raw.githubusercontent.com/Yara-Rules/rules/master/index.yar https://raw.githubusercontent.com/Yara-Rules/rules/master/index.yar"
IFS=' ' read -r -a urls <<< "${raw_list_urls}"

for url in "${urls[@]}" ; do
        echo "Getting ${url}" ; sleep 2
        curl "${url}" | grep include | cut -d'"' -f2
done

for f in $(find . -mindepth 1 ! -path "./$(basename $PWD)_all.yara" -type f -name "*.yara"); do
  if [[ "$f" == *"Mobile_Malware"* || "$f" == *"mobile"* ]]; then  # exclude Android rules
    continue
  fi
  echo "include \"$f\"" >> $(basename $PWD)_index.yara;
done;


cd "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/" || exit
echo "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/dataset/"

Function_Rename_Rules

# wget --continue --verbose --tries=10 --output-document index_gen.sh https://raw.githubusercontent.com/Yara-Rules/rules/master/index_gen.sh

chmod +x "index_gen.sh"
"${DESTINATION}index_gen.sh" || exit


echo " "
echo "Number of YARA Rules files with extension .yara  : " && find . -name "*.yara" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo " "
echo "Number of YARA Rules files with extension .yar  : " && find . -name "*.yar" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo " "
echo "Number of YARA Rules files with extension .yr*   : " && find . -name "*.yr*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo " "
echo "Number of YARA Rules files with extension .Y* : " && find . -name "*.Y*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo " "
echo "Number of YARA Rules files with extension .rule* : " && find . -name "*.rule*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo " "
echo "Number of YARA Rules files with extension .Rule* : " && find . -name "*.Rule*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"
echo " "
echo "Number of YARA Rules files with extension .txt* : " && find . -name "*.txt*" -type f | wc -l && echo "Maybe contain duplicate rules in different Repo(s)!"

echo " "
echo "cp -p -v /Users/${USERSCRIPT}/Yara-Repo-master/Yara-Repo.sh ${DESTINATION}"
echo "only necessary for workstation bootstraping"
cp -p -v /Users/${USERSCRIPT}/Yara-Repo-master/Yara-Repo.sh "${DESTINATION}" || true

echo "cp -p -v /Users/${USERSCRIPT}/Yara-Repo-master/Yara-Repo.sh /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/Yara-Repo.sh"
cp -p -v "/Users/${USERSCRIPT}/Yara-Repo-master/Yara-Repo.sh" "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/Yara-Repo.sh" || true

echo "cp -p -v ${DESTINATION}clamav-unofficial-sigs.sh /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/clamav-unofficial-sigs.sh"
cp -p -v "${DESTINATION}clamav-unofficial-sigs.sh" "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/clamav-unofficial-sigs.sh" || true

echo "cp -p -v ${DESTINATION}clamav-unofficial-sigs_ndaal.sh /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/clamav-unofficial-sigs_ndaal.sh"
cp -p -v "${DESTINATION}clamav-unofficial-sigs_ndaal.sh" "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/clamav-unofficial-sigs_ndaal.sh" || true

echo "cp -p -v ${DESTINATION} /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/update-atomicorp-sigs.sh"
cp -p -v "${DESTINATION}update-atomicorp-sigs.sh" "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/update-atomicorp-sigs.sh" || true

echo "rm -f /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/placeholder.txt"
rm -f "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/tools/placeholder.txt" || true

echo "rm -f /Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/documentation/placeholder.txt"
rm -f "/Users/${USERSCRIPT}/repos/ndaal_ml_data_set_030/documentation/placeholder.txt" || true

git branch yara
git checkout yara
#git branch --set-upstream-to=origin/master master
git pull --verbose
git add --verbose -A
git commit -m "updated YARA rules"
git push --verbose
#git lfs push --all
git config lfs.https://gitlab.com/vPierre/ndaal_ml_data_set_030.git/info/lfs.locksverify true
git push origin HEAD:yara
git branch --set-upstream-to=origin/yara yara
git push --set-upstream origin yara
git branch --set-upstream-to=origin/yara yara
git push --verbose --force

script_name1="basename ${0}"
script_path1="$(dirname "$(readlink -f "${0}")")"
script_path_with_name="${script_path1}/${script_name1}"
echo "Script path with name: ${script_path_with_name}"
echo "Script finished"
exit 0
