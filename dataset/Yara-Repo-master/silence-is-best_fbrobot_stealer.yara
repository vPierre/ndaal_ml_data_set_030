rule fbrobot_bin
{
    meta:
        description = "fbrobot stealer"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/317642cd-924b-4fe4-ba97-0c648f89c7a0"
        date = "2022/04"
        maltype = "Stealer"

    strings:
        $string1 = "VlcpVideoV1.0.1" wide
        $string2 = "firefox.exe" wide
        $string3 = "Library couldnt loaded" ascii

    condition:
        uint16(0) == 0x5A4D and all of ($string*) and filesize < 5MB
}

rule fbrobot_mem
{
    meta:
        description = "fbrobot stealer"
        author = "James_inthe_box"
        reference = "https://app.any.run/tasks/317642cd-924b-4fe4-ba97-0c648f89c7a0"
        date = "2022/04"
        maltype = "Stealer"

    strings:
        $string1 = "VlcpVideoV1.0.1" wide
        $string2 = "firefox.exe" wide
        $string3 = "Library couldnt loaded" ascii

    condition:
        all of ($string*) and filesize > 100KB
}