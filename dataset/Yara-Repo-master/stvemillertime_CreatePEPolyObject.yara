import "pe"
import "console"
rule CreatePEPolyObject {
    strings:
        $a = "CreatePEPolyObject" xor
        $b = "CreatePEPolyObject" nocase ascii wide
        $c = "CreatePEPolyObject" base64 base64wide
    condition:
        any of them
}
rule Export_CreatePEPolyObject {
    condition:
        pe.exports("CreatePEPolyObject")
}
rule Export_CreatePEPolyObject_Loop {
    condition:
        for any func in pe.export_details:
            (
                func.name contains "CreatePEPolyObject"
            )
}
rule PE_Export_Func_Name {
    meta:
        note = "Must have console module via yara-4.2.0-rc1+"
    condition:
        uint16(0) == 0x5A4D and
        for any func in pe.export_details:
            (
                console.log("Export Name: ", func.name)
            )
}
//CEO-PC >> ~/yara-4.2.0-rc1 % yara -r test-export.yar ~/vx/ | sort | grep -e 'Export: ' | sort | uniq -c    