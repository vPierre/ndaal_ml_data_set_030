/*
Generated by Yara-Rules
On 20-05-2023
*/
include "./SadFud/DMALocker-All-Versions.yara"
include "./SadFud/DMALocker4.0.yara"
include "./SadFud/Remcos_RAT.yara"
include "./SadFud/Ripper_ATM.yara"
include "./SadFud/Satana_Ransomware.yara"
include "./SadFud/Vbs-Obfuscated.yara"
