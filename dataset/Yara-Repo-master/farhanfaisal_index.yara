/*
Generated by Yara-Rules
On 20-05-2023
*/
include "./farhanfaisal/detect_blacklist_keyword.yara"
include "./farhanfaisal/detect_exploiter.yara"
include "./farhanfaisal/detect_filemanager.yara"
include "./farhanfaisal/detect_html_js.yara"
include "./farhanfaisal/detect_obfuscation.yara"
include "./farhanfaisal/detect_pattern.yara"
include "./farhanfaisal/detect_phpmailer.yara"
include "./farhanfaisal/detect_small_injector.yara"
include "./farhanfaisal/detect_small_malicios.yara"
include "./farhanfaisal/detect_small_uploader.yara"
include "./farhanfaisal/detect_webshell.yara"
include "./farhanfaisal/index.broad.yara"
