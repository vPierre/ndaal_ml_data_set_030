rule Bumblebee_mem
{
    meta:
        description = "Bumblebee loader"
        author = "James_inthe_box"
        reference = "7a2ac6664ef13971ce464676012092befde8f14b0013b2f0f3e21c9051cb45a0"
        date = "2022/05"
        maltype = "Loader"

    strings:
        $string1 = "client_id"
        $string2 = "group_name"
        $string3 = "qemu-ga" wide
        $string4 = "sys_version"
        $string5 = "client_version"
        $string6 = "task_state"

    condition:
        all of ($string*) and filesize > 1000KB
}