rule Follina_CVE_2022_30190_Generic
{             
                meta:
                                author = "Sajan Kumar"
                                reference = "https://doublepulsar.com/follina-a-microsoft-office-code-execution-vulnerability-1a47fce5629e"
                                reference1 = "https://github.com/scythe-jake/cti-campaigns/blob/main/Follina_msdt_calc-callout_scythe_threat.json"
                strings:
               		        $msdt1 = "ms-msdt:/id" ascii wide nocase
		                $msdt2 = "ms-msdt:-id" ascii wide nocase

		                $para1 = "IT_RebrowseForFile" ascii wide nocase
		                $para2 = "IT_BrowseForFile" ascii wide nocase
	                        $para3 = "IT_LaunchMethod=ContextMenu" ascii wide nocase
		                $para4 = "IT_SelectProgram=NotListed" ascii wide nocase

                condition:
                                (1 of ($msdt*) and 2 of ($para*))
}
