rule alg_salsa20 {
    meta:
        desc = "Identify code for the Salsa20 cryptographic algorithm."
        author = "@shellcromancer <root@shellcromancer.io>"
        version = "0.1"
        last_modified = "2022-01-02"
        reference = "https://www.goggleheadedhacker.com/blog/post/reversing-crypto-functions"
    strings:
        $const16 = "expand 16-byte k" ascii
        $const32 = "expand 32-byte k" ascii

        $code_qr_1 = { c1 c0 07 }   // rol eax, 7
        $code_qr_2 = { c1 c0 09 }   // rol eax, 9
        $code_qr_3 = { c1 c0 0d }   // rol eax, 0xd
        $code_qr_4 = { c1 c0 12 }   // rol eax, 0x12
    condition:
        1 of ($const*) and 2 of ($code*)
}