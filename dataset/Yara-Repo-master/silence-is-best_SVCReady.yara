rule svcready_bin
{
    meta:
        description = "SVCReady"
        author = "James_inthe_box"
        reference = "f690f484c1883571a8bbf19313025a1264d3e10f570380f7aca3cc92135e1d2e"
        date = "2022/05"
        maltype = "RAT"

    strings:
        $string1 = "svc run"
        $string2 = "svc commonMain"

    condition:
        uint16(0) == 0x5A4D and all of ($string*) and filesize < 1500KB
}

rule svcready_mem
{
    meta:
        description = "SVCReady"
        author = "James_inthe_box"
        reference = "f690f484c1883571a8bbf19313025a1264d3e10f570380f7aca3cc92135e1d2e"
        date = "2022/05"
        maltype = "RAT"

    strings:
        $string1 = "svc run"
        $string2 = "svc commonMain"
        $string3 = "Logger"
        $string4 = "screenshot"
        $string5 = "systeminfo"

    condition:
        all of ($string*) and filesize > 1500KB
}