rule unastealer3_other
{
    meta:
        description = "Una Stealer"
        author = "James_inthe_box"
        reference = "https://www.hybrid-analysis.com/string-search/results/54fb74afabde582ae0a730401ea31ee5e0d9cf33582c8a64d634350150cdd78b"
        date = "2020/07"
        maltype = "Stealer"

    strings:
        $string1 = "AVAST Software"
        $string2 = "name_on_card, expiration_month, expiration_year, card_number_encrypted FROM credit_cards"
        $string3 = "_files" ascii wide nocase
        $string4 = "%AppData%\\waves-exchange" wide
        $string5 = "files_" ascii wide nocase

    condition:
        uint16(0) == 0x5A4D and 4 of ($string*) and filesize < 4000KB
}

rule unastealer3_mem
{
    meta:
        description = "Una Stealer"
        author = "James_inthe_box"
        reference = "https://www.hybrid-analysis.com/string-search/results/54fb74afabde582ae0a730401ea31ee5e0d9cf33582c8a64d634350150cdd78b"
        date = "2020/07"
        maltype = "Stealer"

    strings:
        $string1 = "AVAST Software"
        $string2 = "name_on_card, expiration_month, expiration_year, card_number_encrypted FROM credit_cards"
        $string3 = "_files" ascii wide nocase
        $string4 = "%AppData%\\waves-exchange" wide
        $string5 = "files_" ascii wide nocase

    condition:
        4 of ($string*) and filesize > 4000KB
}

rule unastealer3_bin
{
    meta:
        description = "Una Stealer"
        author = "James_inthe_box"
        reference = "https://www.hybrid-analysis.com/yara-search/results/526f6ebd0e1dafa57c797ab3e0165c03abe0d21d03287429973e7f78814f342a"
        date = "2020/07"
        maltype = "Stealer"

    strings:
        $string1 = "AVAST Software"
        $string2 = "name_on_card, expiration_month, expiration_year, card_number_encrypted FROM credit_cards"
        $string3 = "72.12.194.90" ascii
        $string4 = "%AppData%\\waves-exchange" wide

    condition:
        all of ($string*) and filesize < 4000KB
}