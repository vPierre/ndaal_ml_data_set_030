import "hash"

rule Sanesecurity_SpamAttach_001
{
    condition:
        hash.md5(0, filesize) == "480ebdf6aa73b09a5c08438e25ed7e13dab7f9529f6106382af00d9d11f77853"
}
rule Sanesecurity_SpamAttach_002
{
    condition:
        hash.md5(0, filesize) == "ef1525ad9115744d6c3d80a6066a8ffb3958fd031d7a3d6aa128f369b8ff92d1"
}
rule Sanesecurity_SpamAttach_003
{
    condition:
        hash.md5(0, filesize) == "41f0ffdec9df55686e730f9e395ddff9fcab1b915cae59b74cc8ed17c7719fb3"
}
rule Sanesecurity_SpamAttach_004
{
    condition:
        hash.md5(0, filesize) == "63ac6791ce7907ac9b2ba6334f0f9dab44b08137f6c15753de715547ba7d43cc"
}
rule Sanesecurity_SpamAttach_005
{
    condition:
        hash.md5(0, filesize) == "53d459761fb40f109f7827762565352969c5cc720c7e272666b13686d5a7cb94"
}
rule Sanesecurity_SpamAttach_006
{
    condition:
        hash.md5(0, filesize) == "f99684ea1c484cf2382bd9cdc6b40383170bde049bd45b83022a1b9da7efe795"
}
rule Sanesecurity_SpamAttach_007
{
    condition:
        hash.md5(0, filesize) == "25f9a0053ab250e7107fa8d522017ea2189e7513f1be04ee6f79e6082d8b3169"
}
rule Sanesecurity_SpamAttach_008
{
    condition:
        hash.md5(0, filesize) == "c36d0e2baa0b43eb62796c23e2a5268811107fae5686504fb4cadecc4a4d4f55"
}
rule Sanesecurity_SpamAttach_009
{
    condition:
        hash.md5(0, filesize) == "5b987559d353f0b02df6497243fd6472bc4233a840eb6385cb2afd53e592e828"
}
rule Sanesecurity_SpamAttach_010
{
    condition:
        hash.md5(0, filesize) == "b94a9004a328b1d07af6c658cc5566134c10b7fa8dab542eb1e2c1aa700adaca"
}
rule Sanesecurity_SpamAttach_011
{
    condition:
        hash.md5(0, filesize) == "8ca173c03dc89228671057da20af795948a53838c1b71327fafab688423f99a1"
}
rule Sanesecurity_SpamAttach_012
{
    condition:
        hash.md5(0, filesize) == "55111b49db5c6d22554085923e80fc910fbeb0a65c76fa432517518fc06a9328"
}
rule Sanesecurity_SpamAttach_013
{
    condition:
        hash.md5(0, filesize) == "bf8ffb390d18a35fe1ce44b639d79f32a0a4745fa437e2008b5e87a8d4f95fc8"
}
rule Sanesecurity_SpamAttach_014
{
    condition:
        hash.md5(0, filesize) == "09251ada89ea4edaf813b0cf90e24b1c0bda8f83a687ef49f9bfadd95bf2bd70"
}