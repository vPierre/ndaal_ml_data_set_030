rule Nuitka { 
  meta:
    description = "Python code compiled and packaged with Nuitka"
    author = "William Ballenthin <william.ballenthin@mandiant.com>"
  strings:
      // https://github.com/Nuitka/Nuitka/blob/f87667fec2748a735834fc699daa20cedfb8f2c7/nuitka/build/static_src/InspectPatcher.c#L218
      $a1 = "nuitka_types_patch"
      // https://github.com/Nuitka/Nuitka/blob/f87667fec2748a735834fc699daa20cedfb8f2c7/nuitka/build/static_src/MetaPathBasedLoader.c#L1173
      $a2 = "O:is_package"
      // https://github.com/Nuitka/Nuitka/blob/f87667fec2748a735834fc699daa20cedfb8f2c7/nuitka/build/static_src/HelpersConstantsBlob.c#L1229
      $a3 = "Error, corrupted constants object"
  condition: 
    uint16(0) == 0x5A4D
    and all of them
}