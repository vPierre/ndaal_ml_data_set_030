import "pe"

rule Template_Match_CodeView_PDB_Paths
{
    meta:
        description = "Template YARA rule for matching PDB paths in PE files via CodeView debug information."
        author = "Tom Bonner (tbonner@blackberry.com)"

    strings:
        // One or more PDB paths
        $pdb_path1 = "C:\\TestPath" ascii nocase
        $pdb_path2 = "C:\\AnotherPath" ascii nocase
        $pdb_path3 = "C:\\More\\Paths\\If\\You\\Like" ascii nocase
        
    condition:
        // Contains an MZ header?
        uint16(0) == 0x5a4d and
        
        // Contains debug directory?
        pe.data_directories[pe.IMAGE_DIRECTORY_ENTRY_DEBUG].size > 0 and 
        
        // Type: IMAGE_DEBUG_TYPE_CODEVIEW?
        uint32(pe.rva_to_offset(pe.data_directories[pe.IMAGE_DIRECTORY_ENTRY_DEBUG].virtual_address + 0x0c)) == 2 and
        
        // Contains RSDS signature?
        uint32(pe.rva_to_offset(uint32(pe.rva_to_offset(pe.data_directories[pe.IMAGE_DIRECTORY_ENTRY_DEBUG].virtual_address + 0x14)))) == 0x53445352 and
        
        // Any PDB path(s) in the IMAGE_DEBUG_TYPE_CODEVIEW debug directory?
        for any of ($pdb_path*) : 
		( 
			$ in
			(
				pe.rva_to_offset(uint32(pe.rva_to_offset(pe.data_directories[pe.IMAGE_DIRECTORY_ENTRY_DEBUG].virtual_address + 0x14)))
				..
				pe.rva_to_offset(uint32(pe.rva_to_offset(pe.data_directories[pe.IMAGE_DIRECTORY_ENTRY_DEBUG].virtual_address + 0x14))) + uint32(pe.rva_to_offset(pe.data_directories[pe.IMAGE_DIRECTORY_ENTRY_DEBUG].virtual_address + 0x10))
			)
		)
}