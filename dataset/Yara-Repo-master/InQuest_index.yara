/*
Generated by Yara-Rules
On 20-05-2023
*/
include "./InQuest/Adobe_Flash_DRM_Use_After_Free.yara"
include "./InQuest/AgentTesla.yara"
include "./InQuest/Base64_Encoded_Powershell_Directives.yara"
include "./InQuest/CVE_2018_4878_0day_ITW.yara"
include "./InQuest/ClamAV_Emotet_String_Aggregrate.yara"
include "./InQuest/Embedded_PE.yara"
include "./InQuest/Excel_Hidden_Macro_Sheet.yara"
include "./InQuest/Executable_Converted_to_MSI.yara"
include "./InQuest/GlowSpark_Downloader.yara"
include "./InQuest/Hex_Encoded_Powershell.yara"
include "./InQuest/Hidden_Bee_Elements.yara"
include "./InQuest/Hunting_Rule_ShikataGaNai.yara"
include "./InQuest/IQY_File.yara"
include "./InQuest/IQY_File_With_Pivot_Extension_URL.yara"
include "./InQuest/IQY_File_With_Suspicious_URL.yara"
include "./InQuest/MSIExec_Pivot.yara"
include "./InQuest/Microsoft_Excel_Data_Connection.yara"
include "./InQuest/Microsoft_Office_DDE_Command_Execution.yara"
include "./InQuest/Microsoft_Office_Document_with_Embedded_Flash_File.yara"
include "./InQuest/Microsoft_XLSX_with_Macrosheet.yara"
include "./InQuest/NTLM_Credentials_Theft_via_PDF_Files.yara"
include "./InQuest/PDF_Document_with_Embedded_IQY_File.yara"
include "./InQuest/PE.yara"
include "./InQuest/RTF_Byte_Nibble_Obfuscation.yara"
include "./InQuest/Signed_Executable_With_Custom_Elliptic_Curve_Parameters.yara"
include "./InQuest/Symbolic_Link_Files_DLL_Reference_Suspicious_Characteristics.yara"
include "./InQuest/Symbolic_Link_Files_Macros_File_Characteristic.yara"
include "./InQuest/labs.inquest.net/excel40_hunter.yara"
include "./InQuest/labs.inquest.net/macro_hunter.yara"
include "./InQuest/labs.inquest.net/maldoc_hunter.yara"
include "./InQuest/labs.inquest.net/malfash_hunter.yara"
include "./InQuest/labs.inquest.net/maljar_hunter.yara"
include "./InQuest/labs.inquest.net/malpdf_hunter.yara"
include "./InQuest/labs.inquest.net/miscellaneous.yara"
include "./InQuest/labs.inquest.net/pdfjs_hunter.yara"
include "./InQuest/labs.inquest.net/phish_hunter.yara"
include "./InQuest/labs.inquest.net/rtf_hunter.yara"
include "./InQuest/labs.inquest.net/swf_hunter.yara"
