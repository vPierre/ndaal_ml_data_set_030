rule alg_crypto_des {
    meta:
        description = "Identify code/constants for the DES encryption algorithm."
        author = "@shellcromancer <root@shellcromancer.io>"
        version = "0.1"
        date = "2022-01-15"
        reference = "https://en.wikipedia.org/wiki/Data_Encryption_Standard"
        implementation = "https://cs.opensource.google/go/go/+/master:src/crypto/des/block.go"
    strings:
        $permInit = {
            06 0e 16 1e 26 2e 36 3e 
            04 0c 14 1c 24 2c 34 3c
            02 0a 12 1a 22 2a 32 3a
            00 08 10 18 20 28 30 38
            07 0f 17 1f 27 2f 37 3f
            05 0d 15 1d 25 2d 35 3d
            03 0b 13 1b 23 2b 33 3b
            01 09 11 19 21 29 31 39
        }

        // https://go.dev/play/p/lhfaBpy3cBG
        $permFin  = {
            18 38 10 30 08 28 00 20
            19 39 11 31 09 29 01 21
            1a 3a 12 32 0a 2a 02 22 
            1b 3b 13 33 0b 2b 03 23 
            1c 3c 14 34 0c 2c 04 24 
            1d 3d 15 35 0d 2d 05 25 
            1e 3e 16 36 0e 2e 06 26 
            1f 3f 17 37 0f 2f 07 27 
        }

        $sbox1 = { e4 d1 2f b8 3a 6c 59 07 }
        $sbox2 = { f1 8e 6b 34 97 2d c0 5a }
        $sbox3 = { a0 9e 63 f5 1d c7 b4 28  }
        $sbox4 = { 7d e3 06 9a 12 85 bc 4f }
        $sbox5 = { 2c 41 7a b6 85 3f d0 e9 }
        $sbox6 = { c1 af 92 68 0d 34 e7 5b }
        $sbox7 = { 4b 2e f0 8d 3c 97 5a 61 }
        $sbox8 = { d2 84 6f b1 a9 3e 50 c7 }

    condition:
        any of ($perm*) or 4 of ($sbox*)
}