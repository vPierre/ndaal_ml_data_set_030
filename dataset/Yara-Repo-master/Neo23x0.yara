





<!DOCTYPE html>
<html lang="en" data-color-mode="auto" data-light-theme="light" data-dark-theme="dark" data-a11y-animated-images="system">
  <head>
    <meta charset="utf-8">
  <link rel="dns-prefetch" href="https://github.githubassets.com">
  <link rel="dns-prefetch" href="https://avatars.githubusercontent.com">
  <link rel="dns-prefetch" href="https://github-cloud.s3.amazonaws.com">
  <link rel="dns-prefetch" href="https://user-images.githubusercontent.com/">
  <link rel="preconnect" href="https://github.githubassets.com" crossorigin>
  <link rel="preconnect" href="https://avatars.githubusercontent.com">

  

  <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/light-0946cdc16f15.css" /><link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/dark-3946c959759a.css" /><link data-color-theme="dark_dimmed" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_dimmed-9b9a8c91acc5.css" /><link data-color-theme="dark_high_contrast" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_high_contrast-11302a585e33.css" /><link data-color-theme="dark_colorblind" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_colorblind-1a4564ab0fbf.css" /><link data-color-theme="light_colorblind" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/light_colorblind-12a8b2aa9101.css" /><link data-color-theme="light_high_contrast" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/light_high_contrast-5924a648f3e7.css" /><link data-color-theme="light_tritanopia" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/light_tritanopia-05358496cb79.css" /><link data-color-theme="dark_tritanopia" crossorigin="anonymous" media="all" rel="stylesheet" data-href="https://github.githubassets.com/assets/dark_tritanopia-aad6b801a158.css" />
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/primer-primitives-fb1d51d1ef66.css" />
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/primer-57c312e484b2.css" />
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/global-0d04dfcdc794.css" />
    <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/github-c7a3a0ac71d4.css" />
  



  <script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/wp-runtime-377d421cc9f7.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_stacktrace-parser_dist_stack-trace-parser_esm_js-node_modules_github_bro-a4c183-ae93d3fba59c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/ui_packages_failbot_failbot_ts-e38c93eab86e.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/environment-de3997b81651.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_selector-observer_dist_index_esm_js-2646a2c533e3.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_relative-time-element_dist_index_js-99e288659d4f.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_fzy_js_index_js-node_modules_github_markdown-toolbar-element_dist_index_js-e3de700a4c9d.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_delegated-events_dist_index_js-node_modules_github_auto-complete-element-5b3870-ff38694180c6.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_file-attachment-element_dist_index_js-node_modules_github_text-ex-3415a8-7ecc10fb88d0.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_filter-input-element_dist_index_js-node_modules_github_remote-inp-8873b7-5771678648e0.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_primer_view-components_app_components_primer_primer_js-node_modules_gith-3af896-ba2b2ef33e4b.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/github-elements-7b037525f59f.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/element-registry-8f404beaf269.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_lit-html_lit-html_js-9d9fe1859ce5.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_mini-throttle_dist_index_js-node_modules_github_alive-client_dist-bf5aa2-424aa982deef.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_turbo_dist_turbo_es2017-esm_js-ba0e4d5b3207.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_color-convert_index_js-node_modules_github_jtml_lib_index_js-40bf234a19dc.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_remote-form_dist_index_js-node_modules_scroll-anchoring_dist_scro-52dc4b-e1e33bfc0b7e.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_paste-markdown_dist_index_esm_js-node_modules_github_quote-select-743f1d-1b20d530fbf0.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_updatable-content_ts-dadb69f79923.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_behaviors_keyboard-shortcuts-helper_ts-app_assets_modules_github_be-f5afdb-3f05df4c282b.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_blob-anchor_ts-app_assets_modules_github_code-editor_ts-app_assets_-8128e1-65aa849c94d7.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_sticky-scroll-into-view_ts-1d145b63ed56.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_behaviors_ajax-error_ts-app_assets_modules_github_behaviors_include-2e2258-dae7d38e0248.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_behaviors_commenting_edit_ts-app_assets_modules_github_behaviors_ht-83c235-c97eacdef68a.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/behaviors-d1b433c1b6c2.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_delegated-events_dist_index_js-node_modules_github_catalyst_lib_index_js-623425af41e1.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/notifications-global-4dc6f295cc92.js"></script>
  
  <script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/vendors-node_modules_github_clipboard-copy-element_dist_index_esm_js-node_modules_github_remo-71f63c-31b13c4ecaaf.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/app_assets_modules_github_diffs_blob-lines_ts-app_assets_modules_github_diffs_linkable-line-n-f96c66-3aec21e0f76c.js"></script>
<script crossorigin="anonymous" defer="defer" type="application/javascript" src="https://github.githubassets.com/assets/gist-2e46fe8ac96d.js"></script>


  <title>IDDQD - Godmode YARA Rule · GitHub</title>



  <meta name="route-pattern" content="/:user_id/:gist_id(.:format)">

    
  <meta name="current-catalog-service-hash" content="56253a530ab9027b25719525dcbe6007461a3202218f6f5dfce5a601c121cbcb">


  <meta name="request-id" content="5180:17B9:82E645B:84F0EAD:646918FB" data-pjax-transient="true"/><meta name="html-safe-nonce" content="92093d39a27685661788d5bd30d85981644e2422419bade71170ae3bb46456fa" data-pjax-transient="true"/><meta name="visitor-payload" content="eyJyZWZlcnJlciI6IiIsInJlcXVlc3RfaWQiOiI1MTgwOjE3Qjk6ODJFNjQ1Qjo4NEYwRUFEOjY0NjkxOEZCIiwidmlzaXRvcl9pZCI6IjM5NjQyNjcwNzE3MTA4ODYzNSIsInJlZ2lvbl9lZGdlIjoiZnJhIiwicmVnaW9uX3JlbmRlciI6ImZyYSJ9" data-pjax-transient="true"/><meta name="visitor-hmac" content="ed96357526c71a8bf4688a1cc60d3f4f9812f02f6716b3acc3c1860be39139c8" data-pjax-transient="true"/>




  <meta name="github-keyboard-shortcuts" content="" data-turbo-transient="true" />
  

  <meta name="selected-link" value="gist_code" data-turbo-transient>
  <link rel="assets" href="https://github.githubassets.com/">

    <meta name="google-site-verification" content="c1kuD-K2HIVF635lypcsWPoD4kilo5-jA_wBFyT4uMY">
  <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
  <meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
  <meta name="google-site-verification" content="GXs5KoUUkNCoaAZn7wPN-t01Pywp9M3sEjnt_3_ZWPc">
  <meta name="google-site-verification" content="Apib7-x98H0j5cPqHWwSMm6dNU4GmODRoqxLiDzdx9I">

<meta name="octolytics-url" content="https://collector.github.com/github/collect" />

  <meta name="analytics-location" content="/&lt;user-name&gt;/&lt;gist-id&gt;" data-turbo-transient="true" />

  




    <meta name="octolytics-dimension-public" content="true" /><meta name="octolytics-dimension-gist_id" content="96184307" /><meta name="octolytics-dimension-gist_name" content="f1bb645a4f715cb499150c5a14d82b44" /><meta name="octolytics-dimension-anonymous" content="false" /><meta name="octolytics-dimension-owner_id" content="2851492" /><meta name="octolytics-dimension-owner_login" content="Neo23x0" /><meta name="octolytics-dimension-forked" content="false" />


    <meta name="user-login" content="">

  

    <meta name="viewport" content="width=device-width">
    
      <meta name="description" content="IDDQD - Godmode YARA Rule. GitHub Gist: instantly share code, notes, and snippets.">
      <link rel="search" type="application/opensearchdescription+xml" href="/opensearch-gist.xml" title="Gist">
    <link rel="fluid-icon" href="https://gist.github.com/fluidicon.png" title="GitHub">
    <meta property="fb:app_id" content="1401488693436528">
    <meta name="apple-itunes-app" content="app-id=1477376905" />
      <meta name="twitter:image:src" content="https://github.githubassets.com/images/modules/gists/gist-og-image.png" /><meta name="twitter:site" content="@github" /><meta name="twitter:card" content="summary_large_image" /><meta name="twitter:title" content="IDDQD - Godmode YARA Rule" /><meta name="twitter:description" content="IDDQD - Godmode YARA Rule. GitHub Gist: instantly share code, notes, and snippets." />
      <meta property="og:image" content="https://github.githubassets.com/images/modules/gists/gist-og-image.png" /><meta property="og:image:alt" content="IDDQD - Godmode YARA Rule. GitHub Gist: instantly share code, notes, and snippets." /><meta property="og:site_name" content="Gist" /><meta property="og:type" content="article" /><meta property="og:title" content="IDDQD - Godmode YARA Rule" /><meta property="og:url" content="https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44" /><meta property="og:description" content="IDDQD - Godmode YARA Rule. GitHub Gist: instantly share code, notes, and snippets." /><meta property="article:author" content="262588213843476" /><meta property="article:publisher" content="262588213843476" />
      



        <meta name="hostname" content="gist.github.com">



        <meta name="expected-hostname" content="gist.github.com">

    <meta name="enabled-features" content="TURBO_EXPERIMENT_RISKY,IMAGE_METRIC_TRACKING,GEOJSON_AZURE_MAPS">


  <meta http-equiv="x-pjax-version" content="3807c417764ef84ea7031968fd961b802b0cb95a11ee99ccfca958ed7b1d7914" data-turbo-track="reload">
  <meta http-equiv="x-pjax-csp-version" content="0db263f9a873141d8256f783c35f244c06d490aacc3b680f99794dd8fd59fb59" data-turbo-track="reload">
  <meta http-equiv="x-pjax-css-version" content="e8aa01474d0c779a222d3c578e207f1c2a3df88c4896c35f0c67bac5015a1869" data-turbo-track="reload">
  <meta http-equiv="x-pjax-js-version" content="f70ea5b9052274af7e78261f6d80aa2ae693009e3f178b2df9ff5f579348cca6" data-turbo-track="reload">

  <meta name="turbo-cache-control" content="no-preview" data-turbo-transient="">

      <link href="/Neo23x0.atom" rel="alternate" title="atom" type="application/atom+xml">


  <link crossorigin="anonymous" media="all" rel="stylesheet" href="https://github.githubassets.com/assets/gist-1833b2f32fb6.css" />



  <meta name="turbo-body-classes" content="logged-out env-production page-responsive">


  <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">

  <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">

  <meta name="browser-optimizely-client-errors-url" content="https://api.github.com/_private/browser/optimizely_client/errors">

  <link rel="mask-icon" href="https://github.githubassets.com/pinned-octocat.svg" color="#000000">
  <link rel="alternate icon" class="js-site-favicon" type="image/png" href="https://github.githubassets.com/favicons/favicon.png">
  <link rel="icon" class="js-site-favicon" type="image/svg+xml" href="https://github.githubassets.com/favicons/favicon.svg">

<meta name="theme-color" content="#1e2327">
<meta name="color-scheme" content="light dark" />



  </head>

  <body class="logged-out env-production page-responsive" style="word-wrap: break-word;">
    <div data-turbo-body class="logged-out env-production page-responsive" style="word-wrap: break-word;">
      


    <div class="position-relative js-header-wrapper ">
      <a href="#start-of-content" class="px-2 py-4 color-bg-accent-emphasis color-fg-on-emphasis show-on-focus js-skip-to-content">Skip to content</a>
      <span data-view-component="true" class="progress-pjax-loader Progress position-fixed width-full">
    <span style="width: 0%;" data-view-component="true" class="Progress-item progress-pjax-loader-bar left-0 top-0 color-bg-accent-emphasis"></span>
</span>      
      


          <div class="Header js-details-container Details flex-wrap flex-md-nowrap p-responsive" role="banner" >
  <div class="Header-item d-none d-md-flex">
    <a class="Header-link" data-hotkey="g d" aria-label="Gist Homepage " href="/">
  <svg aria-hidden="true" height="24" viewBox="0 0 16 16" version="1.1" width="24" data-view-component="true" class="octicon octicon-mark-github v-align-middle d-inline-block d-md-none">
    <path d="M8 0c4.42 0 8 3.58 8 8a8.013 8.013 0 0 1-5.45 7.59c-.4.08-.55-.17-.55-.38 0-.27.01-1.13.01-2.2 0-.75-.25-1.23-.54-1.48 1.78-.2 3.65-.88 3.65-3.95 0-.88-.31-1.59-.82-2.15.08-.2.36-1.02-.08-2.12 0 0-.67-.22-2.2.82-.64-.18-1.32-.27-2-.27-.68 0-1.36.09-2 .27-1.53-1.03-2.2-.82-2.2-.82-.44 1.1-.16 1.92-.08 2.12-.51.56-.82 1.28-.82 2.15 0 3.06 1.86 3.75 3.64 3.95-.23.2-.44.55-.51 1.07-.46.21-1.61.55-2.33-.66-.15-.24-.6-.83-1.23-.82-.67.01-.27.38.01.53.34.19.73.9.82 1.13.16.45.68 1.31 2.69.94 0 .67.01 1.3.01 1.49 0 .21-.15.45-.55.38A7.995 7.995 0 0 1 0 8c0-4.42 3.58-8 8-8Z"></path>
</svg>
  <svg aria-hidden="true" height="24" viewBox="0 0 45 16" version="1.1" width="67" data-view-component="true" class="octicon octicon-logo-github v-align-middle d-none d-md-inline-block">
    <path d="M8.81 7.35v5.74c0 .04-.01.11-.06.13 0 0-1.25.89-3.31.89-2.49 0-5.44-.78-5.44-5.92S2.58 1.99 5.1 2c2.18 0 3.06.49 3.2.58.04.05.06.09.06.14L7.94 4.5c0 .09-.09.2-.2.17-.36-.11-.9-.33-2.17-.33-1.47 0-3.05.42-3.05 3.73s1.5 3.7 2.58 3.7c.92 0 1.25-.11 1.25-.11v-2.3H4.88c-.11 0-.19-.08-.19-.17V7.35c0-.09.08-.17.19-.17h3.74c.11 0 .19.08.19.17Zm35.85 2.33c0 3.43-1.11 4.41-3.05 4.41-1.64 0-2.52-.83-2.52-.83s-.04.46-.09.52c-.03.06-.08.08-.14.08h-1.48c-.1 0-.19-.08-.19-.17l.02-11.11c0-.09.08-.17.17-.17h2.13c.09 0 .17.08.17.17v3.77s.82-.53 2.02-.53l-.01-.02c1.2 0 2.97.45 2.97 3.88ZM27.68 2.43c.09 0 .17.08.17.17v11.11c0 .09-.08.17-.17.17h-2.13c-.09 0-.17-.08-.17-.17l.02-4.75h-3.31v4.75c0 .09-.08.17-.17.17h-2.13c-.08 0-.17-.08-.17-.17V2.6c0-.09.08-.17.17-.17h2.13c.09 0 .17.08.17.17v4.09h3.31V2.6c0-.09.08-.17.17-.17Zm8.26 3.64c.11 0 .19.08.19.17l-.02 7.47c0 .09-.06.17-.17.17H34.6c-.07 0-.14-.04-.16-.09-.03-.06-.08-.45-.08-.45s-1.13.77-2.52.77c-1.69 0-2.92-.55-2.92-2.75V6.25c0-.09.08-.17.17-.17h2.14c.09 0 .17.08.17.17V11c0 .75.22 1.09.97 1.09s1.3-.39 1.3-.39V6.26c0-.11.06-.19.17-.19Zm-17.406 5.971h.005a.177.177 0 0 1 .141.179v1.5c0 .07-.03.14-.09.16-.1.05-.74.22-1.27.22-1.16 0-2.86-.25-2.86-2.69V8.13h-1.11c-.09 0-.17-.08-.17-.19V6.58c0-.08.05-.15.13-.17.07-.01 1.16-.28 1.16-.28V3.96c0-.08.05-.13.14-.13h2.16c.09 0 .14.05.14.13v2.11h1.59c.08 0 .16.08.16.17v1.7c0 .11-.07.19-.16.19h-1.59v3.131c0 .47.27.83 1.05.83.247 0 .481-.049.574-.05ZM12.24 6.06c.09 0 .17.08.17.17v7.37c0 .18-.05.27-.25.27h-1.92c-.17 0-.3-.07-.3-.27V6.26c0-.11.08-.2.17-.2Zm29.99 3.78c0-1.81-.73-2.05-1.5-1.97-.6.04-1.08.34-1.08.34v3.52s.49.34 1.22.36c1.03.03 1.36-.34 1.36-2.25ZM11.19 2.68c.75 0 1.36.61 1.36 1.38 0 .77-.61 1.38-1.36 1.38-.77 0-1.38-.61-1.38-1.38 0-.77.61-1.38 1.38-1.38Zm7.34 9.35v.001l.01.01h-.001l-.005-.001v.001c-.009-.001-.015-.011-.024-.011Z"></path>
</svg>
  <svg aria-hidden="true" height="24" viewBox="0 0 25 16" version="1.1" width="37" data-view-component="true" class="octicon octicon-logo-gist v-align-middle d-none d-md-inline-block">
    <path d="M4.7 8.73v-1h3.52v5.69c-.78.37-1.95.64-3.59.64C1.11 14.06 0 11.37 0 8.03 0 4.69 1.13 2 4.63 2c1.62 0 2.64.33 3.28.66v1.05c-1.22-.5-2-.73-3.28-.73-2.57 0-3.48 2.21-3.48 5.06 0 2.85.91 5.05 3.47 5.05.89 0 1.98-.07 2.53-.34V8.73Zm10.98.69h.03c2.22.2 2.75.95 2.75 2.23 0 1.21-.76 2.41-3.14 2.41-.75 0-1.83-.19-2.33-.39v-.94c.47.17 1.22.36 2.33.36 1.62 0 2.06-.69 2.06-1.42 0-.71-.22-1.21-1.77-1.34-2.26-.2-2.73-1-2.73-2.08 0-1.11.72-2.31 2.92-2.31.73 0 1.56.09 2.25.39v.94c-.61-.2-1.22-.36-2.27-.36-1.55 0-1.88.57-1.88 1.34 0 .69.28 1.04 1.78 1.17Zm8.58-3.33v.85h-2.42v4.87c0 .95.53 1.34 1.5 1.34.2 0 .42 0 .61-.03v.89c-.17.03-.5.05-.69.05-1.31 0-2.5-.6-2.5-2.13v-5H19.2v-.48l1.56-.44V3.9l1.08-.31v2.5h2.42Zm-13.17-.03v6.41c0 .54.19.7.67.7v.89c-1.14 0-1.72-.47-1.72-1.72V6.06h1.05Zm.25-2.33c0 .44-.34.78-.78.78a.76.76 0 0 1-.77-.78c0-.44.32-.78.77-.78s.78.34.78.78Z"></path>
</svg>
</a>
  </div>

  <div class="Header-item d-md-none">
      <button aria-label="Toggle navigation" aria-expanded="false" type="button" data-view-component="true" class="Header-link js-details-target btn-link">    <svg aria-hidden="true" height="24" viewBox="0 0 16 16" version="1.1" width="24" data-view-component="true" class="octicon octicon-three-bars">
    <path d="M1 2.75A.75.75 0 0 1 1.75 2h12.5a.75.75 0 0 1 0 1.5H1.75A.75.75 0 0 1 1 2.75Zm0 5A.75.75 0 0 1 1.75 7h12.5a.75.75 0 0 1 0 1.5H1.75A.75.75 0 0 1 1 7.75ZM1.75 12h12.5a.75.75 0 0 1 0 1.5H1.75a.75.75 0 0 1 0-1.5Z"></path>
</svg>
</button>  </div>

  <div class="Header-item Header-item--full js-site-search flex-column flex-md-row width-full flex-order-2 flex-md-order-none mr-0 mr-md-3 mt-3 mt-md-0 Details-content--hidden-not-important d-md-flex">
      <div class="header-search flex-self-stretch flex-md-self-auto mr-0 mr-md-3 mb-3 mb-md-0">
  <!-- '"` --><!-- </textarea></xmp> --></option></form><form class="position-relative js-quicksearch-form" role="search" aria-label="Site" data-turbo="false" action="/search" accept-charset="UTF-8" method="get">
    <div class="header-search-wrapper form-control input-sm js-chromeless-input-container">
      <input type="text"
        class="form-control input-sm js-site-search-focus header-search-input"
        data-hotkey="s,/"
        name="q"
        aria-label="Search"
        placeholder="Search…"
        autocorrect="off"
        autocomplete="off"
        autocapitalize="off">
    </div>

</form></div>


    <nav aria-label="Global" class="d-flex flex-column flex-md-row flex-self-stretch flex-md-self-auto">
  <a class="Header-link mr-0 mr-md-3 py-2 py-md-0 border-top border-md-top-0 border-white-fade" data-ga-click="Header, go to all gists, text:all gists" href="/discover">All gists</a>

  <a class="Header-link mr-0 mr-md-3 py-2 py-md-0 border-top border-md-top-0 border-white-fade" data-ga-click="Header, go to GitHub, text:Back to GitHub" href="https://github.com">Back to GitHub</a>

    <a class="Header-link d-block d-md-none mr-0 mr-md-3 py-2 py-md-0 border-top border-md-top-0 border-white-fade" data-ga-click="Header, sign in" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist header&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="91123a7a3cb3c27a87994d05aa28d087d001fe36db1c3625a8cf6d0d0d3a77e7" href="https://gist.github.com/auth/github?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44">
      Sign in
</a>
      <a class="Header-link d-block d-md-none mr-0 mr-md-3 py-2 py-md-0 border-top border-md-top-0 border-white-fade" data-ga-click="Header, sign up" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist header&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="1bbd557b0656b23e53fa74bc96758b57cdb15e3083031d37043ada07ee9997c5" href="/join?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44&amp;source=header-gist">
        Sign up
</a></nav>

  </div>

  <div class="Header-item Header-item--full flex-justify-center d-md-none position-relative">
    <a class="Header-link" data-hotkey="g d" aria-label="Gist Homepage " href="/">
  <svg aria-hidden="true" height="24" viewBox="0 0 16 16" version="1.1" width="24" data-view-component="true" class="octicon octicon-mark-github v-align-middle d-inline-block d-md-none">
    <path d="M8 0c4.42 0 8 3.58 8 8a8.013 8.013 0 0 1-5.45 7.59c-.4.08-.55-.17-.55-.38 0-.27.01-1.13.01-2.2 0-.75-.25-1.23-.54-1.48 1.78-.2 3.65-.88 3.65-3.95 0-.88-.31-1.59-.82-2.15.08-.2.36-1.02-.08-2.12 0 0-.67-.22-2.2.82-.64-.18-1.32-.27-2-.27-.68 0-1.36.09-2 .27-1.53-1.03-2.2-.82-2.2-.82-.44 1.1-.16 1.92-.08 2.12-.51.56-.82 1.28-.82 2.15 0 3.06 1.86 3.75 3.64 3.95-.23.2-.44.55-.51 1.07-.46.21-1.61.55-2.33-.66-.15-.24-.6-.83-1.23-.82-.67.01-.27.38.01.53.34.19.73.9.82 1.13.16.45.68 1.31 2.69.94 0 .67.01 1.3.01 1.49 0 .21-.15.45-.55.38A7.995 7.995 0 0 1 0 8c0-4.42 3.58-8 8-8Z"></path>
</svg>
  <svg aria-hidden="true" height="24" viewBox="0 0 45 16" version="1.1" width="67" data-view-component="true" class="octicon octicon-logo-github v-align-middle d-none d-md-inline-block">
    <path d="M8.81 7.35v5.74c0 .04-.01.11-.06.13 0 0-1.25.89-3.31.89-2.49 0-5.44-.78-5.44-5.92S2.58 1.99 5.1 2c2.18 0 3.06.49 3.2.58.04.05.06.09.06.14L7.94 4.5c0 .09-.09.2-.2.17-.36-.11-.9-.33-2.17-.33-1.47 0-3.05.42-3.05 3.73s1.5 3.7 2.58 3.7c.92 0 1.25-.11 1.25-.11v-2.3H4.88c-.11 0-.19-.08-.19-.17V7.35c0-.09.08-.17.19-.17h3.74c.11 0 .19.08.19.17Zm35.85 2.33c0 3.43-1.11 4.41-3.05 4.41-1.64 0-2.52-.83-2.52-.83s-.04.46-.09.52c-.03.06-.08.08-.14.08h-1.48c-.1 0-.19-.08-.19-.17l.02-11.11c0-.09.08-.17.17-.17h2.13c.09 0 .17.08.17.17v3.77s.82-.53 2.02-.53l-.01-.02c1.2 0 2.97.45 2.97 3.88ZM27.68 2.43c.09 0 .17.08.17.17v11.11c0 .09-.08.17-.17.17h-2.13c-.09 0-.17-.08-.17-.17l.02-4.75h-3.31v4.75c0 .09-.08.17-.17.17h-2.13c-.08 0-.17-.08-.17-.17V2.6c0-.09.08-.17.17-.17h2.13c.09 0 .17.08.17.17v4.09h3.31V2.6c0-.09.08-.17.17-.17Zm8.26 3.64c.11 0 .19.08.19.17l-.02 7.47c0 .09-.06.17-.17.17H34.6c-.07 0-.14-.04-.16-.09-.03-.06-.08-.45-.08-.45s-1.13.77-2.52.77c-1.69 0-2.92-.55-2.92-2.75V6.25c0-.09.08-.17.17-.17h2.14c.09 0 .17.08.17.17V11c0 .75.22 1.09.97 1.09s1.3-.39 1.3-.39V6.26c0-.11.06-.19.17-.19Zm-17.406 5.971h.005a.177.177 0 0 1 .141.179v1.5c0 .07-.03.14-.09.16-.1.05-.74.22-1.27.22-1.16 0-2.86-.25-2.86-2.69V8.13h-1.11c-.09 0-.17-.08-.17-.19V6.58c0-.08.05-.15.13-.17.07-.01 1.16-.28 1.16-.28V3.96c0-.08.05-.13.14-.13h2.16c.09 0 .14.05.14.13v2.11h1.59c.08 0 .16.08.16.17v1.7c0 .11-.07.19-.16.19h-1.59v3.131c0 .47.27.83 1.05.83.247 0 .481-.049.574-.05ZM12.24 6.06c.09 0 .17.08.17.17v7.37c0 .18-.05.27-.25.27h-1.92c-.17 0-.3-.07-.3-.27V6.26c0-.11.08-.2.17-.2Zm29.99 3.78c0-1.81-.73-2.05-1.5-1.97-.6.04-1.08.34-1.08.34v3.52s.49.34 1.22.36c1.03.03 1.36-.34 1.36-2.25ZM11.19 2.68c.75 0 1.36.61 1.36 1.38 0 .77-.61 1.38-1.36 1.38-.77 0-1.38-.61-1.38-1.38 0-.77.61-1.38 1.38-1.38Zm7.34 9.35v.001l.01.01h-.001l-.005-.001v.001c-.009-.001-.015-.011-.024-.011Z"></path>
</svg>
  <svg aria-hidden="true" height="24" viewBox="0 0 25 16" version="1.1" width="37" data-view-component="true" class="octicon octicon-logo-gist v-align-middle d-none d-md-inline-block">
    <path d="M4.7 8.73v-1h3.52v5.69c-.78.37-1.95.64-3.59.64C1.11 14.06 0 11.37 0 8.03 0 4.69 1.13 2 4.63 2c1.62 0 2.64.33 3.28.66v1.05c-1.22-.5-2-.73-3.28-.73-2.57 0-3.48 2.21-3.48 5.06 0 2.85.91 5.05 3.47 5.05.89 0 1.98-.07 2.53-.34V8.73Zm10.98.69h.03c2.22.2 2.75.95 2.75 2.23 0 1.21-.76 2.41-3.14 2.41-.75 0-1.83-.19-2.33-.39v-.94c.47.17 1.22.36 2.33.36 1.62 0 2.06-.69 2.06-1.42 0-.71-.22-1.21-1.77-1.34-2.26-.2-2.73-1-2.73-2.08 0-1.11.72-2.31 2.92-2.31.73 0 1.56.09 2.25.39v.94c-.61-.2-1.22-.36-2.27-.36-1.55 0-1.88.57-1.88 1.34 0 .69.28 1.04 1.78 1.17Zm8.58-3.33v.85h-2.42v4.87c0 .95.53 1.34 1.5 1.34.2 0 .42 0 .61-.03v.89c-.17.03-.5.05-.69.05-1.31 0-2.5-.6-2.5-2.13v-5H19.2v-.48l1.56-.44V3.9l1.08-.31v2.5h2.42Zm-13.17-.03v6.41c0 .54.19.7.67.7v.89c-1.14 0-1.72-.47-1.72-1.72V6.06h1.05Zm.25-2.33c0 .44-.34.78-.78.78a.76.76 0 0 1-.77-.78c0-.44.32-.78.77-.78s.78.34.78.78Z"></path>
</svg>
</a>
  </div>

    <div class="Header-item f4 mr-0" role="navigation">
      <a class="Header-link no-underline mr-3" data-ga-click="Header, sign in" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist header&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="91123a7a3cb3c27a87994d05aa28d087d001fe36db1c3625a8cf6d0d0d3a77e7" href="https://gist.github.com/auth/github?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44">
        Sign&nbsp;in
</a>        <a class="Header-link d-inline-block no-underline border color-border-default rounded px-2 py-1" data-ga-click="Header, sign up" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist header&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="1bbd557b0656b23e53fa74bc96758b57cdb15e3083031d37043ada07ee9997c5" href="/join?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44&amp;source=header-gist">
          Sign&nbsp;up
</a>    </div>
</div>



    </div>

  <div id="start-of-content" class="show-on-focus"></div>







    <div id="js-flash-container" data-turbo-replace>





  <template class="js-flash-template">
    
<div class="flash flash-full   {{ className }}">
  <div class="px-2" >
    <button autofocus class="flash-close js-flash-close" type="button" aria-label="Dismiss this message">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path d="M3.72 3.72a.75.75 0 0 1 1.06 0L8 6.94l3.22-3.22a.749.749 0 0 1 1.275.326.749.749 0 0 1-.215.734L9.06 8l3.22 3.22a.749.749 0 0 1-.326 1.275.749.749 0 0 1-.734-.215L8 9.06l-3.22 3.22a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042L6.94 8 3.72 4.78a.75.75 0 0 1 0-1.06Z"></path>
</svg>
    </button>
    <div aria-atomic="true" role="alert" class="js-flash-alert">
      
      <div>{{ message }}</div>

    </div>
  </div>
</div>
  </template>
</div>


    
    <include-fragment class="js-notification-shelf-include-fragment" data-base-src="https://github.com/notifications/beta/shelf"></include-fragment>






  <div
    class="application-main "
    data-commit-hovercards-enabled
    data-discussion-hovercards-enabled
    data-issue-and-pr-hovercards-enabled
  >
        <div itemscope itemtype="http://schema.org/Code">
    <main id="gist-pjax-container">
      


  <div class="gist-detail-intro gist-banner pb-3">
    <div class="text-center container-lg px-3">
      <p class="lead">
        Instantly share code, notes, and snippets.
      </p>
    </div>
  </div>


<div class="gisthead pagehead pb-0 pt-3 mb-4">
  <div class="px-0">
    
  

<div class="mb-3 d-flex px-3 px-md-3 px-lg-5">
  <div class="flex-auto min-width-0 width-fit mr-3">
    <div class="d-flex">
      <div class="d-none d-md-block">
        <a class="mr-2 flex-shrink-0" data-hovercard-type="user" data-hovercard-url="/users/Neo23x0/hovercard" data-octo-click="hovercard-link-click" data-octo-dimensions="link_type:self" href="/Neo23x0"><img class="avatar avatar-user" src="https://avatars.githubusercontent.com/u/2851492?s=64&amp;v=4" width="32" height="32" alt="@Neo23x0" /></a>
      </div>
      <div class="d-flex flex-column width-full">
          <div class="d-flex flex-row width-full">
            <h1 class="wb-break-word f3 text-normal mb-md-0 mb-1">
              <span class="author"><a data-hovercard-type="user" data-hovercard-url="/users/Neo23x0/hovercard" data-octo-click="hovercard-link-click" data-octo-dimensions="link_type:self" href="/Neo23x0">Neo23x0</a></span><!--
                  --><span class="mx-1 color-fg-muted">/</span><!--
                  --><strong itemprop="name" class="css-truncate-target mr-1" style="max-width: 410px"><a href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44">iddqd.yar</a></strong>
            </h1>

        </div>

        <div class="note m-0">
          Last active
          <relative-time tense="past" datetime="2022-11-17T16:41:14Z" data-view-component="true">November 17, 2022 16:41</relative-time>
        </div>
      </div>
    </div>
  </div>

  <ul class="d-md-flex d-none pagehead-actions float-none">



      <li>
          <a class="btn btn-sm btn-with-count tooltipped tooltipped-n" aria-label="You must be signed in to star a gist" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist star button&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="d54631d0ce19d616a10bc8258c2fb37726101091a843288466499215d9d7276a" href="/login?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-star">
    <path d="M8 .25a.75.75 0 0 1 .673.418l1.882 3.815 4.21.612a.75.75 0 0 1 .416 1.279l-3.046 2.97.719 4.192a.751.751 0 0 1-1.088.791L8 12.347l-3.766 1.98a.75.75 0 0 1-1.088-.79l.72-4.194L.818 6.374a.75.75 0 0 1 .416-1.28l4.21-.611L7.327.668A.75.75 0 0 1 8 .25Zm0 2.445L6.615 5.5a.75.75 0 0 1-.564.41l-3.097.45 2.24 2.184a.75.75 0 0 1 .216.664l-.528 3.084 2.769-1.456a.75.75 0 0 1 .698 0l2.77 1.456-.53-3.084a.75.75 0 0 1 .216-.664l2.24-2.183-3.096-.45a.75.75 0 0 1-.564-.41L8 2.694Z"></path>
</svg>
    Star
</a>
    <a class="social-count" aria-label="82 users starred this gist" href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/stargazers">
      82
</a>
      </li>

        <li>
            <a class="btn btn-sm btn-with-count tooltipped tooltipped-n" aria-label="You must be signed in to fork a gist" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist fork button&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="8c09d86bc88be3544758cba08cc43c83556590b99c6be5a3f723e48c02e5be48" href="/login?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-repo-forked">
    <path d="M5 5.372v.878c0 .414.336.75.75.75h4.5a.75.75 0 0 0 .75-.75v-.878a2.25 2.25 0 1 1 1.5 0v.878a2.25 2.25 0 0 1-2.25 2.25h-1.5v2.128a2.251 2.251 0 1 1-1.5 0V8.5h-1.5A2.25 2.25 0 0 1 3.5 6.25v-.878a2.25 2.25 0 1 1 1.5 0ZM5 3.25a.75.75 0 1 0-1.5 0 .75.75 0 0 0 1.5 0Zm6.75.75a.75.75 0 1 0 0-1.5.75.75 0 0 0 0 1.5Zm-3 8.75a.75.75 0 1 0-1.5 0 .75.75 0 0 0 1.5 0Z"></path>
</svg>
    Fork
</a>    <a class="social-count js-social-count"
      href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/forks"
      aria-label="22 users forked this gist">
      22
    </a>

        </li>
  </ul>
</div>

  <div class="d-block d-md-none px-3 px-md-3 px-lg-5 mb-3">
      <a class="btn btn-sm btn-block tooltipped tooltipped-n" aria-label="You must be signed in to star a gist" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;gist star button&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="d54631d0ce19d616a10bc8258c2fb37726101091a843288466499215d9d7276a" href="/login?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-star">
    <path d="M8 .25a.75.75 0 0 1 .673.418l1.882 3.815 4.21.612a.75.75 0 0 1 .416 1.279l-3.046 2.97.719 4.192a.751.751 0 0 1-1.088.791L8 12.347l-3.766 1.98a.75.75 0 0 1-1.088-.79l.72-4.194L.818 6.374a.75.75 0 0 1 .416-1.28l4.21-.611L7.327.668A.75.75 0 0 1 8 .25Zm0 2.445L6.615 5.5a.75.75 0 0 1-.564.41l-3.097.45 2.24 2.184a.75.75 0 0 1 .216.664l-.528 3.084 2.769-1.456a.75.75 0 0 1 .698 0l2.77 1.456-.53-3.084a.75.75 0 0 1 .216-.664l2.24-2.183-3.096-.45a.75.75 0 0 1-.564-.41L8 2.694Z"></path>
</svg>
    Star
</a>

  </div>

<div class="d-flex flex-md-row flex-column px-0 pr-md-3 px-lg-5">
  <div class="flex-md-order-1 flex-order-2 flex-auto">
    <nav class="UnderlineNav box-shadow-none px-3 px-lg-0 "
     aria-label="Gist"
     data-pjax="#gist-pjax-container">

  <div class="UnderlineNav-body">
    <a class="js-selected-navigation-item selected UnderlineNav-item" data-pjax="true" data-hotkey="g c" aria-current="page" data-selected-links="gist_code /Neo23x0/f1bb645a4f715cb499150c5a14d82b44" href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-code UnderlineNav-octicon">
    <path d="m11.28 3.22 4.25 4.25a.75.75 0 0 1 0 1.06l-4.25 4.25a.749.749 0 0 1-1.275-.326.749.749 0 0 1 .215-.734L13.94 8l-3.72-3.72a.749.749 0 0 1 .326-1.275.749.749 0 0 1 .734.215Zm-6.56 0a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042L2.06 8l3.72 3.72a.749.749 0 0 1-.326 1.275.749.749 0 0 1-.734-.215L.47 8.53a.75.75 0 0 1 0-1.06Z"></path>
</svg>
      Code
</a>
      <a class="js-selected-navigation-item UnderlineNav-item" data-pjax="true" data-hotkey="g r" data-selected-links="gist_revisions /Neo23x0/f1bb645a4f715cb499150c5a14d82b44/revisions" href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/revisions">
        <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-git-commit UnderlineNav-octicon">
    <path d="M11.93 8.5a4.002 4.002 0 0 1-7.86 0H.75a.75.75 0 0 1 0-1.5h3.32a4.002 4.002 0 0 1 7.86 0h3.32a.75.75 0 0 1 0 1.5Zm-1.43-.75a2.5 2.5 0 1 0-5 0 2.5 2.5 0 0 0 5 0Z"></path>
</svg>
        Revisions
        <span title="23" data-view-component="true" class="Counter hx_reponav_item_counter">23</span>
</a>
      <a class="js-selected-navigation-item UnderlineNav-item" data-pjax="true" data-hotkey="g s" data-selected-links="gist_stars /Neo23x0/f1bb645a4f715cb499150c5a14d82b44/stargazers" href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/stargazers">
        <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-star UnderlineNav-octicon">
    <path d="M8 .25a.75.75 0 0 1 .673.418l1.882 3.815 4.21.612a.75.75 0 0 1 .416 1.279l-3.046 2.97.719 4.192a.751.751 0 0 1-1.088.791L8 12.347l-3.766 1.98a.75.75 0 0 1-1.088-.79l.72-4.194L.818 6.374a.75.75 0 0 1 .416-1.28l4.21-.611L7.327.668A.75.75 0 0 1 8 .25Zm0 2.445L6.615 5.5a.75.75 0 0 1-.564.41l-3.097.45 2.24 2.184a.75.75 0 0 1 .216.664l-.528 3.084 2.769-1.456a.75.75 0 0 1 .698 0l2.77 1.456-.53-3.084a.75.75 0 0 1 .216-.664l2.24-2.183-3.096-.45a.75.75 0 0 1-.564-.41L8 2.694Z"></path>
</svg>
        Stars
        <span title="82" data-view-component="true" class="Counter hx_reponav_item_counter">82</span>
</a>
      <a class="js-selected-navigation-item UnderlineNav-item" data-pjax="true" data-hotkey="g f" data-selected-links="gist_forks /Neo23x0/f1bb645a4f715cb499150c5a14d82b44/forks" href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/forks">
        <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-git-branch UnderlineNav-octicon">
    <path d="M9.5 3.25a2.25 2.25 0 1 1 3 2.122V6A2.5 2.5 0 0 1 10 8.5H6a1 1 0 0 0-1 1v1.128a2.251 2.251 0 1 1-1.5 0V5.372a2.25 2.25 0 1 1 1.5 0v1.836A2.493 2.493 0 0 1 6 7h4a1 1 0 0 0 1-1v-.628A2.25 2.25 0 0 1 9.5 3.25Zm-6 0a.75.75 0 1 0 1.5 0 .75.75 0 0 0-1.5 0Zm8.25-.75a.75.75 0 1 0 0 1.5.75.75 0 0 0 0-1.5ZM4.25 12a.75.75 0 1 0 0 1.5.75.75 0 0 0 0-1.5Z"></path>
</svg>
        Forks
        <span title="22" data-view-component="true" class="Counter hx_reponav_item_counter">22</span>
</a>  </div>
</nav>

  </div>

  <div class="d-md-flex d-none flex-items-center flex-md-order-2 flex-order-1 file-navigation-options" data-multiple>

    <div class="d-lg-table d-none">
      <div class="file-navigation-option v-align-middle">

  <div class="d-md-flex d-none">
    <div class="input-group">
      <div class="input-group-button">
        <details class="details-reset details-overlay select-menu">
            <summary data-ga-click="Repository, clone Embed, location:repo overview" data-view-component="true" class="select-menu-button btn-sm btn">    <span data-menu-button>Embed</span>
</summary>          <details-menu
            class="select-menu-modal position-absolute"
            data-menu-input="gist-share-url"
            style="z-index: 99;" aria-label="Clone options">
            <div class="select-menu-header">
              <span class="select-menu-title">What would you like to do?</span>
            </div>
            <div class="select-menu-list">
                <button name="button" type="button" class="select-menu-item width-full" aria-checked="true" role="menuitemradio" value="&lt;script src=&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44.js&quot;&gt;&lt;/script&gt;" data-hydro-click="{&quot;event_type&quot;:&quot;clone_or_download.click&quot;,&quot;payload&quot;:{&quot;feature_clicked&quot;:&quot;EMBED&quot;,&quot;git_repository_type&quot;:&quot;GIST&quot;,&quot;gist_id&quot;:96184307,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="c63087f2f1b98ce3087113b0be7108c43eba4aae0cdd3a61d1e81bcd59040006">
                  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check select-menu-item-icon">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
                  <div class="select-menu-item-text">
                    <span class="select-menu-item-heading" data-menu-button-text>
                      
                      Embed
                    </span>
                      <span class="description">
                        Embed this gist in your website.
                      </span>
                  </div>
</button>                <button name="button" type="button" class="select-menu-item width-full" aria-checked="false" role="menuitemradio" value="https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44" data-hydro-click="{&quot;event_type&quot;:&quot;clone_or_download.click&quot;,&quot;payload&quot;:{&quot;feature_clicked&quot;:&quot;SHARE&quot;,&quot;git_repository_type&quot;:&quot;GIST&quot;,&quot;gist_id&quot;:96184307,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="f4fcfdf5d6d4d135cf5c80424aa749386038880998af85d3287f7462f9e8565c">
                  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check select-menu-item-icon">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
                  <div class="select-menu-item-text">
                    <span class="select-menu-item-heading" data-menu-button-text>
                      
                      Share
                    </span>
                      <span class="description">
                        Copy sharable link for this gist.
                      </span>
                  </div>
</button>                <button name="button" type="button" class="select-menu-item width-full" aria-checked="false" role="menuitemradio" value="https://gist.github.com/f1bb645a4f715cb499150c5a14d82b44.git" data-hydro-click="{&quot;event_type&quot;:&quot;clone_or_download.click&quot;,&quot;payload&quot;:{&quot;feature_clicked&quot;:&quot;USE_HTTPS&quot;,&quot;git_repository_type&quot;:&quot;GIST&quot;,&quot;gist_id&quot;:96184307,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="55d9724be11858afca7b1306c306239dc7452bbbc1bf9952ae44419ba5a18a5c">
                  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check select-menu-item-icon">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
                  <div class="select-menu-item-text">
                    <span class="select-menu-item-heading" data-menu-button-text>
                      Clone via
                      HTTPS
                    </span>
                      <span class="description">
                        Clone with Git or checkout with SVN using the repository’s web address.
                      </span>
                  </div>
</button>            </div>
            <div class="select-menu-list">
              <a role="link" class="select-menu-item select-menu-action" href="https://docs.github.com/articles/which-remote-url-should-i-use" target="_blank">
                <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-question select-menu-item-icon">
    <path d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8Zm8-6.5a6.5 6.5 0 1 0 0 13 6.5 6.5 0 0 0 0-13ZM6.92 6.085h.001a.749.749 0 1 1-1.342-.67c.169-.339.436-.701.849-.977C6.845 4.16 7.369 4 8 4a2.756 2.756 0 0 1 1.637.525c.503.377.863.965.863 1.725 0 .448-.115.83-.329 1.15-.205.307-.47.513-.692.662-.109.072-.22.138-.313.195l-.006.004a6.24 6.24 0 0 0-.26.16.952.952 0 0 0-.276.245.75.75 0 0 1-1.248-.832c.184-.264.42-.489.692-.661.103-.067.207-.132.313-.195l.007-.004c.1-.061.182-.11.258-.161a.969.969 0 0 0 .277-.245C8.96 6.514 9 6.427 9 6.25a.612.612 0 0 0-.262-.525A1.27 1.27 0 0 0 8 5.5c-.369 0-.595.09-.74.187a1.01 1.01 0 0 0-.34.398ZM9 11a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path>
</svg>
                <div class="select-menu-item-text">
                  Learn more about clone URLs
                </div>
              </a>
            </div>
          </details-menu>
        </details>
      </div>

      <input
        id="gist-share-url"
        type="text"
        data-autoselect
        class="form-control input-monospace input-sm"
        value="&lt;script src=&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44.js&quot;&gt;&lt;/script&gt;"
        aria-label="Clone this repository at &lt;script src=&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44.js&quot;&gt;&lt;/script&gt;"
        readonly>

      <div class="input-group-button">
        <clipboard-copy for="gist-share-url" aria-label="Copy to clipboard" class="btn btn-sm zeroclipboard-button" data-hydro-click="{&quot;event_type&quot;:&quot;clone_or_download.click&quot;,&quot;payload&quot;:{&quot;feature_clicked&quot;:&quot;COPY_URL&quot;,&quot;git_repository_type&quot;:&quot;GIST&quot;,&quot;gist_id&quot;:96184307,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="7a87c71490e3ff1f1fe25a873ae3aad5f228baf826fbb2c9aca87f52beaa9342"><svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg></clipboard-copy>
      </div>
    </div>
  </div>
</div>

    </div>

    <div class="ml-2 file-navigation-option">
    <a class="btn btn-sm tooltipped tooltipped-s tooltipped-multiline js-remove-unless-platform" data-platforms="windows,mac" aria-label="Save Neo23x0/f1bb645a4f715cb499150c5a14d82b44 to your computer and use it in GitHub Desktop." data-hydro-click="{&quot;event_type&quot;:&quot;clone_or_download.click&quot;,&quot;payload&quot;:{&quot;feature_clicked&quot;:&quot;OPEN_IN_DESKTOP&quot;,&quot;git_repository_type&quot;:&quot;GIST&quot;,&quot;gist_id&quot;:96184307,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="bc5d4486677be37053de1a7db95fafdc0ca4f72b1c35e9c853146aa21687b1a2" href="https://desktop.github.com"><svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-desktop-download">
    <path d="m4.927 5.427 2.896 2.896a.25.25 0 0 0 .354 0l2.896-2.896A.25.25 0 0 0 10.896 5H8.75V.75a.75.75 0 1 0-1.5 0V5H5.104a.25.25 0 0 0-.177.427Z"></path><path d="M1.573 2.573a.25.25 0 0 0-.073.177v7.5a.25.25 0 0 0 .25.25h12.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25h-3a.75.75 0 1 1 0-1.5h3A1.75 1.75 0 0 1 16 2.75v7.5A1.75 1.75 0 0 1 14.25 12h-3.727c.099 1.041.52 1.872 1.292 2.757A.75.75 0 0 1 11.25 16h-6.5a.75.75 0 0 1-.565-1.243c.772-.885 1.192-1.716 1.292-2.757H1.75A1.75 1.75 0 0 1 0 10.25v-7.5A1.75 1.75 0 0 1 1.75 1h3a.75.75 0 0 1 0 1.5h-3a.25.25 0 0 0-.177.073ZM6.982 12a5.72 5.72 0 0 1-.765 2.5h3.566a5.72 5.72 0 0 1-.765-2.5H6.982Z"></path>
</svg></a>
</div>


    <div class="ml-2">
      <a class="btn btn-sm" rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;clone_or_download.click&quot;,&quot;payload&quot;:{&quot;feature_clicked&quot;:&quot;DOWNLOAD_ZIP&quot;,&quot;git_repository_type&quot;:&quot;GIST&quot;,&quot;gist_id&quot;:96184307,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="5a0ac00b899faddff6c84e407436548480f11e65221d0a9372f67ac9a51203ae" data-ga-click="Gist, download zip, location:gist overview" href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/archive/d621fcfd496d03dca78f9ff390cad88684139d64.zip">Download ZIP</a>
    </div>
  </div>
</div>


  </div>
</div>

<div class="container-lg px-3">
  <div class="repository-content gist-content" >
    
  <div>
      <div itemprop="about">
    IDDQD - Godmode YARA Rule
  </div>

        <div class="js-gist-file-update-container js-task-list-container file-box">
  <div id="file-iddqd-yar" class="file my-2">
      <div class="file-header d-flex flex-md-items-center flex-items-start">
        <div class="file-actions flex-order-2 pt-0">

            <a href="/Neo23x0/f1bb645a4f715cb499150c5a14d82b44/raw/d621fcfd496d03dca78f9ff390cad88684139d64/iddqd.yar" data-view-component="true" class="Button--secondary Button--small Button">    <span class="Button-content">
      <span class="Button-label">Raw</span>
    </span>
</a>  

        </div>
        <div class="file-info pr-4 d-flex flex-md-items-center flex-items-start flex-order-1 flex-auto">
          <span class="mr-1">
            <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-code-square color-fg-muted">
    <path d="M0 1.75C0 .784.784 0 1.75 0h12.5C15.216 0 16 .784 16 1.75v12.5A1.75 1.75 0 0 1 14.25 16H1.75A1.75 1.75 0 0 1 0 14.25Zm1.75-.25a.25.25 0 0 0-.25.25v12.5c0 .138.112.25.25.25h12.5a.25.25 0 0 0 .25-.25V1.75a.25.25 0 0 0-.25-.25Zm7.47 3.97a.75.75 0 0 1 1.06 0l2 2a.75.75 0 0 1 0 1.06l-2 2a.749.749 0 0 1-1.275-.326.749.749 0 0 1 .215-.734L10.69 8 9.22 6.53a.75.75 0 0 1 0-1.06ZM6.78 6.53 5.31 8l1.47 1.47a.749.749 0 0 1-.326 1.275.749.749 0 0 1-.734-.215l-2-2a.75.75 0 0 1 0-1.06l2-2a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042Z"></path>
</svg>
          </span>
          <a class="wb-break-all" href="#file-iddqd-yar">
            <strong class="user-select-contain gist-blob-name css-truncate-target">
              iddqd.yar
            </strong>
          </a>
        </div>
      </div>
    
    <div itemprop="text" class="Box-body p-0 blob-wrapper data type-yara  gist-border-0">

        
<div class="js-check-bidi js-blob-code-container blob-code-content">

  <template class="js-file-alert-template">
  <div data-view-component="true" class="flash flash-warn flash-full d-flex flex-items-center">
  <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path d="M6.457 1.047c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0 1 14.082 15H1.918a1.75 1.75 0 0 1-1.543-2.575Zm1.763.707a.25.25 0 0 0-.44 0L1.698 13.132a.25.25 0 0 0 .22.368h12.164a.25.25 0 0 0 .22-.368Zm.53 3.996v2.5a.75.75 0 0 1-1.5 0v-2.5a.75.75 0 0 1 1.5 0ZM9 11a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path>
</svg>
    <span>
      This file contains bidirectional Unicode text that may be interpreted or compiled differently than what appears below. To review, open the file in an editor that reveals hidden Unicode characters.
      <a href="https://github.co/hiddenchars" target="_blank">Learn more about bidirectional Unicode characters</a>
    </span>


  <div data-view-component="true" class="flash-action">        <a href="{{ revealButtonHref }}" data-view-component="true" class="btn-sm btn">    Show hidden characters
</a>
</div>
</div></template>
<template class="js-line-alert-template">
  <span aria-label="This line has hidden Unicode characters" data-view-component="true" class="line-alert tooltipped tooltipped-e">
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path d="M6.457 1.047c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0 1 14.082 15H1.918a1.75 1.75 0 0 1-1.543-2.575Zm1.763.707a.25.25 0 0 0-.44 0L1.698 13.132a.25.25 0 0 0 .22.368h12.164a.25.25 0 0 0 .22-.368Zm.53 3.996v2.5a.75.75 0 0 1-1.5 0v-2.5a.75.75 0 0 1 1.5 0ZM9 11a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path>
</svg>
</span></template>

  <table data-hpc class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" data-tab-size="8" data-paste-markdown-skip data-tagsearch-lang="YARA" data-tagsearch-path="iddqd.yar">
        <tr>
          <td id="file-iddqd-yar-L1" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="1"></td>
          <td id="file-iddqd-yar-LC1" class="blob-code blob-code-inner js-file-line"><span class="pl-c">/*</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L2" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="2"></td>
          <td id="file-iddqd-yar-LC2" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      _____        __  __  ___        __      </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L3" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="3"></td>
          <td id="file-iddqd-yar-LC3" class="blob-code blob-code-inner js-file-line"><span class="pl-c">     / ___/__  ___/ / /  |/  /__  ___/ /__    </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L4" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="4"></td>
          <td id="file-iddqd-yar-LC4" class="blob-code blob-code-inner js-file-line"><span class="pl-c">    / (_ / _ \/ _  / / /|_/ / _ \/ _  / -_)   </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L5" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="5"></td>
          <td id="file-iddqd-yar-LC5" class="blob-code blob-code-inner js-file-line"><span class="pl-c">    \___/\___/\_,_/_/_/__/_/\___/\_,_/\__/    </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L6" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="6"></td>
          <td id="file-iddqd-yar-LC6" class="blob-code blob-code-inner js-file-line"><span class="pl-c">     \ \/ / _ | / _ \/ _ |   / _ \__ __/ /__  </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L7" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="7"></td>
          <td id="file-iddqd-yar-LC7" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      \  / __ |/ , _/ __ |  / , _/ // / / -_) </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L8" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="8"></td>
          <td id="file-iddqd-yar-LC8" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      /_/_/ |_/_/|_/_/ |_| /_/|_|\_,_/_/\__/  </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L9" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="9"></td>
          <td id="file-iddqd-yar-LC9" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      Florian Roth - v0.5.0 October 2019</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L10" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="10"></td>
          <td id="file-iddqd-yar-LC10" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L11" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="11"></td>
          <td id="file-iddqd-yar-LC11" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      A proof-of-concept rule that shows how easy it actually is to detect red teamer</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L12" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="12"></td>
          <td id="file-iddqd-yar-LC12" class="blob-code blob-code-inner js-file-line"><span class="pl-c">      and threat group tools and code </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L13" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="13"></td>
          <td id="file-iddqd-yar-LC13" class="blob-code blob-code-inner js-file-line"><span class="pl-c">*/</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L14" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="14"></td>
          <td id="file-iddqd-yar-LC14" class="blob-code blob-code-inner js-file-line">
</td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L15" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="15"></td>
          <td id="file-iddqd-yar-LC15" class="blob-code blob-code-inner js-file-line"><span class="pl-k">rule</span> <span class="pl-en">IDDQD_Godmode_Rule</span> {</td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L16" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="16"></td>
          <td id="file-iddqd-yar-LC16" class="blob-code blob-code-inner js-file-line">   <span class="pl-k">meta</span>:</td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L17" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="17"></td>
          <td id="file-iddqd-yar-LC17" class="blob-code blob-code-inner js-file-line">      <span class="pl-c1"><span class="pl-e">description</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">This is the most powerful YARA rule. It detects literally everything.</span><span class="pl-pds">&quot;</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L18" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="18"></td>
          <td id="file-iddqd-yar-LC18" class="blob-code blob-code-inner js-file-line">      <span class="pl-c1"><span class="pl-e">author</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Florian Roth</span><span class="pl-pds">&quot;</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L19" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="19"></td>
          <td id="file-iddqd-yar-LC19" class="blob-code blob-code-inner js-file-line">      <span class="pl-c1"><span class="pl-e">reference</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Internal Research - get a Godmode YARA rule set with Valhalla by Nextron Systems</span><span class="pl-pds">&quot;</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L20" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="20"></td>
          <td id="file-iddqd-yar-LC20" class="blob-code blob-code-inner js-file-line">      <span class="pl-c1"><span class="pl-e">date</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">2019-05-15</span><span class="pl-pds">&quot;</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L21" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="21"></td>
          <td id="file-iddqd-yar-LC21" class="blob-code blob-code-inner js-file-line">      <span class="pl-c1"><span class="pl-e">score</span> <span class="pl-k">=</span> <span class="pl-c1">60</span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L22" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="22"></td>
          <td id="file-iddqd-yar-LC22" class="blob-code blob-code-inner js-file-line">   <span class="pl-en"><span class="pl-k">strings</span>:</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L23" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="23"></td>
          <td id="file-iddqd-yar-LC23" class="blob-code blob-code-inner js-file-line"><span class="pl-en">      <span class="pl-c">/* Plain strings */</span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L24" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="24"></td>
          <td id="file-iddqd-yar-LC24" class="blob-code blob-code-inner js-file-line"><span class="pl-en">      <span class="pl-c1"><span class="pl-c1">$</span><span class="pl-smi">s01</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">sekurlsa::logonpasswords</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span> <span class="pl-k">nocase</span>           <span class="pl-c">/* Mimikatz Command */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L25" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="25"></td>
          <td id="file-iddqd-yar-LC25" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s02</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">ERROR kuhl</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">wide</span>                                      <span class="pl-c">/* Mimikatz Error */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L26" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="26"></td>
          <td id="file-iddqd-yar-LC26" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s03</span> <span class="pl-k">=</span> <span class="pl-sr">/(<span class="pl-sr">@subtee</span><span class="pl-k">|</span><span class="pl-sr">@mattifestation</span><span class="pl-k">|</span><span class="pl-sr">@enigma0x3</span>)/</span> <span class="pl-k">fullword</span> <span class="pl-k">ascii</span>  <span class="pl-c">/* Red Team Tools */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L27" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="27"></td>
          <td id="file-iddqd-yar-LC27" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s04</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s"> -w hidden </span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                               <span class="pl-c">/* Power Shell Params */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L28" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="28"></td>
          <td id="file-iddqd-yar-LC28" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s05</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s"> -decode </span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                                 <span class="pl-c">/* certutil command */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L29" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="29"></td>
          <td id="file-iddqd-yar-LC29" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s06</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Koadic.</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span>                                        <span class="pl-c">/* Koadic Framework */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L30" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="30"></td>
          <td id="file-iddqd-yar-LC30" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s07</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">ReflectiveLoader</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">fullword</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                 <span class="pl-c">/* Generic - Common Export Name */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L31" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="31"></td>
          <td id="file-iddqd-yar-LC31" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s08</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">InjectDLL</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">fullword</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                        <span class="pl-c">/* DLL Injection Keyword */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L32" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="32"></td>
          <td id="file-iddqd-yar-LC32" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s09</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">[System.Convert]::FromBase64String(</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>       <span class="pl-c">/* PowerShell - Base64 Encoded Payload */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L33" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="33"></td>
          <td id="file-iddqd-yar-LC33" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s10</span> <span class="pl-k">=</span> <span class="pl-sr">/<span class="pl-cce">\\</span>(<span class="pl-sr">Release</span><span class="pl-k">|</span><span class="pl-sr">Debug</span>)<span class="pl-cce">\\</span><span class="pl-sr">ms1</span><span class="pl-c1">[</span><span class="pl-c1">2</span><span class="pl-c1">-</span><span class="pl-c1">9</span><span class="pl-c1">]</span>/</span> <span class="pl-k">ascii</span>                    <span class="pl-c">/* Exploit Codes / PoCs */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L34" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="34"></td>
          <td id="file-iddqd-yar-LC34" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s11</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">/meterpreter/</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span>                                  <span class="pl-c">/* Metasploit Framework - Meterpreter */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L35" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="35"></td>
          <td id="file-iddqd-yar-LC35" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s12</span> <span class="pl-k">=</span> <span class="pl-sr">/<span class="pl-sr"> </span>(<span class="pl-sr">-e </span><span class="pl-k">|</span><span class="pl-sr">-enc </span><span class="pl-k">|</span><span class="pl-sr">&#39;</span><span class="pl-k">|</span><span class="pl-sr">&quot;</span>)(<span class="pl-sr">JAB</span><span class="pl-k">|</span><span class="pl-sr">SUVYI</span><span class="pl-k">|</span><span class="pl-sr">aWV4I</span><span class="pl-k">|</span><span class="pl-sr">SQBFAFgA</span><span class="pl-k">|</span><span class="pl-sr">aQBlAHgA</span>)/</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>  <span class="pl-c">/* PowerShell Encoded Code */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L36" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="36"></td>
          <td id="file-iddqd-yar-LC36" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s13</span> <span class="pl-k">=</span> <span class="pl-sr">/<span class="pl-sr">  </span>(<span class="pl-sr">sEt</span><span class="pl-k">|</span><span class="pl-sr">SEt</span><span class="pl-k">|</span><span class="pl-sr">SeT</span><span class="pl-k">|</span><span class="pl-sr">sET</span><span class="pl-k">|</span><span class="pl-sr">seT</span>)<span class="pl-sr">  </span>/</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                 <span class="pl-c">/* Casing Obfuscation */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L37" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="37"></td>
          <td id="file-iddqd-yar-LC37" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s14</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">);iex </span><span class="pl-pds">&quot;</span></span> <span class="pl-k">nocase</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                             <span class="pl-c">/* PowerShell - compact code */</span> </span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L38" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="38"></td>
          <td id="file-iddqd-yar-LC38" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s15</span> <span class="pl-k">=</span> <span class="pl-sr">/<span class="pl-sr"> </span>(<span class="pl-sr">cMd</span><span class="pl-cce">\.</span><span class="pl-k">|</span><span class="pl-sr">cmD</span><span class="pl-cce">\.</span><span class="pl-k">|</span><span class="pl-sr">CmD</span><span class="pl-cce">\.</span><span class="pl-k">|</span><span class="pl-sr">cMD</span><span class="pl-cce">\.</span>)/</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                <span class="pl-c">/* Casing Obfuscation */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L39" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="39"></td>
          <td id="file-iddqd-yar-LC39" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s16</span> <span class="pl-k">=</span> <span class="pl-sr">/(<span class="pl-sr">TW96aWxsYS</span><span class="pl-k">|</span><span class="pl-sr">1vemlsbGEv</span><span class="pl-k">|</span><span class="pl-sr">Nb3ppbGxhL</span><span class="pl-k">|</span><span class="pl-sr">TQBvAHoAaQBsAGwAYQAv</span><span class="pl-k">|</span><span class="pl-sr">0AbwB6AGkAbABsAGEAL</span><span class="pl-k">|</span><span class="pl-sr">BNAG8AegBpAGwAbABhAC</span>)/</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span> <span class="pl-c">/* Base64 Encoded UA */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L40" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="40"></td>
          <td id="file-iddqd-yar-LC40" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s17</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Nir Sofer</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">fullword</span> <span class="pl-k">wide</span>                              <span class="pl-c">/* Hack Tool Producer */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L41" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="41"></td>
          <td id="file-iddqd-yar-LC41" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s18</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Web Shell By </span><span class="pl-pds">&quot;</span></span> <span class="pl-k">nocase</span> <span class="pl-k">ascii</span>                           <span class="pl-c">/* Web Shell Copyright */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L42" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="42"></td>
          <td id="file-iddqd-yar-LC42" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s19</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">impacket.</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span>                                      <span class="pl-c">/* Impacket Library */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L43" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="43"></td>
          <td id="file-iddqd-yar-LC43" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s20</span> <span class="pl-k">=</span> <span class="pl-sr">/<span class="pl-cce">\[</span><span class="pl-c1">[</span><span class="pl-cce">\+\-</span><span class="pl-c1">!E</span><span class="pl-c1">]</span><span class="pl-cce">\]</span><span class="pl-sr"> </span>(<span class="pl-sr">exploit</span><span class="pl-k">|</span><span class="pl-sr">target</span><span class="pl-k">|</span><span class="pl-sr">vulnerab</span><span class="pl-k">|</span><span class="pl-sr">shell</span><span class="pl-k">|</span><span class="pl-sr">inject</span><span class="pl-k">|</span><span class="pl-sr">dump</span>)/</span> <span class="pl-k">nocase</span>  <span class="pl-c">/* Hack Tool Output Pattern */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L44" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="44"></td>
          <td id="file-iddqd-yar-LC44" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s21</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">ecalper</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">fullword</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                          <span class="pl-c">/* Reversed String - often found in scripts or web shells */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L45" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="45"></td>
          <td id="file-iddqd-yar-LC45" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s22</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">0000FEEDACDC}</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span>                             <span class="pl-c">/* Squiblydoo - Class ID */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L46" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="46"></td>
          <td id="file-iddqd-yar-LC46" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s23</span> <span class="pl-k">=</span> <span class="pl-sr">/(<span class="pl-sr">click enable editing</span><span class="pl-k">|</span><span class="pl-sr">click enable content</span><span class="pl-k">|</span><span class="pl-sr">&quot;Enable Editing&quot;</span><span class="pl-k">|</span><span class="pl-sr">&quot;Enable Content&quot;</span>)/</span> <span class="pl-k">ascii</span>  <span class="pl-c">/* Phishing Docs */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L47" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="47"></td>
          <td id="file-iddqd-yar-LC47" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s24</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">vssadmin delete shadows</span><span class="pl-pds">&quot;</span></span>                              <span class="pl-c">/* Shadow Copy Deletion - often used in Ransomware */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L48" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="48"></td>
          <td id="file-iddqd-yar-LC48" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s25</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">stratum+tcp://</span><span class="pl-pds">&quot;</span></span>                                       <span class="pl-c">/* Stratum Address - used in Crypto Miners */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L49" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="49"></td>
          <td id="file-iddqd-yar-LC49" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s26</span> <span class="pl-k">=</span> <span class="pl-sr">/<span class="pl-cce">\\</span>(<span class="pl-sr">Debug</span><span class="pl-k">|</span><span class="pl-sr">Release</span>)<span class="pl-cce">\\</span>(<span class="pl-sr">Downloader</span><span class="pl-k">|</span><span class="pl-sr">Key</span><span class="pl-c1">[</span><span class="pl-c1">lL</span><span class="pl-c1">]</span><span class="pl-sr">og</span><span class="pl-k">|</span><span class="pl-c1">[</span><span class="pl-c1">Ii</span><span class="pl-c1">]</span><span class="pl-sr">nject</span><span class="pl-k">|</span><span class="pl-sr">Steal</span><span class="pl-k">|</span><span class="pl-sr">By</span><span class="pl-c1">[</span><span class="pl-c1">Pp</span><span class="pl-c1">]</span><span class="pl-sr">ass</span><span class="pl-k">|</span><span class="pl-sr">UAC</span><span class="pl-k">|</span><span class="pl-sr">Dropper</span><span class="pl-k">|</span><span class="pl-sr">Loader</span><span class="pl-k">|</span><span class="pl-sr">CVE</span><span class="pl-cce">\-</span>)/</span>  <span class="pl-c">/* Typical PDB Strings 1 */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L50" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="50"></td>
          <td id="file-iddqd-yar-LC50" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">s27</span> <span class="pl-k">=</span> <span class="pl-sr">/(<span class="pl-sr">Dropper</span><span class="pl-k">|</span><span class="pl-sr">Downloader</span><span class="pl-k">|</span><span class="pl-sr">Bypass</span><span class="pl-k">|</span><span class="pl-sr">Injection</span>)<span class="pl-cce">\.</span><span class="pl-sr">pdb</span>/</span> <span class="pl-k">nocase</span>    <span class="pl-c">/* Typical PDF strings 2 */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L51" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="51"></td>
          <td id="file-iddqd-yar-LC51" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c">/* Combos */</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L52" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="52"></td>
          <td id="file-iddqd-yar-LC52" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">xo1</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Mozilla/5.0</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">xor</span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L53" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="53"></td>
          <td id="file-iddqd-yar-LC53" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">      <span class="pl-c1">$</span><span class="pl-smi">xf1</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">&quot;</span><span class="pl-s">Mozilla/5.0</span><span class="pl-pds">&quot;</span></span> <span class="pl-k">ascii</span> <span class="pl-k">wide</span></span></span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L54" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="54"></td>
          <td id="file-iddqd-yar-LC54" class="blob-code blob-code-inner js-file-line"><span class="pl-en"><span class="pl-c1">   </span></span><span class="pl-en"><span class="pl-k">condition</span>:</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L55" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="55"></td>
          <td id="file-iddqd-yar-LC55" class="blob-code blob-code-inner js-file-line"><span class="pl-en">      <span class="pl-c1">1</span> <span class="pl-k">of</span> (<span class="pl-smi"><span class="pl-c1">$</span><span class="pl-smi">s</span><span class="pl-s">*</span></span>) <span class="pl-k">or</span> </span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L56" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="56"></td>
          <td id="file-iddqd-yar-LC56" class="blob-code blob-code-inner js-file-line"><span class="pl-en">      ( <span class="pl-smi"><span class="pl-c1">$</span><span class="pl-smi">xo1</span></span> <span class="pl-k">and</span> <span class="pl-k">not</span> <span class="pl-smi"><span class="pl-c1">$</span><span class="pl-smi">xf1</span></span> )</span></td>
        </tr>
        <tr>
          <td id="file-iddqd-yar-L57" class="blob-num js-line-number js-code-nav-line-number js-blob-rnum" data-line-number="57"></td>
          <td id="file-iddqd-yar-LC57" class="blob-code blob-code-inner js-file-line"><span class="pl-en"></span>}</td>
        </tr>
  </table>
</div>


    </div>

  </div>
</div>


      <a name="comments"></a>
      <div class="js-quote-selection-container" data-quote-markdown=".js-comment-body">
        <div class="js-discussion "
        >
          <div class="ml-md-6 pl-md-3 ml-0 pl-0">
            



<!-- Rendered timeline since 2022-11-17 08:41:14 -->
<div id="partial-timeline-marker"
      class="js-timeline-marker js-updatable-content"
      data-last-modified="Thu, 17 Nov 2022 16:41:14 GMT"
      >
</div>

          </div>

          <div class="discussion-timeline-actions">
            <div data-view-component="true" class="flash flash-warn mt-3">
  
    <a rel="nofollow" class="btn btn-primary" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;signed out comment&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="6fec1831f16c66293ad325299e593234c6703f2ac3ae7bae24e8b774f74dc676" href="/join?source=comment-gist">Sign up for free</a>
    <strong>to join this conversation on GitHub</strong>.
    Already have an account?
    <a rel="nofollow" data-hydro-click="{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;signed out comment&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;LOG_IN&quot;,&quot;originating_url&quot;:&quot;https://gist.github.com/Neo23x0/f1bb645a4f715cb499150c5a14d82b44&quot;,&quot;user_id&quot;:null}}" data-hydro-click-hmac="23ec98099f74a78550c38ef739f59d605b796ebb354aa24799b65aa5f9b84d72" data-test-selector="comments-sign-in-link" href="/login?return_to=https%3A%2F%2Fgist.github.com%2FNeo23x0%2Ff1bb645a4f715cb499150c5a14d82b44">Sign in to comment</a>


  
</div>
          </div>
        </div>
      </div>
</div>
  </div>
</div><!-- /.container -->

    </main>
  </div>

  </div>

          <footer class="footer width-full container-lg p-responsive" role="contentinfo">
  <h2 class='sr-only'>Footer</h2>

  <div class="position-relative d-flex flex-items-center pb-2 f6 color-fg-muted border-top color-border-muted flex-column-reverse flex-lg-row flex-wrap flex-lg-nowrap mt-6 pt-6">
    <div class="list-style-none d-flex flex-wrap col-0 col-lg-2 flex-justify-start flex-lg-justify-between mb-2 mb-lg-0">
      <div class="mt-2 mt-lg-0 d-flex flex-items-center">
        <a aria-label="Homepage" title="GitHub" class="footer-octicon mr-2" href="https://github.com">
          <svg aria-hidden="true" height="24" viewBox="0 0 16 16" version="1.1" width="24" data-view-component="true" class="octicon octicon-mark-github">
    <path d="M8 0c4.42 0 8 3.58 8 8a8.013 8.013 0 0 1-5.45 7.59c-.4.08-.55-.17-.55-.38 0-.27.01-1.13.01-2.2 0-.75-.25-1.23-.54-1.48 1.78-.2 3.65-.88 3.65-3.95 0-.88-.31-1.59-.82-2.15.08-.2.36-1.02-.08-2.12 0 0-.67-.22-2.2.82-.64-.18-1.32-.27-2-.27-.68 0-1.36.09-2 .27-1.53-1.03-2.2-.82-2.2-.82-.44 1.1-.16 1.92-.08 2.12-.51.56-.82 1.28-.82 2.15 0 3.06 1.86 3.75 3.64 3.95-.23.2-.44.55-.51 1.07-.46.21-1.61.55-2.33-.66-.15-.24-.6-.83-1.23-.82-.67.01-.27.38.01.53.34.19.73.9.82 1.13.16.45.68 1.31 2.69.94 0 .67.01 1.3.01 1.49 0 .21-.15.45-.55.38A7.995 7.995 0 0 1 0 8c0-4.42 3.58-8 8-8Z"></path>
</svg>
</a>        <span>
        &copy; 2023 GitHub, Inc.
        </span>
      </div>
    </div>

    <nav aria-label='Footer' class="col-12 col-lg-8">
      <h3 class='sr-only' id='sr-footer-heading'>Footer navigation</h3>
      <ul class="list-style-none d-flex flex-wrap col-12 flex-justify-center flex-lg-justify-between mb-2 mb-lg-0" aria-labelledby='sr-footer-heading'>
          <li class="mr-3 mr-lg-0"><a href="https://docs.github.com/site-policy/github-terms/github-terms-of-service" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to terms&quot;,&quot;label&quot;:&quot;text:terms&quot;}">Terms</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://docs.github.com/site-policy/privacy-policies/github-privacy-statement" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to privacy&quot;,&quot;label&quot;:&quot;text:privacy&quot;}">Privacy</a></li>
          <li class="mr-3 mr-lg-0"><a data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to security&quot;,&quot;label&quot;:&quot;text:security&quot;}" href="https://github.com/security">Security</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://www.githubstatus.com/" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to status&quot;,&quot;label&quot;:&quot;text:status&quot;}">Status</a></li>
          <li class="mr-3 mr-lg-0"><a data-ga-click="Footer, go to help, text:Docs" href="https://docs.github.com">Docs</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://support.github.com?tags=dotcom-footer" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to contact&quot;,&quot;label&quot;:&quot;text:contact&quot;}">Contact GitHub</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://github.com/pricing" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to Pricing&quot;,&quot;label&quot;:&quot;text:Pricing&quot;}">Pricing</a></li>
        <li class="mr-3 mr-lg-0"><a href="https://docs.github.com" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to api&quot;,&quot;label&quot;:&quot;text:api&quot;}">API</a></li>
        <li class="mr-3 mr-lg-0"><a href="https://services.github.com" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to training&quot;,&quot;label&quot;:&quot;text:training&quot;}">Training</a></li>
          <li class="mr-3 mr-lg-0"><a href="https://github.blog" data-analytics-event="{&quot;category&quot;:&quot;Footer&quot;,&quot;action&quot;:&quot;go to blog&quot;,&quot;label&quot;:&quot;text:blog&quot;}">Blog</a></li>
          <li><a data-ga-click="Footer, go to about, text:about" href="https://github.com/about">About</a></li>
      </ul>
    </nav>
  </div>

  <div class="d-flex flex-justify-center pb-6">
    <span class="f6 color-fg-muted"></span>
  </div>
</footer>




  <div id="ajax-error-message" class="ajax-error-message flash flash-error" hidden>
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path d="M6.457 1.047c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0 1 14.082 15H1.918a1.75 1.75 0 0 1-1.543-2.575Zm1.763.707a.25.25 0 0 0-.44 0L1.698 13.132a.25.25 0 0 0 .22.368h12.164a.25.25 0 0 0 .22-.368Zm.53 3.996v2.5a.75.75 0 0 1-1.5 0v-2.5a.75.75 0 0 1 1.5 0ZM9 11a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path>
</svg>
    <button type="button" class="flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path d="M3.72 3.72a.75.75 0 0 1 1.06 0L8 6.94l3.22-3.22a.749.749 0 0 1 1.275.326.749.749 0 0 1-.215.734L9.06 8l3.22 3.22a.749.749 0 0 1-.326 1.275.749.749 0 0 1-.734-.215L8 9.06l-3.22 3.22a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042L6.94 8 3.72 4.78a.75.75 0 0 1 0-1.06Z"></path>
</svg>
    </button>
    You can’t perform that action at this time.
  </div>

  <div class="js-stale-session-flash flash flash-warn flash-banner" hidden
    >
    <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-alert">
    <path d="M6.457 1.047c.659-1.234 2.427-1.234 3.086 0l6.082 11.378A1.75 1.75 0 0 1 14.082 15H1.918a1.75 1.75 0 0 1-1.543-2.575Zm1.763.707a.25.25 0 0 0-.44 0L1.698 13.132a.25.25 0 0 0 .22.368h12.164a.25.25 0 0 0 .22-.368Zm.53 3.996v2.5a.75.75 0 0 1-1.5 0v-2.5a.75.75 0 0 1 1.5 0ZM9 11a1 1 0 1 1-2 0 1 1 0 0 1 2 0Z"></path>
</svg>
    <span class="js-stale-session-flash-signed-in" hidden>You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
    <span class="js-stale-session-flash-signed-out" hidden>You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
  </div>
    <template id="site-details-dialog">
  <details class="details-reset details-overlay details-overlay-dark lh-default color-fg-default hx_rsm" open>
    <summary role="button" aria-label="Close dialog"></summary>
    <details-dialog class="Box Box--overlay d-flex flex-column anim-fade-in fast hx_rsm-dialog hx_rsm-modal">
      <button class="Box-btn-octicon m-0 btn-octicon position-absolute right-0 top-0" type="button" aria-label="Close dialog" data-close-dialog>
        <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-x">
    <path d="M3.72 3.72a.75.75 0 0 1 1.06 0L8 6.94l3.22-3.22a.749.749 0 0 1 1.275.326.749.749 0 0 1-.215.734L9.06 8l3.22 3.22a.749.749 0 0 1-.326 1.275.749.749 0 0 1-.734-.215L8 9.06l-3.22 3.22a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042L6.94 8 3.72 4.78a.75.75 0 0 1 0-1.06Z"></path>
</svg>
      </button>
      <div class="octocat-spinner my-6 js-details-dialog-spinner"></div>
    </details-dialog>
  </details>
</template>

    <div class="Popover js-hovercard-content position-absolute" style="display: none; outline: none;" tabindex="0">
  <div class="Popover-message Popover-message--bottom-left Popover-message--large Box color-shadow-large" style="width:360px;">
  </div>
</div>

    <template id="snippet-clipboard-copy-button">
  <div class="zeroclipboard-container position-absolute right-0 top-0">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn js-clipboard-copy m-2 p-0 tooltipped-no-delay" data-copy-feedback="Copied!" data-tooltip-direction="w">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon m-2">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none m-2">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>
<template id="snippet-clipboard-copy-button-unpositioned">
  <div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div>
</template>




    </div>

    <div id="js-global-screen-reader-notice" class="sr-only" aria-live="polite" ></div>
  </body>
</html>

