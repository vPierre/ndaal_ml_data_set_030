/*
Generated by Yara-Rules
On 20-05-2023
*/
include "./MalGamy/APT29.yara"
include "./MalGamy/Mars_Stealer.yara"
include "./MalGamy/Nokoyawa.yara"
include "./MalGamy/Nosu_Stealer.yara"
include "./MalGamy/Rifdoor.yara"
include "./MalGamy/Shc_downloader.yara"
include "./MalGamy/SystemBC.yara"
include "./MalGamy/Typhone.yara"
include "./MalGamy/ViceSociety.yara"
include "./MalGamy/Vidar_Stealer.yara"
include "./MalGamy/catB.yara"
include "./MalGamy/colibri.yara"
include "./MalGamy/cova.yara"
include "./MalGamy/cuba_ransomware.yara"
include "./MalGamy/formbook.yara"
include "./MalGamy/lumma.yara"
include "./MalGamy/lumma_stealer.yara"
include "./MalGamy/mimic.yara"
include "./MalGamy/raccoon_stealer.yara"
include "./MalGamy/silence.yara"
include "./MalGamy/strelastealer.yara"
include "./MalGamy/tofsee.yara"
include "./MalGamy/vohuk.yara"
