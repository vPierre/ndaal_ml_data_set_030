/*
Generated by Yara-Rules
On 20-05-2023
*/
include "./elceef_yara-rulz/rules/Base64_SVG_Javascript.yara"
include "./elceef_yara-rulz/rules/EICAR_Encrypted_ZIP.yara"
include "./elceef_yara-rulz/rules/HTA_OneNote.yara"
include "./elceef_yara-rulz/rules/HTML_Smuggling.yara"
include "./elceef_yara-rulz/rules/OLE2_Reversed.yara"
include "./elceef_yara-rulz/rules/Obfuscated_IPAddr_URL.yara"
include "./elceef_yara-rulz/rules/Outlook_CVE_2023_23397.yara"
include "./elceef_yara-rulz/rules/Suspicious_BAT.yara"
include "./elceef_yara-rulz/rules/Suspicious_OneNote.yara"
include "./elceef_yara-rulz/rules/Suspicious_SFX.yara"
include "./elceef_yara-rulz/rules/ZIP_High_Ratio_Single_Doc.yara"
