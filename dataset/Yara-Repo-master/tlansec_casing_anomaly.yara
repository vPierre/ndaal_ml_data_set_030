rule general_win_runkey_casing_anomaly : General
{
    meta:
        author = "threatintel@volexity.com"
        description = "Looks for files containing to a reference to the HKCU run key where the reference uses unusual casing."
        date = "2021-08-03"
        hash1 = "c20997c72508bc7340f4ec99fe9eb4f1ccde518e81bda66e7c86632f0748bffa"
        memory_suitable = 0

    strings:
        $legit1 = "Software\\Microsoft\\Windows\\CurrentVersion\\Run" wide ascii
        $legit2 = "Software\\Microsoft\\Windows\\CurrentVersion\\run" wide ascii 
        $legit3 = "software\\microsoft\\windows\\currentVersion\\run" wide ascii
        $legit4 = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run" wide ascii
        $legit5 = "SOFTWARE\\MICROSOFT\\WINDOWS\\CURRENTVERSION\\RUN" wide ascii
        $legit6 = "SOFTWARE\\MICROSOFT\\Windows\\CurrentVersion\\Run" wide ascii
        $legit7 = "SOFTWARE\\MICROSOFT\\Windows\\CurrentVersion\\run" wide ascii
        
        $anycase = "Software\\Microsoft\\Windows\\CurrentVersion\\Run" wide ascii nocase
        

    condition:
        $anycase and not 
        any of ($legit*)
}