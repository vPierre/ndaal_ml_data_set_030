
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
<<<<<<< HEAD
=======
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
>>>>>>> e238cb08 (updated YARA rules)
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}
rule winnow_phish_application_1
{
    strings:
			$a0 = { 646f63756d656e742e777269746528756e6573636170652827253343253231253434253446253433253534253539253530253435253230253438253534253444253443253230253530253535253432 }

    condition:
        any of them
}
rule winnow_phish_ts_emailblaster_1
{
    strings:
			$a0 = { 656d61696c626c617374657240 }

    condition:
        any of them
}
rule winnow_phish_esp_adobe_1
{
    strings:
			$a0 = { 416374696f6e2052657175697265643a20446f776e6c6f6164204e65772041646f6265204163726f6261742052656164657220466f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_10
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_11
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d616379204578737072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_12
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d6163792045787072657373 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_13
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_14
{
    strings:
			$a0 = { 46726f6d3a20[0-20]223a3a43616e616469616e20506861726d6163793a3a }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_15
{
    strings:
			$a0 = { 46726f6d3a20[0-20]2243616e616469616e20506761726d616369 }

    condition:
        any of them
}
rule winnow_phish_ts_drugs_16
{
    strings:
			$a0 = { 46726f6d3a20[0-20]22506861726d204f6e2d4c696e65 }

    condition:
        any of them
}
rule winnow_phish_ts_unkn_17
{
    strings:
			$a0 = { 582d6d61696c65723a2055646b676a2031 }

    condition:
        any of them
}
rule winnow_phish_ts_scam_1
{
    strings:
			$a0 = { 2e636f6d2f636f6e74616374732f733322202f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_2
{
    strings:
			$a0 = { 2e72752f636f6e74616374732f7333222f3e }

    condition:
        any of them
}
rule winnow_phish_ts_scam_3
{
    strings:
			$a0 = { 202d20427573696e657373205765656b204a6f75726e616c20 }

    condition:
        any of them
}
rule winnow_phish_ts_webmail_6
{
    strings:
			$a0 = { 636f707920746865206c696e6b20616e6420706173746520697420696e20796f757220616464726573732062617220666f72207765626d61696c206d61696e7461696e616365 }

    condition:
        any of them
}
rule winnow_phish_ts_secretshopper_1
{
    strings:
			$a0 = { 5365637265742053686f7070657220496e63 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_1
{
    strings:
			$a0 = { 436f6e74656e742d547970653a20746578742f706c61696e3b20636861727365743d2277696e646f77732d31323532687474702d6571756976436f6e74656e742d5479706522 }

    condition:
        any of them
}
rule winnow_phish_ts_formbuddy_1
{
    strings:
			$a0 = { 757365726e616d653d636f6f6c6d6f7665 }

    condition:
        any of them
}
rule winnow_spam_ts_hiddenurl_1
{
    strings:
			$a0 = { 64656c65746520737061636573206265666f7265207669736974696e67 }

    condition:
        any of them
}
rule winnow_malware_ts_javawebstart_1
{
    strings:
			$a0 = { 3c68746d6c3e3c626f64793e3c6469762069643d226f626a223e3c2f6469763e3c6469762069643d22706466223e3c2f6469763e3c6469762069643d226a617661223e3c2f6469763e3c7363726970743e686f7374203d20 }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_1
{
    strings:
			$a0 = { 5b7072786d616769635d }

    condition:
        any of them
}
rule winnow_malware_ts_botcontrol_2
{
    strings:
			$a0 = { 73797374656d28227767657420687474703a2f2f76617269616e742e676f6f676c65636f64652e636f6d2f66696c65732f }

    condition:
        any of them
}
rule winnow_malware_ts_phpbot_1
{
    strings:
			$a0 = { 3c3f706870202f2a205741524e494e473a20546869732066696c652069732070726f74656374656420656e6372797074656420766961206675642066696c65206e6f20646574656364696e672e202a2f }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_1
{
    strings:
			$a0 = { 7061737465204a61766173637269707420696e20796f75722062726f77736572206164647265737320626172 }

    condition:
        any of them
}
rule winnow_malware_ts_facebook_2
{
    strings:
			$a0 = { 53756976657a206c6573206574617065732063692d646573736f757320706f757220766f69722071756920612072656761726465206d6f6e2070726f66696c2e }

    condition:
        any of them
}
rule winnow_malware_ts_driveby_2
{
    strings:
			$a0 = { 44726f7046696c654e616d65203d2022737663686f73742e65786522 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2222292e73756273747228322d312c34293b69662828613d3d2266756e6322297c7c28613d3d22756e63742229297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b226576616c225d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_1a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e343034206e6f7420666f756e643c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_Trojan_Downloader_JS_htaccess_2a
{
    strings:
			$a0 = { 3c68746d6c3e3c686561643e3c7469746c653e34303320666f7262696464656e3c2f7469746c653e3c2f686561643e3c626f64793e[120-250]733d6e6577737472696e6728293b613d28646f63756d656e742e6372656174656174747269627574652b2727292e73756273747228322d312c34293b69662828613d3d2766756e6327297c7c28613d3d27756e63742729297b723d313b633d737472696e673b7d696628722626646f63756d656e742e637265617465746578746e6f646529793d323b653d77696e646f775b276576616c275d3b6d3d6e6577617272617928 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_1
{
    strings:
			$a0 = { 5375626a6563743a2055706461746520596f7572205044462052656164657220666f722057696e646f7773 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_2
{
    strings:
			$a0 = { 5375626a6563743a20446f776e6c6f61642050444620526561646572203130 }

    condition:
        any of them
}
rule winnow_malware_ts_esp_3
{
    strings:
			$a0 = { 5375626a6563743a205044462055736572733a20557267656e742050444620557064617465 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_4
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_malware_ts_gambling_5
{
    strings:
			$a0 = { 527562792050616c616365 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_1
{
    strings:
			$a0 = { 66726f6d203038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_2
{
    strings:
			$a0 = { 66726f6d2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_Phish_ts_eSMS_3
{
    strings:
			$a0 = { 746f2038343630353738333537 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_1
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_2
{
    strings:
			$a0 = { 2f626c6f672f34633248 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_3
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_fiesta_ek_wordpress_4
{
    strings:
			$a0 = { 2f6a5f38367a667379 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_rce_wordpress_5
{
    strings:
			$a0 = { 22504354344241364f4453455f22 }

    condition:
        any of them
}
rule winnow_malware_php_img_1
{
    strings:
			$a0 = { 474946383961[0-45]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_img_2
{
    strings:
			$a0 = { 4749463839[0-3]203c3f706870 }

    condition:
        any of them
}
rule winnow_malware_php_id_1
{
    strings:
			$a0 = { 3c3f706870202f2a20[0-2]4678[0-2]4944202a2f206563686f2822[0-18]22293b206469652822[0-18]22293b202f2a20[0-2]4678[0-2]4944202a2f203f3e }

    condition:
        any of them
}
rule winnow_malware_php_bot_2
{
    strings:
			$a0 = { 2e75736572203c70617373776f72643e[0-2]2f2f6c6f67696e20746f2074686520626f74 }

    condition:
        any of them
}
rule winnow_phish_ts_badhdr_3
{
    strings:
			$a0 = { 636f6e74656e742d747970653a20746578742f706c61696e3b0d0a636861727365743d22 }

    condition:
        any of them
}
rule winnow_phish_ts_test_test
{
    strings:
			$a0 = { 74657374706f696e7473746172742d3e77696e6e6f77207068697368207465737420706f696e74207479706520343c2d74657374706f696e74656e64 }

    condition:
        any of them
}