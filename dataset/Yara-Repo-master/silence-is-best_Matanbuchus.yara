rule Matanbuchus_name_only
{
    meta:
        description = "Matanbuchus"
        author = "James_inthe_box"
        reference = "https://twitter.com/pr0xylife/status/1537511268591992840"
        date = "2022/06"
        maltype = "Loader"

    strings:
        $string1 = "Matanbuchus" ascii wide

    condition:
        all of ($string*) and filesize > 100KB
}

rule Matanbuchus_secondary_bin
{
    meta:
        description = "Matanbuchus secondary"
        author = "James_inthe_box"
        reference = "8833f28dc0cadd4b3c5676981b2a76e1c0683f2e2b8e3dac8270622c12e032ef"
        date = "2022/06"
        maltype = "Loader"

    strings:
        $string1 = "LoadDLL"
        $string2 = "HackCheck"

    condition:
        all of ($string*) and filesize < 500KB
}