/*
Generated by Yara-Rules
On 20-05-2023
*/
include "./0xThiebaut_Signatures/YARA/mal_electron_js_backdoor.yara"
include "./0xThiebaut_Signatures/YARA/mal_havoc.yara"
include "./0xThiebaut_Signatures/YARA/mal_injection_function_stomping.yara"
include "./0xThiebaut_Signatures/YARA/mal_metasploit_windows_shellcode.yara"
include "./0xThiebaut_Signatures/YARA/mal_syscall_hellshall.yara"
include "./0xThiebaut_Signatures/YARA/mal_syscall_hwsyscalls.yara"
include "./0xThiebaut_Signatures/YARA/sus_pe.yara"
include "./0xThiebaut_Signatures/YARA/sus_xll.yara"
include "./0xThiebaut_Signatures/YARA/weird_png.yara"
include "./0xThiebaut_Signatures/YARA/weird_zip.yara"
