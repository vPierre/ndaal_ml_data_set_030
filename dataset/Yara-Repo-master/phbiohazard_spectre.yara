import "hash"

rule RESERVED_QA
{
meta:
ref_IOC = "RESERVED_QA"
author = "Laboratoire Epidemiology & Signal Intelligence"

condition:
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}

rule IOC_NA_apk
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "apk"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "b41f6dfb115e0bdf3fd8e860036b4e04432f1af43ecb9cccea78539e701c03a1" or
hash.sha256(0, filesize) == "d66aac16d486b19ad92f0aea804d547eb8a91e640c4dc052877793873ff4af67" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_msi
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-09 16:42:02"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "msi"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "6bc1ce5595c1a093c01bae8cb1f049c1fbc60511914c7b581df2a89c47203803" or
hash.sha256(0, filesize) == "02d266017daa63108d520772e541f73a41b6d93808995d724a3e14db53696edc" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_dll
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-09 15:30:29"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "dll"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "cfacdbf069973361203b31e06bfedc579f38d80726334ad8a58422778306492b" or
hash.sha256(0, filesize) == "34279a1d45b05d672cf330f7c89617cc9c9a62851669b485453786bd2591e2f0" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AZORULT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AZORULT_LAB"
date_IOC = "2023-05-09 11:39:02"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AZORULT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "cf4bf8acac563ab519f40d9054376cb148154e4a7e2f6cab5ec6103cbecef655" or
hash.sha256(0, filesize) == "99eddc2794077f97a5cfe3098f431c4cfc4fd6353957ee715b2eccbff066ce1d" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FORMBOOK_rar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FORMBOOK_LAB"
date_IOC = "2023-05-09 11:33:26"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FORMBOOK"
file_type = "rar"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "6d54236955ff8f970a099705fc3daff94d1f5d024042bbe61a7b2dba1bb1ebe8" or
hash.sha256(0, filesize) == "ee77462522115980a08c313b7e5ffb6920c91d28291958f4821cbb9bb94f2810" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_DARKCOMET_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_DARKCOMET_LAB"
date_IOC = "2023-05-09 11:07:28"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "DARKCOMET"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "4f250e4c6d12fc351a9e0cfae8036b78a88c409e6c6c88a45aa6b659d8c4901e" or
hash.sha256(0, filesize) == "a5fb447c2992f48abd863452bcd6934032cf29bef1f0907bd9344a31a41e0edb" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_vbs
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-09 09:38:19"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "vbs"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "3e3ef2c4b7f4643431a881b7ab9f37e684053a54f3c9c75cd919560a9a4caca2" or
hash.sha256(0, filesize) == "bda7b64bfc5ce228789d160109671917c8a158ff8195ecc29eab9f9d022f7247" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ARKEISTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ARKEISTEALER_LAB"
date_IOC = "2023-05-09 08:58:57"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ARKEISTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "3b47e1955bcd60357e34f2e121cd227a36d1a8fd1c9ee83d162719a12e01f2e8" or
hash.sha256(0, filesize) == "2068c94f118c1921f4333d97935e35ce175803820c44444277563fe0f82398fb" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LOKI_rar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LOKI_LAB"
date_IOC = "2023-05-09 07:15:18"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LOKI"
file_type = "rar"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "2f7e808c9d3b349dee6bd9562cd2a52db31f71e3cfd649132b34e6085e543634" or
hash.sha256(0, filesize) == "e5d0cc163a7c1e14b68cc568e20198961b924e11dbaf1337a88ea521990de5da" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SYSTEMBC_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SYSTEMBC_LAB"
date_IOC = "2023-05-09 06:33:29"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SYSTEMBC"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "c671384b86b7a99d4328402936f51ca9143543f8e6f715c8315e18d2e3c660e6" or
hash.sha256(0, filesize) == "547c8ae1a463eef31e8d51f27408d144fe1cdde0cd31d4ccd8892f5200e111b1" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AVEMARIARAT_gz
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AVEMARIARAT_LAB"
date_IOC = "2023-05-09 06:08:55"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AVEMARIARAT"
file_type = "gz"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "c202c87add154c023ba273f759bdee5d934f33344da7077e510cbce60dffec67" or
hash.sha256(0, filesize) == "fd77641f06334a337f304443dc4bd15ecaffa57c2998b5b85ce81ea53be09d42" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SYSTEMBC_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SYSTEMBC_LAB"
date_IOC = "2023-05-09 04:03:10"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SYSTEMBC"
file_type = "zip"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "0bb78df6c8e049c7a33d2656555e15388a59ee96bde6f221ac5494b959cd60eb" or
hash.sha256(0, filesize) == "d817131a06e282101d1da0a44df9b273f2c65bd0f4dd7cd9ef8e74ed49ce57e4" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_COBALTSTRIKE_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_COBALTSTRIKE_LAB"
date_IOC = "2023-05-09 02:40:19"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "COBALTSTRIKE"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "03825741c04fb75f7bcb5d53d54ccbd0a7c00ebf3f187084e49b0fb95e67b51e" or
hash.sha256(0, filesize) == "c525b74313f951c336ad1074c4c43c447b7cd4d63d8988bb094d193ad2dfe182" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_ps1
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-09 02:31:04"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "ps1"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "12c7cbf78d296ce5d51cf1f36b96c7ed6bcecf22eb400e6cf172f792ccff0838" or
hash.sha256(0, filesize) == "bdda44318119236174b9499616434dc0ddf2651bccae2f86ed34bc1f25f57cfa" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_elf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-09 02:07:41"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "elf"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "25ff7ca0f48f7045c5a484bc576cb4ff875089f28a40fa5234af573f09eb597a" or
hash.sha256(0, filesize) == "3615ac05ea7d4e300fd3f175d8e78486d2ce9ea1bf8aada689c3f6d2222a1e77" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GURCUSTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GURCUSTEALER_LAB"
date_IOC = "2023-05-09 01:00:58"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GURCUSTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "c24938a87190df896986a22f9f66fb84401da04cda2a535856b0ce9eacb2bd0d" or
hash.sha256(0, filesize) == "446278b00e672276ebd77b7a20356f9fdad4aeb0add39d714de87f3c6b17af89" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_STOP_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_STOP_LAB"
date_IOC = "2023-05-09 00:11:32"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "STOP"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "ee871953192d79c2a1a9a7a0141419c9ac42b79e1eec17514e3e4edd18010ac4" or
hash.sha256(0, filesize) == "7201687dfb39aa50e4154a4c7cdcc5c0f6559d2c33eddf10b88b3b08f2042bd0" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_vbs
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 20:33:46"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "vbs"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "e75e7b4f4db640c54deb092dd373afa2b692a2078461cd0193e2b54b84c8250c" or
hash.sha256(0, filesize) == "d9577a11fb93cf09c220f70d087e55eb4c7c5fed0537aebd8013e7e01a8d5d15" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GULOADER_vbs
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GULOADER_LAB"
date_IOC = "2023-05-08 18:25:42"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GULOADER"
file_type = "vbs"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "beda408709feea7d2023f328e9c97bf4d090bcfb3948fc4e4d9c5c580d8f5858" or
hash.sha256(0, filesize) == "d167c08baf19919551b1731763974baa43a7193cfaf4704d754308d53bdb2b5e" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ASYNCRAT_js
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ASYNCRAT_LAB"
date_IOC = "2023-05-08 12:59:54"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ASYNCRAT"
file_type = "js"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "a779a194e7901ef59d91eef611fb4973560b399dfe4df6b6e64f07fd254d271a" or
hash.sha256(0, filesize) == "1139866f47d640744a522f86679627e568b52197f58b6484355b6c4544a6da22" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_rar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-08 08:55:02"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "rar"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "71e312895c934d6ee09573275ee3ed0f27a55d8dac9b051569cc08dadec76cbf" or
hash.sha256(0, filesize) == "32b4474225af93ed2d1cfd31e0af6939d3135eb6426a7cf8d7303b1d2d52b1a5" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_iso
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 07:04:02"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "iso"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "c9a906faa58da0d3ba2f29d91fe81eca228d57ce1dcee87f153139c5f1ac54f4" or
hash.sha256(0, filesize) == "7c7a54f513fa6f98b27c0bc0be8003f6f1caf5977fd98c75c8319d78f94cd3ac" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LOCKBIT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LOCKBIT_LAB"
date_IOC = "2023-05-08 03:27:40"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LOCKBIT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "acfe3f344edfbd4f62f2e28ff96bb3c62b589ab895990ee2498a33280442c0b3" or
hash.sha256(0, filesize) == "f7c432afe20c31ddd75dbb214f1b7aaeb400d9d17da6d9e3c0d7cf1dd38bf524" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NANOCORE_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NANOCORE_LAB"
date_IOC = "2023-05-09 01:05:27"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NANOCORE"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "a066c15490cf2a08af42f507381d6774e6de4650ba2b11c0880aff2b118b1cb9" or
hash.sha256(0, filesize) == "516dde3ce16ca9d1b0e2bf90b4eb57c6d00d3870f6cdb811d2ce88d350c46219" or
hash.sha256(0, filesize) == "8b959a5e40331f3f99835c6e8dc037def50a54211f258e419f7d5558675696ed" or
hash.sha256(0, filesize) == "bfa312225ab422d8ac44afd82b9c62868edaaaa75eca579f74ff943244e0e8ce" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-08 05:02:53"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "53f31bf8011613bead4f530d63b146825f5156a990ea6e486627f98db9a7f0c8" or
hash.sha256(0, filesize) == "45696f25affde4061e553cd2a5ef1fd943a17e15a0ca70937008c3d59473752d" or
hash.sha256(0, filesize) == "4a2b6f59a10bff2ffbbaad5ed9fbec98f78daae5fb43e3d283975dcb5491d264" or
hash.sha256(0, filesize) == "3c0149f7a435f3bd4ac52cc7c7d97188e5b817cd4df4e650c525da27f0c3b952" or
hash.sha256(0, filesize) == "032e861138d0a304d5a51bc7614f867b04d558596be03cf69ff9fd63098bc46d" or
hash.sha256(0, filesize) == "a481d0f71f3e020be9067340c91bd8a9472611cbff06571cf5084085b1ffa570" or
hash.sha256(0, filesize) == "5a9313a52213439705ad9f16926a8684e09a5ac91366e06d92f34e02832418e8" or
hash.sha256(0, filesize) == "324273456c3ed22910fabca9bdc9a52260e602a4adb59258ba0237eaf05142a0" or
hash.sha256(0, filesize) == "024bfe0a05c6e6790d67c8e32e075fa551a1bb0bec80d53f293b105d5ac29f05" or
hash.sha256(0, filesize) == "4dc6ddb5c569e1d297d4a076e355a9f2f79030b5d2f05b24b093a32946fea0e5" or
hash.sha256(0, filesize) == "dcf24840d82fa6f6be648d778bb5b0ae3373e58f66f2624267dc560a2ef5fcc0" or
hash.sha256(0, filesize) == "6f4414977fde0931838785b13d21964f972a8f24b0331ac22b43040d7693da27" or
hash.sha256(0, filesize) == "0ab70f9d6b40bfb958cedb2288ee9852cb0976f56a86b4adabd3bdc6464e66dc" or
hash.sha256(0, filesize) == "947e2ee6c6f9fc6d7b34b921a61f201831e3f53a84d1a1a8ded3ba557ba560ae" or
hash.sha256(0, filesize) == "ebf21a68edf7b17901564c4ae1c157f357dcd7fcdc436db86c0b46f61057c794" or
hash.sha256(0, filesize) == "a3acbb629367176c78ba48376ec9b7d2ae76541881bd65adb181be42ee730e20" or
hash.sha256(0, filesize) == "c190d2e389f4be05044069ea574f1ec1f5d1eeb239c35f4184b41909dd7488b6" or
hash.sha256(0, filesize) == "3c7edca94de418d0a4fe84cbc39abf773a079ea281544a72ed1fe56eb911aad2" or
hash.sha256(0, filesize) == "b5c8d3f1134cb93346fa8b5647a95e14b6fb8b04ab58583da61fbabf9d7052a6" or
hash.sha256(0, filesize) == "7bf2809cb8157e070a2a4e0ce55cc6f705ed141c26ccfbf27574bbde4edd8235" or
hash.sha256(0, filesize) == "80517b66f5d28df06e141862e5ccb316cf616b82f8dc2bcfc506a7a65e0ce61c" or
hash.sha256(0, filesize) == "d3395ef1a38dc51ca114b4882f29a53e729a4c48a2090577e751f2eaac4a7f27" or
hash.sha256(0, filesize) == "e4f9bf323dcc06acac0174c164c36852a34a7d4c81f355297043d14c8ed77cee" or
hash.sha256(0, filesize) == "fba557d1ea30dc5810637b80408cc8d6491f33e5cb4def703f2b3413d476d93d" or
hash.sha256(0, filesize) == "b38d4e0e0cb062b0a761b08bcfb1e7bf9c655904b9f48332d7815c26307128f1" or
hash.sha256(0, filesize) == "4e440f5b8395f605b933295d2c8565a7b50445775eed478bc38a34f25b4f3bef" or
hash.sha256(0, filesize) == "b8a70013e02074ef65e40f9ceb419540d39121ec06a2eb01805f24e80f079627" or
hash.sha256(0, filesize) == "2f622d5a6134cdc02effd75cb70303471fea46bcdc70ea6f2b1eee0f6683c8e6" or
hash.sha256(0, filesize) == "5af5c4eb8bd69890b35cf09aa325738b7fdab2cf95cbd0f41ec51dcdbcc165b9" or
hash.sha256(0, filesize) == "3830c48c32650ea0dc476491345f474d23987b52a1c94a86a23f1befb383504f" or
hash.sha256(0, filesize) == "296744a9dae3d11cc2ee7a0ca9ae5c62940c7fa8e24c6eec8fab0161550ff9cc" or
hash.sha256(0, filesize) == "5e7caa2b2832d9cbc83eb5b678a8d803b692557366bc1070007e1c2e944cbcf1" or
hash.sha256(0, filesize) == "f19bcf3f38989787c4b67bc1479584337732eef644cfbcee83fc3cb7d8456392" or
hash.sha256(0, filesize) == "d8de9d67f945ce1b7a759a9d56f2e6172df6c48e53cc18fe32cf35cd3f9947e3" or
hash.sha256(0, filesize) == "7fb7b571c56778ca2e33cb0abff5690943ed8184e0fbb1c8fc61f6ff89bd0d80" or
hash.sha256(0, filesize) == "c8dcace2c920912998c27f396e8945fa924757f3a7596a2da6044c1d0d47c7ab" or
hash.sha256(0, filesize) == "00fa8d12b92d3daf376df5f5a3a955259fa3a8d8be15362c2a4e22df89d79e86" or
hash.sha256(0, filesize) == "934d2aea354d349ada6bc649631181e4fe4b3c6a219b1acb96913f3c97cf13af" or
hash.sha256(0, filesize) == "7ccb7f63df4247bbe86f4c6dcea7ed51a17e0d5eea9843c29d2ef05d8208067e" or
hash.sha256(0, filesize) == "71cb2c38db0c3696d250b1a864087ba3a33f6daa236e63dc4059ac17e895855a" or
hash.sha256(0, filesize) == "165d31d8ba042952b9d0b56cd24703de15154434cc65788a563febcd1619159a" or
hash.sha256(0, filesize) == "d155d0af73b3e86f42672714caa4391ab615c426a3e3fc44a41e4d125a06172a" or
hash.sha256(0, filesize) == "7f220b2ce522ccb9588869864bfb24fb774ee738dbb9de12204f417467fe601e" or
hash.sha256(0, filesize) == "b7ce8b92e0d41329e98be9b84f42e6a716f263cd24e302e2d795f7c0cb5cecd4" or
hash.sha256(0, filesize) == "3a2bf257d6c6ecb246abd6e50163f126ea4683ffaca7d20fbd861ddfa3dfe0a5" or
hash.sha256(0, filesize) == "99ddaa801c589e1f2af3e72380517c1221a1d3a91c6753e2024e39f1981104e5" or
hash.sha256(0, filesize) == "882965a5a042f18294522e73e0c8a8ecf458980189a177f4fb9546b2569a9dce" or
hash.sha256(0, filesize) == "a8572124651997c309a4a88a83dca8c7b12e2424df8f729779041e5848d9924d" or
hash.sha256(0, filesize) == "f05410be61773c8019254c58a2932d47da169ad47887fe14cc26a43b55399f17" or
hash.sha256(0, filesize) == "baee13a0f456700073ae6aff109a6eb736237a1257acd7a706c3ab58fca22db1" or
hash.sha256(0, filesize) == "b63fedc06d9e500f1a8e0da5abb8a9e191ae67a6018698bfcf453b96f519aad7" or
hash.sha256(0, filesize) == "27c97c12f697a19100c97e75f8f61fe75c2b995d904594c554a5b35681c3ace4" or
hash.sha256(0, filesize) == "cbfc4f69200a5e7ee96ae849cc47a5a7606da8b38b911b2d4d741f067611692f" or
hash.sha256(0, filesize) == "45d29afc212f2d0be4e198759c3c152bb8d0730ba20d46764a08503eab0b454f" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_RACCOONSTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_RACCOONSTEALER_LAB"
date_IOC = "2023-05-08 06:51:58"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "RACCOONSTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "6bfb2a956e4f0c9b6697d8198cd8db8538a2f31778d5f7f9a527b224e58aad66" or
hash.sha256(0, filesize) == "0b676a206b26be5c6aa5caa6beea20c14889f15cdc58d8c39c520807382a86d3" or
hash.sha256(0, filesize) == "f0ea6cf3417bf75417b5228b4afd31e5257f65f19d3deb39a39499c15a872c93" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AVEMARIARAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AVEMARIARAT_LAB"
date_IOC = "2023-05-09 06:18:27"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AVEMARIARAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "4d275403b2993bb1dcf4d3262a5a70b32c0caa04e3cdb8c236420a3b1b1855b6" or
hash.sha256(0, filesize) == "ab8ec9fc896b49e5fd66c8c630279399a15ee365d843eea0a20af9aa87c094a4" or
hash.sha256(0, filesize) == "378b2e1c2c56ab4c5990aff52baf0ba351d61ccff03db14bbd574f7d38274b0d" or
hash.sha256(0, filesize) == "7d68d0c59a97ebb368a2db5ebb2d4f09b804b96e9d638d879c774066920a8451" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 05:26:25"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "c7e7c62f3d9ba103218de8ab0961e23821e91df67896cf7d450f39f535ca94f5" or
hash.sha256(0, filesize) == "529bb27de4876e215a65d62f1166d244bf8d16396a4cae982af3900260bd68e9" or
hash.sha256(0, filesize) == "a830659cb5950a876a44c9cf929aa552fb32f772f625bd7406b86d05c7fdf0a9" or
hash.sha256(0, filesize) == "7e4f2a5a6801e34e8f36b4a1b10f55721a36bcf044894e5011d9e008deb93326" or
hash.sha256(0, filesize) == "ad3b7fbafb3b8778f1ba4cc5a02f9e5976bc93eef018b83dc99edd59a2360715" or
hash.sha256(0, filesize) == "5a224ab3b182f13b491469c57c336848721fd9b3205c7b7905b1d05e5d99aefa" or
hash.sha256(0, filesize) == "3427beaba3edaca6c83418c4a76d762c1236f8062032461f814ec7cb4043cf54" or
hash.sha256(0, filesize) == "feb3a8fc0da4f632c7aeb9a19460d04f5881f592b5458988dfa61aa0fdc00d7e" or
hash.sha256(0, filesize) == "b503c54e91d38ab1cd433d59d1d0148a5da44f00c3b60cad6546e89c85bb77dd" or
hash.sha256(0, filesize) == "32606aec367ff7a71d0af223af5d22075a6cebc9fb967e4f2b5903fdee682bf6" or
hash.sha256(0, filesize) == "f1d17b3eaebacbc4bb2bbbd958910c39391ba63cf9ff0bc7ecbc8875dded84b7" or
hash.sha256(0, filesize) == "4a9e0f48cae4c1ba4ca9eb6b366f5f2675bd7dead400ebc41b7961a2fd9981a9" or
hash.sha256(0, filesize) == "49869ab252eb66d84096e98a2dba2638560d9bf15c360f73135bb30b23a61108" or
hash.sha256(0, filesize) == "40e150ee28f63298efa1413383e08088101b7936e2d70dc450647c2b23c4bfa6" or
hash.sha256(0, filesize) == "2dca250a7b956a8c5cc87138f120738c0cfea3ce639c764c149644617a1a5859" or
hash.sha256(0, filesize) == "1ea9644685ac7dc63840bcd5320d5b9f6d8f92afee7fce135c8271d2bc726cab" or
hash.sha256(0, filesize) == "ea9aa031bd6fafe4bf83372545d0e5d83141c6892d731f360de36528456009db" or
hash.sha256(0, filesize) == "dae4a7ae7b88e0e864d2dac3ebabee86916b29c609623b754fbd253d2812a491" or
hash.sha256(0, filesize) == "135e204ebe9c5fc699eeb7cdb425d2bf184ccbe5a092bf792bc6be37d865c7fa" or
hash.sha256(0, filesize) == "eabf4231e0b5fbe98a97c140a691b79076ac1c4f9663181567301cc307771025" or
hash.sha256(0, filesize) == "ea6ca8b50b5fa2bb41ccca9076020cc9ce9e53068fa85cb1be11d1ef1a3c591a" or
hash.sha256(0, filesize) == "6b0e8e6683df482c7579f4ba45e062d65af78a2eb8d310bdde706437dab53904" or
hash.sha256(0, filesize) == "fa06c367eea3dc7496e8b8c3baf34ed7775518d1b779d041f41b0a4b92d4a922" or
hash.sha256(0, filesize) == "f7953a29e021758179e7dd133785404773846f2cfb56c178b1da6ee542ab5fef" or
hash.sha256(0, filesize) == "fa838dff56df25103842ba40c7e130f7fce0541a5318c935db67f472cbb35669" or
hash.sha256(0, filesize) == "5c98f2049c63db553be8fa363503970a5dddc57966148d123c6b12ca17dc9838" or
hash.sha256(0, filesize) == "42a1b1333505c88f790a7fbc5c39856f94cbddaeeffa5be29ae38fb5f567350d" or
hash.sha256(0, filesize) == "32758fc5150d78fc2eb377d6a3468e199a6d2f3222070c2a7f72b90bfbd95759" or
hash.sha256(0, filesize) == "d92a021eedafe2b1a34852c5e37caf7a53bbc81eaf9b493acd529aa268d4a033" or
hash.sha256(0, filesize) == "2c78017d8001429c9377a93a0e0740ac8e6b6e4b7beae7c70ab2c4a367acfb42" or
hash.sha256(0, filesize) == "b9d37d2bd06ca6ea4cfc72d49f30ffab897351e19f4cd31464a2a3314ee642ba" or
hash.sha256(0, filesize) == "f58fea7363083d8ad73358f871c6483ab17d804ce34eb6558c62b3350ad368df" or
hash.sha256(0, filesize) == "fce5b9328c9d4ca0ee77a1f674ac9575c6362c78ba047df67b4704bb8ff802a3" or
hash.sha256(0, filesize) == "cf7ecaebf8b1d31a0d5ebaa74b29ec6455d2ccdcd0d1d1587729eee498f41a4f" or
hash.sha256(0, filesize) == "1f4688c6fcc7bd15697a9eac29e89410a97ee8d3be8ebe10d5290843470ab66b" or
hash.sha256(0, filesize) == "a782d89a06f36333a3e7370c6b535d5dfb518edfed64e660d989de3b53b207fa" or
hash.sha256(0, filesize) == "05451697650c572c6b41802d4dd8b1043a6e3fc89e47af247fb88f152dbe4072" or
hash.sha256(0, filesize) == "9e1bf35e05cb53c6d98507f56241e4759c63fe9000a5f787a9796ff66eb24463" or
hash.sha256(0, filesize) == "035148bb0d0fd47a963bc28d1725e4e5613bd2a077e745a58a1481d26577dd00" or
hash.sha256(0, filesize) == "2fe7d14259ba7b5d9b82ab7494fbfa3675b3c2f2cc83cb7e5c412e52d28303e7" or
hash.sha256(0, filesize) == "9021a3505db83100e3cb5a4e6d19becb9387ba4b02576cda9d6cfda3387ffb4e" or
hash.sha256(0, filesize) == "e6d98d839dd2372b008ed4ec970c951963b863f89959d2d917cbe32fe7ce7f21" or
hash.sha256(0, filesize) == "1db6551d7efb2ebfad70efe198dea03dcc6fb41bab1f8297cbbedc80775accde" or
hash.sha256(0, filesize) == "6e6fbb8f5874a70451963264d87adb2f6aedce3c02d32881f4178e245c134537" or
hash.sha256(0, filesize) == "84c7973871a6a639c40534f9e3fd7135bd35cd1b0eee937cc849af90c903c4c2" or
hash.sha256(0, filesize) == "c336b24aeca365ef88005a1bdf4daeee797b2ae2c0976b3db1fa4cd7c9290b87" or
hash.sha256(0, filesize) == "5644a5e00ee1418ea4a41d962011154eb3e546143cfb4140d4e72953b1b75251" or
hash.sha256(0, filesize) == "4d893a17ab2385422786d079adc3da605566293b45758380306d5146f58c6fed" or
hash.sha256(0, filesize) == "55a64db52aa28afdf1a222db6a0a74f92a3585d6526cbb015488978aa1992161" or
hash.sha256(0, filesize) == "1cd323bfff08daf001361fb642f30bf06ccda8d78d01c77727d80774a283b269" or
hash.sha256(0, filesize) == "206c85493018084c42f4a2694b349475b1ac95efa91d149f61825e12a508e250" or
hash.sha256(0, filesize) == "feae31de8c6c8cdd42411db959087e258aa5da132edb1f34cba5c13eaef56cd1" or
hash.sha256(0, filesize) == "9454da092866823747fb0fb7e5b11652794974fad0d3fbab3f80db4ff97e4654" or
hash.sha256(0, filesize) == "ce951f9946a66af4cf461317865d760231a083710a35ae4d2ff362201ec66966" or
hash.sha256(0, filesize) == "c1e3b76bda3518d5b42ef7e7bb24df83b15fb3d97632ac0951bd814a1efdf246" or
hash.sha256(0, filesize) == "2547bb68679d447dbde8f5631c385ba265aaf1dd9a8b3807e77ad475ad15adfa" or
hash.sha256(0, filesize) == "a86d1dc8abd7cdd7ce8eb34ee9e739c35ba49d2a4a4c9d62d0da1d2dc14addf1" or
hash.sha256(0, filesize) == "4b81215a34d41cbdfa37fa7b93710ed6ddaea795f793740c9da1fe04366893b0" or
hash.sha256(0, filesize) == "8a41b79f891041bd386d0886027e3a214bfc1597f5958dd897553612df87dc30" or
hash.sha256(0, filesize) == "131a734d6ff364c64bba02d1e27843e78223bc9c3ea917cfd568482c74f23707" or
hash.sha256(0, filesize) == "1990292761ec740bbacef00eb11ce5d812011391813723ddab04ea0246249625" or
hash.sha256(0, filesize) == "91dff7bab30d0d6713ba56c522859ce8f773611c10ec7a7e7a0d9889612d0641" or
hash.sha256(0, filesize) == "8d83ca472b9dbdfd4ee9ee267f02174c18ddcd3cd166ce3bb2f2fdbc50f328dc" or
hash.sha256(0, filesize) == "f5eec7db443f930140c0bb15e7f5867a036fb32a5adee722b2ed6013925bfd8b" or
hash.sha256(0, filesize) == "6e5c020da66ea1b65b6b339dbc32987746a48a112bb75317808ac6c77777f7e7" or
hash.sha256(0, filesize) == "43a5329fe9f6d43a4de5875d0fe039a056f10a1887a52ad0acd11863587b9204" or
hash.sha256(0, filesize) == "b17834b8b1ab830ad583d159929a01013cabf188895657bcfd44c4fb95a12258" or
hash.sha256(0, filesize) == "707d570828f75bafdc1f7b8272a0c01154b51ff29348d6cf0d8533d26a9f25b0" or
hash.sha256(0, filesize) == "74037b2fc5946e17cc71304c5cf248879e4e6aeede8e0ef7677f232ab6d8fdac" or
hash.sha256(0, filesize) == "1d0952c3ca806e58dd50cbf10364474624d16351a65be323d728574ea6e9cfe6" or
hash.sha256(0, filesize) == "84652d3181fc4dfad881b733e96b9181c2aa8bf5d3257781228613419f7583a2" or
hash.sha256(0, filesize) == "bad91ee1b8c242bcc2f84bce17fe357e8121ea564cb62c4ff96e0bf52f1a06a2" or
hash.sha256(0, filesize) == "07908541bef6e13c0cefca9e55c7b629dd506d3bbbb1767623b5963fe0125e79" or
hash.sha256(0, filesize) == "bdfd3a90007f6305a48bf0297b5e0f9015cda1d82b1cb90ce6627bd9e7bc16cd" or
hash.sha256(0, filesize) == "36cb70021e8a153c22312876626b4143dc1cca60862d667d2af0c95a1fb9e8ac" or
hash.sha256(0, filesize) == "2f2cc821895c4efdcfb12bf2eb8013e5e4f686b2b6e5fb0325bc46567b561782" or
hash.sha256(0, filesize) == "e15f9f1f3757325a4af0c327a602598d1f52fbaee85a0a1e370b7437ac3e9ebf" or
hash.sha256(0, filesize) == "b9d4bb3f31579d3d087081446f7afe01b4e525a5770e99449256f8615aaf86c8" or
hash.sha256(0, filesize) == "a3e624899642025593a5646ecff5cbdf946e5b13debcdf57aad589d4da03de3a" or
hash.sha256(0, filesize) == "2ce7d781b4110aea5d90e86a8798e44516a33724c212e4b6099cb4babb7090d0" or
hash.sha256(0, filesize) == "ac4f95e274427abe5af52a9af50ffc74db27a0c87969d1097dc35d75d36d77d7" or
hash.sha256(0, filesize) == "6f87506ad54bd39f08b8f3da216f4f31713377aa512b32628c2ef53162b222ff" or
hash.sha256(0, filesize) == "ccd330005237339918c1ec5dc3bc253a2881a6b14ee8502e6a0f70b4e5847e47" or
hash.sha256(0, filesize) == "3d67dcad5026beb5c577274f5b8b01470485f74d26ffce21428ddb42359c388a" or
hash.sha256(0, filesize) == "49a15b151e2ebc7d1d3e4cffb461cfd3491bbd5ac90ee61846ee74ca9a7e3569" or
hash.sha256(0, filesize) == "3fb98b057620b6aad473c5e6a67c38c601d05272db5128b108c77c22cfb2fce2" or
hash.sha256(0, filesize) == "2a232d34df6f69da4deaf5c66978d63ed8de80b166c57f1b48c64dc35718fef5" or
hash.sha256(0, filesize) == "00b4678b94d884d5638bd270ed0c42f20697ebb1ba2746d14b45515da43bd3b7" or
hash.sha256(0, filesize) == "d67557fd37174016e188f4e3932341f74c0a79ade0a34fbffc190e91b9e5a0e4" or
hash.sha256(0, filesize) == "f10dd98b11b51294979160959031a2087f00361c546d945d75a4c2fd7fac6c28" or
hash.sha256(0, filesize) == "3c4d4ae6875508403d1c24d4422c588a14cd0515d7c11a388e8ea6403d03540d" or
hash.sha256(0, filesize) == "1c3e05c1dc57b82fb25bb8af32bad88ef17a5078818598c30738675ccc4729fd" or
hash.sha256(0, filesize) == "494979f4e2eca84b240d774c80ad703ada9db1f3314d6431bdb6228045c7e2b5" or
hash.sha256(0, filesize) == "b5d3c7a0a47dcfc26d108e8ab87d79a33955bf53c66ceff7f60f309df2ce3353" or
hash.sha256(0, filesize) == "c8bfd9368340e6e057d18ac8d45eb758bff6fadc9550711d5e25e9f183cecee4" or
hash.sha256(0, filesize) == "8ab970bd939d38f2d99d43e9d831a4871ad22edcb0d1287d3782a432050b30c1" or
hash.sha256(0, filesize) == "8838925ac19a77ed2817ed8e52b6b9db02d58987dd10774eac576f839df1c5f5" or
hash.sha256(0, filesize) == "d4601263f30743332a7f29149b89adb1b59f3b12c38ef70b9c5fe6c7c8a18faf" or
hash.sha256(0, filesize) == "290900426b7f0450c34ffe8152e39484e70c6bd90e59958524074c70e3b19290" or
hash.sha256(0, filesize) == "8c307fced5146d81f7ec8c083337d3c7e8a92ff09a05ac20bc4b4efea7826821" or
hash.sha256(0, filesize) == "5777e088b6df69cb6a38e324ff037a955ecbe8cd04a941407ebf7c09c51fc4ef" or
hash.sha256(0, filesize) == "adf7ac344717cc3b2f0a8aad0f0d9aafbe719abfbabbf6e4f82d13352d73dce9" or
hash.sha256(0, filesize) == "866e30140fc553f94493a28cae2b615924240cba323fd08f95ee0c716f28ee0e" or
hash.sha256(0, filesize) == "a3928afcfa8b3e1825c9796ea9099e59e1e7ae2306fbfcc20dc3aae7c6065121" or
hash.sha256(0, filesize) == "4629b5912131c5a9c21c3ff2130f9f021398c15ebb66857ee543a55a076f5c07" or
hash.sha256(0, filesize) == "66d310e8f9cdfda5bd0f690a822444429036236846daa4dde72739ff3ed079b9" or
hash.sha256(0, filesize) == "1884b7ff2246263409d8e8d53d03371c80affdc9bc588455d0b86e2a77c5cbf1" or
hash.sha256(0, filesize) == "afabd885f80a72d339be30cfe7dc52e6cae848fe7cd1e371361b7d6ae2c726f6" or
hash.sha256(0, filesize) == "8d9ee26ff16ba2d8a4aadddf38b00e02d5b09b584fc7743609c594adbfdb68b7" or
hash.sha256(0, filesize) == "7a030c6ef58fc59c941cef4dadec7d5ce8f1b0057df06aa9eef9fa84aeefa586" or
hash.sha256(0, filesize) == "df4865204f2a95edd8ec606aa34b61932584193dd64c7670f4cb6ccf44c16c42" or
hash.sha256(0, filesize) == "fb012706e028ca770d05ae4580045de7450d40b069bc8f8a250e292746b45b90" or
hash.sha256(0, filesize) == "b153261c18823db96bfd055e398bac3f7c3853aaa006c2017dc17d13e49e23f1" or
hash.sha256(0, filesize) == "bb3167924a5576e7b920d9f7976452b90c6c2674f08e63b0ba658d49eaec20ac" or
hash.sha256(0, filesize) == "8d1e9f52f24cb93e8030726b63b48d43d90e18390b3571f52f9326964e5de23d" or
hash.sha256(0, filesize) == "468d21c12e3590ec0a5a97a2f515878192f6beaddb4a721346890218af827bc6" or
hash.sha256(0, filesize) == "e98bd4ed05c5d8f05861357156e67cf2eb1ece78e082e39abc9db81be3a408b5" or
hash.sha256(0, filesize) == "ba3eb4a8f3d82525829a03faafc5324d90b4274ce40df20dca51d3715f8de848" or
hash.sha256(0, filesize) == "8d7cb3ea332af3f322d29710f6a59aca7be468a27082c341e20a0aae49359e23" or
hash.sha256(0, filesize) == "1136a7337b15d84cb01ce91fe74e0be8a7e1df783243cb8c38feb070aba7c54e" or
hash.sha256(0, filesize) == "c1bcade2701007e8f761a5c6b8106fb736cbd36cf08e4f58ec7137eaee1f09ba" or
hash.sha256(0, filesize) == "ad7d0cc457d1c3143cbf3e7d9c02cc61ca1a38aa2161168be3905d9948a8c72c" or
hash.sha256(0, filesize) == "8743e0488e2396c788528f3febac8b7ced15f219750753231918a2b67a40ca8b" or
hash.sha256(0, filesize) == "1302dd8497ba8909d81dce0a4069767bc358b0879443c1982ffea0292f9707e3" or
hash.sha256(0, filesize) == "0fe40012d9bc6466173c5860fea01e4b7f4390c4dcc2fb95aeaef466d32c8904" or
hash.sha256(0, filesize) == "05a6e74beef92dc711c25b3b01565f9b1a36f08f914567e467e5a47dc2d1087f" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_BLUSTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_BLUSTEALER_LAB"
date_IOC = "2023-05-08 07:01:33"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "BLUSTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "2bfafdc20b461ef574d77bd7c29d586c6a7c3ad6b3ad9bbecab8c014308b07d9" or
hash.sha256(0, filesize) == "dbf75a247c2141d83e39f08a304bfde553e4e1553bb0524721bd4c8b189ebb02" or
hash.sha256(0, filesize) == "374bd46f94cdf56eb2775ec23f0b70e8179541f348de2959a4a885b8f22af99c" or
hash.sha256(0, filesize) == "b092e2388c1f790870d97b73440930d4e0c134610c17091e8987622745a0cc48" or
hash.sha256(0, filesize) == "a894ab5bc1a3a77398b7c8b154acc165d9dc5e4e183e573daa8dda6c969d58f3" or
hash.sha256(0, filesize) == "2a84cd5f54e03590ad92e86ce6618dc5c31cb290e0845bb5de357f8c92af8749" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GAFGYT_elf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GAFGYT_LAB"
date_IOC = "2023-05-08 05:01:05"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GAFGYT"
file_type = "elf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "2273ef203aebcca4f6f660716acc47ff5def38ba85f790c84381f04e1212b5aa" or
hash.sha256(0, filesize) == "1a239fee16ae6f51bf808207754eb74605880806541ad9f819dfc208a758d7f8" or
hash.sha256(0, filesize) == "881de044fd57afa88ccd2b5e41cbd6defe92ecec0086535c11200f941c24cb0f" or
hash.sha256(0, filesize) == "c2dc4bca07f217eecf4dc50e46bb1becf1941cec4da030de6ff2301a0e941925" or
hash.sha256(0, filesize) == "d0c5e409fd220353197f52e2a91de61a7357f7e5debd53e2eb06e6ec2c34b467" or
hash.sha256(0, filesize) == "a30cb3cace5c987037b58943ab419bdc3a9f9ee4f4c5529b3b7002de130513da" or
hash.sha256(0, filesize) == "94a7beb2d03227aa404988abfde526fb9d36d8f3da6f8e68b998506e111905af" or
hash.sha256(0, filesize) == "276cd734d3903733a027c6309c63ad7f795e78b627fc9489246e00053ac5cbef" or
hash.sha256(0, filesize) == "2c35d0bc456774abfa3e173d4537654ee268773c7f3379d46b8e5a8e0642a736" or
hash.sha256(0, filesize) == "1df17eef596bde698c42aa9b64782cddc13459cc3b850f6e8a461a1f2609f019" or
hash.sha256(0, filesize) == "a03a9cf42ad36e47673b0ea1bd06b4f0257cbce5d793381be7cb1674f30cf892" or
hash.sha256(0, filesize) == "34111d2767cbfe0f6b168dc1e1438b08eef8b583fcb8fadb9cc9fe823997dc6d" or
hash.sha256(0, filesize) == "fdefd44b6f5ac57528163af24cb2e4d9fecdad180f252fb677228896faea31ea" or
hash.sha256(0, filesize) == "93b2fb60ef28f0d556de3d3f47789e641877c389cbd8b1cfae5ceff2db3646c9" or
hash.sha256(0, filesize) == "a82d9973b94775a22a63b0950482faf242f8ebb2f4766fb3390971b151ce245f" or
hash.sha256(0, filesize) == "f72382801e0b77a6f5e7c0c657c7e4257e423ff2dd28b5182cb93885bd121cfa" or
hash.sha256(0, filesize) == "cd6dc1691f951e4a045783a83a44cb52ad4455729e2e08215a1e2dc4bfbddd2a" or
hash.sha256(0, filesize) == "3f40ed87dc0052881ae3db841936deb4d41d2301d87e2e8a506d1f14f82269f3" or
hash.sha256(0, filesize) == "fd2c0eaaa0978b3fde44ea05b31116abd55a4e582209aa2888850e5b5145ca86" or
hash.sha256(0, filesize) == "17fdb16fc4312428d82c8aeef4a5f6df16b8d6e1c97c35fcd3bbf5d1ab7e4d02" or
hash.sha256(0, filesize) == "ce3760474f136ea0d77a0751c17c0368a089f2fd0c128e878fbaec5db822df13" or
hash.sha256(0, filesize) == "ce111709cc0ad487615e8484f84a14a4d4b3b457336c5d766382264629f698f3" or
hash.sha256(0, filesize) == "75c0dd632ce9b1397d9e7faa7351bf6ded47c50d688f1f96ba278b9b3bce4396" or
hash.sha256(0, filesize) == "20f406e59b8ed724d0cbf5885f7b828e69f8496ebf065b9dcbfedc01d741789f" or
hash.sha256(0, filesize) == "9ae7dce519a0e200014fc623d5af91dd106820f104d7b6ff9a7fe599a6701e46" or
hash.sha256(0, filesize) == "4387c1be9255ee394e49a49f2f302d3e0def1b397f3b3515abeb2c88858ae506" or
hash.sha256(0, filesize) == "ccadfe530835090afda7be358411caa8465fcd924eb3aea46a6972c76d405dac" or
hash.sha256(0, filesize) == "04bba167668fb635dfb00c6e829e5bfbc90920d03678ca00096785fde7d59b89" or
hash.sha256(0, filesize) == "2f845886e5f42f7ee6b21adf699e1b7e6b0f679ee8c9141cee4c94a52f8005bd" or
hash.sha256(0, filesize) == "81f1431ad4d86e70e0be85a0c875c15f8716a40e33dffe03ec06b3f36515a0ac" or
hash.sha256(0, filesize) == "659838bed2be65c437f228d3ce7e6aac413ab6b99762137162b43472052c69f1" or
hash.sha256(0, filesize) == "b6749c6357b1b627df2f096461a0e68abe00ffed6e8c133b828549bedf5cc4d3" or
hash.sha256(0, filesize) == "bcf6454ddb0c495f8dd6956ba6888c2769c5d21ade63d653ce32ca3261bfadeb" or
hash.sha256(0, filesize) == "6f1885f22875d7c7c487d3118f4df36bda833a71be2cdda113b44f65926807f5" or
hash.sha256(0, filesize) == "1075b1c6fbb36dbe94c275d43c4037e857187f7d0af3fd600e7194db5bb20f53" or
hash.sha256(0, filesize) == "a6701b03f2c3bbf423336ce8baf6504da9a1760801a89a3449ee2d6ee7196e02" or
hash.sha256(0, filesize) == "b4170e2de823b4e00db8ff66e14ccb05271d01634dc9b9773b39fa2a45cb8708" or
hash.sha256(0, filesize) == "f8f2b2f71133b706578b94371b9d2b2551b8baf285da9bdfb942cde4870fa3b7" or
hash.sha256(0, filesize) == "d5fc3a531b433f4de47f6c565f774dc3ff7c98ddd86d51321af466a546451443" or
hash.sha256(0, filesize) == "83ed801c4ce0552edb6b7a21a0e8a6d881ccfb39799fa25cf399a2104ac68bf0" or
hash.sha256(0, filesize) == "3c6d304487038dd1a72d4d89b50e3159dd4385f63c6c238fd8a8d09d931b8f5d" or
hash.sha256(0, filesize) == "02524e76a81250423517bbd962d33b60b879e3d29ed4900d6f744179e66d0fac" or
hash.sha256(0, filesize) == "fc1c0a8eaf1b798c3e72d4842a4bab10ddb55db44b9540ddcaac5954b6a20e5e" or
hash.sha256(0, filesize) == "0e191a8587791b01e1f6fbf2bc6c99febfb19a06945577abf6d3b374fb74b325" or
hash.sha256(0, filesize) == "1b1c1e47117c9196cd1a6a086541628fdd2ce3d694a4a6a3d4e3a270eef4ae05" or
hash.sha256(0, filesize) == "3f5e3db88b6e8fbe3ba89c5376674077a1cfe00a85cc61f8aaddbac0d4c29683" or
hash.sha256(0, filesize) == "dc87ea7e42d469b52cc28f78b9b45b0c974f19f25484c0f84fb6abe95b593564" or
hash.sha256(0, filesize) == "1c1374614b3433325b9756cae61777c06eda2395d0ac3e513b016f2bc09c2e05" or
hash.sha256(0, filesize) == "24124a04e21f08ba69090b0b0bbb667d5fa4a0eb2828930067e9891cd82830c5" or
hash.sha256(0, filesize) == "987e111a434a46dd0cf734a983b0e3f238166ebe800f25de15fe613516b75204" or
hash.sha256(0, filesize) == "83f31acb3715d3b70b5714ce80c1f6f44208ea1bc1adc1e97e768c1147c2f6a4" or
hash.sha256(0, filesize) == "e9f314df59f021a3ff84d73aad71a488d676c44cf521e7951c76b9fc71bdae51" or
hash.sha256(0, filesize) == "9466dbcc980cb8029e8d007ecb3d462def505d0487a004661bd7405895a3fd93" or
hash.sha256(0, filesize) == "44885431a45d3f867466c22a081ab4711102f919c9e40cc508f9892c5009857c" or
hash.sha256(0, filesize) == "afc6fe19fb874b3a322266e3ff1c5c1f1a44298ceb499cb2735e5638b63f3b49" or
hash.sha256(0, filesize) == "16f3472f5b8a6c0eeb0e89415f188ee44da9b66cc58298bfd7e71fd6d8fcc1b8" or
hash.sha256(0, filesize) == "cf6245e3240281a1c0d2ce991df6ca6c3c9ac06b34ca110ff2e56da91f94fbdc" or
hash.sha256(0, filesize) == "649592adca3e5101328d9aa423c115f68640f044d268839d4488e930c9b4296c" or
hash.sha256(0, filesize) == "bb78e6c828d241b87c2f45151ec64f21075fa2c7655646a1397d40a376dcd0fe" or
hash.sha256(0, filesize) == "3ed568821de0658d8a11a31fd1d75f55910009387c1ab1305a55529b2938544c" or
hash.sha256(0, filesize) == "f2a91d6453e0bd1302696ca27b730fb7acffdd12e8b5899ebf3bee65983b7f6e" or
hash.sha256(0, filesize) == "e0b4099f319b0ed63b2b20a29f73423c256f6dcc18b27ff6477632093d3f0a67" or
hash.sha256(0, filesize) == "b45e98ce0bfe48a2b2dd5f9a985a1449d8bf4099dd5b596cb1d053e32de8947f" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_MIRAI_elf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_MIRAI_LAB"
date_IOC = "2023-05-08 05:01:10"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "MIRAI"
file_type = "elf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "9e1beeb0aacf9a131ee133101dca738496f7081ccb8bbea5e6ce8a79ec7d5b45" or
hash.sha256(0, filesize) == "4c97d10d22fe3d5a6d49e0bb29d215faeeaa036d09443dc296b7fb075baedbca" or
hash.sha256(0, filesize) == "232ee34254a2f9fe496fc72c15af2841f959ae246d7879b83861d7cd7718a92c" or
hash.sha256(0, filesize) == "d957c15b9ae0846d1fa1043a65d94cc95bb4b69785a4d1defd269c890d896b1d" or
hash.sha256(0, filesize) == "3328c40d86f9cdf681e5be9831f8f93affa1280d643da3d121f576d369e6372c" or
hash.sha256(0, filesize) == "ad13f5087a1858717c5ff7f43af25d0e4275e4c305579bbb090d12db665989a4" or
hash.sha256(0, filesize) == "f14c391aab5f27f48c38542dcb89ac7d171eaab225a8e10f49216b77f8c823a7" or
hash.sha256(0, filesize) == "8a21e7d17535d9d6cab0d17a384175b7e1790495a215b06150fec7a08b2ef069" or
hash.sha256(0, filesize) == "725897a2b69030d40ce445389bddea415a2d6a96b30ee3ae57772eb480d49493" or
hash.sha256(0, filesize) == "b5b0771cd7476ae15544beee49c40f63669f34dd9378342f31891f5d947252b6" or
hash.sha256(0, filesize) == "cd91ef0bb3a780bff671dbbc521e030c25a659aee25c22032629db8507406210" or
hash.sha256(0, filesize) == "0d31f12224dc63a9cb686f01abf0a4a711c9f5c70ce5668c03f94b525e6f70d2" or
hash.sha256(0, filesize) == "71a72a52e687eb23f58b70a10932f56155d6001c88d76ba04c3db871e525da03" or
hash.sha256(0, filesize) == "7a73db9dcd8d7b23e66786a2d6e14678337bee853e646fe39c617b30c0835201" or
hash.sha256(0, filesize) == "9df9fe62071b0b549b917e5335b7dc060e24e8298b53ff3faa7bd1fb2b7ce704" or
hash.sha256(0, filesize) == "dbf08541611ae42ee029374a8f26d2bbbdac419558b1c51622d03e06c0246115" or
hash.sha256(0, filesize) == "8b5cf6106d5d0e7b8384369a5fe145b7e384872b4f2ac15213ae7c4bf1f57a8a" or
hash.sha256(0, filesize) == "1bcbfb8e255b6723bc302ac81886be6413010d37c62fd2f76126322b880bf0b9" or
hash.sha256(0, filesize) == "2eb3c83d32c0795b95282ca99c2f01042a95ad18264c5f3a6a7dbf08acdc1a35" or
hash.sha256(0, filesize) == "790cae08516ce0c1ccc8f761887e947d01cf8127e85e7776c855e0912973d95e" or
hash.sha256(0, filesize) == "0a3080b9066a4388584665610cb685f440b2d7fbee45b660d1152e677e40af6a" or
hash.sha256(0, filesize) == "85d5473faf7a8cf9e059b8db1f30a10881ddc9ba30576a0b5d0cd2eca729acb0" or
hash.sha256(0, filesize) == "5a5b5c2a3fe1cd4fd5607fda62e2ce8e575278881d6043fb8b7f94f76a59cea7" or
hash.sha256(0, filesize) == "2aa78b480250f6277edd45aa7c54692e36a90d5d06f55931e62c330f92251df1" or
hash.sha256(0, filesize) == "d3a8457e0819faa49875d59298bcd00a4c50a55f27fb717672fb04bf06db4801" or
hash.sha256(0, filesize) == "a3d4a869c75914403f908d38e94ecbc891767638c9a98ee97b3feb03a0ec09a3" or
hash.sha256(0, filesize) == "bba2ec7451ced3415773c5504f88a71aea75f81ee69981c1eaf7bafff38e8297" or
hash.sha256(0, filesize) == "fc536dbf0b14a0829d99fae69e6cf809a0bdba51a444b0f8dda83036e9d78d24" or
hash.sha256(0, filesize) == "96bb1e69b18966dafc679a4a1281f6e954c8461b2e2696b06dc1c53b91bc6847" or
hash.sha256(0, filesize) == "55722ff56d4c70f60df25678a70ce61d58568acffe9d55f8867b516359c0c80d" or
hash.sha256(0, filesize) == "e1b9b69102eef182dcec2a50a1f9a8f8d1734bebe1d1316d3b3dcc4b2a9e86af" or
hash.sha256(0, filesize) == "ef7b4476d67c9834415365b31a18202589e1176d9a3493c1312bdb9df3163ce4" or
hash.sha256(0, filesize) == "167a6e8e480f15fc39895c15f2f200506d6e5bebcdd5bda05419d83fdb3e407d" or
hash.sha256(0, filesize) == "c300bcf277d247cc6db37e3aff2b9ca051118d160c8cf34757c28a8fd80a6693" or
hash.sha256(0, filesize) == "ca5bd5d8bf7a75c7e9c4616f5ccea8d6a3b7234a3fadeb69132f8f379825adbf" or
hash.sha256(0, filesize) == "610d0f9bad75e5674d25172529510b2b142697f680647f0d30f00651c338fff7" or
hash.sha256(0, filesize) == "83dc12ad088c281b395be7ef366c458a0143d6d659c439c990263fbf8ad82ea2" or
hash.sha256(0, filesize) == "129805381f005a6bc02bad7a8333e38af9062218763338c1ad09aa10666dde5b" or
hash.sha256(0, filesize) == "8d75b881a14314f10ab6c4257f18faf222354669b7c510ed8bb3a8b02613a646" or
hash.sha256(0, filesize) == "a9bd8c2dbebf1ced0b61534a1e79fb0d53a36892e2b6773a9f0fe8470f73099f" or
hash.sha256(0, filesize) == "29a5964a16f326ea181f9cb881c3702d9c56b3ec5d8a5f328966b96f8f34b1ce" or
hash.sha256(0, filesize) == "ed972fb2262a035a56dfedbddd93667c93f0f992fc55b12ba779430fee3e0265" or
hash.sha256(0, filesize) == "0a82035a6c75d79dadf1a51502bf310121a82056203dd3881e17f0317c23c791" or
hash.sha256(0, filesize) == "9503582954b7651ddb26b1f9fba92e397726c0511ddfc3cec3fa0dc49215a57f" or
hash.sha256(0, filesize) == "3f9f082ed34550ec31566853b270d78539b39c0fbf1087e364ee07f2c7b04ff9" or
hash.sha256(0, filesize) == "cecd67f335894186e6725fdbfadee703223b56f29c3bd312157cb710b494165c" or
hash.sha256(0, filesize) == "c514bf5e71a5420332693dcbbb0a9963b174a982df75e189b8b6a9804c8881a1" or
hash.sha256(0, filesize) == "e0a7be43503ea58e35926a3a4e33811b9288375ec784d63e0bb689cd56c44090" or
hash.sha256(0, filesize) == "8b409d5bbf1dfef04108ce307dbba3bccff1573cd287e12732bf6b9d576af893" or
hash.sha256(0, filesize) == "e2ff7b616289111a5961d9a49317abdf3d0d419b809c8b68b9171c49bbe3b99a" or
hash.sha256(0, filesize) == "3584c87c0fd019ecb9f7d894811b7cf81a515e1022764da504740fafedd23ba1" or
hash.sha256(0, filesize) == "f7d3a44dff06155f8bacae471204aca8b755bb7b15381bb2d97b46d2dba23cdc" or
hash.sha256(0, filesize) == "a81c74c42bcdb89aaa2b7c0092f49bab5b09ca46c971e1eb508960274a6fb74e" or
hash.sha256(0, filesize) == "52cc2a68202938346bbfda022560975a35c7b879975bab2a1757ec538cf5a507" or
hash.sha256(0, filesize) == "7a587ed492cace51d98334869b973753ffddbd53b08343268adb654fb510ab97" or
hash.sha256(0, filesize) == "3692115fd0f87d2680c9376f2d7e4873610a50736ba243ddf168a5cdf183c22a" or
hash.sha256(0, filesize) == "a3269778324af8b8de5c549dcad105b0f91f1ea5319d4362b08105f18f502d7b" or
hash.sha256(0, filesize) == "b5097af7468477ca809168044586defe209b4529636adcffc3b8bf223527cc83" or
hash.sha256(0, filesize) == "522a3ff52c1e23e7f36e8e140c4526ff94c561eba0978e524c6b6816ca944ab3" or
hash.sha256(0, filesize) == "7467344ab7d2419858a436a8960ad82f9ffe373875b4ade765fa164f93069e7a" or
hash.sha256(0, filesize) == "e8e5b5a64bfeae27c83106d701a8c181ca8edfb5abdaefa278141dd9f3dc1301" or
hash.sha256(0, filesize) == "9ee6b06d3e7f2b565f51efde93cb3cb64e7d7ce2effa98f337e3c311ffe5fa58" or
hash.sha256(0, filesize) == "1d6ca15ce9e039726f1bcea0c5c0016430af86e9830d231426a0dec723a4c7e6" or
hash.sha256(0, filesize) == "c34758ca44055600e5709939ca4d153ffef3b03145e26b3d0386384ffcf9a967" or
hash.sha256(0, filesize) == "a278ce757f096097b7c1b5a3c5e24aab628eb07e63f85a03622d703367c005e3" or
hash.sha256(0, filesize) == "0840d08affa9f035a001211c251ab6c438cad32b01f0d99c7212f09e470f96f9" or
hash.sha256(0, filesize) == "49868df3ee08e44d827b66f42bd8d2c5c174cab0e513de0e3c61858c4cc1432a" or
hash.sha256(0, filesize) == "0a6011210e6a16d54f60f388ebe38fa3e253b4e039d23d4e4e7c91069eabfcdb" or
hash.sha256(0, filesize) == "f9b2aa92ed311468d817a5fcb7d30e490dfd9f1e6450d06d272e97babba162d7" or
hash.sha256(0, filesize) == "e7dc118ec7e243c1eba9b98b5edbfa0d24d2adf48e9c9efe7fa8ea8e92ccf70e" or
hash.sha256(0, filesize) == "2e7656a610c6f4572541dc2a39056d1a457918626a2d8507bcdc0b38c4bc1a9c" or
hash.sha256(0, filesize) == "7e87b1212af041512ee9f594c84b9a65699b76249561dbb321dd2d126331920d" or
hash.sha256(0, filesize) == "9db1d4a59bc577efa4769790fe711344745bb9c6c433dd4d500c03f226610063" or
hash.sha256(0, filesize) == "665dfb2324c8eb4962f6eea85a4ec48c299a6b5018256c2d9f3a0349fd1ba669" or
hash.sha256(0, filesize) == "77ce3489b0df5c52bb8e3ca6add5a507ba8058c26d8a7235e19d093fafb8dbc5" or
hash.sha256(0, filesize) == "4a15bc2a7f9e08d7f9fb7825cb78aee115c065e0f40ca932702cdd7953a7d84a" or
hash.sha256(0, filesize) == "6ad801df54cc66f292e1d3606dedda58c438bfc8871543afba8ef38b8a58eb2f" or
hash.sha256(0, filesize) == "da9c35337e4411d969573ef09c6d7bfb31746ccb541f8672077c3b2f9e2de71d" or
hash.sha256(0, filesize) == "a012e1d0844e22305ca573285d481d07781a0ea17f679887efeb5783e039f4af" or
hash.sha256(0, filesize) == "359b1981b8435121bdc929d449385f50fa844823efe5bc49a24eeb8446f63257" or
hash.sha256(0, filesize) == "8bc2c497e50eab58d318ed08d8a848edd4bc8e08b88080add4fe3b6541d809a5" or
hash.sha256(0, filesize) == "0b47ff4950be491f52ab488974c5c2d129a3e26b76291620e13a90660c5789e8" or
hash.sha256(0, filesize) == "22e4c31dfb6b53abf0df909ce7c1e6837066e625ab9e1dbbd426d0608792cb8f" or
hash.sha256(0, filesize) == "7310a4b36cb2c97139e7f948300be399400bbb5cca6ca0c6374d2b1edf0233cd" or
hash.sha256(0, filesize) == "aca1bdfa6c84f89a79cee73dbf8dcdf55b8fd0342c90793d624f062328670286" or
hash.sha256(0, filesize) == "86d15c6595bf8318ce2e0ca46727a8dca0c604d3114e1bbc089b8eea67c046eb" or
hash.sha256(0, filesize) == "21ba496a638eb57aeea8b90331f9913b7d92e223a97668a5e991be3910291e9f" or
hash.sha256(0, filesize) == "2d204e0a17b9333d2b5d60a01cc57e0398cfa08d3ac5544b76962f8e8b524980" or
hash.sha256(0, filesize) == "67c14eee9f3c546f3b48b00e3b2f2ab0058b7b5f7edd55e6449baea5b1082111" or
hash.sha256(0, filesize) == "be5f194a7675c68633938b4d4a52542c665c3d600312825eadc387cf4cdd773a" or
hash.sha256(0, filesize) == "5bcbc739a4ef555025c9dff21e2159e2ee419cc19261133d162625a790abb4e9" or
hash.sha256(0, filesize) == "75a88da8eb68a86955194ffd839ace87201ebad837cf6d9dfddbb2f6a1ef08aa" or
hash.sha256(0, filesize) == "46fcfb66e679db167fc4f53ab739be7c6849394b841b4b8b8948a9d2d8388abb" or
hash.sha256(0, filesize) == "03329faa6938dc3beca088aa71c0c2e07b48552e8d288be6e325ca3503a8b743" or
hash.sha256(0, filesize) == "8ceeb5fbd124d8f80750c042e941c24d122a72f0620739ab4eeea38702116004" or
hash.sha256(0, filesize) == "0ef7b4e838b0fac95373b243d043b0df34bcf9266554fa71d83a6fe05d4a6c4b" or
hash.sha256(0, filesize) == "aef1da71f6cc330c00b3ef4a289c227d074c0c05f7c542341c2be63d0847ddc5" or
hash.sha256(0, filesize) == "5cca5acac331e08f1bf033d6360be4385c9f2d455ff17fb8f6ebdd319b86fdd1" or
hash.sha256(0, filesize) == "300fe97673668d2051ade53fb08ce730518f06fc50391d53b2ca4e9f5297075d" or
hash.sha256(0, filesize) == "cad3f3d80c07015925bc8f82cceded126038ab5d3d07b3b0c443e15939796866" or
hash.sha256(0, filesize) == "47db0007e841a6488c50c3214b3decdd2c405600f215441d335607724e713b70" or
hash.sha256(0, filesize) == "1ccde5057994934ccf53e2876fd1a19ed7ae423d83d2c9cfde0bf4b0d48171aa" or
hash.sha256(0, filesize) == "fd4ceed043f3002cc082e2b3b718e2a69a835047eb698c7d58d6395373a9442f" or
hash.sha256(0, filesize) == "09fcddfd9a8519d30056c7dec8f8ed3e438d880b71aeb790bf980b682a41e977" or
hash.sha256(0, filesize) == "48315af03825c3802d77e822316b9365d5259069229e3c99571fd5201d4840d4" or
hash.sha256(0, filesize) == "460d59de9cad72876c20a644610d17ee159fca2589b3b87ec7ae8845a194fd0f" or
hash.sha256(0, filesize) == "9e0310e5fbab5136e312ac781691519af7ea3285475f793b298076743a2d9a10" or
hash.sha256(0, filesize) == "acffb1390fa3001a975cdf932be37b2cb57f5dfab827a8c94a2b9651bf83c7ff" or
hash.sha256(0, filesize) == "dd0f8bdabe8057ddc9705c740aa246a0bdc04480368d1ab60d4c7bc5efb8bbfa" or
hash.sha256(0, filesize) == "c419fec16dc1671b8bfde1cecab59f291f5496ceefa52732c2d1edaaa3f64c70" or
hash.sha256(0, filesize) == "72de38fa9ad08b0654b9fd51b5d5c81eca20579a64d658afa5fd83915a089653" or
hash.sha256(0, filesize) == "195434b94b49040d7caac0cde9d32affb93353b55778ea00542b5e632e0ba250" or
hash.sha256(0, filesize) == "de1d975edb0141983d134664344aa57a234f996c8fe34a7801e78bd87886d6e8" or
hash.sha256(0, filesize) == "51649ba609ff1aefc517503651a61f1f4784587fd93fee7d39047ef014fb27e7" or
hash.sha256(0, filesize) == "89811478f714260d54062c115420ba21a174f4bd57565251abff5f984d8318c6" or
hash.sha256(0, filesize) == "6172fca55f1ed866adf6a6d0728ac286f48be3880ca9a54438628215a18e6f37" or
hash.sha256(0, filesize) == "be699f87a6970f06efe2e51d278b0ffac8f20debe4b9dba34b5c6add515d41fe" or
hash.sha256(0, filesize) == "78420a33fc6d787832ce54801f9368909b190fd1e7adb55f1e791a73fd470930" or
hash.sha256(0, filesize) == "c356e79e75b271b8260eb22303df686461ea397a215feca921fac67524366826" or
hash.sha256(0, filesize) == "f3439f877a4be1c5ee1f2d22b0c73c725fe13da496c0976ac4042c235eb05dad" or
hash.sha256(0, filesize) == "648098e9cff59b6d50f97324621ad2120cba039ba72b2d00d7462d2fd3156a27" or
hash.sha256(0, filesize) == "5f1204a3d0ba95f0d099b953175e43b16ca0f5607aac7a3ce5d32d7680b6dc8d" or
hash.sha256(0, filesize) == "ea25dcbd3827aeaf34ae28c171bd3bd3117c94a74add3f7a37e4ee2cf238587c" or
hash.sha256(0, filesize) == "c4763fa8dc439acbf87f9f557ea1202fc665a5cfbd9643d1c6eedf120980e2bd" or
hash.sha256(0, filesize) == "9aa2ea75d1447378d3f0eb265d5b2ecb6fa50a29308caf9e5fd5302e0e85f73d" or
hash.sha256(0, filesize) == "2784d5a3a712ef3dbe5e84d8a0e48fec73c6f9300dc153cb171f1f206e09ad1f" or
hash.sha256(0, filesize) == "88d970947e4e0f008305a34e256871c93d156e17a0ed10571180d4f1f371fc34" or
hash.sha256(0, filesize) == "bb360de929ca6604c02340fe3abe23bbfb3c2efbe99fc3b710dc05742d00ba18" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GULOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GULOADER_LAB"
date_IOC = "2023-05-08 11:46:51"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GULOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "4b68dccffb6601d78041a03d53b92fc1a29f9300a0b3af086be054895172cb17" or
hash.sha256(0, filesize) == "47de950d472855f6353abba8be998aa93c2711995859492974ee0a7a8bf140b5" or
hash.sha256(0, filesize) == "a39ae2351c3e7217edf0616efdfaed4600b19191474a4a72c8b3fa8a892fc746" or
hash.sha256(0, filesize) == "7dec684e4f8201ed9aa44dca7eed761a0e2c62f0df0d6a6d3bf70d2993bcaa31" or
hash.sha256(0, filesize) == "a790aabb6892eb48275bb661288c2310c9da955fa6dc34070a264ad717a7c6d9" or
hash.sha256(0, filesize) == "a4b0f2de37c5c22359a9476c2a0801037c30e959072600fb190c8dd84f188154" or
hash.sha256(0, filesize) == "3f4fc4a448b6dd81657ca70d8d26321c940320a10ae852f57d6a0646c5198f59" or
hash.sha256(0, filesize) == "22038abe5b99a566ae05f31d2781c49af9a2554f66fed7e51d8faa69aa4e72d9" or
hash.sha256(0, filesize) == "998cf74840de65f50e76b856f8b999eadab09ed7a993ddfa8b026efad4e42950" or
hash.sha256(0, filesize) == "9ae2e398f3c3346697bec12a775d67d2f74cbedf1f8b676df3a967e47a88a7c4" or
hash.sha256(0, filesize) == "76e2ff187e9efffb2029b3f5cadee9e0dd99c421ab045d2dcc9eb8301767b543" or
hash.sha256(0, filesize) == "208628f8c2a48749dab1ff9932960b8ea95d2dacf0d4f19130fbe5b015f269ae" or
hash.sha256(0, filesize) == "5dfed8e51baefc0d5183e0a03b0bbcae5e72cf411c6db30bf1a8110d4ae34086" or
hash.sha256(0, filesize) == "d54645117158603ba561fc062627f335301bfaead5468df13e66b8a7a1f2b45c" or
hash.sha256(0, filesize) == "2b1f9072d11a6ac9170cb1ef608dfd7369f63639cb78296f7643fce2b4192a7a" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-08 13:52:17"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "zip"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "392fd89539908357b683cbaa85475e27fed4eb32b72edce0776b8a0ee2210302" or
hash.sha256(0, filesize) == "36d5395c756522613712c91c897018f0ecbf5d6db739aa1550776c5ad507d867" or
hash.sha256(0, filesize) == "0643be7e3ecb2129d770e8ebea090b2a1c6af7dd1b800aaf85fafa24c6718b51" or
hash.sha256(0, filesize) == "dc69aa25d32e263dbedff17dbfb066878061e916bbb79191c254a0f11e47ba83" or
hash.sha256(0, filesize) == "6a3bb094c3380fc88acc9aa77cfa269a28fd71b8725930bf178a8aed732ec2a0" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ZGRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ZGRAT_LAB"
date_IOC = "2023-05-08 07:03:19"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ZGRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "fd9fa9cb1bf0d368eaaea92a5bacff9a75cf33bd6d2eed99d3e206a561c39716" or
hash.sha256(0, filesize) == "d227e4601da48882cc8bcf03332cb1bff3230174cf587df27cd70b93ffac2b96" or
hash.sha256(0, filesize) == "b5302cc12ec1c6c9d4af95c1ea8189e9857c2befbe27da6a508605b703fdaeb7" or
hash.sha256(0, filesize) == "e03343f5ca5d24480eb5058f6c5b1e10a3a3ee72bf9ff7bbd04ca64591d68e85" or
hash.sha256(0, filesize) == "d7d00b8231ecceab81f7dd488780c12201c4f1ac8ea0fb76fe0c73519b070f05" or
hash.sha256(0, filesize) == "ea6320058e0cfe9e518434cc19d4c7b6f12f0b176fdf2b087c099ffdb650ab0c" or
hash.sha256(0, filesize) == "c1ffe0dba8ed1afcc2811a20ed498ff078dc887a413744e74d96dc7b3b06306c" or
hash.sha256(0, filesize) == "cc470b72ccef523a5e3253e5d58b5ef614abdef7509da84580d681011d90eca0" or
hash.sha256(0, filesize) == "16e0f70aaf0cc2f3c42a4733756a09896247876b8f17e371c378f1ee4c0076d6" or
hash.sha256(0, filesize) == "e2b0b8c5fe219a1b655d2d064b670b8d0b9347f74602c5528ec47d1b22d06717" or
hash.sha256(0, filesize) == "980d032454e0f471a820a594683ae833ac86d4c074fbf43cb47798ca774b35f6" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LOKI_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LOKI_LAB"
date_IOC = "2023-05-08 05:55:29"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LOKI"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "0eaeda634e3df8cd7985a88a15ed14167877ab010f3b8cab9f03bdeba2d361a2" or
hash.sha256(0, filesize) == "dca1d0825cdbf98bf61db27a0f5c14862c9e339b9b3d52f3b7c671e5a76256ec" or
hash.sha256(0, filesize) == "28a4489a297705ee3a1b429ab4799eceed929b25592a0bbf4aca2dea07ef719e" or
hash.sha256(0, filesize) == "b0d4208da27fb1e62586bae0beb7b01dab7fd10f06aaf065e7000614120b1572" or
hash.sha256(0, filesize) == "23dbc26b804d9759bf1071f4972658b648b6aa0ffe4a68986282c38fb9702ecd" or
hash.sha256(0, filesize) == "84404319135e2e1cc2bced7f56c99a81c1b19dc442966d3f0b1fa21756ee94b8" or
hash.sha256(0, filesize) == "50b98481af6d01585e639143040a43d067403bc84511267fc4ea965966a9f1d1" or
hash.sha256(0, filesize) == "0cd2587e9b5be20d794e45065357bf8eb547d6bf7bd209510dff279eda3f2225" or
hash.sha256(0, filesize) == "1e0557bb24ff7057aa35904c7ce0edf8cd20c7dee89bbcd9485cce5b04fb80ce" or
hash.sha256(0, filesize) == "4920cdf96db967e0df5414de0d8318d018be7af985158dddd3a4cf77af565bf9" or
hash.sha256(0, filesize) == "d1d7a83580ad42bb3161a92105ba3ed7308cdce0f65cec403462b5d2c3493705" or
hash.sha256(0, filesize) == "14334abea3f6a624d563fb59fa4b62bb145ac7e89332cef7b956cd36abae2d7a" or
hash.sha256(0, filesize) == "c8e8f7f75e522acef9134c34a0d74ad0f3e7c52d28aeb890823e506f7bd71597" or
hash.sha256(0, filesize) == "6abaaed9f099e14081efb00f9c34f304613c7ac2afc7eecb961857c832f510ea" or
hash.sha256(0, filesize) == "7608a4d226021049b15b84fdb7ef5af0a3995270cd5f6a1ed1bfcb163cfc8b28" or
hash.sha256(0, filesize) == "9a2f50a963266521072bdce5439faba012ea3c822a9f6c2c62ae118875801874" or
hash.sha256(0, filesize) == "68de9f9de58cc646734a635dc1c3b3a6747a7ebf746f3a4910d130ddc52c38d1" or
hash.sha256(0, filesize) == "a561eef01c27269d36395d338216574df2a9ea902d254cddd2c6c0553ff5f0e0" or
hash.sha256(0, filesize) == "0211f1dc94407a86459131e36dcffe165c9329eaac37c195701817260a3e01d6" or
hash.sha256(0, filesize) == "9b6573b930e72d319ef4efa0975ff1b59673f96633a03d5e338bc8d7418418f4" or
hash.sha256(0, filesize) == "446532d38cb4ebb9c65b386158c45599c5215f64a4f7fba9824141ddf33ec16e" or
hash.sha256(0, filesize) == "850877e704f398dcf595d4e4f2f89d9e39f636ce49d35a0cb1696864b7f56e7e" or
hash.sha256(0, filesize) == "216c876b63a28c7262bf7e425521f035509db0260e830710fbaf94dd2b61212f" or
hash.sha256(0, filesize) == "c2b5806d85c2bc07ea08bae0ad6827378de60328adc733196652b74ab65640f1" or
hash.sha256(0, filesize) == "e4a9cb185c46bf812bf29890c14a2f20f6dbddfc1e34abe5e15af668077b33f6" or
hash.sha256(0, filesize) == "583810f9b255c471b1881133ca5d65efd3c96a389cf0f40461c40db3f7861355" or
hash.sha256(0, filesize) == "1098c30f8d02c4acabc58ffabc34f30e2a234c702851e1c6c88a072d795a2ef1" or
hash.sha256(0, filesize) == "45ecfd36d97932c3c4fb1548684eaa696d4288cb373d95bae9010e057291611b" or
hash.sha256(0, filesize) == "57b52ccdf1b1bd983b94eb086e66941aff4f98e99f08083396c24aeefc73a53d" or
hash.sha256(0, filesize) == "6d4b6902689bfea045b5dcb0ae5357cd840886cde981343be7d437696b369cc5" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SOCELARS_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SOCELARS_LAB"
date_IOC = "2023-05-09 08:34:54"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SOCELARS"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "170496575753e5df311bb9ee14c9f68ed25e9947d78404cccf638d02ec4045b3" or
hash.sha256(0, filesize) == "9e9fd649377a38ee3685cab1220fb23bb23fef5d5aa09b5bc154fa37d9a8e22f" or
hash.sha256(0, filesize) == "0cf4f177b5abc780f9e9abb5cc05b3c41dfc0bbf7e74c28a951f80959a6c56b5" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SNAKEKEYLOGGER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SNAKEKEYLOGGER_LAB"
date_IOC = "2023-05-08 06:29:10"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SNAKEKEYLOGGER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "c05e009a0ed57b473c85bb82e2a47e132426999eef14fe3523bb704dd2532fa8" or
hash.sha256(0, filesize) == "cecdd08e61f932e95f379ce175ea4845583efe4cfbea94b3d46ee2149089c57f" or
hash.sha256(0, filesize) == "14105392349904938a9d2e42e5eaf8a401d2b832c89f14fb79440a7126525d57" or
hash.sha256(0, filesize) == "110f8f032e4b93189e7ed0b50830994114487e118d63bb8386d71e43829e8b87" or
hash.sha256(0, filesize) == "e25a323d5a0785d9e69188f5a57d68939cdee4fd1b75586739a5d899abe19e54" or
hash.sha256(0, filesize) == "234f7d8ed5bd6ab03e43324e3dee0406c727075e7334548d7e8bab8e18500660" or
hash.sha256(0, filesize) == "4809c391a6d2c3899d58973b84153cebba1618400fcc4cae2914ac873509420b" or
hash.sha256(0, filesize) == "3db10778e1d0ccc415a6ad057180e3ef8025d2f9ccfa77a14f37369189526955" or
hash.sha256(0, filesize) == "b809f6da220a4b9a46cd55eec616cd846349d6c9259b2623bd07e9eecf45a095" or
hash.sha256(0, filesize) == "b97c6335f3e28fb25346720785352929884243a545e2a4ec10abd2f6448ad176" or
hash.sha256(0, filesize) == "11c40e089217d16a0e9011687cf21883e5039ea51c1f32f529d55190e81214bf" or
hash.sha256(0, filesize) == "203495007a95ee6a8fd76e9e4b910e55c7126c1484a31b464b3f5206c6bfa388" or
hash.sha256(0, filesize) == "92d153f7f41fa901887ec1494773140e4ef9b41378f2ec05deeb0ad8ec3f5d16" or
hash.sha256(0, filesize) == "5fc74ee0611e268046704352c5e4e1562942d9c339adcbc2d71ef89948b7c804" or
hash.sha256(0, filesize) == "46a47f20c3eed9e71fd44ce1bbb288d3da1283f85ccc226679a030dde37f3c94" or
hash.sha256(0, filesize) == "164e737095c31ab5c5d9ad0cf5b410ca75ddd968da6ef7b3051970ad517b9a93" or
hash.sha256(0, filesize) == "773487046d018f6263a41e61d0387dbe6570096212418e29ea9de596d19ca08b" or
hash.sha256(0, filesize) == "06491cd2952c4f625d71050ecd36d6256cf3111de0dacd18c81aedefd8035f9e" or
hash.sha256(0, filesize) == "caac1ca43926e89b75c3a5f17be13f381c421730cafaca62c7d1d2a6fd2deeff" or
hash.sha256(0, filesize) == "6ae9300941c5572a3eec4d9b891e405318fd5595395c9251fe6baab66fa6700e" or
hash.sha256(0, filesize) == "a98cb609a48a550a1afedb557a9519289fe8d51755a16864617612e16f7b0982" or
hash.sha256(0, filesize) == "1b35cecdfafc65bb5e77421cdae6f4f2f2b559ccc3226ea0dad77417e3347221" or
hash.sha256(0, filesize) == "5c613c7403edd5e27a26a3f5aec588ce9c75806a9ce7892ff6fdfc615f80367e" or
hash.sha256(0, filesize) == "f79efc614f027353e325afc63ad28655b796e13a5862aa9dbe07978a2d9516a0" or
hash.sha256(0, filesize) == "3ace1af5dc3cf09c40ae8d4c0c4f499fbe1996c7c041ee07e30fb24283b4343f" or
hash.sha256(0, filesize) == "148e2bede0b826dfbaa3b409373e8635e810a39fff7e5677c2baae19f1be01e1" or
hash.sha256(0, filesize) == "4aab887cdbebf7604e30a8b5178b6249d69347622cc044cec3689380047a61da" or
hash.sha256(0, filesize) == "06bf0a18fa2bb4779e3c3b5efe493cc77014366ce4c54b40faac7ad7bc20e8af" or
hash.sha256(0, filesize) == "e7b9e29ce2d8c5beed41169e84a935735691f4d05a3f7d7c0524525ce4c63c80" or
hash.sha256(0, filesize) == "4a6e60538cef0a6bf4bdad671a97d825108b26f8b239a556f34d6ffd488b05d3" or
hash.sha256(0, filesize) == "a3964b75fb255439ee78983d5b862af551603990f7b06fcf8923ee660fb70283" or
hash.sha256(0, filesize) == "1667f1e836cc2e1cb068bdd25482814cee224d1ccec6abca06c885baa612781d" or
hash.sha256(0, filesize) == "d59e0227f0df4944cfa157554ef86a131e2b5d9a1d3983780e0022b98f1d42f4" or
hash.sha256(0, filesize) == "ec5a5103e89195cb445c89737a73996d34374ea0a99ddbfb0165b70b645dc414" or
hash.sha256(0, filesize) == "f68d6a0977abf5978dbcc20cfff9fbf9b308ff08c7066e94d0a4a50abc545fc6" or
hash.sha256(0, filesize) == "82acd801b01b749f54598cea752c642b2b47f45dd63fafe53050ed4b06b342a5" or
hash.sha256(0, filesize) == "20fb89955cb329bf5597f90a7510f92a33ab31d05c21e1374a52e89dde377a91" or
hash.sha256(0, filesize) == "92f0a55426e5040dc80133f908906acddaa338792783507fb0bb62d5b786c3ce" or
hash.sha256(0, filesize) == "976480e208e2df3334c4e4cb6a0a16b74250612e9e068df1e26ab24db3e24b25" or
hash.sha256(0, filesize) == "fbadbfd07054097238ecc6ed27a1e69e4907cbf038b4cc2ef52bd2f07085d0a9" or
hash.sha256(0, filesize) == "d18cf7fcde053ba33105a836de182859fbfc493eb5185bbaddee1133b9063509" or
hash.sha256(0, filesize) == "c9234000f8f576a39a7d2a957826094c1c0e37b141ef26ee2e8e36a62e09805a" or
hash.sha256(0, filesize) == "eeb17e6d418ca892e7fc46aa88fba3fc6dd0b72d38c4e7accc58facb15504a97" or
hash.sha256(0, filesize) == "fe115e2f52a393dde19834949351aab5dad7e0aa5021f66b863cef86c1180c67" or
hash.sha256(0, filesize) == "54c042777168fbf754e0bb6de6547d13398f958344d2e0aa76060ef4daa02419" or
hash.sha256(0, filesize) == "886e952321e1eae1a50c426ea8bff0e574d28a33ce33d85613f8cebfc45c7845" or
hash.sha256(0, filesize) == "823a206dcc8e78823dd154d935da7c7ba7029b9b51adfa1caaaca282fc7df829" or
hash.sha256(0, filesize) == "b050f56ceec41d0c409065b66cf598cbaf75a565afab1b47552624e085a3a9c4" or
hash.sha256(0, filesize) == "99b2648789b255b806ad8ef3e1452db4ababbf42e2cb91c94cf1d34cab808292" or
hash.sha256(0, filesize) == "a66fe6a02551d6fe4813f4d7d9d7b5afe13d9ed94983009132535ade72bdbabc" or
hash.sha256(0, filesize) == "8fb29a999e6c92b4511cff2615d6e518ebb42028ec49652ea76e1d8322166b17" or
hash.sha256(0, filesize) == "e30555e8b658827feff626d722c97c1239ae48ace30f9dea89172d476f569582" or
hash.sha256(0, filesize) == "bb29bd98dc2e9ea502e1e473b659e56940e604cd87058071b08a95dc7eddf7ee" or
hash.sha256(0, filesize) == "10bbbb979fe5c51ef5402549101a4460666e9be265d430d1214c93ec0b6ca915" or
hash.sha256(0, filesize) == "726bc1cc141d4ad30a0a1622d3aa8ac21af76273180b0efd160d2cc12d4a7fcb" or
hash.sha256(0, filesize) == "627a8e84326a097362789f0fa864cbb4b2bbbdd8b888c3ee1b180a9a345b14e9" or
hash.sha256(0, filesize) == "3b2ad36b753cef8bf7916c7a2e2261bbfea8e44bb2366d86a638e2057a493b8b" or
hash.sha256(0, filesize) == "e853894bb59928160a6692647303507c4363ee5278758429ef2e6352f1b22a74" or
hash.sha256(0, filesize) == "c867fd488ab6ce0432121c22a8135548149fdec7ac7034c5e9ad6a78f4d08c5d" or
hash.sha256(0, filesize) == "c3f3e5052e6a603098bfe0838f32ac8f3c09710817af3ae0e96384dc013b007b" or
hash.sha256(0, filesize) == "c9cd4f96ee3af2513298b62ff2f1c877364fb537fa3d70ef5268db133a4b9b85" or
hash.sha256(0, filesize) == "7da17822c2ee73d1a947ae98cf77ec65a89b65d928123d95ef114438da2c5186" or
hash.sha256(0, filesize) == "d82034c5d5633ff4f38939fd15a82c3612886742e8bedf4060fdb2129c09aacb" or
hash.sha256(0, filesize) == "224aab197e463ac0475240b508cd22a3657ce4d5baca37a7e05b7629912f9ac6" or
hash.sha256(0, filesize) == "dfb1e4b77bceb4aa51ca6e7f79785f573cdfa9d047bf38717926730be7f85026" or
hash.sha256(0, filesize) == "e64d2225119a5860cb2d7a12914d0834d174a72d387e84b58e3d94c89aadbc58" or
hash.sha256(0, filesize) == "2535b5bde8fbe58fcddfbf7acf196a1290159e707c87929a1ae94bc7970e1c5c" or
hash.sha256(0, filesize) == "e41b0a6b4bcbd587687a7d0fb61fad61a4540df2b09bee9c19f2dccb1478e554" or
hash.sha256(0, filesize) == "ea138d2457b49cccaf16cef8ff2a7479ed5e5207081c5d7ced0a2a3049dcdc16" or
hash.sha256(0, filesize) == "eb70269bb8ef78bb7c44342ed5efac0d76df99207bf45e9f9739aad5b9cf9563" or
hash.sha256(0, filesize) == "dc5d1afb2584bade5caff58e4e153c22ec7a52cdbac6c1b56d289b0621c98b10" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FORMBOOK_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FORMBOOK_LAB"
date_IOC = "2023-05-08 05:50:26"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FORMBOOK"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "b5db87325a5b386200f2379ce42a371b27efe823cf726d707ba79248bbecb2f2" or
hash.sha256(0, filesize) == "338bce8dae0d4fe01a719210414e31861456fd8584853636e775ac4e1c83b453" or
hash.sha256(0, filesize) == "d549aca33462425f1f32e75b6635986df724a63b6f738247d4be19de580bc899" or
hash.sha256(0, filesize) == "682f46b161401fb00f263cb29ec705cde9ab8de124ea372800b841e9b36349fd" or
hash.sha256(0, filesize) == "0ff778c726b0fbebddcd0cdf011e1dcc406f0da01af601d0847fd26127bbb52e" or
hash.sha256(0, filesize) == "ecfa6953a0d70eac807833f74a201fc694a325cfa964303e82348abf082dfaa5" or
hash.sha256(0, filesize) == "5a5c76f1d8102545a33474c1a81b7742425cea8bfc4dc81d112fc03af2d3682b" or
hash.sha256(0, filesize) == "22846a4ce2d41e6016ca35cec6d8a943874f6c96a028a7a687c4794e66476168" or
hash.sha256(0, filesize) == "a9c993b79501ed32742c5ccfd04e3c47d75fd26795fd87bb3dfc288384de7be3" or
hash.sha256(0, filesize) == "70d1125e7b5eeec17ca0248a72e7ea949bb78364928423d5ec062bd4e7eb825a" or
hash.sha256(0, filesize) == "030026bf6983702ec4865754d69d48af120bf0dea165ef7ef22428422412fff4" or
hash.sha256(0, filesize) == "7d9650c4e743709880c6173017783e0a4a4bda3c1aaf4197bee2fb4203514fd3" or
hash.sha256(0, filesize) == "9d53f6d13b02326eca5357286c0852c653dd17f8459eb30dd0086fe2ef2f9049" or
hash.sha256(0, filesize) == "e323ee081dcd5002f4df050289a12efb32163aed062cae1e59d6ecc778215c96" or
hash.sha256(0, filesize) == "00b678e92dbc23f5d49fa664274e0896bc194d32957ce953340f078b7f740df5" or
hash.sha256(0, filesize) == "79d20833e8b6c58ece93da20c2c927565e26198e5a6c802cbf5b559429eb4a94" or
hash.sha256(0, filesize) == "39e060b9061f02370ce9755e5fdd0be380668b6758fd1df7a31415f3aa3983ed" or
hash.sha256(0, filesize) == "684b96d914f865bdbc71a04378e152e84555c198d56775b2cdb15cf22bc6af93" or
hash.sha256(0, filesize) == "e96818760ea636a3e1d90d1bf38fa2f254587462b3dcb4e29113dd34b0903054" or
hash.sha256(0, filesize) == "5708134963ec09acd66b22cf1115ec458151bdb151b5ecdeb69cca55081acadd" or
hash.sha256(0, filesize) == "5da038f135ce18d328cd2669818a22472143fca71149a96ede113a018eeaea88" or
hash.sha256(0, filesize) == "f6bb03b4bffe8e0ab71cd8e26a65b3baa5f870cee4e974851dce6d8140316c84" or
hash.sha256(0, filesize) == "abbf85558290e3fa302f88f51243e5216f24c9bc4fce64a0f616db3be2c46e8e" or
hash.sha256(0, filesize) == "4627b5c64c1c639a6930dd1f9f135e396853e6bf418c8c0d3c6eaec18b3cdc12" or
hash.sha256(0, filesize) == "09c909d826f67375085a88c5a85bd863d6df12765f7584ed2d439af77afc4c21" or
hash.sha256(0, filesize) == "6f6b3a191b4aad04649e0a6da4c6d2d73f037813cbeb84f4635c1a0d06ea2252" or
hash.sha256(0, filesize) == "813edf2d24ed87e7182a05b6c7ae8acaee063fe5ff270b82d20ed00030caacb6" or
hash.sha256(0, filesize) == "cfe9062e6bd88ae993c3e8b295386c2e5e9aa7d8b9ceb168f56ccd3e0e5cbe36" or
hash.sha256(0, filesize) == "c1a8208a8af8fb2ab397e586ea8a7f265921e6f21eb592af42a8f5c805d18557" or
hash.sha256(0, filesize) == "69087db34f2934a2bc582a76273d4f3d75e15fda3e900a56e8f89bcc04c8040e" or
hash.sha256(0, filesize) == "a0944a8143c9b1520e9d4c27ba2a6699eafc3352c7fecee0039ab0f410b047ff" or
hash.sha256(0, filesize) == "fbcd0824d723107fbf65f4d82506544ff6514364e745242e74a8d7f86d16575f" or
hash.sha256(0, filesize) == "7517367b3b61170bb7637de6f89077069159c4a04f430c28102e2d7cf5a0343a" or
hash.sha256(0, filesize) == "272872a41e4e0ae720f6f61e50320720cf0313fe65d4c039334ed6c0cb7f37b0" or
hash.sha256(0, filesize) == "6982ab1d029213bfcfbc542eee0d955b770f5c0df083dc94463c441e2de35fe5" or
hash.sha256(0, filesize) == "fb0e85c85e1e2745115bb7c832f102cc4e4ef11f60740e77fc0a47163080419d" or
hash.sha256(0, filesize) == "c11514fce0720af2328f702e0a42aaea3b9d4ef635de46d638a9ca0629e5f75d" or
hash.sha256(0, filesize) == "fd969f116ad42b7daa0fc85cb45fbb2e5c0982ac6c59b88e7859d298292379f1" or
hash.sha256(0, filesize) == "7d0fdf3abd56e00933c0fa8cf573b948a4c9d85248915249f1909e6775f75e75" or
hash.sha256(0, filesize) == "cc9fd84fffa1811ef3177586d7c6aee88dc4dc5412fd658ee4bb36ff934eec01" or
hash.sha256(0, filesize) == "ee4ff10990ca92dca3a0ee6feeff6930f5dbeb239e747cf69ab4560a65a9fd0e" or
hash.sha256(0, filesize) == "db18c2ea53e693b3fa16b5c6ca9512c359f657e2f0ea2cbd462ad17d99da2ce9" or
hash.sha256(0, filesize) == "7dc1242cf01a3a2527ab8a7e03c370f556e3d62f4ad2567aa383000bb3448517" or
hash.sha256(0, filesize) == "2413b677dc6d6a5c958ce9b8cfab7eab38151088dcbe5a1d3b0ea903a0fc5ef6" or
hash.sha256(0, filesize) == "b07bf739cd3a803bcf0d57fcbf8d5a628005457db39fd045324e08bd92e6537a" or
hash.sha256(0, filesize) == "5e793a03ccf02064a081df03e0ec584ce32ae3582fc08f8bca88bc1a26c45e89" or
hash.sha256(0, filesize) == "ba666b362fbf39a28389eacd17bbe764cf1ea11428f381bb2c874e2dbbe5586d" or
hash.sha256(0, filesize) == "ece8d55b66fdd470a1037290b30a2e6e66d0ff38fa366ec37a2b0a330364093e" or
hash.sha256(0, filesize) == "6f505344ed0b63ea1ca3b058da81bd53069385a4fec5b94f7e97ecd3e53bf57f" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_doc
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-08 12:56:25"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "doc"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "c8b4d0795b49a78e0de9122265286b18ac5ccef0aa4ecae129e6f277a443e017" or
hash.sha256(0, filesize) == "6c49fb06dfe965c43e04fb518e026f5d1b86ab844797d76df6a8a13955099702" or
hash.sha256(0, filesize) == "70b7879f9b98ece1dea628f1389a1400c8444f1b98bcdfba57c3fa6ebce75dab" or
hash.sha256(0, filesize) == "cd34e596b84956502c4844a029b087b39d92527bb5d79022dd19731c9acf47c5" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LGOOGLOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LGOOGLOADER_LAB"
date_IOC = "2023-05-08 04:52:41"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LGOOGLOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "160e57964d48e96e4bbe2e4abcb0befb9d8dc4cb72253557d6a4ee1e8c84f4e9" or
hash.sha256(0, filesize) == "5867c5321292565fa017f4e88b6c4894572d7fa557e9a0ddb1ced4362413b6b3" or
hash.sha256(0, filesize) == "33d19ef3e937679341017f230d096df286eeed85afe5af4862ae8a9ef31db6bd" or
hash.sha256(0, filesize) == "a38ee725e23f1acc01722da5a54cbf1cd76937271509f08a9c795fc3a0301f2b" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FORMBOOK_xls
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FORMBOOK_LAB"
date_IOC = "2023-05-08 12:56:44"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FORMBOOK"
file_type = "xls"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "3aa8436210e3c325f1b084fa4f89735bba24b88ae5a8e4b0b1283542dcc02467" or
hash.sha256(0, filesize) == "3bd14656f0eb0ae5d7b6e3a346e9b57394b1d082b267142bf87d9bfa02667591" or
hash.sha256(0, filesize) == "370d84c40c7b03fbf799cfb90ed498dc479945deaa2b1fbcf1320da59a234e80" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SMOKE_LOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SMOKE_LOADER_LAB"
date_IOC = "2023-05-08 03:40:21"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SMOKE_LOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "70867d16ba96af0ff04aafa9a6c724942a0345bcd2c98c2003f0810eb92b11db" or
hash.sha256(0, filesize) == "08932cace59574e4f7e8101e75a3cc29d1840fba4eaedcb50abc49c65e272650" or
hash.sha256(0, filesize) == "4969ef4af9004baaf340293cb7b7a4b46289d2648105c840997a961b19f7d846" or
hash.sha256(0, filesize) == "fcde51354930deb6bbac2c54fe699a99e9f9ac0d3abc832b9769d379812a95e1" or
hash.sha256(0, filesize) == "bbeb5f94ea18e0ce8b051648ca84ef117dcc0efa2352cc5e576c293e1bf55857" or
hash.sha256(0, filesize) == "374e1a295df446b79b3d27fb45688ff7879f0b0798745e963cb94397a4ae9105" or
hash.sha256(0, filesize) == "00997f8016ded8b467d957d8807032afd3e496dd94c3c9ec1349750abade4f39" or
hash.sha256(0, filesize) == "fb332f7b241cc9956ead9401f2fa69b14ffbf0730996e4b0718ee0f874d7d52a" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_REDLINESTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_REDLINESTEALER_LAB"
date_IOC = "2023-05-08 04:08:37"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "REDLINESTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "779831b59285775871a764575e193a05ceb77405278004698462f38b9838ac8e" or
hash.sha256(0, filesize) == "04e10c4821f3e6a5e13e60b1281623d095802c8c295a2a4bfe9de4b175881209" or
hash.sha256(0, filesize) == "08317611732baf1318f2f4344b805765a0e7e35116098bc4c1f999952495bdf4" or
hash.sha256(0, filesize) == "72f689974f397e95426f561c8ded3348e839c9dad066b1b1fb2f813d85be97a6" or
hash.sha256(0, filesize) == "6deb6d760d447f8ce82f834e4b928b4b6849a69a948f232b1b51e234c3aee418" or
hash.sha256(0, filesize) == "16d4e713b6970d966dc0df74b54d3d974016649ac65fd2188ee9d35c679ef13b" or
hash.sha256(0, filesize) == "637de2a8fa40e4d42f73be06a969781505ccf5cfa87f6e3bea9391ed7356c34a" or
hash.sha256(0, filesize) == "07288e08c4feaf24e41234c8ea5420cd9859d4056eb86130d540f8c42c483c12" or
hash.sha256(0, filesize) == "a36bb3b9756b8937718560af35cb3d25ef20ad99ca44157c02c1c79a56fd275d" or
hash.sha256(0, filesize) == "dd1d7a4709d9cbd633549404b9ee292a15c672968874bd40a858ee29da6c9b9e" or
hash.sha256(0, filesize) == "929e8a2598a4046d4064b608af291b658872c5f2a6bc089467bdf1925fce5aa6" or
hash.sha256(0, filesize) == "33930ac5b5a58d7b66f205e0e131e29b2ea421316a07b4a4020db722c399f66d" or
hash.sha256(0, filesize) == "355187cd71c38b58210b22e41e9d3df14dbfca37503289038b1c0a71c08491c2" or
hash.sha256(0, filesize) == "31c679761cde0dde6ca8218c6a3c372897800a3cbe5d4e6917b57c429c8010d3" or
hash.sha256(0, filesize) == "bb7479d1e0689a68a0e9cb3341d22e625dfba03a436a9306a6e4bfe47eca0638" or
hash.sha256(0, filesize) == "2d22008c64c85e23e6436e2ca951ccc52305835949c90f8b952446969bd6e75c" or
hash.sha256(0, filesize) == "b545eb55ad689d565abf98b9ce3aed93ae4325687c1257d7c53720a33550fa97" or
hash.sha256(0, filesize) == "b00f54f3033c071fa5844a8afad3623a2ad086a0a8fb4efa51dc65f6bc0818ae" or
hash.sha256(0, filesize) == "651c9fb71952bd29c53b3bde439bc7f0538241608c1e6ed3a7f96fe063f41bbb" or
hash.sha256(0, filesize) == "efb1bd2291e35da7f22a7fe0345bef2c04d3fe3fcc73bc448dda00dc4e9bad8f" or
hash.sha256(0, filesize) == "47d84b51a9fb5db387b0ad30fee83657196f181699c7385215c2b1ac1cbd070c" or
hash.sha256(0, filesize) == "612ece4f1edfa547cb2c224d9018245e3d4407ee587e00c648e80358e0349493" or
hash.sha256(0, filesize) == "59b3199a56df925054079872734e8750806580e6d0d98ef6e77ed9e9932abc03" or
hash.sha256(0, filesize) == "998e92e03f9fdb7f792b182ae22d9edb689ca0cebb8a11bd42e032b858ee63ae" or
hash.sha256(0, filesize) == "c5908bf913514bbf1b52bb43734288950c002d9de7a44b0e900761f42a87e200" or
hash.sha256(0, filesize) == "ffb8eaa7c99aec6346c904a7adf3fedaad7f758a72eddef93b6aa6159f033e9e" or
hash.sha256(0, filesize) == "f1ec4855006e287da707713cb5b7182ca5d9da1fb40dc4e499b5ae6939ba448b" or
hash.sha256(0, filesize) == "4a69335a427546cfc1b44efdd2d2be9d1190d271ba8920cfc661ebf352cb48c9" or
hash.sha256(0, filesize) == "08424055881f85eef71b6e252b91ba305ec887913828ffc024e579ed1ea57d8d" or
hash.sha256(0, filesize) == "d1867294a81b3772ad1307eac10b78eceecf28210aeaeb7d524efca6515f0f3f" or
hash.sha256(0, filesize) == "3fd79ba1fc89f18be599076afd1baaee113f7757dafeebe0167f126918f4edcc" or
hash.sha256(0, filesize) == "fc7638f62d84fd3e510ac1cd6c3db50f5f1851947cec10aa27fc897ec893228d" or
hash.sha256(0, filesize) == "1273432c8ab01a3162ce6025ea1fbc11353ecda5655e3ff204e3ef64f0e509de" or
hash.sha256(0, filesize) == "05304efb0d6d7f4f5d5bea50cb9e1dc7c2034bfdd374ca737f604f1ec78cac68" or
hash.sha256(0, filesize) == "63fa3c9886f2ecbfa8c4cd602e3862884d49f63e5c72d5e36314f5b1945bd18e" or
hash.sha256(0, filesize) == "8ce111dc6707a72e8e6c27b21c23c5daf52056b149c71f3bd6ee10da957339ec" or
hash.sha256(0, filesize) == "78f7edca34bf0d7b0b58c46c4c6c8230a7c46d246e0d2723caa23ee1b4df4b4a" or
hash.sha256(0, filesize) == "37b8387d3e126e057247e99ae3d0e0fcce3c0013ac1d1442cc1dae21eb3d15ab" or
hash.sha256(0, filesize) == "64b5040a080b628c06eb5cf86e410cdf54b8000498d1765a309c7b3a7dcfce5e" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AMADEY_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AMADEY_LAB"
date_IOC = "2023-05-08 04:19:08"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AMADEY"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "f894c33733e346c9cefada8e6136e53633bd70aac1c6ef081d374dd852426fcc" or
hash.sha256(0, filesize) == "ed7c1bc55bc1f2e1d9df7a1005721d7745bf17ff25bfdb588d5f0aabea816c62" or
hash.sha256(0, filesize) == "d5f14b56cd42d5ef42a59c6b1140de0fd19c4f53e6bab07979b941acd30a11e0" or
hash.sha256(0, filesize) == "0b1780772e521630e77a1f6b32201bed228a9c97134c9f462d5ac8d6b08ccae4" or
hash.sha256(0, filesize) == "26a0d827866e344e7c2d0dadf3a45bc70e9fa7bd7a932605c0a27939d9e43a0e" or
hash.sha256(0, filesize) == "a5abac1f66c4216dfeed1b5de73dc8ed397545a162510d63c3fea512b00d46dd" or
hash.sha256(0, filesize) == "a2b3d06d9dc9c1ed4dd58bd8c867c1c27e877e07d199f8977f7bca46e6561c19" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FABOOKIE_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FABOOKIE_LAB"
date_IOC = "2023-05-08 04:14:03"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FABOOKIE"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "3b978947ee1c869486e1926f11b9f1708cfe87d44ddaaa8f5487837c42ab832e" or
hash.sha256(0, filesize) == "a32677191be14234346cefd2bc582bfca49f3315c82ebb9f6aea5efac6fc5cc6" or
hash.sha256(0, filesize) == "82e7dd71b5ff943bb1829fbba1f1903948a98e2ebd901ea6bf15054ec8d3bd47" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_DARKCLOUD_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_DARKCLOUD_LAB"
date_IOC = "2023-05-08 11:03:18"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "DARKCLOUD"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "8219c51d6b3c5e8731f2f9164bd41d2ef90fd9b3c9d36254a9a5e2ed5169129c" or
hash.sha256(0, filesize) == "0959d0ecb65dd71a370b8d70f8fe2940422cf4757337595bb6ee649d71236bd7" or
hash.sha256(0, filesize) == "cea471b18f25770dc19304e536ca3926c0fbf161c6f64aa018cd077a8a150a1b" or
hash.sha256(0, filesize) == "1d4eb9077c97b5ad001206eff72789b01386a2c253d34875fa1c1acb716e2e56" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_rar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-09 06:07:32"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "rar"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "3eef07c9d499efcd05537eed9a011451f1430f23832546b1eb1e1fefa2e59c10" or
hash.sha256(0, filesize) == "72004f9e1265a003c3d1d06654f22d7dd2b7301e5abb325c384d0cb97a07650a" or
hash.sha256(0, filesize) == "d83e8c5d468181fb6a7068872aff4f9230659a67373eba0f4265b15486e42cfe" or
hash.sha256(0, filesize) == "a7fc4e0ed36dfd894b97bab7088c857eec4fa795b81b53293892813d907a8a65" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_STRRAT_jar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_STRRAT_LAB"
date_IOC = "2023-05-08 06:05:31"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "STRRAT"
file_type = "jar"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "66b2c690add64f44bb5c8267aa936b5a7fe55d6b6ea689718f7d1fc5276abdf0" or
hash.sha256(0, filesize) == "538b56bb9dfd234b01c7d1d28fd4b79cc6ae7b14f2db5227f72379b772d072ee" or
hash.sha256(0, filesize) == "5578f766f6f22f8de8e25ed39f59b099c030247538c22c9938aa12b3a28f34d8" or
hash.sha256(0, filesize) == "39b2579e79869bede2ba65366cde7b71a5125a2e96604f29b4107a2ec1137c2c" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 06:26:27"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "zip"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "5c534ace347a0feecea19a821dd9f391af8c89eee4dc7a5a621a2b800496d3f3" or
hash.sha256(0, filesize) == "5172a3ff726e2bc49d6b00ed7f9e7305beac049a145c00d81ee289085e9bddd9" or
hash.sha256(0, filesize) == "f6181d41b293f3dfc259c2cbbc2ab57d074adbc141d0ff345a7230a5031a0928" or
hash.sha256(0, filesize) == "c7e26d11ffc48c3f8687e5d148409e5a0340dfedfbb9e39fb7962d88421e53bd" or
hash.sha256(0, filesize) == "96008298e97996892e74d426f7d5dab61e1730c8627f407e5fcff0d1ec21293a" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA___exe_
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 06:34:39"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "__exe_"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "d46e1ec4fd88c836e0f27060808c2506e0df5d1034f9c88e56dd37bfc2113120" or
hash.sha256(0, filesize) == "392ee3c9d47409b170b5e4d6f7eedf427bf1121be42b024a663340bed3025bd4" or
hash.sha256(0, filesize) == "c46119f3e5e6f2e040db68e5e463068b4c1491f2126af04b4a06fc7a83ae0bb2" or
hash.sha256(0, filesize) == "3b0acc239747010ba9c56cf22b4ace536d82bf3748dfb14e8b977c07f8dc976d" or
hash.sha256(0, filesize) == "70eddfd85da24eeaffddfdd6326c749326f098b597ce9c8eb79b3e69242a7801" or
hash.sha256(0, filesize) == "02fa0fa7b89d8ad41c32e0613ef1bb1c38d9b93ea98d9295b7d61c2ca400a300" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_xls
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 12:57:46"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "xls"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "6833802e17ff48102d8e071b5cd4c55210032660755fc1c32a34e931e9e9125c" or
hash.sha256(0, filesize) == "be5e804110d4b0a978f95431e4eb2b1eeef4d855f3fd2944bf890bb5c447e901" or
hash.sha256(0, filesize) == "7cfb5ed8e9d16e9639b704b5991fedfb7abd82c1022fb00a83cf6bb5df31cf27" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ASYNCRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ASYNCRAT_LAB"
date_IOC = "2023-05-08 08:28:44"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ASYNCRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "a666414c0efa7accdea98d49cf5b0e97fa92d5c28a33e0382eff9fe3ca5638ab" or
hash.sha256(0, filesize) == "e33ad76851f23305a18b0db3a98c45280c581505da95f701e265a8479713c3ff" or
hash.sha256(0, filesize) == "029166bde069ee02c46c3541bfe4f9ff8c97d9ff353f814c716e2d1fbdb29c1f" or
hash.sha256(0, filesize) == "1de2e96d4ccbc3954e5a7932b823f268e565639a0aac20e41fae23248d32d1a2" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_COINMINER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_COINMINER_LAB"
date_IOC = "2023-05-08 08:22:23"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "COINMINER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "3a60b811771921ba75cd82dedb4c98b15419e2487ac00ba78ca7d19b04a3747c" or
hash.sha256(0, filesize) == "fd975f721676ab06f6158d4999c83e97a8946059f0b4b0bbc3919eec67f220ea" or
hash.sha256(0, filesize) == "c0243843ad83a16bc3c9fe59f8d530aee08fbe163b45cf676c4327d1f2a0a6fb" or
hash.sha256(0, filesize) == "0a54dc3c661a3431846dbcfd90f76aca5c9259c3250f1d1c50fe6cd9ced00573" or
hash.sha256(0, filesize) == "84c18f78f11b9bc3fd3e96925d2a7b76ab5ecfb927c377ad27456e191815b24a" or
hash.sha256(0, filesize) == "df35ae0d0b0981dcee4616fc5f7e5fa69d6d2b73cbd500fb9950eb0bbaabdc74" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_REMCOSRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_REMCOSRAT_LAB"
date_IOC = "2023-05-08 08:22:51"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "REMCOSRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "6466784605af932d9fed42461d13c19ab1bfa3745d4bd102b9dd79f5c49ca18a" or
hash.sha256(0, filesize) == "cdbba1052727bd2bcec565a5a4851c0d2b8956440fc33bea798ba3d69107706a" or
hash.sha256(0, filesize) == "27d5b86fa6821ac78a1ad2ad6dbc94cd34d24e461ebc1fa15a0014acd4cd71d6" or
hash.sha256(0, filesize) == "231989cd97aca7bf4fea9fd690b489cad61a2277ab602c5a2fbb42c8739f145b" or
hash.sha256(0, filesize) == "3bebabea8a9d00a8add9979fe330b1e0494a4595677929f8c94f3d7e9e04997d" or
hash.sha256(0, filesize) == "59d9df7e128711ba9e34b6a6cac31cd50e25c5e350849abfc1b53e8c25854719" or
hash.sha256(0, filesize) == "1a925bac006fd8d7a5d58d0229eabe2e30634626731add96939b0ab542c446a5" or
hash.sha256(0, filesize) == "7f930ad707464ff08068026c219b7d470da3adedf984f0b0897e1adb4126440d" or
hash.sha256(0, filesize) == "58fd0463cf7793ad6cbd0cd048369e70f01051c943a8655b97358065f4e2b0fb" or
hash.sha256(0, filesize) == "f4e8b7d9843776c8d92cbc77a69ba7cdd9ade0314f8e4cfe7cad71b2f83de6c4" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_RHADAMANTHYS_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_RHADAMANTHYS_LAB"
date_IOC = "2023-05-08 08:33:15"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "RHADAMANTHYS"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "9bc4e5ea2f350cf4931c62e261169163bd8b005a8432780635f0909cdeef959e" or
hash.sha256(0, filesize) == "543c264b4e3db65f803e6a0fa4c71ac957fcb64a802e2a29dda024b58d0e7aaf" or
hash.sha256(0, filesize) == "c58f48f203d4c0458672355b29caf31ad7ed5456cda3211eea2606452cf17a8f" or
hash.sha256(0, filesize) == "b562b18a6df7d02dae4a37840084958370bb9a8cd821b7f85d2fd3e0caa662bf" or
hash.sha256(0, filesize) == "597a4ade5d862166430037813118d0e94d993d87490fed195c0ade3fb6ab3e42" or
hash.sha256(0, filesize) == "653c12db63dc13521af00bac41d549d599d5705c81b8b8ee40988a4a0924eb22" or
hash.sha256(0, filesize) == "3c63cba176da52d065dd409c50fe0b6a2be79fcd1dbdf291547b181885df43b7" or
hash.sha256(0, filesize) == "cbe0c63e75f5586741a62e04f41339bc0d96f17b165e7d7ff6b8b635cb9a5e8a" or
hash.sha256(0, filesize) == "ccad7b6f65bf0381e93cabfceeb6e0bc5f838a37112981733b7f719f8d90087e" or
hash.sha256(0, filesize) == "3afa8aa568f26c6dcc9e6b32f376def4d0f54912ca431490e3b6558044fb2368" or
hash.sha256(0, filesize) == "c71c91c8c6cdb4cc17f2777a9078e0863f1e9ec76555043e85286af14fe275d4" or
hash.sha256(0, filesize) == "9a1ed7d76de14600ab79f7d60e45dc18d857c1ce0d2383df629d0fbe91e6ea0c" or
hash.sha256(0, filesize) == "c7e1d534ed1a723b3aaecf3a257cf2f1bea5b46fb91b8510d9ae9523a3caf457" or
hash.sha256(0, filesize) == "4e98115b5fba13ab772a232e8c454490e7e76df4c844bf02b176635aa737c87f" or
hash.sha256(0, filesize) == "5d19773133777f71070defab8ca8292254cd3db52dea5617868c336fc9316966" or
hash.sha256(0, filesize) == "cba464617c0ab39774135cc89752650e41f8ed3cfc773619995be0267f7570a8" or
hash.sha256(0, filesize) == "a81817d4fbe3876c3816816bdcf8c4f9a62e1fee006a74932bed350afa7f59f4" or
hash.sha256(0, filesize) == "9dbcf39905c23fd99583b0605b14218f0bcad230ef0d29dcf5e805b63bb07628" or
hash.sha256(0, filesize) == "99db3c3d444e7d3130fa3e1404a7e53fe255a53bfb0b356a779da524c9e5c11d" or
hash.sha256(0, filesize) == "4b4fa37e0aa56e673d1fc43a8c99efe824b01c8ac1a7eb838f4637595e569af7" or
hash.sha256(0, filesize) == "bf2cd59f7af11716d59b147af959c306ff601f229f8ae8dfe016ae064a55cd8a" or
hash.sha256(0, filesize) == "5938a2e1c2cd918167c1e7f9e9797e039993c841408c1e4245d0bf8e9c3ca333" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GCLEANER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GCLEANER_LAB"
date_IOC = "2023-05-08 04:03:14"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GCLEANER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "56a12ef9c5f068a71cb23bd974cfdcd95783dab652930c5a9ae981a4529a820c" or
hash.sha256(0, filesize) == "1ca314c9c69ed42a7a800542757d83c7e34e9fdc5cb5c2cd9589a17094fa6b0f" or
hash.sha256(0, filesize) == "8c21b42779afd2a536ddd8520791c84e268ef29b8deb4c9979c00e52fee7fdca" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_MODILOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_MODILOADER_LAB"
date_IOC = "2023-05-08 13:18:58"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "MODILOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "41d598443c6324a957f6de00a7dee27e5f21d5f9168ab8eb82e5180621771674" or
hash.sha256(0, filesize) == "b415a5cc8d0c1c960e7bc16bcb9351943b2c998f9430b1a1425b715754cc1e11" or
hash.sha256(0, filesize) == "f983cdcb52e6144ba8a87fb6f8904f39d3626be45c1725bdb89a2522525f3a9b" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LOKI_doc
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LOKI_LAB"
date_IOC = "2023-05-08 08:26:37"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LOKI"
file_type = "doc"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "321ab0b1df215447186a761b854ed239113af908ddcd40740f2b8d539530ad49" or
hash.sha256(0, filesize) == "7a1d9b74743b984e06990b35c0ffec7ffd2e20e9a0c484f31f0aed84103be299" or
hash.sha256(0, filesize) == "f24781001f198ec760cdf8805dc1fb123558d60d32e0cefbffe0a410f0519838" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_img
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-05-08 07:00:09"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "img"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "ee218f30983a6f24f72e1c20681485efc3cf71d25f2e5b290e0e41658700b100" or
hash.sha256(0, filesize) == "2c245c349bf18188ed56488cd0f42e05706c07dad4484847583013fd577fce72" or
hash.sha256(0, filesize) == "9c4b6802871fbcdb02dd050c63b78fd90563823a9f354e668eab955add424b90" or
hash.sha256(0, filesize) == "99d862b5be1bb682eccce4c68821ba60ac79396333fd6a535787acea60bda9a8" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_CHAOS_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_CHAOS_LAB"
date_IOC = "2023-05-09 03:13:38"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "CHAOS"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "542c157186bae766dd3e2df424e9c25251d71086b99cc9df121bc9bf50462688" or
hash.sha256(0, filesize) == "662704790ba57db166be625d0ad9102f44fa30f0a6608ff33ed76355e9ebc4db" or
hash.sha256(0, filesize) == "7301c4c8728f9db28887657f361a19219451fa78fc7b524a5f86c805587e56e3" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GANDCRAB_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GANDCRAB_LAB"
date_IOC = "2023-05-08 03:27:34"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GANDCRAB"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "9151656aa3199f9357335d5f72d5ed7d5c7cb0e140a7567e4482322bb0d4e195" or
hash.sha256(0, filesize) == "b62795206314c3ae1ace8aa4b5b7721e02228609767b23aae504cf29722b0adf" or
hash.sha256(0, filesize) == "a10fef39a1053e601ce8faf3e641cbb498424355f16f54ed13011138a63bf8ad" or
hash.sha256(0, filesize) == "a78366e68d6a773d6fb0bde6650c0bf30de13ccee56257ce2d80943b935dd4eb" or
hash.sha256(0, filesize) == "d9f6f27110bfa5ba6be14ad5361d1fa53bd1615ee2a5ea860e939a3a22599ad7" or
hash.sha256(0, filesize) == "48ce4be850c2fc89813d924122fe1994f0c760753032d2fc65098084f5f0d65a" or
hash.sha256(0, filesize) == "1e56efa7243a1d6456e47a8736751a3e338c837a1f1e27da3ca804544df41fba" or
hash.sha256(0, filesize) == "4bfc0214e7fa9f8aa864cbc8956fa3ffcbc9932a37863fee6217e42a3105ebaa" or
hash.sha256(0, filesize) == "69d85dc2b6dd936312d5533878926709eaf60bacb54251c4fa035aa9fd8f64e2" or
hash.sha256(0, filesize) == "b79e2e71a34db04466e40c11534e6b997210c52f4b28135c5336216b7f025236" or
hash.sha256(0, filesize) == "0a20f03af50d14404b349279b3dc20886b1bf91419ede3f1b0c7fbdda579ae77" or
hash.sha256(0, filesize) == "39213ba291c04a7c2092a8a0654c6bdf39da7ba769bc3a233de8882f5d597b01" or
hash.sha256(0, filesize) == "63ead7101148bab37e009da2b9969da6afcf47fffef3c2e09382ca17e0162015" or
hash.sha256(0, filesize) == "9c85397c39f7799687df61ffe6ea85231d6c7e02da9f8bb768d8d2681eec6d59" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_WANNACRY_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_WANNACRY_LAB"
date_IOC = "2023-05-08 03:27:37"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "WANNACRY"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "e8952e7fc097cf27f490b6efa585c6f4394db830dcfe14f669787949dbbfe3d6" or
hash.sha256(0, filesize) == "016f4a27b69d68bde5ad5c7bae5f81f012cf766d8e38a08f900a120b26d8f49f" or
hash.sha256(0, filesize) == "ce2194c96ebab334f8484a7a3e45e2c3bb74296fc5eddd335abf3f5c65f34967" or
hash.sha256(0, filesize) == "b15c431868dfe6d02a565442e46aaf7ecf75df3341e55cf1ce42dc761aa19bf2" or
hash.sha256(0, filesize) == "011c24bce46c2ded7236482e0e36530dd27c937e31a0896e91659d9acd7ceb69" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LOKI_rtf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LOKI_LAB"
date_IOC = "2023-05-09 01:01:03"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LOKI"
file_type = "rtf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "a48d7318f8a71f19109687b60d2a996c7b2635e1b9525160643187462b7f056a" or
hash.sha256(0, filesize) == "9acbf2ec6d4d9e5a0e0f373409bfaa540daf14a95bb5f2743a07c440f65f7a7c" or
hash.sha256(0, filesize) == "3e7a8b89e1117cd722953e34a1c9080e48f055056aa04eedcf0abba5de8f5c44" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NJRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NJRAT_LAB"
date_IOC = "2023-05-08 08:38:09"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NJRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "40ecf3ec9b5ca8e87f63e95db85bc256bacad1afc564aa93382d8d80c7c9aa8b" or
hash.sha256(0, filesize) == "2d8c76c2bbd7c5a00595ff4e870d1b4009006bb7befd48258d9c8859e440cc28" or
hash.sha256(0, filesize) == "e924442c7e088d452460e0b2710b2d118be929d58476496de18fd69340f8e007" or
hash.sha256(0, filesize) == "644e22017e7ea1528dca300ff5efc8a07f8587b3b15ea079ea0b9b205b0b4d83" or
hash.sha256(0, filesize) == "cd34e9df2818c24a997cb20b620af8498b42df79617e062e4802eae63d56cf32" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_VIDAR_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_VIDAR_LAB"
date_IOC = "2023-05-08 04:24:27"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "VIDAR"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "32bc8648c499ce2115cc18b912b59a64a1869f23102a06ac18c70c70be9385a3" or
hash.sha256(0, filesize) == "552fba4dc6da172a89fa1598a1bd3c62ad0ae663faf6548987999f3649144d2b" or
hash.sha256(0, filesize) == "43283967357b7430a89b8904c8ad5022759cf0d5e9adc6505e68e95d7eebf993" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_unknown
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-08 20:34:57"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "unknown"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "ac461923f315c985621f33a60d135715272d8b191bc2dfb5b94642233bd1b1f1" or
hash.sha256(0, filesize) == "e50ab59039f451ae841fb504e0a1337c34c6e4e8b40d477af345f9c1445dcd10" or
hash.sha256(0, filesize) == "2d65ab89e2f72fe03eae822528c575b88ef45c2eac901573cb69ffeb0023991e" or
hash.sha256(0, filesize) == "aa69f5bb20fe1168f9d41f0bfd4081bdb6971ed5d46b7c4a41b5d8357ab6ef65" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_xls
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-05-08 12:58:18"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "xls"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "b0b6f157d6b952826b3f20ace4d1da4c28d4e1fb5a29bfdeb641f8bc3651504d" or
hash.sha256(0, filesize) == "b5f0c66caedcadd343ae5a3f65f988860a253fa6f1f1c25493ebf928956468d8" or
hash.sha256(0, filesize) == "152a3a9807d0619f61c65233b73348a6a38586cee81ddba59c5daa1158b89c30" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GUILDMA_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GUILDMA_LAB"
date_IOC = "2023-05-08 07:50:20"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GUILDMA"
file_type = "zip"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "c4dbe99fdf11cf3b7c8b1646e68fc2bf67ceed0b2b188aff08735c1af1a5eef4" or
hash.sha256(0, filesize) == "22aae52b04e15a32e8c2e59942d284d758e56937decd31f7588749eb26163b12" or
hash.sha256(0, filesize) == "b70603ee3cb4595a977f9a0bf95eaaabe804ebe35ccb5eeebd0315989c71abb0" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


