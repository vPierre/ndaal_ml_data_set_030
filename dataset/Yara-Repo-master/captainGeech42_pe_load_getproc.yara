import "pe"

rule Methodology_PE_LoadLibraryGetProcAddrOnly {
    meta:
        date = "2022-04-18"
        author = "Zander Work (@captainGeech42)"
        ref = "80ecb9e09772f5c54b2c02519ed68883"
        desc = "Look for binaries with only LoadLibrary* and GetProcAddress imports. Not necessarily a sign of maliciousness, but worth looking into probably."
    condition:
        pe.is_pe and pe.number_of_imported_functions == 2 and
        pe.imports("kernel32.dll", "GetProcAddress") and
        (
            pe.imports("kernel32.dll", "LoadLibraryA") or
            pe.imports("kernel32.dll", "LoadLibraryW") or
            pe.imports("kernel32.dll", "LoadLibraryExA") or
            pe.imports("kernel32.dll", "LoadLibraryExW")
        )
}