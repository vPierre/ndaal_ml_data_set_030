// Intended for scanning of Atlassian home directories
rule Atlassian_Plugin {    
	strings: 
	$a1 = {50 4B 03 04} // jar magic
	// file names containing the malicious code ("content" and "ContentRest.class" are customly chosen by plugin developer)
	$b1 = "com/atlassian/plugins/content/rest/ContentRest.class"
	// class renamed into xml due to obfuscation
	$b2 = /com\/atlassian\/plugins\/content\/rest\/[0-9a-zA-Z\-_]{1,40}\.xml/        
    condition:
	(filesize < 15MB)
	and
	($a1 at 0) 
	and
	( all of ($b*) ) 		
}
	
rule Atlassian_Plugin_osgi_cache {
    strings:
	// jar magic
	$a1 = {50 4B 03 04}
	
	// REST module 
	$r1 = "com/atlassian/plugins"
	$r2 = "/rest/"
	// file names containing the malicious code ("content" and "ContentRest.class" can be chosen different by plugin developer)
	$b1 = "com/atlassian/plugins/content/rest/ContentRest.class"
	// class renamed into xml due to obfuscation        
	$b2 = /com\/atlassian\/plugins\/content\/rest\/[0-9a-zA-Z\-_]{1,40}\.xml/        
	// confluence directory plugins-osgi-cache/transformed-plugins contains the jar with more visible strings
	$b3 = "AES/CBC/PKCS5Padding"  
	$b4 = "AnonymousAllowed"            
	$b5 = "X-Upgrade-Insecure-Requests"
	$b6 = "Cache-Control"
	$c1 = "spec valve not exist and valve class is null"
	$c2 = "/bin/sh"
	$c3 = "yv66vgAAADQ"
	$c4 = "max-age=0.5"
	$c5 = "webappClassLoader" 
    condition:
	(filesize < 30MB)
	and
	($a1 at 0) 
	and        
	( all of ($r*) )
	and
	( ( 5 of ($b*) ) or ( 2 of ($c*)) )	
}
			
rule IOC_GO_Tunneling_Shellcode {
	meta:
	description = "Custom attacker Go shellcode ls.bin (3,4MB) implements tunneling with yamux and SOCKS5. This rule can only identify shellcode once decripted im-memory by the combination of Go libraries used."
	
	strings:
	$golib1 = "github.com/hashicorp/yamux" nocase wide ascii
	$golib2 = "github.com/armon/go-socks5" nocase wide ascii
	$golib3 = "github.com/mattn/go-isatty" nocase wide ascii
	$golib4 = "github.com/mattn/go-colorable" nocase wide ascii
	$golib5 = "github.com/fatih/color" nocase wide ascii
	
	condition:
	all of them
}
