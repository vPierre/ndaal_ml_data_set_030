import "hash"

rule RESERVED_QA
{
meta:
ref_IOC = "RESERVED_QA"
author = "Laboratoire Epidemiology & Signal Intelligence"

condition:
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_docx
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-08-01 20:39:45"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "docx"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "c49b4d370ad0dcd1e28ee8f525ac8e3c12a34cfcf62ebb733ec74cca59b29f82" or
hash.sha256(0, filesize) == "56ca24b57c4559f834c190d50b0fe89dd4a4040a078ca1f267d0bbc7849e9ed7" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_HIVE_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_HIVE_LAB"
date_IOC = "2023-08-01 20:06:05"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "HIVE"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "8756f0619caff132b0d4dfefad4387b8d5ea134b8706f345757b92658e6e50ff" or
hash.sha256(0, filesize) == "aafb0a46610064cd88ba99672e0f18456ed827cf46b2d3064487c45bac75637a" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_rtf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-08-01 17:32:42"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "rtf"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "d0f587d98a7b30811509fca9474b18611712db554e7f80a72f7ff759fae1008f" or
hash.sha256(0, filesize) == "92ee9736e63b48f5192b3c70251175f4347efc5d5bee4013b846c66e673db399" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_N_W0RM_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_N_W0RM_LAB"
date_IOC = "2023-08-01 16:20:24"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "N_W0RM"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "b982290b6daded567141491a2656e239cc761f189695b179bfe079937cfc592a" or
hash.sha256(0, filesize) == "0a2a3e6eb5d920e00c424039f54ad1b9f66ecf1c7f854c544e9b2e22d153c9ed" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_hta
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-08-01 13:35:38"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "hta"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "a8b6bac764f3178d130f50949c19f37c5310b23cdc0cba66569e047b732844c4" or
hash.sha256(0, filesize) == "cd0fef90cd95ee8682d15ed06428d82fbdebb7532d09ed965481fa5467d71d3c" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ARKEISTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ARKEISTEALER_LAB"
date_IOC = "2023-08-01 10:57:02"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ARKEISTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "bd5aff6936d77e3deae4e45195b44ec5d4e7ba4f2a9dfe68ee7d6f7be2cfd97a" or
hash.sha256(0, filesize) == "5279a9a1f2521b29d83b608c4d5c9e8cf539dbff4ff730b9811d613c59205a3c" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LU0BOT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LU0BOT_LAB"
date_IOC = "2023-08-01 08:59:33"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LU0BOT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "e6bf861332a771e037a76546d095dd752db63ba0e9fec254a69e0864ae248921" or
hash.sha256(0, filesize) == "4a6ff95b69e3af76e8b36ec5de23b7dd5f8edb72f86a98a710da1dc08f41d799" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_PRIVATELOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_PRIVATELOADER_LAB"
date_IOC = "2023-07-31 16:42:51"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "PRIVATELOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "67aa172caed83272300ae72ef7caf0f892170c2bfa347c991b19f7ad3dd3912d" or
hash.sha256(0, filesize) == "ccdaed2d99d145ec6354aad6e431fc60e16bc99f2126911ee3ac56ac7159dcaf" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FORMBOOK_doc
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FORMBOOK_LAB"
date_IOC = "2023-07-31 12:25:04"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FORMBOOK"
file_type = "doc"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "2ffb25af19a71fd2c2677c7f71855e27892525b3e4986377b9d2d6b998c5d5c2" or
hash.sha256(0, filesize) == "1234892874714a8caf6af5807ad237e0027ab75045f0e7b82741e00f8d7b5540" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SPYNOTE_apk
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SPYNOTE_LAB"
date_IOC = "2023-07-31 10:07:31"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SPYNOTE"
file_type = "apk"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "7a16952bea9714cad6330960898edf6262c0e5e19d0b47716e31b6cc24463e07" or
hash.sha256(0, filesize) == "bad77dca600dc7569db4de97806a66fa969b55b77c24e3a7eb2c49e009c1f216" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ZYKLON___exe_
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ZYKLON_LAB"
date_IOC = "2023-07-31 09:25:24"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ZYKLON"
file_type = "__exe_"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "36ccccca3b77da2675a76da285f3a1a2654d8d1835adb6e4a4eac667d9a27711" or
hash.sha256(0, filesize) == "7894e439be9c591dd2c64b93212ca0eedc869802fde696ef3a23c5bb4cce5238" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NETSUPPORT_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NETSUPPORT_LAB"
date_IOC = "2023-07-31 07:40:46"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NETSUPPORT"
file_type = "zip"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "8d8387b1a4374853020ad43af4ed738bfd6538738c448b7b4fbc61b61da79ba4" or
hash.sha256(0, filesize) == "9f9502eb3d4d529cc058241a6436047409a0075d7ca88b6ce25313a75409b0a7" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_VIDAR_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_VIDAR_LAB"
date_IOC = "2023-07-31 07:24:41"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "VIDAR"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "48987d9c89542a8cb4f8d34eb34902a4762cc8643c0e491deb6115907db4887b" or
hash.sha256(0, filesize) == "669db7933d32ae2ec86d164c4474b6643c2af453646c12f9f0f8e9716cb71099" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_RECORDBREAKER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_RECORDBREAKER_LAB"
date_IOC = "2023-07-31 07:20:56"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "RECORDBREAKER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "c4395ae438ce235952f56642e133750c1fbcfc01275e77402425f549cdd2805d" or
hash.sha256(0, filesize) == "ceb0b34bf3d2f9ef826aefe57e9f1c599925a5c57cb35425a5af808c5f1a979b" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_MODILOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_MODILOADER_LAB"
date_IOC = "2023-07-31 06:54:28"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "MODILOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "a40e51ae0b5734285859813b2a74d104789a356d96f64382404404565c6f9960" or
hash.sha256(0, filesize) == "baf94f8cdd24e33df2940f4a38fcd40c34a3091a51dd26db10374d0f7065b70d" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_doc
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 06:33:42"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "doc"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "e376444e1801cfe19e05e015ce935e120022d913655e74f43d403ee0820f397f" or
hash.sha256(0, filesize) == "1dfdd574a50e7447c06ff2299ba112ab5e7bf56077a0d3ec87ab63f93531ed46" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_WSHRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_WSHRAT_LAB"
date_IOC = "2023-07-30 20:51:20"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "WSHRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:

hash.sha256(0, filesize) == "60782165f5bb1099f28bb39ea3150ee08a6281701f1ee647b00a4220d36d1640" or
hash.sha256(0, filesize) == "7e160f885fe15d7f5b67e3d321c1bd8240a63bb80c8156f604829f0cbadba313" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}

rule IOC_NA_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 01:29:09"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "8c8cb887b081e0d92856fb68a7df0dabf0b26ed8f0a6c8ed22d785e596ce87f4" or
hash.sha256(0, filesize) == "326a159fc2e7f29ca1a4c9a64d45b76a4a072bc39ba864c49d804229c5f6d796" or
hash.sha256(0, filesize) == "9d9e949ecd72d7a7c4ae9deae4c035dcae826260ff3b6e8a156240e28d7dbfef" or
hash.sha256(0, filesize) == "7b379458349f338d22093bb634b60b867d7fd1873cbd7c65c445f08e73cbb1f6" or
hash.sha256(0, filesize) == "9535a9bb1ae8f620d7cbd7d9f5c20336b0fd2c78d1a7d892d76e4652dd8b2be7" or
hash.sha256(0, filesize) == "5f4b0aa22ce65b30fb232421673fad4c126970928207ade256d3bfee33dc3687" or
hash.sha256(0, filesize) == "806761850d19f0cc9f41618e74db471e85c494e952f900f827c1779f2d1c4d31" or
hash.sha256(0, filesize) == "4550635143c9997d5499d1d4a4c860126ee9299311fed0f85df9bb304dca81ff" or
hash.sha256(0, filesize) == "38e66e1c80433f2a4e16a708f8cb5e26ed32963f38664ffe398827271d7f41e6" or
hash.sha256(0, filesize) == "87d36c48bf6d1d9a3b157aaab45ae162b78b79b0c956383a670dcc7d9d7c14e8" or
hash.sha256(0, filesize) == "0992aa7f311e51cf84ac3ed7303b82664d7f2576598bf852dbf55d62cb101601" or
hash.sha256(0, filesize) == "480cea45f9c10159ef76555a0b86c25b232952b5cbc6da2862ff4b8cbb2943c1" or
hash.sha256(0, filesize) == "4564ca0c436fde9e76f5fa65cbcf483adf1fbfa3d7369b7bb67d2c95457f6bc5" or
hash.sha256(0, filesize) == "d8026801e1b78d9bdcb4954c194748d0fdc631594899b29a2746ae425b8bfc79" or
hash.sha256(0, filesize) == "4368f30798a1caa0a7b30735111e143068678a0547dfd38c050926619869c73a" or
hash.sha256(0, filesize) == "ef25f37fb988e1e041e5dbbd6f30aac3918e540fc253964b054fc1ec6e45b6a2" or
hash.sha256(0, filesize) == "e69026db820b4aecb17d98bf3cb9f40b78758232a5b45b5b7ba84850bd9f9ec5" or
hash.sha256(0, filesize) == "cffcae5f9936636f8c3835a038b95ab44533be813290d67b83883f6356da8359" or
hash.sha256(0, filesize) == "2a0b83c316219ed8c7ce1d14edf09794fa76a71cb04348d2a332991f3fceab2b" or
hash.sha256(0, filesize) == "486e5a611e29d76bdd2cfa9fc600931539f920b2552eab45e3dc7878b58a19d5" or
hash.sha256(0, filesize) == "62d6772ba5baf00adf63f09ee7c84b19b78d299155dad37d6a28ede9b4cd7813" or
hash.sha256(0, filesize) == "6a2a5ba52b4a649cc18e3c506525e9f426fabedfcf86fc9ff18b63b0e9e8e3c2" or
hash.sha256(0, filesize) == "21bfa985ba51579c65e40c0c07064ed5f882a82392075b584b7c4019e60ea962" or
hash.sha256(0, filesize) == "d940cb43e9c0bc8abbe36a5ff2ee5949aba5cdc122323f14e80d87b37b76f106" or
hash.sha256(0, filesize) == "0c21cc2b9bf1e49a8b2eada21a695170c89a52fe209b13c6b136cb189fd62abb" or
hash.sha256(0, filesize) == "fa117f704146848a5582058c90a591c994d9e12eb5292a1ebc847db2947a6100" or
hash.sha256(0, filesize) == "4d4029e75c3722ea557201310fff604b84c52bdcf85a9e5c529981488628a1db" or
hash.sha256(0, filesize) == "ad19f8621b5665218b6e4f6c43cabe8be69d7d8cf2ea9494e8ead66042389e78" or
hash.sha256(0, filesize) == "4ad51ee71da26867904c568a85bd257a3ebe2afdb5d1a4b6c7746c19b1743173" or
hash.sha256(0, filesize) == "a216650a36498389a10434af63324705ac991f815bae1ba65d9ecf31f71862de" or
hash.sha256(0, filesize) == "d1aef9b3fe6c9d795a51f2bca66634d3165421aeb02c293004b652d90cbcb434" or
hash.sha256(0, filesize) == "d4b6eb6b90b35b44d38bc1ad63b00eb6fe2e74ddb035cbfb40a7ab0203ea528e" or
hash.sha256(0, filesize) == "b805ee9e91c4716831f7f44ab17a2e58ec9f90c73be93cf7468fee62edcbc2d7" or
hash.sha256(0, filesize) == "bcf6541c870bfd5bbbcb4c085821daf13e432f6cfd88bf9139cfcba3dfdd6c76" or
hash.sha256(0, filesize) == "8678f9a3ecfd06cd0f5f2ecd34414ccfd0dc475b8f7d4269e330b5935a8e8f99" or
hash.sha256(0, filesize) == "4f4992ebc64f845f531c2e7f3c1a819037c685686ec5a9af906eee4d363908aa" or
hash.sha256(0, filesize) == "eb1d4c2d7b6416e64eb5bf185f32580c89d314c003ff165fc30f9fb959cbac93" or
hash.sha256(0, filesize) == "09b6791aa44d4ea5ef9fde46e065d4088148d90748a2c5e65305b116e09ba08f" or
hash.sha256(0, filesize) == "e74f215b1c07d7c1a94bb07d6366474fdf70edb2baeb570f260d7260c2618116" or
hash.sha256(0, filesize) == "2e1ac1d041b43d2a42fde84341e9c2f048ff77d6ce0ffb6b2faf072ee60978a9" or
hash.sha256(0, filesize) == "e87c338d926cc32c966fce2e968cf6a20c088dc6aedf0467224725ce36c9a525" or
hash.sha256(0, filesize) == "ed1f7fd8f537cce31429bdd3687396e3af98d88c98923c575f822b1a63551f44" or
hash.sha256(0, filesize) == "d1aab5929869cfe69e83022529492d8c60f749c4e2c33af45da5adee88435cdd" or
hash.sha256(0, filesize) == "ea1816e340b5b6edf28008dcfe9946b867096a47ae1cccf023ae350301b3c088" or
hash.sha256(0, filesize) == "a5099cd426a4ba8bf32e38db26ef1bbdd7e75a8a5cc052727e771b66d42ad18b" or
hash.sha256(0, filesize) == "73f2bda2748d084de9a966db5a390504cc5bd65f030492ac50861d2587b49e7f" or
hash.sha256(0, filesize) == "f8e32cb5e526179391a15edf9525670dc8280b40dade1b8765d5d33ce125ca94" or
hash.sha256(0, filesize) == "7b6910c25b8a4c1851786ec2364bb997336e88fbf35b3cd55980f56943c9e2a3" or
hash.sha256(0, filesize) == "0214e02ea9c904b6c739dd9b38bcb689962929c831324e923325de227b3faef1" or
hash.sha256(0, filesize) == "b25a47645766d7dfa183d33a80e337be5adab6a401fbfb2e6d9722e7b28ae54b" or
hash.sha256(0, filesize) == "2fa0c6ca9b2c352e6fbac1ea28a12f4662665b39f8ead973c497b1fac8d1e290" or
hash.sha256(0, filesize) == "4cb622e428772576bdcf156ec4030b782f0764fd6ea9da4de543e5723fb62b9d" or
hash.sha256(0, filesize) == "94df97f56ad0b323684f14b54ab8858af8e9c0a442ce31e07c342fbbb41de5cc" or
hash.sha256(0, filesize) == "a8b132b5fda14468aed8a5f270b664dd06c12f8788ff0c9419152d0577e9fd16" or
hash.sha256(0, filesize) == "d5dff38d0773eefad7e6b3fe7005e8ace7c37fc9a6b88eca21f6120d2b860f32" or
hash.sha256(0, filesize) == "2522e04f7abcd7c32d2c73aa0e66d97d0d121e86aefc7e715dd013e8e27a73f3" or
hash.sha256(0, filesize) == "95c06a49c439b9c6baf3e39786a25e09c065e407c6b9bb0ba0e31a0bd8f12ad8" or
hash.sha256(0, filesize) == "60d96d8d3a09f822ded0a3c84194a5d88ed62a979cbb6378545b45b04353bb37" or
hash.sha256(0, filesize) == "38f8b8036ed2a0b5abb8fbf264ee6fd2b82dcd917f60d9f1d8f18d07c26b1534" or
hash.sha256(0, filesize) == "fa62e3c36f1f9a3edb0b33038b8d54ffac403ceafe9fc1b881e6e9f49286043a" or
hash.sha256(0, filesize) == "26f8e4a26de81a75b403875c04a6377e2ff65bd861f1febe3e2148a8081ca438" or
hash.sha256(0, filesize) == "e6fe70cf20e6a993a19009f8b01eb77d839579840270ba3478fed0b5c4ba3c0a" or
hash.sha256(0, filesize) == "60f95a88f8557ededb3465a372987ccd2642ce875916b57ac2e276c44f5f4fb7" or
hash.sha256(0, filesize) == "34a51dc07bde9cbe8888d913bada3b4f66240ea94e035f49c9c1e4aa087d489b" or
hash.sha256(0, filesize) == "e7910bc87f0092c72a683ae1bb28ed7ba91077bdfe38dc82376f154b2ccf022d" or
hash.sha256(0, filesize) == "976f424ce6f94f179410d53600426b8dfe5ac15a2dd3c7ee200350ad3699e4e1" or
hash.sha256(0, filesize) == "585db67ebb71c0a0e1b8a33ce6f2a45a6adc29cc7222681f7fba10185681a3cd" or
hash.sha256(0, filesize) == "01ceef81d39a73f012b399e4fc3d93a955236a226fb044e27c77127804e31cea" or
hash.sha256(0, filesize) == "e2174975292ea851f0cdd7c0386a224575fde9a9b6ca42b539431c01f5cdb310" or
hash.sha256(0, filesize) == "79ef73f35651b337d974ad3ec5048033b9aca0c38f3709d2ebb5817085eaf3d1" or
hash.sha256(0, filesize) == "d5760282582a6df0802ac704fcccddff3ce2be07cfec176a80a6b683082a4792" or
hash.sha256(0, filesize) == "10aae97b22d925283d023822c26d5457a1048ec1b8782a4568aff9d725dc04a0" or
hash.sha256(0, filesize) == "69ef55ef19c5ca6376a3daf4f42752cbe03d4fe0356272f0b00292946e27e530" or
hash.sha256(0, filesize) == "cdd9b66c16de51ebf5863b1dff82b2ce347efce4d93b9be9d40a47ecac04b4b8" or
hash.sha256(0, filesize) == "e3725f57d26d63fa6e326e5cc622d408ebb6eb9c2d7468eb34c3b79f42c07169" or
hash.sha256(0, filesize) == "3d4c1e4e446f6bbfb9f8f8d654b090d7b459a73425c87df8c008bab390f63bca" or
hash.sha256(0, filesize) == "9b5ab61cd8172b3b603f1836ea13a3fed6d54c4af403c5d3050554eb7ee7606f" or
hash.sha256(0, filesize) == "a17f463792710e222b6bbb4baf87455ce30c06b2213b897165dc9529826071a0" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_REDLINESTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_REDLINESTEALER_LAB"
date_IOC = "2023-07-31 00:05:18"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "REDLINESTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "747a43c82c4a13158da7adc6634bae72b5b7aafcd9214cbd2694bf5d60999369" or
hash.sha256(0, filesize) == "9831eaa1e5436e1c1765902f904e6500b9f46fa63b3e8023ea7eba24b95063c1" or
hash.sha256(0, filesize) == "2ad50e8b657ffe4828366983a2a65bc676fde41727693bc15f6f43f66b83c928" or
hash.sha256(0, filesize) == "d33001b8fbdda1653da2eadd428e5c9c983f1514b56c7ba60dd15e3aa9d68d9d" or
hash.sha256(0, filesize) == "6a2af5ce271d6550958866ec9b3c609f0307d0f1f216bb547e9de9a532c06558" or
hash.sha256(0, filesize) == "5329d964b869b30fa824a883ad2ead0f44e9137ff2826dfb0280000588a6722e" or
hash.sha256(0, filesize) == "9feb069969bb7179830e96f4e6520aad8a24839fb5589bacfc7352eb3a179c68" or
hash.sha256(0, filesize) == "be217a59ed88419ed1523e7a9b519895efdf0f25f5fab20bda7a003a60959fbe" or
hash.sha256(0, filesize) == "5c249d73d8a1451c87291303f8568f8602ee6c9eaeeeaf304e484d00e0a00fa9" or
hash.sha256(0, filesize) == "c6d77f6bc9bc12ce2a14dec13c7f1eaa1f495d6abd61be3f53e11d4b63af8318" or
hash.sha256(0, filesize) == "9b398e52ec699eef001bf8298ee3f5b8e0366c7ddf4c16fff5b6f82cf83d7029" or
hash.sha256(0, filesize) == "74add2a645f5132682b694baf6326ceee6b920eedc596ff77f827de13789f7e6" or
hash.sha256(0, filesize) == "2d822e644473f2088a70c1d54efb299e7e9c97de15210d118a2cbcb49b77fa93" or
hash.sha256(0, filesize) == "8441a6306008e6ab7681cd1ab5583ae26d7c8d60e85ec73759af901f756a5d35" or
hash.sha256(0, filesize) == "8f61fb247d39dfb97f59db9374ac43793fa6432057a6e1d76aa06a22ea9f3ca8" or
hash.sha256(0, filesize) == "3c42b93801f02696487de64bb623f81cf7baf73a379a46e1459ca19ae7dc2454" or
hash.sha256(0, filesize) == "ade21a7060ec863cd1b596f5fe0709d1691c5b83b9d0df90ca0be1c6606026f4" or
hash.sha256(0, filesize) == "6ca29012d6cb607eb6d565283eab3f55a1855417a2481c86f3f2641baeb45223" or
hash.sha256(0, filesize) == "3a2cd3bb7ec786ce38aad12d7f3513d6923aa5cde286fec2ff52b8f5dd1fdea9" or
hash.sha256(0, filesize) == "594a7cc189e0d4deb168da69b3c36d805ff56d4174a02f01f0b9c93fdc7a05d5" or
hash.sha256(0, filesize) == "31b442766d69f7e989e3839c60b3d0792745ce2e97461495559d3082f5b0607c" or
hash.sha256(0, filesize) == "e51fec6dac7dcf6e329afb113aeb9539924ecb47cb18a621438c09d756debf93" or
hash.sha256(0, filesize) == "aaa5a230ef1ad7e160ae67a715f74935999b1c84674706d3afcde2afe1f38a22" or
hash.sha256(0, filesize) == "631f60869b2b4ea6f83975904972780456a83d8c4d9eba5c84bf0bb66c45dcec" or
hash.sha256(0, filesize) == "990debcd18c2cda7bef7a8b78a747ef4201a9324ce1b5176f82bbb942e65ad1f" or
hash.sha256(0, filesize) == "469d1c9d8249dd25b4f09291a612e3e1534053a29e8b8cdc40974b2f8b866563" or
hash.sha256(0, filesize) == "924f000ee5e3b7cd618b1f9d9d7e2203f469fba4536a86eb022d718b71f13e96" or
hash.sha256(0, filesize) == "74555aa1a0f797262f25ee23d6aef085880e4267b9f3973b97d6205cfc66e922" or
hash.sha256(0, filesize) == "74f4632c70177bc53bd951fa37c1053796017b6b8a3d6d58a281fab70af7d3a3" or
hash.sha256(0, filesize) == "03c9475585d70e21788016891d935f83c475a4c2ff12b261b0ae964dba32a2e1" or
hash.sha256(0, filesize) == "8e72d1b1e6f190912da44a5402ea83f7a8cf98af7f4344af8df6781e056b2f39" or
hash.sha256(0, filesize) == "223f8d67c784e3f6cc85c721dd718af53510f6884dbc1ea4dd328cc26da03f5e" or
hash.sha256(0, filesize) == "b72afb61c73671878ff13ff431a7a37d79b9c3c940c7aad1dfe2b5973a89d5b4" or
hash.sha256(0, filesize) == "04ed4093d05e068101d7b0ecf8e485dfbeb27d4c98d38c8e562c75df70607fde" or
hash.sha256(0, filesize) == "e3119e0ff31316bdc580183591b2c5555667e2af422968916d89def1c0559360" or
hash.sha256(0, filesize) == "6e183fbb7006572b28fe4283d9cc9f240913cc376912885e6927d7ce3687eb15" or
hash.sha256(0, filesize) == "3b9358c613ce407235633a92b5aec1be67b941b2228cc6b6698253f899e4d68a" or
hash.sha256(0, filesize) == "5322d719bf93a1a513cdfb3fb04e20e15b060bd2d2566ff36b112ef79819f0b4" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_STEALC_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_STEALC_LAB"
date_IOC = "2023-07-30 23:30:19"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "STEALC"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "fd1af7b25adc2346a8752e41c68aa8e94b5fc2d51b257e12ed81f5a7e7b97c16" or
hash.sha256(0, filesize) == "0d46b13d71ca5d6f0d261313969c6e35cb061407339fd3751ea496bdfa06f0f7" or
hash.sha256(0, filesize) == "4313a76cfa154e393aff5075354e22bd46516309c8c5dbbceef18c21eab0d27e" or
hash.sha256(0, filesize) == "912a69862b4f70093e5fb456a70b75c7c1ff187ef42cbbcabb68c6c7936eed78" or
hash.sha256(0, filesize) == "cead24187e759a0ddf49d1a67476fe0284b586c1aa5066c189879a143e7966be" or
hash.sha256(0, filesize) == "78aff71e97c42a0151e6352169a345e0b31af296beed8d3bfe4145b451fa076d" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_iso
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 16:29:19"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "iso"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "1fc7b0e1054d54ce8f1de0cc95976081c7a85c7926c03172a3ddaa672690042c" or
hash.sha256(0, filesize) == "347715f967da5debfb01d3ba2ede6922801c24988c8e6ea2541e370ded313c8b" or
hash.sha256(0, filesize) == "fb8e52ec2e9d21a30d7b4dee8721d890a4fbec48103a021e9c04dfb897b71060" or
hash.sha256(0, filesize) == "b38de1920dda3805754ceeeff617b5c9c0300213682f8ea9ea9ecf6ecd240761" or
hash.sha256(0, filesize) == "405d77b73596a9feec41b2d97fe7c78279485e9c557748c76cc4d31ae025ec09" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_dll
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 02:28:28"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "dll"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "8078d4866aeec4d686472aaacc455cad0a1f620c464b649ae919eeae0f097a76" or
hash.sha256(0, filesize) == "8b32e5df0928da99bb6307484132eca333fa29f675345360d8c804e3a18ddd51" or
hash.sha256(0, filesize) == "c401070db22f1fa3a5dc170b4b60920c8dde1d1bd7f0404952c13e897f07b820" or
hash.sha256(0, filesize) == "9bed965557631646dc5f0bf1126a9da3bf9c8c8e92e792055f981668e06c3708" or
hash.sha256(0, filesize) == "1d154a37cd713680bf7fb3d6ecac3873e948d8aa6a92d8c2b9303fe288528054" or
hash.sha256(0, filesize) == "b58c634e29b462ab79276a427f47900474031a284783c8c2e7ebab2d4d994059" or
hash.sha256(0, filesize) == "444834cb42a8446f97566f6e9e449ebe1bc7cfd238d89bb55953507dcc3d8968" or
hash.sha256(0, filesize) == "f8fdc0c6ce42aed3a2a46809d9a458d0c22a88757df1c2fabca6f75704874cdc" or
hash.sha256(0, filesize) == "f51a413ebb9524ef80d4e195a00bfa07bcc268ddf6f2b714333c0bf1dcf9709f" or
hash.sha256(0, filesize) == "aad0ec8b9b756bffb21084e96e99ef085450cb3798f8b7d6f8451a15dc079b5b" or
hash.sha256(0, filesize) == "a556e576a196de79eb772e958dd89208d946cd08e1b74d7faa5dc1d910f31e69" or
hash.sha256(0, filesize) == "9dd7892f043aa0921a262195eade2afdbfc7a8d7b31479058d6bb0e934d98cad" or
hash.sha256(0, filesize) == "40417b7f85176d01898329bc7c9764df6c5bad1d0482ce5e1a02be8b9566c4e0" or
hash.sha256(0, filesize) == "9f2902e656007b121f0f6195a281ddf32c3ca10fd4e5752e095f9f12d589ffa2" or
hash.sha256(0, filesize) == "dee2d5bac7e8da024013d8642120381be50db585f5277b9224ad87fd511b6aed" or
hash.sha256(0, filesize) == "27c697b130c816b682bd8329d344371768a95d276d3d9621051f926f77a3315a" or
hash.sha256(0, filesize) == "f3256c298bef3b4070df894a2497eccb7619554966299f2a4c26fcf94b7d541d" or
hash.sha256(0, filesize) == "cc90528ac686d0cdcc0bec6a2b7a73dbdd2634116ea82ccaf7b0c176adf0fea8" or
hash.sha256(0, filesize) == "98fba428890bffdb22b1ce6d8ce6282c43a721d19f605d2cc40af208f050ac9e" or
hash.sha256(0, filesize) == "3f85ff00fe071f9d58be56b27ecfb05bb1cac2bf311eda467f20d8b0ad2e3dca" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_unknown
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 06:45:42"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "unknown"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "4a88f9feaed04917f369fe5089f5c09f791bf1214673f6313196188e98093d74" or
hash.sha256(0, filesize) == "9ae22f4e7ca81f09ec3318c9f6dbf65adbbfce9dd553cb8f7530671567b028f7" or
hash.sha256(0, filesize) == "caab5620f256b860cb9d75afcee61cc0c2d9f4b082014d9eba8b38a6f1dc90e2" or
hash.sha256(0, filesize) == "abfbc47903276e0c4e2174fb7b9075d754f82a8504eb2b47fd7c6cea01989b87" or
hash.sha256(0, filesize) == "5add56525668381cfba0beda52efa2f5732a0e7de96eea52b927a10136756776" or
hash.sha256(0, filesize) == "f7114bfc2804994501012f15cc8306955b54bd70af70bec6f4d9c4ea012a1324" or
hash.sha256(0, filesize) == "566a6ac5d85558dca7d91914abc5d683ce9e778d45e1ecc820f53e885d3fb435" or
hash.sha256(0, filesize) == "3ad57e95facaf9acd8356d0054e2821719b7962210cace57ed9a9146636c68b3" or
hash.sha256(0, filesize) == "25d168ed7d026546ff4edf5f42e470a37190af18910b210e35ee6ec919ed16df" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NJRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NJRAT_LAB"
date_IOC = "2023-07-30 21:35:15"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NJRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "98866ab8f86171f241ddc77e880188e06102d349cbe6c029e1bb86a1c24b216f" or
hash.sha256(0, filesize) == "53446fe57f6c5a19150e5cfa43c2e48fe5ec92bfc8da1006269dd008e00d07b5" or
hash.sha256(0, filesize) == "5197774889d7540ec8a8aac9dfef9f6367f6977de9314ab6ceca488f9145e417" or
hash.sha256(0, filesize) == "17ce77ba51558d7591c2563cc6a528e03b2a5aa17f9383d4ff8859541b2a5ee9" or
hash.sha256(0, filesize) == "6f0daa56b7cb46e562234df38a3e1d3ca5760372b9dc1e9e5ae36196a1d59c16" or
hash.sha256(0, filesize) == "e34e9a08c3564e231436f8fc5e3ce15110ca8aff3d3cf9e2a76b8f17f96413d2" or
hash.sha256(0, filesize) == "f9d2cf8071acf5b9a920caff4ad7e8f6eb57f4a074189222b76285768aa00e27" or
hash.sha256(0, filesize) == "b696409b52eb8407911cc75ea6e480717f64784179a2f33eccd95d0a37c68d5e" or
hash.sha256(0, filesize) == "3a4864163edd1ccae20fdfbcfbabbdd49c70f923f29c0f4a8c1687fa5c734eee" or
hash.sha256(0, filesize) == "562e19c3874e4174fe8d0d57b2d7a7d9698fa3dc3797e952172f77049c010ef0" or
hash.sha256(0, filesize) == "0252a2c1d321499ce83b232e636d6df82049bac70697734c1bfc3e8d2eeb4f7f" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_DCRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_DCRAT_LAB"
date_IOC = "2023-07-31 17:05:21"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "DCRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "2376875fddd58b0881876aa755b377a257835e5e3b522bbc31eec7a363b2722d" or
hash.sha256(0, filesize) == "5700162b095157adc7f790e5e063b941411bbb33de6651832851caf8ae698e59" or
hash.sha256(0, filesize) == "55d398dd395a13c92b854cb5f51d26fe3879e6b5b2d2d6f4b98324dd3be7326c" or
hash.sha256(0, filesize) == "43deb83f3cfe3efa5c3f293ebe23ab68a08b3136a098fb7302882fad99564926" or
hash.sha256(0, filesize) == "d1a77a440b5de12ada96a899834b36d2a5beef617db3b2cb9c8f0926f6f998a9" or
hash.sha256(0, filesize) == "06a684578ec541ae39a1c1971cfb40f5767a09663d9a46e09042bf336e7cb3ff" or
hash.sha256(0, filesize) == "3374d0c0acdff4f67bc219fe6a311bbc21d70271099b462c097c86d043f8dbab" or
hash.sha256(0, filesize) == "4187623c2862328da86414eefedf4ffc231a3f39011d6791d23e94a8eb6e84a9" or
hash.sha256(0, filesize) == "d13a78c8609a7611f97852c7aca5e8d8984407c46c2a8c5084192ba5e4494b19" or
hash.sha256(0, filesize) == "3fe08bed843dedf8cb769b5606f8adb5c2a1cfdabd6d8b3445a4b87c0cc35733" or
hash.sha256(0, filesize) == "edbfeb8823137279327b61ab1c7d73c8f6e72f2234fc0558ed43a99315171023" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_MIRAI_elf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_MIRAI_LAB"
date_IOC = "2023-07-30 22:00:04"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "MIRAI"
file_type = "elf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "446ae979e771f879fb90914ff7540c31f5837f57b5c62e3408ce218357305577" or
hash.sha256(0, filesize) == "2d542e5b90108267d96db6889066015331ef41071b7d7395e5fa4c9cd43b1229" or
hash.sha256(0, filesize) == "84a4e4ce6b44fcd1ef97213395de5f377bbc6af0985e271099097b5a047c64bf" or
hash.sha256(0, filesize) == "5633cd298a4c12d09ec647f9644aa3ee5f502d33a29368f61b402e3261c51208" or
hash.sha256(0, filesize) == "affc7e442eff17be1d36321f7b07a89c26d018f8f2df5975d5f2a6fb50cc0455" or
hash.sha256(0, filesize) == "e55acc671f6ab382847dff0572be97d4b1d33a6a3ed1f9a998fbbae19beb6ebd" or
hash.sha256(0, filesize) == "aa66b99f43549b9d5c7d9c4d927d93a8b8e06c2fadc122418cd40e6698b9f186" or
hash.sha256(0, filesize) == "f74e19adf4d19eafb6b1a13c4fcafe62c008c9850fbfd488dd07773894eca6b0" or
hash.sha256(0, filesize) == "bdff815241aedf4b44015988bb3736cd9615d37ea791c1f99dd737a1b68bc8ea" or
hash.sha256(0, filesize) == "e1ab1f8f7410899c9d0ba918d7b6216fd37a90984f44aa1df94207f69e80178f" or
hash.sha256(0, filesize) == "44a9154e7e44eff27b165329bd5fe967b42472e78d39f358fe559d9f80b2d2ca" or
hash.sha256(0, filesize) == "81f798796248b416b2c90cea1819d7458c615ec0d6b5726577b101f5bca2496a" or
hash.sha256(0, filesize) == "74d017bc05b5c2fdf87ddb45fabc7a732bc513de6d8a641b1588477d698df43a" or
hash.sha256(0, filesize) == "c6656fcf634bf2be4c509383d9fb84e0a48a53e0a4595f00e4278af429656d44" or
hash.sha256(0, filesize) == "8d1c1ee8c1ff44350625b978781d5bbe5014f22522908fb465615a4f8f9e6b34" or
hash.sha256(0, filesize) == "6619ccc9e53f0933921368ddb15dc03f409f417bd1f994c2748b55e1928fbee3" or
hash.sha256(0, filesize) == "070e5ade7a13b9220cd9c9de981edab0abfd87c9a29991209d3ac0a30e95fbdb" or
hash.sha256(0, filesize) == "b435277ac428d968821c44d98673d6b04e73ca054723d2f09f42f2245777bec3" or
hash.sha256(0, filesize) == "a3944cf1b59a8481386873d6fa131c9e7fbb85ae0b0642d65d0962f94a2e3dbe" or
hash.sha256(0, filesize) == "9132dce93a1db68341cd4d7cf79411cdeae2cf2a1a3f64a805f9ec50de680fe8" or
hash.sha256(0, filesize) == "83b023c87016de3b7e4633773cfc1c034f8923da968850c998bef0660431df70" or
hash.sha256(0, filesize) == "19b6e4ca25940457310af99a3498c6043adb0ccece6d3300ad828f746dc095b9" or
hash.sha256(0, filesize) == "2aad2a03c18e8f87f0669a8083c5ac2a96e15a48cc2f2d6ee762898b15f48e69" or
hash.sha256(0, filesize) == "7722969303bcc72aade0f9688089b9f24f7abac2a47dc9170abde533fa745ffb" or
hash.sha256(0, filesize) == "f0f911e5023140e3b401540d9b91c66f56ae21923255d145779c88405e88f02f" or
hash.sha256(0, filesize) == "4d6d22aea7a1147911f99a57718a41b144b0c70761198629cb5d13d0b86c359f" or
hash.sha256(0, filesize) == "db72c6b7f6598485b9fb3e1121d380c7d12fd154f3b9143082058c2b1d9a5a86" or
hash.sha256(0, filesize) == "c8e7520a99e0beda9f4a630bcf573ce1ea9fa8a65a15970f03a3c43d70d6e83a" or
hash.sha256(0, filesize) == "94afd8ffd4c2ee826de913991d5d12a9b6ba3b357462516adf41f925c0434d2d" or
hash.sha256(0, filesize) == "cc989ac226f0966abdb7d9d61df8b5b47006b0ad2d37b2b4137bbb1959c0fb11" or
hash.sha256(0, filesize) == "08c80c24e4a1e6c0bc041f86cfd1f1ef9372389d19530cd94ae3eeaf4cc49d5c" or
hash.sha256(0, filesize) == "8d580e8e868cd572c4b93c94f2c521f7d02683ba0a485014d6cc7b7e067bd84e" or
hash.sha256(0, filesize) == "9f397a81cfdb6e5372d5a9f8baaa1a3459208edb949264c2241eeef818e56034" or
hash.sha256(0, filesize) == "87cc0c1bd526b4cd967d339a123af6326a99933ac7dd15a016bf256839c0d82d" or
hash.sha256(0, filesize) == "6accc44817efa38c1d6111866dbd8f390572b2c98cab363be3e4d56b8d4e3e40" or
hash.sha256(0, filesize) == "1cd60a0e6cd485bfb3c80262bf805927ea6601f2a38d8034787c50d59e580642" or
hash.sha256(0, filesize) == "6da9d42ffc8bc8aea31250dd656b573acd9e198eedd960593bb06ffd946cd061" or
hash.sha256(0, filesize) == "1304bc9af9b32188743e9d749da25212c879ed1e01e5055986565a4e59c781f1" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_elf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-08-01 02:11:10"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "elf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "bd5deff218b2e78a3707670153bb8c206145be3cfce6b4cfc1b5acf108dc42c7" or
hash.sha256(0, filesize) == "25b9ed78450eb9ebad4e850a3bc71f108dbaf75f6e5a84de1b5ac7680bb5b0cb" or
hash.sha256(0, filesize) == "370c49996924705467f396ba7a2a2863a38f5051790b7b46cd667235a6036a32" or
hash.sha256(0, filesize) == "2bba5d4e69bcb1baeb05d778e9f8f7a4f637beea0a8f2a43547ea02fd18630a7" or
hash.sha256(0, filesize) == "dfccaa38a2e408779483bbd0da0bdfe3323465fda1ff413328c81d32a7713eb9" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 04:27:42"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "ba698cd54cd138ebd1df234e780463070b9e108e980512fb33a857af1c3c6248" or
hash.sha256(0, filesize) == "82b354f83928919dc0c81da93c72bb4013366e856679090021c8cf92df2f2810" or
hash.sha256(0, filesize) == "1aa319975a9d1142cd5737d4b41d1004223881bf4e3485770a75be645e54934e" or
hash.sha256(0, filesize) == "b9008341c54406621f94780f0d01e2cf61946a48101738fa73531687a5f1caad" or
hash.sha256(0, filesize) == "4ff2e8cb6e81b9946d403ee59271618620e4e55164d9b591be3d648425505185" or
hash.sha256(0, filesize) == "129f85ae2da5c30f9439ef123d09d75e2930f1c025e9913a91c212bbe5758c0b" or
hash.sha256(0, filesize) == "16bc432fb8bd0b28cc9964382523f9c18415c7188f629b1a1b865acea7e07b09" or
hash.sha256(0, filesize) == "e7381cc97a46b747466b313387a75b99c21aac2ee4d0405323d97dc364bd4202" or
hash.sha256(0, filesize) == "44828584c1957e1e8ed8b43fd4102c392bc70a33605f3f3dafbaa8635ba0c2bd" or
hash.sha256(0, filesize) == "b1dd2f23a4e0925adcfe19c55bf701af33e6d3d66f3fd1537d30ea7ceddee9a4" or
hash.sha256(0, filesize) == "ce2bdcc4087d372411c30e4d003a90c7794accf14004a5200fab1948b0c94659" or
hash.sha256(0, filesize) == "0cf8c5e306889b3049cd0b4449bb02572576db25d4137491b527677b828af9e9" or
hash.sha256(0, filesize) == "77f42c84ab266e5cb7c3ac457e324874c7b6e3b1f64191ada33bcab82f33a57d" or
hash.sha256(0, filesize) == "cd1a3a3951014346894a253fa1a9dc05b221640be311dc679a83b4f91b1449f0" or
hash.sha256(0, filesize) == "50e477905ddd38a8b1fde3c4588fe0d7564267b6b91f5e68fd77fad07090fcfc" or
hash.sha256(0, filesize) == "91cebe14386e053247997445208e08475f64480520316f826e252dd11431e240" or
hash.sha256(0, filesize) == "ac0ec8320601c7a274a4a97536cc6f3387964581b1a676d0c276381437967ca0" or
hash.sha256(0, filesize) == "9ec26251cd3ecfd1b63b02ff4b70961724f9aa4ee2fc1390de80bab52286f586" or
hash.sha256(0, filesize) == "c4ab03eb1096d5643db922730824168efa45ba7f308c3336c47558360fa8b44b" or
hash.sha256(0, filesize) == "242c46363e5922bbda1ebb8078362cdb3dfaae0b9bd03cade10b74b4afee8d9c" or
hash.sha256(0, filesize) == "17b7a8bce60617c9f97a3464bbdba87d94da9c08b533fc07f3727376feae538d" or
hash.sha256(0, filesize) == "974299d11689e7d89d4994a9e64c6e1db9516ac3a085c079f2be6b2491aa1dfd" or
hash.sha256(0, filesize) == "e163660f2b270299aa1ff5846e0b7b8d9eac1f91ad2d3f5cfe3cfc261123bdcf" or
hash.sha256(0, filesize) == "f93f7d0bc7a656a43a5a3e19aa837852572e89a8402bd3101a9374ddb45842b3" or
hash.sha256(0, filesize) == "b11a4c548a5b3cd421e38904f4131678b6418ab9264f2aa53eef20d1689ae28f" or
hash.sha256(0, filesize) == "2ad4c4f27ff93553beebc1e426bd73cb6415c88bb233442fdc727de557a95c2d" or
hash.sha256(0, filesize) == "86233dde1f7f4fc654835a61c19c671d221a8b6c60c2b8bf47971a09a9b834fc" or
hash.sha256(0, filesize) == "50cc3104bd011e1bc093f9d532454a2fecfe88d36609897ee10e0be465237ad9" or
hash.sha256(0, filesize) == "bd8c4eb8ae0047367c1d20d2132d3a2fe2bdf0d197001ea4ecaa3816d59c84e9" or
hash.sha256(0, filesize) == "e949856ccd8b9d36fb7c2322f2c09d2a969c0121c9b08361cf16dc08c316d3ad" or
hash.sha256(0, filesize) == "ace5aaaa22b0abac1b5201cc8e3d8da42861d29682912a0a4b5fb39fa79c08a2" or
hash.sha256(0, filesize) == "8471aa8030a6a8499690bca54915d620941eb7c2083c8f39658d379dab9d406d" or
hash.sha256(0, filesize) == "0255e2579c043b9a3557a95a0a37af0dceb75077680a436087c70268d70411de" or
hash.sha256(0, filesize) == "396b5562de7bc8b4652e763c8241d7b55aaa02c563278597163b768acdfc306d" or
hash.sha256(0, filesize) == "7b146c7f767b0f6b2468dd7a7c16d781b2ac1d0b54d5ced897fc67c72aa43b5b" or
hash.sha256(0, filesize) == "198a27bb3eafb16e85363be12dc849311bc4e25043794c5ee1364f2422dbdf4d" or
hash.sha256(0, filesize) == "9f31bb30ebd91a758214962fd6ead4a2fb6b5dc99a4e4296084a3af02eae2b8a" or
hash.sha256(0, filesize) == "3e6e5db115474d3d62f5b5c14dff34d5bebc993c0622de5419ff26cfea5fbca8" or
hash.sha256(0, filesize) == "23a66e6506fb3f90e082b018ffb8570f475bc5d41c3721c7e4b93a7d18138548" or
hash.sha256(0, filesize) == "6d0ab1c3aadadef9335264a57ef26a307636d0367f57695f95284f03fa5e4111" or
hash.sha256(0, filesize) == "fb0533fddd270310cba6cd3a1cae5f23356a0a28cfdb85caf6dc0c41f8c19590" or
hash.sha256(0, filesize) == "485b45513f839ff3c972cbd2434273388b87c070c8c6125a8462aeabe2fb96d5" or
hash.sha256(0, filesize) == "3f4a8984909eda1e1aecac2a21bc6db08b748928b9e32fa468a6296ec52e4602" or
hash.sha256(0, filesize) == "e66836eb3337c16dd9b5f6d4b1a744827daf92f0722e0034748630cc5f5decb6" or
hash.sha256(0, filesize) == "9357718e3aaeef225d04c8f1d8fde88bd210fff3c1205ee84c0c1f7327bd17f3" or
hash.sha256(0, filesize) == "dabc270dd34ae99b08391c5db84d3972cfd2073431f96860a8f19fa398b4ae93" or
hash.sha256(0, filesize) == "7f34ca6249b2e1d2b908765af70d5f3bfe645407c6ff65a3e2a24ffeb7b2ddc9" or
hash.sha256(0, filesize) == "ae26382f191225447550e9a691453fc3ea2e02127222787c662efc8db63c59e3" or
hash.sha256(0, filesize) == "7fd4f276c26f9d25e10af688e4b21db9719e2f03b9ef0b33eb9f6fa83a1a6591" or
hash.sha256(0, filesize) == "b7ba35e9eab9d351fe253284cefe14b16cb8312d56c9f96a309b851e515d1090" or
hash.sha256(0, filesize) == "eb8e351b8fdaadf5429c19d052428ab81b91170406b937d0f3753e334a757fe3" or
hash.sha256(0, filesize) == "946cb5dd9fb12e5afeda48574c0614820ec6499fb0a38e4b70d58ac5dedf9d69" or
hash.sha256(0, filesize) == "9e7eaf53bfef161396377f312d623d10c49746779710206acbe408496e50d68e" or
hash.sha256(0, filesize) == "8e0b90738a755ae2c8f37c3bda29687984e9380985d5b6701d43494aa33ae416" or
hash.sha256(0, filesize) == "e4e644b7281e2a855c0408ff61cbf4c27b30d8bfdf83df8881af56f4ba3f05a0" or
hash.sha256(0, filesize) == "cd5b9a2232aa8c58cda9de46393561c776626722533a69f330cf47090e09db2e" or
hash.sha256(0, filesize) == "3f38cf8bf4f45838256ae62125c621a163eca64f52277acb7244e2ce54041224" or
hash.sha256(0, filesize) == "3e1bb2f560706daed48028a385a067c689404620742370061a70302ff5e5a1f6" or
hash.sha256(0, filesize) == "77a90478610c27bfc5d1c59e4d57f321de96504d8ee2e7787bd4b36065a06c12" or
hash.sha256(0, filesize) == "645ad77845422397cbbf8d693ac930047571abae040f058b88d2b5555b1f0754" or
hash.sha256(0, filesize) == "74ffb384c8a3ac7a8ad39faac568e6ff38665af684fc7d4a0661e4f7e563a8bc" or
hash.sha256(0, filesize) == "4f14ea5723dc55b7fc4a76f7bc7f5a834a16f531d8d47342b9a64a79678d417b" or
hash.sha256(0, filesize) == "cf5076ce2ae1cbfb94993dbfb3f5f2c9c488fda02aa30da75693645a214b6459" or
hash.sha256(0, filesize) == "2572cbafc999216fe489d457721d60891da56a4936aa48a9ef822dac6ef83696" or
hash.sha256(0, filesize) == "7d843637f3c3a97b7fbaafc8748539cd452c3472720eb282d472d22c2bfbcae8" or
hash.sha256(0, filesize) == "2743a30069ac7bd06abed1ca7b5a867d034b3c0793d92eba2b91bce0e98f67f6" or
hash.sha256(0, filesize) == "a394d1c47fbaacefdfb9bf4b4e39471fd17ef82a107cc0d29306a7de6e45743d" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FORMBOOK_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FORMBOOK_LAB"
date_IOC = "2023-07-31 06:29:37"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FORMBOOK"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "5348bcccead193b46acc11339b9c3ff05395753d536a0952641af34382e08604" or
hash.sha256(0, filesize) == "2c9e58b98c0929168074d7742170d85eb83c23f97aff8786381aec250af1d6f8" or
hash.sha256(0, filesize) == "09bf94af5ceabc708551e3bf599d3d94c8b3ced7606f2018c506cfc9b5a21710" or
hash.sha256(0, filesize) == "181c961adb5e3152a9384f18a7fecdc0574a311640434b1a98b3b514311ed645" or
hash.sha256(0, filesize) == "6a3ea5ef986f8bc290a7393c4a890e87b583f5d30d2afa771b3540a54ae0313e" or
hash.sha256(0, filesize) == "ce28b5bb36eae2c073a5856951a22de3968efd713df1c4e373b1e16cfe5ae255" or
hash.sha256(0, filesize) == "e6db950824ebaa85c4bd6b49915bee14d1bbdb7124e68e5494fce0b6d4ae7b38" or
hash.sha256(0, filesize) == "679e687ae1611a7eb7d00d06c9f8ae37b9168838c9ff9b822174f6b0de6304d0" or
hash.sha256(0, filesize) == "1a166a2469e8634ab832469a47076affd97eec7d5b4855f33879960e559a235d" or
hash.sha256(0, filesize) == "9741f09bf253fafd56b462e747b2f72ea181eda7172af3244058f3444e549e0f" or
hash.sha256(0, filesize) == "d834b1c88d442842f60e1c7ee077cab03f844ea7769ca66ebf0b327af1047789" or
hash.sha256(0, filesize) == "83d83343a9fa330be703706b44275305825d3908458286e9b011e194783c5079" or
hash.sha256(0, filesize) == "d0441e0b51d91f61154e0c149b3a1a69473cc5eb5004d0b7850d9d7195c8aeb7" or
hash.sha256(0, filesize) == "8b8d1eeb091a02dfc6019fc94feaef08287385ab9cecb1e05cd613344be8cd35" or
hash.sha256(0, filesize) == "8628f8bf13db33a13b131c2f1d59d50456c4c807a98ff92578b6112ca050473a" or
hash.sha256(0, filesize) == "291885e66da48c5ea16c3552a947993bda7f8fcf6572aad611adc59ed276897f" or
hash.sha256(0, filesize) == "623c473171fe352776ad9f44c81781ad120c2df8252a347eae1348d5d7f0f756" or
hash.sha256(0, filesize) == "73ff2753b011fd1295ce8a2ca311a308bdc993e95258fe0b93ee353b08e58403" or
hash.sha256(0, filesize) == "4bc2162ae0ae133d359603b912865d8ed2f26029421a7dcfcf734f2aee216923" or
hash.sha256(0, filesize) == "455919e0f632a8110dd692a79c784dace5bc2f981e9751d8fba5ccf37bb50d33" or
hash.sha256(0, filesize) == "c69b660c246706c03fa2a9ac42c9947515de273b76cde68c62549a6d9f5e3e9f" or
hash.sha256(0, filesize) == "5bad70f6450f10a687e5fd74019fbfa8efc9a70069c049b252dd0e2d0fd932ac" or
hash.sha256(0, filesize) == "995d5013bce09437806040d688105e78d8420ddc8dc93b993199c04589c8806c" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_COINMINER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_COINMINER_LAB"
date_IOC = "2023-07-31 07:23:29"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "COINMINER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "13310e4b5dfe1f4139a661af7f1561a55e85f4b1afd9460c5022b10ef322ab6f" or
hash.sha256(0, filesize) == "1ad8fc7446d7b601cd269425d9c556c73ee7b863a866bfb0e8a998355c1e898a" or
hash.sha256(0, filesize) == "054715e9dcac6afc1257b2d304f49876a62e7bc96cea3c593d69007e87f3581b" or
hash.sha256(0, filesize) == "bd61459061571af387a855ff79aa71ab1a0b3f2005572a789c71cccd12a6fdce" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_pdf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 06:52:00"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "pdf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "7dae530ace3b6d84b05050a53ab7dbfeb29920f5627827714c973c41b000a04b" or
hash.sha256(0, filesize) == "fe8f2b78ab48a3e597ce566a5ceee60cf2d999fc73ac1c99ec37a802cac91a06" or
hash.sha256(0, filesize) == "c906ab1c58ac70fece30126edbb043f2ff8ca7da614f2e2a0f3c98a42c8a2431" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_xlsx
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 07:17:11"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "xlsx"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "40dd9ae37b95be98534af43904353406debd8f9a95557279b0be3a9cb73dc4ed" or
hash.sha256(0, filesize) == "d787035485bbb5297b18bd6bbb11af735181435011fa0e68ecb4e73e5d4801cb" or
hash.sha256(0, filesize) == "546e312963d6dc654858d939ee2371b81b940eb95dca1dca8cf452b475de8157" or
hash.sha256(0, filesize) == "4a7936d2046d9b11e44b82f8a8870599504caeb5d65ecd4a0ac2c39d7c3c8a93" or
hash.sha256(0, filesize) == "8be32d79ef965a52db139292fae09bed7b51734a1a38d917a99af7d8d282f05e" or
hash.sha256(0, filesize) == "9ac67a8d47b91bf1fb3d5f97a4ded9c014e7f153765ed37f7de196011b102315" or
hash.sha256(0, filesize) == "cd2cb936e95479be3ae468d5390a8b6e94b84c76a129a91923ade7f6c751cac6" or
hash.sha256(0, filesize) == "df0dcbf2e2200b9b7443faf67f6f7746558f82df47894d87741bb090966f072f" or
hash.sha256(0, filesize) == "125bfc7a666a4fddf58a9ce69bc5cb16f84d804c1d622fbff0958bae16b8c98a" or
hash.sha256(0, filesize) == "0300a565e10a5a667e53367fd0be46f2f9dc2292f580db0f92a4269ba8e68a2a" or
hash.sha256(0, filesize) == "549e5ebf9dc96357e79a4c8f76b651a8823bdfa01ddc7d8b22d38ec9af66baec" or
hash.sha256(0, filesize) == "9718029bc8351010b4d01977d1b6440449921404d175b7bf268bdf46ee5fdcb2" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GULOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GULOADER_LAB"
date_IOC = "2023-07-31 06:27:05"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GULOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "0833d3623ff0b7912e1cf1379ffb1f838077f6e0dc9fc93f1260117bc24c02bd" or
hash.sha256(0, filesize) == "df5fe9a0ba7f10d92cb21521aaa7850da19e7b3cfee35c2387dfe5d28e3480b3" or
hash.sha256(0, filesize) == "5288a90e3ffc7fb7c13e74849005440fdf35467f2102cf4a49ae1b9bc18ec8d0" or
hash.sha256(0, filesize) == "d184eebb08b5c307945c25f3f355a191802574b12449f707891ffd4697a84efd" or
hash.sha256(0, filesize) == "7d982b49456267e913a88fa59dc74f405acc6f9f9d27038cf54596523877c799" or
hash.sha256(0, filesize) == "e4166f298cbdea94680f5bbf8d711cffc45da1f3d31958514f9542e1b052adac" or
hash.sha256(0, filesize) == "e404268152cd1de192e01e4965f4768dbd1d2d2a1f21a3ce9db0c20aa742499b" or
hash.sha256(0, filesize) == "6f61121924dbe1b34c6af8af8ee78528ca9d839856c9fd8ffe8a3a2e48cae506" or
hash.sha256(0, filesize) == "d206ccc4397de8003b4770d67ea8315e4d997f519c694b0d1f0a59451389277d" or
hash.sha256(0, filesize) == "335f5cd155653a07ee6eee171f272c7e02bd22065b1dd856c23206a00ab9a4e5" or
hash.sha256(0, filesize) == "4b7d1b8ea4216a534fd58d14e57d896be794d15ac910ff2b3c31a9762fdb6923" or
hash.sha256(0, filesize) == "8e886adf2b0ab4809abfefc7ffe5906a5e00629dd89bca322c85f7f6ace48024" or
hash.sha256(0, filesize) == "62e76dd8a7ab2f0e462b5679a273f1f7c463faa232fa2a0900ba56298788e17e" or
hash.sha256(0, filesize) == "ddcacbb25e7da7516ba0d502fdd5b274b284481a32a52e698321cad7b63e43cf" or
hash.sha256(0, filesize) == "db95b1fd14afe70b52d79131a53865993353ccfc5070146615a7c4f1a115d1bb" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AMADEY_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AMADEY_LAB"
date_IOC = "2023-07-31 06:34:44"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AMADEY"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "2e976baf097df5f017d2ed15f3456345d0180afbf5910432d7629a29fdf75fef" or
hash.sha256(0, filesize) == "23d2138b76775d5169145dedfaff7db5bca58b481994ced84cade8490e720fc1" or
hash.sha256(0, filesize) == "0ba15af0375d744fcc1a1b2ae1653ee994baf9a4ba152608eb60e954155a285b" or
hash.sha256(0, filesize) == "76561149747905d84bfa8837e17244dfa750b0d0a4dde56f5e63f6973fd347f8" or
hash.sha256(0, filesize) == "da73cc87b58e7bce392c5298eaa7bb748fc1a285e309f22a1ef2b81b6e372f1b" or
hash.sha256(0, filesize) == "5514e5a91e4b192cae4f78fc9d4d10641704c3778d0fd418f305b081ba5b9862" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_vbs
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-08-01 10:48:13"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "vbs"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "650f88a58b64ee406451a4c23bd7618ad2b640405a66c8bdbfc58927d3bfdf3d" or
hash.sha256(0, filesize) == "ebaa5d1c5367e8ae95833054050dcc192b9e18ae7402b0809dbcac3476019ff7" or
hash.sha256(0, filesize) == "a41aa6ac96ed5cceaa4ad9badd1b0d49c34aeb0513081a768fd0f9da2c4f9432" or
hash.sha256(0, filesize) == "4c5fad1cf5ffc7f0b75bbd79d05d204c5f4bef00905f791e33bf45e2da1b8349" or
hash.sha256(0, filesize) == "5843497eed3781c5569f53cd5709e93891fdb74cd12cdaff9487dd1d353dbe6b" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AVEMARIARAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AVEMARIARAT_LAB"
date_IOC = "2023-07-31 06:54:07"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AVEMARIARAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "1fe0659929bf6a67424138eddac1876c3fcc771e614bf90b286ecfe083641bdf" or
hash.sha256(0, filesize) == "fb51bb72c6b9b8b315becd1525ee0aaa0c66c215bec5131b04e84995e81c5b25" or
hash.sha256(0, filesize) == "9bccd0a03f1d6df0ec9ea530dbf51c462236f3b99b484efaf13e44a7cff82b62" or
hash.sha256(0, filesize) == "e2d531b4f2c134312878d444f7650cc9121b0374680f77b97a085f60e793d0eb" or
hash.sha256(0, filesize) == "a02344e64ee11821464a89bd717273adb4f5916ea7e8785bc6be7b0141a03ae2" or
hash.sha256(0, filesize) == "0b2fe9ed6a7e5da7f211a891a6b578a4dd1c850e546703e9d8bf6acb27da4a20" or
hash.sha256(0, filesize) == "575f7ed9b0676ffed65627bc4666c635921564f6360728131b1da0ef329b98af" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_RUSTYSTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_RUSTYSTEALER_LAB"
date_IOC = "2023-07-31 07:21:17"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "RUSTYSTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "57900d48479c89ae5474f50c40d5c58b2b05bf9baf9da4f0c2b25ab33b3a4291" or
hash.sha256(0, filesize) == "8149ba01c10af54023431c3796cfc72b5e30c5377bc641bc25dc031925620166" or
hash.sha256(0, filesize) == "e65881aa7f6c33636776089b3584dbf8a1c1e7d04de4ed286bb0d5a50aa769f5" or
hash.sha256(0, filesize) == "a00e0ffba9c2c81ef3a2f30a3fab80606bb943f9bf30233d3c26dd14c3a795c4" or
hash.sha256(0, filesize) == "493ad61378a62b59a6a4d0544a94f418a6086a38302e974ce007aafb36fc25f1" or
hash.sha256(0, filesize) == "fe96c32ee5e4a68691e1cca8b1898bd2376d592bc4e7e7330e1e91fde4a96659" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_QUASARRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_QUASARRAT_LAB"
date_IOC = "2023-07-31 00:53:25"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "QUASARRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "51fd7ec86eb447de524db155117eb5a0422bbec61aabf2a887f0ba2d6f8065d1" or
hash.sha256(0, filesize) == "dcbbadb9460751f4a7684af8b8bb8c78f3327b066445100d277af5c01f184f4e" or
hash.sha256(0, filesize) == "beeb985707b6f8539ad51664530bfb507ca9f65d9c1e6e716ca95f5d7e13291b" or
hash.sha256(0, filesize) == "3561b1eab2b650f0a714dc2da14bb751e03008a444dcc7dc1293eca7056727a2" or
hash.sha256(0, filesize) == "295a185629cf9edf7bcdddd8cf1a68c95bb82f4debfad3f5540075feada42d85" or
hash.sha256(0, filesize) == "c1d67650c1478f217e31fc7d54d9196bf6384d6e6edcafcc85f600a858ea2252" or
hash.sha256(0, filesize) == "1ead164add4a39d1dea816fef36c153347edd9516554db804fd847ebdaad8688" or
hash.sha256(0, filesize) == "6c16c890ebece47d2e9c9160c366e632fc7577ac766ae32ef640070481ab8c3e" or
hash.sha256(0, filesize) == "c0008144ddbf580b5aa762cdc847c84ea6222f9b47543c17ddb90d86cd7fd0ca" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_rtf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 07:16:18"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "rtf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "0581cf084cf7c01dea29d1c44b81cfaf4a6b1441a7f5e97840317066c2593d6b" or
hash.sha256(0, filesize) == "ca8cbf6d4259cd404477add5bcea6974eb9526eade7ee5560f78108d62f787d5" or
hash.sha256(0, filesize) == "abe2ccc4b4bb38f9e7af5cfe7dcc331c5a3ce286b6995b81cf26e011beba7e90" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_REMCOSRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_REMCOSRAT_LAB"
date_IOC = "2023-07-31 06:40:05"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "REMCOSRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "7247a3f88c9926488072d10907f19c9ed6b73f2ad2e218c89749d53957ba0362" or
hash.sha256(0, filesize) == "4367214e711a4e335c0ad9cf6b186e835b5e34ffcec7468ffa3afdd68dac0403" or
hash.sha256(0, filesize) == "be34a7f3fb6fd3a52d2c2fa5452b6cf7fb4afa00ca29bda99c0e4710771ef71c" or
hash.sha256(0, filesize) == "280d070644a6fef2f149ff79f5125c149d8b287f7debd7c30750a67231866d1d" or
hash.sha256(0, filesize) == "08f99aa27cbedd18401cfae07c7dd2e79966c6f63777fb95bc7a73c5cad5a537" or
hash.sha256(0, filesize) == "c06b82a4003da0da508dbad0b63fad050682b8490aefa104f4f9f016abb60fb6" or
hash.sha256(0, filesize) == "48fb43de46240bb31eb2e76cf6302d3ba008de77319692a326c0f684b4923a06" or
hash.sha256(0, filesize) == "7da5b2207cf789cf6807b6cc3373048cbc951d7fd09ca8fb858693cfa5f5edba" or
hash.sha256(0, filesize) == "b3b189de40e32305a83993fcda0d13f3a84200cd06e7d9549323940c603bff22" or
hash.sha256(0, filesize) == "90bceca2624bacafc6ee3a1ddf61107dc5d5dadaa733ab7323e8f96292a9df83" or
hash.sha256(0, filesize) == "9fa60053165fb875d9c7a4b23c33bf13eeb3bfc414a284921bf07df60a4181c9" or
hash.sha256(0, filesize) == "a0ebf0e5b7ddce607d73f58a9a3a676bc9cc4645bb1918c8ded7d287fd2275b9" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GCLEANER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GCLEANER_LAB"
date_IOC = "2023-07-31 06:40:50"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GCLEANER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "772ce07b410b21539ba1f40b6d4c58bbb78d3f14746257d96626644368e2f0fe" or
hash.sha256(0, filesize) == "215190f20cadb0dffea0aac14a17ecf3874e520f2dac49216e4dedc454a33aa5" or
hash.sha256(0, filesize) == "9d6bdf9c821aeb3a727c396b7b902d0d2d5eb0f55a6f4027574fb8bb16732f4c" or
hash.sha256(0, filesize) == "4636d352e5e5f0bce2ee610076bbe49bff0afefb7ab7b84411ed7ba1dd7df982" or
hash.sha256(0, filesize) == "d2a8f8ac4e31599db6b55aa90c81ff39e168e2a148393e91defa5c538f5d6ff3" or
hash.sha256(0, filesize) == "6deebfc2681d63379fe4efd299fd4eb2dc76c5f8fbf8b7d740302be4a877647b" or
hash.sha256(0, filesize) == "8fc055b97e29323ef0f570ef76b2bacdceb4f8d1b8a2eb62bb974f0abf03e5c0" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FABOOKIE_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FABOOKIE_LAB"
date_IOC = "2023-07-31 07:21:09"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FABOOKIE"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "c80c6cfe6aa7cf172a5c2ba370d82339357112495ad79e70c2f9bca05bda0e33" or
hash.sha256(0, filesize) == "e3815234522ee1f479c736d60bc4459b34b64b77bb1f5e13938d8541675fad99" or
hash.sha256(0, filesize) == "99839e1fa6c619ee000e97683cfc120c6bf8d8c5dd538e714a0377923d4536e2" or
hash.sha256(0, filesize) == "e5903742cad2793cbff490b24b0d56f929efefcc9639da45985ce5524c5513a7" or
hash.sha256(0, filesize) == "0fd3c496e9732329a5c7959ef40d78e6ce4ed1b3d5d573812f70459505140a50" or
hash.sha256(0, filesize) == "6339b9fc46aa632f3054259b9a47127e433479f36fe52dff78a554505ca817ef" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_ASYNCRAT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_ASYNCRAT_LAB"
date_IOC = "2023-07-31 06:46:38"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "ASYNCRAT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "1fe8f55ff197ec8cb7121a3c9946a1ff52ca18f9776243f68f9c199414c5706e" or
hash.sha256(0, filesize) == "5b3af8e442e66694d6aaba48bd26b4e52cc20e95beec609e9153adc4120b6c0d" or
hash.sha256(0, filesize) == "0e4df6585ab07e8e62e0072fcb86f4f6ef0bca491851ab448b0bdcc4ea9e4e0d" or
hash.sha256(0, filesize) == "8c85033c65a24be354cbee309aaeca8fce197c110f64e33d6365a0e6e6b00c2e" or
hash.sha256(0, filesize) == "d2f81bbe46925afb1f7e4c266d35e1e9f9fb5144690ae4218e54f4a7d68e1da4" or
hash.sha256(0, filesize) == "b62fc62a03e4d6c0eac25a8b104d8064288fdc5665ddc19ba55ed520ab9f2827" or
hash.sha256(0, filesize) == "ab8af698453096f711c7bcdfc196c8e13cfa93889976f7a95e94a35fc2758c02" or
hash.sha256(0, filesize) == "5896069020d48f052b8428fe941a2fc5b735c45b812998f07b260687a7a794a7" or
hash.sha256(0, filesize) == "dc8f1fa577b69088c4e572c204c2b40c33c59ac58e63d1977c5eb1a58d933e4d" or
hash.sha256(0, filesize) == "002502891e9e63904545fafdac5256575df15d3c9a556e9eb27a7b0c88c4569f" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_FORMBOOK_xls
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_FORMBOOK_LAB"
date_IOC = "2023-08-01 10:43:43"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "FORMBOOK"
file_type = "xls"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "e9f78852b31b35763d5ec70f9fab58d9195a08f35c419f23ea98d5dd2545c557" or
hash.sha256(0, filesize) == "cd1d7aded53f0b459eb7b06313f9f519909f6471dbc074b2cc80d7392ebc4d39" or
hash.sha256(0, filesize) == "deb58953ebd9cf2e4f2911de58ebf6e6fdf0caf40b3ce7ff453be6503332f3b6" or
hash.sha256(0, filesize) == "026b972cd5a3752844db26d1011e93933b00f15029f7aad5adae7a70637ef653" or
hash.sha256(0, filesize) == "4f10ceff719d30b79ce6a9a9ba3ba6177544d0b220ddc32a972a09670ee6a640" or
hash.sha256(0, filesize) == "b57ebed8d8c286a7168ae4f8187bc287e437adbd68a8ed9d385dc982f2da5444" or
hash.sha256(0, filesize) == "6c4f22a8de0e93b04bd1dd6d17ed70732f4efcfce622b25038a9f9c17c7426be" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_xls
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 06:23:07"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "xls"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "7332e56b52d0993782b3e13e6ebcfb45d6fdb7367e46a3eafe1bae387817ccef" or
hash.sha256(0, filesize) == "9e5fb11dc1007cfb422cfb4db61f479007419ed794e3be6e8aa4fcdc599bae15" or
hash.sha256(0, filesize) == "9269a36ba8da27b3a5f274ab10c119be533177e608f344c33d3ca17cede941fa" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SNAKEKEYLOGGER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SNAKEKEYLOGGER_LAB"
date_IOC = "2023-07-31 06:45:44"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SNAKEKEYLOGGER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "3587ab3f5a463683ef5c0a08dfd722285dd148be0b6c7d674447f54c103b0f4e" or
hash.sha256(0, filesize) == "4fed56d9edf0d59d245feaf620cfda9d2a0ac68cd311f2a9b60bddde94ea061b" or
hash.sha256(0, filesize) == "06a9ec554700c23590c89a5281d875fb042e1f8f1ce0ba8615883a4529cbe84f" or
hash.sha256(0, filesize) == "75fe2e05556461cf5b623ce60ba191ec4153ca13931260faa99ce558ac86914a" or
hash.sha256(0, filesize) == "4ea05268962825997751d50f975c217ee9e5cde265fefa4a5335ef2f39730852" or
hash.sha256(0, filesize) == "a44d262ed7c8085b22ad15653bd2a7d3ecc966c3e7c54252de5670ce64027847" or
hash.sha256(0, filesize) == "d00f8dee3e81decbb37ef2651c88d3ba46a959d5bfe1d71fc17afd8b4704b4bd" or
hash.sha256(0, filesize) == "9441f2f74776f33df8f0f42c53a6fccea0d3173c9a2f403a16d07958509116c7" or
hash.sha256(0, filesize) == "555af21b8831e78e3b1313dda0d2924af9507c4e701b6b42d0777d28a9405134" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LOKI_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LOKI_LAB"
date_IOC = "2023-07-31 05:35:18"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LOKI"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "93a26c45838c0147b6227526ef8abad9cfabb115300e703c0c169ca7d3a7d77e" or
hash.sha256(0, filesize) == "9582410d025ac1cdbeeb2fab3091cd50daba650e371fab2d82ed4c5f1d26f0af" or
hash.sha256(0, filesize) == "5388ab765bd614a7350d9ce7126afab89ca2e0b0e55d23e1cd43459cb9bc745d" or
hash.sha256(0, filesize) == "8ccb5dc4ee7dbe6c28d9b26670ebd57269e8d982c35f9098ebeb5bdd4abc2fbf" or
hash.sha256(0, filesize) == "e626db552e3975b073a3c3d621ea039c431f0431fa6e220d4b66cbf540a9a02f" or
hash.sha256(0, filesize) == "415213f9cb65250175ce01e2db87679d5ae7a09ac3ccb3d63e710848a0a3515a" or
hash.sha256(0, filesize) == "e1e3f4af3f0fa4927d325291b09f341f5441f0f673daa0873facea80e38c699a" or
hash.sha256(0, filesize) == "193314526f9742a532ee1f3c293064edf84caf372ca584cf059b0d3fbd0b6196" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_DARKCLOUD_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_DARKCLOUD_LAB"
date_IOC = "2023-07-31 06:54:01"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "DARKCLOUD"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "c71f0e07ad8827974bdc2096bd1db1f86831f997473e40b31167711a00c1fa37" or
hash.sha256(0, filesize) == "4e0c4ff1b04d55403948acded8a2a584b869a984d1f846a18f52c6bd67631337" or
hash.sha256(0, filesize) == "ec94fe18197f8fccb0e786717ba7fbafcb1f7376e6350e19c2cd7072e62dd204" or
hash.sha256(0, filesize) == "e85e26cd006afea56d7a13ffc41a0c177a21d8be7b132a4b4d8b71ed47bf2d0e" or
hash.sha256(0, filesize) == "d120e6bb00677771286e6b3f0d29425c87c3f6aa00cf454a4205c8cc746ea297" or
hash.sha256(0, filesize) == "8aed07e1593f87ba11f4d18a3dd8b1d2181a41784ee1c39ffbde7d87999cac45" or
hash.sha256(0, filesize) == "533c8e6de7ac10fba06ffc98f41626f8e8af3fff7c16407463b37d84df239dcb" or
hash.sha256(0, filesize) == "33d310746c4c533488b24c9ee3ce26246d3c7637c10dbb31b0c0bd59d6f6e3c9" or
hash.sha256(0, filesize) == "216612bf6ed5af82680461d3da4d1b3ebcddc54b91ecfa07634b8a9ebf5623d9" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_7z
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 06:48:55"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "7z"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "ffaf0dfe7ed8cb2f41a5c9dad3040118918f3997de4979bd1c89e8466d01c815" or
hash.sha256(0, filesize) == "f567d3c712fea95fa7643dfab89b7c193fa217217bc89e3a4b68101df0946183" or
hash.sha256(0, filesize) == "ff1d598bd62f40bbe2dea0737be2fef7319fb655261b7323d362f2ffc30ae62b" or
hash.sha256(0, filesize) == "545572036a4f5a01ebfdcce7eda2343ff6d6d08251dcb961d1ea0b33099674d2" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_STRELASTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_STRELASTEALER_LAB"
date_IOC = "2023-07-31 06:45:51"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "STRELASTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "b3070cd02c3da848cfbded87b39539094abdc28b79a6017176eb4b3f031bede0" or
hash.sha256(0, filesize) == "9f103e5cbfe2d0d2ff33f08885f0ab9c2d859031e16d62c7ec0932a957530887" or
hash.sha256(0, filesize) == "0a8ad4d66d69b43cb3e88cab50556f6a98db1e03634da4d7ed0d6b8ced7cb0bd" or
hash.sha256(0, filesize) == "58802ab24d5543b2eb8798584b957153d28c40fea4a81f747357d3437012f491" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AZORULT_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AZORULT_LAB"
date_IOC = "2023-07-31 04:09:49"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AZORULT"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "9345ae44b7e5e1a78088458c78eec3b6f511f2ddbdc0f31a694c413835b0eb12" or
hash.sha256(0, filesize) == "cb003e07b2f6b1286333fedb15c3e15389c8faa917c082fb04ede40a065ee55c" or
hash.sha256(0, filesize) == "34f4bbd112599ef33347225656011d8682bee4691b78071ebf1553d8b8393d2e" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_GAFGYT_elf
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_GAFGYT_LAB"
date_IOC = "2023-08-01 03:01:53"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "GAFGYT"
file_type = "elf"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "99b89211de870231a67b1b7ea9502e4b384203cf71e50b5657765773e58239df" or
hash.sha256(0, filesize) == "205210f856c1c5c18492754713ec1824323f09980ba3413da871c31c8151af46" or
hash.sha256(0, filesize) == "46ff9f7c0e437df7dd6e1c69790c8fc94e65091e9f3cf1f3243c808f1a1e8621" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_apk
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-30 23:13:15"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "apk"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "69260764e600edd5fbcde910809edea7d4f3f07840464b357a38ddaf2b73922d" or
hash.sha256(0, filesize) == "1b38f612a6e83edbdcaee91b8aa3ff439d3e9556bc27997909a6c8e30f8e0031" or
hash.sha256(0, filesize) == "beacce2fdd5bb72cbd9e8302410ae6c9e16ced5ec8aa52b7f07946e4b3d3e007" or
hash.sha256(0, filesize) == "3d52a2b78248dbe975c25d9b8a82f9883c7958d71f74703db70fae39de4b4c80" or
hash.sha256(0, filesize) == "fe87e9ca6a7a6da4fedc9b2dd7a0cf75b0658fb68776b7a853ed3e6f568d49ac" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_LUMMASTEALER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_LUMMASTEALER_LAB"
date_IOC = "2023-07-31 13:14:43"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "LUMMASTEALER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "ca63c3ed5c8183b324703ada2b10bf18602970ae9b613c2b3a2757c81636acad" or
hash.sha256(0, filesize) == "f09a014d0585a020f9caacb1b31a410fc83feda3aa3ce13b8764c8715b9e5d6a" or
hash.sha256(0, filesize) == "69246a1db69750de928338a36b96a92fa8e565b9a52e44214b525fe04cbc1e29" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_STRRAT_js
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_STRRAT_LAB"
date_IOC = "2023-07-31 07:00:22"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "STRRAT"
file_type = "js"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "3841d50e1acd5bb1af45f52b341e3b01fdb55f9881e587904719e9dec701855c" or
hash.sha256(0, filesize) == "99feb902857229fff2ca72162768e7bdf6d6677de01dff2bb3c96b4876f5d240" or
hash.sha256(0, filesize) == "5f8088e823507a50d26d2c25bbbf6036b5e2a21eb44b7f7bd96cdba4ddc5002d" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_STRELASTEALER_js
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_STRELASTEALER_LAB"
date_IOC = "2023-07-31 06:45:33"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "STRELASTEALER"
file_type = "js"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "2dad3b13f73049ed938bc8b49049f89a81b9cdb556d62b6f8c8f48e58c0c3492" or
hash.sha256(0, filesize) == "9ca8bcd4a994e89063911a220d95204e3ac744babe515b1c221725001d690b9e" or
hash.sha256(0, filesize) == "f7014b7fb1932f07dada4879b5368afe144ca03df046a0573a0d5c4a5fbecaba" or
hash.sha256(0, filesize) == "e4bc864a2d43f206c053b6fce4eaa08c04ad67ef1c137a2e9eb8e4cb6a5de012" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_rar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 09:18:14"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "rar"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "acbf15c97bf58b8232d5446cf977c31442d239153b5eed6d44273b85212d754e" or
hash.sha256(0, filesize) == "d9191a4bbef1483389a357e64ba442fbef2f285002984b73f83c3f80c17a70b7" or
hash.sha256(0, filesize) == "ef510c3e22e3337a4a9b95f3973a836b09ad8737017b3d2561dce355e1fdc8c9" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_XWORM_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_XWORM_LAB"
date_IOC = "2023-07-30 20:51:34"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "XWORM"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "44a79bc2cbc8edd8d9fe5c11e6a142559c07141fa8250af46f97072b8811fe2b" or
hash.sha256(0, filesize) == "91e32198b158ebb1632727783aa6b175e23fa704d1e2e3320c2465fba404a3d2" or
hash.sha256(0, filesize) == "73f15797675a797f60e6226bb83ea9d9e70c26151e5533ab4fa7ac6e0c34a8fe" or
hash.sha256(0, filesize) == "72ab332da034bd819d83d26272974048b24de773a3440d641202872161b3e514" or
hash.sha256(0, filesize) == "a4ea9aac544248e1346d88e3c93fbc6973419ff7ce5266c7cb00be39518f1f11" or
hash.sha256(0, filesize) == "52634ade55558807042eae35e2777894e405e811102e980a2e2b25d151fde121" or
hash.sha256(0, filesize) == "8948b34d471db1e334e6caa00492bd11a60d0ec378933386b0cb7bc1b971c102" or
hash.sha256(0, filesize) == "422ba689937e3748a4b6bd3c5af2dce0211e8a48eb25767e6d1d2192d27f1f58" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_SMOKE_LOADER_exe
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_SMOKE_LOADER_LAB"
date_IOC = "2023-07-31 07:21:15"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "SMOKE_LOADER"
file_type = "exe"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "2234e1b6cbc9e8f7f88cc6515b6c633b4aae6a668dd24da6f7bf40a3f1a7325b" or
hash.sha256(0, filesize) == "f8896ca2a901da194a2479237a084ee46b329ef65d0a6795eb3717cbbacb106f" or
hash.sha256(0, filesize) == "14fe31c6b82551cb23ed6001a0de68670f5ed09e2c135c0f8a39a11154150dae" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_ini
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 07:23:44"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "ini"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "98dce50a0b892075cab00bee4c5c9ddf91befb28f080e6d48454c852d5f0c488" or
hash.sha256(0, filesize) == "450ac21300745478462300d0cb5ece3224f11b9c276b0f12f378f8a97aee9016" or
hash.sha256(0, filesize) == "4536ff38e0ad3191aa7682ae532660f1b51d3d7f8dbcabb90fe9bdfa12eaada5" or
hash.sha256(0, filesize) == "dd2e6120ad65d4283cf2890935ae3dedd076feab4b144fe773d8c564c0d2e307" or
hash.sha256(0, filesize) == "95b753799e11ad4356df39d9d35c6474c46468eacc167f4f82ed175283d0e114" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 06:51:30"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "zip"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "743b95141f1adc891d847dc87d4f1446a26444999b2729d85b2f990618290718" or
hash.sha256(0, filesize) == "b2fbdaa0f4519667d0b62998d119f3a009b34144e6df793ca48ea3cc2092bed8" or
hash.sha256(0, filesize) == "c6acac28426ad1458d46fdb4805ad88d219e2b4d7cee07015bda1b0f9592d77e" or
hash.sha256(0, filesize) == "73d4adc82649f9cced7d5a63dacd458a8a15a6fbf2861b4f3daa71a259588151" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_AGENTTESLA_rar
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_AGENTTESLA_LAB"
date_IOC = "2023-07-31 05:50:02"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "AGENTTESLA"
file_type = "rar"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "43e44a3efd4e7da791291158ec12c4a3ebce61db35808fb7e87e7f3651dd8dd7" or
hash.sha256(0, filesize) == "0e1bc9c6235f2bb4af7f4dee4a3b69937cb6ab40bbce45c3396017cc5e84e217" or
hash.sha256(0, filesize) == "132ce56ab1c25edb0023919de4530eef2221702e34876e602d2906898de0e7ee" or
hash.sha256(0, filesize) == "86195d078722845092dee85faaf7b5f4bfc21ce0073c3d97aaac30f0f46b90cd" or
hash.sha256(0, filesize) == "50cbbcaa4d937557745df7efe8ac8d50114f47f62a9f45802465d57c3d93386e" or
hash.sha256(0, filesize) == "3b06635e45723f582f94799f463503d74702a52bec755f6fe0c45424a6f76948" or
hash.sha256(0, filesize) == "dea742765a05a991b4ed498de599ac7ca71eadfe5593cd406cbaf9147dfeaa89" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


rule IOC_NA_zip
{
meta:
author = "Laboratoire Epidemiology & Signal Intelligence"
ref_IOC = "IOC_NA_LAB"
date_IOC = "2023-07-31 00:39:44"
info = "Version 1.0 b"
internal = false
score = 99
risk_score = 10
threat = "NA"
file_type = "zip"
comment = "Source : abuse.ch"


condition:
hash.sha256(0, filesize) == "6187fcf6af0c78fbe6f20bcb3eb05255aa67c67ff43c607c4fcf57a9bf4b8c35" or
hash.sha256(0, filesize) == "397498adbf494f0bd6e671d0d1cf1449e1cb724e9b084d69b75f7006283745b3" or
hash.sha256(0, filesize) == "a610239afa7a6a36fd7277e0d6ef935a725bade98cbfec95283ca04115fda728" or
hash.sha256(0, filesize) == "7630f2f45ee1cc1fe77ddbbbae10c47fdb07192d83f7c3bfae968b840b8b3578" or
hash.sha256(0, filesize) == "831abc8d1a70104ae46b5c2c1ce6fce24ef449a03bde0d770a5a67f96ab22e7c"
}


